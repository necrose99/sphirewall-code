#!/bin/bash
# Loose script to build the debian packages
#
# To build for a new release:
#	-) You must increment the version number in the relevant files, ['configure.ac', 'setup.py', '.debian/changelog']
#	-) Update the release notes
#	-) Build the debian packages using this script
#	-) Build the sources tar balls using this script
#

VERSION=1.0.2.4
TEMP_DIR=/tmp/debian_build
RESULT_DIR=/tmp/debs
SOURCES_DIR=`pwd`

mkdir $RESULT_DIR

ensure_versions_are_correct() {
	if [ "x$(grep -e $VERSION sphirewall-core/configure.ac)" == "x" ]; then
		echo "Sphirewall Core does not have a correct version in configure.ac";
		exit -1;
	fi
	if [ "x$(grep -e $VERSION sphirewall-kernel/startstop.c)" == "x" ]; then
		echo "Sphirewall Kernel does not have a correct version in startstop.c";
		exit -1;
	fi
	echo "Binary versions are good"
}

clean_or_create_tmp() {
	rm -Rf $TEMP_DIR
	mkdir $TEMP_DIR
}

#Library Methods -- These build the packages based on a template
build_kernel_debian_pkg() {
	clean_or_create_tmp

	cd sphirewall-kernel
	cp -Rf .debian debian
	dpkg-buildpackage -us -uc -b	
	cd ../
	mv sphirewall-kernel-dkms_*.deb $TEMP_DIR/
        rm -Rf sphirewall-kernel/debian
        cp $TEMP_DIR/*.deb $RESULT_DIR
}

update_change_log(){
	SOURCE_DIR=$1
	PACKAGE=$2

cat > $SOURCE_DIR/.debian/changelog << EOF
$PACKAGE ($VERSION) stable; urgency=high

  * major release, see notes on sphirewall.net
 -- Michael Lawson <michael@sphinix.com>  `date --rfc-2822` 
EOF
}

build_python_debian_pkg(){
	SOURCE_DIR=$1	
	PACKAGE=$2

	clean_or_create_tmp	
	cd $SOURCE_DIR 
        python setup.py sdist
        cp dist/$PACKAGE-$VERSION.tar.gz $TEMP_DIR
        cp .debian $TEMP_DIR -Rf
        cd $TEMP_DIR
        tar zxvf $PACKAGE-$VERSION.tar.gz
        cd $PACKAGE-$VERSION
        python setup.py --command-packages=stdeb.command debianize
        rm -rf debian/patches
        echo "$PACKAGE.egg-info/*" > debian/clean
        cp ../$PACKAGE-$VERSION.tar.gz ../${PACKAGE}_$VERSION.orig.tar.gz
        cp ../.debian/* debian -Rf
        dpkg-buildpackage
        cp $TEMP_DIR/*.deb $RESULT_DIR

        cd $SOURCES_DIR
}

build_standard_debian_lib(){
	SOURCE_DIR=$1
	PACKAGE=$2

	clean_or_create_tmp
	cp $SOURCE_DIR $TEMP_DIR/$PACKAGE-$VERSION -Rf
        cd $TEMP_DIR/$PACKAGE-$VERSION
	cd ../
        tar zcf $PACKAGE-$VERSION.tar.gz $PACKAGE-$VERSION/
        cd $PACKAGE-$VERSION
        echo "" | dh_make -f ../$PACKAGE-$VERSION.tar.gz -l -c gpl
        cp .debian/* debian/

        dpkg-buildpackage
	
	cp $TEMP_DIR/*.deb $RESULT_DIR
	cd $SOURCES_DIR
}
build_standard_debian_pkg(){
	SOURCE_DIR=$1
	PACKAGE=$2

	clean_or_create_tmp
	cp $SOURCE_DIR $TEMP_DIR/$PACKAGE-$VERSION -Rf
        cd $TEMP_DIR/$PACKAGE-$VERSION

        autoreconf -i
        cd ../
        tar zcf $PACKAGE-$VERSION.tar.gz $PACKAGE-$VERSION/
        cd $PACKAGE-$VERSION
        echo "" | dh_make -f ../$PACKAGE-$VERSION.tar.gz -l -c gpl
        cp .debian/* debian/

        dpkg-buildpackage
	
	cp $TEMP_DIR/*.deb $RESULT_DIR
	cd $SOURCES_DIR
}

build_standard_sources_pkg() {
	PACKAGE=$1	

        cd $PACKAGE 
        autoreconf -i
        sh configure
        make -j4
        ldconfig
        make dist -j4
        cp $PACKAGE-$VERSION.tar.gz $TEMP_DIR/
	cd ../
}

build_source_packages() {
	build_standard_sources_pkg "sphirewall-libs"
	build_standard_sources_pkg "sphirewall-core"
	build_standard_sources_pkg "sphirewall-ana"

	tar cfz sphirewall-utils-$VERSION.tar.gz sphirewall-utils/
	tar cfz sphirewall-scli-$VERSION.tar.gz sphirewall-scli/
	tar cfz sphirewall-wmi-$VERSION.tar.gz sphirewall-wmi/
	tar cfz sphirewall-kernel-$VERSION.tar.gz sphirewall-kernel/

	mv sphirewall-scli-$VERSION.tar.gz $TEMP_DIR/
	mv sphirewall-wmi-$VERSION.tar.gz $TEMP_DIR/
	mv sphirewall-kernel-$VERSION.tar.gz $TEMP_DIR/
	mv sphirewall-utils-$VERSION.tar.gz $TEMP_DIR/

	tar cf $TEMP_DIR/sphirewall-all-$VERSION.tar $TEMP_DIR/*.tar.gz
}

build_python_basic_debian_pkg(){
	DIRECTORY=$1
	PACKAGE=$2
	cd $DIRECTORY
	rm -Rf debian
	cp .debian/ debian -Rf
	debuild -i -us -uc -b
	cd ../
	cp $PACKAGE*.deb $RESULT_DIR/
}

main(){
	#ensure_versions_are_correct
	if [ $# -eq 0 ] 
	then
		echo "Sphirewall Packager"
		echo "1. Build All Debs"
		echo "2. Build Sources Tar"
		echo "3. Upload To Snapshots"
		echo "4. Upload To Release Folder (commented out currently)"
		echo "5. Upload To Debian Repository (commented out currently)"
		echo "6. Cleanup"
		echo "7. Update debian changelogs (required before 1.)"
		echo "0. Exit"
		read opt
	else	
		opt=$1	
	fi

	if [ $opt == 1 ]; then
	       	build_python_basic_debian_pkg "sphirewall-crashreporter/" "sphirewall-crashreporter"
	       	build_python_debian_pkg "sphirewall-wizard/" "swizard"
	       	build_python_debian_pkg "sphirewall-dbcollector/" "sphirewalldbcollector"
	       	build_python_debian_pkg "sphirewall-autorun/" "sautorun"
	        build_python_debian_pkg "sphirewall-scli2/" "scli2"
	        build_python_debian_pkg "sphirewall-api/" "sphirewallapi"
    		build_python_debian_pkg "sphirewall-cc/connector" "sphirewallcc"
      		build_standard_debian_pkg "sphirewall-core" "sphirewall-core"
      		build_standard_debian_lib "sphirewall-ppp" "sphirewall-ppp"
      		build_python_debian_pkg "sphirewall-captiveportal" "sphirewallcp"
		build_kernel_debian_pkg
        	exit
	elif [ $opt == 3 ]; then
		ssh root@repo.sphirewall.net -C "mkdir -p /external/mirror/snapshots/debian/all/current/`date +%Y/%m/%d`"
		ssh root@repo.sphirewall.net -C "mkdir -p /external/mirror/snapshots/src/all/current/`date +%Y/%m/%d`"
		scp $RESULT_DIR/*.deb root@repo.sphirewall.net:/external/mirror/snapshots/debian/all/current/`date +%Y/%m/%d`
		scp $RESULT_DIR/*.tar root@repo.sphirewall.net:/external/mirror/snapshots/src/all/current/`date +%Y/%m/%d`
		exit

	elif [ $opt == 4 ]; then
                ssh root@repo.sphirewall.net -C "mv /external/mirror/releases/stable/debian/i386/*  /external/mirror/releases/old/stable/debian/i386/"
                ssh root@repo.sphirewall.net -C "mv /external/mirror/releases/stable/debian/amd64/*  /external/mirror/releases/old/stable/debian/amd64/"
                ssh root@repo.sphirewall.net -C "cp /external/mirror/snapshots/debian/all/current/`date +%Y/%m/%d`/*.deb /external/mirror/releases/stable/debian/i386/"
                ssh root@repo.sphirewall.net -C "cp /external/mirror/snapshots/debian/all/current/`date +%Y/%m/%d`/*.deb /external/mirror/releases/stable/debian/amd64/"
                ssh root@repo.sphirewall.net -C "rm /external/mirror/releases/stable/debian/amd64/*i386*"
                ssh root@repo.sphirewall.net -C "rm /external/mirror/releases/stable/debian/i386/*amd64*"
                exit

	elif [ $opt == 5 ]; then
		ssh root@repo.sphirewall.net -C "rm *.deb"
		ssh root@repo.sphirewall.net -C "wget -r -l1 --no-parent -nH --cut-dirs=7 http://mirror.sphirewall.net/snapshots/debian/all/current/`date +%Y/%m/%d`"
		exit
	elif [ $opt == 6 ]; then
		clean_or_create_tmp
		rm -Rf $RESULT_DIR			
		exit
	elif [ $opt == 7 ]; then
		update_change_log "sphirewall-core" "sphirewall-core"
		update_change_log "sphirewall-ppp" "sphirewall-ppp"
		update_change_log "sphirewall-kernel" "sphirewall-kernel"
		update_change_log "sphirewall-api" "sphirewallapi"
		update_change_log "sphirewall-scli2" "scli2"
		update_change_log "sphirewall-autorun" "sautorun"
		update_change_log "sphirewall-cc/connector" "sphirewallcc"
                update_change_log "sphirewall-dbcollector" "sphirewalldbcollector"
                update_change_log "sphirewall-captiveportal" "sphirewallcp"
                update_change_log "sphirewall-wizard" "swizard"
                update_change_log "sphirewall-crashreporter" "sphirewall-crashreporter"
		exit
	else
        	exit
	fi
}

main "$@"
