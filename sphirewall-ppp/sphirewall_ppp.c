#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <pppd/pppd.h>
#include <pppd/fsm.h>
#include <pppd/lcp.h>
#include <pppd/ccp.h>
#include <pppd/ipcp.h>
#include <sys/stat.h>
#include <net/if.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <signal.h>

#include <sys/socket.h>
#include <strings.h>
#include <string.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <linux/types.h>

#define BUFMAX 1024

const char* AUTH_OK = "{\"code\":0,\"response\":{\"response\":0}}\n\n";
const char* SPHIREWALL_HOSTNAME = "127.0.0.1";
const int SPHIREWALL_PORT = 8001;
const char pppd_version[] = VERSION;

/* State variables */
char* state_username = NULL;

static char *option_realm_key = NULL;
static int option_authenticate = 0;
static option_t options[] = {
	{ "realm_key", o_string, &option_realm_key, "Sphirewall realm key" }, 
	{ "authenticate", o_int, &option_authenticate, "Sphirewall realm key" }, 
	{ NULL }
};

int sw__pap_check_hook(void){
	return option_authenticate == 1 ? 1 : -1;
}

int sw__pap_passwd_hook(char *user, char *passwd){
	return 0;
}

int make_sw_request(char* response, char* request){
        int sockfd,n;
        struct sockaddr_in servaddr,cliaddr;
        sockfd=socket(AF_INET,SOCK_STREAM,0);

        bzero(&servaddr,sizeof(servaddr));
        servaddr.sin_family = AF_INET;
        servaddr.sin_addr.s_addr=inet_addr(SPHIREWALL_HOSTNAME);
        servaddr.sin_port=htons(SPHIREWALL_PORT);

        if(connect(sockfd, (struct sockaddr *)&servaddr, sizeof(servaddr)) < 0){
		warn("sphirewall_ppp.so: Could not connect to the sphirewalld process, denying access");
                return -1;
        }

        if(sendto(sockfd, request,strlen(request),0,(struct sockaddr *)&servaddr,sizeof(servaddr)) < 0){
		warn("sphirewall_ppp.so: Could not sendto the sphirewalld process, denying access");
                return -1;
        }

        return recvfrom(sockfd,response,BUFMAX,0,NULL,NULL);
}

int make_auth_request(char* username, char* password){

	char* buffer = calloc(sizeof(char), BUFMAX);
        char* request = calloc(sizeof(char), BUFMAX);
        sprintf(request, "{\"request\": \"auth/realm/login\", \"args\":{\"username\":\"%s\", \"password\":\"%s\", \"realm\":\"xl2tpd\", \"realm-key\":\"%s\"}}\n", username, password, option_realm_key);

        if(make_sw_request(buffer, request)){
                if(strcmp(buffer, AUTH_OK) == 0){
			warn("sphirewall_ppp.so: sphirewall granted realm access");
                        free(buffer);
                        free(request);
                        return 1;
                }
        }

	warn("sphirewall_ppp.so: access was denied");
        free(buffer);
        free(request);
        return 0;
}

int make_join_notication(){

	char* buffer = calloc(sizeof(char), BUFMAX);
        char* request = calloc(sizeof(char), BUFMAX);
        sprintf(request, "{\"request\": \"auth/realm/join\", \"args\":{\"username\":\"%s\", \"local_ip\":%d,\"remote_ip\":%d, \"device\":\"%s\", \"realm\":\"xl2tpd\", \"realm-key\":\"%s\"}}\n", 
		state_username, ntohl(ipcp_gotoptions[0].ouraddr), ntohl(ipcp_hisoptions[0].hisaddr), ifname, option_realm_key);

        make_sw_request(buffer, request);
        free(buffer);
        free(request);
        return 0;
}

int make_leave_notification(){

	char* buffer = calloc(sizeof(char), BUFMAX);
        char* request = calloc(sizeof(char), BUFMAX);
        sprintf(request, "{\"request\": \"auth/realm/leave\", \"args\":{\"username\":\"%s\", \"local_ip\":%d,\"remote_ip\":%d, \"device\":\"%s\", \"realm\":\"xl2tpd\", \"realm-key\":\"%s\"}}\n", 
		state_username, ntohl(ipcp_gotoptions[0].ouraddr), ntohl(ipcp_hisoptions[0].hisaddr), ifname, option_realm_key);

        make_sw_request(buffer, request);
        free(buffer);
        free(request);
        return 0;
}

int sw__pap_auth_hook(char *user, char *passwd, char **msgp, struct wordlist **paddrs, struct wordlist **popts){
	if(make_auth_request(user, passwd)){
		state_username = strdup(user);
		
		/* I sure hope we are not in charge of denying access here */
		struct wordlist* wlist = (struct wordlist*) malloc(sizeof(struct wordlist));
		wlist->word = calloc(sizeof(char), 2);
		wlist->word[0] = '*';
		wlist->word[1] = '\0';
		wlist->next = NULL;
		*paddrs = wlist;
	
		return 1;
	}

	return 0;
}

static void sw__leave(void *arg, int val)
{
	make_leave_notification();
}

static void sw__join(void *arg, int val)
{
	make_join_notication();	
}


void plugin_init(void)
{
#if defined(__linux__)
	extern int new_style_driver;	/* From sys-linux.c */
	if (!ppp_available() && !new_style_driver)
		fatal("Kernel doesn't support ppp_generic - needed for sphirewall_ppp");
#else
	fatal("No sphirewall_ppp support on this OS");
#endif

	add_options(options);

	/* Pap checks */
	pap_check_hook = sw__pap_check_hook;
	pap_passwd_hook = sw__pap_passwd_hook;
	pap_auth_hook = sw__pap_auth_hook;

	add_notifier(&exitnotify, sw__leave, 0);
	add_notifier(&ip_up_notifier, sw__join, 0);
}

