# Copyright Michael Lawson
# This file is part of Sphirewall.
#
# Sphirewall is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Sphirewall is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.

class FirewallSettings:
    connection = None

    def __init__(self, connection):
        self.connection = connection

    def connections_list(self, mac=None, ip=None, user=None):
        args = {}
        if mac: args["mac"] = mac
        if ip: args["ip"] = ip
        if user: args["user"] = user
        return self.connection.request("firewall/tracker/list", args)["connections"]

    def connections_stats(self):
        return self.connection.request("firewall/tracker/details", None)

    def connection_details(self, connection_id):
        return self.connection.request("firewall/tracker/connection/details", {"id": connection_id})

    def connections_size(self):
        return self.connection.request("firewall/tracker/size", None)["size"]

    def aliases(self, id=None, name=None):
        if id or name:
            for alias in self.aliases():
                if alias["id"] == id:
                    return alias
                elif alias["name"] == name:
                    return alias
            return None

        return self.connection.request("firewall/aliases/list", None)["aliases"]

    def aliases_create(self, name, type, source, detail):
        args = {"name": name, "type": int(type), "source": int(source), "detail": detail}

        self.connection.request("firewall/aliases/add", args)

    def aliases_truncate(self, id):
        args = {"id": id}
        self.connection.request("firewall/aliases/truncate", args)

    def aliases_bulk_create(self, aliases):
        self.connection.request("firewall/aliases/add/bulk", {"items": aliases})

    def aliases_delete(self, id):
        args = {"id": id}
        self.connection.request("firewall/aliases/del", args)

    def aliases_delete_all(self):
        self.connection.request("firewall/aliases/del/all", {})

    def aliases_sync(self, id):
        args = {"id": id}
        self.connection.request("firewall/aliases/load", args)

    def aliases_list(self, id):
        args = {"id": id}
        return self.connection.request("firewall/aliases/alias/list", args)["items"]

    def aliases_list_add(self, id, value):
        args = {"id": id, "value": value}
        self.connection.request("firewall/aliases/alias/add", args)

    def aliases_list_search(self, id, search_expression):
        args = {"id": id, "search": search_expression}
        return self.connection.request("firewall/aliases/alias/search", args)["matched"]

    def aliases_list_del(self, id, value):
        args = {"id": id, "value": value}
        self.connection.request("firewall/aliases/alias/del", args)

    def webfilter(self, id=None, name=None):
        if id is not None:
            all_rules = self.connection.request("firewall/webfilter/rules/list", None)["rules"]
            return next((rule for rule in all_rules if rule.get("id") == id), None)
        if name is not None:
            all_rules = self.connection.request("firewall/webfilter/rules/list", None)["rules"]
            return next((rule for rule in all_rules if rule.get("name") == name), None)
        return self.connection.request("firewall/webfilter/rules/list", None)["rules"]

    def webfilter_get(self, id):
        if id is not None:
            all_rules = self.connection.request("firewall/webfilter/rules/list", None)["rules"]
            return next((rule for rule in all_rules if rule.get("id") == id), None)

    def webfilter_add(self, id, name, criteria, source_criteria, action, fireEvent=False, redirect=False,
                      redirectUrl="", exclusive=False, metadata=None, temp_rule=False, expiry_timestamp=-1, top=False):
        args = {}
        if id is not None:
            args["id"] = id

        args["action"] = int(action)
        args["fireEvent"] = fireEvent
        args["redirect"] = redirect
        args["redirectUrl"] = redirectUrl
        args["criteria"] = criteria
        args["name"] = name

        args["temp_rule"] = temp_rule
        args["expiry_timestamp"] = expiry_timestamp

        args["exclusive"] = exclusive
        args["source_criteria"] = source_criteria
        args["top"] = top
        if metadata: args["metadata"] = metadata
        return self.connection.request("firewall/webfilter/rules/add", args)

    def webfilter_add_bulk(self, rules):
        self.connection.request("firewall/webfilter/rules/add/bulk", {"items": rules})

    def webfilter_delete(self, id):
        args = {"id": id}
        self.connection.request("firewall/webfilter/rules/remove", args)

    def webfilter_delete_all(self):
        self.connection.request("firewall/webfilter/rules/deleteall", None)

    def webfilter_moveup(self, id):
        args = {"id": id}
        self.connection.request("firewall/webfilter/rules/moveup", args)

    def webfilter_movedown(self, id):
        args = {"id": id}
        self.connection.request("firewall/webfilter/rules/movedown", args)

    def webfilter_enable(self, id):
        args = {"id": id}
        self.connection.request("firewall/webfilter/rules/enable", args)

    def webfilter_disable(self, id):
        args = {"id": id}
        self.connection.request("firewall/webfilter/rules/disable", args)

    def denylist(self):
        return self.connection.request("firewall/deny/list", None)["list"]

    def denylist_delete(self, host):
        args = {"ip": host}
        return self.connection.request("firewall/deny/del", args)

    def qos(self, id=None):
        all_rules = self.connection.request("firewall/acls/list/trafficshaper", None)["items"]
        for rule in all_rules:
            if rule["id"] == id:
                return rule
        return all_rules

    def qos_add(self, id=None, name="", source_criteria=[], destination_criteria=[], cumulative=True, upload=1024,
                download=1024):
        args = {
            "source_criteria": source_criteria,
            "destination_criteria": destination_criteria,
            "upload": int(upload),
            "download": int(download),
            "cumulative": cumulative,
            "name": name
        }

        if id: args["id"] = id
        self.connection.request("firewall/acls/add/trafficshaper", args)

    def qos_del(self, id):
        args = {"id": id}
        self.connection.request("firewall/acls/del/trafficshaper", args)

    def normal_add(self, id=None, source_criteria=[], destination_criteria=[], ignoreconntrack=False,
                   comment="", action="ACCEPT", log=False, nice=None, enabled=False):
        args = {}
        if id is not None:
            args["id"] = id
        if log is not None:
            args["log"] = log

        if action == "ACCEPT":
            args["action"] = 1
        if action == "DROP":
            args["action"] = 0
        if action == "PRIORITISE":
            args["action"] = 4
            args["nice"] = int(nice)
        args["source_criteria"] = source_criteria
        args["destination_criteria"] = destination_criteria
        args["comment"] = comment
        args["ignoreconntrack"] = ignoreconntrack
        args["enabled"] = enabled

        return self.connection.request("firewall/acls/add", args)

    def normal_add_bulk(self, rules):
        return self.connection.request("firewall/acls/add/bulk", {"rules": rules})


    def acls(self):
        return self.connection.request("firewall/acls/list", None)["normal"]

    def acls_groups(self):
        return self.connection.request("firewall/acls/groups/list", None)["firewall_groups"]

    def acls_groups_add(self, groups):
        return self.connection.request("firewall/acls/groups/add", {"groups": groups})

    def priority(self):
        return self.connection.request("firewall/acls/list", None)["priority"]

    def get(self, id):
        all_rules = self.acls()
        for rule in all_rules:
            if rule["id"] == id:
                return rule

        all_rules = self.nat()
        for rule in all_rules:
            if rule["id"] == id:
                return rule

        all_rules = self.priority()
        for rule in all_rules:
            if rule["id"] == id:
                return rule

        return None

    def nat(self):
        return self.connection.request("firewall/acls/list", None)["nat"]

    def normal_delete(self, id):
        args = {"id": id}
        self.connection.request("firewall/acls/filter/delete", args)

    def autowan(self):
        return self.connection.request("firewall/autowan", {})

    def signatures(self):
        return self.connection.request("firewall/signatures", {})["signatures"]

    def signatures_import(self, url=None):
        params = {}
        if url: params = {"url": url}
        return self.connection.request("firewall/signatures/import", params)["imported"]

    def autowan_set(self, mode, interface=None, devices=None):
        configuration = {"mode": mode}
        if devices:
            configuration["interfaces"] = devices
        if interface:
            configuration["interface"] = interface
        self.connection.request("firewall/autowan/set", configuration)

    def autowan_set_interface(self, interface, failover_index=0, criteria=[]):
        configuration = {
            "interface": interface,
            "failover_index": failover_index,
            "criteria": criteria,
        }
        self.connection.request("firewall/autowan/interface/set", configuration)

    def autowan_del_interface(self, interface):
        configuration = {
            "interface": interface
        }
        self.connection.request("firewall/autowan/interface/del", configuration)

    def normal_up(self, id):
        args = {"id": id}
        self.connection.request("firewall/acls/filter/up", args)

    def normal_down(self, id):
        args = {"id": id}
        self.connection.request("firewall/acls/filter/down", args)

    def normal_enable(self, id):
        args = {"id": id, "ruleType": 0}
        self.connection.request("firewall/acls/enable", args)

    def normal_disable(self, id):
        args = {"id": id, "ruleType": 0}
        self.connection.request("firewall/acls/disable", args)

    def connection_terminate(self, connection_id):
        self.connection.request("firewall/tracker/terminate", {"id": connection_id})

    def defaultruleset(self):
        current_acls = self.acls()
        if len(current_acls) == 2:
            if current_acls[0]["id"] == "default1" and current_acls[1]["id"] == "default2":
                return True
        return False

    def periods(self, id=None):
        if id:
            for period in self.periods():
                if period["id"] == id:
                    return period
            return None
        return self.connection.request("firewall/periods/list", {})["periods"]

    def periods_create(self, name):
        return self.connection.request("firewall/periods/add", {"name": name})["id"]

    def periods_modify(self, id, any=False, mon=False, tue=False, wed=False, thu=False, fri=False, sat=False, sun=False,
                       startTime=0, endTime=2400, startDate=-1, endDate=-1):
        self.connection.request("firewall/periods/add", dict(
            id=id,
            startTime=startTime,
            endTime=endTime,
            startDate=startDate,
            endDate=endDate,
            any=any,
            mon=mon,
            tue=tue,
            wed=wed,
            thu=thu,
            fri=fri,
            sat=sat,
            sun=sun
        ))

    def periods_del(self, id):
        self.connection.request("firewall/periods/delete", {"id": id})

    def balancer_set(self, enabled, devices):
        self.connection.request("firewall/balancer/set", {"enabled": enabled, "devices": devices})

    def balancer(self):
        return self.connection.request("firewall/balancer", {})

    def forwarding_rules(self, id=None):
        if id:
            all_rules = self.forwarding_rules()
            for rule in all_rules:
                if rule["id"] == id:
                    return rule
            return None
        return self.connection.request("firewall/acls/forwarding", {})["rules"]

    def forwarding_rules_add(self, source_criteria, destination_criteria, forwardingDestination,
                             forwardingDestinationPort, comment="", id=None, enabled=False):
        args = {
            "forwardingDestination": forwardingDestination,
            "forwardingDestinationPort": forwardingDestinationPort,
            "source_criteria": source_criteria,
            "destination_criteria": destination_criteria,
            "comment": comment,
            "enabled": enabled
        }

        if id: args["id"] = id

        return self.connection.request("firewall/acls/forwarding/add", args)

    def forwarding_rules_delete(self, id):
        self.connection.request("firewall/acls/forwarding/delete", {"id": id})

    def forwarding_rules_disable(self, id):
        self.connection.request("firewall/acls/forwarding/disable", {"id": id})

    def forwarding_rules_enable(self, id):
        self.connection.request("firewall/acls/forwarding/enable", {"id": id})

    def masquerading_rules(self, id=None):
        if id:
            all_rules = self.masquerading_rules()
            for rule in all_rules:
                if rule["id"] == id:
                    return rule
            return None
        return self.connection.request("firewall/acls/masquerading", {})["rules"]

    def masquerading_rules_add(self, source_criteria, destination_criteria, natTargetDevice=None, natTargetIp=None,
                               comment=None, id=None, enabled=False):
        args = {
            "source_criteria": source_criteria,
            "destination_criteria": destination_criteria,
            "enabled": enabled
        }

        if natTargetDevice:
            args["natTargetDevice"] = natTargetDevice
        if natTargetIp:
            args["natTargetIp"] = natTargetIp
        if comment:
            args["comment"] = comment
        if id: args["id"] = id

        return self.connection.request("firewall/acls/masquerading/add", args)

    def masquerading_rules_delete(self, id):
        self.connection.request("firewall/acls/masquerading/delete", {"id": id})

    def masquerading_rules_disable(self, id):
        self.connection.request("firewall/acls/masquerading/disable", {"id": id})

    def masquerading_rules_enable(self, id):
        self.connection.request("firewall/acls/masquerading/enable", {"id": id})

    def onetoonenat_rules(self, id=None):
        if id:
            all_rules = self.onetoonenat_rules()
            for rule in all_rules:
                if rule["id"] == id:
                    return rule
            return None
        return self.connection.request("firewall/acls/otonat", {})["rules"]

    def onetoonenat_rules_add(self, interface, internal_ip, external_ip, name, enabled=False, id=None):
        args = {
            "interface": interface,
            "internalIp": internal_ip,
            "externalIp": external_ip,
            "name": name,
            "enabled": enabled
        }

        if id: args["id"] = id
        return self.connection.request("firewall/acls/otonat/add", args)

    def onetoonenat_rules_delete(self, id):
        self.connection.request("firewall/acls/otonat/delete", {"id": id})

    def captiveportal(self):
        return self.connection.request("firewall/captiveportal", None)

    def captiveportal_set(self, mode, endpoint, url=None, inclusions=[], exclusions=[], object_inclusions=[],
                          object_exclusions=[], allow_local=True, capture_all_traffic=False,
                          disable_apple_captive_prompt=False):
        data = {
            "mode": mode,
            "endpoint": endpoint,
            "url": url,
            "inclusions": inclusions,
            "exclusions": exclusions,
            "object_inclusions": object_inclusions,
            "object_exclusions": object_exclusions,
            "allow_local": allow_local,
            "capture_all_traffic": capture_all_traffic,
            "disable_apple_captive_prompt": disable_apple_captive_prompt
        }

        self.connection.request("firewall/captiveportal/set", data)

    def securitygroups_add(self, name):
        self.connection.request("firewall/securitygroups/add", {"name": name})

    def securitygroups_del(self, id):
        self.connection.request("firewall/securitygroups/del", {"id": id})

    def securitygroups_list(self):
        return self.connection.request("firewall/securitygroups/list", {})["groups"]

    def securitygroups_set(self, id, entries):
        self.connection.request("firewall/securitygroups/set", {"id": id, "entries": entries})

    def securitygroups_get(self, id):
        for item in self.securitygroups_list():
            if item["id"] == id:
                return item

    def securitygroups_entries_add(self, groupid, name):
        self.connection.request("firewall/securitygroups/entries/add", {"groupid": groupid, "name": name})

    def securitygroups_entries_del(self, groupid, id):
        self.connection.request("firewall/securitygroups/entries/del", {"groupid": groupid, "id": id})

    def securitygroups_entries_update(self, groupid, entries):
        self.connection.request("firewall/securitygroups/entries/update", {"groupid": groupid, "entries": entries})

    def fingerprints_list(self):
        return self.connection.request("firewall/fingerprints/list", {})["fingerprints"]

    def securitygroups_entries_get(self, groupid, id):
        security_group = self.securitygroups_get(groupid)
        for entry in security_group["entries"]:
            if entry["id"] == id:
                return entry

    def securitygroups_entries_set(self, groupid, id, name, criteria):
        self.connection.request(
            "firewall/securitygroups/entries/set",
            {
                "groupid": groupid, "id": id, "name": name, "criteria": criteria
            }
        )
