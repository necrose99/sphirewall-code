# Copyright Michael Lawson
# This file is part of Sphirewall.
#
# Sphirewall is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Sphirewall is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.

from _socket import SOCK_STREAM, AF_INET, socket
import json


class SphirewallSocketTransportProvider:
    """ This is the standard socket transport provider for Sphirewall. It passes requests via a socket
        connection directly to Sphirewall. Exceptions thrown here by the socket libraries will
        be caught by the Sphirewall Api and rethrown as a TransportProviderException.

        Normally operates on port 8001
    """

    hostname = None
    port = None

    def __init__(self, hostname, port):
        self.hostname = hostname
        self.port = port

    def send(self, data):
        s = socket(AF_INET, SOCK_STREAM)
        s.connect((self.hostname, int(self.port)))
        s.sendall(json.dumps(data) + "\n")

        response_buffer = ""
        while 1:
            data = s.recv(4096)
            if not data:
                break

            response_buffer += data
        return response_buffer

