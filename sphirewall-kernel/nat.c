/*
Copyright Michael Lawson
This file is part of sphirewall-kernel for Sphirewall.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

*/

#include <asm/types.h>
#include <linux/module.h>
#include <linux/socket.h>
#include <linux/net.h>
#include <linux/skbuff.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/ip.h>
#include <linux/tcp.h>
#include <linux/icmp.h>
#include <linux/in.h>
#include <linux/netfilter_ipv4.h>
#include <linux/delay.h>
#include <linux/kthread.h>
#include <linux/netfilter.h>
#include <linux/kfifo.h>
#include <linux/msg.h>
#include <linux/ipc.h>
#include <linux/types.h>
#include <linux/in_route.h>
#include <linux/kfifo.h>
#include <linux/if_ether.h>
#include <net/ip.h>
#include <net/sock.h>
#include <net/netfilter/nf_queue.h>
#include <net/tcp.h>

#include "time.h"
#include "queue.h"
#include "sphirewall_queue.h"
#include "checksum.h"
#include "transport.h"
#include "nat.h"

#define NIPQUAD(addr) \
((unsigned char *)&addr)[0], \
((unsigned char *)&addr)[1], \
((unsigned char *)&addr)[2], \
((unsigned char *)&addr)[3]
#define NIPQUAD_FMT "%u.%u.%u.%u"


static int UDP_MAX_CONNECTION_LIFE = 120;
static int ICMP_MAX_CONNECTION_LIFE = 120;
static int NAT_GC_TIMER = 300;
static int TCP_CONNECTION_TIMEOUT_THESHOLD = 7200;
static int TCP_CLOSING_TIMEOUT = 120;

module_param(UDP_MAX_CONNECTION_LIFE, int, 0644);
module_param(ICMP_MAX_CONNECTION_LIFE, int, 0644);
module_param(NAT_GC_TIMER, int, 0644);
module_param(TCP_CONNECTION_TIMEOUT_THESHOLD, int, 0644);
module_param(TCP_CLOSING_TIMEOUT, int, 0644);

void free_snat_port(unsigned int destination_ip_address, unsigned short active_port);
int nat_conntracker_hasexpired(struct natct* ct);
void nat_conntracker_updateconnectionstate(struct natct* ct, struct nf_queue_entry* entry);
unsigned short aquire_snat_port(unsigned int destination_ip_address, unsigned short active_port);
bool nat_tcp_matches(struct nf_queue_entry* entry, unsigned int source_adddress, unsigned int destination_address, int source_port, int destination_port);
bool nat_icmp_matches(struct nf_queue_entry* entry, unsigned int source_adddress, unsigned int destination_address, unsigned int id);
bool nat_gre_matches(struct nf_queue_entry* entry, unsigned int source_adddress, unsigned int destination_address, unsigned int id);

void init_snat_port_pool(void);
void nat_conntracker__gc(struct nat_connection_tracker* tracker);
bool nat_conntracker__should_run_gc(struct nat_connection_tracker* _self);

bool nat_conntracker__gre_expired(struct nat_connection_tracker_protocol_handler* _self, struct natct* target);
bool nat_conntracker__icmp_expired(struct nat_connection_tracker_protocol_handler* _self, struct natct* target);
bool nat_conntracker__udp_expired(struct nat_connection_tracker_protocol_handler* _self, struct natct* target);
bool nat_conntracker__tcp_expired(struct nat_connection_tracker_protocol_handler* _self, struct natct* target);

void init_nat_connection_tracker_store(struct nat_connection_tracker* target){
	int x = 0;
	for(; x < NAT_CONNTRACKER_MAXCONNS; x++){
		INIT_LIST_HEAD(&target->store[x]);			
	}
}

struct natct* nat_connection_tracker__check(struct nat_connection_tracker* _self, struct nf_queue_entry* entry, int direction){
	/*Would this be a good time to run the garbage collection?*/
	struct iphdr* ip = NULL;
	struct nat_connection_tracker_protocol_handler* protocol_handler = NULL;
	int hash = 0, timestamp = 0;
	struct natct *cursor = NULL;
	struct list_head *pos, *q;

	if(_self->should_run_gc(_self)){
		_self->run_gc(_self);
	}		

	ip = ip_hdr(entry->skb);
	if(ip->protocol > MAX_PROTOCOL_HANDLERS){
		printk("ip protocol outside accepted range, %d", ip->protocol);
		return NULL;
	}

	protocol_handler = _self->protocol_handlers[ip->protocol];
	if(!protocol_handler){
		return NULL;
	}

	hash = protocol_handler->hash(entry, direction);
	if(hash == -1){	
		return NULL;
	}

	timestamp = currenttimestamp();
	list_for_each_safe(pos, q, &_self->store[hash]) {
		cursor = list_entry(pos, struct natct, list);

		if(protocol_handler->match(cursor, entry, direction)){
			cursor->timestamp = timestamp;
			return cursor;
		}

		if(protocol_handler->expired(protocol_handler, cursor)){
			list_del(pos);
			_self->free_state(_self, cursor);
		}
	}

	return NULL;

}

void nat_conntracker_free_state(struct nat_connection_tracker* tracker, struct natct* state){
	kfree(state);
}

void nat_conntracker_free_snat_state(struct nat_connection_tracker* tracker, struct natct* state){
	kfree(state);
	free_snat_port(state->destination_address, ntohs(state->target_port));
}

void nat_connection_tracker__translate(struct nat_connection_tracker* _self, struct natct* ct, struct nf_queue_entry* entry, int direction){
	struct iphdr* ip = ip_hdr(entry->skb);
	struct nat_connection_tracker_protocol_handler* protocol_handler = _self->protocol_handlers[ip->protocol];
	if(!protocol_handler){
		return;
	}

	protocol_handler->translate(ct, entry, direction);
	nat_conntracker_updateconnectionstate(ct, entry);
}

struct natct* nat_connection_tracker__create(struct nat_connection_tracker* _self, struct nf_queue_entry* entry, struct message_verdict* m){
	struct natct* ct = NULL;
	struct iphdr* ip = ip_hdr(entry->skb);
	struct nat_connection_tracker_protocol_handler* protocol_handler = _self->protocol_handlers[ip->protocol];
	if(!protocol_handler){
		return NULL;
	}

	ct = protocol_handler->create(protocol_handler, entry, m);
	ct->protocol_handler = protocol_handler;
	ct->hash = protocol_handler->hash(entry, SAME);
	list_add_tail(&ct->list, &_self->store[ct->hash]);
	return ct;
}

int nat_connection_tracker__snat__tcp_hash(struct nf_queue_entry* entry, int direction){
	struct iphdr* ip = ip_hdr(entry->skb);
	struct tcphdr* tcp = tcphdr_ptr(entry->skb);

	if(direction == SAME){
		return (ip->daddr + tcp->dest) % NAT_CONNTRACKER_MAXCONNS;
	}else if(direction == DIFF){
		return (ip->saddr + tcp->source) % NAT_CONNTRACKER_MAXCONNS;
	}

	return -1;
}

void nat_connection_tracker__snat__tcp_translate(struct natct* ct, struct nf_queue_entry* entry, int direction){
	struct tcphdr* tcp = NULL;
	struct iphdr* ip = ip_hdr(entry->skb);
	if(direction == SAME){
		tcp = (struct tcphdr *) ((unsigned char*) ip + (ip->ihl << 2));
		ip->saddr = ct->target_address;
		tcp->source = ct->target_port;

		nat_tcp_updatechecksum(entry->skb, ip, tcp);
		return;
	}else if(direction == DIFF){
		tcp = (struct tcphdr *) ((unsigned char*) ip + (ip->ihl << 2));
		ip->daddr = ct->source_address;
		tcp->dest = ct->source_port;
		nat_tcp_updatechecksum(entry->skb, ip, tcp);
		return;
	}
}

struct natct* nat_connection_tracker__snat__tcp_create(struct nat_connection_tracker_protocol_handler* _self, struct nf_queue_entry* entry, struct message_verdict* m){
	struct natct* ct = (struct natct*) kmalloc(sizeof(struct natct),  GFP_ATOMIC);
	ct->type = NAT_SNAT;
	ct->target_address = htonl(m->nat.address);
	ct->timestamp = currenttimestamp();
	ct->routemark = m->nat.field;
	ct->fin= 0;

	ct->source_address = ip_hdr(entry->skb)->saddr;
	ct->destination_address = ip_hdr(entry->skb)->daddr;

	ct->source_port = tcphdr_ptr(entry->skb)->source;
	ct->destination_port = tcphdr_ptr(entry->skb)->dest;
	ct->protocol = IPPROTO_TCP;
	ct->state = TCPUP;
	ct->target_port = htons(aquire_snat_port(ct->destination_address, ntohs(tcphdr_ptr(entry->skb)->source)));
	ct->hash = _self->hash(entry, SAME);
	return ct;
}

bool nat_connection_tracker__snat__tcp_match(struct natct* target, struct nf_queue_entry* entry, int direction){
	if(direction == SAME){
		return nat_tcp_matches(entry, target->source_address, target->destination_address, target->source_port, target->destination_port);
	}else if(direction == DIFF){
		return nat_tcp_matches(entry, target->destination_address, target->target_address, target->destination_port, target->target_port);
	}

	return false;
}

int nat_connection_tracker__snat__udp_hash(struct nf_queue_entry* entry, int direction){
	struct iphdr* ip = ip_hdr(entry->skb);
	struct udphdr* tcp = udphdr_ptr(entry->skb);

	if(direction == SAME){
		return (ip->daddr + tcp->dest) % NAT_CONNTRACKER_MAXCONNS;
	}else if(direction == DIFF){
		return (ip->saddr + tcp->source) % NAT_CONNTRACKER_MAXCONNS;
	}

	return -1;
}

void nat_connection_tracker__snat__udp_translate(struct natct* ct, struct nf_queue_entry* entry, int direction){
	struct iphdr* ip = ip_hdr(entry->skb);
	struct udphdr* udp = (struct udphdr*) ((unsigned char*) ip + (ip->ihl << 2));

	if(direction == SAME){
		ip->saddr = ct->target_address;
		udp->source = ct->target_port;
		nat_udp_updatechecksum(ip, udp);

	}else if(direction == DIFF){
		ip->daddr = ct->source_address;
		udp->dest = ct->source_port;
		nat_udp_updatechecksum(ip, udp);
	}
}

struct natct* nat_connection_tracker__snat__udp_create(struct nat_connection_tracker_protocol_handler* _self, struct nf_queue_entry* entry, struct message_verdict* m){
	struct natct* ct = (struct natct*) kmalloc(sizeof(struct natct),  GFP_ATOMIC);
	ct->type = NAT_SNAT;
	ct->target_address = htonl(m->nat.address);
	ct->timestamp = currenttimestamp();
	ct->routemark = m->nat.field;
	ct->fin= 0;

	ct->source_address = ip_hdr(entry->skb)->saddr;
	ct->destination_address = ip_hdr(entry->skb)->daddr;

	ct->target_port = htons(aquire_snat_port(ct->destination_address, ntohs(udphdr_ptr(entry->skb)->source)));
	ct->source_port = udphdr_ptr(entry->skb)->source;
	ct->destination_port = udphdr_ptr(entry->skb)->dest;
	ct->protocol = IPPROTO_UDP;

	ct->hash = _self->hash(entry, SAME);
	return ct;
}

bool nat_connection_tracker__snat__udp_match(struct natct* target, struct nf_queue_entry* entry, int direction){
	if(direction == SAME){
		return nat_tcp_matches(entry, target->source_address, target->destination_address, target->source_port, target->destination_port);
	}else if(direction == DIFF){
		return nat_tcp_matches(entry, target->destination_address, target->target_address, target->destination_port, target->target_port);
	}               

	return false;
}

int nat_connection_tracker__snat__icmp_hash(struct nf_queue_entry* entry, int direction){
	struct iphdr* ip = ip_hdr(entry->skb);
	if(direction == SAME){
		return ip->daddr % NAT_CONNTRACKER_MAXCONNS;
	}else if(direction == DIFF){
		return ip->saddr % NAT_CONNTRACKER_MAXCONNS;
	}

	return -1;
}

void nat_connection_tracker__snat__icmp_translate(struct natct* ct, struct nf_queue_entry* entry, int direction){
	struct iphdr* ip = ip_hdr(entry->skb);
	struct icmphdr* icmp = (struct icmphdr*) ((unsigned char*) ip + (ip->ihl << 2));
	if(direction == SAME){
		ip->saddr = ct->target_address;
		nat_icmp_updatechecksum(ip, icmp);
		return;
	}else if(direction == DIFF){
		ip->daddr = ct->source_address;
		nat_icmp_updatechecksum(ip, icmp);
		return;
	}
}

struct natct* nat_connection_tracker__snat__icmp_create(struct nat_connection_tracker_protocol_handler* _self, struct nf_queue_entry* entry, struct message_verdict* m){
	struct natct* ct = (struct natct*) kmalloc(sizeof(struct natct),  GFP_ATOMIC);
	ct->type = NAT_SNAT;
	ct->target_address = htonl(m->nat.address);
	ct->target_port = htons(m->nat.field);
	ct->timestamp = currenttimestamp();
	ct->routemark = m->nat.field;
	ct->fin= 0;

	ct->source_address = ip_hdr(entry->skb)->saddr;
	ct->destination_address = ip_hdr(entry->skb)->daddr;

	ct->icmp_id = icmphdr_ptr(entry->skb)->un.echo.id;
	ct->protocol = IPPROTO_ICMP;
	ct->hash = _self->hash(entry, SAME);
	return ct;
}

bool nat_connection_tracker__snat__icmp_match(struct natct* target, struct nf_queue_entry* entry, int direction){
	if(direction == SAME){
		return nat_icmp_matches(entry, target->source_address, target->destination_address, target->icmp_id);
	}else if(direction == DIFF){
		return nat_icmp_matches(entry, target->destination_address, target->target_address, target->icmp_id);
	}
	return false;
}

//Shit
int nat_connection_tracker__snat__gre_hash(struct nf_queue_entry* entry, int direction){
        struct iphdr* ip = ip_hdr(entry->skb);
        if(direction == SAME){
                return ip->daddr % NAT_CONNTRACKER_MAXCONNS;
        }else if(direction == DIFF){
                return ip->saddr % NAT_CONNTRACKER_MAXCONNS;
        }

        return -1;
}

void nat_connection_tracker__snat__gre_translate(struct natct* ct, struct nf_queue_entry* entry, int direction){
        struct iphdr* ip = ip_hdr(entry->skb);
        struct grehdr* gre = (struct grehdr*) ((unsigned char*) ip + (ip->ihl << 2));
        if(direction == SAME){
                ip->saddr = ct->target_address;
                nat_gre_updatechecksum(ip, gre);
                return;
        }else if(direction == DIFF){
                ip->daddr = ct->source_address;
                nat_gre_updatechecksum(ip, gre);
                return;
        }
}

struct natct* nat_connection_tracker__snat__gre_create(struct nat_connection_tracker_protocol_handler* _self, struct nf_queue_entry* entry, struct message_verdict* m){
        struct natct* ct = (struct natct*) kmalloc(sizeof(struct natct),  GFP_ATOMIC);
        ct->type = NAT_SNAT;
        ct->target_address = htonl(m->nat.address);
        ct->target_port = htons(m->nat.field);
        ct->timestamp = currenttimestamp();
        ct->routemark = m->nat.field;
        ct->fin= 0;

        ct->source_address = ip_hdr(entry->skb)->saddr;
        ct->destination_address = ip_hdr(entry->skb)->daddr;

        ct->icmp_id = grehdr_ptr(entry->skb)->call_id;
        ct->protocol = IPPROTO_GRE;
        ct->hash = _self->hash(entry, SAME);
        return ct;
}

bool nat_connection_tracker__snat__gre_match(struct natct* target, struct nf_queue_entry* entry, int direction){
        if(direction == SAME){
                return nat_gre_matches(entry, target->source_address, target->destination_address, target->icmp_id);
        }else if(direction == DIFF){
                return nat_gre_matches(entry, target->destination_address, target->target_address, target->icmp_id);
        }
        return false;
}


/* DNAT Specific Methods */
int nat_connection_tracker__dnat__tcp_hash(struct nf_queue_entry* entry, int direction){
	struct iphdr* ip = ip_hdr(entry->skb);
	struct tcphdr* tcp = tcphdr_ptr(entry->skb);

	if(direction == SAME){
		return (ip->saddr + tcp->source) % NAT_CONNTRACKER_MAXCONNS;
	}else if(direction == DIFF){
		return (ip->daddr + tcp->dest) % NAT_CONNTRACKER_MAXCONNS;
	}
	return -1;
}

void nat_connection_tracker__dnat__tcp_translate(struct natct* ct, struct nf_queue_entry* entry, int direction){
	struct iphdr* ip = ip_hdr(entry->skb);
	struct tcphdr* tcp = (struct tcphdr *) ((unsigned char*) ip + (ip->ihl << 2));
	if(direction == SAME){
		ip->daddr = ct->target_address;
		tcp->dest= ct->target_port;
		nat_tcp_updatechecksum(entry->skb, ip, tcp);
		return;
	}else if (direction == DIFF){
		ip->saddr = ct->destination_address;
		tcp->source= ct->destination_port;
		nat_tcp_updatechecksum(entry->skb, ip, tcp);
		return;
	}
}

struct natct* nat_connection_tracker__dnat__tcp_create(struct nat_connection_tracker_protocol_handler* _self, struct nf_queue_entry* entry, struct message_verdict* m){
	struct natct* ct = (struct natct*) kmalloc(sizeof(struct natct),  GFP_ATOMIC);
	ct->type = NAT_DNAT;
	ct->target_address = htonl(m->nat.address);
	ct->target_port = htons(m->nat.field);
	ct->timestamp = currenttimestamp();
	ct->routemark = m->nat.field;
	ct->fin= 0;

	ct->source_address = ip_hdr(entry->skb)->saddr;
	ct->destination_address = ip_hdr(entry->skb)->daddr;

	ct->source_port = tcphdr_ptr(entry->skb)->source;
	ct->destination_port = tcphdr_ptr(entry->skb)->dest;
	ct->protocol = IPPROTO_TCP;
	ct->state = TCPUP;

	ct->hash = _self->hash(entry, SAME);
	return ct;
}

bool nat_connection_tracker__dnat__tcp_match(struct natct* target, struct nf_queue_entry* entry, int direction){
	if(direction == SAME){
		return nat_tcp_matches(entry, target->source_address, target->destination_address, target->source_port, target->destination_port);
	}else if(direction == DIFF){
		return nat_tcp_matches(entry, target->target_address, target->source_address, target->target_port, target->source_port);
	}
	return false;
}

int nat_connection_tracker__dnat__udp_hash(struct nf_queue_entry* entry, int direction){
	struct iphdr* ip = ip_hdr(entry->skb);
	struct udphdr* tcp = udphdr_ptr(entry->skb);

	if(direction == SAME){
		return (ip->saddr + tcp->source) % NAT_CONNTRACKER_MAXCONNS;
	}else if(direction == DIFF){
		return (ip->daddr + tcp->dest) % NAT_CONNTRACKER_MAXCONNS;
	}
	return -1;
}

void nat_connection_tracker__dnat__udp_translate(struct natct* ct, struct nf_queue_entry* entry, int direction){
	struct iphdr* ip = ip_hdr(entry->skb);
	struct udphdr* udp = (struct udphdr*) ((unsigned char*) ip + (ip->ihl << 2));
	if(direction == SAME){
		ip->daddr = ct->target_address;
		udp->dest = ct->target_port;

		nat_udp_updatechecksum(ip, udp);

	}else if(direction == DIFF){
		ip->saddr = ct->destination_address;
		udp->source = ct->destination_port;

		nat_udp_updatechecksum(ip, udp);
	}
}

struct natct* nat_connection_tracker__dnat__udp_create(struct nat_connection_tracker_protocol_handler* _self, struct nf_queue_entry* entry, struct message_verdict* m){
	struct natct* ct = (struct natct*) kmalloc(sizeof(struct natct),  GFP_ATOMIC);
	ct->type = NAT_DNAT;
	ct->target_address = htonl(m->nat.address);
	ct->target_port = htons(m->nat.field);
	ct->timestamp = currenttimestamp();
	ct->routemark = m->nat.field;
	ct->fin= 0;

	ct->source_address = ip_hdr(entry->skb)->saddr;
	ct->destination_address = ip_hdr(entry->skb)->daddr;

	ct->source_port = udphdr_ptr(entry->skb)->source;
	ct->destination_port = udphdr_ptr(entry->skb)->dest;
	ct->protocol = IPPROTO_UDP;
	ct->hash = _self->hash(entry, SAME);
	return ct;
}

bool nat_connection_tracker__dnat__udp_match(struct natct* target, struct nf_queue_entry* entry, int direction){
	if(direction == SAME){
		return nat_tcp_matches(entry, target->source_address, target->destination_address, target->source_port, target->destination_port);
	}else if(direction == DIFF){
		return nat_tcp_matches(entry, target->target_address, target->source_address, target->target_port, target->source_port);
	}
	return false;
}

int nat_connection_tracker__dnat__gre_hash(struct nf_queue_entry* entry, int direction){
        struct iphdr* ip = ip_hdr(entry->skb);

        if(direction == SAME){
                return ip->saddr % NAT_CONNTRACKER_MAXCONNS;
        }else if(direction == DIFF){
                return ip->daddr % NAT_CONNTRACKER_MAXCONNS;
        }
        return -1;
}

void nat_connection_tracker__dnat__gre_translate(struct natct* ct, struct nf_queue_entry* entry, int direction){
        struct iphdr* ip = ip_hdr(entry->skb);
        struct grehdr* gre= (struct grehdr*) ((unsigned char*) ip + (ip->ihl << 2));
        if(direction == SAME){
                ip->daddr = ct->target_address;
                nat_gre_updatechecksum(ip, gre);
        }else if(direction == DIFF){
                ip->saddr = ct->destination_address;
                nat_gre_updatechecksum(ip, gre);
        }
}

struct natct* nat_connection_tracker__dnat__gre_create(struct nat_connection_tracker_protocol_handler* _self, struct nf_queue_entry* entry, struct message_verdict* m){
        struct natct* ct = (struct natct*) kmalloc(sizeof(struct natct),  GFP_ATOMIC);
        ct->type = NAT_DNAT;
        ct->target_address = htonl(m->nat.address);
        ct->target_port = htons(m->nat.field);
        ct->timestamp = currenttimestamp();
        ct->routemark = m->nat.field;
        ct->fin= 0;

        ct->source_address = ip_hdr(entry->skb)->saddr;
        ct->destination_address = ip_hdr(entry->skb)->daddr;

        ct->icmp_id = grehdr_ptr(entry->skb)->call_id;
        ct->protocol = IPPROTO_GRE;
        ct->hash = _self->hash(entry, SAME);
        return ct;
}

bool nat_connection_tracker__dnat__gre_match(struct natct* target, struct nf_queue_entry* entry, int direction){
        if(direction == SAME){
                return nat_gre_matches(entry, target->source_address, target->destination_address, target->icmp_id);
        }else if(direction == DIFF){
                return nat_gre_matches(entry, target->target_address, target->source_address, target->icmp_id);
        }
        return false;
}


int nat_connection_tracker__dnat__icmp_hash(struct nf_queue_entry* entry, int direction){
	struct iphdr* ip = ip_hdr(entry->skb);

	if(direction == SAME){
		return ip->saddr % NAT_CONNTRACKER_MAXCONNS;
	}else if(direction == DIFF){
		return ip->daddr % NAT_CONNTRACKER_MAXCONNS;
	}
	return -1;
}

void nat_connection_tracker__dnat__icmp_translate(struct natct* ct, struct nf_queue_entry* entry, int direction){
	struct iphdr* ip = ip_hdr(entry->skb);
	struct icmphdr* icmp = (struct icmphdr*) ((unsigned char*) ip + (ip->ihl << 2));
	if(direction == SAME){
		ip->daddr = ct->target_address;
		nat_icmp_updatechecksum(ip, icmp);
	}else if(direction == DIFF){
		ip->saddr = ct->destination_address;
		nat_icmp_updatechecksum(ip, icmp);
	}
}

struct natct* nat_connection_tracker__dnat__icmp_create(struct nat_connection_tracker_protocol_handler* _self, struct nf_queue_entry* entry, struct message_verdict* m){
	struct natct* ct = (struct natct*) kmalloc(sizeof(struct natct),  GFP_ATOMIC);
	ct->type = NAT_DNAT;
	ct->target_address = htonl(m->nat.address);
	ct->target_port = htons(m->nat.field);
	ct->timestamp = currenttimestamp();
	ct->routemark = m->nat.field;
	ct->fin= 0;

	ct->source_address = ip_hdr(entry->skb)->saddr;
	ct->destination_address = ip_hdr(entry->skb)->daddr;

	ct->icmp_id = icmphdr_ptr(entry->skb)->un.echo.id;
	ct->protocol = IPPROTO_ICMP;
	ct->hash = _self->hash(entry, SAME);
	return ct;
}

bool nat_connection_tracker__dnat__icmp_match(struct natct* target, struct nf_queue_entry* entry, int direction){
	if(direction == SAME){
		return nat_icmp_matches(entry, target->source_address, target->destination_address, target->icmp_id);
	}else if(direction == DIFF){
		return nat_icmp_matches(entry, target->target_address, target->source_address, target->icmp_id);
	}
	return false;
}

/* ROUTE Specific Methods */
int nat_connection_tracker__route__tcp_hash(struct nf_queue_entry* entry, int direction){
	struct iphdr* ip = ip_hdr(entry->skb);
	struct tcphdr* tcp = tcphdr_ptr(entry->skb);

	if(direction == SAME){
		return (ip->saddr + tcp->source) % NAT_CONNTRACKER_MAXCONNS;
	}
	return -1;
}

void nat_connection_tracker__route__translate(struct natct* ct, struct nf_queue_entry* entry, int direction){
	if(direction == SAME){
		entry->skb->mark = ct->routemark;
	}
	nat_conntracker_updateconnectionstate(ct, entry);
}

struct natct* nat_connection_tracker__route__tcp_create(struct nat_connection_tracker_protocol_handler* _self, struct nf_queue_entry* entry, struct message_verdict* m){
	struct natct* ct = (struct natct*) kmalloc(sizeof(struct natct),  GFP_ATOMIC);
	ct->type = NAT_ROUTE;
	ct->target_address = htonl(m->nat.address);
	ct->target_port = htons(m->nat.field);
	ct->timestamp = currenttimestamp();
	ct->routemark = m->nat.field;
	ct->fin= 0;

	ct->source_address = ip_hdr(entry->skb)->saddr;
	ct->destination_address = ip_hdr(entry->skb)->daddr;

	ct->source_port = tcphdr_ptr(entry->skb)->source;
	ct->destination_port = tcphdr_ptr(entry->skb)->dest;
	ct->protocol = IPPROTO_TCP;
	ct->state = TCPUP;

	ct->hash = _self->hash(entry, SAME);
	return ct;
}

bool nat_connection_tracker__route__tcp_match(struct natct* target, struct nf_queue_entry* entry, int direction){
	if(direction == SAME){
		return nat_tcp_matches(entry, target->source_address, target->destination_address, target->source_port, target->destination_port);
	}
	return false;
}

int nat_connection_tracker__route__udp_hash(struct nf_queue_entry* entry, int direction){
	struct iphdr* ip = ip_hdr(entry->skb);
	struct udphdr* tcp = udphdr_ptr(entry->skb);
	if(direction == SAME){
		return (ip->saddr + tcp->source) % NAT_CONNTRACKER_MAXCONNS;
	}
	return -1;
}

struct natct* nat_connection_tracker__route__udp_create(struct nat_connection_tracker_protocol_handler* _self, struct nf_queue_entry* entry, struct message_verdict* m){
	struct natct* ct = (struct natct*) kmalloc(sizeof(struct natct),  GFP_ATOMIC);
	ct->type = NAT_ROUTE;
	ct->target_address = htonl(m->nat.address);
	ct->target_port = htons(m->nat.field);
	ct->timestamp = currenttimestamp();
	ct->routemark = m->nat.field;
	ct->fin= 0;

	ct->source_address = ip_hdr(entry->skb)->saddr;
	ct->destination_address = ip_hdr(entry->skb)->daddr;
	ct->source_port = udphdr_ptr(entry->skb)->source;
	ct->destination_port = udphdr_ptr(entry->skb)->dest;
	ct->protocol = IPPROTO_UDP;

	ct->hash = _self->hash(entry, SAME);
	return ct;
}

bool nat_connection_tracker__route__udp_match(struct natct* target, struct nf_queue_entry* entry, int direction){
	if(direction == SAME){
		return nat_tcp_matches(entry, target->source_address, target->destination_address, target->source_port, target->destination_port);
	}               
	return false;   
}

int nat_connection_tracker__route__icmp_hash(struct nf_queue_entry* entry, int direction){
	struct iphdr* ip = ip_hdr(entry->skb);
	if(direction == SAME){
		return ip->daddr % NAT_CONNTRACKER_MAXCONNS;
	}
	return -1;
}

struct natct* nat_connection_tracker__route__icmp_create(struct nat_connection_tracker_protocol_handler* _self, struct nf_queue_entry* entry, struct message_verdict* m){
	struct natct* ct = (struct natct*) kmalloc(sizeof(struct natct),  GFP_ATOMIC);
	ct->type = NAT_ROUTE;
	ct->target_address = htonl(m->nat.address);
	ct->target_port = htons(m->nat.field);
	ct->timestamp = currenttimestamp();
	ct->routemark = m->nat.field;
	ct->fin= 0;

	ct->source_address = ip_hdr(entry->skb)->saddr;
	ct->destination_address = ip_hdr(entry->skb)->daddr;
	ct->icmp_id = icmphdr_ptr(entry->skb)->un.echo.id;
	ct->protocol = IPPROTO_ICMP;
	ct->hash = _self->hash(entry, SAME);
	return ct;
}

bool nat_connection_tracker__route__icmp_match(struct natct* target, struct nf_queue_entry* entry, int direction){
	if(direction == SAME){
		return nat_icmp_matches(entry, target->source_address, target->destination_address, target->icmp_id);
	}
	return false;
}

void lock(struct nat_engine* _self){
	spin_lock_bh(&_self->lock_object);
}

void unlock(struct nat_engine* _self){
	spin_unlock_bh(&_self->lock_object);	
}

void init_protocol_handlers(struct nat_connection_tracker* tracker){
	int i = 0;
	for(; i < MAX_PROTOCOL_HANDLERS; i++){
		tracker->protocol_handlers[i] = NULL;
	} 
}

struct nat_engine* init_nat_engine(){
	struct nat_connection_tracker* snat_tracker = (struct nat_connection_tracker*) kmalloc(sizeof(struct nat_connection_tracker), GFP_ATOMIC);
	struct nat_connection_tracker_protocol_handler* tcp_snat_handler = (struct nat_connection_tracker_protocol_handler*) kmalloc(sizeof(struct nat_connection_tracker_protocol_handler), GFP_ATOMIC);
	struct nat_connection_tracker_protocol_handler* udp_snat_handler = (struct nat_connection_tracker_protocol_handler*) kmalloc(sizeof(struct nat_connection_tracker_protocol_handler), GFP_ATOMIC);
	struct nat_connection_tracker_protocol_handler* icmp_snat_handler = (struct nat_connection_tracker_protocol_handler*) kmalloc(sizeof(struct nat_connection_tracker_protocol_handler), GFP_ATOMIC);
	struct nat_connection_tracker_protocol_handler* gre_snat_handler = (struct nat_connection_tracker_protocol_handler*) kmalloc(sizeof(struct nat_connection_tracker_protocol_handler), GFP_ATOMIC);

	struct nat_connection_tracker* dnat_tracker = (struct nat_connection_tracker*) kmalloc(sizeof(struct nat_connection_tracker), GFP_ATOMIC);
	struct nat_connection_tracker_protocol_handler* tcp_dnat_handler = (struct nat_connection_tracker_protocol_handler*) kmalloc(sizeof(struct nat_connection_tracker_protocol_handler), GFP_ATOMIC);
	struct nat_connection_tracker_protocol_handler* udp_dnat_handler = (struct nat_connection_tracker_protocol_handler*) kmalloc(sizeof(struct nat_connection_tracker_protocol_handler), GFP_ATOMIC);
	struct nat_connection_tracker_protocol_handler* icmp_dnat_handler = (struct nat_connection_tracker_protocol_handler*) kmalloc(sizeof(struct nat_connection_tracker_protocol_handler), GFP_ATOMIC);
	struct nat_connection_tracker_protocol_handler* gre_dnat_handler = (struct nat_connection_tracker_protocol_handler*) kmalloc(sizeof(struct nat_connection_tracker_protocol_handler), GFP_ATOMIC);

	struct nat_connection_tracker* route_tracker = (struct nat_connection_tracker*) kmalloc(sizeof(struct nat_connection_tracker), GFP_ATOMIC);
	struct nat_connection_tracker_protocol_handler* tcp_route_handler = (struct nat_connection_tracker_protocol_handler*) kmalloc(sizeof(struct nat_connection_tracker_protocol_handler), GFP_ATOMIC);
	struct nat_connection_tracker_protocol_handler* udp_route_handler = (struct nat_connection_tracker_protocol_handler*) kmalloc(sizeof(struct nat_connection_tracker_protocol_handler), GFP_ATOMIC);
	struct nat_connection_tracker_protocol_handler* icmp_route_handler = (struct nat_connection_tracker_protocol_handler*) kmalloc(sizeof(struct nat_connection_tracker_protocol_handler), GFP_ATOMIC);

	struct nat_engine* engine = (struct nat_engine*) kmalloc(sizeof(struct nat_engine), GFP_ATOMIC);
	engine->lock = &lock;
	engine->unlock = &unlock;
	spin_lock_init(&engine->lock_object);

	/* SNAT */
	snat_tracker->check = &nat_connection_tracker__check;
	snat_tracker->translate = &nat_connection_tracker__translate;
	snat_tracker->create = &nat_connection_tracker__create;
	snat_tracker->free_state = &nat_conntracker_free_snat_state;
	snat_tracker->run_gc = &nat_conntracker__gc;
	snat_tracker->last_gc_run = 0;
	snat_tracker->should_run_gc = &nat_conntracker__should_run_gc;
	init_protocol_handlers(snat_tracker);


	tcp_snat_handler->hash = &nat_connection_tracker__snat__tcp_hash;
	tcp_snat_handler->translate= &nat_connection_tracker__snat__tcp_translate;
	tcp_snat_handler->create= &nat_connection_tracker__snat__tcp_create;
	tcp_snat_handler->match= &nat_connection_tracker__snat__tcp_match;
	tcp_snat_handler->expired = &nat_conntracker__tcp_expired;

	snat_tracker->protocol_handlers[IPPROTO_TCP] = tcp_snat_handler;

	udp_snat_handler->hash = &nat_connection_tracker__snat__udp_hash;
	udp_snat_handler->translate= &nat_connection_tracker__snat__udp_translate;
	udp_snat_handler->create= &nat_connection_tracker__snat__udp_create;
	udp_snat_handler->match= &nat_connection_tracker__snat__udp_match;
	udp_snat_handler->expired = &nat_conntracker__udp_expired;

	snat_tracker->protocol_handlers[IPPROTO_UDP] = udp_snat_handler;

	icmp_snat_handler->hash = &nat_connection_tracker__snat__icmp_hash;
	icmp_snat_handler->translate= &nat_connection_tracker__snat__icmp_translate;
	icmp_snat_handler->create= &nat_connection_tracker__snat__icmp_create;
	icmp_snat_handler->match= &nat_connection_tracker__snat__icmp_match;
	icmp_snat_handler->expired = &nat_conntracker__icmp_expired;
	snat_tracker->protocol_handlers[IPPROTO_ICMP] = icmp_snat_handler;

	gre_snat_handler->hash = &nat_connection_tracker__snat__gre_hash;
	gre_snat_handler->translate= &nat_connection_tracker__snat__gre_translate;
	gre_snat_handler->create= &nat_connection_tracker__snat__gre_create;
	gre_snat_handler->match= &nat_connection_tracker__snat__gre_match;
	gre_snat_handler->expired = &nat_conntracker__gre_expired;
	snat_tracker->protocol_handlers[IPPROTO_GRE] = gre_snat_handler;

	engine->trackers[NAT_SNAT] = snat_tracker;

	/* DNAT */
	dnat_tracker->check = &nat_connection_tracker__check;
	dnat_tracker->translate = &nat_connection_tracker__translate;
	dnat_tracker->create = &nat_connection_tracker__create;
	dnat_tracker->free_state = &nat_conntracker_free_state;
	dnat_tracker->run_gc = &nat_conntracker__gc;
	dnat_tracker->last_gc_run = 0;
	dnat_tracker->should_run_gc = &nat_conntracker__should_run_gc;
	init_protocol_handlers(dnat_tracker);

	tcp_dnat_handler->hash = &nat_connection_tracker__dnat__tcp_hash;
	tcp_dnat_handler->translate= &nat_connection_tracker__dnat__tcp_translate;
	tcp_dnat_handler->create= &nat_connection_tracker__dnat__tcp_create;
	tcp_dnat_handler->match= &nat_connection_tracker__dnat__tcp_match;
	tcp_dnat_handler->expired = &nat_conntracker__tcp_expired;
	dnat_tracker->protocol_handlers[IPPROTO_TCP] = tcp_dnat_handler;

	udp_dnat_handler->hash = &nat_connection_tracker__dnat__udp_hash;
	udp_dnat_handler->translate= &nat_connection_tracker__dnat__udp_translate;
	udp_dnat_handler->create= &nat_connection_tracker__dnat__udp_create;
	udp_dnat_handler->match= &nat_connection_tracker__dnat__udp_match;
	udp_dnat_handler->expired = &nat_conntracker__udp_expired;
	dnat_tracker->protocol_handlers[IPPROTO_UDP] = udp_dnat_handler;

	icmp_dnat_handler->hash = &nat_connection_tracker__dnat__icmp_hash;
	icmp_dnat_handler->translate= &nat_connection_tracker__dnat__icmp_translate;
	icmp_dnat_handler->create= &nat_connection_tracker__dnat__icmp_create;
	icmp_dnat_handler->match= &nat_connection_tracker__dnat__icmp_match;
	icmp_dnat_handler->expired = &nat_conntracker__icmp_expired;
	dnat_tracker->protocol_handlers[IPPROTO_ICMP] = icmp_dnat_handler;

        gre_dnat_handler->hash = &nat_connection_tracker__dnat__gre_hash;
        gre_dnat_handler->translate= &nat_connection_tracker__dnat__gre_translate;
        gre_dnat_handler->create= &nat_connection_tracker__dnat__gre_create;
        gre_dnat_handler->match= &nat_connection_tracker__dnat__gre_match;
        gre_dnat_handler->expired = &nat_conntracker__gre_expired;
        dnat_tracker->protocol_handlers[IPPROTO_GRE] = gre_dnat_handler;

	engine->trackers[NAT_DNAT] = dnat_tracker;

	/* ROUTE */
	route_tracker->check = &nat_connection_tracker__check;
	route_tracker->translate = &nat_connection_tracker__translate;
	route_tracker->create = &nat_connection_tracker__create;
	route_tracker->free_state = &nat_conntracker_free_state;
	route_tracker->run_gc = &nat_conntracker__gc;
	route_tracker->last_gc_run = 0;
	route_tracker->should_run_gc = &nat_conntracker__should_run_gc;
	init_protocol_handlers(route_tracker);

	tcp_route_handler->hash = &nat_connection_tracker__route__tcp_hash;
	tcp_route_handler->translate= &nat_connection_tracker__route__translate;
	tcp_route_handler->create= &nat_connection_tracker__route__tcp_create;
	tcp_route_handler->match= &nat_connection_tracker__route__tcp_match;
	tcp_route_handler->expired = &nat_conntracker__tcp_expired;
	route_tracker->protocol_handlers[IPPROTO_TCP] = tcp_route_handler;

	udp_route_handler->hash = &nat_connection_tracker__route__udp_hash;
	udp_route_handler->translate= &nat_connection_tracker__route__translate;
	udp_route_handler->create= &nat_connection_tracker__route__udp_create;
	udp_route_handler->match= &nat_connection_tracker__route__udp_match;
	udp_route_handler->expired = &nat_conntracker__udp_expired;
	route_tracker->protocol_handlers[IPPROTO_UDP] = udp_route_handler;

	icmp_route_handler->hash = &nat_connection_tracker__route__icmp_hash;
	icmp_route_handler->translate= &nat_connection_tracker__route__translate;
	icmp_route_handler->create= &nat_connection_tracker__route__icmp_create;
	icmp_route_handler->match= &nat_connection_tracker__route__icmp_match;
	icmp_route_handler->expired = &nat_conntracker__icmp_expired;
	route_tracker->protocol_handlers[IPPROTO_ICMP] = icmp_route_handler;
	engine->trackers[NAT_ROUTE] = route_tracker;

	init_nat_connection_tracker_store(snat_tracker);
	init_nat_connection_tracker_store(dnat_tracker);
	init_nat_connection_tracker_store(route_tracker);
	init_snat_port_pool();
	return engine;
}

struct snat_port_store {
	unsigned short store[SNAT_PORT_POOL_PORT_MAX];
	unsigned short cursor;
};	

struct snat_port_store snat_port_pool[SNAT_PORT_POOL_IP_SIZE];
void init_snat_port_pool(){
	int x = 0,  y = 0;

	for(x =0; x < SNAT_PORT_POOL_IP_SIZE; x++){
		snat_port_pool[x].cursor = SNAT_PORT_POOL_PORT_MAX - 5;
		for(y = 0; y < SNAT_PORT_POOL_PORT_MAX; y++){
			snat_port_pool[x].store[y] = NOT_CHECKED_OUT;
		}
	}	
}

unsigned short find_free_snat_port_from_cursor(struct snat_port_store* pool){
	for(; pool->cursor < SNAT_PORT_POOL_PORT_MAX; ++pool->cursor){
		if(pool->store[pool->cursor] != CHECKED_OUT){
			pool->store[pool->cursor] = CHECKED_OUT;
			return pool->cursor;
		}
	}
	return 0;
}

unsigned short find_free_snat_port(struct snat_port_store* pool){
	unsigned short ret = 0;
	ret = find_free_snat_port_from_cursor(pool);
	if(ret == 0){
		//reset the cursor and try again
		pool->cursor = SNAT_PORT_POOL_PORT_MIN;
		ret = find_free_snat_port_from_cursor(pool);
	}

	//We still could not find a matching port, lets trigger the cleanup cron and see how we go
	if(ret == 0){
		printk("find_free_snat_port has run out of available connections, triggering cleanup\n");
	}

	return ret;
}

unsigned short aquire_snat_port(unsigned int destination_ip_address, unsigned short active_port){
	unsigned int bucket = (destination_ip_address % SNAT_PORT_POOL_IP_SIZE);	
	struct snat_port_store* target_bucket = &snat_port_pool[bucket];	

	//First lets see if we can be cheap and get away with using the current port
	if(target_bucket->store[active_port] != CHECKED_OUT){
		target_bucket->store[active_port] = CHECKED_OUT;
		return active_port;
	}

	//Ok, shit happens, we have to mix it up a little bit
	return find_free_snat_port(target_bucket);
}

void free_snat_port(unsigned int destination_ip_address, unsigned short active_port){
	unsigned int bucket = (destination_ip_address % SNAT_PORT_POOL_IP_SIZE);
	struct snat_port_store* target_bucket = &snat_port_pool[bucket];
	target_bucket->store[active_port] = NOT_CHECKED_OUT;
}

bool nat_icmp_matches(struct nf_queue_entry* entry, unsigned int source_adddress, unsigned int destination_address, unsigned int id){
	if(ip_hdr(entry->skb)->protocol == IPPROTO_ICMP){
		if(ip_hdr(entry->skb)->saddr == source_adddress 
				&& ip_hdr(entry->skb)->daddr == destination_address){

			if(icmphdr_ptr(entry->skb)->un.echo.id == id){
				return true;
			}
		}
	}

	return false;
}

bool nat_gre_matches(struct nf_queue_entry* entry, unsigned int source_adddress, unsigned int destination_address, unsigned int id){
        if(ip_hdr(entry->skb)->protocol == IPPROTO_GRE){
                if(ip_hdr(entry->skb)->saddr == source_adddress
                                && ip_hdr(entry->skb)->daddr == destination_address){

                        if(grehdr_ptr(entry->skb)->call_id == id){
                                return true;
                        }
                }
        }

        return false;
}

bool nat_tcp_matches(struct nf_queue_entry* entry, unsigned int source_adddress, unsigned int destination_address, int source_port, int destination_port){
	struct iphdr* ip = ip_hdr(entry->skb);
	if(ip->protocol == IPPROTO_TCP){
		struct tcphdr* tcp = tcphdr_ptr(entry->skb);
		if(ip->saddr == source_adddress && ip->daddr == destination_address){
			if(tcp->source == source_port && tcp->dest== destination_port){
				return true;
			}
		}
	}else if(ip->protocol == IPPROTO_UDP){
		struct udphdr* tcp = udphdr_ptr(entry->skb); 
		if(ip->saddr == source_adddress && ip->daddr == destination_address){
			if(tcp->source == source_port && tcp->dest== destination_port){
				return true;
			}
		}
	}

	return false;
}

void nat_conntracker_updateconnectionstate(struct natct* ct, struct nf_queue_entry* entry){
	ct->timestamp = currenttimestamp();	

	if(ct->protocol == IPPROTO_TCP){
		struct iphdr* ip = ip_hdr(entry->skb);
		struct tcphdr* tcp = (struct tcphdr *) ((unsigned char*) ip + (ip->ihl << 2));
		if(ct->state == CLOSING || tcp->fin == 1 || tcp->rst == 1){
			ct->state = CLOSING;
		}else{
			ct->state = TCPUP;
		}
	}	
}

bool nat_conntracker__tcp_expired(struct nat_connection_tracker_protocol_handler* _self, struct natct* ct){
	int idleTime = currenttimestamp() - ct->timestamp;
	if (ct->state == TCPUP && idleTime >= TCP_CONNECTION_TIMEOUT_THESHOLD){
		return true;
	}

	if (ct->state == CLOSING && idleTime >= TCP_CLOSING_TIMEOUT){
		return true;
	}

	return false;
}

bool nat_conntracker__udp_expired(struct nat_connection_tracker_protocol_handler* _self, struct natct* ct){
	if((currenttimestamp() - ct->timestamp) > UDP_MAX_CONNECTION_LIFE){
		return true;
	}
	return false;
}

bool nat_conntracker__icmp_expired(struct nat_connection_tracker_protocol_handler* _self, struct natct* ct){
	if((currenttimestamp() - ct->timestamp) > ICMP_MAX_CONNECTION_LIFE){
		return true;
	}
	return false;
}

bool nat_conntracker__gre_expired(struct nat_connection_tracker_protocol_handler* _self, struct natct* ct){
        if((currenttimestamp() - ct->timestamp) > ICMP_MAX_CONNECTION_LIFE){
                return true;
        }
        return false;
}

void nat_conntracker__gc(struct nat_connection_tracker* tracker){
	int i = 0;
	int count = 0;
	struct natct *cursor = NULL;
	struct list_head *pos, *q;

	for(; i < NAT_CONNTRACKER_MAXCONNS; i++){
		list_for_each_safe(pos, q, &tracker->store[i]) {
			cursor = list_entry(pos, struct natct, list);

			if(cursor->protocol_handler->expired(cursor->protocol_handler, cursor)){
				list_del(pos);
				tracker->free_state(tracker, cursor);
				count++;
			}
		}

	}	

	tracker->last_gc_run = currenttimestamp();
}

bool nat_conntracker__should_run_gc(struct nat_connection_tracker* _self){
	int time_since_last_run = currenttimestamp() - _self->last_gc_run;
	return time_since_last_run > NAT_GC_TIMER;
}
