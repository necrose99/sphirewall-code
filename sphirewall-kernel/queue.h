/*
Copyright Michael Lawson
This file is part of sphirewall-kernel for Sphirewall.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#ifndef QUEUE_H
#define QUEUE_H
#include <linux/version.h>
#include <linux/module.h>
#include <linux/socket.h>
#include <linux/net.h>
#include <linux/skbuff.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/ip.h>
#include <linux/tcp.h>
#include <linux/icmp.h>
#include <linux/in.h>
#include <linux/netfilter_ipv4.h>
#include <linux/delay.h>
#include <linux/kthread.h>
#include <linux/netfilter.h>
#include <linux/kfifo.h>
#include <linux/msg.h>
#include <linux/ipc.h>
#include <linux/types.h>
#include <linux/in_route.h>
#include <linux/kfifo.h>
#include <linux/if_ether.h>
#include <net/ip.h>
#include <linux/interrupt.h>
#include <net/sock.h>


#include "nbqueue.h"

#if LINUX_VERSION_CODE < KERNEL_VERSION(3,8,0)
#define NFW_register_queue_handler(a,b) nf_register_queue_handler((a),(b))
#define REGISTER_QUEUE_HANDLER_FOR_ALL_PROTOS(a) nf_register_queue_handler(NFPROTO_IPV4, a); \
nf_register_queue_handler(NFPROTO_IPV6, a)
#define UNREGISTER_QUEUE_HANDLER_FOR_ALL_PROTOS(a) nf_unregister_queue_handlers(a);
#endif

#if LINUX_VERSION_CODE >= KERNEL_VERSION(3,8,0)
#define NFW_register_queue_handler(a,b) nf_register_queue_handler(b)
#define NFW_unregister_queue_handler(a) nf_unregister_queue_handler()
#define REGISTER_QUEUE_HANDLER_FOR_ALL_PROTOS(a) nf_register_queue_handler(a)
#define UNREGISTER_QUEUE_HANDLER_FOR_ALL_PROTOS(a) nf_unregister_queue_handler()
#endif

#if LINUX_VERSION_CODE >= KERNEL_VERSION(3,7,0)
#define VM_RESERVED (VM_IO | VM_LOCKED | (VM_DONTEXPAND | VM_DONTDUMP))
#endif

#define DEFAULT_ACTION NF_ACCEPT
#define NETLINK_TEST 19

#define VFW_GROUP 1

#ifdef DEBUG
#define PDEBUG(args...) printk ("sphirewall_queue: " args)
#else
#define PDEBUG(args...)
#endif

#define PMESSAGE(args...) printk("sphirewall_queue: " args)

/* Hash map used to store nf_queue_entries based on id, that are waiting on a response from the client */
extern int entry_map_add(struct nf_queue_entry *entry);
extern struct nf_queue_entry *entry_map_get(int x);

/* Processing queue*/
extern void send_queue_init(void);
extern int send_queue_empty(void);
extern void send_queue_push_entry(struct nf_queue_entry *entry);
extern struct nf_queue_entry *send_queue_pop_entry(void);

/* Processing workers */
extern void squeue_incoming_worker(struct work_struct *work);
extern void squeue_outgoing_worker(struct work_struct *work);
extern int squeue_outgoing_push(struct nf_queue_entry *entry, unsigned int queuenum);

extern unsigned int hook_func( unsigned int hooknum,
                        struct sk_buff *pskb,
                        const struct net_device *in,
                        const struct net_device *out,
                        int (*okfn)(struct sk_buff *));

/* Setup and tear down the queues and hooks*/
extern void register_hooks(void);
extern void unregister_hooks(void);

extern int open_shm(void);
extern void close_shm(void);

extern wait_queue_head_t recv_wait_lock;
static DECLARE_WORK(incomming_packets_worker, squeue_incoming_worker);
static DECLARE_WORK(outgoing_packet_worker, squeue_outgoing_worker);

extern struct workqueue_struct* incomming_packets_worker_wq;
extern struct workqueue_struct* outgoing_packet_worker_wq;
#endif
