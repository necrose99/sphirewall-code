/*
Copyright Michael Lawson
This file is part of sphirewall-kernel for Sphirewall.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#include <asm/types.h>
#include <linux/module.h>
#include <linux/socket.h>
#include <linux/net.h>
#include <linux/skbuff.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/ip.h>
#include <linux/tcp.h>
#include <linux/icmp.h>
#include <linux/in.h>
#include <linux/netfilter_ipv4.h>
#include <linux/delay.h>
#include <linux/kthread.h>
#include <linux/netfilter.h>
#include <linux/kfifo.h>
#include <linux/msg.h>
#include <linux/ipc.h>
#include <linux/types.h>
#include <linux/in_route.h>
#include <linux/kfifo.h>
#include <linux/if_ether.h>
#include <net/ip.h>
#include <net/sock.h>
#include <net/netfilter/nf_queue.h>
#include <net/tcp.h>

#include "checksum.h"

long checksum(unsigned short *addr, unsigned int count) {
        register long sum = 0;
        while (count > 1) {
                sum += *addr++;
                count -= 2;
        }

        if (count > 0)
                sum += *(unsigned char *) addr;

        while (sum >> 16) {
                sum = (sum & 0xffff) + (sum >> 16);
        }
        return ~sum;
}

static void nf_nat_ipv4_csum_recalc(struct sk_buff *skb, u8 proto, void *data, __sum16 *check, int datalen, int oldlen){
	const struct iphdr *iph = ip_hdr(skb);
	if((!skb->dev || skb->dev->features & NETIF_F_V4_CSUM)) {
		skb->ip_summed = CHECKSUM_PARTIAL;
		skb->csum_start = skb_headroom(skb) +
			skb_network_offset(skb) +
			ip_hdrlen(skb);
		skb->csum_offset = (void *)check - data;
		*check = ~csum_tcpudp_magic(iph->saddr, iph->daddr, datalen, proto, 0);
	}else{
		*check = csum_tcpudp_magic(iph->saddr, iph->daddr, datalen, proto, csum_partial(data, datalen,0));
		if (proto == IPPROTO_UDP && !*check){
			*check = CSUM_MANGLED_0;
		}

		skb->ip_summed =CHECKSUM_UNNECESSARY;
	}
}

void nat_tcp_updatechecksum(struct sk_buff* skb, struct iphdr* ip, struct tcphdr* tcp){
	int len = 0;

	ip->check = 0;
	ip->check = checksum((unsigned short *) ip, sizeof (struct iphdr));

	tcp->check = 0;
	len = ntohs(ip->tot_len) - (ip->ihl << 2);
	nf_nat_ipv4_csum_recalc(skb, IPPROTO_TCP, tcp, &tcp->check, len, len);
}

void nat_udp_updatechecksum(struct iphdr* ip, struct udphdr* udp){
	ip->check = 0;
	ip->check = checksum((unsigned short *) ip, sizeof (struct iphdr));
	udp->check = 0;
}

void nat_icmp_updatechecksum(struct iphdr* ip, struct icmphdr* icmp){
	int size = 0;
	ip->check = 0;
	ip->check = checksum((unsigned short *) ip, sizeof (struct iphdr));
	icmp->checksum = 0;
	size = ntohs(ip->tot_len) - sizeof (struct iphdr);
	icmp->checksum = checksum((unsigned short*) icmp, size);
}

void nat_gre_updatechecksum(struct iphdr* ip, struct grehdr* icmp){
	int size = 0;
	ip->check = 0;
	ip->check = checksum((unsigned short *) ip, sizeof (struct iphdr));
	size = ntohs(ip->tot_len) - sizeof (struct iphdr);
}

