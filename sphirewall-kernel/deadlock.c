#include <asm/types.h>
#include <linux/module.h>
#include <linux/socket.h>
#include <linux/net.h>
#include <linux/skbuff.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/ip.h>
#include <linux/tcp.h>
#include <linux/icmp.h>
#include <linux/in.h>
#include <linux/netfilter_ipv4.h>
#include <linux/delay.h>
#include <linux/kthread.h>
#include <linux/netfilter.h>
#include <linux/kfifo.h>
#include <linux/msg.h>
#include <linux/ipc.h>
#include <linux/types.h>
#include <linux/in_route.h>
#include <linux/kfifo.h>
#include <linux/if_ether.h>
#include <net/ip.h>
#include <net/sock.h>
#include <net/netfilter/nf_queue.h>
#include <net/tcp.h>

#include "deadlock.h"
#include "nbqueue.h"
#include "time.h"

static int DEADLOCK_DETECTION_ENABLED= 1;
static int DEADLOCK_DETECTION_TIMEOUT= 600; 

static int DEADLOCK_DETECTION_PANIC= 1;
static int DEADLOCK_DETECTION_TERMINATE= 0; 

static int deadlock_detection_last_packet= 0;

module_param(DEADLOCK_DETECTION_ENABLED, int, 0644);
module_param(DEADLOCK_DETECTION_TIMEOUT, int, 0644);
module_param(DEADLOCK_DETECTION_PANIC, int, 0644);
module_param(DEADLOCK_DETECTION_TERMINATE, int, 0644);

void init_deadlock_detection(void){
        deadlock_detection_last_packet = currenttimestamp();
}

void update_deadlock_detection(void){
        deadlock_detection_last_packet = currenttimestamp();
}

void poll_deadlock_detection(void){
	if(DEADLOCK_DETECTION_ENABLED != 1){
		return;
	}

        if((currenttimestamp() - deadlock_detection_last_packet) > DEADLOCK_DETECTION_TIMEOUT){
		printk("poll_deadlock_detection() has not received packets from userspace in %d seconds, terminating userspace process\n", (currenttimestamp() - deadlock_detection_last_packet));

		if(DEADLOCK_DETECTION_TERMINATE == 1){
			if(qmgr_send && qmgr_send->block){
				struct siginfo info;
				struct task_struct *pid_task_object = NULL;

				memset(&info, 0, sizeof(struct siginfo));
				info.si_signo = SIGSEGV;
				info.si_code = SI_QUEUE;

				if((pid_task_object = pid_task(find_vpid(qmgr_send->block->pid), PIDTYPE_PID))){
					send_sig_info(SIGSEGV, &info, pid_task_object);
				}
			}
		}

		if(DEADLOCK_DETECTION_PANIC == 1){
			panic("sphirewall_queue detected a deadlock in userspace and has decided to panic");
		}
	}
}
