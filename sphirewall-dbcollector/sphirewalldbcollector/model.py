from sqlalchemy import Column, String, Integer
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

class PersistedConnection(Base):
    __tablename__ = "sphirewall_connections"

    id = Column(Integer, primary_key=True, autoincrement=True)
    device_id = Column(String(100))
    source_ip = Column(String(50))
    destination_ip = Column(String(50))
    source_port = Column(Integer)
    destination_port = Column(Integer)
    time = Column(Integer)
    upload = Column(Integer)
    download = Column(Integer)
    user_agent = Column(String(100))
    content_type = Column(String(100))
    input_device = Column(String(5))
    output_device = Column(String(5))
    protocol = Column(Integer)
    source_hostname = Column(String(100))
    hw_address = Column(String(32))
    username = Column(String(100))
    http_hostname = Column(String(100))
    application_layer_tag = Column(String(100))


class PersistedEvent(Base):
    __tablename__ = "sphirewall_events"

    id = Column(Integer, primary_key=True, autoincrement=True)
    device_id = Column(String(100))
    time = Column(Integer)
    type = Column(String(100))
    params = Column(String(1024))

