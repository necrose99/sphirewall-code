The sphirewall-dbcollector is a small application that can be used to persist connections and event objects to a sql database.

INSTALLATION AND CONFIGURATION
To configure:
1. Edit the file /etc/sphirewall_dbcollector.conf, and insert the your database details. The database tables will be created automatically, and
   named as sphirewall_connections and sphirewall_events.

2. Configure sphirewall to push data to the dbcollector application. This can be done through the WMI under Configure --> External Analytics Listener.

   Enabled: Checked
   Url: http://localhost:5002
   Deviceid: An identifier
   Token: Not required

You can start and stop the collector service using the init.d script installed under /etc/init.d
   $> service python-sphirewalldbcollector start/stop

This application will log errors and status information to /var/log/sphirewall_dbcollector.log

DEBUGGING:
The easiest way to debug this application is to run it from the console using the python tools

   $> python
   $> >> from sphirewalldbcollector import app
   $> >> app.run()

Please keep in mind, that Sphirewall only pushes data out periodically, so it might take a few minutes before you start
seeing data in the database.
