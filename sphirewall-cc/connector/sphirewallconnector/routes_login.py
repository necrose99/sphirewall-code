from flask import request, session, redirect, render_template
from flask.ext.login import login_user, login_required, logout_user
from sphirewallapi.sphirewall_api import SphirewallClient, SphirewallSocketTransportProvider
from sphirewallconnector import app, login_manager
from sphirewallconnector.api.utils import arg


def get_session_sphirewall_token():
    return session.get("sphirewall_token")


def get_session_sphirewall_hostname():
    return session["sphirewall_host"]


def get_session_sphirewall_port():
    return session["sphirewall_port"]


def set_session_sphirewall_token(token):
    session["sphirewall_token"] = token


def set_session_sphirewall_hostname(hostname):
    session["sphirewall_host"] = hostname


def set_session_sphirewall_port(port):
    session["sphirewall_port"] = port


class TokenBasedUser():
    def __init__(self, token):
        self.token = token

    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return self.token


def standard_login():
    if request.method == "POST":
        sphirewall_hostname = request.form.get("hostname")
        sphirewall_port = request.form.get("port")
        set_session_sphirewall_hostname(sphirewall_hostname)
        set_session_sphirewall_port(sphirewall_port)

        transport_provider = SphirewallSocketTransportProvider(get_session_sphirewall_hostname(), get_session_sphirewall_port())
        sphirewall_client = SphirewallClient(transport_provider)
        token = sphirewall_client.connection.authenticate(request.form.get("email"), request.form.get("password"))
        if token is not None:
            set_session_sphirewall_token(token)
            login_user(TokenBasedUser(token))
            return redirect("/")
        else:
            return render_template("login.html", error="Access was denied")
    else:
        return render_template("login.html", error=arg("error"), username=request.args.get("username", ""), password=request.args.get("password", ""))


def standard_logout():
    logout_user()
    set_session_sphirewall_token(None)
    set_session_sphirewall_hostname(None)
    return redirect("/")


def standard_load_user(userid):
    session_token = get_session_sphirewall_token()
    if not session_token:
        return None

    return TokenBasedUser(session_token)


@app.route("/login", methods=['GET', 'POST'])
def login():
    return standard_login()


@app.route("/logout")
@login_required
def logout():
    return standard_logout()


@login_manager.user_loader
def load_user(userid):
    return standard_load_user(userid)