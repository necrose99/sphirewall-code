from string import replace
from flask.ext.login import login_required

from flask.globals import request, current_app
from flask.templating import render_template
from werkzeug.utils import redirect
from sphirewallconnector import app
from sphirewallconnector.api.utils import DateHelper, text_value


@login_required
@app.route('/device')
def main():
    return render_template(
        'device_home.html',
        totalUsers=len(current_app.sphirewall().general().active_users_list()),
        totalHosts=current_app.sphirewall().general().hosts_size(),
        totalConnection=current_app.sphirewall().firewall().connections_size(),
        version=current_app.sphirewall().version(),
        events=current_app.sphirewall().general().events_size(),
        token=current_app.sphirewall().get_token())


@login_required
@app.route("/device/status/events")
def status_events():
    return render_template("device_status_events.html", params=process_params())


@login_required
@app.route("/device/status/connections")
def status_connections():
    return render_template("device_status_connections.html", params=process_params())


@login_required
@app.route("/device/status/connections/terminate/<sourceIp>/<sourcePort>/<destIp>/<destPort>")
def status_connections_terminate(sourceIp, sourcePort, destIp, destPort):
    current_app.sphirewall().firewall().connection_terminate(sourceIp, sourcePort, destIp, destPort)
    return redirect("/status/connections")


@login_required
@app.route("/device/status/metrics")
def status_metrics():
    params = process_params()
    return render_template(
        "device_status_metrics.html", params=params)


@login_required
@app.route("/device/status/metrics/all")
def status_metrics_all():
    params = process_params()
    stats = current_app.service().statistics_metrics(process_params())
    return render_template("device_status_metrics_all.html", params=params, stats=stats)


@login_required
@app.route("/device/status/logs", methods=["POST", "GET"])
def status_logs():
    filter = text_value("filter", default="")
    no_lines = int(text_value("no_lines", default=100))
    if filter and len(filter) > 0:
        general__logs = current_app.sphirewall().general().logs(filter=filter, no_lines=no_lines)
    else:
        general__logs = current_app.sphirewall().general().logs(no_lines=no_lines)

    return render_template(
        "device_status_logs.html", logs=replace(general__logs, "\n", "<br>"), filter=filter, no_lines=no_lines
    )


@login_required
@app.route("/device/status/connections/states")
def status_connections_states():
    return render_template("device_status_connections_states.html")


@login_required
@app.route("/device/status/update")
def status_update():
    status = current_app.sphirewall().general().sphireos_update_status()
    status["formatted_log"] = status["log"].replace("\n", "<br>")
    version = current_app.sphirewall().general().sphireos_version()
    return render_template("device_configuration_update.html", status=status, version=version)

@login_required
@app.route("/device/status/update/start")
def status_update_start():
    current_app.sphirewall().general().sphireos_update_start()
    return redirect("/device/status/update")

@login_required
@app.route("/device/status/restart")
def status_restart():
    current_app.sphirewall().general().sphireos_restart()
    return redirect("/device/status")

def process_params():
    params = {}
    if "startDate" in request.args and len(request.args["startDate"]) > 0:
        params["startDate"] = request.args["startDate"]
    elif cookie_value("startDate") is not None:
        params["startDate"] = cookie_value("startDate")
    else:
        params["startDate"] = DateHelper.yesterday().isoformat()

    if "endDate" in request.args and len(request.args["endDate"]) > 0:
        params["endDate"] = request.args["endDate"]
    elif cookie_value("endDate") is not None:
        params["endDate"] = cookie_value("endDate")
    else:
        params["endDate"] = DateHelper.today().isoformat()

    if "offset" in request.args:
        params["offset"] = request.args["offset"]
    if "limit" in request.args:
        params["limit"] = request.args["limit"]
    if "search" in request.args:
        params["search"] = request.args["search"]

    return params


def cookie_value(key):
    return request.cookies.get(key)
