from flask import jsonify, flash
from flask.ext.login import login_required

from flask.globals import request, current_app
from flask.templating import render_template
from werkzeug.utils import redirect
from sphirewallconnector import app
from sphirewallconnector.api.utils import text_value, checkbox_value, int_value


@login_required
@app.route("/device/network/devices")
def network_devices():
    return render_template(
        "device_network_devices.html", routes=current_app.sphirewall().network().routes(),
        devices=current_app.sphirewall().network().devices(configured=True), dns_config=current_app.sphirewall().network().dns_config()
    )


@login_required
@app.route("/device/network/devices/<device>/up")
def network_devices_up(device):
    current_app.sphirewall().network().devices_up(device)
    return redirect("/device/network/devices")


@login_required
@app.route("/device/network/devices/<device>/down")
def network_devices_down(device):
    current_app.sphirewall().network().devices_down(device)
    return redirect("/device/network/devices")


@app.route("/device/network/dns", methods=["POST"])
def network_dns():
    current_app.sphirewall().network().dns_config_set(text_value("domain"), text_value("search"), text_value("ns1"), text_value("ns2"))
    return redirect("/device/network/devices")


@login_required
@app.route("/device/network/devices/<device>/delete")
def network_devices_delete(device):
    current_app.sphirewall().network().devices_remove(device)
    return redirect("/device/network/devices")


@login_required
@app.route("/device/network/devices/create", methods=["GET", "POST"])
@app.route("/device/network/devices/<device>", methods=["GET", "POST"])
def network_devices_edit(device=None):
    if request.method == "POST":
        device_params = {}

        existing_device_configuration = None
        if device:
            existing_device_configuration = current_app.sphirewall().network().devices(device, configured=True)

        device_params["name"] = text_value("name")
        device_params["persisted"] = checkbox_value("persisted")
        if text_value("type") == "physical":
            device_params["physical"] = True
            device_params["physicalInterface"] = text_value("device")

        if text_value("type") == "vlan":
            device_params["vlan"] = True
            device_params["vlanId"] = int(text_value("vlanId"))
            device_params["vlanInterface"] = text_value("vlanInterface")

        if text_value("type") == "bridge" or (existing_device_configuration and existing_device_configuration.get("bridge")):
            device_params["bridge"] = True
            device_params["bridgeDevices"] = request.form.getlist('bridgeDevices')

        if text_value("type") == "lacp" or (existing_device_configuration and existing_device_configuration.get("lacp")):
            device_params["lacp"] = True
            device_params["lacpDevices"] = request.form.getlist('lacpDevices')

            device_params["lacpMode"] = int(text_value("lacpMode", "0", False))
            device_params["lacpLinkStatePollFrequency"] = int(text_value("lacpLinkStatePollFrequency", "0", False))
            device_params["lacpLinkStateDownDelay"] = int(text_value("lacpLinkStateDownDelay", "0", False))
            device_params["lacpLinkStateUpDelay"] = int(text_value("lacpLinkStateUpDelay", "0", False))

        ipv4_addresses = []
        if text_value("aliases"):
            for raw_address in text_value("aliases").split("\n"):
                if raw_address and len(raw_address.split("/")) == 3:
                    address = {
                        "ip": raw_address.split("/")[0].strip(),
                        "mask": raw_address.split("/")[1].strip(),
                        "broadcast": raw_address.split("/")[2].strip()
                    }
                    ipv4_addresses.append(address)

        address_params = {
            "ipv4": ipv4_addresses,
            "dhcp": checkbox_value("dhcp"),
            "gateway": text_value("gateway"),
        }

        dhcp_server = {
            "dhcpServerMode": int(text_value("dhcpServerMode", default="1")),
            "dhcpServerStart": text_value("dhcpServerStart"),
            "dhcpServerEnd": text_value("dhcpServerEnd"),
            "dhcpServerRelay": text_value("dhcpServerRelay"),
            "dhcpServerLeaseTime": int_value("dhcpServerLeaseTime", default=7200),

            "dhcpServerDnsMode": int(text_value("dhcpServerDnsMode", default="0")),
            "dhcpServerDnsServer": text_value("dhcpServerDnsServer"),

            "dhcpServerTftpEnabled": checkbox_value("dhcpServerTftpEnabled"),
            "dhcpServerTftpServer": text_value("dhcpServerTftpServer"),
            "dhcpServerTftpFilename": text_value("dhcpServerTftpFilename")
        }

        current_app.sphirewall().network().devices_set(
            device, device_params=device_params, address_params=address_params, dhcp_server=dhcp_server
        )

        if device:flash("Saved interface configuration")
        else:flash("Created a new virtual device")
        return redirect("/device/network/devices")

    current_device_stored = None
    if device:
        current_device_stored = current_app.sphirewall().network().devices(device, configured=True)
    return render_template(
        "device_network_devices_edit.html", device=current_device_stored,
        devices=current_app.sphirewall().network().devices(configured=True)
    )


@login_required
@app.route("/device/network/devices/<device>/leases", methods=["GET", "POST"])
def network_devices_dhcp(device):
    if request.method == "POST":
        flash("Created new static dhcp lease for '%s'" % text_value("mac"))
        current_app.sphirewall().network().devices_dhcp_static_add(device, text_value("ip"), text_value("mac"))
    device = current_app.sphirewall().network().devices(device=device, configured=True)
    return render_template("device_network_devices_edit_dhcp.html", device=device)


@login_required
@app.route("/device/network/devices/<device>/leases/delete/<ip>/<mac>", methods=["GET"])
def network_devices_dhcp_lease_delete(device, ip, mac):
    flash("Removed dhcp lease for '%s'" % mac)
    current_app.sphirewall().network().devices_dhcp_static_del(device, ip, mac)
    return redirect("/device/network/devices/%s/leases" % device)


@login_required
@app.route("/device/network/devices/routes", methods=["POST"])
def network_devices_routes():
    flash("Added new static route")
    current_app.sphirewall().network().routes_add(text_value("destination"), text_value("destination_cidr"), text_value("route_device"), text_value("route_nexthop"))
    return redirect("/device/network/devices")


@login_required
@app.route("/device/network/devices/routes/del/<id>", methods=["GET"])
def network_devices_routes_del(id):
    flash("Deleted static route")
    current_app.sphirewall().network().routes_del(id)
    return redirect("/device/network/devices")


@login_required
@app.route("/device/network/wireless", methods=["GET", "POST"])
def network_wireless():
    if request.method == "POST":
        current_app.sphirewall().general().configuration("wireless:enabled", checkbox_value("enabled"))
        current_app.sphirewall().general().configuration("wireless:interface", text_value("interface"))
        current_app.sphirewall().general().configuration("wireless:ssid", text_value("ssid"))
        current_app.sphirewall().general().configuration("wireless:channel", int(text_value("channel")))
        current_app.sphirewall().general().configuration("wireless:driver", text_value("driver"))

        if checkbox_value("enabled"):
            current_app.sphirewall().network().wireless_start()
        else:
            current_app.sphirewall().network().wireless_stop()
        flash("Saved wireless access point configuration")

    enabled = current_app.sphirewall().general().configuration("wireless:enabled")
    interface = current_app.sphirewall().general().configuration("wireless:interface")
    channel = current_app.sphirewall().general().configuration("wireless:channel")
    ssid = current_app.sphirewall().general().configuration("wireless:ssid")
    driver = current_app.sphirewall().general().configuration("wireless:driver")
    online = current_app.sphirewall().network().wireless_online()
    devices = current_app.sphirewall().network().devices()

    return render_template(
        "device_network_wireless.html", enabled=enabled, interface=interface, channel=channel, ssid=ssid,
        online=online, driver=driver, devices=devices
    )

@login_required
@app.route("/device/network/devices/<device>/set_save", methods=["POST"])
def device_set_save(device):
    existing_device_configuration = current_app.sphirewall().network().devices(device, configured=True)
    if "persisted" in existing_device_configuration and "persisted" in request.form:
        existing_device_configuration["persisted"] = request.form["persisted"] == "true"
        current_app.sphirewall().network().devices_set(device, device_params=existing_device_configuration)
        return jsonify(was_saved=True)
    else:
        return jsonify(was_saved=False)
