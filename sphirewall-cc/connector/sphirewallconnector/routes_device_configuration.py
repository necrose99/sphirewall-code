import operator
from flask import flash, current_app
from flask.ext.login import login_required

from flask.globals import request
from flask.templating import render_template
from werkzeug.utils import redirect

from sphirewallapi.sphirewall_api_general import QuotaInfo
from sphirewallconnector import app
from sphirewallconnector.api.utils import checkbox_value, text_value, nint


@app.route("/device/configuration/users", methods=["GET", "POST"])
@login_required
def configuration_users():
    if request.method == "POST":
        flash("Added new user called '%s'" % request.form.get("username"))
        current_app.sphirewall().general().users_add(request.form.get("username"), "", "")

    unsorted_users = current_app.sphirewall().general().users()
    users = sorted(unsorted_users, key=operator.itemgetter("username"))
    return render_template(
        "device_configuration_users.html", users=users, groups=current_app.sphirewall().general().groups()
    )


@app.route("/device/configuration/users/delete/<username>")
@login_required
def configuration_users_delete(username):
    flash("Deleted user called '%s'" % username)
    current_app.sphirewall().general().users_delete(username)
    return redirect("/device/configuration/users")


@app.route("/device/configuration/users/<username>", methods=["GET", "POST"])
@login_required
def configuration_users_edit(username):
    if request.method == "POST":
        quota = QuotaInfo()
        # configure groups:
        group_list = request.form.getlist('groups')
        groups = []
        for id in group_list:
            groups.append(id)

        current_app.sphirewall().general().users_groups_merge(username, groups)
        current_app.sphirewall().general().users_save(
            username, request.form["fname"], request.form["lname"], request.form["email"], quota
        )

        if len(request.form["password"]) > 0 and request.form["password"] == request.form["repassword"]:
            current_app.sphirewall().general().user_set_password(username, request.form["password"])
        flash("Saved user configuration")
        return redirect("/device/configuration/users")
    selected_groups = []
    groups = current_app.sphirewall().general().groups()
    user = current_app.sphirewall().general().users(username)
    for grp in user['groups']:
        selected_groups.append(grp['name'])
    return render_template(
        "device_configuration_users_edit.html",user=user, groups=groups, selected_groups=selected_groups
    )


@app.route("/device/configuration/users/<username>/disable")
@login_required
def configuration_users_disable(username):
    current_app.sphirewall().general().user_disable(username)
    return redirect("/device/configuration/users/" + username)


@app.route("/device/configuration/users/<username>/enable")
@login_required
def configuration_users_enable(username):
    current_app.sphirewall().general().user_enable(username)
    return redirect("/device/configuration/users/" + username)


@login_required
@app.route("/device/configuration/users/<username>/groups/remove/<groupid>", methods=["GET", "POST"])
def configuration_users_remove_group(username, groupid):
    current_app.sphirewall().general().user_groups_remove(username, groupid)
    return redirect("/device/configuration/users/" + username)


@app.route("/device/configuration/users/<username>/groups/add", methods=["POST"])
@login_required
def configuration_users_add_group(username):
    current_app.sphirewall().general().user_groups_add(username, request.form["group"])
    return redirect("/device/configuration/users/" + username)


@app.route("/device/configuration/groups", methods=["GET", "POST"])
@login_required
def configuration_groups():
    if request.method == "POST":
        flash("Created a new group called '%s'" % request.form.get("name"))
        current_app.sphirewall().general().group_add(request.form.get("name"))

    unsorted_groups = current_app.sphirewall().general().groups()
    groups = sorted(unsorted_groups, key=operator.itemgetter("name"))
    return render_template("device_configuration_groups.html", groups=groups)


@app.route("/device/configuration/groups/delete/<id>")
@login_required
def configuration_groups_delete(id):
    flash("Deleted a group")
    current_app.sphirewall().general().groups_delete(id)
    return redirect("/device/configuration/groups")


@app.route("/device/configuration/groups/<id>", methods=["GET", "POST"])
@login_required
def configuration_groups_edit(id):
    if request.method == "POST":
        quota = QuotaInfo()
        current_app.sphirewall().general().groups_save(
            id, request.form["manager"], checkbox_value("mui"), request.form["desc"], quota
        )
        flash("Saved group details and configuration")
        return redirect("/device/configuration/groups")

    return render_template("device_configuration_groups_edit.html", group=current_app.sphirewall().general().groups(id))


@app.route("/device/configuration/advanced", methods=["GET", "POST"])
@login_required
def configuration_advanced():
    if request.method == "POST":
        for fieldname, value in request.form.items():
            current_app.sphirewall().general().advanced(fieldname, nint(value))
        flash("Saved advanced configuration variables")
    return render_template("device_configuration_advanced.html", vars=current_app.sphirewall().general().advanced())

@app.route("/device/configuration/cloud", methods=["GET", "POST"])
@login_required
def configuration_cloud():
    if request.method == "POST":
        current_app.sphirewall().general().configuration("cloud:enabled", True)
        current_app.sphirewall().general().configuration("cloud:deviceid", text_value("device_id"))
        current_app.sphirewall().general().configuration("cloud:key", text_value("device_key"))
        flash("Saved cloud configuration")

    device_id = current_app.sphirewall().general().configuration("cloud:deviceid")
    device_key = current_app.sphirewall().general().configuration("cloud:key")
    cloud_connected = current_app.sphirewall().general().cloud_connected()

    return render_template(
        "device_configuration_cloud.html", device_id=device_id, device_key=device_key, cloud_connected=cloud_connected
    )

