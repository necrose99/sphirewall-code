from flask import request, session, redirect, flash, current_app
import flask
from flask.ext.login import current_user, login_required, logout_user
from sphirewallapi.sphirewall_api import SessionTimeoutException, MethodNotFoundException, MissingParamException, TransportProviderException, GeneralApplicationException
from werkzeug.wrappers import Response
from sphirewallconnector import navigator, resources, app


def standard_error_handler(error):
    flash(str(error))
    return redirect("/login?error=" + str(error))


@app.errorhandler(SessionTimeoutException)
def on_error_session_timeout(e):
    logout_user()
    session.clear()
    return redirect("/")


@app.errorhandler(500)
def on_error(error):
    return standard_error_handler(error)


@app.errorhandler(MethodNotFoundException)
def on_error_method_not_found(e):
    return standard_error_handler("Api method '%s' could not be found, your device may be out of date, please upgrade." % e.message)


@app.errorhandler(MissingParamException)
def on_error_param_not_found(e):
    return standard_error_handler("Api parameter '%s' was not found, this can occur if the api library is out of date. " % e.message)


@app.errorhandler(GeneralApplicationException)
def on_error_param_not_found(e):
    return standard_error_handler("A general exception occurred, '%s'. " % e.message)


@app.errorhandler(TransportProviderException)
def on_error_param_not_found(e):
    return standard_error_handler("Could not connect to device")


@app.before_request
def before_routes():
    flask.g.resources = resources

    if current_user.is_authenticated():
        flask.g.navigator = navigator

@app.route("/")
@login_required
def home():
    flash("Welcome, you were logged in successfully")
    return redirect("/device")

