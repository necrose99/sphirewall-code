function createCookie(name, value, days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        var expires = "; expires=" + date.toGMTString();
    } else var expires = "";
    document.cookie = escape(name) + "=" + escape(value) + expires + "; path=/";
}

function adjustDates() {
    createCookie("startDate", $("#datepickerStart").val(), 1)
    createCookie("endDate", $("#datepickerEnd").val(), 1)
}

function inject_date_picker(target_id, start_date, end_date) {
    var date_picker_markup = '';
    date_picker_markup += '<form method=GET style="display:inline-block;">';
    date_picker_markup += '<input type="text" id="datepickerStart" name="startDate" value="'+start_date+'" size="15"/>';
    date_picker_markup += '<input type="text" id="datepickerEnd" name="endDate" value="'+end_date+'" size="15"/>';
    date_picker_markup += '<input onclick="adjustDates(); submit();" type=submit value=Refresh>';
    date_picker_markup += '</form>';

    $("#" + target_id).html(date_picker_markup);

    jQuery.datepicker.setDefaults({autoSize: true, dateFormat: "yy-mm-dd"});
    jQuery("#datepickerStart").datepicker({});
    jQuery("#datepickerEnd").datepicker({});
    jQuery("#datepickerStart, #datepickerEnd").datepicker();
}
