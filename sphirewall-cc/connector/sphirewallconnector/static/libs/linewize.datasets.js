function DatasetManager() {
    this.dataset_results = {};
    this.datasets = [];
    this.counter = 0;
    this.pre_processdata = function () {
    };
    this.post_processdata = function () {
    };
}

DatasetManager.prototype.register_dataset = function (url, callback_method) {
    this.datasets.push({url: url, callback: callback_method});
};

DatasetManager.prototype.pre_processdata = function () {

};

DatasetManager.prototype.post_processdata = function () {

};

DatasetManager.prototype.done = function () {
    this.pre_processdata();

    for (var i = 0; i < this.datasets.length; i++) {
        var dataset_object = this.datasets[i];
        dataset_object.callback(this.dataset_results[dataset_object.url]);
    }

    this.post_processdata();
};

DatasetManager.prototype.process = function () {
    var dataset_manager = this;
    for (var i = 0; i < dataset_manager.datasets.length; i++) {
        var dataset_object = this.datasets[i];

        function get(object) {
            $.getJSON(object.url, function (data) {
                dataset_manager.dataset_results[object.url] = data;
                dataset_manager.counter += 1;

                if (dataset_manager.counter == dataset_manager.datasets.length) {
                    dataset_manager.done();
                }
            });
        }

        get(dataset_object);
    }
};
