/* This guy has some serious scope issues, but its the foundation
 * for some interesting work on the chosen component.
 *
 * Currently limited to use only on the main page
 * */

function AmazingSelector(render_id, chosen_id) {
    this.render_id = render_id;
    this.chosen_id = chosen_id;
}

AmazingSelector.prototype.open_selector = function () {
    $("#" + this.render_id).toggle();
    $(".aw_category_item_block").hide();
    $("#unknown").show();
};

AmazingSelector.prototype.select_this = function (type) {
    var $chosen_selector = $("#" + this.chosen_id);
    var already_selected = $chosen_selector.val();
    if (already_selected == null) {
        already_selected = [];
    }

    var already_exists = $.inArray(type, already_selected);
    if (already_exists != -1) {
        already_selected.splice(already_exists, 1);
    } else {
        already_selected.push(type);
    }

    $chosen_selector.val(already_selected);
    $chosen_selector.trigger("chosen:updated");

    this.setup_selections();
};

AmazingSelector.prototype.setup_selections = function () {
    var super_this = this;
    $.each($(".aw_category_all_item_block_link"), function (i, target) {
        var data_for = $(target).attr("data-for");
        if ($.inArray(data_for, $("#" + super_this.chosen_id).val()) != -1) {
            $.each($(target).parent().children(), function (d, t) {
                if (target != t) {
                    $(t).addClass("aw_disabled_option");
                }
            });
        } else {
            $.each($(target).parent().children(), function (d, t) {
                if (target != t) {
                    $(t).removeClass("aw_disabled_option");
                }
            });
        }
    });


    $.each($(".aw_category_item_block_link"), function (i, target) {
        var data_for = $(target).attr("data-for");
        if ($.inArray(data_for, $("#" + super_this.chosen_id).val()) != -1) {
            $(target).addClass("aw_selected_option");
        } else {
            $(target).removeClass("aw_selected_option");
        }
    });
};

AmazingSelector.prototype.init = function () {
    var super_this = this;
    $(".aw_category").click(function () {
        $(".aw_category").removeClass("aw_selected_option");
        var for_value = $(this).attr("data-for");
        $(".aw_category_item_block").hide();
        $("#" + for_value).show();
        $(this).addClass("aw_selected_option");
    });

    $(".aw_category_item_block_link").click(function () {
        super_this.select_this($(this).attr("data-for"));
    });

    this.setup_selections();
};

AmazingSelector.prototype.add_category = function (category_id, category_name) {
    var id = category_id.replace(/\./g, "_");
    if ($("#category_" + id).length == 0) {
        var row = "<div class='aw_category' id='category_" + id + "' data-for='" + id + "'>" + category_name + "</div>";
        $("#signature_selector_menu").append(row);
    }

    if ($("#" + id).length == 0) {
        var content_block = "<div class='aw_category_item_block' id='" + id + "'></div>";
        $("#signature_selector_content").append(content_block);
    }

};

AmazingSelector.prototype.add_option = function (category, option_key, option_name) {
    var id = category.replace(/\./g, "_");
    $("#" + id).append("<a class='aw_category_item_block_link' href='#' data-for='" + option_key + "'>" + option_name + "</a>");
};

AmazingSelector.prototype.add_top_option = function (category, option_key, option_name) {
    var id = category.replace(/\./g, "_");
    $("#" + id).append("<a class='aw_category_all_item_block_link aw_category_item_block_link' href='#' data-for='" + option_key + "'>" + option_name + "</a>");
};


