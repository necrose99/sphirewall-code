from flask import flash, current_app
from flask.ext.login import login_required

from flask.globals import request
from flask.templating import render_template
from sphirewallconnector import app
from sphirewallconnector.api.utils import checkbox_value, text_value


def format_list(list):
    result = ""
    for element in list:
        result += element + ", "
    return result.rstrip(", ")


def get_name_by_id(object_id):
    periods = current_app.sphirewall().firewall().periods()
    pools = current_app.sphirewall().firewall().aliases()
    groups = current_app.sphirewall().general().groups()
    for period in periods:
        if period['id'] == object_id:
            return period['name']
    for pool in pools:
        if pool['id'] == object_id:
            return pool['name']
    for group in groups:
        if group['id'] == object_id:
            return group['name']
    return ""


@login_required
@app.route("/device/firewall/filtering", methods=["GET", "POST"])
def firewall_acls():
    if request.method == "POST":
        current_app.sphirewall().general().advanced("DEFAULT_VERDICT_DENY", 1 if checkbox_value("default_deny") else 0)
        current_app.sphirewall().general().advanced("ENABLE_FIREWALL_ALLOW_LOCAL_OUTBOUND", 1 if checkbox_value("allow_local") else 0)
        flash("Saved firewall filtering configuration")

    return render_template(
        "device_firewall_filtering.html", default_deny=current_app.sphirewall().general().advanced_value("DEFAULT_VERDICT_DENY") == 1,
        allow_local=current_app.sphirewall().general().advanced_value("ENABLE_FIREWALL_ALLOW_LOCAL_OUTBOUND") == 1
    )


@login_required
@app.route("/device/firewall/masquerading", methods=["POST", "GET"])
def firewall_masquerading():
    if request.method == "POST":
        flash("Saved WAN configuration")
        mode = int(text_value("mode"))
        if mode == 0:
            interface = text_value("interface")
            current_app.sphirewall().firewall().autowan_set(mode, interface)
        else:
            current_app.sphirewall().firewall().autowan_set(mode)

    autowan = current_app.sphirewall().firewall().autowan()
    autowan["device_configuration"] = autowan.get("interfaces")

    return render_template("device_firewall_masquerading.html", autowan=autowan, devices=current_app.sphirewall().network().devices())
