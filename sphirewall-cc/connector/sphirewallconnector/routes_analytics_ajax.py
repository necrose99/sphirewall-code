from datetime import datetime
import json
from flask import session, jsonify, current_app
from flask.ext.login import login_required, current_user
from sphirewallconnector import app
from sphirewallconnector.api.utils import arg


@app.route("/device/analytics/metrics")
@login_required
def analytics_metrics_all():
    return jsonify(result=current_app.sphirewall().general().metrics())


@app.route("/device/analytics/metrics/<metric>")
@login_required
def analytics_metrics(metric):
    return jsonify(result=current_app.sphirewall().general().metrics_spec(metric))



@app.route("/device/analytics/connections")
@login_required
def analytics_connections():
    transfer = current_app.sphirewall().firewall().connections_list()
    return jsonify(dict(result=transfer))


@app.route("/device/analytics/events")
@login_required
def analytics_events():
    provider__events = current_app.sphirewall().general().events_list()
    for event in provider__events:
        #Do translation if required:
        if "message" not in event:
            event["message"] = json.dumps(event["params"])
            event["time"] = datetime.fromtimestamp(event["time"]).strftime('%Y-%m-%d %H:%M:%S')

        event["message_formatted"] = ""
        for param in json.loads(event["message"]):
            for key, value in param.iteritems():
                message_string = "%s: '%s', " % (key, value)
                event["message_formatted"] += message_string

    return jsonify(result=provider__events)


@app.route("/device/analytics/clients")
@login_required
def analytics_clients():
    general__hosts_list = current_app.sphirewall().general().hosts_list()
    users_list = current_app.sphirewall().general().active_users_list()

    return jsonify(result=dict(users=users_list, hosts=general__hosts_list))


@app.route("/device/analytics/logs")
@login_required
def analytics_logs():
    log = current_app.sphirewall().general().logs(filter=arg("filter"))
    return jsonify(result=log)

