import logging
import uuid
from flask import current_app

from flask.app import Flask
import os
from flask.ext.login import LoginManager

app = Flask(__name__)
app.secret_key = str("testin")

if app.config.get("IMPORT_PATH"):
    import sys
    sys.path.append(app.config["IMPORT_PATH"])

login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = "login"
logging.basicConfig(level=logging.DEBUG)

class ResourceManager:
    def __init__(self):
        self.css_resources = []
        self.css_linked_resources = []
        self.js_resources = []

class NavigationManager:
    def __init__(self):
        self.registered_structure = []

    def register_section(self, id, name, default_path):
        self.registered_structure.append({"id": id, "name": name, "default_path": default_path, "children": []})

    def register_section_child(self, section_id, id, path, name):
        for section in self.registered_structure:
            if section["id"] == section_id:
                section["children"].append({"id": id, "path": path, "name": name})

navigator = NavigationManager()
navigator.register_section("status", "Status", "/device")
navigator.register_section_child("status", "dashboard", "/device", "Dashboard")
navigator.register_section_child("status", "metrics", "/device/status/metrics", "Performance")
navigator.register_section_child("status", "events", "/device/status/events", "Events and Alerts")
navigator.register_section_child("status", "logs", "/device/status/logs", "System Logs")
navigator.register_section_child("status", "update", "/device/status/update", "Update")

navigator.register_section("configure", "Configuration", "/device/network/devices")
navigator.register_section_child("configure", "cloud", "/device/configuration/cloud", "Cloud Connection")
navigator.register_section_child("configure", "devices", "/device/network/devices", "Network")
navigator.register_section_child("configure", "wan", "/device/firewall/masquerading", "Wan Config")
navigator.register_section_child("configure", "firewall", "/device/firewall/filtering", "Firewall Filtering")
navigator.register_section_child("configure", "users", "/device/configuration/users", "Users and People")
navigator.register_section_child("configure", "groups", "/device/configuration/groups", "Groups and Roles")
navigator.register_section_child("configure", "advanced", "/device/configuration/advanced", "Advanced Config")

navigator.register_section("tools", "Tools", "/device/tools/ping")
navigator.register_section_child("tools", "ping", "/device/tools/ping", "Ping")
navigator.register_section_child("tools", "dig", "/device/tools/dig", "Dns Lookup")
navigator.register_section_child("tools", "traceroute", "/device/tools/traceroute", "Traceroute")
navigator.register_section_child("tools", "capture", "/device/tools/capture", "Packet Capture")

resources = ResourceManager()
resources.js_resources.append("libs/jquery-1.10.2.js")
resources.js_resources.append("libs/jquery-migrate-1.2.1.js")
resources.js_resources.append("libs/jquery-migrate-1.2.1.js")
resources.js_resources.append("libs/jquery-migrate-1.2.1.js")
resources.js_resources.append("libs/jquery-ui-1.10.4.custom.min.js")
resources.js_resources.append("libs/jquery.dataTables.nightly.js")
resources.js_resources.append("libs/jquery.flot.js")
resources.js_resources.append("libs/jquery.flot.tooltip.js")
resources.js_resources.append("libs/jquery.flot.time.js")
resources.js_resources.append("libs/date.js")
resources.js_resources.append("libs/sphirewall.js")
resources.js_resources.append("libs/jquery.validate.js")
resources.js_resources.append("libs/additional-methods.js")
resources.js_resources.append("libs/TableTools.min.js")
resources.js_resources.append("libs/jquery.flot.pie.js")
resources.js_resources.append("libs/chosen.jquery.min.js")
resources.js_resources.append("libs/protocols.js")
resources.js_resources.append("libs/jquery.templates.js")
resources.js_resources.append("libs/linewize.awesometable.js")
resources.js_resources.append("libs/linewize.dateselector.js")
resources.js_resources.append("libs/linewize.amazingselector.js")
resources.js_resources.append("libs/jquery.tooltipster.js")
resources.js_resources.append("libs/bootstrap.js")
resources.js_resources.append("libs/linewize.datasets.js")

resources.css_resources.append("style/style.css")
resources.css_resources.append("style/sphirewall_style.css")
resources.css_resources.append("style/bootstrap.css")
resources.css_resources.append("style/tables.css")
resources.css_resources.append("style/navigation.css")
resources.css_resources.append("style/jquery-ui-1.10.4.custom.css")
resources.css_resources.append("style/TableTools.css")
resources.css_resources.append("style/TableTools_JUI.css")
resources.css_resources.append("style/chosen.min.css")
resources.css_resources.append("style/linewize.amazingselector.css")
resources.css_resources.append("style/tooltipster.css")
resources.css_linked_resources.append("//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css")

with app.test_request_context():
    from sphirewallconnector.middleware.service import get_sphirewall_direct
    current_app.sphirewall = get_sphirewall_direct

    from sphirewallconnector import routes_base
    from sphirewallconnector import routes_login
    from sphirewallconnector import routes_device_configuration
    from sphirewallconnector import routes_device_firewall
    from sphirewallconnector import routes_device_network
    from sphirewallconnector import routes_device_status
    from sphirewallconnector import routes_analytics_ajax
    from sphirewallconnector import routes_device_tools
