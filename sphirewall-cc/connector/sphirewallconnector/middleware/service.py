import json
from flask import current_app
from sphirewallapi.sphirewall_api import SphirewallClient, SphirewallSocketTransportProvider
from sphirewallconnector.routes_login import get_session_sphirewall_hostname, get_session_sphirewall_port, get_session_sphirewall_token


def get_sphirewall_direct():
    transport_provider = SphirewallSocketTransportProvider(get_session_sphirewall_hostname(), get_session_sphirewall_port())
    client = SphirewallClient(transport_provider, access_token=get_session_sphirewall_token())
    return client
