from setuptools import setup

setup(
    name='sautorun',
    version='1.0.2.4',
    py_modules=['sautorun'],
    install_requires=[
	'pyudev', 'scli2'
    ],
    entry_points='''
        [console_scripts]
        sautorun-runner=sautorun:main
    ''',
)
