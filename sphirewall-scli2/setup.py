from setuptools import setup

setup(
    name='scli2',
    version='1.0.2.4',
    py_modules=['scli2'],
    install_requires=[
        'Click', 'sphirewallapi'
    ],
    entry_points='''
        [console_scripts]
        scli2=scli2:cli
    ''',
)

