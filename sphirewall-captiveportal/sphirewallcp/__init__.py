import logging
import uuid

from flask.app import Flask
import os

app = Flask(__name__)
app.secret_key = str(uuid.uuid4())

default_config = os.path.join(app.root_path, "/etc/sphirewall_captiveportal.conf")
if os.path.exists(default_config):
    app.config.from_pyfile(default_config)
else:
    app.config["SPHIREWALL_API_HOSTNAME"] = "localhost"
    app.config["SPHIREWALL_API_PORT"] = 8001

logging.basicConfig(level=logging.DEBUG)

with app.test_request_context():
    from sphirewallcp import routes_captiveportal

