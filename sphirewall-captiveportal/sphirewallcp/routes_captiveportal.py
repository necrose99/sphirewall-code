from flask import request, flash, render_template, redirect
from sphirewallapi.sphirewall_api import SphirewallClient
from sphirewallapi.sphirewall_connection import SphirewallSocketTransportProvider
from sphirewallcp import app
from sphirewallcp.api.utils import text_value, arg


def get_sphirewall_client():
    client = SphirewallClient(
        SphirewallSocketTransportProvider(app.config["SPHIREWALL_API_HOSTNAME"], app.config["SPHIREWALL_API_PORT"])
    )
    return client


@app.route("/login", methods=["GET", "POST"])
def captive_portal_authenticate():
    if request.method == "POST":
        try:
            response = get_sphirewall_client().authenticate(text_value("username"), text_value("password"), arg("ip"), mac=None)
            if response.get("response") == -1:
                flash("Login failed because '%s'" % response.get("message"))
            else:
                return redirect("/login/success?host=%s&ip=%s" % (arg("url"), arg("ip")))
        except Exception as exc:
            print "exc: %s" % str(exc)
            flash("Login failed due to an internal server error")
    return render_template("captive_portal_login.html")


@app.route("/login/success")
def captive_portal_success():
    return render_template("captive_portal_login_success.html", url=arg("host"), ip=arg("ip"))


@app.route("/logout", methods=["GET"])
def captive_portal_logout():
    get_sphirewall_client().general().disconnect_session(arg("ip"))
    return render_template("captive_portal_logout.html")


@app.route("/blocked")
def blocked_page():
    return render_template("blocked_page.html")

