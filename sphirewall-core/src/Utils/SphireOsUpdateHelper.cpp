#include <iostream>

#include "Utils/SphireOsUpdateHelper.h"

SphireOsUpdateHelper::SphireOsUpdateHelper(){
	this->process = NULL;
}

std::string SphireOsUpdateHelper::get_log(){
	if(process){
		return process->get_output();
	}

	return "";
}

void SphireOsUpdateHelper::start_update(){
        if(process){
		delete process;
	}

	process = new DetachedProcessWithOutput();
	process->set_command("sphireosctl updates update");
	process->start();
}

int SphireOsUpdateHelper::status(){
	if(process && !process->status()){
		return UPDATE_FINISHED;
	}else if(process){
		return UPDATE_RUNNING;
	}
	return UPDATE_NOT_RUNNING;
}


