/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DHCP_H
#define DHCP_H

enum {
        DHCPDISCOVER = 1,
        DHCPOFFER = 2,
        DHCPREQUEST = 3,

        DHCPACK =5,
        DHCPNAK = 6,
        DHCPDECLINE = 4,
        DHCPRELEASE = 7,
        DHCPINFORM = 8
};

enum {
        DHCP_OPTION_SUBNET_MASK = 1,
        DHCP_OPTION_ROUTER = 3,
        DHCP_OPTION_DNS = 6,
        DHCP_OPTION_BROADCAST = 28,
        DHCP_OPTION_REQUEST_CLIENT_IP = 50,
        DHCP_OPTION_LEASE_TIME = 51,
        DHCP_OPTION_MESSAGE_TYPE = 53,
        DHCP_OPTION_SERVER_IP = 54,
        DHCP_OPTION_PARAM_REQUEST_LIST = 55,

	DHCP_OPTION_TFTP_SERVER=66,
	DHCP_OPTION_TFTP_FILENAME=67
};

enum {
	CLIENT=1,
	SERVER=2,
	SERVER_TO_SERVER=3
};

struct dhcp_packet {
        u_int8_t op;
        u_int8_t htype;
        u_int8_t hlen;
        u_int8_t hops;
        u_int32_t xid;
        u_int16_t secs;
        u_int16_t flags;

        u_int32_t ciaddr;
        u_int32_t yiaddr;
        u_int32_t siaddr;
        u_int32_t giaddr;

        char chaddr[16];
        char sname[64];
        char file[128];
};

struct dhcp_packet_option {
        u_int8_t len;
        char* data;
};

struct dhcp_builder_context {
        struct dhcp_packet header;
        struct dhcp_packet_option options[255];
};

int dhcp_open_raw_socket(int ifindex);
int dhcp_udp_send(const char* ifname, unsigned char* data, unsigned int len, int mode=CLIENT, unsigned int source = INADDR_ANY, unsigned int destination=INADDR_BROADCAST);
int dhcp_udp_recv(int sd, unsigned char* data, unsigned int buffer_len, int mode=CLIENT);
struct dhcp_builder_context* dhcp_init_context();
void dhcp_free_context(struct dhcp_builder_context* context);
void dhcp_set_blob_options(struct dhcp_builder_context* context, u_int8_t type, char* blob, u_int8_t size);
void dhcp_set_int8_options(struct dhcp_builder_context* context, u_int8_t type, u_int8_t value);
void dhcp_set_int32_options(struct dhcp_builder_context* context, u_int8_t type, u_int32_t value);
int dhcp_get_int8_option(struct dhcp_builder_context* context, u_int8_t type, u_int8_t* value);
int dhcp_get_int32_option(struct dhcp_builder_context* context, u_int8_t type, unsigned int* value);
int dhcp_serialize(struct dhcp_builder_context* context, char* buffer, size_t len);
int dhcp_deserialize(struct dhcp_builder_context* context, char* buffer, size_t len);

#endif
