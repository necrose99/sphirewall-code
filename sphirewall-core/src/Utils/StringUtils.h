/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SPHIREWALL_STRING_UTILS_H_INCLUDED 
#define SPHIREWALL_STRING_UTILS_H_INCLUDED

#include <vector>
#include <map>

class StringUtils {
public:
	static std::string genRandom();

	static std::string trim(std::string buf); // Trims spaces, tabs, and removes all new lines.
	static std::string trim(std::string buf, std::string trimChars); // Custom trim chars + removes "\n".

	static std::string removeArrayTerminator(std::string str);
	static int strUniqHash(std::string, int);

	static std::string uintToString(unsigned int i);
	static std::string longToString(long long i);
	static std::string ulongToString(unsigned long i);
	static std::string replaceUnprintableChars(const std::string str, const char replacement);
	static char* base64(const unsigned char *input, int len);
	static std::string hexdigest(const unsigned char *input, size_t size);
	static std::string normalize_mac_address(std::string mac);
	static std::string to_lower(std::string input);
};

void split(const std::string &s, char delim, std::vector<std::string> &elems);
std::string intToString(int i);

#endif
