#ifndef PACKET_CAPTURE_H
#define PACKET_CAPTURE_H

#include <boost/shared_ptr.hpp>
#include <list>
#include <pcap.h>
#include "Auth/UserDb.h"
#include "SFwallCore/Packet.h"

struct ethhdr {
    unsigned char	h_dest[6];	/* destination eth addr	*/
    unsigned char	h_source[6];	/* source ether addr	*/
    unsigned short	h_proto;		/* packet type ID field	*/
};


class CapturedPacket {
	public:
		CapturedPacket(const struct pcap_pkthdr *header, const unsigned char* packet);
		CapturedPacket(SFwallCore::Packet* packet);
		~CapturedPacket();

		struct pcap_pkthdr *header;
		u_char *packet;

		std::string to_string();
	private:
		std::string __ipv4_to_string();
		std::string __ipv6_to_string();

};

enum TargetedCaptureType {
	NONE = 0,
	MAC = 1,
	USER = 2,
	PCAP = 3
};

typedef boost::shared_ptr<CapturedPacket> CapturedPacketPtr;

class PacketCaptureEngine {
	public:
		PacketCaptureEngine();	

		void start_capture();
		void stop_capture();

                std::string capture_filter;
                std::string capture_interface;
                TargetedCaptureType capture_type = NONE;
                UserPtr user_target;
                std::string hw;

		void handle_packet_input(const struct pcap_pkthdr *header, const unsigned char* packet);
		void handle_packet_capture_input(SFwallCore::Packet* packet);

		int no_packets_captured(){
			return captured_packets.size();
		}

		bool is_running(){
			return running || inline_capture_enabled;
		}

		list<CapturedPacketPtr> get_captured_packets(){
			return captured_packets;
		}

		std::string get_base64_encoded_pcap_string();

		static int CAPTURE_LIMIT;
	private:
		list<CapturedPacketPtr> captured_packets;
		bool inline_capture_enabled;

		void do_start_capture();
		bool running;
		pcap_t *handle;
};

#endif
