/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef LINUX_UTILS
#define LINUX_UTILS

class LinuxUtils {
	public:
		static int execWithNoFds(std::string command);
		static int exec(std::string command, std::string &ret);
};

class DetachedProcessWithOutput {
        public:
                void set_command(std::string command);
                void start();
                bool status();
                std::string get_output();
        private:
                void __internal_runner();
                std::string command;
                std::string output;
                bool running;
};

#endif
