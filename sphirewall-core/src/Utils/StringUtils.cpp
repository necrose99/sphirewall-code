/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <uuid/uuid.h>
#include <sstream>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <iomanip>
#include <crypt.h>
#include <openssl/sha.h>
#include <boost/random/random_device.hpp>
#include <boost/random/uniform_int_distribution.hpp>

#include <openssl/sha.h>
#include <openssl/hmac.h>
#include <openssl/evp.h>
#include <openssl/bio.h>
#include <openssl/buffer.h>

using namespace std;
using std::string;

#include "Utils/StringUtils.h"

void split(const std::string &s, char delim, std::vector<std::string> &elems) {
	std::stringstream ss(s);
	std::string item = "";

	while (std::getline(ss, item, delim)) {
		elems.push_back(item);
	}
}

string intToString(int i) {
	std::string s;
	std::stringstream out;
	out << i;
	s = out.str();

	return s;
}

string StringUtils::genRandom() {
	string ret;

	uuid_t t;
	uuid_generate(t);

	char* o = (char*) calloc(256, sizeof (char));
	uuid_unparse(t, o);

	for (int n = 0; o[n] != '\0' && n < 256; n++) {
		ret += o[n];
	}

	free(o);
	return ret;
}

string StringUtils::removeArrayTerminator(string str) {

	int pos = str.find('\0');
	while (pos != -1) {
		str.erase(pos);
		pos = str.find('\0');
	}

	return str;
}

string StringUtils::trim(string str, string trimChars) {
	//Trim Both leading and trailing spaces  
	size_t startpos = str.find_first_not_of(trimChars); // Find the first character position after excluding leading blank spaces  
	size_t endpos = str.find_last_not_of(trimChars); // Find the first character position from reverse af  

	// if all spaces or empty return an empty string  
	if ((string::npos == startpos) || (string::npos == endpos)) {
		str = "";
	} else
		str = str.substr(startpos, endpos - startpos + 1);

	//Find a newline
	int nPos = str.find("\n");
	while (nPos != -1) {
		str.erase(nPos, 1);
		nPos = str.find("\n");
	}

        nPos = str.find("\r");
        while (nPos != -1) {
                str.erase(nPos, 1);
                nPos = str.find("\r");
        }

	string ret = str;
	return ret;
}

string StringUtils::trim(string str) {
	return trim(str, " \t");
}

string StringUtils::uintToString(unsigned int i) {
	std::string s;
	std::stringstream out;
	out << i;
	s = out.str();

	return s;
}

string StringUtils::ulongToString(unsigned long i) {
	std::string s;
	std::stringstream out;
	out << i;
	s = out.str();

	return s;
}

string StringUtils::longToString(long long i) {
	std::string s;
	std::stringstream out;
	out << i;
	s = out.str();

	return s;
}

int StringUtils::strUniqHash(string str, int luckNo) {
	/*Awesome hashing function*/

	unsigned int b = time(NULL) - 100000;
	unsigned int a = luckNo;
	unsigned int hash = 10;

	for (std::size_t i = 0; i < str.length(); i++) {
		hash = hash * a + str[i];
		a = a * b;
	}

	return hash / 1000;

}

string StringUtils::replaceUnprintableChars(const string str,
		const char replacement) {
	const char lowestPrintableChar = ' ';
	const char highestPrintableChar = '~';

	string results = "";

	for (unsigned int index = 0; index < str.size(); index++) { // Search for non-printable characters and replace them if found.
		if ((str[index] < lowestPrintableChar) ||
				(str[index] > highestPrintableChar)) { // Replace non-printable character.
			results.push_back(replacement);
		} else { // Printable character so just copy it to results.
			results.push_back(str[index]);
		}
	} // End for index < str.size().

	return results;
}

char *StringUtils::base64(const unsigned char *input, int length)
{
        BIO *bmem, *b64;
        BUF_MEM *bptr;

        b64 = BIO_new(BIO_f_base64());
        bmem = BIO_new(BIO_s_mem());
        b64 = BIO_push(b64, bmem);
        BIO_write(b64, input, length);
        BIO_flush(b64);
        BIO_get_mem_ptr(b64, &bptr);

        char *buff = (char *)malloc(bptr->length);
        memcpy(buff, bptr->data, bptr->length-1);
        buff[bptr->length-1] = 0;

        BIO_free_all(b64);

        return buff;
}

std::string StringUtils::hexdigest(const unsigned char *d, size_t len) {
        std::stringstream hexStringStream;

        hexStringStream << std::hex << std::setfill('0');

        for (size_t i = 0; i < len; i++) {
                hexStringStream << std::setw(2) << (int)d[i];
        }

        return hexStringStream.str();
}

#include <algorithm>
std::string StringUtils::normalize_mac_address(std::string mac){
	std::transform(mac.begin(), mac.end(), mac.begin(), ::tolower);
	for(int x = 0; x < mac.size(); x++){
		if(mac[x] == '-'){
			mac[x] = ':';
		}
	}
	return mac;
}

#include <utility>
std::string StringUtils::to_lower(std::string input){
	std::transform(input.begin(), input.end(), input.begin(), ::tolower);
	return input;
}
