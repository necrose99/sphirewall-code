/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SPHIREWALL_TIMEWRAPPER_H_INCLUDED
#define SPHIREWALL_TIMEWRAPPER_H_INCLUDED

#include <bits/types.h>
#include <ctime>
#include <time.h>
#include <iostream>
#include <sys/time.h>

class Time {
public:
	Time(){}
	Time(int timestamp);
	Time(std::string input);
	Time(std::string input, const char* f);
	std::string format(const char* f);

	int timestamp() const {
		return t;
	}

	//Helper methods:
	std::string midnight();
	std::string last();
	std::string hour();
	std::string day();

	int extractHour();
	int extractMins();
	int dayOfYear();		

	void set_hours(int hours);
	void set_minutes(int hours);
	void set_24h_time(int time);
	void add_days(int days);

	static Time* startOfWeek();
	static Time* startOfMonth();
	static Time* yesterday();

	static Time* startOfWeek(int base);
	static Time* startOfMonth(int base);
	static Time* yesterday(int base);
		
	static Time* getNextDayOfMonth(int base, int day);
	static Time* getLastDayOfMonth(int base, int day);

	static int dayDiff(Time*, Time*);
	static Time* addDays(int base, int days);	
	static Time* nextDays(int base, int wday);	

	int daysLeftInMonth();
	int wday();
	int month();
private:
	time_t t;
};

/**
 *This class is a wrapper aroud system miliseconds time with
 * convinient functions
 */
class TimeWrapper {
public:
	TimeWrapper();
	static TimeWrapper now();
	TimeWrapper(const struct timeval& time);
	__ULONGWORD_TYPE getInMiliseconds() const;
	static const unsigned int MILISECONDS_IN_SECOND = 1000;
	static const unsigned int MICROSECONDS_IN_MILISECOND = 1000;

private:
	struct timeval theTime;
};

#endif
