/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <unistd.h>
#include <unistd.h>
#include <signal.h>
#include "Utils/LinuxUtils.h"
#include <boost/thread.hpp>

using namespace std;

void DetachedProcessWithOutput::set_command(std::string command){
	this->command = command;
}

void DetachedProcessWithOutput::start(){
	boost::thread(boost::bind(&DetachedProcessWithOutput::__internal_runner, this));
}

bool DetachedProcessWithOutput::status(){
	return running;
}	

std::string DetachedProcessWithOutput::get_output(){
	return output;
}

void DetachedProcessWithOutput::__internal_runner(){
	running = true;
	char cbuf[1024];

	FILE *fp;
	fp = popen(command.c_str(), "r");
	if(!fp){
		running = false;
	}

	while (fgets(cbuf, 1024, fp) != NULL){
		output += cbuf;
	}
	int exitCode = pclose(fp);
	running = false;
}

int LinuxUtils::exec(string command, string &ret) {
	string sbuf;

	char cbuf[1024];

	FILE *fp;
	fp = popen(command.c_str(), "r");
	if(!fp){
		return -1;
	}

	while (fgets(cbuf, 1024, fp) != NULL)
		sbuf += cbuf;

	int exitCode = pclose(fp);
	ret = sbuf;
	return exitCode;
}

int LinuxUtils::execWithNoFds(std::string s) {
	signal(SIGCHLD, SIG_IGN);
	if (!fork()) {
		int fd_limit = sysconf(_SC_OPEN_MAX);

		for (int x = 3; x < fd_limit; x++) {
			close(x);
		}

		if (system(s.c_str()));

		exit(-1);
	}

	return 0;
}

