/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef MDNS_FORWARDER_H
#define MDNS_FORWARDER_H

#include <list>
#include <set>
#include <boost/shared_ptr.hpp>

#include "Utils/Interfaces.h"
#include "Core/Lockable.h"

class MdnsForwarderBridge {
	public:
		std::string id;
		std::string name;
		list<InterfacePtr> resolved_members;
		list<std::string> members;

		bool should_forward(InterfacePtr interface){
			for(InterfacePtr target : resolved_members){
				if(target->ifid == interface->ifid){
					return true;
				}
			}
			return false;
		}
};

typedef boost::shared_ptr<MdnsForwarderBridge> MdnsForwarderBridgePtr;

class MdnsForwarder : public IntMgrChangeListener, public Configurable, public Lockable {
	public:
		MdnsForwarder(IntMgr* interface_manager) : Lockable(){
			this->interface_manager = interface_manager;
		}

		void save();
		bool load();
		const char* getConfigurationSystemName(){
			return "MdnsForwarder";
		}

		void interface_change(InterfacePtr interface){
			for(MdnsForwarderBridgePtr rule : get_rules()){
				refresh_rule(rule);
			}
		}		

		void run();
		std::list<MdnsForwarderBridgePtr> get_rules() {
			holdLock();
			list<MdnsForwarderBridgePtr> temp_rules = rules;
			releaseLock();
			return temp_rules;
		}		

		MdnsForwarderBridgePtr get_rule_by_id(std::string id){
			for(MdnsForwarderBridgePtr rule : get_rules()){
				if(rule->id.compare(id) == 0){
					return rule;
				}
			}
			return MdnsForwarderBridgePtr();
		}

		void del_rule(MdnsForwarderBridgePtr rule){
			holdLock();
			std::list<MdnsForwarderBridgePtr>::iterator iter;
			for(iter = rules.begin(); iter != rules.end(); iter++){
				if(rule->id.compare((*iter)->id) == 0){
					rules.erase(iter);
					break;
				}
			}	
			releaseLock();
		}

		void add_rule(MdnsForwarderBridgePtr rule){
			holdLock();
			rules.push_back(rule);
			releaseLock();
		}

		bool has_registered_listener(InterfacePtr interface){
			holdLock();
			bool result = active_listeners.find(interface->name) != active_listeners.end();
			releaseLock();
			return result;
		}

		void register_listener(InterfacePtr interface){
			holdLock();
			active_listeners.insert(interface->name);			
			releaseLock();
		}

		void unregister_listener(InterfacePtr interface){
			holdLock();
			active_listeners.erase(active_listeners.find(interface->name));			
			releaseLock();
		}

		bool should_run_listener(InterfacePtr interface){
			for(MdnsForwarderBridgePtr rule : get_rules()){
				if(rule->should_forward(interface)){
					return true;
				}
			}

			return false;
		}

		void refresh_rule(MdnsForwarderBridgePtr rule){
			rule->resolved_members.clear();
			for(std::string i : rule->members){
				InterfacePtr interface = interface_manager->get_interface(i);
				if(interface){
					rule->resolved_members.push_back(interface);
				}
			}
		}

	private:

		void __process_dns_message(InterfacePtr listen_interface, char* packet, int n);
		void __forward_packet(const char* ifname, unsigned int source,const char* data, int len);
		void __run_listener(InterfacePtr binding_interface);

		std::list<MdnsForwarderBridgePtr> rules;
		std::set<string> active_listeners;

		IntMgr* interface_manager;
};

#endif
