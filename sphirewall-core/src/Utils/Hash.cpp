/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <string>
#include <sstream>
#include <iomanip>
#include <crypt.h>
#include <openssl/sha.h>
#include <boost/random/random_device.hpp>
#include <boost/random/uniform_int_distribution.hpp>
#include "Core/System.h"
#include "Utils/Hash.h"
#include "Utils/StringUtils.h"

std::string rd_salt() {
	std::string chars("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789./");
	std::string res;
	boost::random_device rng;
	boost::random::uniform_int_distribution<> index_dist(0, chars.length() - 1);

	for (int i = 0; i < 16; i++) {
		res += chars[index_dist(rng)];
	}

	return res;
}

static bool check_sha1(std::string check, std::string compare) {
	unsigned char md[SHA_DIGEST_LENGTH] = {0};
	std::string hex_digest;

	SHA1((const unsigned char *) check.c_str(), check.length(), (unsigned char *) md);
	hex_digest = StringUtils::hexdigest((const unsigned char *) md, SHA_DIGEST_LENGTH);
	return !hex_digest.compare(compare);
}

static bool check_crypt(std::string check, std::string compare) {
	char *r = crypt(check.c_str(), compare.c_str());
	return compare.compare(0, compare.length(), r) == 0;
}

bool Hash::check_hash(std::string check, std::string compare) {
	if (compare[0] == '$') {
		return check_crypt(check, compare);
	}
	else {
		return check_sha1(check, compare);
	}
}

string Hash::create_hash(string input) {
	return std::string(crypt(input.c_str(), ("$6$" + rd_salt()).c_str()));
}

string Hash::create_hash(string input, int size) {
	if (input.length() > (size_t) size) {
		return create_hash(input.substr(0, size - 1));
	}

	return create_hash(input);
}

bool Hash::shouldUpdate(string compare) {
	return (compare[0] == '$') ? false : true;
}
