/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * It is unlikely that you know how these work, here is the gist:
 * some of these return pointers to the next-to-parse data rather than the parsed data.
 * methods like these are the object from cstring methods, these are indended to be recursively
 * called by the DnsMessage constructor.
 *
 * Construction of DnsMessages is the opposite, do not use from cstring methods...
 */
#ifndef A_H
#define A_H
#undef CERT
#include <inttypes.h>
#include <string>
#include <list>
#include <vector>
#include <exception>
#include <netinet/in.h>

typedef uint16_t uint_be16_t;
typedef uint32_t uint_be32_t;

namespace DnsFlags {
	typedef uint Flags;
	const Flags
	DNS_QR_QUERY = 0,
	DNS_QR_RESPONSE = 1,
	DNS_OPCODE_STANDARD = 0,
	DNS_AA_AUTHORITATIVE = 1,
	DNS_AA_NON_AUTHORITATIVE = 0,
	DNS_TC_TRUNCATED = 1,
	DNS_TC_NOT_TRUNCATED = 0,
	DNS_RECURSION_DESIRED = 1,
	DNS_RECURSION_NOT_DESIRED = 0,
	DNS_RECURSION_AVAILABLE = 1,
	DNS_RECURSION_UNAVAILABLE = 0;
};


namespace DnsRecordTypes {
	typedef uint RecordType;
	const RecordType
	A = 1,        // Address
	AAAA = 28,    // Address 128
	AFSDB = 18,   // AFS DB (distributed filesystems / databases ?)
	APL = 42,     // Address Prefix List - CIDR list
	CAA = 257,    // Certification Authority
	CERT = 37,    // Certificate
	CNAME = 5,    // Canonical Name (Alias)
	DHCID = 49,   // DHCP Identifiers (DHCP FQDN Option)
	DLV = 32769,  // DNSSEC Lookaside Validation
	DNAME = 39,   // Delegated Name (Alias Classes)
	DNSKEY = 48,  // DNSSEC DNS Key Record
	DS = 43,      // DNSSEC Delegation Signer
	HIP = 55,     // Host Identity Protocol
	IPSECKEY = 45,// IPSEC Key Record
	KEY = 25,     // Key Record (limited use)
	KX = 36,      // Key Exchanger
	LOC = 29,     // Location (geographical)
	MX = 15,      // Mail Server
	NAPTR = 35,   // Naming Authority Pointer
	NS = 2,       // Delegates DNS Server
	NSEC = 47,    // DNSSEC Next Secure (proof of non-existence)
	NSEC3 = 50,   // DNSSEC NSEC v3
	NSEC3PARAM = 51, // DNSSEC NSEC v3 Parameter
	PTR = 12,     // Pointer
	RRSIG = 46,   // DNSSEC Signature
	RP = 17,      // Responsible Person (email with @=>.)
	SIG = 24,     // Signature SIG(0) TKEY
	SOA = 6,      // Start Of Authority
	SPF = 99,     // Sender Policy Framework
	SRV = 33,     // Generalized Internet Service
	SSHFP = 44,   // SSH Public key fingerprint
	TA  = 32768,  // DNSSEC Trust Authority
	TKEY = 249,   // Secret Key (TKEY has KEY)
	TLSA = 52,    // TLSA Cert Association
	TSIG = 250,   // Transaction Signature
	TXT = 16,     // Text Record
	ALL = 255,    // Everything
	AXFR = 252,   // Authoritative Zone Transfer
	IXFR = 251,   // Incremental Zone Transfer
	OPT = 41;     // EDNS Option
};

namespace DnsRecordClasses {
	typedef uint RecordClass;
	const RecordClass
	INTERNET = 0x0001, // The only one that matters
	CSNET = 0x0002,    //
	CHAOS = 0x0003,    //
	HESIOD = 0x0004,   //
	NONE = 0x00fe,     //
	ANY = 0x00ff;      //
};

namespace DnsMessageTypes {
	typedef uint MessageType;
	const MessageType
	QUERY = 0x0000,   // Query
	IQUERY = 0x0001,  // Inverse Query (obsolete)
	STATUS = 0x0002,  // Server status
	UNKNOWN = 0x0003, // Me either
	NOTIFY = 0x0004,  // For peer name servers
	UPDATE = 0x0005;  // Selective record replacement
};

namespace DnsResponseCodes {
	typedef uint ResponseCode;
	const ResponseCode
	NOERROR = 0,   // No Error
	FORMERR = 1,   // Format Error
	SERVFAIL = 2,  // Server Failure
	NXDOMAIN = 3,  // Domain does not exist
	NOTIMPL = 4,   // Not Implemented
	REFUSED = 5,   // REFUSED
	YXDOMAIN = 6,  // Domain should not exist
	YXRRSET = 7,   // Resource should not exist
	NXRRSET = 8,   // Resource should exist but doesn't
	NOTAUTH = 9,   // DNS Server is not authoritative for specified zone
	NOTZONE = 10;  // Prereq or Update contains a name not in zone
};

namespace DnsOpCodes {
	typedef uint OpCode;
	const OpCode
	QUERY = 0,   // Standard Query
	IQUERY = 1,  // Inverse Query
	STATUS = 2;  // Dns Status request
};

// Remember these are BIG ENDIAN
typedef struct dns_packet_header {
	uint_be16_t id;
	uint_be16_t flags;
	uint_be16_t nquestions;
	uint_be16_t nanswers;
	uint_be16_t nauthority;
	uint_be16_t nadditional;
} dns_packet_header_storage_t;

typedef struct dns_qfamily {
	uint_be16_t qtype;
	uint_be16_t qclass;
} dns_qfamily_storage_t;

typedef struct dns_pseudo_qname {
	struct dns_qfamily qfamily;
	char name[0];
} dns_pseudo_qname_storage_t;

// Flag extraction
int dns_ex_qr(uint_be16_t flags);
int dns_ex_opcode(uint_be16_t flags);
int dns_ex_aa(uint_be16_t flags);
int dns_ex_tc(uint_be16_t flags);
int dns_ex_rd(uint_be16_t flags);
int dns_ex_ra(uint_be16_t flags);
int dns_ex_rcode(uint_be16_t flags);

void dns_undot_name(char *name, size_t length) __attribute__((nonnull(1)));
void dns_dot_name(char *name, size_t length) __attribute__((nonnull(1)));
void charvec_append_16bit_int(std::vector<unsigned char> &vec, uint_be16_t val);
void charvec_append_16bit_int(std::vector<char> &vec, uint_be16_t val);
void charvec_append_32bit_int(std::vector<unsigned char> &vec, uint_be32_t val);
void charvec_append_32bit_int(std::vector<char> &vec, uint_be32_t val);
void charvec_append_chars(std::vector<unsigned char> &vec, const char *val, size_t length) __attribute__((nonnull(2)));
void charvec_append_chars(std::vector<unsigned char> &vec, const unsigned char *val, size_t length) __attribute__((nonnull(2)));
void charvec_append_chars(std::vector<char> &vec, const char *val, size_t length) __attribute__((nonnull(2)));
void charvec_append_dns_header(std::vector<unsigned char> &vec, const dns_packet_header_storage_t *header) __attribute__((nonnull(2)));
void charvec_append_dns_header(std::vector<char> &vec, const dns_packet_header_storage_t *header) __attribute__((nonnull(2)));

char *label_to_name(dns_packet_header_storage_t *hdr, ssize_t offset, std::string &name) __attribute__((nonnull(1)));

class DnsResourceRecord {
		std::vector<char> unknownData;
	public:
		static DnsResourceRecord *fromCString(dns_packet_header_storage_t *header, const char *start_of_record, int record_size, const DnsRecordTypes::RecordType record_type) __attribute__((nonnull(1, 2)));
		DnsResourceRecord();
		DnsResourceRecord(dns_packet_header_storage_t *header, const char *record, size_t length) __attribute__((nonnull(1, 2)));
		virtual ~DnsResourceRecord();
		virtual char *toCString(size_t *length) const;
};

class DnsARecord : public DnsResourceRecord {
		in_addr_t ipaddr;
	public:
		DnsARecord(in_addr_t ipaddr);
		DnsARecord(dns_packet_header_storage_t *header, const char *record, size_t length) __attribute__((nonnull(1, 2)));
		virtual ~DnsARecord();
		virtual char *toCString(size_t *length) const;
		uint32_t getIP() const;
		void setIP(const uint32_t ip);
};

class DnsCNAMERecord : public DnsResourceRecord {
		std::string canonicalName;
	public:
		DnsCNAMERecord(dns_packet_header_storage_t *header, const char *record, size_t length) __attribute__((nonnull(1, 2)));
		DnsCNAMERecord(std::string);
		virtual ~DnsCNAMERecord();
		virtual char *toCString(size_t *length) const;
		std::string getCanonicalName();
		void setCanonicalName(const std::string canonicalName);
};

class DnsQuestion {
		std::string qname;
		DnsRecordTypes::RecordType qtype;
		DnsRecordClasses::RecordClass qclass;
	public:
		DnsQuestion();
		DnsQuestion(std::string qname, const DnsRecordTypes::RecordType qtype, const DnsRecordClasses::RecordClass qclass);
		~DnsQuestion();

		char *fromCString(dns_packet_header_storage_t *header, char *cstring) __attribute__((nonnull(1, 2)));
		char *toCString(size_t *length) const;

		std::string getQName() const;
		DnsRecordTypes::RecordType getQType() const;
		DnsRecordClasses::RecordClass getQClass() const;

		void setQName(const std::string qname);
		void setQType(const DnsRecordTypes::RecordType qtype);
		void setQClass(const DnsRecordClasses::RecordClass qclass);
};

class DnsAnswer {
		std::string qname;
		DnsRecordTypes::RecordType qtype;
		DnsRecordClasses::RecordClass qclass;
		uint32_t TTL;
		DnsResourceRecord *rr;

	public:
		DnsAnswer();
		DnsAnswer(const std::string qname, const DnsRecordTypes::RecordType qtype, const DnsRecordClasses::RecordClass qclass, const int ttl, DnsResourceRecord *rr);
		~DnsAnswer();

		char *fromCString(dns_packet_header_storage_t *header, char *cstring) __attribute__((nonnull(1, 2)));
		char *toCString(size_t *length) const;

		std::string getQName() const;
		DnsRecordTypes::RecordType getQType() const;
		DnsRecordClasses::RecordClass getQClass() const;
		uint32_t getTTL() const;
		DnsResourceRecord *getResourceRecord() const;

		void setQName(const std::string qname);
		void setQType(const DnsRecordTypes::RecordType qtype);
		void setQClass(const DnsRecordClasses::RecordClass qclass);
		void setTTL(const uint32_t ttl);
		void setResourceRecord(DnsResourceRecord *rr);
};

class DnsMessage {
		uint_be16_t id;

		bool is_query;
		bool is_authority;
		bool allows_truncation;
		bool recursion_desired;
		bool recursion_allowed;
		DnsResponseCodes::ResponseCode rcode;
		DnsOpCodes::OpCode opcode;

		std::vector<DnsQuestion *> Questions;
		std::vector<DnsAnswer *> Answers;
		std::vector<DnsAnswer *> AuthorizedAnswers;
		std::vector<DnsAnswer *> AdditionalAnswers;

		uint_be16_t getOutputFlags() const;
	public:
		DnsMessage();
		DnsMessage(char *cstring);
		~DnsMessage();

		void fromCString(char *cstring) __attribute__((nonnull(1)));
		char *toCString(size_t *length) const;

		uint16_t getId() const;
		bool isQuery() const;
		bool isAuthority() const;
		bool allowsTruncation() const;
		bool recursionDesired() const;
		bool recursionAllowed() const;
		DnsResponseCodes::ResponseCode getRCode() const;
		DnsOpCodes::OpCode getOpCode() const;

		/* Warning: These return references which must be assigned to references not variables */
		std::vector< DnsQuestion * > &getQuestions();
		std::vector< DnsAnswer * > &getAnswers();
		std::vector< DnsAnswer * > &getAuthorizedAnswers();
		std::vector< DnsAnswer * > &getAdditionalAnswers();

		void setId(const uint16_t id);
		void setQuery(const bool is_query);
		void setAuthority(const bool is_authority);
		void setAllowsTrunctation(const bool allows_truncation);
		void setRecursionDesired(const bool recursion_desired);
		void setRecursionAllowed(const bool recursion_allowed);
		void setRCode(const DnsResponseCodes::ResponseCode rcode);
		void setOpCode(const DnsOpCodes::OpCode opcode);
		void clearResponses();
};

#endif
