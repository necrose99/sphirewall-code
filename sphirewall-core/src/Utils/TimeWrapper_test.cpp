#include <gtest/gtest.h>
#include <iostream>

#include "Utils/TimeWrapper.h"

using namespace std;

TEST(Time, add_hours){
	int base_time = 0;

	Time* t = new Time(base_time);
	t->set_hours(14);

	EXPECT_TRUE(t->timestamp() == 0 + 60 * 60 * 2);
}

TEST(Time, add_minutes){
	int base_time = 0;

	Time* t = new Time(base_time);
	t->set_minutes(30);

	EXPECT_TRUE(t->timestamp() == 0 + 60 * 30);
}

TEST(Time, set_24h_time){
	int base_time = 0;

	Time* t = new Time(base_time);

	t->set_24h_time(2210);
	EXPECT_TRUE(t->timestamp() == 0 + 60 * 60 * 10 + 10 * 60);
}

TEST(Time, nextDays){
	int base_time = 0;

	Time* target = Time::nextDays(base_time, 0);
	cout << target->format("%c") << endl;	
}

