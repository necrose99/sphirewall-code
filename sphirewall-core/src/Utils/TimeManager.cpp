/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "TimeManager.h"
#include <ctime>
#include "Utils/TimeWrapper.h"

TimeManager::TimeManager()
	: usingSystemTime(true), internalTime(0)
{}

TimeManager::~TimeManager() {}

int TimeManager::time(void *compat) {
	if (usingSystemTime) {
		return std::time(NULL);
	}
	else {
		return internalTime;
	}
}

Time TimeManager::AsTime() {
	return Time(time());
}

void TimeManager::useSystemTime() {
	usingSystemTime = true;
}

void TimeManager::useThisTime(int t) {
	usingSystemTime = false;
	internalTime = t;
}
