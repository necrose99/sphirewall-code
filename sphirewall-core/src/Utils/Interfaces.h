/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef INTERFACES_H
#define INTERFACES_H

#include <list>
#include <boost/shared_ptr.hpp>

#include "Utils/NetworkChangeNotifier.h"
#include "Core/Cron.h"
#include "Core/Lockable.h"
#include "Core/ConfigurationManager.h"

#define MAX_RETRIES 5 
#define MAX_PACKET_SIZE 1501 
#define MAX_DEVICES 256

#define DHCP_TRANSACTION_STARTED 0
#define DHCP_TRANSACTION_DISCOVER_SENT 1
#define DHCP_TRANSACTION_REQUEST_SENT 2
#define DHCP_TRANSACTION_FAILED 253
#define DHCP_TRANSACTION_RETRY_REQUIRED 254
#define DHCP_TRANSACTION_FINISHED 255

#define UNKOWN_DUPLEX -1
#define HALF_DUPLEX 0
#define FULL_DUPLEX 1

struct dhcp_lease {
        u_int32_t aquired_time;
        u_int32_t lease_timeout;

	u_int32_t ip;
	u_int32_t mask;
	u_int32_t dns;
	u_int32_t broadcast;
	u_int32_t gw;
	
};

struct dhcp_state {
        u_int8_t state;
        u_int8_t xid;
        u_int8_t retries;

        unsigned int server_ip;
        unsigned int requested_ip;
		
	struct dhcp_lease lease;
};

enum {
	INTERFACE_ADDR_MODE_MANUAL = 0,
	INTERFACE_ADDR_MODE_STATIC = 1,
	INTERFACE_ADDR_MODE_DHCP = 2
};

struct intf_handle;

class DhcpStaticLease {
        public:
                std::string ip;
                std::string mac;
};

class IPv4Address {
	public:
		unsigned int ip;
		unsigned int mask;
		unsigned int bcast;

		std::string to_string(){
			std::stringstream ss;
			return ss.str();
		}
};

class IPv6Address {
//TODO: IPv6 Interface Management
};

typedef boost::shared_ptr<IPv4Address> IPv4AddressPtr;
typedef boost::shared_ptr<IPv6Address> IPv6AddressPtr;


enum {
	DHCP_SERVER_NONE = 1,
	DHCP_SERVER_SERVICE= 2,
	DHCP_SERVER_RELAY= 3
};

class InterfaceConfiguration {
	public:
		InterfaceConfiguration(){
			//do shit here
			this->vlan = false;
			this->lacp = false;
			this->bridge= false;
			this->physical= false;
			this->autoneg = true;
			this->full_duplex = false;
			this->speed = 10;
		}

		std::string interface;
		std::string label;

		//Address information
		bool dhcp;
		list<IPv4AddressPtr> ipv4_addresses;
		unsigned int gateway;

		//Type information
		bool vlan;
		bool lacp;
		bool bridge;
		bool physical;

		//Configuration information
		std::list<std::string> bridgeDevices;
		std::list<std::string> bondedDevices;
		int vlanId;
		std::string vlanInterface;

		int lacpMode;
		int lacpLinkStatePollFrequency;
		int lacpLinkStateDownDelay;
		int lacpLinkStateUpDelay;

	        bool autoneg;
                bool full_duplex;
                int speed; 
};

typedef boost::shared_ptr<InterfaceConfiguration> InterfaceConfigurationPtr;
class IntMgr;
class Interface {
	public:
		Interface(std::string name);

		list<IPv4AddressPtr> ipv4_addresses;
		list<IPv6AddressPtr> ipv6_addresses;

		bool state;
		int ifid;
		std::string name;
		std::string label;
		unsigned int gateway;
		u_int8_t addr_mode;
		struct dhcp_state dhcp;

		bool deleting;

		InterfaceConfigurationPtr configuration_entity;
		virtual void save_state_to_configuration_entity();

		unsigned int get_primary_ipv4_address();
		unsigned int get_primary_ipv4_netmask();
		unsigned int get_primary_ipv4_broadcast();

		void add_ipv4_address(unsigned int ip, unsigned int mask, unsigned int bcast);
		void remove_ipv4_address(unsigned int ip, unsigned int mask, unsigned int bcast);
		int get_hardware_addr(char* addr_ret);
		void refresh_address_configuration();

		static void __bring_up(std::string ifname);
		virtual void bring_up(IntMgr* manager);
		void bring_down();

		std::string to_string();
		virtual bool is_vlan(){ return false; }
		virtual bool is_bridge(){ return false; }
		virtual bool is_bonding(){ return false; }
		virtual bool is_ppp(){ return false; }
		virtual bool is_physical(){ return true; }

		void stop_dhclient_if_running();
                void __configure_interface_routing(unsigned int ip, unsigned int mask, unsigned int gw);

		double stats__get_rx_packets();
		double stats__get_rx_errors();
		double stats__get_rx_dropped();
		double stats__get_rx_bytes();

		double stats__get_tx_packets();
		double stats__get_tx_errors();
		double stats__get_tx_dropped();
		double stats__get_tx_bytes();

		/* Duplex and speed settings */
		bool is_full_duplex();
		bool get_autoneg();
		bool has_link();
		int get_speed();
		bool supports_ethtool();

		void set_speed(bool autoneg, int speed, bool full_duplex);
	private:
		//Internal dhcp methods
		int dhcp_request_lease();
		int dhcp_release_lease();

		void request_lease_dhcp_discover();
		void request_lease_dhcp_request();
		void request_lease_dhcp_release();
		void request_lease_handle_message(char*, size_t);

		bool has_normal_flag(u_int8_t flag);
		double get_statistic(std::string key);
};

class PointToPointInterface : public Interface {
	public:
		PointToPointInterface(std::string name) : Interface(name){}

                bool is_ppp() {
                        return true;
                }
                bool is_physical(){ return false; }
};

class BridgeInterface : public Interface{
	public:
		BridgeInterface(std::string name) : Interface(name){}	

		bool is_bridge() {
			return true;
		}

		bool is_physical(){ return false; }
		std::list<std::string> get_bridged_devices();
		int add_bridged_device(std::string);	
		int remove_bridged_device(std::string);	

		void save_state_to_configuration_entity();
		void bring_up(IntMgr* manager);
};	


class VlanInterface : public Interface{
	public:
		VlanInterface(std::string name) : Interface(name){}	

		bool is_vlan() {
			return true;
		}

		bool is_physical(){ return false; }
		int get_vlan_id();
		std::string get_vlan_interface();
		void save_state_to_configuration_entity();
		void bring_up(IntMgr* manager);
};	

class BondingInterface: public Interface{
	public:
		BondingInterface(std::string name) : Interface(name){}
		bool is_bonding() {
			return true;
		}

		bool is_physical(){ return false; }
		std::list<std::string> get_slaves();
		int add_slave(std::string);
		int remove_slave(std::string);

		int get_variable(std::string key);
		void set_variable(std::string key, int value);
		void set_bonding_mode(int);
		void set_link_state_poll_frequency(int);
		void set_link_down_delay(int);
		void set_link_up_delay(int);

		int get_bonding_mode();
		int get_link_state_poll_frequency();
		int get_link_down_delay();
		int get_link_up_delay();
		void save_state_to_configuration_entity();
};

typedef boost::shared_ptr<Interface> InterfacePtr;

class IntMgrChangeListener {
	public:
		virtual void interface_change(InterfacePtr changed_interface) = 0;
};

class IntMgr : public virtual Lockable, public virtual Configurable, public virtual NetworkChangeNotifierListener {
	public:
		IntMgr();
		bool load();
		void save();
		const char* getConfigurationSystemName(){
			return "IntMgr configuration";
		}

		void run();

		InterfacePtr get_interface(std::string name);
		InterfacePtr get_interface_by_id(int id);
		void put_interface_by_id(int id, InterfacePtr interface);
		list<InterfacePtr> get_all_interfaces();
		void delete_interface(InterfacePtr ptr);

		int create_bridge_interface(std::string name);
		int create_vlan_interface(std::string binding_interface, int vlan_id);
		int create_bonding_interface(std::string name);

		int delete_bridge_interface(std::string name);
		int delete_vlan_interface(std::string binding_interface, int vlan_id);
		int delete_bonding_interface(std::string name);

		bool is_bridge(std::string name);
		bool is_vlan(std::string name);
		bool is_bonding(std::string name);

		int handle_network_change_notification(struct nlmsghdr *nh, size_t len);

		void set_persisted(InterfacePtr interface, bool persisted);
		void register_change_listener(IntMgrChangeListener* l){
			registered_change_listeners.push_back(l);
		}
		void trigger_change_listeners(InterfacePtr changed_interface){
			for(IntMgrChangeListener* l : registered_change_listeners){
				l->interface_change(changed_interface);
			}
		}

	private:
		InterfacePtr active_interfaces[MAX_DEVICES];
		list<InterfaceConfigurationPtr> configured_interfaces;
		list<IntMgrChangeListener*> registered_change_listeners;
		InterfaceConfigurationPtr get_interface_configuration(std::string name){
			for(InterfaceConfigurationPtr ptr : configured_interfaces){
				if(ptr->interface.compare(name) == 0){
					return ptr;
				}
			}
			return InterfaceConfigurationPtr(); 
		}

		NetworkChangeNotifier notifier;
		bool listening;

		bool state_known;
		void handle_rtm_newlink(struct ifinfomsg *info, size_t len);
		void handle_rtm_dellink(struct ifinfomsg *info, size_t len);
		void handle_rtm_getlink(struct ifinfomsg *info, size_t len);
		void handle_rtm_newaddr(struct ifaddrmsg *info, size_t len);
		void handle_rtm_deladdr(struct ifaddrmsg *info, size_t len);
		void handle_rtm_getaddr(struct ifaddrmsg *info, size_t len);
};

class IntMgrCronJob : public virtual CronJob {
	public: 
		IntMgrCronJob(IntMgr* manager);
		void run();	

	private:
		IntMgr* manager;
};

class DNSConfig {
	public:
		DNSConfig();
		void save();

		string getNS1() {
			return nameserver1;
		}

		string getNS2() {
			return nameserver2;
		}

		string getDomain() {
			return domain;
		}

		string getSearch() {
			return search;
		}

		void setNS1(string ns) {
			nameserver1 = ns;
		}

		void setNS2(string ns) {
			nameserver2 = ns;
		}

		void setDomain(string d) {
			domain = d;
		}

		void setSearch(string s) {
			search = s;
		}

	private:
		string nameserver1;
		string nameserver2;
		string domain;
		string search;

};

#endif

