#include <iostream>
#include <gtest/gtest.h>

#include "Utils/IP4Addr.h"

TEST(IP4Addr, cidr_to_netmask){
	EXPECT_TRUE(IP4Addr::cidr_to_netmask(32) == IP4Addr::stringToIP4Addr("255.255.255.255"));
	EXPECT_TRUE(IP4Addr::cidr_to_netmask(24) == IP4Addr::stringToIP4Addr("255.255.255.0"));

}
