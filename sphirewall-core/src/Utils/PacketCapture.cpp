#include <iostream>
#include <pcap.h>

#include <string.h>
#include <openssl/sha.h>
#include <openssl/hmac.h>
#include <openssl/evp.h>
#include <openssl/bio.h>
#include <openssl/buffer.h>

using namespace std;

#include "Core/System.h"
#include "Utils/PacketCapture.h"
#include "SFwallCore/Packet.h"
#include "Kernel/sphirewall_queue.h"

#define ETHER_ADDR_LEN  6
#define ETH_P_IP 0x0800
#define ETH_P_IPV6 0x86DD
#define ETH_P_ARP 0x0806

struct sniff_ethernet {
        u_char ether_dhost[ETHER_ADDR_LEN]; 
        u_char ether_shost[ETHER_ADDR_LEN];
        u_short ether_type;
};

struct pcap_hdr_s {
        uint32_t magic_number;
        uint16_t version_major;
        uint16_t version_minor;
        int32_t  thiszone; 
        uint32_t sigfigs;
        uint32_t snaplen;
        uint32_t network;
};


CapturedPacket::CapturedPacket(const struct pcap_pkthdr *header, const unsigned char* packet){
	this->header = (struct pcap_pkthdr*) malloc(sizeof(struct pcap_pkthdr));
	this->packet= (unsigned char*) malloc(header->len);

	memcpy((void*) this->header, header, sizeof(struct pcap_pkthdr));
	memcpy((void*) this->packet, packet, header->len);
}

CapturedPacket::CapturedPacket(SFwallCore::Packet* input_packet){
        header = (struct pcap_pkthdr*) malloc(sizeof(pcap_pkthdr));
        header->ts.tv_sec= input_packet->getTimestamp();
        header->ts.tv_usec= 0;
        header->caplen = sizeof(struct ethhdr) + input_packet->getInternalSqp()->len;
        header->len = sizeof(struct ethhdr) + input_packet->getInternalSqp()->len;

        packet = (unsigned char*) malloc(sizeof(struct ethhdr) + input_packet->getInternalSqp()->len);

        struct ethhdr e;
        e.h_proto = htons(0x0800);
        memcpy(packet, (unsigned char*) &e, sizeof(struct ethhdr));

        //Write the packet
        memcpy(packet+ sizeof(struct ethhdr), input_packet->getInternalSqp()->raw_packet, input_packet->getInternalSqp()->len);
}

CapturedPacket::~CapturedPacket(){
	free((void*) header);
	free((void*) packet);
}

std::string CapturedPacket::__ipv4_to_string(){
	struct sq_packet* sqp = (struct sq_packet*) malloc(sizeof(struct sq_packet));
	sqp->type = 0;
	sqp->len = header->len;
	sqp->indev = -1;
	sqp->outdev= -1;
	sqp->hwlen = 0;
	sqp->raw_packet = ((unsigned char*) packet) + sizeof(struct sniff_ethernet);

	SFwallCore::PacketV4* packet = new SFwallCore::PacketV4(sqp);
	return packet->toString();
}

std::string CapturedPacket::__ipv6_to_string(){
	struct sq_packet* sqp = (struct sq_packet*) malloc(sizeof(struct sq_packet));
	sqp->type = 1;
	sqp->len = header->len;
	sqp->indev = -1;
	sqp->outdev= -1;
	sqp->hwlen = 0;
	sqp->raw_packet = ((unsigned char*) packet) + sizeof(struct sniff_ethernet);

	SFwallCore::PacketV6* packet = new SFwallCore::PacketV6(sqp);
	return packet->toString();
}

std::string CapturedPacket::to_string(){
	struct sniff_ethernet* ethernet_header = (struct sniff_ethernet*) packet;
	switch(ntohs(ethernet_header->ether_type)){
		case ETH_P_IP:
			return __ipv4_to_string();
		case ETH_P_IPV6:
			return __ipv6_to_string();
	};

	return "unknown packet";
}

void got_packet(u_char *args, const struct pcap_pkthdr *header, const u_char *packet){
	PacketCaptureEngine* engine = (PacketCaptureEngine*) args; 
	engine->handle_packet_input(header, packet);
}

int PacketCaptureEngine::CAPTURE_LIMIT = 10000;

PacketCaptureEngine::PacketCaptureEngine(){
	System::getInstance()->config.getRuntime()->loadOrPut("CAPTURE_LIMIT", &CAPTURE_LIMIT);
	inline_capture_enabled = false;
	running = false;
}

void PacketCaptureEngine::start_capture(){
        captured_packets.clear();

	if (this->capture_type == USER || this->capture_type== MAC) {
		inline_capture_enabled = true;
	} else {
		boost::thread(boost::bind(&PacketCaptureEngine::do_start_capture, this));
	}
}

void PacketCaptureEngine::do_start_capture(){
	stop_capture();
	handle = NULL;

	char errbuf[PCAP_ERRBUF_SIZE]; 
	struct bpf_program fp;        

	handle = pcap_open_live(capture_interface.c_str(), BUFSIZ, 1, 1000, errbuf);
	if (handle == NULL) {
		Logger::instance()->log("sphirewalld.packetcaptureengine", ERROR, "Could not open device for capture, reason was '%s'", errbuf);
		return;
	}

	if (pcap_compile(handle, &fp, capture_filter.c_str(), 0, PCAP_NETMASK_UNKNOWN ) == -1) {
		Logger::instance()->log("sphirewalld.packetcaptureengine", ERROR, "Could not parse filter, reason was '%s'", pcap_geterr(handle));
		handle = NULL;
		return;
	}

	if (pcap_setfilter(handle, &fp) == -1) {
		Logger::instance()->log("sphirewalld.packetcaptureengine", ERROR, "Could not install filter, reason was '%s'", pcap_geterr(handle));
		handle = NULL;
		return;
	}

	running = true;

	Logger::instance()->log("sphirewalld.packetcaptureengine", INFO, "Starting packet capture on '%s' with filter '%s'", capture_interface.c_str(), capture_filter.c_str());
	pcap_loop(handle, -1, &got_packet, (u_char*) this);
	running = false;
	Logger::instance()->log("sphirewalld.packetcaptureengine", INFO, "Stopped packet capture on '%s' with filter '%s'", capture_interface.c_str(), capture_filter.c_str());
}

void PacketCaptureEngine::stop_capture(){
	if(handle){
		pcap_breakloop(handle);
		pcap_close(handle);
		handle = NULL;
	}
		
	inline_capture_enabled = false;
}

void PacketCaptureEngine::handle_packet_capture_input(SFwallCore::Packet* packet){
	if (this->inline_capture_enabled && this->CAPTURE_LIMIT > this->captured_packets.size()) {
		cout << "hit --> evaluating\n";
		if(this->capture_type == MAC && packet->hasHwAddr() && packet->getHw().compare(this->hw) == 0) {
			this->captured_packets.push_back(CapturedPacketPtr(new CapturedPacket(SFwallCore::Packet::parsePacket(SFwallCore::Packet::cloneSqp(packet->getInternalSqp())))));
		}
		else if (this->capture_type == USER && packet->getUser() &&  this->user_target &&  packet->getUser() == this->user_target) {
			this->captured_packets.push_back(CapturedPacketPtr(new CapturedPacket(SFwallCore::Packet::parsePacket(SFwallCore::Packet::cloneSqp(packet->getInternalSqp())))));
		}
	}
}

void PacketCaptureEngine::handle_packet_input(const struct pcap_pkthdr *header, const unsigned char* packet){
	if(captured_packets.size() > CAPTURE_LIMIT){
		Logger::instance()->log("sphirewalld.packetcaptureengine", ERROR, "Packet capture has exceeded softlimit, dropping packet. To modify limit set CAPTURE_LIMIT");
		return;
	}

	captured_packets.push_back(CapturedPacketPtr(new CapturedPacket(header, packet)));
}

char *base64(const unsigned char *input, int length)
{
	BIO *bmem, *b64;
	BUF_MEM *bptr;

	b64 = BIO_new(BIO_f_base64());
	bmem = BIO_new(BIO_s_mem());
	b64 = BIO_push(b64, bmem);
	BIO_write(b64, input, length);
	BIO_flush(b64);
	BIO_get_mem_ptr(b64, &bptr);

	char *buff = (char *)malloc(bptr->length);
	memcpy(buff, bptr->data, bptr->length-1);
	buff[bptr->length-1] = 0;

	BIO_free_all(b64);

	return buff;
}

std::string PacketCaptureEngine::get_base64_encoded_pcap_string(){
	int len = 0;
	int cursor = 0;

	struct pcap_hdr_s* item = (struct pcap_hdr_s*) malloc(sizeof(struct pcap_hdr_s));	
	item->magic_number = 0xa1b2c3d4;
	item->version_major = 2.4;
	item->version_minor = 2.4;
	item->thiszone = 0;
	item->sigfigs = 0;
	item->snaplen = 65535;
	item->network = 1;

	unsigned char* buffer = (unsigned char*) malloc(sizeof(struct pcap_hdr_s));
	cursor += sizeof(struct pcap_hdr_s);
	memcpy(buffer, item, sizeof(struct pcap_hdr_s));
	len += sizeof(struct pcap_hdr_s);
	for(CapturedPacketPtr packet : captured_packets){
		/* Increase the buffer to include this packet */
		len += sizeof(struct pcap_pkthdr) + packet->header->len;
		buffer = (unsigned char*) realloc(buffer, len);

		/* Copy the header, and increment the cursor */
		memcpy(buffer + cursor, packet->header, sizeof(struct pcap_pkthdr));	
		cursor += sizeof(struct pcap_pkthdr);

		/* Copy the packet, and increment the cursor*/
		memcpy(buffer + cursor, packet->packet, packet->header->len);	
		cursor += packet->header->len; 
	}		

	char* base64_encoded_string = base64(buffer, len);
	string ret = string(base64_encoded_string);

	/* Cleanup */
	free(base64_encoded_string);
	free(buffer);
	free(item);
	return ret;
}

/*EOF*/
