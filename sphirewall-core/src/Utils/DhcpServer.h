/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DHCP_SERVER_H
#define DHCP_SERVER_H

#include <list>
#include <boost/shared_ptr.hpp>
#include "Utils/Interfaces.h"

#define CLEANUP_PERIOD 600

enum {
	DHCP_SERVER_LEASE_OFFER = 1,
	DHCP_SERVER_LEASE_ESTABLISHED = 2,
	DHCP_SERVER_LEASE_PERMANENT = 3
};

enum {
        DNS_NONE=0,
        DNS_FORWARD=1,
        DNS_DIRECT=2
};

class DhcpServerLease {
	public:
		unsigned int lease_time;
		unsigned int state;	
		
		unsigned int ip;
		std::string mac_address; 

		const char* to_string();
};

typedef boost::shared_ptr<DhcpServerLease> DhcpServerLeasePtr;

class DDInterfaceConfiguration {
	public:
		DDInterfaceConfiguration(std::string interface){
			dhcp_mode= DHCP_SERVER_NONE;
			dns_mode = DNS_NONE;
			start_ip = 0;
			end_ip = 0;
			dns_server= 0;
			default_route= 0;
			max_lease_time = 7200;
			max_offered_lease_time = 60;
			relay_server=0;
			dns_forwarder_listener_running=false;
			tftp_boot_enabled=false;
			
			this->interface = interface;
		}

		int dhcp_mode;
		int dns_mode;

		unsigned int start_ip;
		unsigned int end_ip;

		unsigned int dns_server;
		unsigned int default_route;
		unsigned int max_lease_time;
		unsigned int max_offered_lease_time;
		unsigned int relay_server;

		bool tftp_boot_enabled;
		std::string tftp_boot_server;
		std::string tftp_boot_filename;

		std::string interface;
		InterfacePtr interface_ptr;
		InterfacePtr relay_route_interface_ptr;

		void refresh_interfaces(IntMgr* interfaceManager);

		//Dhcp entry methods
		void handle_dhcp_request(struct dhcp_builder_context* ctx);
		void handle_dhcp_response(struct dhcp_builder_context* ctx);

		//Dns entry methods 
		void __run_dns_forwarder_listener();
		bool dns_forwarder_listener_running;

		std::list<DhcpServerLeasePtr> list_leases();
		void add_leases(DhcpServerLeasePtr lease);
		void del_leases(DhcpServerLeasePtr lease);
                DhcpServerLeasePtr find_lease_by_mac_ip(unsigned int ip, std::string hw);

		void dhcp_cleanup();
		
	private:
		//Private DHCP Server methods
		void __handle_dhcp_request__discover(struct dhcp_builder_context* ctx);
		void __handle_dhcp_request__request(struct dhcp_builder_context* ctx);
		void __handle_dhcp_request__decline(struct dhcp_builder_context* ctx);
		void __handle_dhcp_request__release(struct dhcp_builder_context* ctx);

                DhcpServerLeasePtr find_lease_by_hw(std::string hw);
                DhcpServerLeasePtr find_lease_by_ip(unsigned int ip);
                unsigned int find_free_address();

                std::list<DhcpServerLeasePtr> leases;

		//Private DNS Forwarder methods
		int __dns_open_socket();
		int __tcp_query(void *query, char* buffer, int len);
		void __handle_dns_query(struct sockaddr_in* dns_client, char* query_buffer, int len, int sock);

};

typedef boost::shared_ptr<DDInterfaceConfiguration> DDInterfaceConfigurationPtr;

class DDServerManager : public IntMgrChangeListener, public Configurable {
	public:
		DDServerManager(IntMgr* interfaceManager){
			this->interfaceManager = interfaceManager;
			this->last_cleanup = time(NULL);
		}

		bool load();
		void save();
		const char* getConfigurationSystemName(){
			return "DDServerManager, internal dhcp server, relay and dns forwarder";
		}

		DDInterfaceConfigurationPtr get_server_for_interface(InterfacePtr interface){
			if(!instances[interface->ifid]){
				instances[interface->ifid] = DDInterfaceConfigurationPtr(new DDInterfaceConfiguration(interface->name));				
				instances[interface->ifid]->refresh_interfaces(interfaceManager);
			}

			return instances[interface->ifid];
		}

		void start();

		std::list<DDInterfaceConfigurationPtr> get_instances(){
			list<DDInterfaceConfigurationPtr> ret;
			for(pair<int, DDInterfaceConfigurationPtr> server : instances){
				if(server.second){
					ret.push_back(server.second);
				}
			} 			

			return ret;
		}

		void interface_change(InterfacePtr){
			for(DDInterfaceConfigurationPtr instance : get_instances()){
				instance->refresh_interfaces(this->interfaceManager);
			}
		}
	private:
		map<int, DDInterfaceConfigurationPtr> instances;
		IntMgr* interfaceManager;
		int last_cleanup;
		void __run_dhcp_listener();
		void __run_dns_listeners();
		bool __should_dhcp_listen();
};

#endif
