#ifndef IP6ADDR_H
#define IP6ADDR_H

struct in6_addr;
class IP6Addr {
	public:
		static bool matches(struct in6_addr* a, struct in6_addr* b);
		static std::string toString(struct in6_addr* a);
		static void fromString(struct in6_addr* ret, std::string input);
		static struct in6_addr fromString(std::string input);

		static bool matchNetwork(struct in6_addr* in, struct in6_addr* subnet, int cidr);
		static bool valid(std::string);
		static int hash(struct in6_addr* in);

};

#endif
