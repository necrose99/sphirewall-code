/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Utils/DnsType.h"
#include <Auth/UserDb.h>
#include <cstdlib>
#include <cstdio>
#include <string>
#include <cstring>
#include <vector>
#include <netinet/in.h>

/* Dns Flags (assuming perfect portable packing, impossible)
 be_uint16_t     qr:1 __attribute__ ((packed));
 be_uint16_t opcode:4 __attribute__ ((packed));
 be_uint16_t     aa:1 __attribute__ ((packed));
 be_uint16_t     tc:1 __attribute__ ((packed));
 be_uint16_t     rd:1 __attribute__ ((packed));
 be_uint16_t     ra:1 __attribute__ ((packed));
 be_uint16_t      z:3 __attribute__ ((packed));
 be_uint16_t  rcode:4 __attribute__ ((packed));
*/

#define QR(X)     (ntohs(X) & (1 << 15)) >> 15
#define OPCODE(X) (ntohs(X) & (1 << 11 | 1 << 12 | 1 << 13 | 1 << 14)) >> 11
#define AA(X)     (ntohs(X) & (1 << 10)) >> 10
#define TC(X)     (ntohs(X) & (1 << 9)) >> 9
#define RD(X)     (ntohs(X) & (1 << 8)) >> 8
#define RA(X)     (ntohs(X) & (1 << 7)) >> 7
#define RCODE(X)  (ntohs(X) & (1 | 1 << 1 | 1 << 2 | 1 << 3))
#define DNS_HDR_LEN sizeof(struct dns_packet_header)
#define DNS_MAX_NAME_LENGTH 255


int dns_ex_aa(uint_be16_t flags) {
	return AA(flags);
}

int dns_ex_opcode(uint_be16_t flags) {
	return OPCODE(flags);
}

int dns_ex_qr(uint_be16_t flags) {
	return QR(flags);
}

int dns_ex_ra(uint_be16_t flags) {
	return RA(flags);
}

int dns_ex_rcode(uint_be16_t flags) {
	return RCODE(flags);
}

int dns_ex_rd(uint_be16_t flags) {
	return RD(flags);
}

int dns_ex_tc(uint_be16_t flags) {
	return TC(flags);
}

void dns_undot_name(char *name, size_t length) {
	int s = 0;

	if (name == NULL) {
		return;
	}

	for (int i = length - 1; i >= 0; i--) {
		if (name[i] == '.') {
			name[i] = s;
			s = 0;
		}
		else {
			s++;
		}
	}
}

void dns_dot_name(char *name, size_t length) {
	int s = 0;

	if (name == NULL) {
		return;
	}

	for (unsigned i = 0; i < length - 1; i++) {
		if (s == 0) {
			s = name[i];
			name[i] = '.';
		}
		else {
			s--;
		}
	}
}

void charvec_append_16bit_int(std::vector<unsigned char> &vec, uint_be16_t val) {
	vec.push_back(((unsigned char *)&val)[0]);
	vec.push_back(((unsigned char *)&val)[1]);
}

void charvec_append_16bit_int(std::vector<char> &vec, uint_be16_t val) {
	vec.push_back(((char *)&val)[0]);
	vec.push_back(((char *)&val)[1]);
}

void charvec_append_32bit_int(std::vector<unsigned char> &vec, uint_be32_t val) {
	vec.push_back(((unsigned char *)&val)[0]);
	vec.push_back(((unsigned char *)&val)[1]);
	vec.push_back(((unsigned char *)&val)[2]);
	vec.push_back(((unsigned char *)&val)[3]);
}
void charvec_append_32bit_int(std::vector<char> &vec, uint_be32_t val) {
	vec.push_back(((char *)&val)[0]);
	vec.push_back(((char *)&val)[1]);
	vec.push_back(((char *)&val)[2]);
	vec.push_back(((char *)&val)[3]);
}

void charvec_append_chars(std::vector<unsigned char> &vec, const char *val, size_t length) {
	for (unsigned i = 0; i < length; i++) {
		vec.push_back((const unsigned char)val[i]);
	}
}

void charvec_append_chars(std::vector<unsigned char> &vec, const unsigned char *val, size_t length) {
	for (unsigned i = 0; i < length; i++) {
		vec.push_back(val[i]);
	}
}

void charvec_append_chars(std::vector<char> &vec, const char *val, size_t length) {
	for (unsigned i = 0; i < length; i++) {
		vec.push_back(val[i]);
	}
}

void charvec_append_dns_header(std::vector<unsigned char> &vec, const dns_packet_header_storage_t *header) {
	charvec_append_chars(vec, (const unsigned char *) header, sizeof(*header));
}

void charvec_append_dns_header(std::vector<char> &vec, const dns_packet_header_storage_t *header) {
	charvec_append_chars(vec, (const char *) header, sizeof(*header));
}

char *label_to_name(dns_packet_header_storage_t *hdr, ssize_t offset, std::string &name) {
	if (hdr == NULL) {
		return NULL;
	}

	char *current_pos = ((char *)hdr) + offset;
	char s = 0;

	while (current_pos && current_pos[0]) {
		if (s) {
			s--;
			name.push_back(*++current_pos);

			if (!s) {
				current_pos++;
				name.push_back('.');
			}
		}
		else {
			s = current_pos[0];
			uint16_t p = ntohs(((uint_be16_t *)current_pos)[0]);

			if (p & 0b1100000000000000) {
				label_to_name(hdr, p & 0b0011111111111111, name);
				return current_pos + 2;
			}
		}
	}

	name = name.substr(0, name.length() - 1);
	return current_pos + 1;
}

DnsAnswer::DnsAnswer()
	: qname(), qtype(), qclass(DnsRecordClasses::INTERNET), TTL(7200), rr(NULL)
{}

DnsAnswer::DnsAnswer(const std::string qname, const DnsRecordTypes::RecordType qtype, const DnsRecordClasses::RecordClass qclass, const int ttl, DnsResourceRecord *rr)
	: qname(qname), qtype(qtype), qclass(qclass), TTL(ttl), rr(rr)
{}

char *DnsAnswer::fromCString(dns_packet_header_storage_t *header, char *cstring) {
	size_t size = 0;
	char *current_pos = NULL;

	if ((current_pos = label_to_name(header, &cstring[0] - ((char *)header), qname)) == NULL) {
		return NULL;
	}

	qtype = ntohs(((uint_be16_t *)current_pos)[0]);
	current_pos += 2;
	qclass = ntohs(((uint_be16_t *)current_pos)[0]);
	current_pos += 2;
	TTL = ntohl(((uint_be32_t *)current_pos)[0]);
	current_pos += 4;
	size = ntohs(((uint_be16_t *)current_pos)[0]);
	current_pos += 2;

	if ((rr = DnsResourceRecord::fromCString(header, current_pos, size, qtype)) == NULL) {
		return NULL;
	}

	return current_pos + size;
}

DnsRecordClasses::RecordClass DnsAnswer::getQClass() const {
	return qclass;
}

std::string DnsAnswer::getQName() const {
	return qname;
}

DnsRecordTypes::RecordType DnsAnswer::getQType() const {
	return qtype;
}

DnsResourceRecord *DnsAnswer::getResourceRecord() const {
	return rr;
}

uint32_t DnsAnswer::getTTL() const {
	return TTL;
}

void DnsAnswer::setQClass(DnsRecordClasses::RecordClass qclass) {
	this->qclass = qclass;
}

void DnsAnswer::setQName(const std::string qname) {
	this->qname = qname;
}

void DnsAnswer::setQType(DnsRecordTypes::RecordType qtype) {
	this->qtype = qtype;
}

void DnsAnswer::setResourceRecord(DnsResourceRecord *rr) {
	this->rr = rr;
}

void DnsAnswer::setTTL(const uint32_t ttl) {
	this->TTL = ttl;
}

char *DnsAnswer::toCString(size_t *l) const {
	std::vector<unsigned char> output;
	char *temp = NULL;
	size_t length = 0;

	if (qname[0] == '.') {
		if ((temp = new char [qname.length() + 1])) {
			std::memcpy(temp, qname.c_str(), qname.length());
			temp[qname.length()] = '\0';
		}
	}
	else {
		if ((temp = new char [qname.length() + 2])) {
			std::memcpy(temp, ("." + qname).c_str(), qname.length() + 1);
			temp[qname.length() + 1] = '\0';
		}
	}

	if (temp != NULL) {
		dns_undot_name(temp, strlen(temp));
		charvec_append_chars(output, temp, strlen(temp));
		charvec_append_chars(output, "\0", 1);
		charvec_append_16bit_int(output, htons(getQType()));
		charvec_append_16bit_int(output, htons(getQClass()));
		charvec_append_32bit_int(output, htonl(getTTL()));
		delete[] temp;
	}

	if ((temp = rr->toCString(&length))) {
		charvec_append_chars(output, temp, length);
		delete[] temp;
	}

	if ((temp = new char [output.size()])) {
		std::memcpy(temp, output.data(), output.size());
		*l = output.size();
	}
	else {
		*l = 0;
	}

	return temp;
}

DnsAnswer::~DnsAnswer() {
	if (rr != NULL) {
		delete rr;
	}
}

bool DnsMessage::allowsTruncation() const {
	return allows_truncation;
}

DnsMessage::DnsMessage()
	: id(), is_query(true), is_authority(false), allows_truncation(false), recursion_desired(true), recursion_allowed(false),
	  rcode(0), opcode(DnsOpCodes::QUERY), Questions(), Answers(), AuthorizedAnswers(), AdditionalAnswers()
{}

DnsMessage::DnsMessage(char *cstring)
	: id(), is_query(true), is_authority(false), allows_truncation(false), recursion_desired(true), recursion_allowed(false),
	  rcode(0), opcode(DnsOpCodes::QUERY), Questions(), Answers(), AuthorizedAnswers(), AdditionalAnswers() {
	this->fromCString(cstring);
}

void DnsMessage::fromCString(char *cstring) {
	if (cstring != NULL) {
		dns_packet_header_storage_t *header = (dns_packet_header_storage_t *) cstring;
		uint16_t nquestions = ntohs(header->nquestions);
		uint16_t nanswers = ntohs(header->nanswers);
		uint16_t nauthority = ntohs(header->nauthority);
		uint16_t nadditional = ntohs(header->nadditional);
		id = ntohs(header->id);
		is_authority = dns_ex_aa(header->flags);
		is_query = dns_ex_qr(header->flags) ? false : true;
		allows_truncation = dns_ex_tc(header->flags);
		recursion_desired = dns_ex_rd(header->flags);
		recursion_allowed = dns_ex_ra(header->flags);
		rcode = dns_ex_rcode(header->flags);
		opcode = dns_ex_opcode(header->flags);

		char *current_pos = cstring + sizeof(*header);

		for (int i = 0; i < nquestions; i++) {
			DnsQuestion *q = new DnsQuestion();
			current_pos = q->fromCString(header, current_pos);
			Questions.push_back(q);
		}

		for (int i = 0; i < nanswers; i++) {
			DnsAnswer *a = new DnsAnswer();
			current_pos = a->fromCString(header, current_pos);
			Answers.push_back(a);
		}

		for (int i = 0; i < nauthority; i++) {
			DnsAnswer *a = new DnsAnswer();
			current_pos = a->fromCString(header, current_pos);
			AuthorizedAnswers.push_back(a);
		}

		for (int i = 0; i < nadditional; i++) {
			DnsAnswer *a = new DnsAnswer();
			current_pos = a->fromCString(header, current_pos);
			AdditionalAnswers.push_back(a);
		}
	}
}

std::vector< DnsAnswer * > &DnsMessage::getAdditionalAnswers() {
	return AdditionalAnswers;
}

std::vector< DnsAnswer * > &DnsMessage::getAnswers() {
	return Answers;
}

std::vector< DnsAnswer * > &DnsMessage::getAuthorizedAnswers() {
	return AuthorizedAnswers;
}

uint16_t DnsMessage::getId() const {
	return id;
}

DnsOpCodes::OpCode DnsMessage::getOpCode() const {
	return opcode;
}

std::vector< DnsQuestion * > &DnsMessage::getQuestions() {
	return Questions;
}

DnsResponseCodes::ResponseCode DnsMessage::getRCode() const {
	return rcode;
}

bool DnsMessage::isAuthority() const {
	return is_authority;
}

bool DnsMessage::isQuery() const {
	return is_query;
}

bool DnsMessage::recursionAllowed() const {
	return recursion_allowed;
}

bool DnsMessage::recursionDesired() const {
	return recursion_desired;
}

void DnsMessage::setAllowsTrunctation(const bool allows_truncation) {
	this->allows_truncation = allows_truncation;
}

void DnsMessage::setAuthority(const bool is_authority) {
	this->is_authority = is_authority;
}

void DnsMessage::setId(const uint16_t id) {
	this->id = id;
}

void DnsMessage::setOpCode(DnsOpCodes::OpCode opcode) {
	this->opcode = opcode;
}

void DnsMessage::setQuery(const bool is_query) {
	this->is_query = is_query;
}

void DnsMessage::setRCode(DnsResponseCodes::ResponseCode rcode) {
	this->rcode = rcode;
}

void DnsMessage::setRecursionAllowed(const bool recursion_allowed) {
	this->recursion_allowed = recursion_allowed;
}

void DnsMessage::setRecursionDesired(const bool recursion_desired) {
	this->recursion_desired = recursion_desired;
}

char *DnsMessage::toCString(size_t *l) const {
	std::vector<char> output;
	char *temp = NULL;
	dns_packet_header_storage_t header;
	header.id = htons(id);
	header.flags = getOutputFlags();
	header.nquestions = htons(Questions.size());
	header.nanswers = htons(Answers.size());
	header.nauthority = htons(AuthorizedAnswers.size());
	header.nadditional = htons(AdditionalAnswers.size());

	charvec_append_dns_header(output, &header);

	for (DnsQuestion * q : Questions) {
		size_t length = 0;

		if ((temp = q->toCString(&length))) {
			charvec_append_chars(output, temp, length);
			delete[] temp;
		}
	}

	for (DnsAnswer * a : Answers) {
		size_t length = 0;

		if ((temp = a->toCString(&length))) {
			charvec_append_chars(output, temp, length);
			delete[] temp;
		}
	}

	for (DnsAnswer * a : AuthorizedAnswers) {
		size_t length = 0;

		if ((temp = a->toCString(&length))) {
			charvec_append_chars(output, temp, length);
			delete[] temp;
		}
	}

	for (DnsAnswer * a : AdditionalAnswers) {
		size_t length = 0;

		if ((temp = a->toCString(&length))) {
			charvec_append_chars(output, temp, length);
			delete[] temp;
		}
	}

	if ((temp = new char[output.size()])) {
		std::memcpy(temp, output.data(), output.size());
		*l = output.size();
	}
	else {
		*l = 0;
	}

	return temp;
}

void DnsMessage::clearResponses() {
	for (DnsAnswer * a : Answers) {
		if (a) {
			delete a;
		}
	}

	for (DnsAnswer * a : AuthorizedAnswers) {
		if (a) {
			delete a;
		}
	}

	for (DnsAnswer * a : AdditionalAnswers) {
		if (a) {
			delete a;
		}
	}

	Answers.clear();
	AuthorizedAnswers.clear();
	AdditionalAnswers.clear();
}

DnsMessage::~DnsMessage() {
	clearResponses();

	for (DnsQuestion * q : Questions) {
		if (q) {
			delete q;
		}
	}

	Questions.clear();
}


	DnsQuestion::DnsQuestion()
: qname(), qtype(), qclass()
{}

	DnsQuestion::DnsQuestion(std::string qname, DnsRecordTypes::RecordType qtype, DnsRecordClasses::RecordClass qclass)
: qname(qname), qtype(qtype), qclass(qclass)
{}

char *DnsQuestion::fromCString(dns_packet_header_storage_t *header, char *cstring) {
	char *name = NULL;

	if (header != NULL && cstring != NULL) {
		size_t len = strlen(cstring) + 1;
		if ((name = new char [len])) {
			std::memcpy(name, cstring, len);
			dns_dot_name(name, len);
			qname = name;

			if (qname[0] == '.') {
				qname = qname.substr(1);
			}

			qtype  = ntohs(*((uint_be16_t *)(cstring + len)));
			len += 2;
			qclass = ntohs(*((uint_be16_t *)(cstring + len)));
			len += 2;

			if (name) {
				delete name;
			}

			return &cstring[len];
		}
	}

	return NULL;
}

DnsRecordClasses::RecordClass DnsQuestion::getQClass() const {
	return qclass;
}

std::string DnsQuestion::getQName() const {
	return qname;
}

DnsRecordTypes::RecordType DnsQuestion::getQType() const {
	return qtype;
}

void DnsQuestion::setQClass(DnsRecordClasses::RecordClass qclass) {
	this->qclass = qclass;
}

void DnsQuestion::setQName(const std::string qname) {
	this->qname = qname;
}

void DnsQuestion::setQType(DnsRecordTypes::RecordType qtype) {
	this->qtype = qtype;
}

char *DnsQuestion::toCString(size_t *l) const {
	std::vector<char> output;
	char *temp = NULL;
	std::string tqn = qname;

	if (qname[0] != '.') {
		tqn = "." + tqn;
	}

	if ((temp = new char[tqn.length() + 1])) {
		temp[tqn.length()] = '\0';
		std::memcpy(temp, tqn.c_str(), tqn.length());

		dns_undot_name(temp, tqn.length());

		charvec_append_chars(output, temp, tqn.length() + 1);
		delete[] temp;
	}

	charvec_append_16bit_int(output, htons(qtype));
	charvec_append_16bit_int(output, htons(qclass));

	if ((temp = new char[output.size()])) {
		std::memcpy(temp, output.data(), output.size());
		*l = output.size();
	}
	else {
		*l = 0;
	}

	return temp;
}

DnsQuestion::~DnsQuestion()
{}

DnsResourceRecord *DnsResourceRecord::fromCString(dns_packet_header_storage_t *header, const char *start_of_record, int record_size, DnsRecordTypes::RecordType record_type) {
	if (header == NULL  || start_of_record == NULL) {
		return NULL;
	}

	switch (record_type) {
		case DnsRecordTypes::A:
			return new DnsARecord(header, start_of_record, record_size);

		case DnsRecordTypes::CNAME:
			return new DnsCNAMERecord(header, start_of_record, record_size);

		default:
			return new DnsResourceRecord(header, start_of_record, record_size);
	}
}

uint_be16_t DnsMessage::getOutputFlags() const {
	uint16_t flags = 0;

	flags |= (!is_query ? 1 : 0) << 15;
	flags |= opcode << 11;
	flags |= (is_authority ? 1 : 0) << 10;
	flags |= (allows_truncation ? 1 : 0) << 9;
	flags |= (recursion_desired ? 1 : 0) << 8;
	flags |= (recursion_allowed ? 1 : 0) << 7;
	flags |= rcode;

	return htons(flags);
}

char *DnsARecord::toCString(size_t *length) const {
	std::vector<unsigned char> output;
	char *name = NULL;
	charvec_append_16bit_int(output, htons(4));
	charvec_append_32bit_int(output, ipaddr);

	if ((name = new char[output.size()])) {
		std::memcpy(name, output.data(), output.size());
		*length = output.size();
	}
	else {
		*length = 0;
	}

	return name;
}

DnsARecord::DnsARecord(dns_packet_header_storage_t *header, const char *record, size_t length)
	: ipaddr() {
		if (length == 4 && header != NULL && record != NULL) {
			((unsigned char *)&ipaddr)[0] = record[0];
			((unsigned char *)&ipaddr)[1] = record[1];
			((unsigned char *)&ipaddr)[2] = record[2];
			((unsigned char *)&ipaddr)[3] = record[3];
		}
	}

DnsARecord::DnsARecord(in_addr_t ipaddr) {
	this->ipaddr = ipaddr;
}

uint32_t DnsARecord::getIP() const {
	return ipaddr;
}

void DnsARecord::setIP(const uint32_t ip) {
	ipaddr = ip;
}

DnsResourceRecord::DnsResourceRecord(dns_packet_header_storage_t *header, const char *record, size_t length)
	: unknownData() {
		if (header != NULL && record != NULL) {
			for (unsigned i = 0; i < length; i++) {
				unknownData.push_back(record[i]);
			}
		}
	}

char *DnsResourceRecord::toCString(size_t *length) const {
	char *temp = NULL;
	std::vector<unsigned char> output;
	charvec_append_16bit_int(output, htons(unknownData.size()));
	charvec_append_chars(output, unknownData.data(), unknownData.size());

	if ((temp = new char[output.size()])) {
		std::memcpy(temp, output.data(), output.size());
		*length = output.size();
	}
	else {
		*length = 0;
	}

	return temp;
}

DnsCNAMERecord::DnsCNAMERecord(dns_packet_header_storage_t *header, const char *record, size_t length)
	: canonicalName() {
		label_to_name(header, record - (char *)header, canonicalName);
	}

std::string DnsCNAMERecord::getCanonicalName() {
	return canonicalName;
}

void DnsCNAMERecord::setCanonicalName(const std::string canonicalName) {
	this->canonicalName = canonicalName;
}

char *DnsCNAMERecord::toCString(size_t *length) const {
	std::vector<unsigned char> output;
	char *temp = NULL;

	if (canonicalName[0] != '.') {
		*length = canonicalName.length() + 2;

		if ((temp = new char[*length])) {
			temp[0] = '.';
			temp[(*length) - 1] = 0;
			std::memcpy(temp + 1, canonicalName.c_str(), canonicalName.length());
		}
	}
	else {
		*length = canonicalName.length() + 1;

		if ((temp = new char[*length])) {
			temp[(*length) - 1] = 0;
			std::memcpy(temp, canonicalName.c_str(), canonicalName.length());
		}
	}

	if (temp) {
		dns_undot_name(temp, strlen(temp));
		charvec_append_16bit_int(output, htons(*length));
		charvec_append_chars(output, temp, *length);
		delete[] temp;
	}

	if ((temp = new char[output.size()])) {
		*length = output.size();
		std::memcpy(temp, output.data(), output.size());
	}
	else {
		*length = 0;
	}

	return temp;
}

	DnsCNAMERecord::DnsCNAMERecord(std::string canonicalName)
: canonicalName(canonicalName)
{}

DnsResourceRecord::DnsResourceRecord()
{}

DnsARecord::~DnsARecord()
{}

DnsCNAMERecord::~DnsCNAMERecord()
{}

DnsResourceRecord::~DnsResourceRecord()
{}
