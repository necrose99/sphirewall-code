/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SPHIREWALL_FILE_UTILS_H_INCLUDED 
#define SPHIREWALL_FILE_UTILS_H_INCLUDED

#include <vector>
#include <map>

class FileUtils {
public:
	static void deleteLine(const char* filename, int l);
	static bool updateLine(const char *filename, int targetLineNumber, const char *newLine);
	static bool checkExists(std::string filename);
	static std::string read(const char *filename);
	static void write(const char* filename, std::string input, bool create=false);
};


#endif
