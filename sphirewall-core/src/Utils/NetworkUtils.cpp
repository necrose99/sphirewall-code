/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/param.h>
#include <sys/socket.h>
#include <sys/file.h>
#include <sys/time.h>

#include <netinet/in_systm.h>
#include <netinet/in.h>
#include <netinet/ip_icmp.h>
#include <netinet/ip.h>
#include <netdb.h>

#include <unistd.h>
#include <stdio.h>
#include <ctype.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <iostream>
#include <vector>
#include <pthread.h>
#include <errno.h>

using namespace std;
using std::string;

#include "Utils/NetworkUtils.h"
#include "Utils/Utils.h"
#include "Utils/LinuxUtils.h"
#include "Core/SignalHandler.h"

int NetworkUtils::ipToHost(string ip, string &hostname) {
	struct hostent *hp;
	in_addr_t data;

	data = inet_addr(ip.c_str());
	hp = gethostbyaddr(&data, 4, AF_INET);

	if (hp == NULL) {
		hostname = ip;
	}
	else {
		hostname = hp->h_name;
		return 1;
	}

	return 0;
}

vector<in_addr_t> hostToIPAddresses(const string host) {
	struct hostent *hp;
	in_addr_t addr;
	vector<in_addr_t> addresses;

	if ((addr = inet_addr(host.c_str())) != INADDR_NONE) {
		addresses.push_back(ntohl(addr));
	}
	else {
		hp = gethostbyname(host.c_str());

		if (hp != NULL) {
			while (*hp->h_addr_list) {
				addr = *(in_addr_t *)(*hp->h_addr_list++);
				addresses.push_back(ntohl(addr));
			}
		}
	}

	return addresses;
}

#define	DEFDATALEN	(64-ICMP_MINLEN)
#define	MAXIPLEN	60
#define	MAXICMPLEN	76
#define	MAXPACKET	(65536 - 60 - ICMP_MINLEN)

uint16_t in_cksum(uint16_t *addr, unsigned len) {
	uint16_t answer = 0;
	uint32_t sum = 0;

	while (len > 1) {
		sum += *addr++;
		len -= 2;
	}

	if (len == 1) {
		*(unsigned char *) &answer = *(unsigned char *) addr;
		sum += answer;
	}

	sum = (sum >> 16) + (sum & 0xffff); // add high 16 to low 16
	sum += (sum >> 16); // add carry
	answer = ~sum; // truncate to 16 bits
	return answer;
}

float NetworkUtils::ping(string target) {
	if (target.size() > 0) {
		int s, i, cc, packlen, datalen = DEFDATALEN;
		struct hostent *hp;
		struct sockaddr_in to, from;
		u_char *packet, outpack[MAXPACKET];

		char hnamebuf[MAXHOSTNAMELEN];
		string hostname;
		struct icmp *icp;
		int ret, fromlen, hlen;
		fd_set rfds;
		struct timeval tv;
		int retval;
		struct timeval start, end;
		float end_t;
		bool cont = true;

		to.sin_family = AF_INET;

		// try to convert as dotted decimal address, else if that fails assume it's a hostname
		to.sin_addr.s_addr = inet_addr(target.c_str());

		if (to.sin_addr.s_addr != (u_int) - 1)
			hostname = target;
		else {
			hp = gethostbyname(target.c_str());

			if (!hp) {
				return -1;
			}

			to.sin_family = hp->h_addrtype;
			bcopy(hp->h_addr, (caddr_t) & to.sin_addr, hp->h_length);
			strncpy(hnamebuf, hp->h_name, sizeof(hnamebuf) - 1);
			hostname = hnamebuf;
		}

		packlen = datalen + MAXIPLEN + MAXICMPLEN;

		if ((packet = (u_char *) malloc((u_int) packlen)) == NULL) {
			return -1;
		}

		if ((s = socket(AF_INET, SOCK_RAW, IPPROTO_ICMP)) < 0) {
			return -1;
		}

		icp = (struct icmp *) outpack;
		icp->icmp_type = ICMP_ECHO;
		icp->icmp_code = 0;
		icp->icmp_cksum = 0;
		icp->icmp_seq = 12345;

		static int pId = 100;
		icp->icmp_id = getpid();
		pId++;

		cc = datalen + ICMP_MINLEN;
		icp->icmp_cksum = in_cksum((unsigned short *) icp, cc);


		gettimeofday(&start, NULL);

		i = sendto(s, (char *) outpack, cc, 0, (struct sockaddr *) &to, (socklen_t)sizeof(struct sockaddr_in));

		if (i < 0 || i != cc) {
			if (i < 0) {
				perror("sendto error");
				return -1;
			}
		}

		// Watch stdin (fd 0) to see when it has input.
		FD_ZERO(&rfds);
		FD_SET(s, &rfds);
		tv.tv_sec = 1;
		tv.tv_usec = 0;

		while (cont) {
			retval = select(s + 1, &rfds, NULL, NULL, &tv);

			if (retval == -1) {
				close(s);
				perror("ping: select()");
				return -1;
			}
			else if (retval) {
				fromlen = sizeof(sockaddr_in);

				if ((ret = recvfrom(s, (char *) packet, packlen, 0, (struct sockaddr *) &from, (socklen_t *) & fromlen)) < 0) {
					close(s);
					perror("recvfrom error");
					return -1;
				}

				// Check the IP header
				hlen = sizeof(struct ip);

				if (ret < (hlen + ICMP_MINLEN)) {
					return -1;
				}

				// Now the ICMP part
				icp = (struct icmp *)(packet + hlen);

				if (icp->icmp_type == ICMP_ECHOREPLY) {
					if (icp->icmp_seq != 12345) {
						continue;
					}

					if (icp->icmp_id != getpid()) {
						continue;
					}

					cont = false;
				}
				else {
					continue;
				}

				gettimeofday(&end, NULL);
				end_t = (end.tv_sec - start.tv_sec) * 1000 + ceilf((end.tv_usec - start.tv_usec) / 1000.0);

				close(s);
				return end_t;
			}
			else {
				close(s);
				return 0;
			}
		}
	}

	return 0;
}

std::string NetworkUtils::dig(std::string hostname){
	std::string ret;
	LinuxUtils::exec("dig " + hostname, ret);	
	return ret;
}

std::string NetworkUtils::trace(std::string hostname){
	std::string ret;
	LinuxUtils::exec("traceroute " + hostname, ret);	
	return ret;
}
