/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <fstream>
#include <pthread.h>
#include <time.h>
#include <string>
#include <ctime>
#include <pthread.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <resolv.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netdb.h>
#include <sys/wait.h>
#include <signal.h>
#include <map>
#include <vector>
#include <stdio.h>
#include <string.h>
#include <sstream>

using namespace std;
using std::string;

#include "Utils/FileUtils.h"

bool FileUtils::checkExists(string filename) {
	return access(filename.c_str(), F_OK) == 0;
}

void FileUtils::deleteLine(const char* filename, int l) {
	FILE* fp = fopen(filename, "rt");
	int x = 1;
	char line[1024];
	string fn = "/tmp/delete.temp";

	while (fgets(line, 1024, fp) != NULL) {
		if (x != l) {
			ofstream fp;

			fp.open(fn.c_str(), ios::app);
			fp << line;
			fp.close();
		}
		x++;
	}
	fclose(fp);
	rename(fn.c_str(), filename);
}

/*
 * Updates the current line in the supplied file.  Assumes records are
 * terminated with a newline ('\n').
 *
 * Parameters:
 *     filename - name of file to be updated
 *     targetLineNum - line number of record to be updated (starting with 1)
 *     newLine - updated record
 *
 * Returns:
 *     true - update successful
 *     false - update failed.
 *
 */
bool FileUtils::updateLine(const char* filename, int targetLineNum, const char *newLine) {
	FILE* fp = fopen(filename, "rt");
	string fn = "/tmp/delete.temp";

	FILE *tempFp = fopen(fn.c_str(), "w+");
	int currentLineNum = 1;
	char line[1024];
	bool updated = false;

	while (fgets(line, 1024, fp) != NULL) {
		if (currentLineNum != targetLineNum) {
			fputs(line, tempFp);
		} else {
			fputs(newLine, tempFp);

			if (newLine[strlen(newLine - 1)] != '\n') {
				fputs("\n", tempFp); // Terminate record
			}

			updated = true;
		}

		currentLineNum++;
	}

	fclose(fp);
	fclose(tempFp);
	rename(fn.c_str(), filename);
	return updated;
}

std::string FileUtils::read(const char *filename){
	string str,strTotal;
	ifstream in;
	in.open(filename);
	getline(in,str);
	while ( in ) {
		strTotal += str + "\n";
		getline(in,str);
	}

	return strTotal;
}

void FileUtils::write(const char* filename, std::string input, bool create){
	ofstream outdata;
	if(!create){
		outdata.open(filename, ios::out | ios::app);
	}else{
		outdata.open(filename);
	}

	for (uint i=0; i<input.size(); ++i)
		outdata << input[i];
	outdata.close();
}

