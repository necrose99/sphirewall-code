/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef CHECKSUM_H
#define CHECKSUM_h

/*Static class for generating checksums*/

#include <netinet/in.h>
#include <netinet/ip.h>
#include <netinet/tcp.h>
#include <netinet/udp.h>
#include <arpa/inet.h>
#include <sstream>
#include <vector>

typedef unsigned char u8;
typedef signed char s8;
typedef unsigned short u16;
typedef signed short s16;
typedef unsigned long u32;
typedef signed long s32;

struct tcp_pseudo /*the tcp pseudo header*/ {
	__u32 src_addr;
	__u32 dst_addr;
	__u8 zero;
	__u8 proto;
	__u16 length;
};

class Checksum {
public:
	static long checksum(unsigned short *addr, unsigned int count);
	static long get_tcp_checksum(struct iphdr * myip, struct tcphdr * mytcp);
};

#endif
