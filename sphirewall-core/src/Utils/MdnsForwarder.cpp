/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <net/if.h>
#include <sys/socket.h>
#include <netinet/ip.h>
#include <netinet/udp.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netdb.h>
#include <sys/wait.h>
#include <signal.h>
#include <poll.h>
#include <linux/types.h>
#include <boost/thread.hpp>

#include "Utils/MdnsForwarder.h"
#include "Utils/IP4Addr.h"
#include "Utils/DnsType.h"

#define SIZE 1024
#define MSGBUFSIZE 1500

using namespace std;

void MdnsForwarder::__forward_packet(const char* ifname, unsigned int source, const char* data, int len){
        struct ip ip;
        struct udphdr udp;
        const int on = 1;
        const int off =0;
        struct sockaddr_in sin;
        unsigned char* packet = (unsigned char*)malloc(sizeof(struct ip) + sizeof(struct udphdr) + len);

        ip.ip_hl = 0x5;
        ip.ip_v = 0x4;
        ip.ip_tos = 0x0;
        ip.ip_len = htons(sizeof(struct ip) + sizeof(struct udphdr) + len);
        ip.ip_id = htons(12830);
        ip.ip_off = 0x0;
        ip.ip_ttl = 64;
        ip.ip_p = IPPROTO_UDP;
        ip.ip_sum = 0x0;
        ip.ip_src.s_addr = htonl(source);
        ip.ip_dst.s_addr = inet_addr("224.0.0.251");
        memcpy(packet, &ip, sizeof(struct ip));

	udp.source = htons(5353);
	udp.dest = htons(5353);

	udp.len = htons(sizeof(struct udphdr) + len);
	udp.check = 0;
	memcpy(packet + 20, &udp, sizeof(struct udphdr));
	memcpy(packet + sizeof(struct ip) + sizeof(struct udphdr), data, len);

	int sd = 0;
	if ((sd = socket(AF_INET, SOCK_RAW, IPPROTO_UDP)) < 0) {
		Logger::instance()->log("sphirewalld.mdns.forward_packet", ERROR, "Could not open socket to forward mdns request");
		return;
	}

	if (setsockopt(sd, IPPROTO_IP, IP_HDRINCL, &on, sizeof(on)) < 0) {
		Logger::instance()->log("sphirewalld.mdns.forward_packet", ERROR, "Could not set IP_HDRINCL on socket");
		return;
	}

	if (setsockopt(sd, IPPROTO_IP, IP_MULTICAST_LOOP, &off, sizeof(off)) < 0) {
		Logger::instance()->log("sphirewalld.mdns.forward_packet", ERROR, "Could not set IP_MULTICAST_LOOP on socket");
		return;
	}

	struct ifreq ifr;
	memset(&ifr, 0, sizeof(ifr));
	strncpy(ifr.ifr_name, ifname, sizeof(ifr.ifr_name));
	if (setsockopt(sd, SOL_SOCKET, SO_BINDTODEVICE, (void *)&ifr, sizeof(ifr)) < 0) {
                Logger::instance()->log("sphirewalld.mdns.forward_packet", ERROR, "Could not set SO_BINDTODEVICE on socket");
		return;
	}

	memset(&sin, 0, sizeof(sin));
	sin.sin_family = AF_INET;
	sin.sin_addr.s_addr = ip.ip_dst.s_addr;

	if (sendto(sd, packet, sizeof(struct ip) + sizeof(struct udphdr) + len, 0, (struct sockaddr *)&sin, sizeof(struct sockaddr)) < 0){
		Logger::instance()->log("sphirewalld.mdns.forward_packet", ERROR, "Could not forward mdns query/response, sendto failed");
	}

	free(packet);
	close(sd);
}

void MdnsForwarder::__run_listener(InterfacePtr binding_interface){
	register_listener(binding_interface);
		
	struct sockaddr_in addr;
	int fd, nbytes;
	unsigned int addrlen;
	struct ip_mreq mreq;
	char msgbuf[MSGBUFSIZE];
	u_int yes=1;

	if ((fd=socket(AF_INET,SOCK_DGRAM,0)) < 0) {
                Logger::instance()->log("sphirewalld.mdns.run_listener", ERROR, "Could not open socket to listen for mdns queries/responses");
		unregister_listener(binding_interface);
		return;
	}

	if (setsockopt(fd,SOL_SOCKET,SO_REUSEADDR,&yes,sizeof(yes)) < 0) {
                Logger::instance()->log("sphirewalld.mdns.run_listener", ERROR, "Could not set SO_REUSEADDR on listener");
                unregister_listener(binding_interface);
		close(fd);
                return;
	}
	
	memset(&addr,0,sizeof(addr));
	addr.sin_family=AF_INET;
	addr.sin_addr.s_addr=htonl(INADDR_ANY); 
	addr.sin_port=htons(5353);

	if (::bind(fd,(struct sockaddr *) &addr,sizeof(addr)) < 0) {
                Logger::instance()->log("sphirewalld.mdns.run_listener", ERROR, "Could bind to socket for listener %s", binding_interface->name.c_str());
                unregister_listener(binding_interface);
		close(fd);
                return;
	}

	mreq.imr_multiaddr.s_addr=inet_addr("224.0.0.251");
	mreq.imr_interface.s_addr=htonl(binding_interface->get_primary_ipv4_address());
	if (setsockopt(fd,IPPROTO_IP,IP_ADD_MEMBERSHIP,&mreq,sizeof(mreq)) < 0) {
                Logger::instance()->log("sphirewalld.mdns.run_listener", ERROR, "Could add multicast membership on listener %s", binding_interface->name.c_str());
                unregister_listener(binding_interface);
		close(fd);
                return;
	}

	/* now just enter a read-print loop */
	struct pollfd fds[1];
	fds[0].fd = fd;
	fds[0].events = POLLIN;

	while(poll(fds, 1, 2000) != -1 && should_run_listener(binding_interface))
	{
		if((fds[0].revents & POLLIN) != 0) {
			addrlen=sizeof(addr);
			if ((nbytes=recvfrom(fd,msgbuf,MSGBUFSIZE-1,0, (struct sockaddr *) &addr,&addrlen)) < 0) {
				Logger::instance()->log("sphirewalld.mdns.run_listener", ERROR, "Could not recvfrom message on listener %s", binding_interface->name.c_str());
				unregister_listener(binding_interface);
				close(fd);
				return;
			}

			msgbuf[nbytes] = '\0';
			__process_dns_message(binding_interface, (char*) &msgbuf, nbytes);
		}
	}

	Logger::instance()->log("sphirewalld.mdns.run_listener", INFO, "Closing listener on %s", binding_interface->name.c_str());
	unregister_listener(binding_interface);
	close(fd);
}

void MdnsForwarder::__process_dns_message(InterfacePtr binding_interface, char* packet, int numbytes){
	//Deal with an indidual message from a interface thread

	for(MdnsForwarderBridgePtr rule : get_rules()){
		if(rule->should_forward(binding_interface)){
			for(InterfacePtr forward_target : rule->resolved_members){
				if(forward_target->ifid != binding_interface->ifid){
					__forward_packet(forward_target->name.c_str(), forward_target->get_primary_ipv4_address(), (const char*) packet, numbytes);
				}
			}
		}
	}
}

void MdnsForwarder::run(){
	while(true){
		for(MdnsForwarderBridgePtr rule : get_rules()){
			for(InterfacePtr forward_target : rule->resolved_members){
				if(!has_registered_listener(forward_target)){
					Logger::instance()->log("sphirewalld.mdns.run", INFO, "Launching interface listener on '%s'", forward_target->name.c_str());

					boost::thread(boost::bind(&MdnsForwarder::__run_listener, this, forward_target));
					sleep(2);
				}
			}
		}

		sleep(5);
	}
}

void MdnsForwarder::save(){
	ObjectContainer *root= new ObjectContainer(CREL);
	ObjectContainer *configDevices = new ObjectContainer(CARRAY);

	for(MdnsForwarderBridgePtr bridge: get_rules()){
		ObjectContainer* configDevice = new ObjectContainer(CREL);
		configDevice->put("id", new ObjectWrapper((string) bridge->id));
		configDevice->put("name", new ObjectWrapper((string) bridge->name));

		ObjectContainer* slaves= new ObjectContainer(CARRAY);
		for(string slave : bridge->members){
			slaves->put(new ObjectWrapper((string) slave));
		}

		configDevice->put("slaves", new ObjectWrapper(slaves));
		configDevices->put(new ObjectWrapper(configDevice));
	}

	root->put("rules", new ObjectWrapper(configDevices));

	configurationManager->holdLock();
	configurationManager->setElement("mdns", root);
	configurationManager->save();
	configurationManager->releaseLock();
}

bool MdnsForwarder::load(){
	if (configurationManager->has("mdns")) {
		ObjectContainer *configRoot = configurationManager->getElement("mdns");
		ObjectContainer *configDevices = configRoot->get("rules")->container();
		for (int x = 0; x < configDevices->size(); x++) {
			ObjectContainer *o = configDevices->get(x)->container();
			MdnsForwarderBridgePtr bridge = MdnsForwarderBridgePtr(new MdnsForwarderBridge());
			bridge->id = o->get("id")->string();
			bridge->name = o->get("name")->string();

			ObjectContainer* slaves = o->get("slaves")->container();
			for(int y = 0; y < slaves->size(); y++){
				bridge->members.push_back(slaves->get(y)->string());					
			}

			refresh_rule(bridge);
			add_rule(bridge);	
		}	
	}
	return true;
}

