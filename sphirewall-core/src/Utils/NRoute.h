/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef NROUTE_H
#define NROUTE_H

#include <boost/shared_ptr.hpp>
#include "Core/ConfigurationManager.h"
#include "Utils/Interfaces.h"

class NRoute {
	public:
		NRoute(){
			destination = 0;
			destination_cidr = 0;
			route_device = 0;
			route_nexthop = 0;
			route_table_id = 0;
			link_scope = false;
		}

		unsigned int destination;
		unsigned int destination_cidr;

		int route_device;
		unsigned int route_nexthop;

		int route_table_id;
		bool link_scope;

		void set_destination_from_string(std::string input);
		void set_route_nexthop_from_string(std::string input);

		std::string to_string();
};

typedef boost::shared_ptr<NRoute> NRoutePtr;

class PersistedNRoute {
        public:
                NRoutePtr route;
                std::string id;
};

typedef boost::shared_ptr<PersistedNRoute> PersistedNRoutePtr;

class NRoutingRule {
	public:
		int route_table_id;
	
		unsigned source_ip;
		int fw_mark;

		std::string to_string();
};

class NRouteManager : public IntMgrChangeListener, public Configurable {
	public:
		NRouteManager(){}
		bool load();
		void save();

		void add_persisted_route_entry(PersistedNRoutePtr route);
		void del_persisted_route_entry(PersistedNRoutePtr route);
		std::list<PersistedNRoutePtr> list_persisted_routes();
		PersistedNRoutePtr get_persisted_route_entry(std::string id);

		void add_route_entry(NRoutePtr route);
		void del_route_entry(NRoutePtr route);

		void add_route_rule(NRoutingRule* rule);
		void del_route_rule(NRoutingRule* rule);

		const char* getConfigurationSystemName(){
			return "NRoute Manager";
		}

		void interface_change(InterfacePtr interface_changed);

	private:
		std::list<PersistedNRoutePtr> persisted_routes;
};

#endif
