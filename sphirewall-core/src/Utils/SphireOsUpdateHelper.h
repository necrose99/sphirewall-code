#ifndef SPHIREOS_UPDATE_HELPER_H
#define SPHIREOS_UPDATE_HELPER_H

#include "Utils/LinuxUtils.h"

#define UPDATE_NOT_RUNNING 0
#define UPDATE_RUNNING 1
#define UPDATE_FINISHED 2

class SphireOsUpdateHelper {
	public:
		SphireOsUpdateHelper();
		std::string get_log();
		void start_update();
		int status();

	private:
		DetachedProcessWithOutput* process;
};

#endif
