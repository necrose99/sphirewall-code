/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <string.h>

#include <asm/types.h>
#include <linux/netlink.h>
#include <linux/rtnetlink.h>
#include <sys/socket.h>

extern "C" {
        #include <libnfnetlink/libnfnetlink.h>
}

enum {
        FRA_UNSPEC,
        FRA_DST,        /* destination address */
        FRA_SRC,        /* source address */
        FRA_IIFNAME,    /* interface name */
#define FRA_IFNAME      FRA_IIFNAME
        FRA_GOTO,       /* target to jump to (FR_ACT_GOTO) */
        FRA_UNUSED2,
        FRA_PRIORITY,   /* priority/preference */
        FRA_UNUSED3,
        FRA_UNUSED4,
        FRA_UNUSED5,
        FRA_FWMARK,     /* mark */
        FRA_FLOW,       /* flow/class id */
        FRA_UNUSED6,
        FRA_SUPPRESS_IFGROUP,
        FRA_SUPPRESS_PREFIXLEN,
        FRA_TABLE,      /* Extended table id */
        FRA_FWMASK,     /* mask for netfilter mark */
        FRA_OIFNAME,
        __FRA_MAX
};


using namespace std;

#include "Utils/IP4Addr.h"
#include "Utils/NRoute.h"
#include "Core/System.h"

int addattr_l(struct nlmsghdr *n, int type, const void *data, int alen);
in_addr_t netmask( int prefix );

void NRouteManager::add_route_entry(NRoutePtr route){
	Logger::instance()->log("sphirewalld.nroutemanager", INFO, "Publishing route to rtnetlink '%s'", route->to_string().c_str());

	struct {
		struct nlmsghdr  nh;
		struct rtmsg rt;
		char             attrbuf[512];
	} req;

	struct rtattr *rta;
	int rtnetlink_sk = socket(AF_NETLINK, SOCK_DGRAM, NETLINK_ROUTE);

	memset(&req, 0, sizeof(req));
	req.nh.nlmsg_len = NLMSG_LENGTH(sizeof(struct rtmsg));
	req.nh.nlmsg_flags = NLM_F_REQUEST|NLM_F_CREATE|NLM_F_EXCL;
	req.nh.nlmsg_type = RTM_NEWROUTE;

	req.rt.rtm_family = AF_INET;
	req.rt.rtm_type = RTN_UNICAST;
	req.rt.rtm_dst_len = route->destination_cidr;
	req.rt.rtm_protocol = RTPROT_UNSPEC;

	if(!route->link_scope){
		req.rt.rtm_scope = RT_SCOPE_UNIVERSE;
	}else{
		req.rt.rtm_scope = RT_SCOPE_LINK;
	}
	req.rt.rtm_table = route->route_table_id;

	struct ip_param {
		unsigned int ip;
	} dest_ip;

	memset(&dest_ip, 0, sizeof(dest_ip));
	dest_ip.ip = htonl(route->destination);

	addattr_l(&req.nh, RTA_DST, &dest_ip.ip, sizeof(dest_ip));
	addattr_l(&req.nh, RTA_OIF, &route->route_device, sizeof(route->route_device));

	if(route->route_nexthop != 0){
		unsigned int gateway = htonl(route->route_nexthop);
		addattr_l(&req.nh, RTA_GATEWAY, &gateway, sizeof(gateway));
	}

	send(rtnetlink_sk, &req, req.nh.nlmsg_len, 0);
	close(rtnetlink_sk);
}

void NRouteManager::del_route_entry(NRoutePtr route){
        Logger::instance()->log("sphirewalld.nroutemanager", INFO, "Removing route from rtnetlink %s", route->to_string().c_str());
	
	struct {
		struct nlmsghdr  nh;
		struct rtmsg rt;
		char             attrbuf[512];
	} req;

	struct rtattr *rta;
	int rtnetlink_sk = socket(AF_NETLINK, SOCK_DGRAM, NETLINK_ROUTE);

	memset(&req, 0, sizeof(req));
	req.nh.nlmsg_len = NLMSG_LENGTH(sizeof(struct rtmsg));
	req.nh.nlmsg_flags = NLM_F_REQUEST|NLM_F_CREATE|NLM_F_EXCL;
	req.nh.nlmsg_type = RTM_DELROUTE;

	req.rt.rtm_family = AF_INET;
	req.rt.rtm_type = RTN_UNICAST;
	req.rt.rtm_dst_len = route->destination_cidr;
	req.rt.rtm_protocol = RTPROT_UNSPEC;
	req.rt.rtm_scope = RT_SCOPE_UNIVERSE;
	req.rt.rtm_table = route->route_table_id;

	struct ip_param {
		unsigned int ip;
	} dest_ip;

	memset(&dest_ip, 0, sizeof(dest_ip));
	dest_ip.ip = htonl(route->destination);

	unsigned int gateway = htonl(route->route_nexthop);
	int ifid = route->route_device;

	addattr_l(&req.nh, RTA_DST, &dest_ip.ip, sizeof(dest_ip));
	addattr_l(&req.nh, RTA_OIF, &ifid, sizeof(ifid));
	addattr_l(&req.nh, RTA_GATEWAY, &gateway, sizeof(gateway));

	send(rtnetlink_sk, &req, req.nh.nlmsg_len, 0);
        close(rtnetlink_sk);
}

void NRouteManager::add_route_rule(NRoutingRule* rule){
        Logger::instance()->log("sphirewalld.nroutemanager", INFO, "Publishing routing rule to rtnetlink '%s'", rule->to_string().c_str());

	struct {
		struct nlmsghdr  nh;
		struct rtmsg rt;
		char             attrbuf[512];
	} req;

	struct rtattr *rta;
	int rtnetlink_sk = socket(AF_NETLINK, SOCK_DGRAM, NETLINK_ROUTE);

	memset(&req, 0, sizeof(req));
	req.nh.nlmsg_len = NLMSG_LENGTH(sizeof(struct rtmsg));
	req.nh.nlmsg_flags = NLM_F_REQUEST|NLM_F_CREATE|NLM_F_EXCL;
	req.nh.nlmsg_type = RTM_NEWRULE;

	req.rt.rtm_family = AF_INET;
	req.rt.rtm_type = RTN_UNICAST;

	req.rt.rtm_protocol = RTPROT_UNSPEC;
	req.rt.rtm_scope = RT_SCOPE_UNIVERSE;
	req.rt.rtm_table = rule->route_table_id;

	if(rule->source_ip != 0){
		struct ip_param {
			unsigned int ip;
		} source_ip;

		req.rt.rtm_src_len = 32;
		memset(&source_ip, 0, sizeof(source_ip));
		source_ip.ip = htonl(rule->source_ip);

		addattr_l(&req.nh, FRA_SRC, &source_ip.ip, sizeof(source_ip));
	}

	if(rule->fw_mark != 0){
		addattr_l(&req.nh, FRA_FWMARK, &rule->fw_mark, sizeof(rule->fw_mark));
	}

	send(rtnetlink_sk, &req, req.nh.nlmsg_len, 0);
        close(rtnetlink_sk);
}

void del_route_rule(NRoutingRule* rule);

bool NRouteManager::load(){
	persisted_routes.clear();
	if (configurationManager->has("routing")) {
		ObjectContainer *configRoot = configurationManager->getElement("routing");
                ObjectContainer *configStatic= configRoot->get("static")->container();

		for (int x = 0; x < configStatic->size(); x++) {
			ObjectContainer *route= configStatic->get(x)->container();

			PersistedNRoutePtr target(new PersistedNRoute());
			target->route = NRoutePtr(new NRoute());
			
			if(route->has("destination")){
				target->route->destination = IP4Addr::stringToIP4Addr(route->get("destination")->string());
				target->route->destination_cidr = route->get("destination_cidr")->number();
			}

			if(route->has("route_device")){
				InterfacePtr interface = System::getInstance()->get_interface_manager()->get_interface(route->get("route_device")->string());
				if(interface){
					target->route->route_device = interface->ifid;
				}
			}

			if(route->has("route_nexthop")){
				target->route->route_nexthop = IP4Addr::stringToIP4Addr(route->get("route_nexthop")->string());
			}
	
			target->id = route->get("id")->string();
			add_persisted_route_entry(target);
		}
	}

	return true;
}

void NRouteManager::save(){
	ObjectContainer *root = new ObjectContainer(CREL); 
	ObjectContainer *static_routes = new ObjectContainer(CARRAY); 

	for(PersistedNRoutePtr route : persisted_routes){
		ObjectContainer* ow_route = new ObjectContainer(CREL);

		ow_route->put("id", new ObjectWrapper((string) route->id));

		if(route->route->destination != 0){
			ow_route->put("destination", new ObjectWrapper((string) IP4Addr::ip4AddrToString(route->route->destination)));
			ow_route->put("destination_cidr", new ObjectWrapper((double) route->route->destination_cidr));
		}

		if(route->route->route_device != 0){
			InterfacePtr interface = System::getInstance()->get_interface_manager()->get_interface_by_id(route->route->route_device);
			if(interface){
				ow_route->put("route_device", new ObjectWrapper((string) interface->name));
			}
		}

		if(route->route->route_nexthop != 0){
			ow_route->put("route_nexthop", new ObjectWrapper((string) IP4Addr::ip4AddrToString(route->route->route_nexthop)));
		}

		static_routes->put(new ObjectWrapper(ow_route));
	}

        root->put("static", new ObjectWrapper(static_routes));
	
	configurationManager->holdLock();
        configurationManager->setElement("routing", root);
        configurationManager->save();
	configurationManager->releaseLock();
}

void NRouteManager::add_persisted_route_entry(PersistedNRoutePtr route){
	persisted_routes.push_back(route);
	add_route_entry(route->route);			
}

void NRouteManager::del_persisted_route_entry(PersistedNRoutePtr route){
	persisted_routes.remove(route);
	del_route_entry(route->route);
}

std::list<PersistedNRoutePtr> NRouteManager::list_persisted_routes(){
	return persisted_routes;
}

PersistedNRoutePtr NRouteManager::get_persisted_route_entry(std::string id){
	for(PersistedNRoutePtr route : persisted_routes){
		if(route->id.compare(id) == 0){
			return route;
		}
	}

	return PersistedNRoutePtr();
}

void NRouteManager::interface_change(InterfacePtr interface){
	if(!interface || interface->deleting){
		return;
	}

	Logger::instance()->log("sphirewalld.nroutemanager.change", INFO, "Interface change notification received, updating routing tables for device '%s'", interface->name.c_str());
	for(PersistedNRoutePtr route : persisted_routes){
		add_route_entry(route->route);		
	}

	if(interface->gateway != 0){
		for(IPv4AddressPtr existing_ip : interface->ipv4_addresses){
			NRoute* default_route_table = new NRoute();
			default_route_table->route_device = interface->ifid;
			default_route_table->route_nexthop = interface->gateway;
			default_route_table->route_table_id = interface->ifid;
			add_route_entry(NRoutePtr(default_route_table));

			NRoute* interface_route_table = new NRoute();
			interface_route_table->destination_cidr = __builtin_popcount(existing_ip->mask);
			interface_route_table->destination = (existing_ip->ip & existing_ip->mask);
			interface_route_table->route_device = interface->ifid;
			interface_route_table->route_table_id = interface->ifid;
			interface_route_table->link_scope= true;
			add_route_entry(NRoutePtr(interface_route_table));

			NRoutingRule* mark_rule = new NRoutingRule();
			mark_rule->fw_mark = interface->ifid;
			mark_rule->route_table_id = interface->ifid;
			mark_rule->source_ip = 0;
			add_route_rule(mark_rule);

			NRoutingRule* ip_rule = new NRoutingRule();
			ip_rule->source_ip = interface->gateway;
			ip_rule->route_table_id = interface->ifid;
			add_route_rule(ip_rule);
		}

		NRoute* route = new NRoute();
		route->route_device = interface->ifid;
		route->route_nexthop = interface->gateway;
		add_route_entry(NRoutePtr(route));
	}
}

#define IP_MAX_BUF 1024
void NRoute::set_destination_from_string(std::string input){
	if(input.size() > IP_MAX_BUF){
		return;
	}

	char buf[IP_MAX_BUF];
	sscanf(input.c_str(), "%s/%d", &buf, &destination_cidr);
	destination = IP4Addr::stringToIP4Addr(buf);
}

void NRoute::set_route_nexthop_from_string(std::string input){
	route_nexthop = IP4Addr::stringToIP4Addr(input);
}

std::string NRoute::to_string(){
	std::stringstream ss;
	if(destination != 0){
		ss << " dest " << IP4Addr::ip4AddrToString(destination) << "/" << destination_cidr;
	}

	if(route_nexthop != 0){
		ss << " via " << IP4Addr::ip4AddrToString(route_nexthop);
	}

	if(route_device != 0){
		InterfacePtr interface = System::getInstance()->get_interface_manager()->get_interface_by_id(route_device);
		if(interface){
			ss << " dev " << interface->name;
		}
	}

	ss << " table " << route_table_id;
	return ss.str();
}

std::string NRoutingRule::to_string(){
	std::stringstream ss;
	ss << " table id '" << route_table_id << "'";
	if(source_ip != 0){
		ss << " source " << IP4Addr::ip4AddrToString(source_ip);
	}

	ss << " mark '" << fw_mark << "'";
	return ss.str();
} 
