/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include "Auth/Group.h"
#include "Core/QuotaManager.h"
#include "Utils/StringUtils.h"

using namespace std;

Group::Group()
	: id(0), name(""), desc(""), manager(""), allow_mui(false), quota()
{}

Group::Group(int id, string name, string desc, string manager, bool allow_mui)
	: id(id), name(name), desc(desc), manager(manager), allow_mui(allow_mui), quota(){
	this->name = StringUtils::to_lower(name);
}


int Group::getId() const {
	return id;
}

std::string Group::getName() const {
	return name;
}

void Group::setId(int id) {
	this->id = id;
}

QuotaInfo &Group::getQuota() {
	return quota;
}

std::string Group::getDesc() const {
	return this->desc;
}

std::string Group::getManager() const {
	return this->manager;
}

bool Group::isAllowMui() const {
	return this->allow_mui;
}

void Group::setDesc(std::string desc) {
	this->desc = desc;
}

void Group::setManager(std::string manager) {
	this->manager = manager;
}

void Group::setAllowMui(bool a) {
	this->allow_mui = a;
}

void Group::setName(string name) {
	this->name = StringUtils::to_lower(name);
}

void Group::setQuota(QuotaInfo &quota) {
	this->quota = quota;
}

void Group::setMetadata(std::string metadata){
	this->metadata = metadata;
}

std::string Group::getMetadata() const {
	return metadata;
}

