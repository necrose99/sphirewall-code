/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <string>
#include <netinet/ip6.h>
#include <time.h>

using namespace std;

#include "Core/System.h"
#include "Utils/Utils.h"
#include "Utils/IP4Addr.h"
#include "Utils/IP6Addr.h"
#include "Auth/Session.h"
#include "Auth/User.h"
#include "SFwallCore/Packet.h"
#include <Utils/TimeManager.h>
#include <netinet/in.h>

Session::Session(UserPtr u, string mac, unsigned int ipaddress, int timeout, bool absolute)
	: user(u), macAddr(mac.substr(0, 17)), sessionStartTime(System::getInstance()->getTimeManager().time()),
	  lastPacketTime(System::getInstance()->getTimeManager().time()), ip(ipaddress), type(IPV4),
	  sessionTimeoutLength(timeout), absoluteTimeout(absolute)
{}

Session::Session(UserPtr u, string mac, struct in6_addr ipaddress, int timeout, bool absolute)
	: user(u), macAddr(mac.substr(0, 17)), sessionStartTime(System::getInstance()->getTimeManager().time()),
	  lastPacketTime(System::getInstance()->getTimeManager().time()), ipv6(ipaddress), type(IPV6),
	  sessionTimeoutLength(timeout), absoluteTimeout(absolute)
{}

Session::Session()
{}

string Session::getMac() const {
	return macAddr;
}

string Session::getUserName() const {
	return user->getUserName();
}

int Session::getStartTime() const {
	return sessionStartTime;
}

int Session::getSessionLast() const {
	return lastPacketTime;
}

int Session::getTimeoutLength() const {
	return sessionTimeoutLength;
}

void Session::touch() {
	lastPacketTime = System::getInstance()->getTimeManager().time();
}

UserPtr Session::getUser() const {
	return user;
}

int Session::getType() const {
	return type;
}

string Session::getIp() {
	return (type == IPV6) ? IP6Addr::toString(&ipv6) : IP4Addr::ip4AddrToString(getIp_Int());
}

unsigned int Session::getIp_Int() const {
	return ip;
}

bool Session::isTimedOut() {
	if(absoluteTimeout){
		return System::getInstance()->getTimeManager().time() > getTimeoutLength();
	}

	int when = getSessionLast() + getTimeoutLength();
	int now = System::getInstance()->getTimeManager().time();
	return now > when;
}
