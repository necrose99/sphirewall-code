/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SPHIREWALL_USERS_H_INCLUDED
#define SPHIREWALL_USERS_H_INCLUDED

#include <string>
#include <vector>
#include <map>

using namespace std;

#include "Auth/Group.h"
#include "Core/QuotaManager.h"
#include "Utils/Lock.h"
#include "Auth/AuthenticationType.h"
#include "Core/RuleMetrics.h"

extern bool validateUserInfo(
	string username, string password, string fname,string lname, vector<Group> groups
);

class QuotaInfo;
class User {
	public:
		friend class UserDb;
		//Public member variables
		User();
		User(string username);
		~User();

		void addGroup(GroupPtr group);
		void removeGroup(GroupPtr group);
		bool checkForGroup(GroupPtr group);
		bool enable();
		bool disable();

		string getUserName();
		string getFname();
		string getLname();

		string getPassword();
		bool getIsEnabled();
		int getLastLogin();
		int getLastFail();

		std::string getEmail() const;

		void setFname(string str);
		void setLname(string str);

		void setGroups(vector<GroupPtr> groups);
		bool enableDisable(bool enable);
		void setPassword(string password);

		void setEmail(std::string email);
		bool isAllowedMuiAccess();

		void setUserName(std::string n);
		std::vector<GroupPtr> getGroups() const;

		int getCreationTime() const;
		void setCreationTime(int t);

		QuotaCounter* getQuotaCounter() const{
			return quotaCounter;
		}
		QuotaInfo* getQuota() const;
		bool isQuotaExceeded() const;
		void setQuotaExceeded(bool value);

		std::string dn;
		bool temp_user;
		int expiry_timestamp;
		RuleMetrics* rule_metrics;
		
	protected:
		bool isEnabled;

	private:
		string fname;
		string lname;

		string password;

		int lastLogin;
		int lastFail;
		int creationTime;

		vector<GroupPtr> groups;
		string user;

		bool isTemp;
		map<string, int> hosts;

		string email;
		//Quota Management
		QuotaInfo *quota;
		QuotaCounter *quotaCounter;
		bool quotaExceeded;
		Lock *groupLock;
};

typedef boost::shared_ptr<User> UserPtr;

#endif
