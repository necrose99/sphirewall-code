#include <iostream>
#include "Auth/WindowsWmi.h"

#include "Core/System.h"
#include <boost/regex.hpp>

#include "Utils/LinuxUtils.h"
#include "Utils/StringUtils.h"

using namespace std;

string attribute(string event, string key) {
        stringstream exp;
        exp << key << ":" << "([\\s]+)([:,a-z,A-Z,0-9\\.,@,\\$,-]+)";
        boost::regex req(exp.str());
        boost::smatch what2;

        while (boost::regex_search(event, what2, req)) {
                return what2[2];
        }

        return "";
}

std::string WinDomainController::query(std::string input) {
        Logger::instance()->log("sphirewalld.wmic.query", DEBUG, "executing wmic query '%s'", input.c_str());
        stringstream queryIds;
        queryIds << "wmic -U " << domain << "/" << username << " --password \"" << password << "\" //" << hostname << " \"" << input << "\"";

        string ret;
        if (LinuxUtils::exec(queryIds.str(), ret) != 0) {
		Logger::instance()->log("sphirewalld.wmic.query", ERROR, "error executing wmic query '%s'", input.c_str());

                throw WmiException(ret);
        }

        return ret;
}

void WinDomainController::check() {
        query("Select RecordNumber from Win32_NTLogEvent where RecordNumber = 1");
}

void WinDomainController::updateCursor(std::string input) {
        int value = stoi(input);

        if (value > cursor) {
                cursor = value;
        }
}

void WinDomainController::seedCursor(std::string event_identifier) {
        if(cursor != 0){
                return;
        }

        stringstream q;
        q << "Select RecordNumber from Win32_NTLogEvent Where EventIdentifier = " << event_identifier << " And RecordNumber > " << cursor;
        string ret = query(q.str());
        boost::regex reg("Security|([0-9]+)");
        boost::smatch what;

        std::string::const_iterator start, end;
        start = ret.begin();
        end = ret.end();

        while (boost::regex_search(start, end, what, reg)) {
                string is = what[1];
                int i = atoi(is.c_str());
                if(i > cursor){
                	cursor = i;
                }
                start = what[0].second;
        }
}

list<string> WinDomainController::findEventsSince(std::string event_identifier) {
        stringstream q;
        q << "Select RecordNumber from Win32_NTLogEvent Where EventIdentifier = "<< event_identifier << " And RecordNumber > " << cursor;
        string ret = query(q.str());
        boost::regex reg("Security|([0-9]+)");
        boost::smatch what;

        std::string::const_iterator start, end;
        start = ret.begin();
        end = ret.end();

        list<std::string> items;

        while (boost::regex_search(start, end, what, reg)) {
                items.push_back(what[1]);
                start = what[0].second;
        }

        items.remove("");
        return items;
}

std::string WinDomainController::getRawEvent(std::string id) {
        stringstream ss;
        ss << "Select Message from Win32_NTLogEvent Where RecordNumber = ";
        ss << id;
        return query(ss.str());
}

