/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include "Auth/WindowsWmiAuthenticator.h"
#include "Core/System.h"
#include <boost/regex.hpp>

#include "Core/Event.h"
#include "Core/ConfigurationManager.h"
#include "Auth/User.h"
#include "Auth/UserDb.h"
#include "Utils/IP4Addr.h"
#include "Auth/Session.h"
#include "Core/HostDiscoveryService.h"
#include "Utils/LinuxUtils.h"
#include "Utils/StringUtils.h"
#include "Auth/WindowsWmi.h"

using namespace std;

void WinWmiAuth::save(){
	ObjectContainer *root = new ObjectContainer(CREL);
	ObjectContainer *controllers= new ObjectContainer(CARRAY);

	for(WinDomainControllerPtr dc : domain_controllers){
		ObjectContainer* cc = new ObjectContainer(CREL);
		cc->put("hostname", new ObjectWrapper(dc->hostname));		
		cc->put("domain", new ObjectWrapper(dc->domain));		
		cc->put("username", new ObjectWrapper(dc->username));		
		cc->put("id", new ObjectWrapper(dc->id));		
		cc->put("password", new ObjectWrapper(dc->password));		
		cc->put("mode", new ObjectWrapper((double) dc->mode));		
		cc->put("enabled", new ObjectWrapper((bool) dc->enabled));		

		ObjectContainer* exceptions = new ObjectContainer(CARRAY);
		for(std::string user : dc->exceptions){
			exceptions->put(new ObjectWrapper((std::string) user));
		}
		cc->put("exceptions", new ObjectWrapper(exceptions));
		controllers->put(new ObjectWrapper(cc));
	}

	root->put("controllers", new ObjectWrapper(controllers));
	configurationManager->holdLock();
	configurationManager->setElement("wmi", root);
	configurationManager->save();
	configurationManager->releaseLock();
}

bool WinWmiAuth::load(){
	if(configurationManager->has("authentication:wmic")){
		if(configurationManager->get("authentication:wmic:enabled")->boolean()){
			vector<string> all_hostnames_list;
			split(configurationManager->get("authentication:wmic:hostname")->string(), ',', all_hostnames_list);
			for(string hostname : all_hostnames_list){
				WinDomainControllerPtr temp = WinDomainControllerPtr(new WinDomainController());
				temp->enabled = configurationManager->get("authentication:wmic:enabled")->boolean();
				temp->domain= configurationManager->get("authentication:wmic:domain")->string();
				temp->hostname = hostname;
				temp->username= configurationManager->get("authentication:wmic:username")->string();
				temp->password= configurationManager->get("authentication:wmic:password")->string();
				temp->id= "oldrule";
				temp->mode = WMI_DOMAIN_HANDLER;
				domain_controllers.push_back(temp);
			}
			configurationManager->getElement("authentication")->remove("wmic");
			save();
			return true;
		}
	}	

	if(configurationManager->has("wmi")){
		ObjectContainer *root = configurationManager->getElement("wmi");
		ObjectContainer *controllers= root->get("controllers")->container();

		for (int x = 0; x < controllers->size(); x++) {
			ObjectContainer *o = controllers->get(x)->container();
			WinDomainControllerPtr temp = WinDomainControllerPtr(new WinDomainController());
			temp->hostname = o->get("hostname")->string();
			temp->username = o->get("username")->string();
			temp->password= o->get("password")->string();
			temp->domain= o->get("domain")->string();
			temp->id= o->get("id")->string();
			temp->mode= o->get("mode")->number();
			temp->enabled= o->get("enabled")->boolean();

			if(o->has("exceptions")){
				ObjectContainer* e = o->get("exceptions")->container();
				for(int y = 0; y < e->size(); y++){
					temp->exceptions.insert(e->get(y)->string());
				}
			}	

			domain_controllers.push_back(temp);
		}
	}

	return true;
}

void WinWmiAuth::__process_wminps_event(WinDomainControllerPtr controller, std::string record_number){
	Logger::instance()->log("sphirewalld.wmic.poll", INFO,
			"__process_wminps_event for id '%s'", record_number.c_str());

	std::string event_raw_message = controller->getRawEvent(record_number);
	std::string username = attribute(event_raw_message, "Account Name");
	std::string client_mac_address = StringUtils::normalize_mac_address(attribute(event_raw_message, "Calling Station Identifier"));
	if(!(username.size() > 0 && client_mac_address.size() > 0)){
		Logger::instance()->log("sphirewalld.wmic.__process_wminps_event", ERROR, "Invalid NPS event with username '%s' and mac '%s' via dc '%s'",
				username.c_str(), client_mac_address.c_str(), controller->hostname.c_str());

		controller->updateCursor(record_number);
		return;
	}

	Logger::instance()->log("sphirewalld.wmic.__process_wminps_event", INFO, "Found a NPS user login event for user '%s' on client '%s' via dc '%s'", 
			username.c_str(), client_mac_address.c_str(), controller->hostname.c_str());

	list<HostPtr> host_entries = arp->get_by_mac(client_mac_address);
	if (host_entries.size() == 0) {
		EventParams event_params;
		event_params["dc"] = controller->hostname;
		event_params["user"] = username;
		event_params["mac"] = client_mac_address;
		eventDb->add(new Event(USERDB_COULD_NOT_FIND_HOST, event_params));
		Logger::instance()->log("sphirewalld.wmic.__process_wminps_event", INFO, "Could not find a host entry for '%s'", client_mac_address.c_str());

		controller->updateCursor(record_number);
		return;
	}

	if(controller->exceptions.find(username) != controller->exceptions.end()){
		controller->updateCursor(record_number);
		Logger::instance()->log("sphirewalld.wmic.__process_wminps_event", INFO,
				"Ignoring NPS event from '%s' on dc '%s'", username.c_str(), controller->hostname.c_str());

		return;
	}

	UserPtr user = System::getInstance()->getUserDb()->getUser(username, true);
	if (!user) {
		EventParams event_params;
		event_params["dc"] = controller->hostname;
		event_params["user"] = username; 
		event_params["mac"] = client_mac_address;
		eventDb->add(new Event(USERDB_LOGIN_FAILED, event_params));

		Logger::instance()->log("sphirewalld.wmic.__process_wminps_event", ERROR,
				"Could not find user account for new session '%s' on dc '%s'", username.c_str(), controller->hostname.c_str());

		controller->updateCursor(record_number);
		return;
	}

	for(HostPtr host_entry : host_entries){
		if(host_entry->authenticated_user){
			if (host_entry->authenticated_user == user) {
				controller->updateCursor(record_number);
				return;
			}
			else {
				arp->deauthenticate_user_to_host(host_entry, user);
			}
		}

		arp->authenticate_user_to_host(host_entry, user, "provider.wmi.nps", -1, false);
	}

	controller->updateCursor(record_number);
}

void WinWmiAuth::__process_wmidc_event(WinDomainControllerPtr controller, std::string record_number){
	WmiLoginEvent event(record_number, controller->getRawEvent(record_number));
	if (event.isValidUser()) {
		Logger::instance()->log("sphirewalld.wmic.__process_wmidc_event", INFO, "Found a kerberos user login event '%s' on dc '%s'", event.toString().c_str(), controller->hostname.c_str());

		list<HostPtr> host_entries = arp->get_by_ip(IP4Addr::stringToIP4Addr(event.getIpv4Address()));
		if (host_entries.size() == 0) {
			EventParams event_params;
			event_params["dc"] = controller->hostname;
			event_params["user"] = event.getPlainUsername();
			event_params["ip"] = event.getIpv4Address();
			eventDb->add(new Event(USERDB_COULD_NOT_FIND_HOST, event_params));

			Logger::instance()->log("sphirewalld.wmic.__process_wmidc_event", ERROR,
					"Could not find mac address for new session '%s' on dc '%s'", event.toString().c_str(), controller->hostname.c_str());

			controller->updateCursor(record_number);
			return;
		}

		if(controller->exceptions.find(event.getPlainUsername()) != controller->exceptions.end()){
			controller->updateCursor(record_number);
			Logger::instance()->log("sphirewalld.wmic.__process_wmidc_event", INFO, 
					"Ignoring kerberos event from '%s' on dc '%s'", event.getPlainUsername().c_str(), controller->hostname.c_str());

			return;
		}

		UserPtr user = System::getInstance()->getUserDb()->getUser(event.getPlainUsername(), true);
		if (!user) {
			EventParams event_params;
			event_params["dc"] = controller->hostname;
			event_params["user"] = event.getPlainUsername();
			event_params["ip"] = event.getIpv4Address();
			eventDb->add(new Event(USERDB_LOGIN_FAILED, event_params));

			Logger::instance()->log("sphirewalld.wmic.__process_wmidc_event", ERROR,
					"Could not find user account for new session '%s' on dc '%s'", event.toString().c_str(), controller->hostname.c_str());

			controller->updateCursor(record_number);
			return;
		}

		for(HostPtr host_entry : host_entries){
			if(host_entry->authenticated_user){
				if (host_entry->authenticated_user == user) {
					controller->updateCursor(record_number);
					return;
				}
				else {
					arp->deauthenticate_user_to_host(host_entry, user);
				}
			}

			arp->authenticate_user_to_host(host_entry, user, "provider.wmi.dc", -1, false);
		}
	}
	controller->updateCursor(record_number);
}

void WinWmiAuth::__poll_wmicdc(WinDomainControllerPtr controller){
	controller->active = true;

	try {
		while(controller->enabled){
			controller->seedCursor("4768");
			for (string id : controller->findEventsSince("4768")) {
				__process_wmidc_event(controller, id);
			}
		}
	}catch(const WmiException &e){
		Logger::instance()->log("sphirewalld.wmic.poll", ERROR, e.message());
	}

	controller->active = false;
	Logger::instance()->log("sphirewalld.wmic.poll", DEBUG, "Processing thread for '%s' exited", controller->hostname.c_str());
}

void WinWmiAuth::__poll_wmicnps(WinDomainControllerPtr controller){
	controller->active = true;

	try {
		while(controller->enabled){
			controller->seedCursor("6272");
			for (string id : controller->findEventsSince("6272")) {
				__process_wminps_event(controller, id);
			}
		}
	}catch(const WmiException &e){
		Logger::instance()->log("sphirewalld.wmic.poll", ERROR, e.message());
	}

	controller->active = false;
	Logger::instance()->log("sphirewalld.wmic.poll", DEBUG, "Processing thread for '%s' exited", controller->hostname.c_str());
}

void WinWmiAuth::poll() {
	Logger::instance()->log("sphirewalld.wmic.poll", DEBUG, "Polling for new events");
	for(WinDomainControllerPtr controller : domain_controllers){
		if(!controller->enabled){
			continue;
		}

		try {
			if(controller->mode == WMI_DOMAIN_HANDLER){
				if(!controller->active){
					Logger::instance()->log("sphirewalld.wmic.poll", DEBUG, "Starting new processing thread for '%s'", controller->hostname.c_str());
					boost::thread(boost::bind(&WinWmiAuth::__poll_wmicdc, this, controller));
				}
			}

			if(controller->mode == WMI_NPS_HANDLER){
				if(!controller->active){
					Logger::instance()->log("sphirewalld.wmic.poll", DEBUG, "Starting new processing thread for '%s'", controller->hostname.c_str());
					boost::thread(boost::bind(&WinWmiAuth::__poll_wmicnps, this, controller));
				}
			}


		}catch (const WmiException &e) {
			Logger::instance()->log("sphirewalld.wmic.poll", ERROR, e.message());
		}
	}
}

void WinWmiAuth::run() {
	while (true) {
		poll();
		sleep(2);
	}
}

bool WmiLoginEvent::isValidUser() {
	return getPlainUsername().size() > 0;
}

WmiLoginEvent::WmiLoginEvent(std::string id, std::string message){
	eventId = id;
	user = attribute(message, "Account Name");
	host = attribute(message, "Client Address");
}

std::string WmiLoginEvent::getPlainUsername() {
	boost::regex req("([a-z,A-Z,0-9,\\.,-]+)");
	boost::smatch what2;

	if (boost::regex_search(user, what2, req)) {
		return what2[1];
	}

	return "";
}

std::string WmiLoginEvent::getIpv4Address() {
	boost::regex req("([0-9]+)\\.([0-9]+)\\.([0-9]+)\\.([0-9]+)");
	boost::smatch what2;

	if (boost::regex_search(host, what2, req)) {
		stringstream ss;
		ss << what2[1] << "." << what2[2] << "." << what2[3] << "." << what2[4];
		return ss.str();
	}

	return "";
}

