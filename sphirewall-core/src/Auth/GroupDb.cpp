/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <time.h>
#include <string>
#include <map>
#include <cstring>
#include <vector>

using namespace std;
using std::string;

#include "Core/System.h"
#include "Auth/User.h"
#include "Auth/UserDb.h"
#include "Auth/Group.h"

#include "Utils/Utils.h"
#include "Auth/GroupDb.h"

bool GroupDb::load() {
	/*Ensure group map is empty first*/
	groups.clear();

	if (configurationManager->has("groupDb")) {
		ObjectContainer *root = configurationManager->getElement("groupDb");
		ObjectContainer *groups = root->get("groups")->container();

		for (int x = 0; x < groups->size(); x++) {
			ObjectContainer *jsonGroup = groups->get(x)->container();

			int id = jsonGroup->get("id")->number();
			std::string name = jsonGroup->get("name")->string();
			std::string manager = jsonGroup->get("manager")->string();
			std::string desc = jsonGroup->get("desc")->string();
			bool mui = jsonGroup->get("allowMui")->boolean();
			std::string metadata;

			if (jsonGroup->has("metadata")) {
				metadata= jsonGroup->get("metadata")->string();
			}
			GroupPtr group(new Group(id, name, desc, manager, mui));
			group->setMetadata(metadata);
			QuotaInfo &quota = group->getQuota();

			if (jsonGroup->has("dailyQuota")) {
				quota.dailyQuota = jsonGroup->get("dailyQuota")->boolean();
				quota.dailyQuotaLimit = jsonGroup->get("dailyQuotaLimit")->number();
			}

			if (jsonGroup->has("weeklyQuota")) {
				quota.weeklyQuota = jsonGroup->get("weeklyQuota")->boolean();
				quota.weeklyQuotaLimit = jsonGroup->get("weeklyQuotaLimit")->number();
			}

			if (jsonGroup->has("monthQuota")) {
				quota.monthQuota = jsonGroup->get("monthQuota")->boolean();
				quota.monthQuotaLimit = jsonGroup->get("monthQuotaLimit")->number();
			}

			if (jsonGroup->has("totalQuota")) {
				quota.totalQuota = jsonGroup->get("totalQuota")->boolean();
				quota.totalQuotaLimit = jsonGroup->get("totalQuotaLimit")->number();
			}

			if (jsonGroup->has("timeQuota")) {
				quota.timeQuota = jsonGroup->get("timeQuota")->boolean();
				quota.timeQuotaLimit = jsonGroup->get("timeQuotaLimit")->number();
			}

			this->groups[group->getId()] = group;
		}
	}
	else {
		Logger::instance()->log("sphirewalld.groupdb", INFO, "No groups found in configuration, loading defaults");
		groups[1] = GroupPtr(new Group(1, "defaultadmin", "Default Admin Group", "", true));
		save();
	}

	return true;
}

GroupPtr GroupDb::getGroup(int group) {
	if(groups.find(group) != groups.end()){
		return groups[group];
	}

	return GroupPtr();
}

GroupPtr GroupDb::getGroup(string group) {
	group = StringUtils::to_lower(group);

	for(GroupPtr target : list()) {
		if (target->getName().compare(group) == 0){
			return target;
		}
	}

	return GroupPtr();
}

void GroupDb::save() {
	holdLock();

	if (configurationManager) {
		Logger::instance()->log("sphirewalld.groupdb", EVENT, "Saving Group Configuration File");

		ObjectContainer *root = new ObjectContainer(CREL);
		ObjectContainer *jsonGroups = new ObjectContainer(CARRAY);

		for (pair<int, GroupPtr> gp : groups) {
			GroupPtr group = gp.second;
			ObjectContainer *g = new ObjectContainer(CREL);
			g->put("id", new ObjectWrapper((double) group->getId()));
			g->put("name", new ObjectWrapper((string) group->getName()));
			g->put("manager", new ObjectWrapper((string) group->getManager()));
			g->put("allowMui", new ObjectWrapper((bool) group->isAllowMui()));
			g->put("desc", new ObjectWrapper((string) group->getDesc()));
			g->put("metadata", new ObjectWrapper((string) group->getMetadata()));

			QuotaInfo &quotas = group->getQuota();

			if (quotas.dailyQuota) {
				g->put("dailyQuota", new ObjectWrapper((bool) quotas.dailyQuota));
				g->put("dailyQuotaLimit", new ObjectWrapper((double) quotas.dailyQuotaLimit));
			}

			if (quotas.weeklyQuota) {
				g->put("weeklyQuota", new ObjectWrapper((bool) quotas.weeklyQuota));
				g->put("weeklyQuotaLimit", new ObjectWrapper((double) quotas.weeklyQuotaLimit));
			}

			if (quotas.monthQuota) {
				g->put("monthQuota", new ObjectWrapper((bool) quotas.monthQuota));
				g->put("monthQuotaLimit", new ObjectWrapper((double) quotas.monthQuotaLimit));
			}

			if (quotas.totalQuota) {
				g->put("totalQuota", new ObjectWrapper((bool) quotas.totalQuota));
				g->put("totalQuotaLimit", new ObjectWrapper((double) quotas.totalQuotaLimit));
			}

			if (quotas.timeQuota) {
				g->put("timeQuota", new ObjectWrapper((bool) quotas.timeQuota));
				g->put("timeQuotaLimit", new ObjectWrapper((double) quotas.timeQuotaLimit));
			}

			jsonGroups->put(new ObjectWrapper(g));
		}

		root->put("groups", new ObjectWrapper(jsonGroups));
		configurationManager->holdLock();
		configurationManager->setElement("groupDb", root);
		configurationManager->save();
		configurationManager->releaseLock();
	}

	releaseLock();
}

GroupPtr GroupDb::createGroup(std::string name) {
	int id = findUniqueId();
	groups[id] = GroupPtr(new Group(id, name, "", "", false));
	return groups[id];
}

int GroupDb::findUniqueId() {
	for (int x = 0; x < 102500; x++) {
		if (!getGroup(x)) {
			return x;
		}
	}

	return 0;
}

bool GroupDb::delGroup(GroupPtr group) {
	for (GroupRemovedListener * grl : removeListeners) {
		grl->groupRemoved(group);
	}

	groups.erase(group->getId());
	save();
	return true;
}

vector<GroupPtr> GroupDb::list() {
	vector<GroupPtr> ret;

	for (pair<int, GroupPtr> gp : groups) {
		ret.push_back(gp.second);
	}

	return ret;
}

void GroupDb::registerRemovedListener(GroupRemovedListener *listener) {
	removeListeners.push_back(listener);
}

