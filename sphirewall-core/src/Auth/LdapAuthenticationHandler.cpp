/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
using namespace std;

#include "Auth/UserDb.h"
#include "Auth/User.h"
#include "Json/JSON.h"
#include "Json/JSONValue.h"
#include "Core/ConfigurationManager.h"
#include "Core/System.h"
#include <iostream>
#include <fcntl.h>
#include <sys/resource.h>
#include <pwd.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <ctype.h>
#include "Utils/HttpRequestWrapper.h"
#include <boost/regex.hpp>
#include <set>
#include "Auth/LdapAuthenticationHandler.h"

#define SCOPE LDAP_SCOPE_SUBTREE
#define LDAP_DEPRECATED 1
#include <ldap.h>

using namespace std;

class LdapEntry {
	public:
		multimap<string, string> attributes;

		bool has(string key) {
			return attributes.find(key) != attributes.end();
		}

		std::string scalar(const std::string key) {
			for (multimap<string, string>::iterator iter = attributes.find(key); iter != attributes.end(); iter++) {
				return iter->second;
			}

			return "";
		}
};

class LdapSearchResult {
	public:
		int resultcode;
		std::list<LdapEntry> entries;
};

LdapSearchResult ldapSearch(LDAP *connection, const char *basedn, const char *search) {
	Logger::instance()->log("sphirewalld.auth.manager.ldapSearch", DEBUG, 
		"ldapSearch called basedn = '%s', search = '%s'", basedn, search);

	LdapSearchResult res;
	LDAPMessage *result;

	const char *attributes[] = {"*", "+", NULL};
	int rc = ldap_search_ext_s(connection, basedn, SCOPE, search, const_cast<char **>((const char **)attributes), 0, NULL, NULL, NULL, 0, &result);
	res.resultcode = rc;

	if (rc == LDAP_SUCCESS) {
		LDAPMessage *entry = NULL;

		for (entry = ldap_first_entry(connection, result); entry != NULL;
				entry = ldap_next_entry(connection, entry)) {

			LdapEntry target;
			BerElement *ber;

			for (char *attribute = ldap_first_attribute(connection, entry, &ber);
					attribute != NULL;  attribute = ldap_next_attribute(connection, entry, ber)) {

				char **values = ldap_get_values(connection, entry, attribute);

				if (values != NULL) {
					for (int i = 0; values[i] != NULL; i++) {
						Logger::instance()->log("sphirewalld.auth.manager.ldapSearch.attributes", DEBUG, 
							"ldapSearch pulled %s - %s", attribute, values[i]);
						target.attributes.insert(pair<std::string, std::string>(attribute, values[i]));
					}

					ldap_value_free(values);
				}

				ldap_memfree(attribute);
			}

			if (ber != NULL) {
				ber_free(ber, 0);
			}

			res.entries.push_back(target);
		}
	}

	Logger::instance()->log("sphirewalld.auth.manager.ldapSearch", DEBUG,
		"ldapSearch returned status code %d and no entries %d", res.resultcode, res.entries.size());

	ldap_msgfree(result);
	return res;
}


std::string processGroupAttribute(std::string input) {
        string groupMembershipRaw = input;
        static boost::regex p("cn=(.*?),.+", boost::regex::icase);
        boost::smatch what;

        Logger::instance()->log("sphirewalld.auth.manager.ldap", DEBUG, "processGroupAttribute called with '%s'", input.c_str());
        if (boost::regex_match(groupMembershipRaw, what, p, boost::match_extra)) {
                string groupName = what[1];

                Logger::instance()->log("sphirewalld.auth.manager.ldap", DEBUG, "found group '%s'", groupName.c_str());
                return groupName;
        }

        return "";
}

std::string LdapAuthenticationProvider::find_dn(const std::string& username){
	LDAP *connection = connect(getHostname(), getPort(), getLdapUsername(), getLdapPassword());

	if (connection) {
		if (userOwnsGroupMembership()) {
			stringstream search;
			search << getUserNameAttribute();
			search << "=" << username;
			LdapSearchResult result = ldapSearch(connection, getBaseDn().c_str(), search.str().c_str());

			if (result.resultcode == LDAP_SUCCESS) {
				for (LdapEntry entry : result.entries) {
					if (entry.has(getUserDnAttribute())) {
						ldap_unbind(connection);
						return entry.scalar(getUserDnAttribute());
					}
				}
			}

			ldap_unbind(connection);
		}
	}

	return "";
}

bool LdapAuthenticationProvider::authenticate(const std::string& username, const std::string& password){
	Logger::instance()->log("sphirewalld.auth.manager.ldap.authenticate", INFO, "attempting to authenticate user '%s' with ldap service", username.c_str());

	if (password.size() == 0) {
		Logger::instance()->log("sphirewalld.auth.manager.ldap.authenticate", ERROR, "password is empty, returning false");
		return false;
	}

	LDAP *connection = NULL;
	stringstream search;

	if (!authenticateUserUsingSuppliedDn()) {
		search << getUserNameAttribute() << "=" << username;
		if (getUserBase().size() > 0) {
			search << "," << getUserBase();
		}

		if (getBaseDn().size() > 0) {
			search << "," << getBaseDn();
		}
	}
	else {
		search << find_dn(username);
	}

	if (search.str().size() == 0) {
		Logger::instance()->log("sphirewalld.auth.manager.ldap.authenticate", ERROR, "search string for authenticate request is empty, user = '%s'", username.c_str());
		return false;
	}

	Logger::instance()->log("sphirewalld.auth.manager.ldap.authenticate", DEBUG, "search string for authenticate request is '%s'", search.str().c_str());

	connection = connect(getHostname(), getPort(), search.str(), password);
	if (connection) {
		Logger::instance()->log("sphirewalld.auth.manager.ldap.authenticate", INFO, "authentication request for user '%s' was a success", username.c_str());
		ldap_unbind(connection);
		return true;
	}

	return false;
}

bool LdapDirectoryProvider::find(std::list<std::string>& groups, const std::string& username){
	LDAP *connection = connect(getHostname(), getPort(), getLdapUsername(), getLdapPassword());

	if (connection) {
		if (userOwnsGroupMembership()) {
			stringstream search;
			search << getUserNameAttribute();
			search << "=" << username;
			LdapSearchResult result = ldapSearch(connection, getBaseDn().c_str(), search.str().c_str());

			if (result.resultcode == LDAP_SUCCESS) {
				Logger::instance()->log("sphirewalld.auth.manager.ldap.getGroupNames", DEBUG, "search matches, looking at ldap_first_entry");
				
				for (LdapEntry entry : result.entries) {
					for (multimap<string, string>::iterator iter = entry.attributes.find(getUserGroupAttribute()); iter != entry.attributes.end(); iter++) {
						groups.push_back(processGroupAttribute(iter->second));
					}

					for (multimap<string, string>::iterator iter = entry.attributes.begin(); iter != entry.attributes.end(); iter++) {
						Logger::instance()->log("sphirewalld.auth.manager.ldap", DEBUG,
								"attribute '%s' = '%s'", iter->first.c_str(), iter->second.c_str());
					}

					/* I know, this seems a bit strange, but actually there should only be one user object*/
					ldap_unbind(connection);
					return true;
				}
			}

			ldap_unbind(connection);
			return false;
		}
		else {
			LdapSearchResult result = ldapSearch(connection, getBaseDn().c_str(), NULL);
			if (result.resultcode == LDAP_SUCCESS) {
				Logger::instance()->log("sphirewalld.auth.manager.ldap.getGroupNames", DEBUG, "search matches, looking at ldap_first_entry");

				for (LdapEntry entry : result.entries) {
					if (entry.has(getGroupNameAttribute())) {
						if (entry.has(getGroupMemberNameAttribute())) {
							//Find all the members, and then add the group if it does not already exist
							for (multimap<string, string>::iterator iter = entry.attributes.find(getGroupMemberNameAttribute()); iter != entry.attributes.end(); iter++) {
								if (iter->second.compare(username) == 0) {
									groups.push_back(entry.scalar(getGroupNameAttribute()));
								}
							}
						}
					}
				}

				ldap_unbind(connection);
				return groups.size() > 0;
			}

			ldap_unbind(connection);
			return false;
		}
	}

	return false;
}

bool LdapDirectoryProvider::find_all_groups(std::list<std::string>& groups){
        set<string> ret;
        LDAP *connection = connect(getHostname(), getPort(), getLdapUsername(), getLdapPassword());

        if (connection) {
                Logger::instance()->log("sphirewalld.auth.manager.ldap", INFO, "connected to ldap service");

                if (userOwnsGroupMembership()) {
                        stringstream search;
                        search << getUserNameAttribute() << "=*";
                        Logger::instance()->log("sphirewalld.auth.manager.ldap.sync", DEBUG, "starting user/group sync, searching '%s", search.str().c_str());

                        LdapSearchResult result = ldapSearch(connection, getBaseDn().c_str(), search.str().c_str());

                        if (result.resultcode == LDAP_SUCCESS) {
                                for (LdapEntry entry : result.entries) {
                                        for (multimap<string, string>::iterator iter = entry.attributes.find(getUserGroupAttribute());
                                                        iter != entry.attributes.end(); iter++) {

                                                ret.insert(processGroupAttribute(iter->second));
                                        }
                                }
                        }
                        else {
                                Logger::instance()->log("sphirewalld.auth.manager.ldap", ERROR,
                                                        "could not query ldap service, reason was '%s'", ldap_err2string(result.resultcode));
				ldap_unbind(connection);
				return false;
                        }

                }
                else {
                        stringstream search;
                        search << getGroupBase() << "," << getBaseDn();
                        LdapSearchResult result = ldapSearch(connection, search.str().c_str(), NULL);

                        if (result.resultcode == LDAP_SUCCESS) {
                                for (LdapEntry entry : result.entries) {
                                        if (entry.has(getGroupNameAttribute())) {
                                                ret.insert(entry.scalar(getGroupNameAttribute()));
                                        }
                                }
                        }
                        else {
                                Logger::instance()->log("sphirewalld.auth.manager.ldap", ERROR,
                                                        "could not query ldap service, reason was '%s'", ldap_err2string(result.resultcode));
				ldap_unbind(connection);
				return false;
                        }
                }

                ldap_unbind(connection);
        }

	for(std::string group_name : ret){
		groups.push_back(group_name);
	}	

	return true;
}

std::string LdapBase::getHostname() const {
	if (System::getInstance()->getConfigurationManager()->hasByPath("authentication:ldap:hostname")) {
		return System::getInstance()->getConfigurationManager()->get("authentication:ldap:hostname")->string();
	}

	return "";
}

int LdapBase::getPort() const {
	if (System::getInstance()->getConfigurationManager()->hasByPath("authentication:ldap:port")) {
		return System::getInstance()->getConfigurationManager()->get("authentication:ldap:port")->number();
	}

	return -1;
}

std::string LdapBase::getLdapUsername() const {
	if (System::getInstance()->getConfigurationManager()->hasByPath("authentication:ldap:ldapusername")) {
		return System::getInstance()->getConfigurationManager()->get("authentication:ldap:ldapusername")->string();
	}

	return "";
}

std::string LdapBase::getLdapPassword() const {
	if (System::getInstance()->getConfigurationManager()->hasByPath("authentication:ldap:ldappassword")) {
		return System::getInstance()->getConfigurationManager()->get("authentication:ldap:ldappassword")->string();
	}

	return "";
}

std::string LdapBase::getBaseDn() const {
	if (System::getInstance()->getConfigurationManager()->hasByPath("authentication:ldap:basedn")) {
		return System::getInstance()->getConfigurationManager()->get("authentication:ldap:basedn")->string();
	}

	return "";
}

bool LdapBase::isEnabled() {
	if (System::getInstance()->getConfigurationManager()->hasByPath("authentication:ldap:enabled")) {
		return System::getInstance()->getConfigurationManager()->get("authentication:ldap:enabled")->boolean();
	}

	return false;
}

bool LdapBase::userOwnsGroupMembership() {
	if (System::getInstance()->getConfigurationManager()->hasByPath("authentication:ldap:userownedgroupmembership")) {
		return System::getInstance()->getConfigurationManager()->get("authentication:ldap:userownedgroupmembership")->boolean();
	}

	return true;
}

std::string LdapBase::getUserNameAttribute() {
	//CN or uuid
	if (System::getInstance()->getConfigurationManager()->hasByPath("authentication:ldap:usernameattribute")) {
		return System::getInstance()->getConfigurationManager()->get("authentication:ldap:usernameattribute")->string();
	}

	return "";
}

std::string LdapBase::getUserBase() {
	//OU=Users
	if (System::getInstance()->getConfigurationManager()->hasByPath("authentication:ldap:userbase")) {
		return System::getInstance()->getConfigurationManager()->get("authentication:ldap:userbase")->string();
	}

	return "";
}

std::string LdapBase::getUserGroupAttribute() {
	//memberOf
	if (System::getInstance()->getConfigurationManager()->hasByPath("authentication:ldap:usergroupattribute")) {
		return System::getInstance()->getConfigurationManager()->get("authentication:ldap:usergroupattribute")->string();
	}

	return "";
}

std::string LdapBase::getUserDnAttribute() {
	if (System::getInstance()->getConfigurationManager()->hasByPath("authentication:ldap:userdnattribute")) {
		return System::getInstance()->getConfigurationManager()->get("authentication:ldap:userdnattribute")->string();
	}

	return "";
}

bool LdapBase::authenticateUserUsingSuppliedDn() {
	if (System::getInstance()->getConfigurationManager()->hasByPath("authentication:ldap:authenticateuserusingsupplieddn")) {
		return System::getInstance()->getConfigurationManager()->get("authentication:ldap:authenticateuserusingsupplieddn")->boolean();
	}

	return false;
}

//Only used if users stored on groups
std::string LdapBase::getGroupBase() {
	//OU=Groups
	if (System::getInstance()->getConfigurationManager()->hasByPath("authentication:ldap:groupbase")) {
		return System::getInstance()->getConfigurationManager()->get("authentication:ldap:groupbase")->string();
	}

	return "";
}

std::string LdapBase::getGroupNameAttribute() {
	//cn
	if (System::getInstance()->getConfigurationManager()->hasByPath("authentication:ldap:groupnameattribute")) {
		return System::getInstance()->getConfigurationManager()->get("authentication:ldap:groupnameattribute")->string();
	}

	return "";
}

std::string LdapBase::getGroupMemberNameAttribute() {
	//userid
	if (System::getInstance()->getConfigurationManager()->hasByPath("authentication:ldap:groupmembernameattribute")) {
		return System::getInstance()->getConfigurationManager()->get("authentication:ldap:groupmembernameattribute")->string();
	}

	return "";
}

LDAP *LdapBase::connect(std::string hostname, int port, std::string username, std::string password) {
	LDAP *ld;

	if ((ld = ldap_init(hostname.c_str(), port)) == NULL) {
		Logger::instance()->log("sphirewalld.auth.manager.ldap", ERROR, "ldap init failed");
		throw ProviderException("ldap_init failed");
	}

	int version = LDAP_VERSION3;
	ldap_set_option(ld, LDAP_OPT_PROTOCOL_VERSION, &version);
	ldap_set_option(ld, LDAP_OPT_REFERRALS, LDAP_OPT_OFF);

	int rc = ldap_simple_bind_s(ld, username.c_str(), password.c_str());

	if (rc != LDAP_SUCCESS) {
		Logger::instance()->log("sphirewalld.auth.manager.ldap", ERROR, "could not connect to the ldap service, reason is '%s'", ldap_err2string(rc));
		ldap_unbind(ld);
		throw ProviderException(ldap_err2string(rc));
	}

	return ld;
}

bool LdapBase::isConnected(){
	LDAP* ld = connect(getHostname(), getPort(), getLdapUsername(), getLdapPassword());		
	if(ld){
		ldap_unbind(ld);
		return true;
	}
	
	return false;
}
