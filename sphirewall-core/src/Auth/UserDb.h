/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef USERDB_H
#define USERDB_H

#include "Core/Logger.h"
#include "Core/Lockable.h"
#include "Auth/GroupDb.h"
#include "Auth/User.h"
#include "Core/ConfigurationManager.h"
#include <boost/unordered_map.hpp>

class User;
namespace std {
	typedef basic_string<char> string;
};

class GroupRemovedListener;
class User;
class Event;
class EventDb;

class DeleteUserListener {
	public:
		virtual ~DeleteUserListener() {}
		virtual void handleDeleteUser(UserPtr user) = 0;
};

class ProviderException : std::exception {
        public:
                ProviderException(std::string what) : exception_value(what){}
                ~ProviderException() throw(){}
                std::string what() {
			return exception_value;
		}	

	private:
                std::string exception_value;
};

class AuthenticationProvider {
	public:
		virtual int type() = 0;
		virtual bool enabled() = 0;
		virtual bool authenticate(const std::string& username, const std::string& password) = 0;

		virtual bool check(){
			return true;
		}
};

class DirectoryServiceProvider {
	public:
		virtual int type() = 0;
		virtual bool enabled() = 0;
		virtual bool find(std::list<std::string>& groups, const std::string& username) = 0;
		virtual bool find_all_groups(std::list<std::string>& groups) = 0;
                virtual bool check(){
                        return true;
                }
};

class UserDb : public GroupRemovedListener, public Lockable, public Configurable {
	public:
		class CleanupTempUsers : public CronJob {
			public:
				CleanupTempUsers(UserDb* user_db) :
					CronJob(60 * 10, "CLEANUP_TEMP_USERS_CRONJOB", true),
					user_db(user_db) {

					}

				void run();
				UserDb* user_db;
		};

		class DirectoryServiceSync : public CronJob {
			public:
				DirectoryServiceSync(UserDb* user_db) :
					CronJob(60 * 60 * 24, "DIRECTORY_SERVICE_SYNC_CRONJOB", true),
					user_db(user_db) {
						
					}

				void run();
				UserDb* user_db;
		};


		UserDb(EventDb *e, GroupDb *g);
		~UserDb();

		bool load();
		void save(bool lockIt = true);
		UserPtr createUser(std::string username);
		bool delUser(UserPtr user);

		void disableUser(UserPtr user);
		void enableUser(UserPtr user);

		/* Checks the username and password with all enabled authentication handlers. If true, and the user does not exist, it will create a user object */
		bool authenticateUser(std::string username, std::string password);

		/* Gets the user from the local store, if query_directory_services=true then it will attempt to query the external providers as well. It will not ever create a user */
		UserPtr getUser(std::string username, bool query_directory_services=false);

		std::vector<std::string> list();
		std::vector<UserPtr> listUserPtr();
		std::vector<UserPtr> get_users_of_group(GroupPtr groupPtr);
		boost::unordered::unordered_map<std::string, UserPtr> userMap();
		long getNextTokenId();
		void persistUserFromExternalSource(UserPtr user);
		void registerDeleteListener(DeleteUserListener *target);

		//Listeners:
		void groupRemoved(GroupPtr group);
		const char* getConfigurationSystemName(){
			return "User database";
		}

		static int USERDB_PERSIST_ENABLED;

		DirectoryServiceProvider* get_directory_service(int type);
		AuthenticationProvider* get_authentication_provider(int type);
		void sync_directory_providers();
	private:
		EventDb *eventDb;
		GroupDb *groupDb;
		std::string filename;
		boost::unordered::unordered_map<std::string, UserPtr> users;
		int pamAuth(char *login, char *passwd);
		std::list<DeleteUserListener *> deleteListeners;

		std::list<AuthenticationProvider*> authentication_providers;
		std::list<DirectoryServiceProvider*> directory_providers;

		UserPtr get_user_from_local_db(std::string username);
};

#endif
