#ifndef WINDOWS_WMI_H
#define WINDOWS_WMI_H

#include <list>
#include <set>
#include <boost/shared_ptr.hpp>

class WmiException: public std::exception {
        public:

                WmiException(std::string message) {
                        w = message;
                }

                ~WmiException() throw () {
                }

                virtual const char *what() const throw () {
                        return "WMIC Exception";
                }

                const std::string &message() const {
                        return w;
                }

        private:
                std::string w;
};

#define WMI_NPS_HANDLER 1
#define WMI_DOMAIN_HANDLER 2

class WinDomainController {
        public:
                WinDomainController(std::string hostname, std::string username, std::string password, std::string domain, int mode){
                        this->cursor = 0;
                        this->hostname = hostname;
			this->username = username;
			this->password = password;
			this->domain = domain;
			this->mode = mode;
			this->enabled = false;
			this->active = false;
                }

		WinDomainController(){
                        this->cursor = 0;
			this->enabled = false;
			this->active = false;
		}
		
                std::string query(std::string input);
		void check();
		void updateCursor(std::string input);
		void seedCursor(std::string event_type);

		std::list<std::string> findEventsSince(std::string event_identifier);
		std::string getRawEvent(std::string id);

		int cursor;
		std::string hostname;
		std::string username;
		std::string password;
		std::string domain;
		int mode;
		std::string id;
		bool enabled;
		bool active;
		std::set<std::string> exceptions;
};

std::string attribute(std::string raw, std::string key);

typedef boost::shared_ptr<WinDomainController> WinDomainControllerPtr;
#endif
