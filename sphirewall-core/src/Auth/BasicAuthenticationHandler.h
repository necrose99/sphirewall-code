/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef AUTH_BASIC
#define AUTH_BASIC

#define LDAP_DEPRECATED 1

#include <ldap.h>
#include <list>
#include "Core/Logger.h"
#include "Core/Cron.h"
#include "Auth/AuthenticationType.h"
#include "Auth/UserDb.h"

class UserDb;
class User;
class ConfigurationManager;
class GroupDb;
class Lock;
typedef boost::shared_ptr<User> UserPtr;

class BasicHttpAuthenticationHandler : public virtual AuthenticationProvider {
	public:

		int type() {
			return BASIC_HTTP;
		}

		bool enabled();
		bool authenticate(const std::string& username, const std::string& password);
};
#endif
