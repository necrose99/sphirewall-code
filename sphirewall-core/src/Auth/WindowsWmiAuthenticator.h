/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef WINWMIAUTH_H
#define WINWMIAUTH_H

#include <sstream>
#include <list>

#include "Core/Lockable.h"
#include "Core/ConfigurationManager.h"
#include "Auth/WindowsWmi.h"

using namespace std;

class WmiLoginEvent {
	public:
		WmiLoginEvent(std::string id, std::string message);
		std::string eventId;
		std::string user;
		std::string host;

		bool isValidUser();

		std::string toString() {
			std::stringstream ss;
			ss << "id:" << eventId << " user:" << getPlainUsername() << " host:" << getIpv4Address();
			return ss.str();
		}

		std::string getPlainUsername();
		std::string getIpv4Address();
};


class EventDb;
class AuthenticationManager;
class HostDiscoveryService;
class ConfigurationManager;

class WinWmiAuth : public Configurable {
	public:
		WinWmiAuth(EventDb *eventDb, AuthenticationManager *authManager, HostDiscoveryService *arp, ConfigurationManager *config):
			eventDb(eventDb), authManager(authManager), arp(arp), config(config) {
		}

		WinWmiAuth() {
		}

		void poll();
		void run();

		bool load();
		void save();
		const char* getConfigurationSystemName(){
			return "Windows WMI";
		}

		std::list<WinDomainControllerPtr> get_domain_controllers(){
			return domain_controllers;
		}
		
		void add_domain_controller(WinDomainControllerPtr dc){
			domain_controllers.push_back(dc);
		}

		void del_domain_controller(WinDomainControllerPtr dc){
			dc->active = false;
			dc->enabled = false;
			domain_controllers.remove(dc);
		}

		WinDomainControllerPtr get_controller(std::string id){
			for(WinDomainControllerPtr dc : get_domain_controllers()){
				if(dc->id.compare(id) == 0){
					return dc;
				}
			}
			return WinDomainControllerPtr();
		}
	private:
		void __process_wmidc_event(WinDomainControllerPtr controller, std::string record_number);
		void __process_wminps_event(WinDomainControllerPtr controller, std::string record_number);

		void __poll_wmicdc(WinDomainControllerPtr controller);
		void __poll_wmicnps(WinDomainControllerPtr controller);

		std::list<WinDomainControllerPtr> domain_controllers;

		//Event Mapping Properties:
		std::string getUsernameAttribute();
		std::string getHostAttribute();

		EventDb *eventDb;
		AuthenticationManager *authManager;
		HostDiscoveryService *arp;
		ConfigurationManager *config;
};

#endif
