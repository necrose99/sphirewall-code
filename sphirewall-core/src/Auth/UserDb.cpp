/*
   Copyright Michael Lawson
   This file is part of Sphirewall.

   Sphirewall is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Sphirewall is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <boost/unordered_map.hpp>
#include <sstream>
#include <fstream>

#include "Core/System.h"
#include "Utils/Utils.h"
#include "Auth/User.h"
#include "Auth/UserDb.h"
#include "Utils/Hash.h"
#include "Json/JSON.h"
#include "Json/JSONValue.h"

#include "Auth/LdapAuthenticationHandler.h"
#include "Auth/BasicAuthenticationHandler.h"

int UserDb::USERDB_PERSIST_ENABLED = 1;

UserDb::UserDb(EventDb *e, GroupDb *g) : Lockable(), eventDb(e), groupDb(g) 
{
	System::getInstance()->config.getRuntime()->loadOrPut("USERDB_PERSIST_ENABLED", &USERDB_PERSIST_ENABLED);

	directory_providers.push_back(new LdapDirectoryProvider());	
	authentication_providers.push_back(new LdapAuthenticationProvider());
	authentication_providers.push_back(new BasicHttpAuthenticationHandler());
}

UserDb::~UserDb() {
}

bool UserDb::load() {
	users.clear();
	Logger::instance()->log("sphirewalld.userdb", EVENT, "Loading User Database");

	if (!configurationManager->has("userDb")) {
		Logger::instance()->log("sphirewalld.userdb", ERROR, "No users found in configuration, loading defaults");
		/*Lets load into the db a default admin user*/

		UserPtr defaultAdminUser(new User("admin"));
		defaultAdminUser->setCreationTime(time(NULL));
		defaultAdminUser->setPassword("admin");
		GroupPtr group = groupDb->getGroup(1);

		defaultAdminUser->addGroup(group);
		defaultAdminUser->enable();
		users.insert(pair<string, UserPtr>(defaultAdminUser->getUserName(), defaultAdminUser));
		save();
	}
	else {
		ObjectContainer *root = configurationManager->getElement("userDb");
		ObjectContainer *users = root->get("users")->container();

		for (int x = 0; x < users->size(); x++) {
			ObjectContainer *o = users->get(x)->container();
			UserPtr temp;

			temp = UserPtr(new User());
			temp->user = StringUtils::to_lower(o->get("username")->string());

			if (o->has("firstname")) {
				temp->fname = o->get("firstname")->string();
			}

			if (o->has("lastname")) {
				temp->lname = o->get("lastname")->string();
			}

			if (o->has("password")) {
				temp->password = o->get("password")->string();
			}

			temp->isEnabled = o->get("enabled")->boolean();
			temp->email = o->get("email")->string();

			if (o->has("dn")) {
				temp->dn = o->get("dn")->string();
			}

			temp->email = o->get("email")->string();
			temp->setCreationTime(o->get("createdTime")->number());

			QuotaInfo *quota = temp->getQuota();

			if (o->has("dailyQuota")) {
				quota->dailyQuota = o->get("dailyQuota")->boolean();
				quota->dailyQuotaLimit = o->get("dailyQuotaLimit")->number();
			}

			if (o->has("weeklyQuota")) {
				quota->weeklyQuota = o->get("weeklyQuota")->boolean();
				quota->weeklyQuotaLimit = o->get("weeklyQuotaLimit")->number();
			}

			if (o->has("monthQuota")) {
				quota->monthQuota = o->get("monthQuota")->boolean();
				quota->monthQuotaLimit = o->get("monthQuotaLimit")->number();
			}

			if (o->has("totalQuota")) {
				quota->totalQuota = o->get("totalQuota")->boolean();
				quota->totalQuotaLimit = o->get("totalQuotaLimit")->number();
			}

			if (o->has("timeQuota")) {
				quota->timeQuota = o->get("timeQuota")->boolean();
				quota->timeQuotaLimit = o->get("timeQuotaLimit")->number();
			}

			if(o->has("temp_user")){
				temp->temp_user = o->get("temp_user")->boolean();
				temp->expiry_timestamp= o->get("expiry_timestamp")->number();
			}

			ObjectContainer *groups = o->get("groups")->container();

			for (int gid = 0; gid < groups->size(); gid++) {
				GroupPtr g;

				if (groups->get(gid)->isContainer()) {
					ObjectContainer *jsonGroup = groups->get(gid)->container();
					g = groupDb->getGroup(jsonGroup->get("id")->number());
				}
				else {
					g = groupDb->getGroup(groups->get(gid)->number());
				}

				if (g != NULL) {
					temp->groups.push_back(g);
				}
			}

			this->users[temp->user] = temp;
		}
	}

	System::getInstance()->getCronManager()->registerJob(new UserDb::CleanupTempUsers(this));
	System::getInstance()->getCronManager()->registerJob(new UserDb::DirectoryServiceSync(this));
	return true;
}

/**
  Saves changes to the user database.

  @params lockIt - determines if this method should lock the user DB.
  Defaults to true (see UserDb.h).  Set to false if calling routine
  has user DB already locked.
 **/
void UserDb::save(bool lockIt) {
	if (lockIt) {
		holdLock();
	}

	if (configurationManager) {
		ObjectContainer *root = new ObjectContainer(CREL);
		ObjectContainer *array = new ObjectContainer(CARRAY);

		for (pair<string, UserPtr> p : users) {
			UserPtr source = p.second;
			if(!source){
				continue;
			}

			ObjectContainer *user = new ObjectContainer(CREL);
			user->put("username", new ObjectWrapper((string) source->user));

			if (source->fname.size() > 0) {
				user->put("firstname", new ObjectWrapper((string) source->fname));
			}

			if (source->lname.size() > 0) {
				user->put("lastname", new ObjectWrapper((string) source->lname));
			}

			user->put("enabled", new ObjectWrapper((bool) source->isEnabled));
			user->put("email", new ObjectWrapper((string) source->email));
			user->put("password", new ObjectWrapper((string) source->password));
			user->put("createdTime", new ObjectWrapper((double) source->getCreationTime()));
			user->put("dn", new ObjectWrapper((string) source->dn));

			user->put("temp_user", new ObjectWrapper((bool) source->temp_user));
			user->put("expiry_timestamp", new ObjectWrapper((double) source->expiry_timestamp));

			QuotaInfo *quotas = source->getQuota();

			if (quotas->dailyQuota) {
				user->put("dailyQuota", new ObjectWrapper((bool) quotas->dailyQuota));
				user->put("dailyQuotaLimit", new ObjectWrapper((double) quotas->dailyQuotaLimit));
			}

			if (quotas->weeklyQuota) {
				user->put("weeklyQuota", new ObjectWrapper((bool) quotas->weeklyQuota));
				user->put("weeklyQuotaLimit", new ObjectWrapper((double) quotas->weeklyQuotaLimit));
			}

			if (quotas->monthQuota) {
				user->put("monthQuota", new ObjectWrapper((bool) quotas->monthQuota));
				user->put("monthQuotaLimit", new ObjectWrapper((double) quotas->monthQuotaLimit));
			}

			if (quotas->totalQuota) {
				user->put("totalQuota", new ObjectWrapper((bool) quotas->totalQuota));
				user->put("totalQuotaLimit", new ObjectWrapper((double) quotas->totalQuotaLimit));
			}

			if (quotas->timeQuota) {
				user->put("timeQuota", new ObjectWrapper((bool) quotas->timeQuota));
				user->put("timeQuotaLimit", new ObjectWrapper((double) quotas->timeQuotaLimit));
			}

			ObjectContainer *groups = new ObjectContainer(CARRAY);

			for (int x = 0; x < (int) source->groups.size(); x++) {
				groups->put(new ObjectWrapper((double) source->groups[x]->getId()));
			}

			user->put("groups", new ObjectWrapper(groups));
			array->put(new ObjectWrapper(user));
		}

		root->put("users", new ObjectWrapper(array));

		if(USERDB_PERSIST_ENABLED == 1){
			configurationManager->holdLock();
			configurationManager->setElement("userDb", root);
			configurationManager->save();
			configurationManager->releaseLock();
		}
	}

	if (lockIt) {
		releaseLock();
	}
}

UserPtr UserDb::createUser(string username) {
	UserPtr user(new User(username));
	user->setCreationTime(time(NULL));

	holdLock();
	users[user->user] = user;
	releaseLock();

	save();
	return user;
}

bool UserDb::delUser(UserPtr user) {
	boost::unordered_map<string, UserPtr>::iterator location;
	holdLock();

	std::for_each(deleteListeners.begin(), deleteListeners.end(), boost::bind(&DeleteUserListener::handleDeleteUser, _1, user));

	if ((location = users.find(user->user)) != users.end()) {
		users.erase(location);
		save(false);
	}

	releaseLock();
	return true;
}

UserPtr UserDb::getUser(string username, bool poll_directory_services) {
	/* Convert to lowercase before querying */
	username = StringUtils::to_lower(username);

	if(poll_directory_services){
		for(DirectoryServiceProvider* directory : directory_providers){
			try {
				if(directory->enabled()){
					std::list<std::string> user_groups; 
					if(directory->find(user_groups, username)){
						/* Does this user object exist in our local store already? */
						UserPtr user_object = get_user_from_local_db(username);
						if(!user_object){
							user_object = createUser(username);	
						}

						/* Sync the groups */
						vector<GroupPtr> new_group_objects;
						for (string groupname : user_groups) {
							if(groupname.size() <=0){
								continue;
							}

							GroupPtr group = groupDb->getGroup(groupname);
							if (!group) {
								group = groupDb->createGroup(groupname);
							}

							new_group_objects.push_back(group);
						}

						holdLock();
						user_object->setGroups(new_group_objects);
						releaseLock();
						save();
						groupDb->save();

						return user_object;
					}
				}
			}catch(ProviderException& exception){
				Logger::instance()->log("sphirewalld.userdb", ERROR, "Failed to find user '%s' in directory service, error was '%s'", username.c_str(), exception.what().c_str());
				continue;
			}
		}
	}

	return get_user_from_local_db(username);
}

UserPtr UserDb::get_user_from_local_db(std::string username){
	/* Convert username to lowercase before searching */
	username = StringUtils::to_lower(username);	

	UserPtr ret;
	holdLock();
	if(users.find(username) != users.end()){
		ret = users[username];
	}

	releaseLock();
	return ret;
}

bool UserDb::authenticateUser(string username, string password) {
	username = StringUtils::to_lower(username);	

	/* Iterate over all authentication providers, and test */
	for(AuthenticationProvider* provider : authentication_providers){
		try {
			if(provider->enabled()){
				if(provider->authenticate(username, password)){
					/* Great, we have a match */
					UserPtr provider_user = getUser(username);
					if(!provider_user){
						/* A user already exists*/
						provider_user = createUser(username);
					}

					provider_user->lastLogin = time(NULL);
					save();
					return true;
				}
			}
		}catch(ProviderException& exception){
			Logger::instance()->log("sphirewalld.userdb", ERROR, "Failed to authenticate user '%s' in directory service, error was '%s'", username.c_str(), exception.what().c_str());
			continue;
		}
	}

	UserPtr user = getUser(username);
	if (user && user->isEnabled) {
		if (Hash::check_hash(password, user->getPassword())) {
			if (Hash::shouldUpdate(user->getPassword())) {
				user->password = Hash::create_hash(password);
			}

			user->lastLogin = time(NULL);
			save();
			return true;
		}
		else {
			user->lastFail = time(NULL);
			save();
		}
	}

	return false;
}

void UserDb::sync_directory_providers(){
	for(DirectoryServiceProvider* directory : directory_providers){
		try {
			if(directory->enabled()){
				std::list<std::string> groups_from_provider;
				directory->find_all_groups(groups_from_provider);
				for(string group : groups_from_provider){
					if(group.size() <=0){
						continue;
					}

					if (!groupDb->getGroup(group)) {
						groupDb->createGroup(group);
					}
				}
			}
		}catch(ProviderException& exception){
			Logger::instance()->log("sphirewalld.userdb", ERROR, "Failed to sync directory service, error was '%s'", exception.what().c_str());
		}
	}	

	groupDb->save();
}

vector<string> UserDb::list() {
	vector<string> ret;

	for (pair<string, UserPtr> p : users) {
		ret.push_back(p.first);
	}

	return ret;
}

void UserDb::disableUser(UserPtr user) {
	user->disable();
}

void UserDb::enableUser(UserPtr user) {
	user->enable();
}

void UserDb::registerDeleteListener(DeleteUserListener *target) {
	deleteListeners.push_back(target);
}

void UserDb::groupRemoved(GroupPtr group) {
	holdLock();

	for (std::pair<string, UserPtr> p : users) {
		UserPtr user = p.second;

		if (user->checkForGroup(group)) {
			user->removeGroup(group);
		}
	}

	releaseLock();
}

void UserDb::CleanupTempUsers::run(){
	bool user_to_delete = false;
	for(string username : user_db->list()){
		UserPtr user = user_db->getUser(username);
		if(user && user->temp_user && time(NULL) > user->expiry_timestamp){
			Logger::instance()->log("sphirewalld.userdb.cleanup", INFO, "Deleting temporary user %s as they have expired", user->getUserName().c_str());

			user_db->delUser(user);
			user_to_delete = true;
		}
	}

	if(user_to_delete){
		user_db->save();
	}
}

DirectoryServiceProvider* UserDb::get_directory_service(int type){
	for(auto provider : directory_providers){
		if(provider->type() == type){
			return provider;
		}
	}
	return NULL;
}

AuthenticationProvider* UserDb::get_authentication_provider(int type){
	for(auto provider : authentication_providers){
		if(provider->type() == type){
			return provider;
		}
	}
	return NULL;
}

void UserDb::DirectoryServiceSync::run(){
	Logger::instance()->log("sphirewalld.userdb.sync", INFO, "Syncronizing the directory services");
	user_db->sync_directory_providers();	
	Logger::instance()->log("sphirewalld.userdb.sync", INFO, "Finished syncronizing the directory services");
}

vector<UserPtr> UserDb::listUserPtr() {
	vector<UserPtr> ret;

	for (pair<string, UserPtr> p : users) {
		ret.push_back(p.second);
	}

	return ret;
}

vector<UserPtr> UserDb::get_users_of_group(GroupPtr groupPtr) {
	vector<UserPtr> ret;

	for (pair<string, UserPtr> p : users) {
		UserPtr user = p.second;
		if (user->checkForGroup(groupPtr)) {
			ret.push_back(user);
		}
	}

	return ret;
}
