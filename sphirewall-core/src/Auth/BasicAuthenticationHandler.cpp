/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
using namespace std;

#include "Core/Event.h"
#include "Auth/UserDb.h"
#include "Auth/User.h"
#include "Json/JSON.h"
#include "Json/JSONValue.h"
#include "Core/ConfigurationManager.h"
#include "Core/System.h"
#include <iostream>
#include <fcntl.h>
#include <sys/resource.h>
#include <pwd.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <ctype.h>
#include "Utils/HttpRequestWrapper.h"
#include "Auth/BasicAuthenticationHandler.h"
#include <boost/regex.hpp>
#include <set>

using namespace std;

bool BasicHttpAuthenticationHandler::enabled() {
	if (System::getInstance()->getConfigurationManager()->hasByPath("authentication:basic:enabled")) {
		return System::getInstance()->getConfigurationManager()->get("authentication:basic:enabled")->boolean();
	}

	return false;
}

bool BasicHttpAuthenticationHandler::authenticate(const std::string& username, const std::string& password) {
	string baseurl = System::getInstance()->getConfigurationManager()->get("authentication:basic:baseurl")->string();
	stringstream ss; ss << baseurl << "?username=" << username << "&password=" << password;

	HttpRequestWrapper *req = new HttpRequestWrapper(ss.str(), GET);
	try {
		string response = req->execute();
		if (response.compare("OK") == 0) {
			return true;
		}
		else {
			return false;
		}
	}
	catch (const HttpRequestWrapperException &e) {
		return false;
	}

	delete req;
	return false;
}

