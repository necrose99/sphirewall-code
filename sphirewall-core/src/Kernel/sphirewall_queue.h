/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SPHIREWALL_QUEUE_H
#define SPHIREWALL_QUEUE_H

#define RAWSIZE 1501

#define PREROUTING 1
#define FILTER 2
#define POSTROUTING 3

#define NAT_IGNORE 0
#define NAT_DNAT 1
#define NAT_SNAT 2
#define NAT_ROUTE 3

#define SQ_VERDICT_DROP 0
#define SQ_VERDICT_ACCEPT 1
#define SQ_VERDICT_RESET_OR_DROP 2

struct sq_packet {
        uint8_t type;
        short len;
        int id;

        uint8_t indev;
        uint8_t outdev;

        uint8_t hwlen;
        uint8_t hw_addr[6];

        unsigned char* raw_packet;
};

struct nat_instruction {
        uint8_t type;
        unsigned int address; 
        short field;
};

struct message {
        unsigned int id;
        uint8_t hook;
        struct sq_packet packet;
};

struct message_verdict {
        uint8_t verdict;
        unsigned int id;
        uint8_t hook;
	uint8_t modified;
        struct nat_instruction nat;
        unsigned char* raw_packet;
}; 

#endif
