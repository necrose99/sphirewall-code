/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SQCLIENT_H
#define SQCLIENT_H

#define SQ_HANDLER_IGNORE -1
#define SQ_DENY 0
#define SQ_ACCEPT 1
#define SQ_QUEUE 2
#define SQ_RESET 3
#define SQ_PRIORITY_QUEUE 4

#define LAYER_TYPE_234 0
#define LAYER_TYPE_7 1

#define REQUEST_QUEUE_SUCCESS 0
#define REQUEST_QUEUE_FAILED 1

#define NAT_INSTRUCTION_IGNORE 0
#define NAT_INSTRUCTION_DNAT 1
#define NAT_INSTRUCTION_SNAT 2

struct NatInstruction {
	int type;
	int targetPort;
	long targetAddress;				
	int routemark;				
};

class SqClient {
	public:

		SqClient() {
			life = -1;
		}

		int connect();
		int request_queue();
		void run();
		void disconnect();

		static int (*filter_hook)(struct sq_packet* packet, bool &modified);
		static NatInstruction* (*prerouting_hook)(struct sq_packet* packet);
		static NatInstruction* (*postrouting_hook)(struct sq_packet* packet);

		void registerFilterCallback(int (*ptr2)(struct sq_packet* packet, bool &modified)) {
			filter_hook = ptr2;
		}

		void registerPreroutingCallback(struct NatInstruction* (*ptr2)(struct sq_packet* packet)) {
			prerouting_hook = ptr2;
		}

		void registerPostroutingCallback(struct NatInstruction* (*ptr2)(struct sq_packet* packet)) {
			postrouting_hook = ptr2;
		}

		/* This is mainly for development - basically you can set the max number of packets to be 
		 * processed before run() will exit. 
		 *
		 * Essentially to prevent the module from locking you out when developing shit ^_^
		 */
		void setPacketLifeLimit(int l) {
			life = l;
		}

		void setYieldCountSetting(int* ptr){
			yieldCountSetting = ptr;
		}

		void setUsleepInterval(int* ptr){
			usleepInterval = ptr;
		}

		static bool ALIVE;
		int getQueueSize();

		static int sleepInterval;
		static int* yieldCountSetting;
		static int* usleepInterval;

		int reinject(int nid);
		static int life;

};

#endif
