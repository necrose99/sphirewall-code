/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <string>
#include <signal.h>

using namespace std;

#include "Core/System.h"
#include "Core/SignalHandler.h"
#include "Utils/Utils.h"
#include "Core/Cloud.h"

void start(map<char, string>);
void processRequest(string command, map<char, string> args);

void usage() {
	cout << "sphirewalld [start|stop] <args.....>\n";
}

int main(int argc, char *argv[]) {
	signal(SIGPIPE, sighandler);
	signal(SIGTERM, sighandler);
	signal(SIGHUP, sighandler);
	signal(SIGINT, sighandler);

	if(getuid() != 0){
		cout << "Please run sphirewall as root\n";
		exit(-1);
	}

	std::map<char, string> vars;
	string instruction;

	if (argc >= 2) {
		instruction = argv[1];

		if (argc >= 3) {
			for (int i = 2; i < argc; i++) {
				if (argv[i][0] == '-' && argv[i + 1][0] != '-') {
					vars[argv[i][1]] = argv[i + 1];
				}else if (argv[i][0] == '-') {
					vars[argv[i][1]] = " ";
				}
			}
		}

		processRequest(instruction, vars);
	}else {
		usage();
	}
	return 0;
}

void start(map<char, string> args) {
	bool doFork = true;

	/*Get the mode*/
	if (args.find('m') != args.end()) {
		string mode = args['m'];

		if (mode.compare("bind") == 0) {
			doFork = false;
		}
	}

	string configuration_root = "/etc";
        if (args.find('c') != args.end()) {
		configuration_root = args['c'];
        }

	/*Now we can actually run*/
	if (doFork) {
		pid_t pid;

		if ((pid = fork()) == -1) {
			perror("fork");
		}else {
			if (pid == 0) {
				System::getInstance()->init(configuration_root);
			}
		}
	}else {
		System::getInstance()->init(configuration_root);
	}
}

void stop() {
	//Read the pid file:
	pid_t id = System::getInstance()->getProcessId();
	if (id > 0) {
		cout << "Attempting to stop sphirewall running on PID: " << id << endl;
		kill(id, SIGTERM);
	}else{
		cout << "Could not find running process\n";
	}
}

void version() {
        //Read the pid file:
        cout << System::getInstance()->version << "\n";
	exit(0);
}


void processRequest(string command, map<char, string> args) {
	/*Split into types of request*/
	if (command.compare("start") == 0) {
		start(args);
	}else if (command.compare("stop") == 0) {
		stop();
	}else if(command.compare("version") == 0){
		version();
	}else {
		usage();
	}
}
