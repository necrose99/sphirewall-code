#include <iostream>
#include "Core/EventHandler.h"
#include "Core/HostDiscoveryService.h"

LogEventHandler *LogEventHandler::Clone() {
	return new LogEventHandler(*this);
}

LogEventHandler::LogEventHandler() {
}

LogEventHandler::LogEventHandler(const LogEventHandler &h) : EventHandler(h) {
}

void LogEventHandler::handle(EventPtr event) {
	std::stringstream ss;
	ss << "Event Caught: " + event->key;
	ss << " data = [" << event->params.toString() << "]";
	Logger::instance()->log("sphirewalld.event", INFO, ss.str());
}


FirewallBlockSourceAddressHandler::FirewallBlockSourceAddressHandler() {
}

FirewallBlockSourceAddressHandler::FirewallBlockSourceAddressHandler(const FirewallBlockSourceAddressHandler &h) : EventHandler(h) {
}

FirewallBlockSourceAddressHandler *FirewallBlockSourceAddressHandler::Clone() {
	return new FirewallBlockSourceAddressHandler(*this);
}

void FirewallBlockSourceAddressHandler::handle(EventPtr event) {
	Param *param = event->params.get("source");

	if (param) {
		unsigned int source_ip = IP4Addr::stringToIP4Addr(param->string());
		list<HostPtr> host_entries = System::getInstance()->getArp()->get_by_ip(source_ip);
		for(HostPtr host : host_entries){
			if(host){
				Logger::instance()->log("sphirewalld.eventhandlers", INFO, "Adding quarantined host");
				host->quarantined = true;
			}
		}

	}
}


