/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SPHIREWALL_EVENT_H_INCLUDED
#define SPHIREWALL_EVENT_H_INCLUDED

#include <list>
#include <vector>
#include <map>
#include <sstream>
#include "Core/Cron.h"
#include "Core/Lockable.h"
#include "Core/ConfigurationManager.h"

class Lock;
class Logger;
using namespace std;

extern const char *EVENT_PURGE;
extern const char *SYSTEM_MONITOR ;
extern const char *IDS_ALERT;
extern const char *USERDB_LOGIN_FAILED;
extern const char *USERDB_LOGIN_SUCCESS;
extern const char *USERDB_QUOTA_EXCEEDED;
extern const char *USERDB_QUOTA_OK;
extern const char *USERDB_SESSION_CREATE;
extern const char *USERDB_SESSION_TIMEOUT;
extern const char *USERDB_LOGOUT;
extern const char* USERDB_COULD_NOT_FIND_HOST;

extern const char *GLOBAL_QUOTA_EXCEEDED;
extern const char *GLOBAL_QUOTA_OK;

extern const char *AUDIT_EVENT;
extern const char *AUDIT_RPC_CALL;
extern const char *AUDIT_CONFIGURATION_CHANGED;
extern const char *AUDIT_CONFIGURATION_IDS_EXCEPTION_ADDED;
extern const char *AUDIT_CONFIGURATION_IDS_EXCEPTION_REMOVED;
extern const char *AUDIT_CONFIGURATION_NETWORK_DEVICES_TOGGLE;
extern const char *AUDIT_CONFIGURATION_NETWORK_DEVICES_SETIP;
extern const char *AUDIT_CONFIGURATION_NETWORK_DNS_SET;
extern const char *AUDIT_CONFIGURATION_NETWORK_ROUTES_ADD;
extern const char *AUDIT_CONFIGURATION_NETWORK_ROUTES_DEL;
extern const char *AUDIT_CONFIGURATION_NETWORK_DHCP_SET;
extern const char *AUDIT_CONFIGURATION_NETWORK_DHCP_PUBLISH;
extern const char *AUDIT_CONFIGURATION_NETWORK_DHCP_START;
extern const char *AUDIT_CONFIGURATION_NETWORK_DHCP_STOP;
extern const char *AUDIT_CONFIGURATION_NETWORK_CONNECTION_START;
extern const char *AUDIT_CONFIGURATION_NETWORK_CONNECTION_STOP;
extern const char *AUDIT_CONFIGURATION_GROUPDB_ADD;
extern const char *AUDIT_CONFIGURATION_GROUPDB_DEL;
extern const char *AUDIT_CONFIGURATION_USERDB_ADD;
extern const char *AUDIT_CONFIGURATION_USERDB_DEL;
extern const char *AUDIT_CONFIGURATION_USERDB_MODIFIED;
extern const char *AUDIT_CONFIGURATION_USERDB_SETPASSWORD;
extern const char *AUDIT_CONFIGURATION_USERDB_GROUPS_ADD;
extern const char *AUDIT_CONFIGURATION_USERDB_GROUPS_DEL;
extern const char *AUDIT_CONFIGURATION_USERDB_ENABLE;
extern const char *AUDIT_CONFIGURATION_USERDB_DISABLE;
extern const char *AUDIT_FIREWALL_CONNECTIONS_TERMINATE;
extern const char *AUDIT_CONFIGURATION_GROUPDB_MODIFIED;

extern const char *RUNTIME_METRIC;
extern const char *START;

extern const char *NETWORK_DEVICES_ADDED;
extern const char *NETWORK_DEVICES_CHANGED;
extern const char *NETWORK_DEVICES_REMOVED;
extern const char *NETWORK_ARP_DISCOVERED;
extern const char *NETWORK_ARP_TIMEDOUT;

extern const char *FIREWALL_WEBFILTER_HIT;
extern const char *FIREWALL_FILTERING_HIT;
extern const char *WATCHDOG_INTERNET_CONNECTION_UP;
extern const char *WATCHDOG_INTERNET_CONNECTION_DOWN;

extern const char* NETWORK_DHCPSERVER_LEASE_NEW;
extern const char* NETWORK_DHCPSERVER_LEASE_EXPIRED;
extern const char* NETWORK_DHCPSERVER_LEASE_RENEWED;
extern const char* NETWORK_DHCPSERVER_LEASE_RELEASED;

extern const char* VPN_L2TP_IPSEC_AUTHENTICATE_SUCCESS;
extern const char* VPN_L2TP_IPSEC_AUTHENTICATE_FAILURE;
extern const char* VPN_L2TP_IPSEC_AUTHENTICATE_BAD_PERMISSIONS;
extern const char* VPN_L2TP_IPSEC_JOIN;
extern const char* VPN_L2TP_IPSEC_LEAVE;
extern const char* VPN_L2TP_IPSEC_START;
extern const char* VPN_L2TP_IPSEC_STOP;

extern const char* VPN_SITE_TO_SITE_ESTABLISHING;
extern const char* VPN_SITE_TO_SITE_ESTABLISHED;
extern const char* VPN_SITE_TO_SITE_DISCONNECTED;


class Param {
	public:
		Param();
		Param(std::string value);
		Param(double value);
		Param &operator=(std::string stringValue);
		Param &operator=(double numberValue);
		std::string string() const;
		double number() const;
		bool isNumber() const;
		bool isString() const;
	private:
		std::string stringValue;
		double numberValue;

		bool s;
		bool n;
};

class EventParams {
	public:
		EventParams();
		EventParams(std::list<pair<std::string, Param>>);
		void addParam(std::string key, Param param);
		Param &operator[](const std::string &input);
		Param *get(const std::string input);
		map<string, Param> &getParams();
		std::string toString();
	private:
		map<string, Param> params;
};

class Event {
	public:
		Event(std::string key, EventParams params);
		std::string toString();

		int t;
		string key;
		EventParams params;
};

typedef boost::shared_ptr<Event> EventPtr;

class EventHandler {
	public:
		virtual void handle(EventPtr event) = 0;
		virtual void unhandle(EventPtr event) {};

		virtual string name() {
			return "";
		};
		virtual int getId() {
			return -1;
		};
		virtual std::string key() const {
			return "";
		};
		virtual bool enabled() {
			return true;
		}

		EventHandler() {}
		EventHandler(const EventHandler &h) {
		}

		virtual EventHandler *Clone() {
			return NULL;
		}

		virtual bool dontremove(){
			return false;
		}
};

class EventDb;
class EventHandlerWorker : public Lockable {
	public:
		EventHandlerWorker(EventDb *eventDb);
		void push(EventPtr event);

	private:
		void process();
		std::queue<EventPtr> workQueue;
		EventDb *eventDb;
		bool running;
};


class EventDb : public Lockable, public Configurable {
	public:
		EventDb();
		EventDb(Config *config);

		//Method as part of the CronJob class
		void add(Event *);
		void runhandlers(EventPtr e);
		int size();

		bool load();
		void save();
		std::list<EventPtr> list();
		void purgeEvents();
		void purgeOldEvents();

		void addHandler(std::string key, EventHandler *handler);
		void removeHandler(std::string key, EventHandler *handler);
		void clearHandlers();
		multimap<string, EventHandler *> &getHandlers() {
			return handlers;
		}

		void setAsync(bool value) {
			this->async = value;
		}

                const char* getConfigurationSystemName(){
                        return "Event system";
                }

	private:
		std::list<EventPtr> events;
		std::queue<EventPtr> runHandlerQueue;
		std::multimap<string, EventHandler *> handlers;
		std::map<std::string, std::string> descriptors;

		Config *config;
		bool async;

		static int EVENT_DB_PURGE_THRESHOLD;
		EventHandlerWorker *handlerWorker;
};

class EventDbPurgeCron : public CronJob {
	public:
		EventDbPurgeCron(EventDb *eventDb);
		void run();
	private:
		EventDb *eventDb;
};

#endif
