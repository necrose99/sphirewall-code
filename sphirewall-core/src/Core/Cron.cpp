/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>

using namespace std;

#include "Core/Cron.h"
#include "Core/System.h"
#include "Core/Logger.h"
#include "Utils/StringUtils.h"

void CronManager::registerJob(CronJob *job) {
	config->getRuntime()->loadOrPut(job->name, &job->cron);
	jobs.push_back(job);
}

void CronManager::roll() {
	holdLock();

	for (CronJob* job : jobs) {
		if (job->canRun()) {
			boost::thread t(boost::bind(&CronJob::lockAndExecute, job));
		}
	}

	releaseLock();
}

void CronManager::roll_no_threading() {
	holdLock();

	for (CronJob * job : jobs) {
		if (job->canRun())  job->run();
	}

	releaseLock();
}


void CronManager::start() {
	/* Some of the cron jobs take a while to run, for example the authentication sync. For this reason, we dont run them on startup, lets seed the times instead*/
	for (CronJob * job : jobs) {
		job->setLastRun(time(NULL));
	}	

	new boost::thread(boost::bind(&CronManager::run, this));
}

void CronManager::run() {
	while (true) {
		roll();
		sleep(5);
	}
}

bool CronJob::canRun() const {
	if (cron == 0) {
		return false;
	}

	if (((System::getInstance()->getTimeManager().time() - lastRun) > cron)) {
		return true;
	}

	return false;
}

void CronJob::setLastRun(int t) {
	lastRun = t;
}

void CronJob::lockAndExecute() {
	Logger::instance()->log("sphirewalld.cron", DEBUG, "Attempting to run cronjob '%s'", name.c_str());

	if (tryLock()) {
		setLastRun(System::getInstance()->getTimeManager().time());

		run();
		releaseLock();
	}
	else {
		Logger::instance()->log("sphirewalld.cron", ERROR,
				"Could not execute job '%s', it is currently locked", name.c_str());
	}
}
