/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "dirent.h"
#include <iostream>
#include <sstream>
#include <unistd.h>
#include <curl/curl.h>

using namespace std;

#include "Core/System.h"
#include "Utils/Utils.h"
#include "BandwidthDb/BandwidthDbPrimer.h"
#include "Auth/UserDb.h"
#include "Core/SignalHandler.h"
#include "SFwallCore/Firewall.h"
#include "Api/JsonManagementService.h"
#include "Api/ManagementSocketServer.h"
#include "ConnectionManager.h"
#include "Core/Cloud.h"
#include "Core/Wireless.h"
#include "Core/HostDiscoveryService.h"
#include "Utils/DynDns.h"
#include "autoconf.h"
#include "Core/Event.h"
#include "Ids/Ids.h"
#include "Core/Logger.h"
#include "Auth/WindowsWmiAuthenticator.h"
#include "SFwallCore/TimePeriods.h"
#include "BandwidthDb/CloudAnalyticsListener.h"
#include "Utils/TimeManager.h"
#include "Core/IPSec.h"
#include "Core/Vpn.h"
#include "Utils/Interfaces.h"
#include "Utils/NRoute.h"
#include "Core/TorProvider.h"
#include "Utils/DhcpServer.h"
#include "Utils/MdnsForwarder.h"
#include "Core/DetachedProcess.h"
#include "Utils/PacketCapture.h"
#include "Core/VersionNotifier.h"
#include "SFwallCore/ApplicationLevel/MacVendorMapping.h"

/* Authentication handlers */
#include "Auth/LdapAuthenticationHandler.h"
#include "Auth/BasicAuthenticationHandler.h"
#include "BandwidthDb/AuditListener.h"

#include "Utils/SphireOsUpdateHelper.h"

System *System::m_instance = NULL; // Pointer to THE System instance

System *System::getInstance() {
	if (!m_instance){
		m_instance = new System();
	}
	return m_instance;
}

void System::sysRelease() {
	if (m_instance)
		delete m_instance;

	m_instance = 0;
}

System::System()
	: timeManager() {
	version = PACKAGE_VERSION;
	std::stringstream vn;
	vn << GIT_BRANCH << ":" << GIT_VERSION << "@" << BUILD_TIME;
	versionName = vn.str();

	events= NULL;
}

System::~System() {
	delete sFirewall;
	delete arp;
	delete groupDb;
	delete userDb;
	delete sysMonitor;
	delete bandwidthDb;
	delete ids;
	delete events;
	delete cronManager;
}

pid_t System::getProcessId() {
	lockFile = "/var/lock/sphirewall.lock";
	return retLockPid(lockFile);
}

void System::init(std::string configuration_root) {
	System::getInstance()->ALIVE = true;
	bool loaded = false;

	Logger::instance()->log("sphirewalld.system.init", CONSOLE, "sphirewalld: version %s -- %s pid: %d", version.c_str(), versionName.c_str(), getpid());
	config.setConfigurationManager(&configurationManager);
	lockFile = "/var/lock/sphirewall.lock";

	if (!verifyLock(lockFile)) {
		Logger::instance()->log("sphirewalld.system.init", CONSOLE, "A lock file exists, cannot start the core daemon");
		exit(-1);
	}
	else {
		Logger::instance()->log("sphirewalld.system.init", CONSOLE, "Creating lock file");
		createLock(lockFile);
	}

	curl_global_init(CURL_GLOBAL_ALL);

	Logger::instance()->log("sphirewalld.system.init", CONSOLE, "Starting Sphirewall");
	Logger::instance()->setLevel(INFO);
	loggingConfig = new LoggingConfiguration(Logger::instance());
        SyslogRemoteListener* remote_syslog_listener = new SyslogRemoteListener();

        Logger::instance()->register_listener(new SyslogLocalListener());
        //Logger::instance()->register_listener(remote_syslog_listener);

	events = new EventDb(&config);
	periods = new TimePeriodStore();
		
	dnsConfig = new DNSConfig();
	route_manager = new NRouteManager();

	interface_manager = new IntMgr();
	interface_manager->register_change_listener(route_manager);

	dhcp_server_manager = new DDServerManager(interface_manager);
	interface_manager->register_change_listener(dhcp_server_manager);
	mdns_forwarder = new MdnsForwarder(interface_manager);
	interface_manager->register_change_listener(mdns_forwarder);

	cronManager = new CronManager(&config);
	cronManager->registerJob(new EventDbPurgeCron(events));

	version_notifier = new VersionNotifier();
	cronManager->registerJob(version_notifier);

	arp = new HostDiscoveryService(cronManager, &config);
	arp->setEventDb(events);

	ids = new IDS(events);
	groupDb = new GroupDb();
	userDb = new UserDb(events, groupDb);
	groupDb->registerRemovedListener(userDb);

	CloudAnalyticsListener *cloudAnaListener = new CloudAnalyticsListener();
	events->addHandler("", cloudAnaListener);
	cronManager->registerJob(new CloudAnalyticsListenerEventFlushCron("CLOUD_EVENTS", cloudAnaListener));

	ExternalAnalyticsListener* externalAnaListener = new ExternalAnalyticsListener();
	events->addHandler("", externalAnaListener);
	cronManager->registerJob(new CloudAnalyticsListenerEventFlushCron("EXTERNAL_EVENTS_FLUSH", externalAnaListener));

	audit_listener = new AuditListener();

	bandwidthDb = new BandwidthDbPrimer(userDb, &config);
	bandwidthDb->registerListener(cloudAnaListener);
	bandwidthDb->registerListener(externalAnaListener);
	bandwidthDb->registerListener(audit_listener);

	cronManager->registerJob(bandwidthDb);

	sFirewall = new SFwallCore::Firewall(&config);
	sFirewall->setInterfaceManager(interface_manager);

	quotaManager = new QuotaManager(userDb, groupDb, events);
	cronManager->registerJob(quotaManager);

	sysMonitor = new SysMonitor(events, cronManager, sFirewall, bandwidthDb);
	connectionManager = new ConnectionManager();
	wirelessConfiguration = new WirelessConfiguration(&configurationManager);

	globalDynDns = new GlobalDynDns();
	cronManager->registerJob(globalDynDns);

	vpnManager = new VpnManager();

	metric_store = new InMemoryMetricsStore();
	tor_provider = new TorProvider();
	packet_capture_engine = new PacketCaptureEngine();

	json_management_service = new JsonManagementService();
	json_management_service->setConfig(&config);
	json_management_service->setIds(System::getInstance()->ids);
	json_management_service->setSysMonitor(System::getInstance()->sysMonitor);
	json_management_service->setEventDb(System::getInstance()->events);
	json_management_service->setInterfaceManager(interface_manager);
	json_management_service->setDnsConfig(System::getInstance()->dnsConfig);
	json_management_service->setFirewall(System::getInstance()->sFirewall);
	json_management_service->setGroupDb(System::getInstance()->groupDb);
	json_management_service->setUserDb(System::getInstance()->userDb);
	json_management_service->setHostDiscoveryService(System::getInstance()->arp);
	json_management_service->setLoggingConfiguration(System::getInstance()->loggingConfig);
	json_management_service->setDhcp(dhcpConfigurationManager);
	json_management_service->setAuthenticationManager(authenticationManager);
	json_management_service->setConnectionManager(connectionManager);
	json_management_service->setVpnManager(vpnManager);
	json_management_service->initDelegate();

	ManagementSocketServer *webSvc = new ManagementSocketServer(8001, json_management_service, false); // Web/non-SSL CLI interface server.
	cloud = new CloudManagementConnection(json_management_service);
	wmic = new WinWmiAuth(events, this->authenticationManager, arp, &configurationManager);
	sphireos_update_helper= new SphireOsUpdateHelper();

	//Dummy constructors:
        sysMonitor->registerMetric(sFirewall);
	sysMonitor->registerMetricHandler(cloudAnaListener);
	sysMonitor->registerMetricHandler(externalAnaListener);
	sysMonitor->registerMetricHandler(metric_store);

	configurationManager.registerConfigurable(interface_manager);
	configurationManager.registerConfigurable(wmic);
	configurationManager.registerConfigurable(ids);
	configurationManager.registerConfigurable(dhcp_server_manager);
	configurationManager.registerConfigurable(mdns_forwarder);
	configurationManager.registerConfigurable(tor_provider);
	configurationManager.registerConfigurable(route_manager);
	configurationManager.registerConfigurable(periods);
	configurationManager.registerConfigurable(groupDb);
	configurationManager.registerConfigurable(userDb);
	configurationManager.registerConfigurable(quotaManager);
	configurationManager.registerConfigurable(events);
	configurationManager.registerConfigurable(connectionManager);
	configurationManager.registerConfigurable(globalDynDns);
	configurationManager.registerConfigurable(loggingConfig);
	configurationManager.registerConfigurable(config.getRuntime());
	sFirewall->registerConfigurables(&configurationManager);
	configurationManager.registerConfigurable(sFirewall);
	configurationManager.registerConfigurable(vpnManager);
        configurationManager.registerConfigurable(cloud);
        configurationManager.registerConfigurable(remote_syslog_listener);
        configurationManager.registerConfigurable(arp);

	//Lets load everything here:
	configurationManager.set_configuration_root(configuration_root);
        if(configurationManager.load("sphirewall.conf")){
               Logger::instance()->log("sphirewalld.system.init", CONSOLE, "Loading configuration from file 'sphirewall.conf' succeeded");
	}else if(configurationManager.load("sphirewall.conf", "sphirewall.conf.snapshot.latest")){
               Logger::instance()->log("sphirewalld.system.init", CONSOLE, "Loading configuration from snapshot 'sphirewall.conf.snapshot.latest' succeeded");
	       configurationManager.save();

        }else if(configurationManager.load("sphirewall.conf", "sphirewall.conf.default")){
                Logger::instance()->log("sphirewalld.system.init", CONSOLE, "Loading default configuration from file 'sphirewall.conf.default' succeeded");
                configurationManager.save();
        }else{

                Logger::instance()->log("sphirewalld.system.init", CONSOLE, "Could not load configuration, restoring defaults");
		if(configurationManager.backup()){
			Logger::instance()->log("sphirewalld.system.init", CONSOLE, "Backed up old configuration file to '/etc'");
		}

                Logger::instance()->log("sphirewalld.system.init", CONSOLE, "Loading default configuration");
		configurationManager.initDefaults();
		configurationManager.save();
                Logger::instance()->log("sphirewalld.system.init", CONSOLE, "Saved default configuration to 'sphirewall.conf'");
        }

	MacVendorMapping *mac_vendor_mapping = new MacVendorMapping();
	mac_vendor_mapping->load_mapping();
	cronManager->registerJob(new MacVendorMappingCron(mac_vendor_mapping));

	ids->refresh();
        if (wirelessConfiguration->enabled()) {
                wirelessConfiguration->start();
        }

	sFirewall->register_external_firewall_handler(vpnManager->get_l2tp());
	sFirewall->register_external_firewall_handler(vpnManager->get_sitetosite());
	sFirewall->start();

        new boost::thread(boost::bind(&CloudManagementConnection::connect, cloud));
	new boost::thread(boost::bind(&ManagementSocketServer::run, webSvc));
	new boost::thread(boost::bind(&WinWmiAuth::run, wmic));
	new boost::thread(boost::bind(&MdnsForwarder::run, mdns_forwarder));
	dhcp_server_manager->start();

	events->add(new Event(START, EventParams()));
	cronManager->start();
        Logger::instance()->register_listener(remote_syslog_listener);

	while (ALIVE) {
		sleep(10);
	}
}

void System::shutdown(){
	Logger::instance()->log("sphirewalld.system.init", CONSOLE, "sphirewalld is exiting");
	removeLock(lockFile);
	exit(-1);
}

void System::createLock(string lockfile) {
	ofstream templock(lockfile);
	templock << intToString(getpid());
}

void System::removeLock(string lockfile) {
	unlink(lockfile.c_str());
}

bool System::verifyLock(string lockfile) {
	DIR *dir = NULL;
	pid_t pid;
	struct dirent * ent;
	if (!(dir = opendir("/proc/"))) {return false;}
	if((pid = retLockPid(lockfile)) == -1) {return true;}

	while ((ent = readdir(dir)) != NULL) {
		try {
			if (ent->d_type == DT_DIR && stoi(ent->d_name) == pid) {
				return false;
			}
		} catch (const std::invalid_argument &e) {
			// Do nothing; this is simply not a process dir
		}
	}
	closedir(dir);

	Logger::instance()->log("sphirewalld.system.init", CONSOLE, "Removing stale lock file.");
	removeLock(lockfile);
	return true;
}

pid_t System::retLockPid(string lockFile) {
	ifstream fp(lockFile.c_str());

	if (fp.is_open()) {
		string str_pid;
		getline(fp, str_pid);
		return stoi(str_pid);
	}

	return -1;
}
