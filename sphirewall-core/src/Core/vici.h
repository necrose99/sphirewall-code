/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef vici_h
#define vici_h

class Buffer;
class ViciElementNotFound: public std::exception {
	public:

		ViciElementNotFound(std::string message) {
			w = message;
		}

		~ViciElementNotFound() throw () {
		}

		virtual const char* what() const throw () {
			return w.c_str();
		}

		std::string message() {
			return w;
		}

	private:
		std::string w;
};

class ViciClient;
class ViciMessage {
	friend class ViciClient;
	public:
	ViciMessage();
	~ViciMessage();

	void dump();

	string get_value(std::string key);
	list<std::string> get_list(std::string key);
	map<std::string, std::string> get_key_values(){
		return key_values;
	}
	ViciMessage* get_section(std::string key);

	bool has_section(std::string key);
	bool has_value(std::string key);

	void set_value(std::string key, std::string value);
	void set_list(std::string key, list<string> values);
	void set_section(string key, ViciMessage* section);

	private:
	ViciMessage* root;
	std::map<std::string, std::string> key_values;
	std::map<std::string, std::list<std::string> > list_items;
	std::map<std::string, ViciMessage*> sections;
};

class ViciRequest {
	public:
		ViciRequest(){
			message = NULL;
		}

		~ViciRequest(){
			delete message;
		}

		std::string command;
		u_int8_t type;

		ViciMessage* message;

};

class ViciResponse {
	public:
		ViciResponse(){
			this->message = NULL;
		}

		~ViciResponse(){
			delete message;
		}

		int len;
		u_int8_t type;
		ViciMessage* message;

		void dump();
};

enum {
	VICI_CMD_REQUEST,
	VICI_CMD_RESPONSE,
	VICI_CMD_UNKNOWN,
	VICI_EVENT_REGISTER,
	VICI_EVENT_UNREGISTER,
	VICI_EVENT_CONFIRM,
	VICI_EVENT_UNKNOWN,
	VICI_EVENT
};

class ViciClient {
	public:
		int init();
		void destroy();

		void request(ViciRequest*);
		ViciResponse* response();

	private:
		int s;
		u_int8_t read_uint8(int sock);
		char read_char(int sock);
		u_int8_t read_number_block(int s);
		std::string read_string_block(int s, int size);
		int process_key_value(int s, ViciMessage* section);
		int process_list(int s, ViciMessage* section);
		ViciMessage* handle_response(int s, int response_data_len);
		std::string parse_string_field(int s);

		void serialize_message(Buffer* buffer, ViciMessage* message);

		u_int8_t read_int8(int);
		u_int16_t read_int16(int);
		std::string parse_string_field_len8(int);
		std::string parse_string_field_len16(int);
};

#endif
