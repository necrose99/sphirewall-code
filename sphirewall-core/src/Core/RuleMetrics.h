/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SPHIREWALL_RULEMETRICS_H_
#define SPHIREWALL_RULEMETRICS_H_

#include <string>
#include "Utils/Utils.h"

class RuleMetrics {
    public:
        RuleMetrics();
        ~RuleMetrics();
        FlexibleSecondIntervalSampler* layer234Accepts;
        FlexibleSecondIntervalSampler* layer234Denies;
        FlexibleSecondIntervalSampler* layer7Accepts;
        FlexibleSecondIntervalSampler* layer7Denies;
};



#endif
