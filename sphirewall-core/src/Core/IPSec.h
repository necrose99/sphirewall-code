#ifndef IPSEC_H
#define IPSEC_H

/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <boost/shared_ptr.hpp>
#include <map>
#include <list>
#include <sstream>

#include "Core/Vpn.h"
#include "Core/vici.h"
#include "Core/ConfigurationManager.h"
#include "Auth/Group.h"
#include "Core/DetachedProcess.h"
#include "SFwallCore/Firewall.h"

class DetailedStatusSnapshot {
	public:
		std::string state;
		double bytes_in;
		double bytes_out;
};

class IPSecGatewayToGatewayInstance {
	public:
		IPSecGatewayToGatewayInstance() {
			auto_start = false;
			last_status = VPN_STATE_DEAD;
			auto_configure_firewall = false;

			remote_host = 0;	
			remote_shared_network = 0;
			remote_shared_subnet = 0;
		}

		std::string local_device;
		std::string local_shared_device;
		std::string local_id;

		InterfacePtr resolved_local_device;
		InterfacePtr resolved_shared_device;

		unsigned int remote_host;
		unsigned remote_shared_network;
		unsigned remote_shared_subnet;
		std::string remote_id;

		void initiate();
		void terminate();
		int status();
		map<string, string> detailed_status();
		int last_status;

		std::string meta;
		string name;
		bool auto_start;
		int auth_mode;
		std::string secret_key;
		bool auto_configure_firewall;

	protected:
		void __initiate_connection();
		void __terminate_connection();
		void __unload_connection();

	private:
		void __add_connection();
		void __add_keys();
};

typedef boost::shared_ptr<IPSecGatewayToGatewayInstance> IPSecGatewayToGatewayInstancePtr;

class SiteToSiteVpnManager : public SFwallCore::ExternalFirewallHandler {
	public:
		SiteToSiteVpnManager(){
			this->nat_traversal_enabled = false;
			this->logging_running = false;
		}

		void add(IPSecGatewayToGatewayInstancePtr instance);
		void remove(IPSecGatewayToGatewayInstancePtr instance);
		IPSecGatewayToGatewayInstancePtr get(std::string key);
		std::list<IPSecGatewayToGatewayInstancePtr> list_instances();

		void do_connect(IPSecGatewayToGatewayInstancePtr target);
		void do_disconnect(IPSecGatewayToGatewayInstancePtr target);
		int status(IPSecGatewayToGatewayInstancePtr target);
		std::string log(IPSecGatewayToGatewayInstancePtr target);

		bool nat_traversal_enabled;
		void init_service();

		void __logging_thread();
		void __state_monitoring_thread();
		void start_logging();

		bool load(ConfigurationManager* configuration);
		void save(ConfigurationManager* configuration);

		int efh__postrouting_handle(SFwallCore::Packet* packet);
		int efh__filter_handle(SFwallCore::Packet* packet);
		bool efh__enabled(){
			return true;
		}

	private:
		bool logging_running;
		std::map<string, IPSecGatewayToGatewayInstancePtr> instances;
};

#endif
