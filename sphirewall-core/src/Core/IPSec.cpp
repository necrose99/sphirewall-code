/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <fstream>
using namespace std;

#include "Utils/LinuxUtils.h"
#include "Utils/FileUtils.h"
#include "Core/IPSec.h"
#include "Core/Vpn.h"
#include "Core/Logger.h"
#include "Core/System.h"
#include "Utils/NRoute.h"
#include "Auth/GroupDb.h"
#include "Core/DetachedProcess.h"
#include "Core/Event.h"

#define TAB_CHAR '\t'

void SiteToSiteVpnManager::add(IPSecGatewayToGatewayInstancePtr instance){
	instances[instance->name] = instance;
}

IPSecGatewayToGatewayInstancePtr SiteToSiteVpnManager::get(std::string key){
	if(instances.find(key) != instances.end()){
		return instances[key];
	}

	return IPSecGatewayToGatewayInstancePtr();
}

void SiteToSiteVpnManager::remove(IPSecGatewayToGatewayInstancePtr instance){
	do_disconnect(instance);
	instances.erase(instance->name);
}

list<IPSecGatewayToGatewayInstancePtr> SiteToSiteVpnManager::list_instances(){
	list<IPSecGatewayToGatewayInstancePtr> ret;
	for(auto instance : instances){
		ret.push_back(instance.second);
	}
	return ret;
}

void SiteToSiteVpnManager::init_service(){
        if(!FileUtils::checkExists("/var/run/charon.pid")){
                Logger::instance()->log("sphirewalld.vpn.ipsec.init_service", INFO, "Starting ipsec services");

                string error_string;
                LinuxUtils::exec("ipsec start", error_string);
                Logger::instance()->log("sphirewalld.vpn.ipsec.init_service", INFO, error_string.c_str());
		sleep(2); /* At least give it some time to start */
        }

	start_logging();
}

void SiteToSiteVpnManager::do_connect(IPSecGatewayToGatewayInstancePtr target){
	init_service();
	target->initiate();
}

void SiteToSiteVpnManager::do_disconnect(IPSecGatewayToGatewayInstancePtr target){
	target->terminate();
}

int SiteToSiteVpnManager::status(IPSecGatewayToGatewayInstancePtr target){
	return target->status();
}

std::string SiteToSiteVpnManager::log(IPSecGatewayToGatewayInstancePtr target){
	string ret;
	stringstream read_command;
	read_command << "cat /var/log/syslog | grep sphirewalld.vpn.ipsec | grep " << target->name;
	LinuxUtils::exec(read_command.str().c_str(), ret);
	return ret;
}

void IPSecGatewayToGatewayInstance::terminate(){
	__terminate_connection();
	__unload_connection();	
}

void IPSecGatewayToGatewayInstance::initiate(){
	resolved_local_device = System::getInstance()->get_interface_manager()->get_interface(local_device);
	resolved_shared_device = System::getInstance()->get_interface_manager()->get_interface(local_shared_device);

	if(!resolved_local_device){
		Logger::instance()->log("sphirewalld.vpn.ipsec", ERROR, 
			"could not initiate connection '%s', missing binding device '%s'", name.c_str(), local_device.c_str());
		return;
	}
	
	if(!resolved_shared_device){
		Logger::instance()->log("sphirewalld.vpn.ipsec", ERROR, 
			"could not initiate connection '%s', missing shared device '%s'", name.c_str(), local_shared_device.c_str());
		return;
	}

	__add_keys();
	__add_connection();
	__initiate_connection();	
}

map<string, string> IPSecGatewayToGatewayInstance::detailed_status(){
	list<ViciResponse*> states;

	ViciClient* client = new ViciClient();
	if(client->init()){
		ViciRequest* request = new ViciRequest();
		request->type = VICI_EVENT_REGISTER;
		request->command = "list-sa";
		request->message = new ViciMessage();
		client->request(request);

		ViciRequest* request2 = new ViciRequest();
		request2->type = 0;
		request2->command = "list-sas";
		request2->message = new ViciMessage();

		client->request(request2);
		client->response();

		ViciResponse* response = NULL;
		while(response = client->response()){
			states.push_back(response);
		}

		int return_state = VPN_STATE_DEAD;
		for(ViciResponse* target: states){
			if(target->message->has_section(name)){
				ViciMessage* root_section = target->message->get_section(name);
				return root_section->get_key_values();
			}
		}

		for(ViciResponse* target: states){
			delete target;
		}

		client->destroy();
		delete request;
		delete request2;
	}

	delete client;
	return map<string, string>();
}

int IPSecGatewayToGatewayInstance::status(){
	list<ViciResponse*> states;

	ViciClient* client = new ViciClient();
	if(client->init()){
		ViciRequest* request = new ViciRequest();
		request->type = VICI_EVENT_REGISTER;
		request->command = "list-sa";
		request->message = new ViciMessage();
		client->request(request);

		ViciRequest* request2 = new ViciRequest();
		request2->type = 0;
		request2->command = "list-sas";
		request2->message = new ViciMessage();

		client->request(request2);
		client->response();

		ViciResponse* response = NULL;
		while(response = client->response()){
			states.push_back(response);
		}

		int return_state = VPN_STATE_DEAD;
		for(ViciResponse* target: states){
			if(target->message->has_section(name)){
				//We found something to work with:
				ViciMessage* root_section = target->message->get_section(name);
				std::string state = root_section->get_value("state");
				if(state.compare("CONNECTING") == 0){
					return_state = VPN_STATE_ESTABLISHING;
				}

				if(state.compare("ESTABLISHED") == 0){
					return_state = VPN_STATE_ESTABLISHED;
				}
			}
		}

		for(ViciResponse* target: states){
			delete target;
		}

		client->destroy();
		delete request;
		delete request2;
		delete client;
		return return_state;
	}

	delete client;
	return VPN_STATE_DEAD;
}

void IPSecGatewayToGatewayInstance::__add_keys(){
	ViciClient* client = new ViciClient();
	if(!client->init()){
		delete client;
		return;
	}

	ViciRequest* request = new ViciRequest();
	request->type = 0;
	request->command = "load-shared";
	request->message = new ViciMessage();
	request->message->set_value("type", "ike");
	request->message->set_value("data", secret_key);
	list<string> owners;
	owners.push_back(local_id);
	owners.push_back(remote_id);
	request->message->set_list("owners", owners);

	client->request(request);
	ViciResponse* response = client->response();

	client->destroy();
	delete client;
	delete request;
	delete response;
}

void IPSecGatewayToGatewayInstance::__add_connection(){
	ViciClient* client = new ViciClient();
	if(!client->init()){
		delete client;
		return;
	}

	ViciRequest* request = new ViciRequest();
	request->type = 0;
	request->command = "load-conn";
	request->message = new ViciMessage();
	request->message->set_section(name, new ViciMessage());

	list<std::string> local_addrs; local_addrs.push_back(IP4Addr::ip4AddrToString(resolved_local_device->get_primary_ipv4_address()));
	list<std::string> remote_addrs; remote_addrs.push_back(IP4Addr::ip4AddrToString(remote_host));

	request->message->get_section(name)->set_value("version", "0");
	request->message->get_section(name)->set_list("local_addrs", local_addrs);
	request->message->get_section(name)->set_list("remote_addrs", remote_addrs);

	request->message->get_section(name)->set_section("local-auth1", new ViciMessage());
	request->message->get_section(name + ".local-auth1")->set_value("auth", "psk");
	request->message->get_section(name + ".local-auth1")->set_value("id", local_id);

	request->message->get_section(name)->set_section("remote-auth1", new ViciMessage());
	request->message->get_section(name + ".remote-auth1")->set_value("auth", "psk");
	request->message->get_section(name + ".remote-auth1")->set_value("id", remote_id);

	//CHILD-SA
	stringstream local_subnet_string; local_subnet_string << IP4Addr::ip4AddrToString(resolved_shared_device->get_primary_ipv4_address()) << "/" << __builtin_popcount(resolved_shared_device->get_primary_ipv4_netmask());
	stringstream remote_subnet_string; remote_subnet_string << IP4Addr::ip4AddrToString(remote_shared_network) << "/" << __builtin_popcount(remote_shared_subnet);

	list<std::string> local_ts; local_ts.push_back(local_subnet_string.str());
	list<std::string> remote_ts; remote_ts.push_back(remote_subnet_string.str());

	request->message->get_section(name)->set_section("children", new ViciMessage());
	request->message->get_section(name)->get_section("children")->set_section(name, new ViciMessage());
	request->message->get_section(name)->get_section("children")->get_section(name)->set_list("local_ts", local_ts);
	request->message->get_section(name)->get_section("children")->get_section(name)->set_list("remote_ts", remote_ts);
	request->message->get_section(name)->get_section("children")->get_section(name)->set_value("mode", "tunnel");

	client->request(request);

	client->destroy();
	delete client;
	delete request;
}

void IPSecGatewayToGatewayInstance::__initiate_connection(){
	ViciClient* client = new ViciClient();
	if(!client->init()){
		delete client;
		return;
	}

	ViciRequest* request = new ViciRequest();
	request->type = 0;
	request->command = "initiate";
	request->message = new ViciMessage();
	request->message->set_value("child", name);

	client->request(request);

	client->destroy();
	delete client;
	delete request;
}

void IPSecGatewayToGatewayInstance::__terminate_connection(){
	ViciClient* client = new ViciClient();
	if(!client->init()){
		delete client;
		return;
	}

	ViciRequest* request = new ViciRequest();
	request->type = 0;
	request->command = "terminate";
	request->message = new ViciMessage();
	request->message->set_value("ike", name);

	client->request(request);

	client->destroy();
	delete client;
	delete request;
}

void IPSecGatewayToGatewayInstance::__unload_connection(){
	ViciClient* client = new ViciClient();
	if(!client->init()){
		delete client;
		return;
	}

	ViciRequest* request = new ViciRequest();
	request->type = 0;
	request->command = "unload-conn";
	request->message = new ViciMessage();
	request->message->set_value("name", name);

	client->request(request);

	client->destroy();
	delete client;
	delete request;
}

void SiteToSiteVpnManager::__logging_thread(){
	ViciClient* client = new ViciClient();
	if(!client->init()){
		delete client;
		return;
	}

	ViciRequest* request = new ViciRequest();
	request->type = VICI_EVENT_REGISTER;
	request->command = "log";
	request->message = new ViciMessage();
	client->request(request);
	client->response();

	logging_running = true;
	while(true){
		ViciResponse* response = client->response();
		if(!response){
			break;
		}

		if(response->message->has_value("ikesa-name")){
			string name = response->message->get_value("ikesa-name");
			string message = response->message->get_value("msg");

			Logger::instance()->log("sphirewalld.vpn.ipsec", INFO, "ipsec connection '%s' reported '%s'", name.c_str(), message.c_str());
		}else{
			string message = response->message->get_value("msg");
			Logger::instance()->log("sphirewalld.vpn.ipsec", INFO, "ipsec manager reported '%s'", message.c_str());
		}

		delete response;
	}

	logging_running = false;
	client->destroy();
	delete client;
	delete request;
}

void SiteToSiteVpnManager::__state_monitoring_thread(){
	while(true){
		for(IPSecGatewayToGatewayInstancePtr instance : list_instances()){
			int current_status = instance->status();
			if(current_status != instance->last_status){
				EventParams params;
				params["name"] = instance->name;
				if(current_status == VPN_STATE_ESTABLISHING){
					System::getInstance()->getEventDb()->add(new Event(VPN_SITE_TO_SITE_ESTABLISHING, params));

				}else if(current_status == VPN_STATE_ESTABLISHED){
					System::getInstance()->getEventDb()->add(new Event(VPN_SITE_TO_SITE_ESTABLISHED, params));

				}else {
					System::getInstance()->getEventDb()->add(new Event(VPN_SITE_TO_SITE_DISCONNECTED, params));
				}

				instance->last_status = current_status;
			}
		}

		sleep(1);
	}
}

void SiteToSiteVpnManager::start_logging(){
	//Launch thread
	if(!logging_running){
		new boost::thread(boost::bind(&SiteToSiteVpnManager::__logging_thread, this));
		new boost::thread(boost::bind(&SiteToSiteVpnManager::__state_monitoring_thread, this));
	}
}

int SiteToSiteVpnManager::efh__postrouting_handle(SFwallCore::Packet* packet){
	/* If traffic is coming from the local vpn subnet, then ignore it, no need to pass it to a masquerading handler */
	for(IPSecGatewayToGatewayInstancePtr instance : list_instances()){
		if(instance->auto_configure_firewall && packet->type() == IPV4){
			SFwallCore::PacketV4* ipv4 = (SFwallCore::PacketV4*) packet;
			if(IP4Addr::matchNetwork(ipv4->getDstIp(), instance->remote_shared_network, instance->remote_shared_subnet) == 0){
				return SFwallCore::EXTERNAL_HANDLER__ALLOW;
			}
		}
	}

	return SFwallCore::EXTERNAL_HANDLER__IGNORE;
}

int SiteToSiteVpnManager::efh__filter_handle(SFwallCore::Packet* packet){
	/* Allow ESP traffic to and from our remote node, and also allow IKE and NAT-T */
	for(IPSecGatewayToGatewayInstancePtr instance : list_instances()){
		if(instance->auto_configure_firewall && instance->resolved_local_device && packet->type() == IPV4){	
			SFwallCore::PacketV4* ipv4 = (SFwallCore::PacketV4*) packet;
			if((ipv4->getSrcIp() == instance->resolved_local_device->get_primary_ipv4_address() && ipv4->getDstIp() == instance->remote_host) ||
					(ipv4->getSrcIp() == instance->remote_host && ipv4->getDstIp() == instance->resolved_local_device->get_primary_ipv4_address())){

				if(packet->getProtocol() == SFwallCore::ESP){
					return SFwallCore::EXTERNAL_HANDLER__ALLOW;
				}

				if(packet->getProtocol() == SFwallCore::UDP){
					int dst_port = packet->getDstPort();
					int src_port = packet->getSrcPort();

					if(dst_port == 500 || dst_port == 4500 || src_port == 500 || src_port == 4500){
						return SFwallCore::EXTERNAL_HANDLER__ALLOW;
					}

				}
			}
		}
	}

	return SFwallCore::EXTERNAL_HANDLER__IGNORE;
}

bool SiteToSiteVpnManager::load(ConfigurationManager* configuration){
	init_service();

	if (configuration->has("vpn:ipsec")) {
		ObjectContainer *root = configuration->getElement("vpn:ipsec");
		ObjectContainer *arr1 = root->get("connections")->container();

		for (int x = 0; x < arr1->size(); x++) {
			ObjectContainer *o = arr1->get(x)->container();

			IPSecGatewayToGatewayInstance* instance = new IPSecGatewayToGatewayInstance();
			instance->auto_start= o->get("auto_start")->boolean();
			instance->auth_mode= o->get("auth_mode")->number();
			instance->secret_key= o->get("secret_key")->string();
			instance->meta= o->get("meta")->string();
			instance->name = o->get("name")->string();

			instance->local_device = o->get("local_device")->string();
			instance->local_shared_device = o->get("local_shared_device")->string();
			instance->local_id= o->get("local_id")->string();

			instance->remote_host= IP4Addr::stringToIP4Addr(o->get("remote_host")->string());
			instance->remote_shared_network = IP4Addr::stringToIP4Addr(o->get("remote_network")->string());
			instance->remote_shared_subnet = IP4Addr::stringToIP4Addr(o->get("remote_subnet")->string());
			instance->remote_id= o->get("remote_id")->string();

			IPSecGatewayToGatewayInstancePtr instance_ptr(instance);	
			add(instance_ptr);

			if(instance_ptr->auto_start){
				do_connect(instance_ptr);					
			}
		}
	}
	return true;
}

void SiteToSiteVpnManager::save(ConfigurationManager* configuration){
	ObjectContainer *root = new ObjectContainer(CREL);
	ObjectContainer *a = new ObjectContainer(CARRAY);

	for(IPSecGatewayToGatewayInstancePtr instance :  list_instances()){
		ObjectContainer* o = new ObjectContainer(CREL);

		o->put("name", new ObjectWrapper((string) instance->name));	
		o->put("auto_start", new ObjectWrapper((bool) instance->auto_start));	
		o->put("meta", new ObjectWrapper((string) instance->meta));	
		o->put("auth_mode", new ObjectWrapper((double) instance->auth_mode));	
		o->put("secret_key", new ObjectWrapper((string) instance->secret_key));	

		o->put("remote_host", new ObjectWrapper((string) IP4Addr::ip4AddrToString(instance->remote_host)));	
		o->put("remote_network", new ObjectWrapper((string) IP4Addr::ip4AddrToString(instance->remote_shared_network)));	
		o->put("remote_subnet", new ObjectWrapper((string) IP4Addr::ip4AddrToString(instance->remote_shared_subnet)));	
		o->put("remote_id", new ObjectWrapper((string) instance->remote_id));	

		o->put("local_device", new ObjectWrapper((string) instance->local_device));	
		o->put("local_shared_device", new ObjectWrapper((string) instance->local_shared_device));	
		o->put("local_id", new ObjectWrapper((string) instance->local_id));	

		a->put(new ObjectWrapper(o));
	}

	root->put("connections", new ObjectWrapper(a));
	configuration->holdLock();
	configuration->setElement("vpn:ipsec", root);
	configuration->save();
	configuration->releaseLock();
}
