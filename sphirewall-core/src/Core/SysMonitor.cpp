/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <fstream>
#include <string>
#include <time.h>
#include <sstream>
#include <proc/readproc.h>
#include <boost/unordered_map.hpp>

using namespace std;
using std::string;

#include "Core/SysMonitor.h"
#include "Core/System.h"
#include "Utils/Utils.h"
#include "SFwallCore/ConnTracker.h"
#include "Utils/TimeWrapper.h"
#include "Core/HostDiscoveryService.h"
#include "Core/Event.h"
#include "Utils/NetworkUtils.h"
#include "Auth/User.h"
#include "Auth/UserDb.h"

class GeneralMetricSampler: public MetricSampler {
	public:
		void sample(map<string, double> &items) {
			struct proc_t usage;
			look_up_our_self(&usage);
			items["system.vsize"] = usage.vsize;
			items["system.utime"] = usage.utime;
			items["system.stime"] = usage.stime;
			items["system.priority"] = usage.priority;
			items["system.nice"] = usage.nice;
			items["system.size"] = usage.size;
			
			items["sessiondb.size"] = System::getInstance()->getArp()->get_all_entries(true).size();
			items["firewall.arp.size"] = System::getInstance()->getArp()->size();
			items["firewall.arp.updates"] = System::getInstance()->getArp()->updates();

			std::vector<UserPtr> users = System::getInstance()->getUserDb()->listUserPtr();
			for (UserPtr user: users) {
				if(user){
					items["firewall.layer7.user." + user->getUserName() + ".accepts"] = user->rule_metrics->layer7Accepts->value();
					items["firewall.layer7.user." + user->getUserName() + ".denies"] = user->rule_metrics->layer7Denies->value();

					items["firewall.layer234.user." + user->getUserName() + ".accepts"] = user->rule_metrics->layer234Accepts->value();
					items["firewall.layer234.user." + user->getUserName() + ".denies"] = user->rule_metrics->layer234Denies->value();
				}
			}

			list<HostPtr> hosts = System::getInstance()->getArp()->get_all_entries(false);
			for (HostPtr host: hosts) {
				if(host){
					items["firewall.layer7.host." + host->mac + "_" + host->getIp() + ".accepts"] = host->rule_metrics->layer7Accepts->value();
					items["firewall.layer7.host." + host->mac + "_" + host->getIp() + ".denies"] = host->rule_metrics->layer7Denies->value();

					items["firewall.layer234.host." + host->mac + "_" + host->getIp() + ".accepts"] = host->rule_metrics->layer234Accepts->value();
					items["firewall.layer234.host." + host->mac + "_" + host->getIp() + ".denies"] = host->rule_metrics->layer234Denies->value();
				}
			}
		}
};


void SysMonitor::pollall() {
	for (Watch* watch : registeredWatches) {
		watch->poll();
	}

	map<string, double> input;
	for (MetricSampler* sampler : registeredSamplers){
		sampler->sample(input);
	}

	for(MetricHandler* handler : metricHandlers){
		if(handler->enabled()){
			try {
				handler->operate(input);
			}catch(exception &e){
				Logger::instance()->log("sysmonitor.metrics", ERROR, "A metric hander failed to execute for reason %s", e.what());
			}
		}
	}
}

void SysMonitor::registerMetric(MetricSampler *sampler) {
	registeredSamplers.push_back(sampler);
}

void SysMonitor::registerMetricHandler(MetricHandler* handler){
	metricHandlers.push_back(handler);
}

SysMonitor::SysMonitor(EventDb *eventDb, CronManager *cronManager, SFwallCore::Firewall *firewall, BandwidthDbPrimer *bandwidthDb) {
	this->eventDb = eventDb;
	this->firewall = firewall;

	InternetConnectionWatch *icw = new InternetConnectionWatch(&System::getInstance()->configurationManager);
	icw->setEventDb(eventDb);

	registeredWatches.push_back(icw);
	cronManager->registerJob(new SysMonitorCron(this));

	registerMetric(new GeneralMetricSampler());
}

void InternetConnectionWatch::poll() {
	if (config->hasByPath("watchdog:monitorGoogle") && config->get("watchdog:monitorGoogle")->boolean()) {
		int res = NetworkUtils::ping("google.com");

		if (res == -1 && state) {
			eventDb->add(new Event(WATCHDOG_INTERNET_CONNECTION_DOWN, EventParams()));
			state = false;
		}
		else if (res != -1 && !state) {
			eventDb->add(new Event(WATCHDOG_INTERNET_CONNECTION_UP, EventParams()));
			state = true;
		}
	}
}

void InMemoryMetricsStore::operate(std::map<std::string, double> input){
	holdLock();
	for(pair<string, double> item : input){
		store[item.first].push_back(InMemoryMetricsStorePointPtr(new InMemoryMetricsStorePoint(time(NULL), item.second)));
	}

	if((time(NULL) - last_cleanup_time) > 60 * 60){
		cleanup();	
	}

	releaseLock();
}

list<InMemoryMetricsStorePointPtr> InMemoryMetricsStore::query(string metric){
	return store[metric];
}

#define METRIC_RETENSION_PERIOD 86400 
void InMemoryMetricsStore::cleanup(){
	map<string, list<InMemoryMetricsStorePointPtr> >::iterator oiter;
	list<InMemoryMetricsStorePointPtr>::iterator liter;

	for(oiter = store.begin(); oiter != store.end(); oiter++){
		for(liter = oiter->second.begin(); liter != oiter->second.end(); liter++){	
			InMemoryMetricsStorePointPtr ptr = (*liter);
			if ((time(NULL) - ptr->timestamp) > METRIC_RETENSION_PERIOD){
				liter = oiter->second.erase(liter);	
			}
		}
	}
}

list<string> InMemoryMetricsStore::available_types(){
	list<string> ret;
	for(pair<string, list<InMemoryMetricsStorePointPtr> > item : store){
		ret.push_back(item.first);
	}
	return ret;
}
