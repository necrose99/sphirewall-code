/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SPHIREWALL_SYSTEM_H_INCLUDED
#define SPHIREWALL_SYSTEM_H_INCLUDED

#include "Cron.h"
#include "Config.h"
#include "Core/ConfigurationManager.h"
#include "Core/Logger.h"
#include "Utils/TimeManager.h"

class AuthenticationManager;
class Dhcp3ConfigurationManager;
class ConnectionManager;
class OpenvpnManager;
class WirelessConfiguration;
class CloudManagementConnection;
class HostDiscoveryService;
class GlobalDynDns;
class GroupDb;
class UserDb;
class IDS;
class SysMonitor;
class BandwidthDbPrimer;

class EventDb;
class QuotaManager;
class EventDb;
class InterfaceManager;
class DNSConfig;
class WinWmiAuth;
class TimePeriodStore;
class IPSecManager;
class VpnManager;
class IntMgr;
class NRouteManager;
class JsonManagementService;
class InMemoryMetricsStore;
class TorProvider;
class DDServerManager;
class MdnsForwarder;
class PacketCaptureEngine;
class AuditListener;
class VersionNotifier;
class SphireOsUpdateHelper;

namespace SFwallCore {
	class Firewall;
};

class System {
		friend class CloudManagementConnection;
	public:
		std::string version;
		std::string versionName;

		/*!Return an instance of System*/
		static System *getInstance();
		static void sysRelease();

		void init(std::string configuration_root);
		void shutdown();

		pid_t getProcessId();
		bool ALIVE;
		string lockFile;

		ConfigurationManager configurationManager;
		Config config;

		TimePeriodStore *periods;
		SFwallCore::Firewall *getFirewall() {
			return sFirewall;
		}

		BandwidthDbPrimer *getBandwidthDbPrimer() {
			return bandwidthDb;
		}

		IDS *getIds() {
			return ids;
		}

		GroupDb *getGroupDb() {
			return groupDb;
		}

		UserDb *getUserDb() {
			return userDb;
		}

		HostDiscoveryService *getArp() {
			return arp;
		}

		CronManager *getCronManager() {
			return cronManager;
		}

		EventDb *getEventDb() {
			return events;
		}

		WirelessConfiguration *getWirelessConfiguration() {
			return wirelessConfiguration;
		}

		CloudManagementConnection *getCloudConnection() {
			return cloud;
		}

		void setCloudConnection(CloudManagementConnection *cloud) {
			this->cloud = cloud;
		}

		GlobalDynDns *getGlobalDynDns() {
			return globalDynDns;
		}

		QuotaManager *getQuotaManager() {
			return quotaManager;
		}

		ConfigurationManager *getConfigurationManager() {
			return &configurationManager;
		}

		TimeManager &getTimeManager() {
			return timeManager;
		}

		IPSecManager* getIPSecManager(){
			return ipsecManager;
		}

		VpnManager* getVpnManager(){
			return vpnManager;
		}

		IntMgr* get_interface_manager(){
			return interface_manager;
		}

		NRouteManager* get_route_manager(){
			return route_manager;
		}	

		DNSConfig* get_dns_config(){
			return dnsConfig;
		}
		WinWmiAuth *wmic;
		JsonManagementService* get_json_management_service(){
			return json_management_service;
		}

		InMemoryMetricsStore* get_metric_store(){
			return metric_store;
		}
		
		TorProvider* get_tor_provider(){
			return tor_provider;
		}

		DDServerManager* get_dhcp_server_manager(){
			return dhcp_server_manager;
		}

		MdnsForwarder* get_mdns_forwarder(){
			return mdns_forwarder;
		}

		PacketCaptureEngine* get_capture_engine(){
			return packet_capture_engine;
		}

		SphireOsUpdateHelper* get_sphireos_update_helper(){
			return sphireos_update_helper;
		}
	private:
		System();
		System(const System &);
		System &operator=(const System &);
		virtual ~System();
		static System *m_instance;

		void createLock(std::string lockfile);
		void removeLock(std::string lockfile);
		bool verifyLock(std::string lockfile);
		pid_t retLockPid(string lockFile);

		//Processes
		SysMonitor *sysMonitor;
		SFwallCore::Firewall *sFirewall;
		Dhcp3ConfigurationManager *dhcpConfigurationManager;
		OpenvpnManager *openvpn;
		BandwidthDbPrimer *bandwidthDb;
		IDS *ids;

		GroupDb *groupDb;
		UserDb *userDb;
		EventDb *events;
		HostDiscoveryService *arp;
		DNSConfig* dnsConfig;
		ConnectionManager *connectionManager;

		CronManager *cronManager;
		AuthenticationManager *authenticationManager;
		LoggingConfiguration *loggingConfig;

		WirelessConfiguration *wirelessConfiguration;
		CloudManagementConnection *cloud;
		GlobalDynDns *globalDynDns;
		QuotaManager *quotaManager;
		TimeManager timeManager;
		IPSecManager* ipsecManager;
		VpnManager* vpnManager;
		IntMgr* interface_manager;
		NRouteManager* route_manager;
		JsonManagementService* json_management_service;
		InMemoryMetricsStore* metric_store;
		TorProvider* tor_provider;
		DDServerManager* dhcp_server_manager;
		MdnsForwarder* mdns_forwarder;
		PacketCaptureEngine* packet_capture_engine;
		AuditListener* audit_listener;
		VersionNotifier* version_notifier;
		SphireOsUpdateHelper* sphireos_update_helper;
};

#endif
