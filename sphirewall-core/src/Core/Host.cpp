/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Host.h"
#include "Utils/IP4Addr.h"
#include "Utils/IP6Addr.h"
#include "SFwallCore/Packet.h"

string Host::getIp() {
	if (type == 0) {
		return IP4Addr::ip4AddrToString(ip);
	}
	else if (type == 1) {
		return IP6Addr::toString(&ipV6);
	}

	return "invalid";
}

void Host::fetchHostname() {
	hostnameCache = IP4Addr::hostname(ip);
}

std::string Host::hostname() {
	return hostnameCache;
}

void Host::increment(SFwallCore::Packet* packet){
	total_packet_count++;
	total_transfer_count += packet->getLen();
	lastSeen = time(NULL);
}

Host::~Host() {
	delete this->rule_metrics;
}

bool Host::has_user_timed_out(){
        if(timeout__absolute_timeout_value > 0){
                if(time(NULL) > timeout__absolute_timeout_value){
                        return true;
                }
        }

	if(timeout__idle_timeout_value > 0){
                int elapsed_time = time(NULL) - lastSeen;
                if(elapsed_time > timeout__idle_timeout_value){
                        return true;
                }
        }

        return false;
}

