/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <boost/asio.hpp>
#include <vector>
#include <sstream>
#include <boost/algorithm/string.hpp>
#include <boost/asio/ssl.hpp>

using namespace boost::algorithm;

#include "Core/Cloud.h"
#include "Api/JsonManagementService.h"
#include "Core/System.h"
#include "Core/ConfigurationManager.h"
#include "Utils/FileUtils.h"
#include "Utils/StringUtils.h"
#include "SFwallCore/Acl.h"
#include "SFwallCore/Firewall.h"
#include "SFwallCore/Criteria.h"

using namespace std;

int CloudManagementConnection::CLOUD_CONNECTION_POOL_SIZE = 10;
CloudManagementConnection::CloudManagementConnection(JsonManagementService* json)
	: connectionCount(0), instanceid(StringUtils::genRandom()), json_management_service(json){ 

		System::getInstance()->config.getRuntime()->loadOrPut("CLOUD_CONNECTION_POOL_SIZE", &CLOUD_CONNECTION_POOL_SIZE);	
	}

std::string CloudManagementConnection::getHostname() {
	if (configurationManager->hasByPath("cloud:hostname")) {
		return configurationManager->get("cloud:hostname")->string();
	}

	//deprecated properties
	return "api.linewize.net";
}

std::string CloudManagementConnection::getKey() {
	if (configurationManager->hasByPath("cloud:key")) {
		return configurationManager->get("cloud:key")->string();
	}

	return "";
}

std::string CloudManagementConnection::deviceid = "";
std::string CloudManagementConnection::getDeviceId() {
	if (deviceid.size() == 0) {
		if (configurationManager->hasByPath("cloud:deviceid")) {
			deviceid = configurationManager->get("cloud:deviceid")->string();
			if(deviceid.size() > 0){
				return deviceid;
			}
		}

		deviceid = FileUtils::read("/sys/class/net/eth0/address");
		trim(deviceid);
	}

	return deviceid;
}

std::string CloudManagementConnection::getWatchdogId(){
	string ret = FileUtils::read("/sys/class/net/eth0/address");
	trim(ret);
	return ret;
}

int CloudManagementConnection::getPort() {
	if (configurationManager->hasByPath("cloud:port")) {
		return configurationManager->get("cloud:port")->number();
	}

	return 8084;
}

bool CloudManagementConnection::enabled() {
	if (configurationManager->hasByPath("cloud:enabled")) {
		return configurationManager->get("cloud:enabled")->boolean();
	}

	return false;
}

bool CloudManagementConnection::verify() {
	if (configurationManager->hasByPath("cloud:verify")) {
		return configurationManager->get("cloud:verify")->boolean();
	}

	return true;
}


bool CloudManagementConnection::connected() {
	return this->connectionCount > 0;
}

typedef boost::shared_ptr<boost::asio::io_service> io_service_ptr;

void CloudManagementConnection::single_connector(JsonManagementService *service){
	connectionCount++;
	try {
		boost::asio::io_service io;
		stringstream ss; ss << getPort();
		ssl::context ctx(ssl::context::sslv23);

		if(verify()){
			ctx.set_verify_mode(boost::asio::ssl::verify_peer | boost::asio::ssl::verify_fail_if_no_peer_cert);
			ctx.set_default_verify_paths();
		}

		ssl_socket sock(io, ctx);
		if(verify()){
			sock.set_verify_callback(ssl::rfc2818_verification(getHostname()));
		}

		tcp::resolver resolver(io);
		tcp::resolver::query query(tcp::v4(), getHostname(), ss.str());
		boost::asio::connect(sock.lowest_layer(), resolver.resolve(query));

		sock.lowest_layer().set_option(boost::asio::socket_base::keep_alive(true));
		sock.lowest_layer().set_option(tcp::no_delay(true));
		sock.handshake(ssl_socket::client);

		Logger::instance()->log("sphirewalld.cloud.ssl", INFO, "Connecting to a sphirewall cloud provider at %s:%d", getHostname().c_str(), getPort());
		stringstream auth; 
		auth << "{\"key\":\"" << getKey() << "\", \"deviceid\":\"" << getDeviceId() << "\",\"sphirewallid\":\""<< instanceid << "\", \"watchdogid\":\"" << getWatchdogId()<< "\"}\n";
		boost::asio::write(sock, boost::asio::buffer(auth.str(), auth.str().size()));

		Logger::instance()->log("sphirewalld.cloud.ssl",  INFO, "Connected to cloud provider, ready to accept rpc calls");
		while (1) {
			boost::asio::streambuf response;
			boost::asio::read_until(sock, response, "\n");
			string buffer = string((istreambuf_iterator<char>(&response)), istreambuf_iterator<char>());
			pair<string, int> output = service->process(buffer, getHostname(), true);
			output.first += "\n";

			Logger::instance()->log("sphirewalld.cloud.ssl",  DEBUG, "returned %s", output.first.c_str());
			boost::asio::write(sock, boost::asio::buffer(output.first, output.first.size()));
		}

		delete service;
		io.stop();
	}
	catch (exception &e) {
		Logger::instance()->log("sphirewalld.cloud", ERROR, "Connection to the cloud service could not be established or was dropped due to %s", e.what());
		
	}

	connectionCount--;
}

void CloudManagementConnection::connect() {
	while (System::getInstance()->ALIVE) {
		if(enabled() && connectionCount < CLOUD_CONNECTION_POOL_SIZE){
			Logger::instance()->log("sphirewalld.cloud", INFO, "Cloud connection pool is less than CLOUD_CONNECTION_POOL_SIZE, creating connection");
			try{
				boost::thread(boost::bind(&CloudManagementConnection::single_connector, this, json_management_service));
			}catch(const exception& e){
				Logger::instance()->log("sphirewalld.cloud", ERROR, "Could not create thread for cloud connection, reason %s", e.what());
			}
		}			

		sleep(2);
	}

}
