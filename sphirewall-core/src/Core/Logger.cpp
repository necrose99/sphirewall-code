/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <queue>
#include <syslog.h>
#include <sstream>
#include <map>
#include <stdarg.h>

#include <string.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <errno.h>
#include <unistd.h>


#include "Core/Logger.h"
#include "Utils/Utils.h"
#include "Utils/TimeUtils.h"
using namespace std;

Logger* Logger::ins = NULL;
void Logger::log(std::string context, Priority p, std::string msg, ...) {
	vector<string> lines;
	split(msg, '\n', lines);
	for(std::string line : lines){
		va_list args;
		char buffer[BUFSIZ];
		va_start(args, msg);
		if(line.size() < BUFSIZ){
			vsnprintf(buffer, sizeof buffer, line.c_str(), args);
		}
		va_end(args);

		if(p == CONSOLE){
			cout << context << " :: " << buffer << endl;
		}

		//Find context to log this to:
		if (p <= level || p == CONSOLE) {
			Log nLog;
			nLog.context = context;
			nLog.priority = p;
			nLog.message = buffer;
			nLog.timestamp = time(NULL);

			for(LogListener* listener : listeners){
				listener->enqueue(nLog);
			}
		}else{
			//Check other priorities:
			for(map<string, Priority>::iterator iter = priorities.begin();
					iter != priorities.end();
					iter++){

				if(context.find(iter->first) != string::npos && p <= iter->second){
					Log nLog;
					nLog.context = context;
					nLog.priority = p;
					nLog.message = buffer;
					nLog.timestamp = time(NULL);

					for(LogListener* listener : listeners){
						listener->enqueue(nLog);
					}
					break;
				}
			}
		}
	}
}

void SyslogLocalListener::process_log(const Log& log){
	openlog("sphirewall", LOG_CONS | LOG_PID | LOG_NDELAY, LOG_LOCAL1);
	std::stringstream msg; msg << log.context << " :: " << log.message;

	int syslogPriority = SyslogPriority::getSyslogPriority(log.priority);
	syslog(syslogPriority, "%s", msg.str().c_str());

	closelog();
}

bool SyslogRemoteListener::is_enabled(){
	if (configurationManager->hasByPath("general:remote_syslog_enabled")) {
		return configurationManager->get("general:remote_syslog_enabled")->boolean();
	}

	return false;

}

int SyslogRemoteListener::port(){
	if (configurationManager->hasByPath("general:remote_syslog_port")) {
		return configurationManager->get("general:remote_syslog_port")->number();
	}

	return -1;
}

std::string SyslogRemoteListener::hostname(){
	if (configurationManager->hasByPath("general:remote_syslog_hostname")) {
		return configurationManager->get("general:remote_syslog_hostname")->string();
	}

	return "";
}

void SyslogRemoteListener::process_log(const Log& log){
	/* This is a bit inefficient, lets fix it up one day */
	if(is_enabled()){
		int sock = socket(AF_INET, SOCK_DGRAM, 0);
		if (sock < 0)
		{
			return;
		}

		struct hostent *host_name_entry = gethostbyname(hostname().c_str());
		if (host_name_entry == NULL)
		{
			close(sock);
			return;
		}

		struct sockaddr_in addr;
		memset(&addr, 0, sizeof(addr));
		addr.sin_family = AF_INET;
		memcpy(&addr.sin_addr.s_addr, host_name_entry->h_addr, host_name_entry->h_length);
		addr.sin_port = htons(port());

		std::stringstream msg; msg << log.context << " :: " << log.message << "\n";
		sendto(sock, msg.str().c_str(), msg.str().size(), 0, (sockaddr*)&addr, sizeof(addr));
		close(sock);
	}
}

Logger::Logger() : criticalPathEnabled(false), level(ERROR)
{}
