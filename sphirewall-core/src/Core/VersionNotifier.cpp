#include <iostream>

using namespace std;

#include "VersionNotifier.h"
#include "Core/System.h"
#include "Utils/HttpRequestWrapper.h"
#include "Utils/StringUtils.h"

VersionNotifier::VersionNotifier() 
	: CronJob(60 * 60, "VERSION_NOTIFIER", false){

	this->uuid = StringUtils::genRandom();
}

void VersionNotifier::run(){
	stringstream ss;
	ss << "http://stats.linewize.net/push/";
	ss << this->uuid;
	ss << "/";
	ss << System::getInstance()->version;

	HttpRequestWrapper request_to_endpoint(ss.str(), GET);	

	try {
		request_to_endpoint.execute();
	}catch(HttpRequestWrapperException& ex){
	}
}

