/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef HOST_H
#define HOST_H

#include <string>
#include <netinet/in.h>
#include <boost/shared_ptr.hpp>

#include "Auth/User.h"
#include "Utils/Interfaces.h"
#include "Core/RuleMetrics.h"

namespace SFwallCore {
	class Packet;
	class Fingerprint;
	typedef boost::shared_ptr<Fingerprint> FingerprintPtr;
};

class Host {
	public:
		Host(){
			quarantined = false;
			lastSeen = 0;
			firstSeen = 0;

			total_packet_count = 0;
			total_transfer_count = 0;
			total_active_connections = 0;

			authenticated_user_login_time=0;
			timeout__idle_timeout_value = 0;
			timeout__absolute_timeout_value = 0;
			rule_metrics = new RuleMetrics();
				
			device_fingerprint_retry_time = 0;
			device_fingerprint_retry_count = 0;
		}

		~Host();

		std::string getIp();
		std::string hostname();
		void fetchHostname();

		std::string mac;
		std::string vendor;
		in_addr_t ip;
		struct in6_addr ipV6;
		InterfacePtr interface;

		int lastSeen;
		int firstSeen;
		int type;
		bool quarantined;

		double total_packet_count;
		double total_transfer_count;
		double total_active_connections;

		UserPtr authenticated_user;	

		double timeout__idle_timeout_value;
		double timeout__absolute_timeout_value;
		double authenticated_user_login_time;
		std::string authentication_provider;	

		bool has_user_timed_out();
		void increment(SFwallCore::Packet* packet);
		RuleMetrics* rule_metrics;

		/* Fingerprinting stuff*/
		SFwallCore::FingerprintPtr device_fingerprint;
		int device_fingerprint_retry_time;
		int device_fingerprint_retry_count;
	private:
		std::string hostnameCache;
};

typedef boost::shared_ptr<Host> HostPtr;

#endif // HOST_H
