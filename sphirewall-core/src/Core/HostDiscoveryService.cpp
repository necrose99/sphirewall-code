/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>

using namespace std;

#include "Core/HostDiscoveryService.h"
#include "Utils/IP4Addr.h"
#include "Utils/IP6Addr.h"
#include "Utils/Utils.h"
#include "Core/System.h"
#include "Core/Event.h"
#include "Utils/Lock.h"
#include "Core/Lockable.h"
#include "SFwallCore/Packet.h"
#include "SFwallCore/Firewall.h"
#include "SFwallCore/Alias.h"
#include "Core/Host.h"
#include "Utils/TimeWrapper.h"

int HostDiscoveryService::HOST_DISCOVERY_SERVICE_TIMEOUT = 60 * 10;
int HostDiscoveryService::PERSIST_SESSIONS_FOR_RESTORE= 0;

long HostDiscoveryService::updates(){
	return _updates;
}

HostPtr HostDiscoveryService::update(unsigned int ip, std::string mac, InterfacePtr interface) {
	if (tryLock()) {
		HostPtr arpEntry = getByMac(mac, ip, interface);
		_updates++;

		if (arpEntry) {
			arpEntry->lastSeen = time(NULL);
			if(!arpEntry->authenticated_user){
				PersistedSessionPtr persisted_session = persisted_session_store->check_for_persisted_session(mac);
				if(persisted_session){
					authenticate_user_to_host(arpEntry, System::getInstance()->getUserDb()->getUser(persisted_session->username), "provider.persistedsession", -1, false); 
				}
			}
		}
		else {
			arpEntry = add(mac, ip, interface);

			PersistedSessionPtr persisted_session = persisted_session_store->check_for_persisted_session(mac);
			if(persisted_session){
				authenticate_user_to_host(arpEntry, System::getInstance()->getUserDb()->getUser(persisted_session->username),"provider.persistedsession", -1, false);
			}
		}

		releaseLock();
		return arpEntry;
	}

	return HostPtr();
}

HostPtr HostDiscoveryService::update(struct in6_addr *ip, std::string mac, InterfacePtr interface) {
	if (tryLock()) {
		HostPtr arpEntry = getByMac(mac, ip, interface);
		_updates++;
		if (arpEntry) {
			arpEntry->lastSeen = time(NULL);
			if(!arpEntry->authenticated_user){
				PersistedSessionPtr persisted_session = persisted_session_store->check_for_persisted_session(mac);
				if(persisted_session){
					authenticate_user_to_host(arpEntry, System::getInstance()->getUserDb()->getUser(persisted_session->username),"provider.persistedsession", -1, false); 
				}
			}
		}
		else {
			arpEntry = add(mac, ip, interface);
                        PersistedSessionPtr persisted_session = persisted_session_store->check_for_persisted_session(mac);
                        if(persisted_session){
                                authenticate_user_to_host(arpEntry, System::getInstance()->getUserDb()->getUser(persisted_session->username),"provider.persistedsession", -1, false); 
                        }
		}

		releaseLock();
		return arpEntry;
	}

	return HostPtr();
}

HostPtr HostDiscoveryService::update(SFwallCore::Packet *pk) {
	if (pk->type() == IPV4) {
		SFwallCore::PacketV4 *v4 = (SFwallCore::PacketV4 *) pk;

		if (IP4Addr::isLocal(v4->getSrcIp()) && v4->getHw().size() != 0) {
			return update(v4->getSrcIp(), pk->getHw(), pk->getSourceNetDevice());
		}
	}
	else if (pk->type() == IPV6) {
		SFwallCore::PacketV6 *v4 = (SFwallCore::PacketV6 *) pk;

		if (v4->getSrcIp() && v4->getHw().size() != 0) {
			return update(v4->getSrcIp(), pk->getHw(), pk->getSourceNetDevice());
		}
	}

	return HostPtr();
}

HostPtr HostDiscoveryService::getByMac(string mac, in_addr_t ip, InterfacePtr interface) {
	boost::unordered_map<string, std::list<HostPtr> >::iterator iter = entryTable.find(mac);

	if (iter != entryTable.end()) {
		std::list<HostPtr> entries = iter->second;

		for (HostPtr entry : entries) {
			if (entry->type == 0 && entry->ip == ip && entry->interface == interface) {
				return entry;
			}
		}
	}

	return HostPtr();
}

HostPtr HostDiscoveryService::getByMac(string mac, struct in6_addr *ip, InterfacePtr interface) {
	boost::unordered_map<string, std::list<HostPtr> >::iterator iter = entryTable.find(mac);

	if (iter != entryTable.end()) {
		std::list<HostPtr> entries = iter->second;

		for (HostPtr entry : entries) {
			if (entry->type == 1) {
				if (memcmp(ip, &entry->ipV6, sizeof(struct in6_addr)) == 0 && interface == entry->interface) {
					return entry;
				}
			}
		}
	}

	return HostPtr();
}

std::list<HostPtr> HostDiscoveryService::get_all_entries(bool authenticated){
	holdLock();
	std::list<HostPtr> ret = get_all_entries_nonlocking(authenticated);
	releaseLock();
	return ret;
}

std::list<HostPtr> HostDiscoveryService::get_all_entries_nonlocking(bool authenticated){
	std::list<HostPtr> ret;
	for (boost::unordered_map<string, std::list<HostPtr> >::iterator iter = entryTable.begin(); iter != entryTable.end(); iter++) {
		for (HostPtr entry : iter->second) {
			if(authenticated){
				if(entry->authenticated_user){
					ret.push_back(entry);
				}
			}else{
				ret.push_back(entry);
			}
		}
	}
	return ret;
}

list<HostPtr> HostDiscoveryService::get_by_mac(std::string mac){
	list<HostPtr> ret;
	holdLock();
	ret = entryTable[mac];
	releaseLock();
	return ret;
}

list<HostPtr> HostDiscoveryService::get_by_ip(in_addr_t ip){
	list<HostPtr> ret;
	holdLock();
	for (std::pair<std::string, std::list<HostPtr> > entries : entryTable) {
		for (HostPtr entry : entries.second) {
			if (entry->type == 0 && entry->ip == ip) {
				ret.push_back(entry);
			}
		}
	}
	releaseLock();
	return ret;
}

HostPtr HostDiscoveryService::get_by_ip_mac(in_addr_t ip, std::string mac){
	holdLock();
	boost::unordered_map<string, std::list<HostPtr> >::iterator iter = entryTable.find(mac);
	if (iter != entryTable.end()) {
		std::list<HostPtr> entries = iter->second;

		for (HostPtr entry : entries) {
			if (entry->type == 0 && entry->ip == ip) {
				releaseLock();
				return entry;
			}
		}
	}

	releaseLock();
	return HostPtr();
}

string HostDiscoveryService::get_vendor(string mac) {
	string mac_prefix = mac.substr(0, 8);
	boost::unordered_map<string, string>::iterator iter = this->mac_vendor_table.find(mac_prefix);
	string vendor;
	if (iter == mac_vendor_table.end()) {
		vendor = "-";
	} else {
		vendor = iter->second;
	}
	return vendor;
}


void HostDiscoveryService::CleanUpCron::run() {
	int t = time(NULL) - HOST_DISCOVERY_SERVICE_TIMEOUT;
	root->holdLock();
	vector<string> rowsToDelete;
	int session_removed = 0;
	int hosts_removed = 0;
	Logger::instance()->log("sphirewalld.arp.cleanup", INFO, "Removing stale host and user session entries");

	for (boost::unordered_map<string, std::list<HostPtr> >::iterator iter = root->entryTable.begin(); iter != root->entryTable.end(); iter++) {
		for (std::list<HostPtr>::iterator eiter = iter->second.begin(); eiter != iter->second.end(); eiter++) {

			HostPtr target = (*eiter);
			/* Do we have a user Object? If so then we must evaluate the user based timeouts */
			if(target->authenticated_user){
				if(target->has_user_timed_out()){
					if(System::getInstance()->getEventDb()){
						EventParams params;
						params["user"] = target->authenticated_user->getUserName();
						params["mac"] = target->mac;
						params["ip"] = target->getIp();
						System::getInstance()->getEventDb()->add(new Event(USERDB_SESSION_TIMEOUT, params));
					}

					root->deauthenticate_user_to_host(target, target->authenticated_user);
					session_removed++;
				}else{
					continue;
				}
			}

			if (target->lastSeen < t) {
				EventParams params;
				params["ip"] = target->getIp();
				params["hw"] = target->mac;

				if (root->eventDb) {
					root->eventDb->add(new Event(NETWORK_ARP_TIMEDOUT, params));
				}

				eiter = iter->second.erase(eiter);
				root->_size -= 1;
				hosts_removed++;
			}
		}

		if (iter->second.size() == 0) {
			rowsToDelete.push_back(iter->first);
		}
	}

	for (unsigned x = 0; x < rowsToDelete.size(); x++) {
		root->entryTable.erase(rowsToDelete[x]);
	}

	Logger::instance()->log("sphirewalld.arp.cleanup", INFO, "Removed %d stale host entries, and %d user sessions", hosts_removed, session_removed);
	root->releaseLock();
        if(root->PERSIST_SESSIONS_FOR_RESTORE == 1){
                root->save();
        }
}

HostPtr HostDiscoveryService::add(string mac, in_addr_t ip, InterfacePtr interface) {
	if (mac.size() == 17) {
		HostPtr entry(new Host());
		entry->mac = mac;
		entry->vendor = get_vendor(mac);
		entry->ip = ip;
		entry->firstSeen = time(NULL);
		entry->lastSeen = time(NULL);
		entry->type = 0;
		entry->interface = interface;

		/* Check that the ip address is not already in use*/
		for (boost::unordered_map<string, std::list<HostPtr> >::iterator iter = entryTable.begin(); iter != entryTable.end(); iter++) {
			for (std::list<HostPtr>::iterator eiter = iter->second.begin(); eiter != iter->second.end(); eiter++) {
				HostPtr old_entry = (*eiter);
				if (old_entry->type == 0 && old_entry->ip == ip && old_entry->interface == interface) {
					Logger::instance()->log("sphirewalld.arp.add", ERROR, "Found a host entry collision, removing old entry %s/%s and replacing with %s/%s",
							old_entry->mac.c_str(), old_entry->getIp().c_str(), entry->mac.c_str(), entry->getIp().c_str());

					eiter = iter->second.erase(eiter);
					_size -= 1;
				}
			}
		}

		/* Right, now that we know there can be no collisions, lets add it*/
		dnsHostnameWorker->push(entry);
		entryTable[mac].push_back(entry);
		_size += 1;

		if (eventDb) {
			EventParams params;
			params["ip"] = IP4Addr::ip4AddrToString(ip);
			params["hw"] = mac;

			if (eventDb) {
				eventDb->add(new Event(NETWORK_ARP_DISCOVERED, params));
			}
		}

		return entry;
	}

	return HostPtr();
}


HostPtr HostDiscoveryService::add(string mac, struct in6_addr *ip, InterfacePtr interface) {
	if (mac.size() == 17) {
		HostPtr entry(new Host());
		entry->mac = mac;
		entry->vendor = get_vendor(mac);
		memcpy(&entry->ipV6, ip, sizeof(struct in6_addr));
		entry->firstSeen = time(NULL);
		entry->lastSeen = time(NULL);
		entry->type = 1;
		entry->interface = interface;
		dnsHostnameWorker->push(entry);
		entryTable[mac].push_back(entry);
		_size += 1;

		if (eventDb) {
			EventParams params;
			params["ip"] = IP6Addr::toString(ip);
			params["hw"] = mac;

			if (eventDb) {
				eventDb->add(new Event(NETWORK_ARP_DISCOVERED, params));
			}
		}

		return entry;
	}

	return HostPtr();
}


int HostDiscoveryService::size() {
	return _size;
}

HostDiscoveryService::HostDiscoveryService(CronManager *cron, Config *config) :  _size(0), _updates(0) {
	cron->registerJob(new CleanUpCron(this));
	config->getRuntime()->loadOrPut("HOST_DISCOVERY_SERVICE_TIMEOUT", &HOST_DISCOVERY_SERVICE_TIMEOUT);
	config->getRuntime()->loadOrPut("PERSIST_SESSIONS_FOR_RESTORE", &PERSIST_SESSIONS_FOR_RESTORE);
	eventDb = NULL;

	persisted_session_store = new PersistedSessionStore();
	timeout_configuration_store = new TimeoutConfigurationStore();
	dnsHostnameWorker = new HostDnsHostnameResolverWorker();
}


HostDiscoveryService::HostDiscoveryService(): _size(0), _updates(0) {
	eventDb = NULL;
	dnsHostnameWorker = new HostDnsHostnameResolverWorker();
	persisted_session_store = new PersistedSessionStore();
	timeout_configuration_store = new TimeoutConfigurationStore();
}

void HostDiscoveryService::setEventDb(EventDb *eventDb) {
	this->eventDb = eventDb;
}

HostDnsHostnameResolverWorker::HostDnsHostnameResolverWorker()
	: running(false) {}

	void HostDnsHostnameResolverWorker::push(HostPtr hostPtr) {
		holdLock();
		workQueue.push(hostPtr);

		if (!running) {
			running = true;
			releaseLock();
			boost::thread(boost::bind(&HostDnsHostnameResolverWorker::process, this));
		}
		else {
			releaseLock();
		}
	}

void HostDnsHostnameResolverWorker::process() {
	while (true) {
		holdLock();

		if (workQueue.empty()) {
			releaseLock();
			break;
		}

		HostPtr host = workQueue.front();
		workQueue.pop();
		releaseLock();

		if (host) {
			host->fetchHostname();
		}
	}

	running = false;
}

bool HostDiscoveryService::is_user_allowed_to_login(HostPtr current_host, UserPtr user){
	int current_user_sessions = 0;
	for (HostPtr host : get_all_entries_nonlocking(true)) {
		if (host->authenticated_user && host->authenticated_user == user) {
			current_user_sessions++;
		}
	}
	int lowest_concurrent_sessions = 0;
	for(TimeoutConfigurationPtr nbt: get_authentication_timeout_configurations()) {
		if (nbt->matches(current_host->ip, user)) {
			if(nbt->sessions_limit != 0 && (lowest_concurrent_sessions == 0 || nbt->sessions_limit < lowest_concurrent_sessions)) {
				lowest_concurrent_sessions = nbt->sessions_limit;
			}
		}
	}
	if (lowest_concurrent_sessions != 0 && current_user_sessions >= lowest_concurrent_sessions) {
		Logger::instance()->log("sphirewalld.arp.login", INFO, "User %s reached maximum number of concurrent sessions (%s)",
				user->getUserName().c_str(), lowest_concurrent_sessions);
		return false;
	}

	return true;
}

int HostDiscoveryService::authenticate_user_to_host(HostPtr host, UserPtr user, const char* provider, int timeout, bool absolute_timeout){
	if(!user){
		Logger::instance()->log("sphirewalld.arp.login", ERROR, "Attempted to authenticate an invalid user object");
		return OK;
	}	

	//Determine Timeouts
	host->authentication_provider = provider;	

	if(timeout == -1){
		for(TimeoutConfigurationPtr nbt: get_authentication_timeout_configurations()){
			if(nbt->matches(host->ip, user)){
				if(nbt->type == IDLE){
					if(host->timeout__idle_timeout_value > nbt->value || host->timeout__idle_timeout_value == 0){
						host->timeout__idle_timeout_value = nbt->value;
					}
				}else if(nbt->type == ELAPSED){
					if(host->timeout__absolute_timeout_value > (nbt->value + time(NULL)) || host->timeout__absolute_timeout_value == 0){
						host->timeout__absolute_timeout_value = nbt->value + time(NULL);
					}
				}else if(nbt->type == TIME){
					int base_time = time(NULL);
					if(nbt->mon){
						Time* next = Time::nextDays(base_time, 1);
						next->set_24h_time(nbt->value);

						if(next->timestamp() < base_time){
							next->add_days(7);
						}

						if(host->timeout__absolute_timeout_value == 0 || host->timeout__absolute_timeout_value > next->timestamp()){
							host->timeout__absolute_timeout_value = next->timestamp();
						}

						delete next;
					}			

					if(nbt->tues){
						Time* next = Time::nextDays(base_time, 2);
						next->set_24h_time(nbt->value);
						if(next->timestamp() < base_time){
							next->add_days(7);
						}

						if(host->timeout__absolute_timeout_value == 0 || host->timeout__absolute_timeout_value > next->timestamp()){
							host->timeout__absolute_timeout_value = next->timestamp();
						}
						delete next;
					}

					if(nbt->wed){
						Time* next = Time::nextDays(base_time, 3);
						next->set_24h_time(nbt->value);

						if(next->timestamp() < base_time){
							next->add_days(7);
						}

						if(host->timeout__absolute_timeout_value == 0 || host->timeout__absolute_timeout_value > next->timestamp()){
							host->timeout__absolute_timeout_value = next->timestamp();
						}
						delete next;
					}

					if(nbt->thur){
						Time* next = Time::nextDays(base_time, 4);
						next->set_24h_time(nbt->value);
						if(next->timestamp() < base_time){
							next->add_days(7);
						}

						if(host->timeout__absolute_timeout_value == 0 || host->timeout__absolute_timeout_value > next->timestamp()){
							host->timeout__absolute_timeout_value = next->timestamp();
						}
						delete next;
					}

					if(nbt->fri){
						Time* next = Time::nextDays(base_time, 5);
						next->set_24h_time(nbt->value);

						if(next->timestamp() < base_time){
							next->add_days(7);
						}

						if(host->timeout__absolute_timeout_value == 0 || host->timeout__absolute_timeout_value > next->timestamp()){
							host->timeout__absolute_timeout_value = next->timestamp();
						}
						delete next;
					}

					if(nbt->sat){
						Time* next = Time::nextDays(base_time, 6);
						next->set_24h_time(nbt->value);

						if(next->timestamp() < base_time){
							next->add_days(7);
						}

						if(host->timeout__absolute_timeout_value == 0 || host->timeout__absolute_timeout_value > next->timestamp()){
							host->timeout__absolute_timeout_value = next->timestamp();
						}
						delete next;
					}

					if(nbt->sun){
						Time* next = Time::nextDays(base_time, 0);
						next->set_24h_time(nbt->value);

						if(next->timestamp() < base_time){
							next->add_days(7);
						}

						if(host->timeout__absolute_timeout_value == 0 || host->timeout__absolute_timeout_value > next->timestamp()){
							host->timeout__absolute_timeout_value = next->timestamp();
						}
						delete next;
					}
				}
			}
		}

		if(host->timeout__absolute_timeout_value == 0 && host->timeout__idle_timeout_value == 0){
			int default_idle_timeout = configurationManager->hasByPath("authentication:session_timeout") ?
				configurationManager->get("authentication:session_timeout")->number() : 0;
			int default_timeout_mode = configurationManager->hasByPath("authentication:session_timeout_mode") ?
				configurationManager->get("authentication:session_timeout_mode")->number() : 0;

			if(default_timeout_mode == IDLE){
				host->timeout__idle_timeout_value = configurationManager->hasByPath("authentication:session_timeout") ?
					configurationManager->get("authentication:session_timeout")->number() : 0;
			}else if(default_timeout_mode == ELAPSED){
				host->timeout__absolute_timeout_value = configurationManager->hasByPath("authentication:session_timeout") ?
					configurationManager->get("authentication:session_timeout")->number() : 0;
				host->timeout__absolute_timeout_value += time(NULL);
			}
		}

		if (!is_user_allowed_to_login(host, user)) {
			return DEVICE_LIMIT_EXCEEDED;
		}else{
			host->authenticated_user = user;
			host->authenticated_user_login_time = time(NULL);

			Logger::instance()->log("sphirewalld.arp.login", INFO, "User session was created from %s/%s for user %s",
					host->mac.c_str(), host->getIp().c_str(), user->getUserName().c_str());
		}

	}else{
		if (!is_user_allowed_to_login(host, user)) {
			return DEVICE_LIMIT_EXCEEDED;
		}else{
			host->timeout__absolute_timeout_value = timeout;
			host->authenticated_user = user;
			host->authenticated_user_login_time = time(NULL);

			Logger::instance()->log("sphirewalld.arp.login", INFO, "User session was created from %s/%s for user %s",
					host->mac.c_str(), host->getIp().c_str(), user->getUserName().c_str());
		}
	}

	if(System::getInstance()->getEventDb()){
		EventParams params;
		params["user"] = user->getUserName();
		params["ip"] = IP4Addr::ip4AddrToString(host->ip);
		params["idle_timeout"] = host->timeout__idle_timeout_value;
		params["absolute_timeout"] = host->timeout__absolute_timeout_value;
		params["hw"] = host->mac;
		params["mac"] = host->mac;
		params["provider"] = provider; 
		System::getInstance()->getEventDb()->add(new Event(USERDB_LOGIN_SUCCESS, params));
	}

	trigger_user_login_handlers(host);
	return OK;
}

void HostDiscoveryService::deauthenticate_user_to_host(HostPtr host, UserPtr user){
	Logger::instance()->log("sphirewalld.arp.logout", INFO, "User session was removed from %s/%s for user %s", 
			host->mac.c_str(), host->getIp().c_str(), user->getUserName().c_str());

	host->authenticated_user = UserPtr();	
	host->timeout__absolute_timeout_value= 0;
	host->timeout__idle_timeout_value= 0;
	host->authenticated_user_login_time = 0;
}

void HostDiscoveryService::persist_authentication_session(HostPtr host, UserPtr user){
	add_persisted_authentication_session(host->mac, user->getUserName());
}

void HostDiscoveryService::trigger_user_login_handlers(HostPtr host){
	for(UserLoginListener* listener : registered_user_login_listeners){
		listener->user_logged_in(host);	
	}
}

bool TimeoutConfiguration::__matches_network(unsigned int ip){
	if(networks.size() == 0){
		return true;
	}

	for(SFwallCore::AliasPtr a : networks){
		if(a->searchForNetworkMatch(ip)){
			return true;
		}
	}

	return false;
}

bool TimeoutConfiguration::__matches_groups(UserPtr user){
	if(groups.size() == 0){
		return true;
	}

	for(GroupPtr group : groups){
		if(user->checkForGroup(group)){
			return true;
		}
	}

	return false;
}


bool TimeoutConfiguration::matches(unsigned int ip, UserPtr user){
	return __matches_network(ip) && __matches_groups(user);
}

std::list<TimeoutConfigurationPtr> TimeoutConfigurationStore::get_authentication_timeout_configurations(){
	std::list<TimeoutConfigurationPtr> ret;
	holdLock();
	ret = timeout_configurations;
	releaseLock();
	return timeout_configurations;
}

void TimeoutConfigurationStore::remove_authentication_timeout_configuration(TimeoutConfigurationPtr timeout){
	holdLock();
	timeout_configurations.remove(timeout);
	releaseLock();
}

void TimeoutConfigurationStore::add_authentication_timeout_configuration(TimeoutConfigurationPtr timeout){
	holdLock();
	timeout_configurations.push_back(timeout);
	releaseLock();
}

void TimeoutConfigurationStore::clear(){
	holdLock();
	timeout_configurations.clear();
	releaseLock();
}

void PersistedSessionStore::add_persisted_authentication_session(std::string mac_address, std::string username) {
	holdLock();
	persisted_sessions[mac_address] = PersistedSessionPtr(new PersistedSession(mac_address, username));
	releaseLock();
}

void PersistedSessionStore::remove_persisted_authentication_session(std::string mac_address, std::string username){
	holdLock();
	persisted_sessions.erase(mac_address);
	releaseLock();
}

PersistedSessionPtr PersistedSessionStore::check_for_persisted_session(std::string mac_address){
	if(persisted_sessions.find(mac_address) != persisted_sessions.end()){
		return persisted_sessions[mac_address];
	}

	return PersistedSessionPtr();
}

std::list<PersistedSessionPtr> PersistedSessionStore::get_persisted_authentication_sessions(){
	std::list<PersistedSessionPtr> ret;

	holdLock();
	for (pair<std::string, PersistedSessionPtr> target : persisted_sessions) {
		ret.push_back(target.second);
	}
	releaseLock();
	return ret;
}

void PersistedSessionStore::clear(){
	holdLock();
	persisted_sessions.clear();
	releaseLock();
}

void HostDiscoveryService::save(){
	this->holdLock();

	Logger::instance()->log("sphirewalld.sessiondb", EVENT, "Saving Sessions");
	if (configurationManager) {
		ObjectContainer *root = new ObjectContainer(CREL);
		ObjectContainer *array = new ObjectContainer(CARRAY);

		for (PersistedSessionPtr target : get_persisted_authentication_sessions()) {
			ObjectContainer *session = new ObjectContainer(CREL);

			session->put("username", new ObjectWrapper(target->username));
			session->put("macAddress", new ObjectWrapper(target->mac));

			array->put(new ObjectWrapper(session));
		}

		root->put("sessions", new ObjectWrapper(array));

		if(PERSIST_SESSIONS_FOR_RESTORE == 1){
			ObjectContainer *states= new ObjectContainer(CARRAY);
			for (HostPtr target : get_all_entries_nonlocking(true)) {
				ObjectContainer *state= new ObjectContainer(CREL);

				state->put("username", new ObjectWrapper(target->authenticated_user->getUserName()));
				state->put("mac", new ObjectWrapper(target->mac));
				state->put("ip", new ObjectWrapper(target->getIp()));
				state->put("timeout__idle_timeout_value", new ObjectWrapper((double) target->timeout__idle_timeout_value));
				state->put("timeout__absolute_timeout_value", new ObjectWrapper((double) target->timeout__absolute_timeout_value));
				state->put("authenticated_user_login_time", new ObjectWrapper((double) target->authenticated_user_login_time));
				state->put("authentication_provider", new ObjectWrapper((string) target->authentication_provider));
				state->put("interface", new ObjectWrapper((string) target->interface->name));

				states->put(new ObjectWrapper(state));
			}
			root->put("states", new ObjectWrapper(states));
		}

		ObjectContainer *timeoutArray = new ObjectContainer(CARRAY);
		for (TimeoutConfigurationPtr timeout : get_authentication_timeout_configurations()) {
			ObjectContainer *to= new ObjectContainer(CREL);

			to->put("id", new ObjectWrapper((string) timeout->id));

			to->put("timeout", new ObjectWrapper((double) timeout->value));
			to->put("type", new ObjectWrapper((double) timeout->type));
			to->put("sessions_limit", new ObjectWrapper((double) timeout->sessions_limit));
			to->put("mon", new ObjectWrapper((bool) timeout->mon));
			to->put("tues", new ObjectWrapper((bool) timeout->tues));
			to->put("wed", new ObjectWrapper((bool) timeout->wed));
			to->put("thur", new ObjectWrapper((bool) timeout->thur));
			to->put("fri", new ObjectWrapper((bool) timeout->fri));
			to->put("sat", new ObjectWrapper((bool) timeout->sat));
			to->put("sun", new ObjectWrapper((bool) timeout->sun));

			ObjectContainer *networksArray= new ObjectContainer(CARRAY);
			for(SFwallCore::AliasPtr network : timeout->networks){
				networksArray->put(new ObjectWrapper(network->id));
			}

			to->put("networks", new ObjectWrapper(networksArray));

			ObjectContainer *groupArray = new ObjectContainer(CARRAY);
			for(GroupPtr group: timeout->groups){
				groupArray->put(new ObjectWrapper((double) group->getId()));
			}

			to->put("groups", new ObjectWrapper(groupArray));

			timeoutArray->put(new ObjectWrapper(to));
		}

		root->put("network_based_timeouts", new ObjectWrapper(timeoutArray));

		configurationManager->holdLock();
		configurationManager->setElement("sessionDb", root);
		configurationManager->save();
		configurationManager->releaseLock();
	}

	this->releaseLock();
}

bool HostDiscoveryService::load(){
	this->holdLock();
	persisted_session_store->clear();
	timeout_configuration_store->clear();

	Logger::instance()->log("sphirewalld.sessiondb", EVENT, "Loading Sessions");

	if (!configurationManager->has("sessionDb")) {
		Logger::instance()->log("sphirewalld.sessiondb", ERROR, "Loading session database failed, no sessions found. Ignoring");
	}
	else {
		ObjectContainer* root = configurationManager->getElement("sessionDb");
		ObjectContainer* sessions = root->get("sessions")->container();

		for (int x = 0; x < sessions->size(); x++) {
			ObjectContainer* o = sessions->get(x)->container();
			std::string mac = o->get("macAddress")->string();
			std::string username = o->get("username")->string();
			add_persisted_authentication_session(mac, username);
		}

		if(PERSIST_SESSIONS_FOR_RESTORE == 1){
			if(root->has("states")){
				ObjectContainer* nbto = root->get("states")->container();
				for (int x = 0; x < nbto->size(); x++) {
					ObjectContainer* o = nbto->get(x)->container();
					Host* restored_host = new Host();				
					restored_host->mac = o->get("mac")->string();
					restored_host->ip = IP4Addr::stringToIP4Addr(o->get("ip")->string());
					restored_host->timeout__idle_timeout_value = o->get("timeout__idle_timeout_value")->number();
					restored_host->timeout__absolute_timeout_value= o->get("timeout__absolute_timeout_value")->number();
					restored_host->authenticated_user_login_time= o->get("authenticated_user_login_time")->number();
					restored_host->authentication_provider= "provider.restored"; 
					restored_host->authenticated_user = System::getInstance()->getUserDb()->getUser(o->get("username")->string()); 
					restored_host->interface = System::getInstance()->get_interface_manager()->get_interface(o->get("interface")->string()); 
					restored_host->type = 0; 

					entryTable[restored_host->mac].push_back(HostPtr(restored_host));
				}
			}
		}

		if(root->has("network_based_timeouts")){
			ObjectContainer* nbto = root->get("network_based_timeouts")->container();

			for (int x = 0; x < nbto->size(); x++) {
				ObjectContainer* o = nbto->get(x)->container();

				TimeoutConfigurationPtr nbtp = TimeoutConfigurationPtr(new TimeoutConfiguration());
				nbtp->id = o->get("id")->string();
				nbtp->value = o->get("timeout")->number();
				if(o->has("sessions_limit")) {
					nbtp->sessions_limit = o->get("sessions_limit")->number();
				}
				if(o->has("type")){
					nbtp->type = o->get("type")->number();
					nbtp->mon= o->get("mon")->boolean();
					nbtp->tues= o->get("tues")->boolean();
					nbtp->wed = o->get("wed")->boolean();
					nbtp->thur= o->get("thur")->boolean();
					nbtp->fri= o->get("fri")->boolean();
					nbtp->sat= o->get("sat")->boolean();
					nbtp->sun= o->get("sun")->boolean();
				}

				SFwallCore::AliasDb* aliases = System::getInstance()->getFirewall()->aliases;
				ObjectContainer* networks = o->get("networks")->container();

				for(int y = 0; y < networks->size(); y++){
					SFwallCore::AliasPtr network = aliases->get(networks->get(y)->string());
					if(network){
						nbtp->networks.push_back(network);
					}
				}

				if(o->has("groups")){
					ObjectContainer* groups = o->get("groups")->container();
					for(int y = 0; y < groups->size(); y++){
						GroupPtr group = System::getInstance()->getGroupDb()->getGroup(groups->get(y)->number());
						if(group){
							nbtp->groups.push_back(group);
						}
					}
				}

				add_authentication_timeout_configuration(nbtp);
			}
		}
	}

	this->releaseLock();
	return true;
}

std::list<TimeoutConfigurationPtr> HostDiscoveryService::get_authentication_timeout_configurations(){
	return timeout_configuration_store->get_authentication_timeout_configurations();
}

void HostDiscoveryService::remove_authentication_timeout_configuration(TimeoutConfigurationPtr conf){
	timeout_configuration_store->remove_authentication_timeout_configuration(conf);
}

void HostDiscoveryService::add_authentication_timeout_configuration(TimeoutConfigurationPtr conf){
	timeout_configuration_store->add_authentication_timeout_configuration(conf);
}

/* Persisted session methods */
void HostDiscoveryService::add_persisted_authentication_session(std::string mac_address, std::string username){
	persisted_session_store->add_persisted_authentication_session(mac_address, username);
}

void HostDiscoveryService::remove_persisted_authentication_session(std::string mac_address, std::string username){
	persisted_session_store->remove_persisted_authentication_session(mac_address, username);
}

std::list<PersistedSessionPtr> HostDiscoveryService::get_persisted_authentication_sessions(){
	return persisted_session_store->get_persisted_authentication_sessions();
}

