/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <list>
#include <gtest/gtest.h>

using namespace std;

#include "Core/Event.h"

static bool FakeHandlerHandled = false;
static bool BaseHandlerHandled = false;

class BaseHandler : public EventHandler {
	public:

		void handle(EventPtr event) {
			BaseHandlerHandled = true;
		}

		string name() {
			return "BaseHandler";
		}

		int getId() {
			return 3;
		}

		BaseHandler *Clone() {
			return new BaseHandler(*this);
		}

		std::string key() const {
			return "base.handler";
		};
};

class FakeHandler : public EventHandler {
	public:

		std::string key() const {
			return "fake.handler";
		};

		void handle(EventPtr event) {
			FakeHandlerHandled = true;
		}

		string name() {
			return "FakeHandler";
		}

		int getId() {
			return 1;
		}

		FakeHandler *Clone() {
			return new FakeHandler(*this);
		}
};

TEST(EventDb,  installHandler) {
	EventDb *db = new EventDb();
	db->clearHandlers();
	EventHandler *handler1 = new BaseHandler();
	EventHandler *handler2 = new FakeHandler();

	db->addHandler("event", handler1);
	db->addHandler("event.bitching", handler2);

	EXPECT_TRUE(db->getHandlers().size() == 2);
}

TEST(EventDb,  removeHandler) {
	EventDb *db = new EventDb();
	db->clearHandlers();
	EventHandler *handler1 = new BaseHandler();
	EventHandler *handler2 = new FakeHandler();

	db->addHandler("event", handler1);
	db->addHandler("event.bitching", handler2);

	EXPECT_TRUE(db->getHandlers().size() == 2);
	db->removeHandler("event.bitching", handler2);

	EXPECT_TRUE(db->getHandlers().size() == 1);
}

TEST(EventDb,  handlerTriggeredWithEvent) {
	EventDb *db = new EventDb();
	db->setAsync(false);
	db->clearHandlers();
	EventHandler *handler1 = new BaseHandler();
	EventHandler *handler2 = new FakeHandler();

	db->addHandler("event", handler1);
	db->addHandler("event.bitching", handler2);

	db->add(new Event("event.shit", EventParams()));
	EXPECT_TRUE(BaseHandlerHandled == true);
	EXPECT_TRUE(FakeHandlerHandled == false);
}

TEST(EventDb,  handleMultipleHandlers) {
	EventDb *db = new EventDb();
	db->setAsync(false);
	db->clearHandlers();
	EventHandler *handler1 = new BaseHandler();
	EventHandler *handler2 = new FakeHandler();

	db->addHandler("event", handler1);
	db->addHandler("event.bitching", handler2);

	db->add(new Event("event.bitching", EventParams()));
	EXPECT_TRUE(BaseHandlerHandled == true);
	EXPECT_TRUE(FakeHandlerHandled == true);
}

TEST(EventDb,  list) {
	EventDb *db = new EventDb();

	db->add(new Event("event", EventParams()));
	db->add(new Event("event.bitching", EventParams()));

	list<EventPtr> events = db->list();
	EXPECT_TRUE(events.size() == 2);
}

TEST(EventDb,  purge) {
	EventDb *db = new EventDb();

	db->add(new Event("event", EventParams()));
	db->add(new Event("event.bitching", EventParams()));

	db->purgeEvents();

	list<EventPtr> events = db->list();
	EXPECT_TRUE(events.size() == 1);
}
