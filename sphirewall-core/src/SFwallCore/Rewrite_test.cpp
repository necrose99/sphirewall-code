#include <iostream>
#include <gtest/gtest.h>

using namespace std;

#include "test.h"
#include "SFwallCore/TestFactory.h"
#include "SFwallCore/Rewrite.h"
#include "SFwallCore/Connection.h"
#include "SFwallCore/State.h"
#include "SFwallCore/Packet.h"
#include "SFwallCore/ApplicationLevel/Handlers/DnsHandler.h"
#include "SFwallCore/ApplicationLevel/Handlers/HttpHandler.h"
#include "SFwallCore/ApplicationLevel/FilterHandler.h"

#include "SFwallCore/ApplicationLevel/CapturePortal.h"
#include "SFwallCore/ApplicationLevel/GoogleSafeSearchEnforcer.h"
#include <Utils/DnsType.h>
#include <Core/System.h>
#include <Utils/Utils.h>
#include "Core/Cloud.h"

using namespace SFwallCore;

HttpHandler h;

TEST(CapturePortalEngine, ignoreIfSessionExists) {
	MockFactory *tester = new MockFactory();
	SFwallCore::CapturePortalEngine *cp = new SFwallCore::CapturePortalEngine(new SFwallCore::AliasDb());

	Packet *packet = PacketBuilder::createTcp("10.1.1.1", "88.1.1.1", 12334, 80);
	TcpConnection *connection = new TcpConnection(packet);
	connection->getApplicationInfo()->type = SFwallCore::ConnectionApplicationInfo::Classifier::HTTP;
        connection->getState()->state = TCPUP;

	HostPtr host = HostPtr(new Host());
	host->authenticated_user = UserPtr(new User());
	connection->setHostDiscoveryServiceEntry(host);

        EXPECT_FALSE(cp->matchesIpCriteria(connection, packet));
}

TEST(CapturePortalEngine, ignoreIfNotTcp_and_capture_all_false) {
	MockFactory *tester = new MockFactory();
	SFwallCore::CapturePortalEngine *cp = new SFwallCore::CapturePortalEngine(new SFwallCore::AliasDb());
	Packet *packet = PacketBuilder::createTcp("10.1.1.1", "8.8.8.8", 12334, 80);
	Connection *connection = new UdpConnection(packet);
	connection->getApplicationInfo()->type = SFwallCore::ConnectionApplicationInfo::Classifier::HTTP;

        HostPtr host = HostPtr(new Host());
        connection->setHostDiscoveryServiceEntry(host);

        EXPECT_FALSE(cp->matchesIpCriteria(connection, packet));
}

TEST(CapturePortalEngine, ignoreIfStateNotUp_and_capture_all_false) {
	MockFactory *tester = new MockFactory();

        SFwallCore::CapturePortalEngine *cp = new SFwallCore::CapturePortalEngine(new SFwallCore::AliasDb());
	Packet *packet = PacketBuilder::createTcp("10.1.1.1", "8.8.8.8", 12334, 80);
	TcpConnection *connection = new TcpConnection(packet);
	connection->getApplicationInfo()->type = SFwallCore::ConnectionApplicationInfo::Classifier::HTTP;
        connection->getState()->state = TIME_WAIT;

        HostPtr host = HostPtr(new Host());
        connection->setHostDiscoveryServiceEntry(host);

        EXPECT_FALSE(cp->matchesIpCriteria(connection, packet));
}

TEST(CapturePortalEngine, tcp_and_capture_all_true) {
	MockFactory *tester = new MockFactory();
	SFwallCore::CapturePortalEngine *cp = new SFwallCore::CapturePortalEngine(new SFwallCore::AliasDb());
	Packet *packet = PacketBuilder::createTcp("10.1.1.1", "8.8.8.8", 12334, 80);
	Connection *connection = new UdpConnection(packet);
	cp->capture_all_traffic = true;

        HostPtr host = HostPtr(new Host());
        connection->setHostDiscoveryServiceEntry(host);

        EXPECT_TRUE(cp->matchesIpCriteria(connection, packet));
}

TEST(CapturePortalEngine, ignoreIfLocalAndIgnoreLocalTrue) {
	MockFactory *tester = new MockFactory();

        SFwallCore::CapturePortalEngine *cp = new SFwallCore::CapturePortalEngine(new SFwallCore::AliasDb());
	Packet *packet = PacketBuilder::createTcp("10.1.1.1", "192.168.1.1", 12334, 80);
	TcpConnection *connection = new TcpConnection(packet);
	cp->allow_local = true;

        HostPtr host = HostPtr(new Host());
        connection->setHostDiscoveryServiceEntry(host);

        EXPECT_FALSE(cp->matchesIpCriteria(connection, packet));
}

TEST(CapturePortalEngine, matches_and_capture_all_false) {
	MockFactory *tester = new MockFactory();
        SFwallCore::CapturePortalEngine *cp = new SFwallCore::CapturePortalEngine(new SFwallCore::AliasDb());

	Packet *packet = PacketBuilder::createTcp("10.1.1.1", "8.8.8.8", 12334, 80);
	TcpConnection *connection = new TcpConnection(packet);
	connection->getApplicationInfo()->type = SFwallCore::ConnectionApplicationInfo::Classifier::HTTP;
        connection->getState()->state = TCPUP;

        EXPECT_TRUE(cp->matchesIpCriteria(connection, packet));
}

TEST(CapturePortalEngine, redirectToAuthPage) {
	MockFactory *tester = new MockFactory();
	ConfigurationManager *config = new ConfigurationManager();

        SFwallCore::CapturePortalEngine *cp = new SFwallCore::CapturePortalEngine(new SFwallCore::AliasDb());
	cp->setConfigurationManager(config);
        cp->mode = 1;
	cp->endpoint = 0;
	cp->cp_url =  "http://google.com";

	Alias *alias = new IpSubnetAlias();
        alias->addEntry("10.1.1.1/255.255.255.255");
	cp->provided_inclusions_aliases.push_back(std::shared_ptr<Alias>(alias));
	cp->initopts();

	Packet *packet = PacketBuilder::createTcp("10.1.1.1", "192.168.1.1", 12334, 80);
	TcpConnection *connection = new TcpConnection(packet);
        connection->getState()->state = TCPUP;
	connection->getApplicationInfo()->type = SFwallCore::ConnectionApplicationInfo::Classifier::HTTP;

        cp->process(connection, packet);
        EXPECT_TRUE(connection->getRewriteRule() != NULL);
}

TEST(CapturePortalEngine, redirectToAuthPage_mode_all_traffic) {
	ConfigurationManager *config = new ConfigurationManager();

        SFwallCore::CapturePortalEngine *cp = new SFwallCore::CapturePortalEngine(new SFwallCore::AliasDb());
	cp->setConfigurationManager(config);
        cp->mode = 2;
	cp->endpoint = 0;
	cp->cp_url =  "http://google.com";
	cp->initopts();

	Packet *packet = PacketBuilder::createTcp("10.1.1.1", "192.168.1.1", 12334, 80);
	TcpConnection *connection = new TcpConnection(packet);
        connection->getState()->state = TCPUP;
	connection->getApplicationInfo()->type = SFwallCore::ConnectionApplicationInfo::Classifier::HTTP;

        cp->process(connection, packet);
        EXPECT_TRUE(connection->getRewriteRule() != NULL);
}

TEST(CapturePortalEngine, captive_portal_detection_negative_hit) {
        ConfigurationManager *config = new ConfigurationManager();

        SFwallCore::CapturePortalEngine *cp = new SFwallCore::CapturePortalEngine(new SFwallCore::AliasDb());
        cp->setConfigurationManager(config);
        cp->mode = 2;
        cp->endpoint = 0;
        cp->disable_apple_captive_prompt= true;
        cp->cp_url =  "http://google.com";
        cp->initopts();

        Packet *packet = PacketBuilder::createTcp("10.1.1.1", "192.168.1.1", 12334, 80);
        TcpConnection *connection = new TcpConnection(packet);
        connection->getState()->state = TCPUP;
        connection->getApplicationInfo()->type = SFwallCore::ConnectionApplicationInfo::Classifier::HTTP;
        connection->getApplicationInfo()->http_useragentSet = true;
        connection->getApplicationInfo()->http_useragent= "invalid_user agent";

        cp->process(connection, packet);
        EXPECT_TRUE(connection->getRewriteRule() != NULL);
}

TEST(CapturePortalEngine, captive_portal_detection_positive_hit) {
        ConfigurationManager *config = new ConfigurationManager();

        SFwallCore::CapturePortalEngine *cp = new SFwallCore::CapturePortalEngine(new SFwallCore::AliasDb());
        cp->setConfigurationManager(config);
        cp->mode = 2;
        cp->endpoint = 0;
        cp->disable_apple_captive_prompt= true;
        cp->cp_url =  "http://google.com";
        cp->initopts();

        Packet *packet = PacketBuilder::createTcp("10.1.1.1", "192.168.1.1", 12334, 80);
        TcpConnection *connection = new TcpConnection(packet);
        connection->getState()->state = TCPUP;
        connection->getApplicationInfo()->type = SFwallCore::ConnectionApplicationInfo::Classifier::HTTP;
        connection->getApplicationInfo()->http_useragentSet = true;
        connection->getApplicationInfo()->http_useragent= "CaptiveNetworkSupport-277.20.5 wispr";

        cp->process(connection, packet);
        EXPECT_TRUE(connection->getRewriteRule() == NULL);
}

TEST(CapturePortalEngine, ignoreIfDestIpInRage) {
	MockFactory *tester = new MockFactory();
	ConfigurationManager *config = new ConfigurationManager();

        SFwallCore::CapturePortalEngine *cp = new SFwallCore::CapturePortalEngine(new SFwallCore::AliasDb());
	cp->setConfigurationManager(config);
        cp->mode = 1;
        cp->endpoint = 0;
	cp->cp_url = "http://google.com";

	Alias *alias = new IpSubnetAlias();
        alias->addEntry("10.1.1.1/255.255.255.255");
	cp->provided_inclusions_aliases.push_back(shared_ptr<Alias>(alias));

	Alias *ignoreAlias = new IpSubnetAlias();
        ignoreAlias->addEntry("192.168.1.1/255.255.255.255");
	cp->provided_exclusions_aliases.push_back(shared_ptr<Alias>(ignoreAlias));
	cp->initopts();

	Packet *packet = PacketBuilder::createTcp("10.1.1.1", "192.168.1.1", 12334, 80);
	TcpConnection *connection = new TcpConnection(packet);
        connection->getState()->state = TCPUP;
        connection->getApplicationInfo()->type = SFwallCore::ConnectionApplicationInfo::Classifier::HTTP;

        HostPtr host = HostPtr(new Host());
        host->authenticated_user = UserPtr(new User());
        connection->setHostDiscoveryServiceEntry(host);

        cp->process(connection, packet);
        EXPECT_TRUE(connection->getRewriteRule() == NULL);
}

TEST(CapturePortalEngine, ignoreIfWebsiteInAuthExceptionList) {
	MockFactory *tester = new MockFactory();
	ConfigurationManager *config = new ConfigurationManager();
        SFwallCore::CapturePortalEngine *cp = new SFwallCore::CapturePortalEngine(new SFwallCore::AliasDb());
        cp->setConfigurationManager(config);
        cp->mode = 1;
        cp->endpoint = 0;
        cp->cp_url = "http://google.com";

	Alias *alias = new IpSubnetAlias();
        alias->addEntry("10.1.1.1/255.255.255.255");
        cp->provided_inclusions_aliases.push_back(shared_ptr<Alias>(alias));

	Alias *ignoreAlias = new WebsiteListAlias();
        ignoreAlias->addEntry("google.com");
        ignoreAlias->addEntry("yahoo.com");
        cp->provided_exclusions_aliases.push_back(shared_ptr<Alias>(ignoreAlias));
	cp->initopts();

	Packet *packet = PacketBuilder::createTcp("10.1.1.1", "192.168.1.1", 12334, 80);
	TcpConnection *connection = new TcpConnection(packet);
	connection->getApplicationInfo()->setHttpHost("www.google.com");
        connection->getState()->state = TCPUP;
        connection->getApplicationInfo()->type = SFwallCore::ConnectionApplicationInfo::Classifier::HTTP;

        HostPtr host = HostPtr(new Host());
        host->authenticated_user = UserPtr(new User());
        connection->setHostDiscoveryServiceEntry(host);

        cp->process(connection, packet);
        EXPECT_TRUE(connection->getRewriteRule() == NULL);
}


TEST(CapturePortalEngine, dont_disable_captive_portal_if_magic_url_not_visited) {
        MockFactory *tester = new MockFactory();
        ConfigurationManager *config = new ConfigurationManager();

        SFwallCore::CapturePortalEngine *cp = new SFwallCore::CapturePortalEngine(new SFwallCore::AliasDb());
        cp->setConfigurationManager(config);
        cp->mode = 1;
        cp->endpoint = 0;
        cp->cp_url =  "http://google.com";
        cp->CAPTURE_PORTAL_MAGIC_LINK_ENABLED = 1;
        cp->CAPTURE_PORTAL_MAGIC_LINK_TIMEOUT = 600;
        cp->CAPTURE_PORTAL_MAGIC_LINK_CLICKED_TIME = 0;

        Alias *alias = new IpSubnetAlias();
        alias->addEntry("10.1.1.1/255.255.255.255");
        cp->provided_inclusions_aliases.push_back(std::shared_ptr<Alias>(alias));
        cp->initopts();

        Packet *packet = PacketBuilder::createTcp("10.1.1.1", "192.168.1.1", 12334, 80);
        TcpConnection *connection = new TcpConnection(packet);
        connection->getState()->state = TCPUP;
        connection->getApplicationInfo()->type = SFwallCore::ConnectionApplicationInfo::Classifier::HTTP;

        cp->process(connection, packet);

        EXPECT_TRUE(connection->getRewriteRule() != NULL);
}

TEST(CapturePortalEngine, disable_captive_portal_if_magic_url_visited) {
        MockFactory *tester = new MockFactory();
        ConfigurationManager *config = new ConfigurationManager();

        SFwallCore::CapturePortalEngine *cp = new SFwallCore::CapturePortalEngine(new SFwallCore::AliasDb());
        cp->setConfigurationManager(config);
        cp->mode = 1;
        cp->endpoint = 1;
        cp->CAPTURE_PORTAL_MAGIC_LINK_ENABLED = 1;
        cp->CAPTURE_PORTAL_MAGIC_LINK_TIMEOUT = 600;
        Alias *alias = new IpSubnetAlias();
        alias->addEntry("10.1.1.1/255.255.255.255");
        cp->provided_inclusions_aliases.push_back(std::shared_ptr<Alias>(alias));
        cp->initopts();

        // click the magic url
        Packet *packet0 = PacketBuilder::createTcp("10.1.1.1", "192.168.1.1", 12334, 80);
        TcpConnection *connection0 = new TcpConnection(packet0);
        connection0->getApplicationInfo()->setHttpHost("disablecp.linewize.net");
        connection0->getState()->state = TCPUP;
        connection0->getApplicationInfo()->type = SFwallCore::ConnectionApplicationInfo::Classifier::HTTP;

        cp->process(connection0, packet0);

        System::getInstance()->getTimeManager().useThisTime(time(NULL) + 60);


        Packet *packet = PacketBuilder::createTcp("10.1.1.1", "192.168.1.1", 12334, 80);
        TcpConnection *connection = new TcpConnection(packet);
        connection->getState()->state = TCPUP;
        connection->getApplicationInfo()->type = SFwallCore::ConnectionApplicationInfo::Classifier::HTTP;

        cp->process(connection, packet);

        EXPECT_TRUE(connection->getRewriteRule() == NULL);
}

TEST(CapturePortalEngine, enable_captive_portal_if_magic_url_visited_but_timeout_elapsed) {
        MockFactory *tester = new MockFactory();
        ConfigurationManager *config = new ConfigurationManager();
        SFwallCore::CapturePortalEngine *cp = new SFwallCore::CapturePortalEngine(new SFwallCore::AliasDb());
        cp->setConfigurationManager(config);
        cp->mode = 1;
        cp->endpoint = 1;
        cp->CAPTURE_PORTAL_MAGIC_LINK_ENABLED = 1;
        cp->CAPTURE_PORTAL_MAGIC_LINK_TIMEOUT = 600;

        Alias *alias = new IpSubnetAlias();
        alias->addEntry("10.1.1.1/255.255.255.255");
        cp->provided_inclusions_aliases.push_back(std::shared_ptr<Alias>(alias));
        cp->initopts();

        Packet *packet0 = PacketBuilder::createTcp("10.1.1.1", "192.168.1.1", 12334, 80);
        TcpConnection *connection0 = new TcpConnection(packet0);
        connection0->getApplicationInfo()->setHttpHost("disablecp.linewize.net");
        connection0->getState()->state = TCPUP;
        connection0->getApplicationInfo()->type = SFwallCore::ConnectionApplicationInfo::Classifier::HTTP;

        cp->process(connection0, packet0);

        System::getInstance()->getTimeManager().useThisTime(time(NULL) + 60*11);

        Packet *packet = PacketBuilder::createTcp("10.1.1.1", "192.168.1.1", 12334, 80);
        TcpConnection *connection = new TcpConnection(packet);
        connection->getState()->state = TCPUP;
        connection->getApplicationInfo()->type = SFwallCore::ConnectionApplicationInfo::Classifier::HTTP;

        cp->process(connection, packet);

        EXPECT_TRUE(connection->getRewriteRule() != NULL);
}