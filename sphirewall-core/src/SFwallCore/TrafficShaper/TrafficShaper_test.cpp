#include <gtest/gtest.h>

#include "SFwallCore/Criteria.h"
#include "SFwallCore/TrafficShaper/TrafficShaper.h"
#include "SFwallCore/TestFactory.h"

using namespace std;
using namespace SFwallCore;

TEST(TrafficShaper, match){
        TrafficShaper* shaper = new TrafficShaper();

        TsRule* rule = new TsRule("id1");
        IpCriteria* sc = new IpCriteria(); sc->ip = IP4Addr::stringToIP4Addr("10.1.1.1"); sc->mask = IP4Addr::stringToIP4Addr("255.255.255.0");
        rule->filter->source_criteria.push_back(CriteriaPtr(sc));

        shaper->add(TsRulePtr(rule));

        Packet *packet = PacketBuilder::createIcmp("10.1.1.1", "10.1.1.2", 10);

        TokenBucketPtr bucket = shaper->match(packet);
        EXPECT_TRUE(bucket.get() != NULL);
        EXPECT_TRUE(bucket->filter->source_criteria.size() == 1);
        EXPECT_TRUE(shaper->listBuckets().size() == 1);
}

TEST(TrafficShaper, mismatch_no_rules){
        TrafficShaper* shaper = new TrafficShaper();

        Packet *packet = PacketBuilder::createIcmp("10.1.1.1", "10.1.1.2", 10);

        TokenBucketPtr bucket = shaper->match(packet);
        EXPECT_TRUE(bucket.get() == NULL);
        EXPECT_TRUE(shaper->listBuckets().size() == 0);
}

TEST(TrafficShaper, mismatch){
        TrafficShaper* shaper = new TrafficShaper();

        TsRule* rule = new TsRule("id2");
        IpCriteria* sc = new IpCriteria(); sc->ip = IP4Addr::stringToIP4Addr("10.1.1.1"); sc->mask = IP4Addr::stringToIP4Addr("255.255.255.0");
        rule->filter->source_criteria.push_back(CriteriaPtr(sc));

        shaper->add(TsRulePtr(rule));

        Packet *packet = PacketBuilder::createIcmp("8.8.8.8", "10.1.1.2", 10);

        TokenBucketPtr bucket = shaper->match(packet);
        EXPECT_TRUE(bucket.get() == NULL);
        EXPECT_TRUE(shaper->listBuckets().size() == 0);
}


