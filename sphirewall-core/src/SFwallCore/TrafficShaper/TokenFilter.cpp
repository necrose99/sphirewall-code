/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>

#include "SFwallCore/Packet.h"
#include "SFwallCore/Criteria.h"
#include "SFwallCore/TrafficShaper/TokenFilter.h"

using namespace std;

TokenFilter::TokenFilter() {
}

TokenFilter::TokenFilter(TokenFilter &orig){}

bool TokenFilter::check(SFwallCore::Packet *packet) {
	for(SFwallCore::CriteriaPtr c: source_criteria){
		if(!c->match(packet, DIRECTION_SOURCE)){
			return false;
		}	
	}

        for(SFwallCore::CriteriaPtr c: destination_criteria){
                if(!c->match(packet, DIRECTION_DESTINATION)){
                        return false;
                }
        }

	return true;
}

