/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef TOKENFILTER_H_INCLUDED
#define TOKENFILTER_H_INCLUDED

#include "SFwallCore/Packetfwd.h"
#include "SFwallCore/Criteria.h"
#include "Auth/User.h"

class TokenFilter {
	public:
		TokenFilter();
		TokenFilter(TokenFilter &orig);
		bool check(SFwallCore::Packet *packet);
		
		std::list<SFwallCore::CriteriaPtr> source_criteria;
		std::list<SFwallCore::CriteriaPtr> destination_criteria;
};

typedef boost::shared_ptr<TokenFilter> TokenFilterPtr;

#endif
