/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include "SFwallCore/ConnTracker.h"
#include "SFwallCore/TrafficShaper/TrafficShaper.h"
#include "Core/System.h"
#include "SFwallCore/Packet.h"
#include "SFwallCore/Criteria.h"
#include "Core/Logger.h"

using namespace std;

void TrafficShaper::addBucket(TokenBucketPtr b) {
	bucket.push_back(b);
}

TrafficShaper::TrafficShaper(GroupDb *groupDb, SFwallCore::PlainConnTracker *connections) 
	: groupDb(groupDb), connections(connections) {
}

bool TrafficShaper::load(){
        Logger::instance()->log("sphirewalld.firewall.ts", EVENT, "Loading Rules from file: " + ruleFile);

        ObjectContainer *root;
        rules.clear();
        if (configurationManager->has("qos")) {

                root = configurationManager->getElement("qos");
                ObjectContainer *arr = root->get("rules")->container();

                for (int x = 0; x < arr->size(); x++) {
                        ObjectContainer *source = arr->get(x)->container();

                        TsRulePtr target = TsRulePtr(new TsRule(source->get("id")->string()));
                        target->uploadRate = source->get("upload")->number();
                        target->downloadRate = source->get("download")->number();

                        target->name = source->get("name")->string();
                        target->cumulative = source->get("cumulative")->boolean();

                        if(source->has("source_criteria")){
                                ObjectContainer* criteria = source->get("source_criteria")->container();
                                for(int y = 0; y < criteria->size(); y++){
                                        ObjectContainer* individualCriteria = criteria->get(y)->container();
                                        target->filter->source_criteria.push_back(SFwallCore::CriteriaPtr(SFwallCore::CriteriaBuilder::parse(criteria->get(y)->container())));
                                }
                        }

                        if(source->has("destination_criteria")){
                                ObjectContainer* criteria = source->get("destination_criteria")->container();
                                for(int y = 0; y < criteria->size(); y++){
                                        ObjectContainer* individualCriteria = criteria->get(y)->container();
                                        target->filter->destination_criteria.push_back(SFwallCore::CriteriaPtr(SFwallCore::CriteriaBuilder::parse(criteria->get(y)->container())));
                                }
                        }


                        //Blocks
                        rules.push_back(target);
                }
        }
	return true;	
}

void TrafficShaper::save(){
        if (configurationManager == NULL) {
                return;
        }

        ObjectContainer *root = new ObjectContainer(CREL);
        ObjectContainer *arr = new ObjectContainer(CARRAY);

        for (TsRulePtr b: rules) {
                ObjectContainer *o = new ObjectContainer(CREL);
                o->put("upload", new ObjectWrapper((double) b->uploadRate));
                o->put("download", new ObjectWrapper((double) b->downloadRate));
                o->put("id", new ObjectWrapper((string) b->id));
                o->put("cumulative", new ObjectWrapper((bool) b->cumulative));
                o->put("name", new ObjectWrapper((string) b->name));

                ObjectContainer* target_source_criteria = new ObjectContainer(CARRAY);
                for(SFwallCore::CriteriaPtr c: b->filter->source_criteria){
			target_source_criteria->put(new ObjectWrapper(SFwallCore::CriteriaBuilder::serialize(c.get())));
                }
                o->put("source_criteria", new ObjectWrapper(target_source_criteria));

                ObjectContainer* target_destination_criteria = new ObjectContainer(CARRAY);
                for(SFwallCore::CriteriaPtr c: b->filter->destination_criteria){
                        target_destination_criteria->put(new ObjectWrapper(SFwallCore::CriteriaBuilder::serialize(c.get())));
                }
                o->put("destination_criteria", new ObjectWrapper(target_destination_criteria));

                arr->put(new ObjectWrapper(o));
        }

        root->put("rules", new ObjectWrapper(arr));

	configurationManager->holdLock();
        configurationManager->setElement("qos", root);
        configurationManager->save();
	configurationManager->releaseLock();
}


void TrafficShaper::start() {
	new boost::thread(boost::bind(&TrafficShaper::loop, this));
}

TrafficShaper::~TrafficShaper() {
}

void TrafficShaper::loop() {
	Logger::instance()->log("sphirewalld.firewall.ts", INFO, "Starting rate limiter background task");
	while (System::getInstance()->ALIVE) {
		if (bucket.size() > 0) {
			for (std::list<TokenBucketPtr>::iterator iter = bucket.begin(); iter != bucket.end(); iter++) {
				TokenBucketPtr tb = (*iter);
				tb->rollBucket();

				if (tb->hasExpired()) {
					iter = bucket.erase(iter);	
				}
			}

			usleep(100);
		}
		else {   // No shaping specified.  Wait and see if situation changes.
			sleep(1);
		}
	}
}

TokenBucketPtr TrafficShaper::match(SFwallCore::Packet *packet) {
        for (auto tb : bucket) {
                if (tb->filter->check(packet)) {
                        return tb;
                }
        }

        for (TsRulePtr rule : rules) {
                if (rule->filter->check(packet)) {
                        TokenFilterPtr filter = rule->filter;

                        TokenBucketPtr bucket;
                        /*What kind of rule are we dealing with? Is this a cumulative rule?*/
                        if(!rule->cumulative){
                                //Create a new Filter Object
                                TokenFilterPtr newFilter(new TokenFilter());

                                //Translate filters that are required
                                for(SFwallCore::CriteriaPtr criteria : filter->source_criteria){
                                        if(criteria->key().compare("group") == 0){
                                                SFwallCore::UsersCriteria* uc = new SFwallCore::UsersCriteria();
                                                uc->users.insert(packet->getUser());
                                                newFilter->source_criteria.push_back(SFwallCore::CriteriaPtr(uc));
                                        }else{
                                                newFilter->source_criteria.push_back(criteria);
                                        }
                                }

				for(SFwallCore::CriteriaPtr criteria : filter->destination_criteria){
					newFilter->destination_criteria.push_back(criteria);
				}

				bucket = TokenBucketPtr(new TokenBucket(rule->uploadRate, rule->downloadRate, newFilter));

			}else{
				bucket = TokenBucketPtr(new TokenBucket(rule->uploadRate, rule->downloadRate, filter));
			}

			addBucket(bucket);

			rule->incrementHits();
			return bucket;
		}
	}

	return TokenBucketPtr();
}

void TrafficShaper::add(TsRulePtr rule) {
	Logger::instance()->log("sphirewalld.firewall.ts", EVENT, "Added a new TSRule");
	rules.push_back(rule);
}

TsRulePtr TrafficShaper::get(std::string id) {
	for(TsRulePtr rule : rules){
		if(rule->id.compare(id) == 0){
			return rule;
		}
	}	

	return TsRulePtr();
}

void TrafficShaper::del(TsRulePtr rule) {
	Logger::instance()->log("sphirewalld.firewall.ts", EVENT, "Deleting a TSRule");
	rules.remove(rule);
}

list<TsRulePtr> TrafficShaper::list() {
	return rules;
}

void TrafficShaper::invalidate(){
	holdLock();
	for(TokenBucketPtr tb : bucket){
		tb->invalidate();
	}
	bucket.clear();
	releaseLock();
}

void TrafficShaper::delBucket(TokenBucketPtr tkb) {
	if (tkb) {
		holdLock();
		for (std::list<TokenBucketPtr>::iterator iter = bucket.begin();
				iter != bucket.end();
				++iter) {
			if (*iter == tkb) {
				bucket.erase(iter);
				break;
			}
		}
		releaseLock();
	}
}

