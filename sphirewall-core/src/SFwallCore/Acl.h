/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef ACL_H
#define ACL_H

#include <vector>
#include "Packetfwd.h"
#include "SFwallCore/Alias.h"
#include "Core/Logger.h"
#include "Auth/UserDb.h"
#include "Auth/GroupDb.h"
#include "SFwallCore/TimePeriods.h"
#include "Utils/Interfaces.h"
#include "Utils/Utils.h"

class ConfigurationManager;
class ObjectContainer;
class User;
namespace SFwallCore {
	class ACLStore;
	class FilteringRuleProvider;

	class Criteria;
	typedef boost::shared_ptr<Criteria> CriteriaPtr;
	enum NAT_TYPE {
		NO_NAT = -1, DNAT = 0, SNAT = 1
	};

	class AliasDb;

	class Nat {
		public:
			Nat();
			int target;
			NAT_TYPE type;
	};

	class FilterRule : public Lockable {
		public:
			FilterRule();
			~FilterRule(){
				delete hits_per_minute;
			}

			/* To modify variables, you must aquire a lock on this object */
			std::string id;
                        bool enabled;
                        bool valid;
                        double count;
                        double last_hit;
                        bool ignoreconntrack;
                        bool ignore_application_layer_filters;

			std::list<CriteriaPtr> source_criteria;
			std::list<CriteriaPtr> destination_criteria;
                        bool isActive();
			bool log;
			int action;
			std::string comment;
			int group;
			FlexibleSecondIntervalSampler* hits_per_minute;

			/* These methods are threadsafe and will aquire and release a lock on this object */
                        bool supermatch(SFwallCore::Packet *packet, GroupDb *groupDb);
			void refresh();
                private:
                        bool valueInRange(int lower, int value, int higher);
	};

	typedef boost::shared_ptr<FilterRule> FilterRulePtr;
	class ACLStore : public Lockable, public Configurable {
		public:
			class InterfaceChangeListener : public virtual IntMgrChangeListener {
				public:
					InterfaceChangeListener(ACLStore *store);
					void interface_change(InterfacePtr);
				private:
					ACLStore *store;
			};

			ACLStore(UserDb *userdb, GroupDb *groupdb, AliasDb *aliases, TimePeriodStore *tps);
			ACLStore();
			int loadAclEntry(FilterRulePtr rule, bool add=false);
			int unloadAclEntry(FilterRulePtr rule);

			FilterRulePtr getRuleById(std::string id);
			void movedown(FilterRulePtr rule);
			void moveup(FilterRulePtr rule);

			FilterRulePtr match_rule(Packet *);

			void initLazyObjects(FilterRulePtr rule);

			void save();
			bool load();
			void setInterfaceManager(IntMgr* im);

			vector<FilterRulePtr> listFilterRules();
			const char* getConfigurationSystemName(){
				return "Filtering rule store";
			}

			map<int, std::string> group_labels;
			void sample(map<string, double> &input);
		private:
			int findPos(FilterRulePtr rule);
			UserDb *userDb;
			GroupDb *groupDb;
			TimePeriodStore *tps;
			AliasDb *aliases;

			IntMgr* interfaceManager;

			vector<FilterRulePtr> entries;
			void serializeRule(ObjectContainer *rule, FilterRulePtr target);
			void deserializeRule(ObjectContainer *source, FilterRulePtr entry);
	};

}

#endif
