/*
   Copyright Michael Lawson
   This file is part of Sphirewall.

   Sphirewall is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Sphirewall is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <vector>
#include <time.h>
#include <sys/types.h>
#include <sstream>
#include <map>
#include <fstream>
#include <string>

using namespace std;

#include "Utils/IP4Addr.h"
#include "Utils/IP6Addr.h"
#include "SFwallCore/Firewall.h"
#include "SFwallCore/Priority.h"
#include "SFwallCore/Alias.h"
#include "Utils/Utils.h"
#include "Utils/StringUtils.h"
#include "Json//JSON.h"
#include "Json/JSONValue.h"
#include "Core/ConfigurationManager.h"
#include "Utils/TimeWrapper.h"
#include "Core/System.h"
#include "SFwallCore/TimePeriods.h"
#include "Packet.h"
#include "Auth/User.h"
#include "SFwallCore/Criteria.h"

bool SFwallCore::PriorityAclStore::load() {
	priorityQosRules.clear();
	ObjectContainer *root;

	if (configurationManager->has("priorityAcls")) {
		root = configurationManager->getElement("priorityAcls");
		ObjectContainer *rules = root->get("rules")->container();

		for (int x = 0; x < rules->size(); x++) {
			PriorityRulePtr entry = PriorityRulePtr(new PriorityRule());

			ObjectContainer *source = rules->get(x)->container();
			deserializeRule(source, entry);

			entry->nice = source->get("nice")->number();
			loadAclEntry(entry, true);
		}
	}
	else {
		Logger::instance()->log("sphirewalld.firewall.acls", INFO, "Could not find any nat rules in configuration");
	}
	return true;
}

void SFwallCore::PriorityAclStore::deserializeRule(ObjectContainer *source, SFwallCore::PriorityRulePtr entry) {
	if (source->has("id")) {
		entry->id = source->get("id")->string();
	}

	if (source->has("enabled")) {
		entry->enabled = source->get("enabled")->boolean();
	}

	if(source->has("source_criteria")){
		ObjectContainer* criteria = source->get("source_criteria")->container();
		for(int y = 0; y < criteria->size(); y++){
			CriteriaPtr c = CriteriaPtr(CriteriaBuilder::parse(criteria->get(y)->container()));
			if(c){
				entry->source_criteria.push_back(c);
			}
		}
	}

	if(source->has("destination_criteria")){
		ObjectContainer* criteria = source->get("destination_criteria")->container();
		for(int y = 0; y < criteria->size(); y++){
			CriteriaPtr c = CriteriaPtr(CriteriaBuilder::parse(criteria->get(y)->container()));
			if(c){
				entry->destination_criteria.push_back(c);
			}
		}
	}
}


void SFwallCore::PriorityAclStore::serializeRule(ObjectContainer *rule, SFwallCore::PriorityRulePtr target) {
	rule->put("id", new ObjectWrapper((string) target->id));
	ObjectContainer* target_source_criteria = new ObjectContainer(CARRAY);
	for(auto c: target->source_criteria){
		target_source_criteria->put(new ObjectWrapper(CriteriaBuilder::serialize(c.get())));
	}
	rule->put("source_criteria", new ObjectWrapper(target_source_criteria));

	ObjectContainer* target_destination_criteria = new ObjectContainer(CARRAY);
	for(auto c: target->destination_criteria){
		target_destination_criteria->put(new ObjectWrapper(CriteriaBuilder::serialize(c.get())));
	}
	rule->put("destination_criteria", new ObjectWrapper(target_destination_criteria));
}

void SFwallCore::PriorityAclStore::save() {
	int loaded = 0;

	ObjectContainer *root = new ObjectContainer(CREL);
	ObjectContainer *rules = new ObjectContainer(CARRAY);

	for (int x = 0; x < (int) priorityQosRules.size(); x++) {
		ObjectContainer *rule = new ObjectContainer(CREL);
		serializeRule(rule, priorityQosRules[x]);
		rule->put("id", new ObjectWrapper((string) priorityQosRules[x]->id));
		rule->put("enabled", new ObjectWrapper((bool) priorityQosRules[x]->enabled));
		rule->put("nice", new ObjectWrapper((double) priorityQosRules[x]->nice));
		loaded++;

		rules->put(new ObjectWrapper(rule));
	}

	root->put("rules", new ObjectWrapper(rules));

	configurationManager->holdLock();
	configurationManager->setElement("priorityAcls", root);
	configurationManager->save();
	configurationManager->releaseLock();

}

void SFwallCore::PriorityAclStore::initLazyObjects(PriorityRulePtr rule) {
}

void SFwallCore::PriorityAclStore::movedown(PriorityRulePtr rule) {
	holdLock();
	int x = findPos(rule);

	if ((x + 1) < (int) priorityQosRules.size()) {
		PriorityRulePtr a = priorityQosRules[x];
		PriorityRulePtr b = priorityQosRules[x + 1];

		priorityQosRules[x + 1] = a;
		priorityQosRules[x]     = b;
	}

	releaseLock();
	save();
}

void SFwallCore::PriorityAclStore::moveup(PriorityRulePtr rule) {
	holdLock();
	int x = findPos(rule);
	if ((x - 1) >= 0) {
		PriorityRulePtr a = priorityQosRules[x];
		PriorityRulePtr b = priorityQosRules[x - 1];

		priorityQosRules[x - 1] = a;
		priorityQosRules[x]     = b;
	}
        releaseLock();
	save();
}


int SFwallCore::PriorityAclStore::findPos(PriorityRulePtr rule) {
	for (uint x = 0; x < priorityQosRules.size(); x++) {
		PriorityRulePtr target = priorityQosRules[x];

		if (target == rule) {
			return x;
		}
	}

	return -1;
}

SFwallCore::PriorityRulePtr SFwallCore::PriorityAclStore::getRuleById(std::string id) {
	holdLock();
	for (auto rule : priorityQosRules) {
		if (rule->id.compare(id) == 0) {
			releaseLock();
			return rule;
		}
	}
	releaseLock();
	return SFwallCore::PriorityRulePtr();
}

void SFwallCore::PriorityAclStore::InterfaceChangeListener::interface_change() {
}

SFwallCore::PriorityAclStore::InterfaceChangeListener::InterfaceChangeListener(SFwallCore::PriorityAclStore* store) : store(store)
{}

int SFwallCore::PriorityAclStore::loadAclEntry(PriorityRulePtr rule, bool add){
	initLazyObjects(rule);

	holdLock();
	if(rule->id.size() == 0 || add){
		rule->id = StringUtils::genRandom();
		priorityQosRules.push_back(rule);
	}
	releaseLock();
	return 0;
}

int SFwallCore::PriorityAclStore::unloadAclEntry(PriorityRulePtr rule){
	holdLock();
	int pos = findPos(rule);
	if(pos != -1){
		priorityQosRules.erase(priorityQosRules.begin() + pos);
	}
	releaseLock();
	return pos;
}

SFwallCore::PriorityRulePtr SFwallCore::PriorityAclStore::match_rule(Packet *packet) {
	if (packet == NULL) {
		return SFwallCore::PriorityRulePtr();
	}

	holdLock();
	for (auto rule : priorityQosRules) {
		if (rule->supermatch(packet, groupDb)) {
			releaseLock();
			return rule;
		}
	}

	releaseLock();
	return SFwallCore::PriorityRulePtr();
}

std::vector<SFwallCore::PriorityRulePtr> SFwallCore::PriorityAclStore::listPriorityRules() {
	vector<SFwallCore::PriorityRulePtr> ret;
	holdLock();
	ret = priorityQosRules;
	releaseLock();
	return ret;
}

SFwallCore::PriorityRule::PriorityRule(): nice(1)
{}

SFwallCore::PriorityAclStore::PriorityAclStore(UserDb *userdb, GroupDb *groupdb, SFwallCore::AliasDb *aliases, TimePeriodStore *tps)
	: userDb(userdb),
	groupDb(groupdb),
	aliases(aliases),
	tps(tps) {
	}

SFwallCore::PriorityAclStore::PriorityAclStore(){}

void SFwallCore::PriorityAclStore::setInterfaceManager(IntMgr* interfaceManager) {
	this->interfaceManager = interfaceManager;
}

bool SFwallCore::PriorityRule::supermatch(Packet *packet, GroupDb *groupDb) {
	holdLock();
	if(!enabled){
		releaseLock();
		return false;
	}

	for(auto c : source_criteria){
		if(!c->match(packet, DIRECTION_SOURCE)){
			releaseLock();
			return false;
		}
	}

	for(auto c : destination_criteria){
		if(!c->match(packet, DIRECTION_DESTINATION)){
			releaseLock();
			return false;
		}
	}

	releaseLock();
	return true;
}

void SFwallCore::PriorityAclStore::aliasRemoved(AliasPtr alias) {
}

