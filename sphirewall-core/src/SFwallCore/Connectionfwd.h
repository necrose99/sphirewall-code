/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SPHIREWALL_CONNFWD_H
#define SPHIREWALL_CONNFWD_H

#include <string>
#include <boost/shared_ptr.hpp>

#define CAI_UNKNOWN 0
#define CAI_HTTP 1
#define REQUEST 0
#define RESPONSE 1

#define VALID_HOSTNAME_THREHOLD 3

const static int HTTP_HOSTNAME = 1; 
const static int HTTP_USERAGENT = 2;
const static int HTTP_CONTENTTYPE = 3;
const static int HTTP_REQUEST = 4;

class TokenBucket;
typedef boost::shared_ptr<TokenBucket> TokenBucketPtr;
namespace SFwallCore {
	class ConnectionIp;
	class ApplicationLayerTrackerHandler;
	class ConnectionApplicationInfo;
	class ConnectionIpV4;
	class ConnectionIpV6;
	class Connection;
	class TcpConnection;
	class UdpConnection;
	class IcmpConnection;
	class TcpTrackable;
	class TcpState;
	class RewriteRule;
};

#endif
