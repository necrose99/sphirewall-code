/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <sstream>
#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <netinet/in.h>
#include <linux/types.h>
#include <stdarg.h>
#include <limits.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <signal.h>
#include <time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <ctype.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <netinet/tcp.h>
#include <netinet/udp.h>
#include <arpa/inet.h>
#include <linux/icmp.h>
#include <boost/regex.hpp>
#include <boost/concept_check.hpp>

using namespace std;

#include "Utils/IP4Addr.h"
#include "SFwallCore/ConnTracker.h"
#include "SFwallCore/Packet.h"
#include "Auth/Session.h"
#include "Core/System.h"
#include "SFwallCore/Firewall.h"
#include "SFwallCore/ApplicationLevel/ApplicationFilter.h"
#include "SFwallCore/ApplicationLevel/Handlers.h"
#include "Utils/IP6Addr.h"

#include <net/ethernet.h>
#include <netinet/ip.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>

#define SERVER_NAME_LEN 256
#define TLS_HEADER_LEN 5
#define TLS_HANDSHAKE_CONTENT_TYPE 0x16
#define TLS_HANDSHAKE_TYPE_CLIENT_HELLO 0x01


SFwallCore::Packet::Packet()
	: safetycheck(100), transport(NULL), connection(NULL), sqp(NULL),
	  hasHw(false)
{
	this->timestamp = (unsigned int) time(0);
}

void SFwallCore::Packet::setHw(std::string hw) {
	this->hasHw = true;
	sscanf(hw.c_str(), "%02x:%02x:%02x:%02x:%02x:%02x", &hwBuf[0],  &hwBuf[1],  &hwBuf[2],  &hwBuf[3],  &hwBuf[4],  &hwBuf[5]);	
}

bool SFwallCore::Packet::hasHwAddr() {
	return hasHw;
}

SFwallCore::Connection *SFwallCore::Packet::getConnection() const {
	return connection;
}

sq_packet *SFwallCore::Packet::getInternalSqp() {
	return sqp;
}

SFwallCore::TransportFrame *SFwallCore::Packet::getTransport() {
	return transport;
}

void SFwallCore::Packet::setConnection(SFwallCore::Connection *connection) {
	this->connection = connection;
}

void SFwallCore::Packet::setInternalSqp(sq_packet *sqp) {
	this->sqp = sqp;
}

void SFwallCore::Packet::setUser(UserPtr user) {
	userPtr = user;
}

void SFwallCore::Packet::incrementRuleMetrics(int layer_type, int accept_or_deny) {
	if (this->getUser()) {
		if (accept_or_deny == SQ_ACCEPT) {
			if (layer_type == LAYER_TYPE_234) {
				this->getUser()->rule_metrics->layer234Accepts->input(1);
			} else if (layer_type == LAYER_TYPE_7) {
				this->getUser()->rule_metrics->layer7Accepts->input(1);
			}
		} else {
			if (layer_type == LAYER_TYPE_234) {
				this->getUser()->rule_metrics->layer234Denies->input(1);
			} else if (layer_type == LAYER_TYPE_7) {
				this->getUser()->rule_metrics->layer7Denies->input(1);
			}
		}
	}
	if (this->getHostDiscoveryServiceEntry()) {
		if (accept_or_deny == SQ_ACCEPT) {
			if (layer_type == LAYER_TYPE_234) {
				this->getHostDiscoveryServiceEntry()->rule_metrics->layer234Accepts->input(1);
			} else if (layer_type == LAYER_TYPE_7) {
				this->getHostDiscoveryServiceEntry()->rule_metrics->layer7Accepts->input(1);
			}
		} else {
			if (layer_type == LAYER_TYPE_234) {
				this->getHostDiscoveryServiceEntry()->rule_metrics->layer234Denies->input(1);
			} else if (layer_type == LAYER_TYPE_7) {
				this->getHostDiscoveryServiceEntry()->rule_metrics->layer7Denies->input(1);
			}
		}
	}
}

	SFwallCore::ApplicationFrame::ApplicationFrame(unsigned char *packet, int len)
: packet(packet), len(len)
{}

SFwallCore::ApplicationFrame::~ApplicationFrame()
{}

int SFwallCore::ApplicationFrame::getSize() {
	return len;
}

bool SFwallCore::ApplicationFrame::write(std::string input) {
	return write((unsigned char *) input.c_str(), input.size());
}

bool SFwallCore::ApplicationFrame::write(unsigned char *data, int len) {
	if (len <= 0 || len >= 1500 - sizeof(struct iphdr) - sizeof(struct tcphdr)) {
		return false;
	}

	if(getSize() > len){
		bzero((unsigned char*) packet + len, getSize() - len);
	}
	memcpy((unsigned char *) packet, data, len);
	return true;
}

unsigned char *SFwallCore::ApplicationFrame::getInternal() {
	return packet;
}


void SFwallCore::PacketV6::resizePayload(int newlen) {
	//TODO:
}

void SFwallCore::PacketV4::resizePayload(int newlen) {
	//Perform resize - This is recursive
	TransportFrame *frame = getTransport();
	frame->resizePayload(newlen);

	int size = frame->getSize() + frame->getApplication()->getSize();

	struct iphdr *packet_header = (struct iphdr *) sqp->raw_packet;
	int tot_len_value = htons(sizeof(struct iphdr) + size);
	packet_header->tot_len = tot_len_value;
}

void SFwallCore::TcpPacket::resizePayload(int newlen) {
	getApplication()->resizePayload(newlen);
}

void SFwallCore::UdpPacket::resizePayload(int newlen) {
	getApplication()->resizePayload(newlen);

	struct udphdr *packet_header = (struct udphdr *) packet;
	packet_header->len = htons(getSize() + getApplication()->getSize());
}

int SFwallCore::PacketV6::hash() {
	int cursor = 1;
	cursor += IP6Addr::hash(getSrcIp());
	cursor += IP6Addr::hash(getDstIp());

	if (transport) {
		return cursor + transport->hash();
	}

	return cursor;
}


std::string SFwallCore::Packet::toString() {
	return "unknown packet";
}

in_addr_t SFwallCore::PacketV4::getSrcIp() const {
	struct ip *ip = (struct ip *) sqp->raw_packet;
	return ntohl(ip->ip_src.s_addr);
}

in_addr_t SFwallCore::PacketV4::getDstIp() const {
	struct ip *ip = (struct ip *) sqp->raw_packet;
	return ntohl(ip->ip_dst.s_addr);
}

int SFwallCore::PacketV4::getLen() {
	struct iphdr *packet_header = (struct iphdr *) sqp->raw_packet;
	return ntohs(packet_header->tot_len);
}

int SFwallCore::PacketV4::getHeaderLen() {
	struct iphdr *packet_header = (struct iphdr *) sqp->raw_packet;
	return packet_header->ihl * 4;
}

int SFwallCore::PacketV4::getPayloadLen() {
	return getLen() - getHeaderLen();
}

int SFwallCore::PacketV4::getCheckSum() {
	struct iphdr *packet_header = (struct iphdr *) sqp->raw_packet;
	return packet_header->check;
}

InterfacePtr SFwallCore::Packet::getSourceNetDevice() {
	return System::getInstance()->get_interface_manager()->get_interface_by_id(getSourceDev());
}

InterfacePtr SFwallCore::Packet::getDestNetDevice() {
	return System::getInstance()->get_interface_manager()->get_interface_by_id(getDestDev());
}


int SFwallCore::Packet::getSourceDev() {
	return sqp->indev;
}

unsigned int SFwallCore::Packet::getTimestamp() {
	return this->timestamp;
}

int SFwallCore::Packet::getDestDev() {
	return sqp->outdev;
}

UserPtr SFwallCore::Packet::getUser() {
	HostPtr host_pointer = getHostDiscoveryServiceEntry();
	if (host_pointer) {
		return host_pointer->authenticated_user;	
	}

	return UserPtr();
}

std::string SFwallCore::Packet::getUsername() const {
	if (userPtr) {
		return userPtr->getUserName();
	}

	return "unknown";
}

std::string SFwallCore::Packet::getHw() {
	if(hasHw){
		return IP4Addr::convertHw(hwBuf);
	}
	return "-";
}

int SFwallCore::PacketV4::hash() {
	/*yes I know this overflows, it does not seem to matter*/
	if (getProtocol() == ICMP) {
		return transport->hash();
	}
	else if (getProtocol() == UDP || getProtocol() == TCP) {
		int hash = 0;
		hash += transport->hash();
		hash += getSrcIp();
		hash += getDstIp();
		return hash;
	}

	return -1;
}

int SFwallCore::PacketV4::getProtocol() {
	unsigned char *packet = sqp->raw_packet;
	struct iphdr *packet_header = (struct iphdr *) packet;

	return packet_header->protocol;
}

int SFwallCore::PacketV6::getProtocol() {
	if(transport){
		return transport->getProtocol();
	}
	return -1;
}


int SFwallCore::TcpUdpPacket::getProtocol() const {
	return -1;
}

int SFwallCore::TcpUdpPacket::getSrcPort() {
	if (getProtocol() == TCP) {
		struct tcphdr *packet_tcp_header = (struct tcphdr *) packet;

		return ntohs(packet_tcp_header->source);
	}
	else if (getProtocol() == UDP) {
		struct udphdr *packet_udp_header = (struct udphdr *) packet;
		return ntohs(packet_udp_header->source);
	}

	return -1;
}

int SFwallCore::TcpUdpPacket::getDstPort() {
	if (getProtocol() == TCP) {
		struct tcphdr *packet_tcp_header = (struct tcphdr *) packet;

		return ntohs(packet_tcp_header->dest);
	}
	else if (getProtocol() == UDP) {
		struct udphdr *packet_udp_header;
		packet_udp_header = (struct udphdr *) packet;
		return ntohs(packet_udp_header->dest);
	}

	return -1;
}

unsigned int SFwallCore::TcpUdpPacket::getTimestamp() {
	return this->timestamp;
}

void SFwallCore::TcpPacket::rst() {
	struct tcphdr *packet_tcp_header = (struct tcphdr *) packet;
	packet_tcp_header->rst = 1;
}

bool SFwallCore::TcpPacket::finSet() const {
	struct tcphdr *packet_tcp_header = (struct tcphdr *) packet;
	return packet_tcp_header->fin == 1;
}

bool SFwallCore::TcpPacket::ackSet() const {
	struct tcphdr *packet_tcp_header = (struct tcphdr *) packet;
	return packet_tcp_header->ack == 1;
}

bool SFwallCore::TcpPacket::synSet() const {
	struct tcphdr *packet_tcp_header = (struct tcphdr *) packet;
	return packet_tcp_header->syn == 1;
}

bool SFwallCore::TcpPacket::rstSet() const {
	struct tcphdr *packet_tcp_header = (struct tcphdr *) packet;
	return packet_tcp_header->rst == 1;
}

unsigned long SFwallCore::TcpPacket::getAckSeq() const {
	struct tcphdr *packet_tcp_header = (struct tcphdr *) packet;
	return ntohl(packet_tcp_header->ack_seq);
}

unsigned long SFwallCore::TcpPacket::getSeq() const {
	struct tcphdr *packet_tcp_header = (struct tcphdr *) packet;
	return ntohl(packet_tcp_header->seq);
}

unsigned short SFwallCore::IcmpPacket::getIcmpId() const {
	struct icmphdr *packet_tcp_header = (struct icmphdr *) packet;
	return ntohs(packet_tcp_header->un.echo.id);
}

int SFwallCore::TcpPacket::getSize() const {
	struct tcphdr *packet_tcp_header = (struct tcphdr *) packet;
	return packet_tcp_header->doff * 4;
}

int SFwallCore::UdpPacket::getSize() const {
	return sizeof(struct udphdr);
}

int SFwallCore::IcmpPacket::getSize() const {
	return sizeof(struct icmphdr);
}

SFwallCore::Packet *SFwallCore::Packet::parsePacket(const struct sq_packet *sqp) {
	if (sqp->type == 0) {
		return new PacketV4(sqp);
	}
	else if (sqp->type == 1) {
		return new PacketV6(sqp);
	}

	return NULL;
}

SFwallCore::Packet::Packet(const struct sq_packet *sqp)
	: safetycheck(100), transport(NULL), connection(NULL),
	sqp(const_cast<struct sq_packet *>(sqp)), hasHw(false) {
		if (!System::getInstance()->getFirewall()->IGNORE_MAC_ADDRESS && sqp->hwlen == 6) {
			hwBuf[0] = sqp->hw_addr[0];
			hwBuf[1] = sqp->hw_addr[1];
			hwBuf[2] = sqp->hw_addr[2];
			hwBuf[3] = sqp->hw_addr[3];
			hwBuf[4] = sqp->hw_addr[4];
			hwBuf[5] = sqp->hw_addr[5];
			hasHw = true;
		}
		this->timestamp = (unsigned int) time(0);
	}

SFwallCore::PacketV4::PacketV4(const struct sq_packet *sqp) : Packet(sqp) {
	unsigned char *packet = sqp->raw_packet;
	struct iphdr *packet_header = (struct iphdr *) packet;
	int transportLen = getLen() - (packet_header->ihl << 2);

	if (packet_header->protocol == IPPROTO_TCP) {
		transport = new TcpPacket((packet + (packet_header->ihl << 2)), transportLen);
	}
	else if (packet_header->protocol == IPPROTO_UDP) {
		transport = new UdpPacket((packet + (packet_header->ihl << 2)), transportLen);
	}
	else if (packet_header->protocol == IPPROTO_ICMP) {
		transport = new IcmpPacket((packet + (packet_header->ihl << 2)), transportLen);
	}
	else {
		transport = NULL;
	}
	this->timestamp = (unsigned int) time(0);
}

SFwallCore::PacketV6::PacketV6(const struct sq_packet *sqp) : Packet(sqp) {
	//Find the transport layer:
	unsigned char *packet = sqp->raw_packet;
	struct ip6_hdr *packet_header = (struct ip6_hdr *) packet;

	int nh = packet_header->ip6_ctlun.ip6_un1.ip6_un1_nxt;
	int advance = sizeof(struct ip6_hdr);
	int len = sqp->len;
	int cursor = 0;
	this->timestamp = (unsigned int) time(0);

	while (cursor < len) {
		cursor += advance;

		switch (nh) {
			case IPPROTO_TCP:
				transport = new TcpPacket(packet + cursor, len - cursor);
				return;

			case IPPROTO_UDP:
				transport = new UdpPacket(packet + cursor, len - cursor);
				return;

			case 58:
				transport = new IcmpPacket(packet + cursor, len - cursor);
				return;

			default:
				stringstream ss;
				ss << "Unknown ipv6 data segment " << nh;
				Logger::instance()->log("sphirewalld.firewall", ERROR,
						"Unknown ipv6 data segment, note: ipv6 support is experimental. nh = %d", nh);
				break;
		};
	}
}

SFwallCore::Packet::~Packet() {
	delete transport;
}

struct in6_addr *SFwallCore::PacketV6::getSrcIp() {
	unsigned char *packet = sqp->raw_packet;
	struct ip6_hdr *packet_header = (struct ip6_hdr *) packet;
	return &packet_header->ip6_src;
}

struct in6_addr *SFwallCore::PacketV6::getDstIp() {
	unsigned char *packet = sqp->raw_packet;
	struct ip6_hdr *packet_header = (struct ip6_hdr *) packet;
	return &packet_header->ip6_dst;
}

bool SFwallCore::PacketV4::isBroadcast() {
	if (IP4Addr::isBroadCast(getSrcIp()) || IP4Addr::isBroadCast(getDstIp())) {
		return true;
	}

	return false;
}

bool SFwallCore::PacketV4::isMulticast() {
	if (IP4Addr::isMultiCast(getSrcIp()) || IP4Addr::isMultiCast(getDstIp())) {
		return true;
	}

	return false;
}

void hexdump(std::ostream &output, char *buffer, size_t len, size_t line_len);

std::string SFwallCore::TcpPacket::toString() {
	std::stringstream ret;
	ret << "TCP dport:" << getDstPort() << " sport:" << getSrcPort() << " ack:" << getAckSeq() << " seq:" << getSeq();
	ret << " flags [ ";
	if(finSet()){
		ret << " FIN";	
	}

	if(ackSet()){
		ret << " ACK";
	}

	if(synSet()){
		ret << " SYN";
	}

	if(rstSet()){
		ret << " RST";	
	}

	ret << " ]";
	return ret.str();
}

std::string SFwallCore::UdpPacket::toString() {
	std::stringstream ret;
	ret << "UDP dport:" << getDstPort() << " sport:" << getSrcPort() << endl;
	return ret.str();
}

std::string SFwallCore::IcmpPacket::toString() {
	std::stringstream ret;
	ret << "ICMP icmpid: " << getIcmpId() << endl;
	return ret.str();
}

SFwallCore::TcpPacket::TcpPacket(unsigned char *packet, int len) : TcpUdpPacket(packet, len) {
	//Create Transports
	struct tcphdr *packet_tcp_header = (struct tcphdr *) packet;
	unsigned char *data = packet + (packet_tcp_header->doff * 4);

	int datalen = len - (packet_tcp_header->doff * 4);
	application = new ApplicationFrame(data, datalen);
	this->timestamp = (unsigned int) time(0);
}

SFwallCore::UdpPacket::UdpPacket(unsigned char *packet, int len) : TcpUdpPacket(packet, len) {
	application = new ApplicationFrame(packet + sizeof(struct udphdr), len - sizeof(struct udphdr));
	this->timestamp = (unsigned int) time(0);
}

SFwallCore::IcmpPacket::IcmpPacket(unsigned char *packet, int len): TransportFrame(packet, len) {
	application = new ApplicationFrame(packet + sizeof(struct icmphdr), len - sizeof(struct icmphdr));
}

SFwallCore::TransportFrame::TransportFrame(unsigned char *packet, int len) {
	this->packet = packet;
	this->len = len;
}

string SFwallCore::stringProtocol(proto p) {
	string s;

	switch (p) {
		case TCP:
			s = "TCP";
			break;

		case UDP:
			s = "UDP";
			break;

		case ICMP:
			s = "ICMP";
			break;

		case IGMP:
			s = "IGMP";
			break;

		default:
			s = "UKNOWN";
			break;
	}

	return s;
}

bool safematch(const char *match, unsigned char *buffer, int position, int len, int matchsize) {
	int diff = len - position;

	if (matchsize > diff) {
		return false;
	}

	for (int x = 0; x < matchsize; x++) {
		if (match[x] != buffer[x + position]) {
			return false;
		}
	}

	return true;
}

int SFwallCore::Packet::getSrcPort() {
	if (getTransport()->getProtocol() == TCP || getTransport()->getProtocol() == UDP) {
		TcpUdpPacket *p = (TcpUdpPacket *) getTransport();
		return p->getSrcPort();
	}

	return -1;
}

int SFwallCore::Packet::getDstPort() {
	if (getTransport()->getProtocol() == TCP || getTransport()->getProtocol() == UDP) {
		TcpUdpPacket *p = (TcpUdpPacket *) getTransport();
		return p->getDstPort();
	}

	return -1;
}

//Some virtual methods that we should probably remove:
bool SFwallCore::Packet::isBroadcast() {
	return false;
}

bool SFwallCore::Packet::isMulticast() {
	return false;
}
int SFwallCore::Packet::getLen() {
	return -1;
}
int SFwallCore::Packet::getHeaderLen() {
	return -1;
}
int SFwallCore::Packet::getPayloadLen() {
	return -1;
}
int SFwallCore::Packet::hash() {
	return -1;
}
int SFwallCore::Packet::type() {
	return -1;
}

std::string SFwallCore::PacketV4::toString() {
	stringstream ss;
	ss << "sqp_len:" << sqp->len << " IPv4 src:" << IP4Addr::ip4AddrToString(getSrcIp()) << " dest:" << IP4Addr::ip4AddrToString(getDstIp()) << " len:" << getLen();

	if (transport) {
		ss << " Transport:" << transport->toString();
	}

	return ss.str();
}

std::string SFwallCore::PacketV6::toString() {
	stringstream ss;
	ss << "IPv6 src:" << IP6Addr::toString(getSrcIp()) << " dst:" << IP6Addr::toString(getDstIp()) << " len: " << getLen();

	if (transport) {
		ss << " Transport:" << transport->toString();
	}

	return ss.str();
}


HostPtr SFwallCore::Packet::getHostDiscoveryServiceEntry() {
	if(getConnection()){
		return getConnection()->getHostDiscoveryServiceEntry();
	}

	return host_ptr;
}
