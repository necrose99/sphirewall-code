#include <iostream>

using namespace std;

#include "SFwallCore/SecurityGroup.h"
#include "SFwallCore/Criteria.h"

list<SFwallCore::SecurityGroupPtr> SFwallCore::SecurityGroupStore::list_groups(){
	list<SFwallCore::SecurityGroupPtr> ret;
	holdLock();
	ret = security_groups;
	releaseLock();
	return ret;
}

SFwallCore::SecurityGroupPtr SFwallCore::SecurityGroupStore::get_group(std::string id){
	holdLock();
	for(auto group : security_groups){
		if(group->id.compare(id) == 0){
			releaseLock();
			return group;
		}
	}

	releaseLock();
	return SFwallCore::SecurityGroupPtr();
}

/* Required for Configurable */
bool SFwallCore::SecurityGroupStore::load(){
        ObjectContainer *root;

        if (configurationManager->has("sgroups")) {
                root = configurationManager->getElement("sgroups");
                ObjectContainer *groups = root->get("groups")->container();

                for (int x = 0; x < groups->size(); x++) {
                        SecurityGroupPtr target_sgroup = SecurityGroupPtr(new SecurityGroup());
                        ObjectContainer *source = groups->get(x)->container();
			
			target_sgroup->id = source->get("id")->string();
			target_sgroup->name = source->get("name")->string();
			ObjectContainer *entries= source->get("entries")->container();				
			for(int y = 0; y < entries->size(); y++){
				ObjectContainer* entry = entries->get(y)->container();
				SecurityGroupEntryPtr target_sgroup_entry = SecurityGroupEntryPtr(new SecurityGroupEntry());
				target_sgroup_entry->id = entry->get("id")->string();					
				target_sgroup_entry->name = entry->get("name")->string();					

				ObjectContainer* criteria = entry->get("criteria")->container();
				for(int y = 0; y < criteria->size(); y++){
					CriteriaPtr c = CriteriaPtr(CriteriaBuilder::parse(criteria->get(y)->container()));
					if(c){
						target_sgroup_entry->criteria.push_back(c);
					}
				}


				target_sgroup->entries.push_back(target_sgroup_entry);
			}

			security_groups.push_back(target_sgroup);	
		}
	}

	refresh();
	return true;
}

void SFwallCore::SecurityGroup::refresh(){
	holdLock();
	for(SecurityGroupEntryPtr entry : entries){
		for(CriteriaPtr c : entry->criteria){
			c->refresh();
		}
	}
	releaseLock();
}

void SFwallCore::SecurityGroupStore::refresh(){
	for(SecurityGroupPtr group : security_groups){
		group->refresh();
	}
}

void SFwallCore::SecurityGroupStore::save(){
	holdLock();
	ObjectContainer *root = new ObjectContainer(CREL);
	ObjectContainer *sgroups = new ObjectContainer(CARRAY);

	for (auto sgroup : security_groups) {
		sgroup->holdLock();
		ObjectContainer *target_sgroup = new ObjectContainer(CREL);
		target_sgroup->put("id", new ObjectWrapper((string) sgroup->id));
		target_sgroup->put("name", new ObjectWrapper((string) sgroup->name));

		ObjectContainer *target_sgroup_entries = new ObjectContainer(CARRAY);
		for(auto sgroup_entry : sgroup->entries){
			ObjectContainer *target_sgroup_entry = new ObjectContainer(CREL);
			target_sgroup_entry->put("id", new ObjectWrapper((string) sgroup_entry->id));
			target_sgroup_entry->put("name", new ObjectWrapper((string) sgroup_entry->name));

			ObjectContainer* criteria = new ObjectContainer(CARRAY);
			for(auto c: sgroup_entry->criteria){
				criteria->put(new ObjectWrapper(CriteriaBuilder::serialize(c.get())));
			}
			target_sgroup_entry->put("criteria", new ObjectWrapper(criteria));
			target_sgroup_entries->put(new ObjectWrapper(target_sgroup_entry));
		}

		target_sgroup->put("entries", new ObjectWrapper(target_sgroup_entries));
		sgroups->put(new ObjectWrapper(target_sgroup));
		sgroup->releaseLock();
	}

	root->put("groups", new ObjectWrapper(sgroups));
	releaseLock();

	configurationManager->holdLock();
	configurationManager->setElement("sgroups", root);
	configurationManager->save();
	configurationManager->releaseLock();
}

bool SFwallCore::SecurityGroup::matches(SFwallCore::Packet* packet, int direction){
	holdLock();
	for(SecurityGroupEntryPtr entry : entries){
		if(entry->matches(packet,direction)){
			releaseLock();
			return true;
		}
	}

	releaseLock();
	return false;
}

bool SFwallCore::SecurityGroupEntry::matches(SFwallCore::Packet* packet, int direction){
	for(CriteriaPtr c : criteria){
		if(!c->match(packet, direction)){
			return false;
		}
	}

	return true;
}

void SFwallCore::SecurityGroupStore::add_group(SecurityGroupPtr group){
	holdLock();
	security_groups.push_back(group);
	releaseLock();
}

void SFwallCore::SecurityGroupStore::del_group(SecurityGroupPtr group){
	holdLock();
	security_groups.remove(group);
	releaseLock();
}

