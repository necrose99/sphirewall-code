/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef CAPTURE_H
#define CAPTURE_H

#include "Packetfwd.h"
#include "Core/System.h"
#include "Kernel/client.h"

namespace SFwallCore {
	class Nat;

	void redirectToHTTPAuthPortal(unsigned char *packet, int len);

	NatInstruction *processPrerouting(struct sq_packet *sqp);
	int processFilterHook(struct sq_packet *sqp, bool &modified);
	NatInstruction *processPostrouting(struct sq_packet *sqp);

	int processPacket(struct sq_packet *packet, bool &modified);
	void startCapture();

	string extractStringPacketData(unsigned char *packet, int len);

	int processFilter(Packet *pk);
	Nat *processNat(Packet *pk);
	int is_verdict_applied_from_external_handler(SFwallCore::Packet* packet);
};

#endif
