/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef NAT_ACLS_H
#define NAT_ACLS_H

#define UNSET (unsigned int) -1

#include "SFwallCore/Criteria.h"
#include "Utils/Interfaces.h"
#include <boost/unordered_set.hpp>
namespace SFwallCore {
	class Packet;

	class PortForwardingRule : public Lockable {
		public:
			PortForwardingRule() :
				forwardingDestination(UNSET),
				forwardingDestinationPort(UNSET),
				enabled(false) {}

			std::string id;
			std::string comment;
			std::list<CriteriaPtr> source_criteria;
			std::list<CriteriaPtr> destination_criteria;

			unsigned int forwardingDestination;
			unsigned int forwardingDestinationPort;
			bool enabled;

			bool match(SFwallCore::Packet *packet);
	};

	class OneToOneNatRule : public Lockable {
		public:
			OneToOneNatRule() :
				internalIp(UNSET),
				externalIp(UNSET),
				enabled(false) {}

			std::string id;

			unsigned int internalIp;
			unsigned int externalIp;
			std::string interface;
			InterfacePtr interface_ptr;
			bool enabled;
			std::string name;

			bool match(SFwallCore::Packet *packet);
	};


	class MasqueradeRule : public Lockable {
		public:
			MasqueradeRule() :
				enabled(false),
				natTargetIp(UNSET)
			{}

			in_addr_t getUsableNatTargetIp() {
				if (natTargetDevicePtr) {
					return natTargetDevicePtr->get_primary_ipv4_address();
				}
				else if (natTargetIp) {
					return natTargetIp;
				}

				return -1;
			}
	
                        std::list<CriteriaPtr> source_criteria;
                        std::list<CriteriaPtr> destination_criteria;
	
			std::string natTargetDevice;
			std::string comment;
			InterfacePtr destinationDevicePtr;
			InterfacePtr natTargetDevicePtr;
			unsigned int natTargetIp;
			std::string id;
			bool enabled;

			bool match(SFwallCore::Packet *packet);
	};

        class AutowanInterface : public Lockable {
                public:
                        int failover_index;
                        std::string interface;

                        InterfacePtr resolved_interface;
                        std::list<Criteria*> criteria;
        };

        typedef boost::shared_ptr<AutowanInterface> AutowanInterfacePtr;
	typedef boost::shared_ptr<OneToOneNatRule> OneToOneNatRulePtr;
	typedef boost::shared_ptr<PortForwardingRule> PortForwardingRulePtr;
	typedef boost::shared_ptr<MasqueradeRule> MasqueradeRulePtr;

	enum {
		DISABLED = -1, SINGLE=0, AUTOWAN_MODE_LOADBALANCING = 1, FAILOVER = 2, TOR_GATEWAY=3
	};

	class NatAclStore : public Configurable, public Lockable {
		public:
			class InterfaceChangeListener : public virtual IntMgrChangeListener {
				public:
					InterfaceChangeListener(NatAclStore *store) :
						store(store) {}

					void interface_change(InterfacePtr) {
						store->initRules();
					}
				private:
					NatAclStore *store;
			};


			NatAclStore(IntMgr *interfaceManager):
				interfaceManager(interfaceManager) {
				interfaceManager->register_change_listener(new InterfaceChangeListener(this));
			}
			NatAclStore(){}

			void addMasqueradeRule(MasqueradeRulePtr rule);
			void delMasqueradeRule(MasqueradeRulePtr rule);
			std::list<MasqueradeRulePtr> listMasqueradeRules();
			MasqueradeRulePtr getMasqueradeRule(std::string id);

			void addForwardingRule(PortForwardingRulePtr rule);
			void delPortForwardingRule(PortForwardingRulePtr rule);
			std::list<PortForwardingRulePtr> listPortForwardingRules();
			PortForwardingRulePtr getPortForwardingRule(std::string id);

                        void addOneToOneRule(OneToOneNatRulePtr rule);
                        void delOneToOneRule(OneToOneNatRulePtr rule);
                        std::list<OneToOneNatRulePtr> listOneToOneRule();
                        OneToOneNatRulePtr getOneToOneRule(std::string id);

			bool load();
			void save();

			MasqueradeRulePtr matchMasqueradeRule(SFwallCore::Packet *packet);
			PortForwardingRulePtr matchForwardingRule(SFwallCore::Packet *packet);
			SFwallCore::OneToOneNatRulePtr matchOneToOneNatRule_prerouting(SFwallCore::Packet*);
			SFwallCore::OneToOneNatRulePtr matchOneToOneNatRule_postrouting(SFwallCore::Packet*);

			void initRules();

			//Autowan code
			int get_autowan_mode();
			InterfacePtr get_autowan_single_interface(){
				return autowan_single_interface;
			}

			void set_autowan_single_interface(std::string interface){
				this->autowan_single_interface_str = interface;
			}

			void set_autowan_single_interface(InterfacePtr interface){
				this->autowan_single_interface = interface;
			}

			void set_autowan_mode(int mode);
			int determine_routing_fwmark(SFwallCore::Packet* incomming_packet);
			bool determine_autowan_matches(SFwallCore::Packet* incomming_packet);
			bool determine_automatic_tor_gateway(SFwallCore::Packet* incomming_packet);
			std::vector<AutowanInterfacePtr> get_autowan_rules();
			void remove_autowan_rules();

			AutowanInterfacePtr get_autowan_rule_interface(std::string interface);
			AutowanInterfacePtr get_autowan_rule_interface(InterfacePtr target);
			void del_autowan_rule_interface(std::string interface);
			void add_autowan_rule(AutowanInterfacePtr rule);

			const char* getConfigurationSystemName(){
				return "Forwarding and Filtering system";
			}

		private:
			std::list<MasqueradeRulePtr> masqueradeRules;
			std::list<PortForwardingRulePtr> portFowardingRules;
			std::list<OneToOneNatRulePtr> oneToOneNatRules;

			vector<AutowanInterfacePtr> autowan_rules;
			std::set<InterfacePtr> autowan_active_interfaces;

			InterfacePtr autowan_single_interface;
			std::string autowan_single_interface_str;
			int autowan_load_balancing_device_pool_last;
			int autowan_mode;

			IntMgr* interfaceManager;
	};
};

#endif
