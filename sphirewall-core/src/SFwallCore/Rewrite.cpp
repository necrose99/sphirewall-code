/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <cstring>
#include <boost/algorithm/string.hpp>
#include <boost/format.hpp>
#include "SFwallCore/Rewrite.h"
#include "SFwallCore/Packet.h"
#include "Core/ConfigurationManager.h"
#include "SFwallCore/ApplicationLevel/FilterHandler.h"
#include "SFwallCore/ApplicationLevel/GoogleSafeSearchEnforcer.h"
#include "Core/System.h"
#include "Core/Cloud.h"
#include "SFwallCore/Connection.h"
#include "SFwallCore/State.h"
#include "Utils/DnsType.h"
#include <Utils/Dump.h>
#include "SFwallCore/Alias.h"
#include "SFwallCore/Firewall.h"
#include "Utils/StringUtils.h"

using namespace std;

const string redirectTemplate = "HTTP/1.1 307 Temporary Redirect\r\n"
"Location: %1%?url=%2%&deviceid=%3%&ip=%4%&mac=%5%&rule=%6%\r\n"
"Content-Length:0\r\n\r\n";

SFwallCore::DNSRecaster::~DNSRecaster() {}
SFwallCore::HttpRedirectRewrite::HttpRedirectRewrite(string url) : url(url), id() {}
SFwallCore::HttpRedirectRewrite::HttpRedirectRewrite(string id, string url) : url(url), id(id) {}
SFwallCore::HttpRedirectRewrite::~HttpRedirectRewrite() {}
SFwallCore::RewriteRule::~RewriteRule() {}
SFwallCore::DNSRecaster::DNSRecaster() {}


SFwallCore::DNSRecaster::DNSRecaster(std::set<std::string> from, std::string to) : from(from) {
	if (to.length() > 0 && to[0] != '.') {
		this->to = "." + to;
	}
	else if (to.length() > 0) {
		this->to = to;
	}
}

void SFwallCore::HttpRedirectRewrite::rewrite(SFwallCore::Connection *connection, SFwallCore::Packet *packet) {
	ConfigurationManager &config = System::getInstance()->configurationManager;
	unsigned char * rule_name_char = (unsigned char*) id.c_str();
	char* base64_encoded_rule_name = StringUtils::base64(rule_name_char, (int) id.length());
	const string redirect = boost::str(boost::format(redirectTemplate)
			% url
			% connection->getApplicationInfo()->http_hostName
			% System::getInstance()->getCloudConnection()->getDeviceId()
			% connection->getIp()->getSrcIpString()
			% connection->getHwAddress()
			% base64_encoded_rule_name
			);
	if (packet->getTransport()->getApplication()->write(redirect)) {
		if(redirect.size() > packet->getTransport()->getApplication()->getSize()){
			packet->resizePayload(redirect.size());
			connection->resizePayload(DIR_OPPOSITE, redirect.size() - packet->getTransport()->getApplication()->getSize());
		}
	}else{
		connection->terminate();
	}
}

void SFwallCore::HttpEnforceGoogleSafeSearch::rewrite(SFwallCore::Connection *conn, SFwallCore::Packet *packet) {
	std::string appendage = "safe=vss&";
	unsigned char *link = packet->getTransport()->getApplication()->getInternal();

	if (!GoogleSafeSearchEngine::requestNeedsRewrite(link)) {
		return;
	}

	int newLen = packet->getTransport()->getApplication()->getSize() + appendage.size();
	int deltaSize = 0;

	unsigned char *data = new unsigned char[newLen];
	uint i = 0, j = 0, skipping = 0;
	unsigned k = 0;

	data[i++] = 'G';
	data[i++] = 'E';
	data[i++] = 'T';
	data[i++] = ' ';
	data[i++] = '/';

	for (; i < packet->getTransport()->getApplication()->getSize(); i++) {
		data[i] = link[i];

		if (link[i] == '?') {
			break;
		}
	}

	for (j = ++i, k = 0; k < appendage.length(); k++, j++) {
		data[j] = (unsigned char) appendage[k];
	}

	for (; i < packet->getTransport()->getApplication()->getSize(); i++) {
		if (!skipping && link[i] == 'R' && !strncmp("Referer:", (const char *) &link[i], strlen("Referer"))) {
			skipping = 1;
		}

		if (!skipping) {
			data[j++] = link[i];
		}
		else {
			if (link[i] == '\n') {
				skipping = 0;
			}

			deltaSize--;
		}
	}

	if (!packet->getTransport()->getApplication()->write(data, newLen + deltaSize)) {
		conn->terminate();
	}else{
		packet->resizePayload(newLen + deltaSize);
		conn->resizePayload(DIR_SAME, deltaSize + appendage.size());
	}

	delete[] data;
}


void SFwallCore::DNSRecaster::rewrite(SFwallCore::Connection *conn, SFwallCore::Packet *packet) {
	DnsMessage *m = new DnsMessage((char *)packet->getTransport()->getApplication()->getInternal());
	size_t len = 0;
	char *data = NULL;

	if (! m->isQuery()) {
		m->clearResponses();
		m->getAnswers().push_back(
				new DnsAnswer(
					packet->getConnection()->getApplicationInfo()->dnsQueryName,
					DnsRecordTypes::CNAME,
					DnsRecordClasses::INTERNET,
					7200,
					new DnsCNAMERecord(to)
					)
				);

		m->getAnswers().push_back(
				new DnsAnswer(
					to,
					DnsRecordTypes::A,
					DnsRecordClasses::INTERNET,
					7200,
					new DnsARecord(htonl(IP4Addr::stringToIP4Addr("216.239.32.20")))
					)
				);

		if ((data = m->toCString(&len))) {
			packet->resizePayload(len);

			if (!packet->getTransport()->getApplication()->write((unsigned char *)data, len)) {
				conn->terminate();
			}

			delete data;
		}

		delete m;
	}
}

SFwallCore::PacketDirection SFwallCore::DNSRecaster::direction() {
	return DIR_OPPOSITE;
}

SFwallCore::PacketDirection SFwallCore::HttpRedirectRewrite::direction() {
	return DIR_OPPOSITE;
}

SFwallCore::PacketDirection SFwallCore::HttpEnforceGoogleSafeSearch::direction() {
	return DIR_SAME;
}


