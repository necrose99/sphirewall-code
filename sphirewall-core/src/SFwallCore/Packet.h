/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <SFwallCore/Packetfwd.h>

#ifndef PACKET_H
#define PACKET_H

/*Packet.h*/
#include <netinet/ip.h>
#include <netinet/ip6.h>
#include <cstring>
#include <string>

#include "Utils/IP4Addr.h"
#include "Kernel/sphirewall_queue.h"
#include "Auth/User.h"
#include "Auth/Session.h"
#include "Core/Host.h"

#include "Utils/Interfaces.h"

#define IPV4 0
#define IPV6 1

#define AF_NDEF 0
#define AF_HTTP 1
#define AF_HTTPS 2
#define AF_HTTP_PROXY 3


namespace SFwallCore {

	class ApplicationFrame {
		public:
			ApplicationFrame(unsigned char *packet, int len);
			~ApplicationFrame();
			unsigned char *getInternal();
			int getSize();
			void resizePayload(int newlen) {
				len = newlen;
			}
			bool write(std::string input);
			bool write(unsigned char *data, int len);
		protected:
			unsigned char *packet;
			int len;
	};

	class TransportFrame {
		public:
			TransportFrame(unsigned char *packet, int len);
			virtual ~TransportFrame() {
				delete application;
			}
			virtual int getProtocol() const {
				return -1;
			}
			virtual int hash() {
				return -1;
			}

			virtual std::string toString() {
				return "";
			};
			ApplicationFrame *getApplication() {
				return application;
			}

			void setApplicationFrame(ApplicationFrame *frame) {
				this->application = frame;
			}
			unsigned char *getInternal() {
				return packet;
			}

			virtual int getSize() const = 0;
			virtual void resizePayload(int newlen) {}
		protected:
			unsigned char *packet;
			int len;
			ApplicationFrame *application;
	};

	class Packet {
		public:
			Packet();
			Packet(const struct sq_packet *sqp);
			virtual ~Packet();

			static Packet *parsePacket(const struct sq_packet *sqp);
			static struct sq_packet *cloneSqp(struct sq_packet *sqp);

			UserPtr getUser() ;
			int getSrcPort() ;
			int getDstPort() ;
			int getDestDev() ;
			int getSourceDev() ;
			unsigned int getTimestamp();
			bool hasHwAddr();
			std::string getHw() ;
			virtual int getProtocol() = 0;
			void setHw(std::string hw);
			TransportFrame *getTransport();
			std::string getUsername() const;
			struct sq_packet *getInternalSqp();
			InterfacePtr getSourceNetDevice();
			InterfacePtr getDestNetDevice();
			Connection *getConnection() const;

                        void setHostDiscoveryServiceEntry(HostPtr host){ this->host_ptr = host;}
                        HostPtr getHostDiscoveryServiceEntry();

			void setConnection(Connection *connection) ;
			void setInternalSqp(struct sq_packet *sqp);
			void setUser(UserPtr user);
			void incrementRuleMetrics(int layer_type, int accept_or_deny);

			virtual int getPayloadLen();
			virtual int getHeaderLen();
			virtual int getLen();
			virtual bool isBroadcast();
			virtual bool isMulticast();
			virtual std::string toString() ;
			virtual int hash();
			virtual int type();
			virtual void resizePayload(int newlen) = 0;

			int safetycheck;

		protected:
			bool hasHw;
			int sport;
			int icmpId;
			unsigned int timestamp;

			//proto protocol;
			UserPtr userPtr;
			Connection *connection;

			//Root information:
			struct sq_packet *sqp;
			TransportFrame *transport;
			u_int8_t hwBuf[6];
			HostPtr host_ptr;
	};

	class PacketV4 : public Packet {
		public:
			PacketV4(const struct sq_packet *sqp);
			virtual ~PacketV4() {}

			in_addr_t getSrcIp() const;
			in_addr_t getDstIp() const;

			int getCheckSum();
			int getLen();
			int getHeaderLen();
			int getPayloadLen();
			int getProtocol();
			int type() {
				return IPV4;
			}

			int hash();

			bool isBroadcast();
			bool isMulticast();

			std::string toString();
			void resizePayload(int newlen);
	};

	class PacketV6 : public Packet {
		public:
			PacketV6(const struct sq_packet *sqp);
			virtual ~PacketV6() {}

			struct in6_addr *getSrcIp();
			struct in6_addr *getDstIp();

			int getProtocol();
			int type() {
				return IPV6;
			}

			int hash();
			std::string toString();
			void resizePayload(int newlen);
	};

	class TcpUdpPacket : public TransportFrame {
		public:
			TcpUdpPacket(unsigned char *ptr, int len) : TransportFrame(ptr, len) {}

			int getSrcPort();
			int getDstPort();
			unsigned int getTimestamp();

			virtual ~TcpUdpPacket() {
			}

			virtual int getSize() const = 0;

			virtual int getProtocol() const ;
			int hash() {
				int h = getSrcPort() + getDstPort();
				return h;
			}

		protected:
			unsigned int timestamp;
	};


	class TcpPacket : public TcpUdpPacket {
		public:

			TcpPacket(unsigned char *ptr, int len);
			~TcpPacket() {
			}

			std::string echo() const;
			bool finSet() const;
			bool ackSet() const;
			bool synSet() const;
			bool rstSet() const;

			unsigned long getAckSeq() const;
			unsigned long getSeq() const;

			void rst();
			std::string toString() ;

			int getProtocol() const {
				return TCP;
			}

			int getSize() const ;
			void resizePayload(int newlen);
	};

	class UdpPacket : public TcpUdpPacket {
		public:

			UdpPacket(unsigned char *ptr, int len);
			~UdpPacket() {
			}

			std::string echo() const;
			std::string toString();
			int getProtocol() const {
				return UDP;
			}
			int getSize() const ;
			void resizePayload(int newlen);
	};

	class IcmpPacket : public TransportFrame {
		public:
			IcmpPacket(unsigned char *ptr, int len);
			~IcmpPacket() {
			}

			std::string toString() ;
			unsigned short getIcmpId() const;
			int getProtocol() const {
				return ICMP;
			}

			int hash() {
				return getIcmpId();
			}
			int getSize() const ;
	};
}

#endif
