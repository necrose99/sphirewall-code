#ifndef CONNTRACKER_DELETE_HOOK_H
#define CONNTRACKER_DELETE_HOOK_H

namespace SFwallCore {

	class Connection;
        class DeleteConnectionHook {
                public:
                        virtual void hook(Connection *conn) = 0;
        };
};

#endif
