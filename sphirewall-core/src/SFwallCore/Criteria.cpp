/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <iostream>
#include "SFwallCore/Criteria.h"
#include "SFwallCore/Connection.h"
#include "SFwallCore/TimePeriods.h"
#include "Utils/IP6Addr.h"
#include "SFwallCore/Firewall.h"
#include "Core/System.h"
#include "Auth/GroupDb.h"
#include "Utils/HttpRequestWrapper.h"
#include "Utils/StringUtils.h"

using namespace std;

std::string SFwallCore::SourceIpCriteria::key() const {
	return "source.ipv4";
}

bool SFwallCore::SourceIpCriteria::inner_match(SFwallCore::Packet* packet){
	if (packet->type() != IPV4) {
		return false;
	}

	PacketV4 *ipv4 = (PacketV4 *) packet;
	if (IP4Addr::matchNetwork(ipv4->getSrcIp(), ip, mask) == -1) {
		return false;
	}

	return true;
}

std::string SFwallCore::DestinationIpCriteria::key() const {
	return "destination.ipv4";
}

bool SFwallCore::DestinationIpCriteria::inner_match(SFwallCore::Packet* packet){
	if (packet->type() != IPV4) {
		return false;
	}

	PacketV4 *ipv4 = (PacketV4 *) packet;
	if (IP4Addr::matchNetwork(ipv4->getDstIp(), ip, mask) == -1) {
		return false;
	}

	return true;
}

SFwallCore::StatefulDnsDestinationCriteria::StatefulDnsDestinationCriteria(){
	stateful_cnames = AliasPtr(new WebsiteListAlias());
	stateful_ipaddresses = AliasPtr(new IpRangeAlias());
}

std::string SFwallCore::StatefulDnsDestinationCriteria::key() const {
	return "dns.destination";
}

bool SFwallCore::StatefulDnsDestinationCriteria::inner_match(SFwallCore::Packet* packet){
	if (packet->type() != IPV4) {
		return false;
	}

	if(packet->getConnection()->getApplicationInfo()->type == ConnectionApplicationInfo::Classifier::DNS){
		/* We need to collect the data from DNS queries here */
		ConnectionApplicationInfo* info = packet->getConnection()->getApplicationInfo();
		if(info->dnsQueryNameSet && info->dnsQueryAnswerSet){
			if(info->dnsQueryName.find(target_hostname) != -1 || stateful_cnames->search(info->dnsQueryName)){
				//Found a query that matches our requirements	
				for(unsigned int ip : info->dnsQueryAAnswers){
					stateful_ipaddresses->addEntry(IP4Addr::ip4AddrToString(ip));
				}
			}
		}	
	}else{
		PacketV4 *ipv4 = (PacketV4 *) packet;
		if (!stateful_ipaddresses->searchForNetworkMatch(ipv4->getDstIp())) {
			return false;
		}

		return true;
	}

	return false;
}

std::string SFwallCore::IpCriteria::key() const {
	return "ipv4";
}

bool SFwallCore::IpCriteria::inner_match(SFwallCore::Packet* packet, int direction){
	if (packet->type() != IPV4) {
		return false;
	}

	PacketV4 *ipv4 = (PacketV4 *) packet;
	if(direction == DIRECTION_SOURCE){
		if (IP4Addr::matchNetwork(ipv4->getSrcIp(), ip, mask) == -1) {
			return false;
		}
	}else if(direction == DIRECTION_DESTINATION){
		if (IP4Addr::matchNetwork(ipv4->getDstIp(), ip, mask) == -1) {
			return false;
		}
	}

	return true;
}

std::string SFwallCore::SourceIp6Criteria::key() const {
	return "source.ipv6";
}

bool SFwallCore::SourceIp6Criteria::inner_match(SFwallCore::Packet* packet){
	if (packet->type() != IPV6) {
		return false;
	}

	PacketV6 *ip = (PacketV6 *) packet;
	if (!IP6Addr::matchNetwork(ip->getSrcIp(), addr, cidr)) {
		return false;
	}

	return true;
}

std::string SFwallCore::DestinationIp6Criteria::key() const {
	return "destination.ipv6";
}

bool SFwallCore::DestinationIp6Criteria::inner_match(SFwallCore::Packet* packet){
	if (packet->type() != IPV6) {
		return false;
	}

	PacketV6 *ip = (PacketV6*) packet;
	if (!IP6Addr::matchNetwork(ip->getDstIp(), addr, cidr)) {
		return false;
	}

	return true;
}

std::string SFwallCore::Ip6Criteria::key() const {
	return "ipv6";
}

bool SFwallCore::Ip6Criteria::inner_match(SFwallCore::Packet* packet, int direction){
	if (packet->type() != IPV6) {
		return false;
	}

	PacketV6 *ip = (PacketV6 *) packet;
	if(direction == DIRECTION_SOURCE){
		if (!IP6Addr::matchNetwork(ip->getSrcIp(), addr, cidr)) {
			return false;
		}
	}else if(direction == DIRECTION_DESTINATION){
		if (!IP6Addr::matchNetwork(ip->getDstIp(), addr, cidr)) {
			return false;
		}
	}

	return true;
}

std::string SFwallCore::GroupCriteria::key() const {
	return "group";
}

bool SFwallCore::GroupCriteria::inner_match(SFwallCore::Packet* packet){
	if (!(packet->getConnection()->getHostDiscoveryServiceEntry() && packet->getConnection()->getHostDiscoveryServiceEntry()->authenticated_user)) {
		return false;
	}

	if(group){
		if (packet->getConnection()->getHostDiscoveryServiceEntry()->authenticated_user->checkForGroup(group)) {
			return true;	
		}
	}else if(groups.size() > 0){
		bool hasGroupMatch = false;
		for (GroupPtr group : groups) {
			if (packet->getConnection()->getHostDiscoveryServiceEntry()->authenticated_user->checkForGroup(group)) {
				return true;
			}       
		}       
	}	

	return false;
}

std::string SFwallCore::SourceIpAliasCriteria::key() const {
	return "source.ipv4.alias";
}

bool SFwallCore::SourceIpAliasCriteria::inner_match(SFwallCore::Packet* packet){
	if (aliases.size() > 0) {
		bool hasSourceMatch = false;

		if (packet->type() != IPV4) {
			return false;
		}

		PacketV4 *ipv4 = (PacketV4 *) packet;

		for (AliasPtr alias_target : aliases) {
			if (alias_target->searchForNetworkMatch(ipv4->getSrcIp())) {
				return true;
			}
		}
	}

	if(alias){
		PacketV4 *ipv4 = (PacketV4 *) packet;
		if (alias->searchForNetworkMatch(ipv4->getSrcIp())) {
			return true;
		}       
	}

	return false;
}

void SFwallCore::SourceIpAliasCriteria::refresh(){
	if(alias && alias->source){
		alias->load();
	}
}

std::string SFwallCore::DestinationIpAliasCriteria::key() const {
	return "destination.ipv4.alias";
}

bool SFwallCore::DestinationIpAliasCriteria::inner_match(SFwallCore::Packet* packet){
	if (aliases.size() > 0) {
		bool hasSourceMatch = false;

		if (packet->type() != IPV4) {
			return false;
		}

		PacketV4 *ipv4 = (PacketV4 *) packet;

		for (AliasPtr alias_target : aliases) {
			if (alias_target->searchForNetworkMatch(ipv4->getDstIp())) {
				return true;
			}
		}
	}

	if(alias){
		PacketV4 *ipv4 = (PacketV4 *) packet;
		if (alias->searchForNetworkMatch(ipv4->getDstIp())) {
			return true;
		}
	}

	return false;
}

void SFwallCore::DestinationIpAliasCriteria::refresh(){
	if(alias && alias->source){
		alias->load();
	}
}

std::string SFwallCore::DestinationPortCriteria::key() const {
	return "destination.transport.port";
}

std::string SFwallCore::IpAliasCriteria::key() const {
	return "ipv4.alias";
}

bool SFwallCore::IpAliasCriteria::inner_match(SFwallCore::Packet* packet, int direction){
	if (aliases.size() > 0) {
		bool hasSourceMatch = false;

		if (packet->type() != IPV4) {
			return false;
		}

		PacketV4 *ipv4 = (PacketV4 *) packet;

		if(direction == DIRECTION_SOURCE){
			for (AliasPtr alias_target : aliases) {
				if (alias_target->searchForNetworkMatch(ipv4->getSrcIp())) {
					return true;
				}
			}
		}else if(direction == DIRECTION_DESTINATION){
			for (AliasPtr alias_target : aliases) {
				if (alias_target->searchForNetworkMatch(ipv4->getDstIp())) {
					return true;
				}
			}
		}
	}

	if(alias){
		PacketV4 *ipv4 = (PacketV4 *) packet;
		if(direction == DIRECTION_SOURCE){
			if (alias->searchForNetworkMatch(ipv4->getSrcIp())) {
				return true;
			}
		}else if(direction == DIRECTION_DESTINATION){
			if (alias->searchForNetworkMatch(ipv4->getDstIp())) {
				return true;
			}
		}
	}

	return false;
}

void SFwallCore::IpAliasCriteria::refresh(){
	if(alias && alias->source){
		alias->load();
	}
}


bool SFwallCore::DestinationPortCriteria::inner_match(SFwallCore::Packet* packet){
	if (!(packet->getProtocol() == TCP || packet->getProtocol() == UDP)) {
		return false;
	}

	if (ports.size() > 0 && ports.find(packet->getDstPort()) == ports.end()) {
		return false;
	}

	return true;
}

std::string SFwallCore::SourcePortCriteria::key() const {
	return "source.transport.port";
}

bool SFwallCore::SourcePortCriteria::inner_match(SFwallCore::Packet* packet){
	if (!(packet->getProtocol() == TCP || packet->getProtocol() == UDP)) {
		return false;
	}

	if (ports.size() > 0 && ports.find(packet->getSrcPort()) == ports.end()) {
		return false;
	}

	return true;
}

std::string SFwallCore::PortCriteria::key() const {
	return "transport.port";
}

bool SFwallCore::PortCriteria::inner_match(SFwallCore::Packet* packet, int direction){
	if (!(packet->getProtocol() == TCP || packet->getProtocol() == UDP)) {
		return false;
	}

	if(direction == DIRECTION_DESTINATION){
		if (ports.size() > 0 && ports.find(packet->getDstPort()) == ports.end()) {
			return false;
		}
	}else if(direction == DIRECTION_SOURCE){
		if (ports.size() > 0 && ports.find(packet->getSrcPort()) == ports.end()) {
			return false;
		}
	}

	return true;
}

std::string SFwallCore::SourcePortRangeCriteria::key() const {
	return "source.transport.portrange";
}

bool SFwallCore::SourcePortRangeCriteria::inner_match(SFwallCore::Packet* packet){
	if (!(packet->getProtocol() == TCP || packet->getProtocol() == UDP)) {
		return false;
	}

	if (!(packet->getSrcPort() >= startPort && packet->getSrcPort() <= endPort)) {
		return false;
	}

	return true;
}

std::string SFwallCore::DestinationPortRangeCriteria::key() const {
	return "destination.transport.portrange";
}

bool SFwallCore::DestinationPortRangeCriteria::inner_match(SFwallCore::Packet* packet){
	if (!(packet->getProtocol() == TCP || packet->getProtocol() == UDP)) {
		return false;
	}

	if (!(packet->getDstPort() >= startPort && packet->getDstPort() <= endPort)) {
		return false;
	}

	return true;
}

std::string SFwallCore::PortRangeCriteria::key() const {
	return "transport.portrange";
}

bool SFwallCore::PortRangeCriteria::inner_match(SFwallCore::Packet* packet, int direction){
	if (!(packet->getProtocol() == TCP || packet->getProtocol() == UDP)) {
		return false;
	}

	if(direction == DIRECTION_SOURCE){
		if (!(packet->getSrcPort() >= startPort && packet->getSrcPort() <= endPort)) {
			return false;
		}	
	}else if(direction == DIRECTION_DESTINATION){
		if (!(packet->getDstPort() >= startPort && packet->getDstPort() <= endPort)) {
			return false;
		}       

	}


	return true;
}


std::string SFwallCore::TimePeriodCriteria::key() const {
	return "timeperiod";
}

bool SFwallCore::TimePeriodCriteria::inner_match(SFwallCore::Packet* packet){
	if (!TimePeriodStore::checkMatch(periods, time(NULL))) {
		return false;
	}

	return true;
}

std::string SFwallCore::HttpContentType::key() const {
	return "application.http.contenttype";
}

bool SFwallCore::HttpContentType::inner_match(SFwallCore::Packet* packet){
	if (aliases.size() > 0) {
		ConnectionApplicationInfo *a = packet->getConnection()->getApplicationInfo();
		if (a->httpHeadersResponseParsed && a->http_contains(RESPONSE, HTTP_CONTENTTYPE)) {
			for (AliasPtr ua : aliases) {
				if (ua->search(a->http_get(RESPONSE, HTTP_CONTENTTYPE))) {
					return true;
				}
			}
		}
	}

	return false;
}

std::string SFwallCore::HttpUserAgent::key() const {
	return "application.http.useragent";
}

bool SFwallCore::HttpUserAgent::inner_match(SFwallCore::Packet* packet){
	if (aliases.size() > 0) {
		ConnectionApplicationInfo *a = packet->getConnection()->getApplicationInfo();
		if (a->httpHeadersParsed && a->http_contains(REQUEST, HTTP_USERAGENT)) {
			for (AliasPtr ua : aliases) {
				if (ua->search(a->http_get(REQUEST, HTTP_USERAGENT))) {
					return true;
				}
			}
		}
	}

	return false;
}

std::string SFwallCore::HttpContentTypeRegex::key() const {
	return "application.http.contenttype.regex";
}

std::string SFwallCore::IsHttp::key() const {
	return "application.http";
}

bool SFwallCore::IsHttp::inner_match(Packet* packet){
	if (packet->getConnection()->getApplicationInfo()->type == SFwallCore::ConnectionApplicationInfo::Classifier::HTTP) {
		return true;
	}
	return false;
}

std::string SFwallCore::HttpRequestRegexCriteria::key() const {
	return "application.http.request.regex";
}

bool SFwallCore::HttpRequestRegexCriteria::inner_match(SFwallCore::Packet* packet){
	if (packet->getConnection()->getApplicationInfo()->type != SFwallCore::ConnectionApplicationInfo::Classifier::HTTP) {
		return false;
	}

	ConnectionApplicationInfo *a = packet->getConnection()->getApplicationInfo();
	if (a->httpHeadersParsed && a->http_requestSet) {
		if (regex_matches(a->http_request)) {
			return true;
		}
	}

	return false;
}

bool SFwallCore::HttpContentTypeRegex::inner_match(SFwallCore::Packet* packet){
	if (packet->getConnection()->getApplicationInfo()->type != SFwallCore::ConnectionApplicationInfo::Classifier::HTTP) {
		return false;
	}

	ConnectionApplicationInfo *a = packet->getConnection()->getApplicationInfo();
	if (a->httpHeadersResponseParsed && a->http_contains(RESPONSE, HTTP_CONTENTTYPE)) {
		if (regex_matches(a->http_get(RESPONSE, HTTP_CONTENTTYPE))) {
			return true;
		}
	}

	return false;
}

std::string SFwallCore::HttpUserAgentRegex::key() const {
	return "application.http.useragent.regex";
}

bool SFwallCore::HttpUserAgentRegex::inner_match(SFwallCore::Packet* packet){
	if (packet->getConnection()->getApplicationInfo()->type != SFwallCore::ConnectionApplicationInfo::Classifier::HTTP) {
		return false;
	}

	ConnectionApplicationInfo *a = packet->getConnection()->getApplicationInfo();
	if (a->httpHeadersParsed && a->http_contains(REQUEST, HTTP_USERAGENT)) {
		if (regex_matches(a->http_get(REQUEST, HTTP_USERAGENT))) {
			return true;
		}
	}

	return false;
}

bool SFwallCore::HttpHostnameRegex::httphostname_matches(const std::string hostname){
	if (regex_matches(hostname)) {
		return true;
	}

	return false;
}

std::string SFwallCore::HttpHostnameRegex::key() const {
	return "application.http.hostname.regex";
}

void SFwallCore::HttpHostname::refresh(){
	cache.clear();
	if(alias && alias->source){
		alias->load();	
	}
}

std::string SFwallCore::HttpHostname::key() const {
	return "application.http.hostname";
}

void SFwallCore::HttpHostname::BanHitListItem::clear() {
	banned.clear();
}

int SFwallCore::HttpHostname::BanHitListItem::size() {
	return banned.size();
}

bool SFwallCore::HttpHostname::BanHitListItem::check(SFwallCore::ConnectionIp *ip) {
	if (ip->type() == IPV4) {
		ConnectionIpV4 *ipv4 = (ConnectionIpV4 *) ip;
		return banned.find(ipv4->getDstIp()) != banned.end();
	}

	return false;
}


void SFwallCore::HttpHostname::BanHitListItem::insert(SFwallCore::ConnectionIp *ip) {
	if (ip->type() == IPV4) {
		ConnectionIpV4 *ipv4 = (ConnectionIpV4 *) ip;
		banned.insert(ipv4->getDstIp());
	}
}


bool SFwallCore::HttpHostname::httphostname_matches(const std::string hostname){
	if(aliases.size() > 0){
		for (AliasPtr websiteList :  aliases) {
			if (websiteList->search(hostname)) {
				return true;
			}
		}
	}

	if(alias){
		if (alias->search(hostname)) {
			return true;
		}

	}
	return false;	
}

bool SFwallCore::HttpHostname::inner_match(SFwallCore::Packet* packet){
	if (System::getInstance()->getFirewall()->BLOCK_HTTP_FILTERING_TLS_SSLV3_RETRY == 1 && !packet->getConnection()->getApplicationInfo()->tls) {
		int block = System::getInstance()->getFirewall()->BLOCK_HTTP_FILTERING_TLS_SSLV3_RETRY;
		int dport = packet->getConnection()->getDestinationPort();
		int tls = packet->getConnection()->getApplicationInfo()->tls;
		ConnectionIp *ip = packet->getConnection()->getIp();

		if (block == 1 && dport == 443 && !tls && nonTlsBannedItems.check(ip)) {
			packet->getConnection()->getApplicationInfo()->http_ssl3IpMatch = true;
			return true;
		}
	}

	if (!packet->getConnection()->getApplicationInfo()->http_hostNameSet) {
		return false;
	}

	const string& host = packet->getConnection()->getApplicationInfo()->http_hostName;
	if(httphostname_matches(host)){
		if (System::getInstance()->getFirewall()->BLOCK_HTTP_FILTERING_TLS_SSLV3_RETRY == 1 && packet->getConnection()->getApplicationInfo()->tls) {
			if (Logger::instance()->criticalPathEnabled) {
				ConnectionIpV4 *ip = (ConnectionIpV4 *) packet->getConnection()->getIp();
				string hostname = packet->getConnection()->getApplicationInfo()->http_hostName;
				string ipaddr = IP4Addr::ip4AddrToString(ip->getDstIp());
				Logger::instance()->log("sphirewalld.firewall.webfiltering", INFO, "Adding dstip to ban list for website " + hostname + " under " + ipaddr + " %s");
			}

			if (System::getInstance()->getFirewall()->BLOCK_HTTP_FILTERING_TLS_SSLV3_RETRY_THRESHOLD < nonTlsBannedItems.size()) {
				Logger::instance()->log("sphirewalld.firewall.webfiltering", INFO, "Clearing nonTlsBannedItems as it has exceeded the max threhold");
				nonTlsBannedItems.clear();
			}

			nonTlsBannedItems.insert(packet->getConnection()->getIp());
		}

		return true;
	}

	return false;
}

std::string SFwallCore::SourceNetworkDeviceCriteria::key() const {
	return "source.device";
}

bool SFwallCore::SourceNetworkDeviceCriteria::inner_match(SFwallCore::Packet* packet){
	if(!devicePtr){
		return false;	
	}

	if(packet->getSourceDev() != devicePtr->ifid){
		return false;
	}

	return true;
}

std::string SFwallCore::NetworkDeviceCriteria::key() const {
	return "device";
}

bool SFwallCore::NetworkDeviceCriteria::inner_match(SFwallCore::Packet* packet, int direction){
	if(!devicePtr){
		return false;
	}

	if(direction == DIRECTION_SOURCE){
		if(packet->getSourceDev() != devicePtr->ifid){
			return false;
		}
	}else if(direction == DIRECTION_DESTINATION){
		if(packet->getDestDev() != devicePtr->ifid){
			return false;
		}
	}

	return true;
}

std::string SFwallCore::DestinationNetworkDeviceCriteria::key() const {
	return "destination.device";
}

bool SFwallCore::DestinationNetworkDeviceCriteria::inner_match(SFwallCore::Packet* packet){
	if(!devicePtr){
		return false;	
	}

	if(packet->getDestDev() != devicePtr->ifid){
		return false;
	}

	return true;
}

std::string SFwallCore::ProtocolCriteria::key() const {
	return "protocol";
}

bool SFwallCore::ProtocolCriteria::inner_match(SFwallCore::Packet* packet){
	if(packet->getProtocol() != type){
		return false;
	}

	return true;
}

std::string SFwallCore::UsersCriteria::key() const {
	return "source.user";
}

bool SFwallCore::UsersCriteria::inner_match(SFwallCore::Packet* packet){
	if(!packet->getUser()){
		return false;
	}

	if(users.find(packet->getUser()) == users.end()){
		return false;
	}

	return true;
}

std::string SFwallCore::MacAddressCriteria::key() const {
	return "source.mac";
}

bool SFwallCore::MacAddressCriteria::inner_match(SFwallCore::Packet* packet){
	if(macs.find(packet->getHw()) == macs.end()){
		return false;
	}

	return true;
}

std::string SFwallCore::SignatureCriteria::key() const {
	return "signature";
}

bool SFwallCore::SignatureCriteria::inner_match(SFwallCore::Packet* packet){
	if(packet->getConnection()->getApplicationInfo()->exclusive_signature){
		for(SignaturePtr sig : resolved_signatures){
			if(sig->is_category()){
				if(packet->getConnection()->getApplicationInfo()->exclusive_signature->resolved_category
						&& sig.get() == packet->getConnection()->getApplicationInfo()->exclusive_signature->resolved_category.get()){
					return true;
				}
			}else{
				if(packet->getConnection()->getApplicationInfo()->exclusive_signature.get() == sig.get()){
					return true;
				}
			}
		}
	}
	return false;
}

void SFwallCore::SignatureCriteria::refresh(){
	resolved_signatures.clear();
	for(std::string sig_type : unresolved_signatures){
		SignaturePtr signature = System::getInstance()->getFirewall()->signatureStore->get(sig_type);
		if(signature){
			signature->refresh();
			resolved_signatures.push_back(signature);
		}
	}
}

std::string SFwallCore::FingerprintCriteria::key() const {
	return "fingerprint";
}

bool SFwallCore::FingerprintCriteria::inner_match(SFwallCore::Packet* packet){
	if(packet->getHostDiscoveryServiceEntry() && packet->getHostDiscoveryServiceEntry()->device_fingerprint){
		if (std::find(fingerprints.begin(), fingerprints.end(),
					  packet->getHostDiscoveryServiceEntry()->device_fingerprint) != fingerprints.end()) {
			return true;
		}
	}
	return false;
}

void SFwallCore::FingerprintCriteria::refresh(){
	fingerprints.clear();
	for(std::string id : fingerprint_ids){
		FingerprintPtr finger = System::getInstance()->getFirewall()->fingerprint_store->get(id);
		if(finger){
			fingerprints.push_back(finger);
		}
	}
}

void SFwallCore::DestinationNetworkDeviceCriteria::refresh(){
	devicePtr = System::getInstance()->get_interface_manager()->get_interface(device);
}

void SFwallCore::SourceNetworkDeviceCriteria::refresh(){
	devicePtr = System::getInstance()->get_interface_manager()->get_interface(device);
}

void SFwallCore::NetworkDeviceCriteria::refresh(){
	devicePtr = System::getInstance()->get_interface_manager()->get_interface(device);
}

std::string SFwallCore::UserQuotaExceededCriteria::key() const {
	return "user.quota";
}

bool SFwallCore::UserQuotaExceededCriteria::inner_match(SFwallCore::Packet* packet){
	if(!packet->getUser()){
		return false;
	}

	return packet->getUser()->isQuotaExceeded();
}

std::string SFwallCore::GlobalQuotaExceededCriteria::key() const {
	return "global.quota";
}

bool SFwallCore::GlobalQuotaExceededCriteria::inner_match(SFwallCore::Packet* packet){
	return System::getInstance()->getQuotaManager()->isGlobalQuotaExceeded();
}

std::string SFwallCore::HostQuarantinedCriteria::key() const {
	return "source.quarantined";
}

bool SFwallCore::HostQuarantinedCriteria::inner_match(SFwallCore::Packet* packet){
	if(packet->getHostDiscoveryServiceEntry()){
		return packet->getHostDiscoveryServiceEntry()->quarantined;
	}
	return false;
}

std::string SFwallCore::AuthenticatedCriteria::key() const {
	return "session.active";
}

bool SFwallCore::AuthenticatedCriteria::inner_match(SFwallCore::Packet* packet){
	if(packet->getUser()){
		return true;
	}
	return false;
}

std::string SFwallCore::NotAuthenticatedCriteria::key() const {
	return "session.inactive";
}

bool SFwallCore::NotAuthenticatedCriteria::inner_match(SFwallCore::Packet* packet){
	if(!packet->getUser()){
		return true;
	}
	return false;
}

std::string SFwallCore::SourceMacAddressPoolCriteria::key() const {
	return "source.mac.pool";
}

bool SFwallCore::RegexCriteria::inner_match(SFwallCore::Packet* packet){
	std::string packetData = (const char *) packet->getTransport()->getApplication()->getInternal();
	boost::smatch what;
	return boost::regex_search(packetData, what, reg);
}

std::string SFwallCore::RegexCriteria::key() const {
	return "regex";
}

bool SFwallCore::SourceMacAddressPoolCriteria::inner_match(SFwallCore::Packet* packet){
	for(AliasPtr alias : aliases){
		if(alias->type() == MAC_ADDRESS_LIST){
			if(alias->search(packet->getHw())){
				return true;
			}
		}
	}

	return false;
}

void SFwallCore::GeoIPCriteriaBase::refresh(){
	Logger::instance()->log("sphirewalld.firewall.aliases", INFO, "Loading Maxmind GeoIP alias for criteria for country '%s'", country.c_str());
	HttpRequestWrapper *request = new HttpRequestWrapper("http://mirror.sphirewall.net/resources/GeoIPCountryWhois.csv", GET);
	IpRangeAlias* new_alias = new IpRangeAlias();
	try {
		int loaded_entries = 0;
		vector<string> items;
		split(request->execute(), '\n', items);

		for (string item : items) {
			vector<string> csv;
			split(item, ',', csv);
			std::string startIp = StringUtils::trim(csv[0], "\"");
			std::string endIp = StringUtils::trim(csv[1], "\"");

			std::string targetCountryCode = StringUtils::trim(csv[4], "\"");
			std::string targetCountry = StringUtils::trim(csv[5], "\"");

			if (targetCountry.compare(country) == 0 || targetCountryCode.compare(country) == 0) {
				stringstream ss;
				ss << startIp << "-" << endIp;
				new_alias->addEntry(ss.str());
				loaded_entries++;
			}
		}

		Logger::instance()->log("sphirewalld.firewall.aliases", INFO, "Loaded Maxmind GeoIP alias for criteria, %d entries added", loaded_entries);
		delete alias;
		alias = new_alias;
	}
	catch (const HttpRequestWrapperException &ex) {
		Logger::instance()->log("sphirewalld.firewall.aliases", ERROR, "Failed to load Maxmind GeoIP country for %s, reason %s", country.c_str(), ex.what());
		delete request;
		delete new_alias;
		return;
	}

	delete request;
}

std::string SFwallCore::GeoIPSourceCriteria::key() const{
	return "geoip.source";
}

bool SFwallCore::GeoIPSourceCriteria::inner_match(Packet* packet){
	if(alias){
		if(packet->type() == IPV4){
			if(alias->searchForNetworkMatch(((PacketV4*) packet)->getSrcIp())){
				return true;
			}
		}
	}       

	return false;
}

std::string SFwallCore::GeoIPDestinationCriteria::key() const{
	return "geoip.destination";
}

bool SFwallCore::GeoIPDestinationCriteria::inner_match(Packet* packet){
	if(alias){
		if(packet->type() == IPV4){
			if(alias->searchForNetworkMatch(((PacketV4*) packet)->getDstIp())){
				return true;
			}
		}
	}

	return false;
}

std::string SFwallCore::GeoIPCriteria::key() const{
	return "geoip";
}

bool SFwallCore::GeoIPCriteria::inner_match(Packet* packet, int direction){
	if(alias){
		if(packet->type() == IPV4){
			if(direction == DIRECTION_SOURCE){
				if(alias->searchForNetworkMatch(((PacketV4*) packet)->getSrcIp())){
					return true;
				}

			}else if(direction == DIRECTION_DESTINATION){
				if(alias->searchForNetworkMatch(((PacketV4*) packet)->getDstIp())){
					return true;
				}
			}
		}
	}

	return false;
}

std::string SFwallCore::SecurityGroupCriteria::key() const{
	return "securitygroup";
}

bool SFwallCore::SecurityGroupCriteria::inner_match(SFwallCore::Packet* packet, int direction){
	for(SecurityGroupPtr group : groups){
		if(group){
			for(SecurityGroupEntryPtr entry : group->entries){
				if(entry->matches(packet, direction)){
					return true;
				}
			}
		}
	}	

	return false;
}

void SFwallCore::SecurityGroupCriteria::refresh(){
	groups.clear();
	for(std::string id : security_group_ids){ 
		SecurityGroupPtr group = System::getInstance()->getFirewall()->security_groups_store->get_group(id);
		if(group){
			groups.push_back(group);
		}
	}
}


ObjectContainer* SFwallCore::CriteriaBuilder::serialize(Criteria* criteria){
	ObjectContainer* singleCriteria = new ObjectContainer(CREL);
	singleCriteria->put("type", new ObjectWrapper(criteria->key()));
	singleCriteria->put("negate", new ObjectWrapper(criteria->negate));

	ObjectContainer* conditions = NULL;
	if(criteria->key().compare("source.ipv4") == 0){
		SourceIpCriteria* rc = dynamic_cast<SourceIpCriteria*>(criteria);
		conditions = new ObjectContainer(CREL);
		conditions->put("ip", new ObjectWrapper((string) IP4Addr::ip4AddrToString(rc->ip)));
		conditions->put("mask", new ObjectWrapper((string) IP4Addr::ip4AddrToString(rc->mask)));
	}

	if(criteria->key().compare("destination.ipv4") == 0){
		DestinationIpCriteria* rc = dynamic_cast<DestinationIpCriteria*>(criteria);
		conditions = new ObjectContainer(CREL);
		conditions->put("ip", new ObjectWrapper((string) IP4Addr::ip4AddrToString(rc->ip)));
		conditions->put("mask", new ObjectWrapper((string) IP4Addr::ip4AddrToString(rc->mask)));
	}

	if(criteria->key().compare("ipv4") == 0){
		IpCriteria* rc = dynamic_cast<IpCriteria*>(criteria);
		conditions = new ObjectContainer(CREL);
		conditions->put("ip", new ObjectWrapper((string) IP4Addr::ip4AddrToString(rc->ip)));
		conditions->put("mask", new ObjectWrapper((string) IP4Addr::ip4AddrToString(rc->mask)));
	}

	if(criteria->key().compare("source.ipv6") == 0){
		SourceIp6Criteria* rc = dynamic_cast<SourceIp6Criteria*>(criteria);
		conditions = new ObjectContainer(CREL);
		conditions->put("ip", new ObjectWrapper((string) IP6Addr::toString(rc->addr)));
		conditions->put("cidr", new ObjectWrapper((double) rc->cidr));
	}

	if(criteria->key().compare("destination.ipv6") == 0){
		DestinationIp6Criteria* rc = dynamic_cast<DestinationIp6Criteria*>(criteria);
		conditions = new ObjectContainer(CREL);
		conditions->put("ip", new ObjectWrapper((string) IP6Addr::toString(rc->addr)));
		conditions->put("cidr", new ObjectWrapper((double) rc->cidr));
	}

	if(criteria->key().compare("ipv6") == 0){
		Ip6Criteria* rc = dynamic_cast<Ip6Criteria*>(criteria);
		conditions = new ObjectContainer(CREL);
		conditions->put("ip", new ObjectWrapper((string) IP6Addr::toString(rc->addr)));
		conditions->put("cidr", new ObjectWrapper((double) rc->cidr));
	}

	if(criteria->key().compare("group") == 0){
		GroupCriteria* rc = dynamic_cast<GroupCriteria*>(criteria);
		conditions = new ObjectContainer(CARRAY);

		for (GroupPtr  group : rc->groups) {
			conditions->put(new ObjectWrapper((double) group->getId()));
		}
	}

	if(criteria->key().compare("application.http.hostname.regex") == 0){
		HttpHostnameRegex* rc = dynamic_cast<HttpHostnameRegex*>(criteria);
		conditions = new ObjectContainer(CREL);
		conditions->put("expression", new ObjectWrapper((string) rc->expression));	
	}

	if(criteria->key().compare("application.http.useragent.regex") == 0){
		HttpUserAgentRegex* rc = dynamic_cast<HttpUserAgentRegex*>(criteria);
		conditions = new ObjectContainer(CREL);
		conditions->put("expression", new ObjectWrapper((string) rc->expression));
	}

	if(criteria->key().compare("application.http.contenttype.regex") == 0){
		HttpContentTypeRegex* rc = dynamic_cast<HttpContentTypeRegex*>(criteria);
		conditions = new ObjectContainer(CREL);
		conditions->put("expression", new ObjectWrapper((string) rc->expression));
	}

	if(criteria->key().compare("application.http.request.regex") == 0){
		HttpRequestRegexCriteria* rc = dynamic_cast<HttpRequestRegexCriteria*>(criteria);
		conditions = new ObjectContainer(CREL);
		conditions->put("expression", new ObjectWrapper((string) rc->expression));
	}

	/* Alias based criteria */
	if(criteria->key().compare("application.http.hostname") == 0){
		HttpHostname* rc = dynamic_cast<HttpHostname*>(criteria);

		if(!rc->alias){
			conditions = new ObjectContainer(CARRAY);
			for (AliasPtr alias : rc->aliases) {
				conditions ->put(new ObjectWrapper((std::string) alias->id));
			}
		}else{
			conditions = new ObjectContainer(CREL);
			if(rc->alias->source){
				conditions->put("source_url", new ObjectWrapper((string) rc->alias->source->detail));			
			}else{
				ObjectContainer* entries = new ObjectContainer(CARRAY);
				for(std::string entry : rc->alias->listEntries()){
					entries->put(new ObjectWrapper((string) entry));
				}

				conditions->put("source_entries", new ObjectWrapper(entries));			
			}
		}
	}

	if(criteria->key().compare("application.http.useragent.alias") == 0){
		HttpUserAgent* rc = dynamic_cast<HttpUserAgent*>(criteria);
		conditions = new ObjectContainer(CARRAY);

		for (AliasPtr alias : rc->aliases) {
			conditions->put(new ObjectWrapper((std::string) alias->id));
		}
	}

	if(criteria->key().compare("application.http.contenttype.alias") == 0){
		HttpContentType* rc = dynamic_cast<HttpContentType*>(criteria);
		conditions = new ObjectContainer(CARRAY);

		for (AliasPtr alias : rc->aliases) {
			conditions->put(new ObjectWrapper((std::string) alias->id));
		}
	}

	if(criteria->key().compare("source.ipv4.alias") == 0){
		SourceIpAliasCriteria* rc = dynamic_cast<SourceIpAliasCriteria*>(criteria);
		conditions = new ObjectContainer(CARRAY);

		if(rc->aliases.size() > 0){
			conditions = new ObjectContainer(CARRAY);
			for (AliasPtr alias : rc->aliases) {
				conditions->put(new ObjectWrapper((std::string) alias->id));
			}

		}else if(rc->alias){
			conditions = new ObjectContainer(CREL);
			conditions->put("source_url", new ObjectWrapper((std::string) rc->alias->source->detail));
			conditions->put("source_type", new ObjectWrapper((double) rc->alias->type()));
		}
	}

	if(criteria->key().compare("destination.ipv4.alias") == 0){
		DestinationIpAliasCriteria* rc = dynamic_cast<DestinationIpAliasCriteria*>(criteria);

		if(rc->aliases.size() > 0){
			conditions = new ObjectContainer(CARRAY);
			for (AliasPtr alias : rc->aliases) {
				conditions->put(new ObjectWrapper((std::string) alias->id));
			}

		}else if(rc->alias){
			conditions = new ObjectContainer(CREL);
			conditions->put("source_url", new ObjectWrapper((std::string) rc->alias->source->detail));
			conditions->put("source_type", new ObjectWrapper((double) rc->alias->type()));
		}
	}

	if(criteria->key().compare("ipv4.alias") == 0){
		IpAliasCriteria* rc = dynamic_cast<IpAliasCriteria*>(criteria);
		if(rc->aliases.size() > 0){
			conditions = new ObjectContainer(CARRAY);
			for (AliasPtr alias : rc->aliases) {
				conditions->put(new ObjectWrapper((std::string) alias->id));
			}

		}else if(rc->alias){
			conditions = new ObjectContainer(CREL);
			conditions->put("source_url", new ObjectWrapper((std::string) rc->alias->source->detail));
			conditions->put("source_type", new ObjectWrapper((double) rc->alias->type()));
		}
	}


	if(criteria->key().compare("timeperiod") == 0){
		TimePeriodCriteria* rc = dynamic_cast<TimePeriodCriteria*>(criteria);
		conditions = new ObjectContainer(CARRAY);

		for (TimePeriodPtr period : rc->periods) {
			conditions->put(new ObjectWrapper((string) period->uuid));
		}
	}

	if(criteria->key().compare("source.device") == 0){
		SourceNetworkDeviceCriteria* rc = dynamic_cast<SourceNetworkDeviceCriteria*>(criteria);
		conditions = new ObjectContainer(CREL);
		conditions->put("name", new ObjectWrapper((string) rc->device));
	}

	if(criteria->key().compare("destination.device") == 0){
		DestinationNetworkDeviceCriteria* rc = dynamic_cast<DestinationNetworkDeviceCriteria*>(criteria);
		conditions = new ObjectContainer(CREL);
		conditions->put("name", new ObjectWrapper((string) rc->device));
	}

	if(criteria->key().compare("device") == 0){
		NetworkDeviceCriteria* rc = dynamic_cast<NetworkDeviceCriteria*>(criteria);
		conditions = new ObjectContainer(CREL);
		conditions->put("name", new ObjectWrapper((string) rc->device));
	}

	if(criteria->key().compare("destination.transport.port") == 0){
		DestinationPortCriteria* rc = dynamic_cast<DestinationPortCriteria*>(criteria);
		conditions = new ObjectContainer(CARRAY);
		for(unsigned int port : rc->ports){
			conditions->put(new ObjectWrapper((double) port));
		}
	}

	if(criteria->key().compare("source.transport.port") == 0){
		SourcePortCriteria* rc = dynamic_cast<SourcePortCriteria*>(criteria);
		conditions = new ObjectContainer(CARRAY);
		for(unsigned int port : rc->ports){
			conditions->put(new ObjectWrapper((double) port));
		}
	}

	if(criteria->key().compare("transport.port") == 0){
		PortCriteria* rc = dynamic_cast<PortCriteria*>(criteria);
		conditions = new ObjectContainer(CARRAY);
		for(unsigned int port : rc->ports){
			conditions->put(new ObjectWrapper((double) port));
		}
	}

	if(criteria->key().compare("destination.transport.portrange") == 0){
		DestinationPortRangeCriteria* rc = dynamic_cast<DestinationPortRangeCriteria*>(criteria);
		conditions= new ObjectContainer(CREL);
		conditions->put("startPort", new ObjectWrapper((double) rc->startPort));
		conditions->put("endPort", new ObjectWrapper((double) rc->endPort));
	}

	if(criteria->key().compare("source.transport.portrange") == 0){
		SourcePortRangeCriteria* rc = dynamic_cast<SourcePortRangeCriteria*>(criteria);
		conditions= new ObjectContainer(CREL);
		conditions->put("startPort", new ObjectWrapper((double) rc->startPort));
		conditions->put("endPort", new ObjectWrapper((double) rc->endPort));
	}

	if(criteria->key().compare("transport.portrange") == 0){
		PortRangeCriteria* rc = dynamic_cast<PortRangeCriteria*>(criteria);
		conditions= new ObjectContainer(CREL);
		conditions->put("startPort", new ObjectWrapper((double) rc->startPort));
		conditions->put("endPort", new ObjectWrapper((double) rc->endPort));
	}

	if(criteria->key().compare("protocol") == 0){
		ProtocolCriteria* rc = dynamic_cast<ProtocolCriteria*>(criteria);
		conditions= new ObjectContainer(CREL);
		conditions->put("type", new ObjectWrapper((double) rc->type));
	}

	if(criteria->key().compare("source.user") == 0){
		UsersCriteria* rc = dynamic_cast<UsersCriteria*>(criteria);
		conditions= new ObjectContainer(CARRAY);
		for(UserPtr user: rc->users){
			conditions->put(new ObjectWrapper((string) user->getUserName()));
		}
	}

	if(criteria->key().compare("source.mac") == 0){
		MacAddressCriteria* rc = dynamic_cast<MacAddressCriteria*>(criteria);
		conditions= new ObjectContainer(CARRAY);
		for(std::string i : rc->macs){
			conditions->put(new ObjectWrapper((string) i));
		}
	}

	if(criteria->key().compare("signature") == 0){
		SignatureCriteria* rc = dynamic_cast<SignatureCriteria*>(criteria);
		conditions= new ObjectContainer(CARRAY);
		for(std::string sig : rc->unresolved_signatures){
			conditions->put(new ObjectWrapper((std::string) sig));
		}
	}

	if(criteria->key().compare("fingerprint") == 0){
		FingerprintCriteria* rc = dynamic_cast<FingerprintCriteria*>(criteria);
		conditions= new ObjectContainer(CARRAY);
		for(FingerprintPtr sig : rc->fingerprints){
			conditions->put(new ObjectWrapper((std::string) sig->id()));
		}
	}

	if(criteria->key().compare("user.quota") == 0){
		conditions= new ObjectContainer(CREL);
	}

	if(criteria->key().compare("global.quota") == 0){
		conditions= new ObjectContainer(CREL);
	}

	if(criteria->key().compare("source.quarantined") == 0){
		conditions= new ObjectContainer(CREL);
	}

	if(criteria->key().compare("application.http") == 0){
		conditions= new ObjectContainer(CREL);
	}

	if(criteria->key().compare("session.active") == 0){
		conditions= new ObjectContainer(CREL);
	}

	if(criteria->key().compare("session.inactive") == 0){
		conditions= new ObjectContainer(CREL);
	}

	if(criteria->key().compare("geoip.destination") == 0){
		GeoIPDestinationCriteria* rc = dynamic_cast<GeoIPDestinationCriteria*>(criteria);
		conditions= new ObjectContainer(CREL);
		conditions->put("country", new ObjectWrapper((std::string) rc->country));
	}

	if(criteria->key().compare("geoip.source") == 0){
		GeoIPSourceCriteria* rc = dynamic_cast<GeoIPSourceCriteria*>(criteria);
		conditions= new ObjectContainer(CREL);
		conditions->put("country", new ObjectWrapper((std::string) rc->country));
	}

	if(criteria->key().compare("geoip") == 0){
		GeoIPCriteria* rc = dynamic_cast<GeoIPCriteria*>(criteria);
		conditions= new ObjectContainer(CREL);
		conditions->put("country", new ObjectWrapper((std::string) rc->country));
	}

	if(criteria->key().compare("source.mac.pool") == 0){
		SourceMacAddressPoolCriteria* rc = dynamic_cast<SourceMacAddressPoolCriteria*>(criteria);
		conditions = new ObjectContainer(CARRAY);

		for (AliasPtr alias : rc->aliases) {
			conditions->put(new ObjectWrapper((std::string) alias->id));
		}
	}

	if(criteria->key().compare("regex") == 0){
		RegexCriteria* rc = dynamic_cast<RegexCriteria*>(criteria);
		conditions= new ObjectContainer(CREL);
		conditions->put("expression", new ObjectWrapper((std::string) rc->expression));
	}

	if(criteria->key().compare("securitygroup") == 0){
		SecurityGroupCriteria* rc = dynamic_cast<SecurityGroupCriteria*>(criteria);
		conditions = new ObjectContainer(CARRAY);

		for (SecurityGroupPtr group : rc->groups) {
			conditions->put(new ObjectWrapper((std::string) group->id));
		}
	}

	if(criteria->key().compare("dns.destination") == 0){
		StatefulDnsDestinationCriteria* rc = dynamic_cast<StatefulDnsDestinationCriteria*>(criteria);
		conditions= new ObjectContainer(CREL);
		conditions->put("expression", new ObjectWrapper((std::string) rc->target_hostname));
	}


	singleCriteria->put("conditions", new ObjectWrapper(conditions));
	return singleCriteria;
}

SFwallCore::Criteria* SFwallCore::CriteriaBuilder::parse(ObjectContainer* input){
	Criteria* criteria_ret = NULL;
	if(!input->has("conditions") || !input->has("type")){
		return criteria_ret;
	}

	ObjectContainer* options = input->get("conditions")->container();
	string type = input->get("type")->string();	

	if(type.compare("source.ipv4") == 0){
		SourceIpCriteria* ret = new SourceIpCriteria();
		ret->ip = IP4Addr::stringToIP4Addr(options->get("ip")->string());
		ret->mask= IP4Addr::stringToIP4Addr(options->get("mask")->string());
		criteria_ret = ret;
	}

	if(type.compare("ipv4") == 0){
		IpCriteria* ret = new IpCriteria();
		ret->ip = IP4Addr::stringToIP4Addr(options->get("ip")->string());
		ret->mask= IP4Addr::stringToIP4Addr(options->get("mask")->string());
		criteria_ret = ret;
	}

	if(type.compare("user.quota") == 0){
		criteria_ret = new UserQuotaExceededCriteria();
	}

	if(type.compare("global.quota") == 0){
		criteria_ret = new GlobalQuotaExceededCriteria();
	}

	if(type.compare("application.http") == 0){
		criteria_ret = new IsHttp();
	}

	if(type.compare("destination.ipv4") == 0){
		DestinationIpCriteria* ret = new DestinationIpCriteria();
		ret->ip = IP4Addr::stringToIP4Addr(options->get("ip")->string());
		ret->mask= IP4Addr::stringToIP4Addr(options->get("mask")->string());
		criteria_ret = ret;
	}

	if(type.compare("source.ipv6") == 0){
		SourceIp6Criteria* ret = new SourceIp6Criteria();
		IP6Addr::fromString(ret->addr, options->get("ip")->string());
		ret->cidr = options->get("cidr")->number();
		criteria_ret = ret;
	}

	if(type.compare("destination.ipv6") == 0){
		DestinationIp6Criteria* ret = new DestinationIp6Criteria();
		IP6Addr::fromString(ret->addr, options->get("ip")->string());
		ret->cidr = options->get("cidr")->number();
		criteria_ret = ret;
	}

	if(type.compare("ipv6") == 0){
		Ip6Criteria* ret = new Ip6Criteria();
		IP6Addr::fromString(ret->addr, options->get("ip")->string());
		ret->cidr = options->get("cidr")->number();
		criteria_ret = ret;
	}

	if(type.compare("source.device") == 0){
		SourceNetworkDeviceCriteria* ret = new SourceNetworkDeviceCriteria();
		ret->device = options->get("name")->string();
		criteria_ret = ret;
	}

	if(type.compare("destination.device") == 0){
		DestinationNetworkDeviceCriteria* ret = new DestinationNetworkDeviceCriteria();
		ret->device = options->get("name")->string();
		criteria_ret = ret;
	}

	if(type.compare("device") == 0){
		NetworkDeviceCriteria* ret = new NetworkDeviceCriteria();
		ret->device = options->get("name")->string();
		criteria_ret = ret;
	}

	if(type.compare("group") == 0){
		GroupCriteria* ret = new GroupCriteria();

		for (int y = 0; y < options->size(); y++) {
			GroupPtr g = System::getInstance()->getGroupDb()->getGroup(options->get(y)->number());
			if (g) ret->groups.push_back(g);
		}
		criteria_ret = ret;
	}

	if(type.compare("application.http.hostname") == 0){
		HttpHostname* ret = new HttpHostname();

		if(options->getType() == CREL){
			ObjectContainer* loading_opts = options;
			if(loading_opts->has("source_url")){
				ret->alias = AliasPtr(new WebsiteListAlias());
				ret->alias->source = new HttpFileSource();
				ret->alias->source->detail = loading_opts->get("source_url")->string();

			}else if(loading_opts->has("source_entries")){
				ret->alias = AliasPtr(new WebsiteListAlias());
				ObjectContainer* source_entries = loading_opts->get("source_entries")->container();
				for(int z = 0; z < source_entries->size(); z++){
					ret->alias->addEntry(source_entries->get(z)->string());
				}
			}

		}else{
			for (int y = 0; y < options->size(); y++) {
				AliasPtr alias = System::getInstance()->getFirewall()->aliases->get(options->get(y)->string());
				if (alias) ret->aliases.push_back(alias);
			}
		}
		criteria_ret = ret;
	}

	if(type.compare("application.http.useragent.alias") == 0){
		HttpUserAgent* ret = new HttpUserAgent();

		for (int y = 0; y < options->size(); y++) {
			AliasPtr alias = System::getInstance()->getFirewall()->aliases->get(options->get(y)->string());
			if (alias) ret->aliases.push_back(alias);
		}
		criteria_ret = ret;
	}

	if(type.compare("application.http.contenttype.alias") == 0){
		HttpContentType* ret = new HttpContentType();

		for (int y = 0; y < options->size(); y++) {
			AliasPtr alias = System::getInstance()->getFirewall()->aliases->get(options->get(y)->string());
			if (alias) ret->aliases.push_back(alias);
		}
		criteria_ret = ret;
	}

	if(type.compare("application.http.hostname.regex") == 0){
		HttpHostnameRegex* ret = new HttpHostnameRegex();
		ret->expression = options->get("expression")->string();
		criteria_ret = ret;
	}

	if(type.compare("application.http.useragent.regex") == 0){
		HttpUserAgentRegex* ret = new HttpUserAgentRegex();
		ret->expression = options->get("expression")->string();
		criteria_ret = ret;
	}

	if(type.compare("application.http.contenttype.regex") == 0){
		HttpContentTypeRegex* ret = new HttpContentTypeRegex();
		ret->expression = options->get("expression")->string();
		criteria_ret = ret;
	}

	if(type.compare("application.http.request.regex") == 0){
		HttpRequestRegexCriteria* ret = new HttpRequestRegexCriteria();
		ret->expression = options->get("expression")->string();
		criteria_ret = ret;
	}

	if(type.compare("source.ipv4.alias") == 0){
		SourceIpAliasCriteria* ret = new SourceIpAliasCriteria();
		if(options->getType() == CREL){
			int source_type = options->get("source_type")->number();
			std::string source_url = options->get("source_url")->string();

			if(source_type == IP_RANGE){
				ret->alias = AliasPtr(new IpRangeAlias());
				ret->alias->source = new HttpFileSource();
				ret->alias->source->detail = source_url;
			}else if(source_type == IP_SUBNET){
				ret->alias = AliasPtr(new IpSubnetAlias());
				ret->alias->source = new HttpFileSource();
				ret->alias->source->detail = source_url;
			}
		}else {
			for (int y = 0; y < options->size(); y++) {
				AliasPtr alias = System::getInstance()->getFirewall()->aliases->get(options->get(y)->string());
				if (alias) ret->aliases.push_back(alias);
			}
		}
		criteria_ret = ret;
	}

	if(type.compare("destination.ipv4.alias") == 0){
		DestinationIpAliasCriteria* ret = new DestinationIpAliasCriteria();
		if(options->getType() == CREL){
			int source_type = options->get("source_type")->number();
			std::string source_url = options->get("source_url")->string();

			if(source_type == IP_RANGE){
				ret->alias = AliasPtr(new IpRangeAlias());
				ret->alias->source = new HttpFileSource();
				ret->alias->source->detail = source_url;
			}else if(source_type == IP_SUBNET){
				ret->alias = AliasPtr(new IpSubnetAlias());
				ret->alias->source = new HttpFileSource();
				ret->alias->source->detail = source_url;
			}
		}else {

			for (int y = 0; y < options->size(); y++) {
				AliasPtr alias = System::getInstance()->getFirewall()->aliases->get(options->get(y)->string());
				if (alias) ret->aliases.push_back(alias);
			}
		}
		criteria_ret = ret;
	}

	if(type.compare("ipv4.alias") == 0){
		IpAliasCriteria* ret = new IpAliasCriteria();
		if(options->getType() == CREL){
			int source_type = options->get("source_type")->number();
			std::string source_url = options->get("source_url")->string();

			if(source_type == IP_RANGE){
				ret->alias = AliasPtr(new IpRangeAlias());
				ret->alias->source = new HttpFileSource();
				ret->alias->source->detail = source_url;
			}else if(source_type == IP_SUBNET){
				ret->alias = AliasPtr(new IpSubnetAlias());
				ret->alias->source = new HttpFileSource();
				ret->alias->source->detail = source_url;
			}
		}else {
			for (int y = 0; y < options->size(); y++) {
				AliasPtr alias = System::getInstance()->getFirewall()->aliases->get(options->get(y)->string());
				if (alias) ret->aliases.push_back(alias);
			}
		}
		criteria_ret = ret;
	}

	if(type.compare("timeperiod") == 0){
		TimePeriodCriteria* ret = new TimePeriodCriteria();

		for (int y = 0; y < options->size(); y++) {
			TimePeriodPtr target = System::getInstance()->periods->getById(options->get(y)->string());
			if(target){
				ret->periods.push_back(target);
			}
		}
		criteria_ret = ret;
	}

	if(type.compare("destination.transport.port") == 0){
		DestinationPortCriteria* ret = new DestinationPortCriteria();

		for (int y = 0; y < options->size(); y++) {
			ret->ports.insert(options->get(y)->number());
		}
		criteria_ret = ret;
	}

	if(type.compare("source.transport.port") == 0){
		SourcePortCriteria* ret = new SourcePortCriteria();

		for (int y = 0; y < options->size(); y++) {
			ret->ports.insert(options->get(y)->number());
		}
		criteria_ret = ret;
	}

	if(type.compare("transport.port") == 0){
		PortCriteria* ret = new PortCriteria();

		for (int y = 0; y < options->size(); y++) {
			ret->ports.insert(options->get(y)->number());
		}
		criteria_ret = ret;
	}

	if(type.compare("source.transport.portrange") == 0){
		SourcePortRangeCriteria* ret = new SourcePortRangeCriteria();
		ret->startPort = options->get("startPort")->number();
		ret->endPort = options->get("endPort")->number();
		criteria_ret = ret;
	}

	if(type.compare("destination.transport.portrange") == 0){
		DestinationPortRangeCriteria* ret = new DestinationPortRangeCriteria();
		ret->startPort = options->get("startPort")->number();
		ret->endPort = options->get("endPort")->number();
		criteria_ret = ret;
	}

	if(type.compare("transport.portrange") == 0){
		PortRangeCriteria* ret = new PortRangeCriteria();
		ret->startPort = options->get("startPort")->number();
		ret->endPort = options->get("endPort")->number();
		criteria_ret = ret;
	}

	if(type.compare("protocol") == 0){
		ProtocolCriteria* ret = new ProtocolCriteria();
		ret->type = (proto) options->get("type")->number();
		criteria_ret = ret;
	}

	if(type.compare("source.user") == 0){
		UsersCriteria* ret = new UsersCriteria();
		for (int y = 0; y < options->size(); y++) {
			ret->users.insert(System::getInstance()->getUserDb()->getUser(options->get(y)->string()));
		}
		criteria_ret = ret;
	}

	if(type.compare("source.mac") == 0){
		MacAddressCriteria* ret = new MacAddressCriteria();
		for (int y = 0; y < options->size(); y++) {
			ret->macs.insert(options->get(y)->string());
		}
		criteria_ret = ret;
	}

	if(type.compare("signature") == 0){
		SignatureCriteria* ret = new SignatureCriteria();

		if(options->getType() == CARRAY){
			for (int y = 0; y < options->size(); y++) {
				ret->unresolved_signatures.push_back(options->get(y)->string());
			}
		}else{
			ret->unresolved_signatures.push_back(options->get("type")->string());
		}
		criteria_ret = ret;
	}

	if(type.compare("fingerprint") == 0){
		FingerprintCriteria* ret = new FingerprintCriteria();
		for (int y = 0; y < options->size(); y++) {
			ret->fingerprint_ids.push_back(options->get(y)->string());
		}
		criteria_ret = ret;
	}

	if(type.compare("source.quarantined") == 0){
		HostQuarantinedCriteria* ret = new HostQuarantinedCriteria();
		criteria_ret = ret;
	}

	if(type.compare("session.active") == 0){
		AuthenticatedCriteria* ret = new AuthenticatedCriteria();
		criteria_ret = ret;
	}

	if(type.compare("session.inactive") == 0){
		NotAuthenticatedCriteria* ret = new NotAuthenticatedCriteria();
		criteria_ret = ret;
	}

	if(type.compare("geoip.destination") == 0){
		GeoIPDestinationCriteria* ret = new GeoIPDestinationCriteria();
		ret->country = options->get("country")->string();
		criteria_ret = ret;
	}

	if(type.compare("geoip.source") == 0){
		GeoIPSourceCriteria* ret = new GeoIPSourceCriteria();
		ret->country = options->get("country")->string();
		criteria_ret = ret;
	}

	if(type.compare("geoip") == 0){
		GeoIPCriteria* ret = new GeoIPCriteria();
		ret->country = options->get("country")->string();
		criteria_ret = ret;
	}

	if(type.compare("source.mac.pool") == 0){
		SourceMacAddressPoolCriteria* ret = new SourceMacAddressPoolCriteria();
		for (int y = 0; y < options->size(); y++) {
			AliasPtr alias = System::getInstance()->getFirewall()->aliases->get(options->get(y)->string());
			if (alias){
				ret->aliases.push_back(alias);
			}
		}
		criteria_ret = ret;
	}

	if(type.compare("regex") == 0){
		RegexCriteria* ret = new RegexCriteria();
		ret->expression = options->get("expression")->string();
		criteria_ret = ret;
	}

	if(criteria_ret){
		criteria_ret->refresh();
		if(input->has("negate")){
			criteria_ret->negate = input->get("negate")->boolean();
		}
	}

	if(type.compare("securitygroup") == 0){
		SecurityGroupCriteria* ret = new SecurityGroupCriteria();
		for (int y = 0; y < options->size(); y++) {
			ret->security_group_ids.push_back(options->get(y)->string());
		}
		criteria_ret = ret;
	}

	if(type.compare("dns.destination") == 0){
		StatefulDnsDestinationCriteria* ret = new StatefulDnsDestinationCriteria();
		ret->target_hostname=  options->get("expression")->string();	
		criteria_ret = ret;
	}

	return criteria_ret;
}

SFwallCore::CriteriaSet::~CriteriaSet(){
        for(Criteria* c : criteria){
                delete c;
        }
}

