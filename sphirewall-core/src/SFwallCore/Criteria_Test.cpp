#include <iostream>
#include <gtest/gtest.h>
#include "SFwallCore/TestFactory.h"
#include "Utils/IP4Addr.h"
#include "Utils/IP6Addr.h"
#include "SFwallCore/Packet.h"
#include "SFwallCore/Criteria.h"
#include "SFwallCore/Connection.h"
#include "Utils/Utils.h"
#include "test.h"

using namespace std;
using namespace SFwallCore;

TEST(Criteria, source_ip_matches_negated_no_match) {
        SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("192.168.1.1", "2.1.33.1", 12332, 80, "");

	SourceIpCriteria criteria;
	criteria.ip = IP4Addr::stringToIP4Addr("192.168.1.1");
	criteria.mask = IP4Addr::stringToIP4Addr("255.255.255.255");
	criteria.negate = true;
	EXPECT_FALSE(criteria.match(packet, DIRECTION_NA));       	
}

TEST(Criteria, source_ip_matches_negated) {
        SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("192.168.1.7", "2.1.33.1", 12332, 80, "");

	SourceIpCriteria criteria;
	criteria.ip = IP4Addr::stringToIP4Addr("192.168.1.1");
	criteria.mask = IP4Addr::stringToIP4Addr("255.255.255.255");
	criteria.negate = true;
	EXPECT_TRUE(criteria.match(packet, DIRECTION_NA));       	
}

TEST(Criteria, source_ip_matches) {
        SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("192.168.1.1", "2.1.33.1", 12332, 80, "");

	SourceIpCriteria criteria;
	criteria.ip = IP4Addr::stringToIP4Addr("192.168.1.1");
	criteria.mask = IP4Addr::stringToIP4Addr("255.255.255.255");
	EXPECT_TRUE(criteria.match(packet, DIRECTION_NA));       	
}

TEST(Criteria, source_ip_mismatch) {
        SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("192.168.1.2", "2.1.33.1", 12332, 80, "");

	SourceIpCriteria criteria;
	criteria.ip = IP4Addr::stringToIP4Addr("192.168.1.1");
	criteria.mask = IP4Addr::stringToIP4Addr("255.255.255.255");
	EXPECT_FALSE(criteria.match(packet, DIRECTION_NA));       	
}

TEST(Criteria, destination_ip_matches) {
        SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("192.168.1.1", "2.1.33.1", 12332, 80, "");

	DestinationIpCriteria criteria;
	criteria.ip = IP4Addr::stringToIP4Addr("2.1.33.1");
	criteria.mask = IP4Addr::stringToIP4Addr("255.255.255.255");
	EXPECT_TRUE(criteria.match(packet, DIRECTION_NA));       	
}

TEST(Criteria, destination_ip_mismatch) {
        SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("192.168.1.2", "2.1.33.1", 12332, 80, "");

	DestinationIpCriteria criteria;
	criteria.ip = IP4Addr::stringToIP4Addr("192.168.1.1");
	criteria.mask = IP4Addr::stringToIP4Addr("255.255.255.255");
	EXPECT_FALSE(criteria.match(packet, DIRECTION_NA));       	
}

TEST(Criteria, source_ip6_matches) {
        Packet *packet = PacketBuilder::createTcpV6("2607:f0d0:2001:b::10", "2443:f0d0:2001:b::2", 10, 20);

	SourceIp6Criteria criteria;
        criteria.cidr = 64;
        IP6Addr::fromString(criteria.addr, "2607:f0d0:2001:b::10");
	EXPECT_TRUE(criteria.match(packet, DIRECTION_NA));       	
}

TEST(Criteria, source_ip6_mismatch) {
        Packet *packet = PacketBuilder::createTcpV6("2607:f0d0:2001:d::10", "2443:f0d0:2001:b::2", 10, 20);

	SourceIp6Criteria criteria;
        criteria.cidr = 128;
        IP6Addr::fromString(criteria.addr, "2607:f0d0:2001:b::10");
	EXPECT_FALSE(criteria.match(packet, DIRECTION_NA));       	
}

TEST(Criteria, destination_ip6_matches) {
        Packet *packet = PacketBuilder::createTcpV6("2607:f0d0:2001:b::10", "2443:f0d0:2001:b::2", 10, 20);

	DestinationIp6Criteria criteria;
        criteria.cidr = 64;
        IP6Addr::fromString(criteria.addr, "2443:f0d0:2001:b::2");
	EXPECT_TRUE(criteria.match(packet, DIRECTION_NA));       	
}

TEST(Criteria, destination_ip6_mismatch) {
        Packet *packet = PacketBuilder::createTcpV6("2607:f0d0:2001:d::10", "2443:f0d0:2001:b::2", 10, 20);

	DestinationIp6Criteria criteria;
        criteria.cidr = 128;
        IP6Addr::fromString(criteria.addr, "2222:f0d0:2001:b::10");
	EXPECT_FALSE(criteria.match(packet, DIRECTION_NA));       	
}

TEST(Criteria, group_matches) {
        MockFactory *mocks = new MockFactory();
	Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("192.168.1.2", "2.1.33.1", 12332, 80, "");
        SFwallCore::TcpConnection *connection = new SFwallCore::TcpConnection(packet);
	packet->setConnection(connection);
	
        UserPtr user(new User());
        GroupPtr group(new Group());
	group->setId(10);
        user->addGroup(group);

	HostPtr host = HostPtr(new Host());
	host->authenticated_user = user;
	connection->setHostDiscoveryServiceEntry(host);

	GroupCriteria criteria;
	criteria.groups.push_back(group);
	EXPECT_TRUE(criteria.match(packet, DIRECTION_SOURCE));       	
}

TEST(Criteria, group_mismatch) {
        MockFactory *mocks = new MockFactory();
	Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("192.168.1.2", "2.1.33.1", 12332, 80, "");
        SFwallCore::TcpConnection *connection = new SFwallCore::TcpConnection(packet);
	packet->setConnection(connection);
	
        //SessionDb *sessionDb = mocks->givenSessionDb();
	//sessionDb->setConfigurationManager(new ConfigurationManager());
        UserPtr user(new User());
        GroupPtr group(new Group());
	group->setId(10);
        user->addGroup(group);

        HostPtr host = HostPtr(new Host());
        host->authenticated_user = user;
        connection->setHostDiscoveryServiceEntry(host);

	GroupPtr other(new Group());
	other->setId(1);
	GroupCriteria criteria;
	criteria.groups.push_back(other);
	EXPECT_FALSE(criteria.match(packet, DIRECTION_SOURCE));       	
}

TEST(Criteria, source_alias_matches) {
        SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("192.168.1.1", "2.1.33.1", 12332, 80, "");

        SourceIpAliasCriteria criteria;
	SFwallCore::IpRangeAlias *alias = new SFwallCore::IpRangeAlias( {"10.1.1.1-10.1.1.10", "192.168.1.1-192.168.1.1"});
	criteria.aliases.push_back(AliasPtr(alias));
	
        EXPECT_TRUE(criteria.match(packet, DIRECTION_NA));
}

TEST(Criteria, source_alias_mismatches) {
        SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("192.168.1.100", "2.1.33.1", 12332, 80, "");

        SourceIpAliasCriteria criteria;
	SFwallCore::IpRangeAlias *alias = new SFwallCore::IpRangeAlias( {"10.1.1.1-10.1.1.10", "192.168.1.1-192.168.1.1"});
	criteria.aliases.push_back(AliasPtr(alias));
	
        EXPECT_FALSE(criteria.match(packet, DIRECTION_NA));
}

TEST(Criteria, source_single_alias_matches) {
        SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("192.168.1.1", "2.1.33.1", 12332, 80, "");

        SourceIpAliasCriteria criteria;
	SFwallCore::IpRangeAlias *alias = new SFwallCore::IpRangeAlias( {"10.1.1.1-10.1.1.10", "192.168.1.1-192.168.1.1"});
	criteria.alias = AliasPtr(alias);	
        EXPECT_TRUE(criteria.match(packet, DIRECTION_NA));
}

TEST(Criteria, source_single_alias_mismatches) {
        SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("192.168.1.100", "2.1.33.1", 12332, 80, "");

        SourceIpAliasCriteria criteria;
	SFwallCore::IpRangeAlias *alias = new SFwallCore::IpRangeAlias( {"10.1.1.1-10.1.1.10", "192.168.1.1-192.168.1.1"});
	criteria.alias = AliasPtr(alias);	
        EXPECT_FALSE(criteria.match(packet, DIRECTION_NA));
}

TEST(Criteria, destination_single_alias_matches) {
        SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("192.168.1.1", "10.1.1.1", 12332, 80, "");

        DestinationIpAliasCriteria criteria;
	SFwallCore::IpRangeAlias *alias = new SFwallCore::IpRangeAlias( {"10.1.1.1-10.1.1.10", "192.168.1.1-192.168.1.1"});
        criteria.alias = AliasPtr(alias);
	
        EXPECT_TRUE(criteria.match(packet, DIRECTION_NA));
}

TEST(Criteria, destination_single_alias_mismatches) {
        SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("192.168.1.100", "2.1.33.1", 12332, 80, "");

        DestinationIpAliasCriteria criteria;
	SFwallCore::IpRangeAlias *alias = new SFwallCore::IpRangeAlias( {"10.1.1.1-10.1.1.10", "192.168.1.1-192.168.1.1"});
        criteria.alias = AliasPtr(alias);
	
        EXPECT_FALSE(criteria.match(packet, DIRECTION_NA));
}

TEST(Criteria, http_host_matches) {
        SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("192.168.1.1", "10.1.1.1", 12332, 80, "");
        SFwallCore::TcpConnection *connection = new SFwallCore::TcpConnection(packet);
        packet->setConnection(connection);
	connection->getApplicationInfo()->http_hostNameSet = true;
	connection->getApplicationInfo()->http_hostName = "blah.google.com";

        HttpHostname criteria;
	SFwallCore::WebsiteListAlias *alias = new SFwallCore::WebsiteListAlias( {"yahoo.com", "google.com"});
	criteria.aliases.push_back(AliasPtr(alias));

	EXPECT_TRUE(criteria.match(packet, DIRECTION_SOURCE));
}

TEST(Criteria, http_host_mismatches) {
        SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("192.168.1.1", "10.1.1.1", 12332, 80, "");
        SFwallCore::TcpConnection *connection = new SFwallCore::TcpConnection(packet);
        packet->setConnection(connection);
	connection->getApplicationInfo()->http_hostNameSet = true;
	connection->getApplicationInfo()->http_hostName = "fuck.com";

        HttpHostname criteria;
	SFwallCore::WebsiteListAlias *alias = new SFwallCore::WebsiteListAlias( {"yahoo.com", "google.com"});
	criteria.aliases.push_back(AliasPtr(alias));
	
        EXPECT_FALSE(criteria.match(packet, DIRECTION_SOURCE));
}

TEST(Criteria, http_host_single_matches) {
        SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("192.168.1.1", "10.1.1.1", 12332, 80, "");
        SFwallCore::TcpConnection *connection = new SFwallCore::TcpConnection(packet);
        packet->setConnection(connection);
	connection->getApplicationInfo()->http_hostNameSet = true;
	connection->getApplicationInfo()->http_hostName = "blah.google.com";

        HttpHostname criteria;
	criteria.alias = AliasPtr(new SFwallCore::WebsiteListAlias( {"yahoo.com", "google.com"}));
	EXPECT_TRUE(criteria.match(packet, DIRECTION_SOURCE));
}

TEST(Criteria, http_host_single_mismatches) {
        SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("192.168.1.1", "10.1.1.1", 12332, 80, "");
        SFwallCore::TcpConnection *connection = new SFwallCore::TcpConnection(packet);
        packet->setConnection(connection);
	connection->getApplicationInfo()->http_hostNameSet = true;
	connection->getApplicationInfo()->http_hostName = "fuck.com";

        HttpHostname criteria;
	criteria.alias = AliasPtr(new SFwallCore::WebsiteListAlias( {"yahoo.com", "google.com"}));
	
        EXPECT_FALSE(criteria.match(packet, DIRECTION_SOURCE));
}

TEST(Criteria, IsHttp_matches){
	SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("192.168.1.1", "10.1.1.1", 12332, 80, "");
        SFwallCore::TcpConnection *connection = new SFwallCore::TcpConnection(packet);
        packet->setConnection(connection);
	connection->getApplicationInfo()->http_hostNameSet = true;
	connection->getApplicationInfo()->http_hostName = "fuck.com";
        connection->getApplicationInfo()->type = SFwallCore::ConnectionApplicationInfo::Classifier::HTTP;

        IsHttp criteria;
        EXPECT_TRUE(criteria.match(packet, DIRECTION_SOURCE));
}

TEST(Criteria, IsHttp_mismatches){
	 SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("192.168.1.1", "10.1.1.1", 12332, 80, "");
        SFwallCore::TcpConnection *connection = new SFwallCore::TcpConnection(packet);
        packet->setConnection(connection);

        IsHttp criteria;
        EXPECT_FALSE(criteria.match(packet, DIRECTION_SOURCE));
}

TEST(Criteria, HttpContentTypeRegex_matches){
	SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("192.168.1.1", "10.1.1.1", 12332, 80, "");
	SFwallCore::TcpConnection *connection = new SFwallCore::TcpConnection(packet);
	packet->setConnection(connection); 
		
	connection->getApplicationInfo()->http_set(RESPONSE, "Content-Type", "application/xml");
	connection->getApplicationInfo()->httpHeadersResponseParsed = true;
	connection->getApplicationInfo()->type = SFwallCore::ConnectionApplicationInfo::Classifier::HTTP;

	HttpContentTypeRegex criteria;
	criteria.expression = "application/xml"; 
	criteria.refresh();

	EXPECT_TRUE(criteria.match(packet, DIRECTION_SOURCE));
}

TEST(Criteria, HttpContentTypeRegex_mismatches){
	SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("192.168.1.1", "10.1.1.1", 12332, 80, "");
	SFwallCore::TcpConnection *connection = new SFwallCore::TcpConnection(packet);
	packet->setConnection(connection); 
		
	connection->getApplicationInfo()->http_set(RESPONSE, "Content-Type", "application/xml");
	connection->getApplicationInfo()->httpHeadersResponseParsed = true;
	connection->getApplicationInfo()->type = SFwallCore::ConnectionApplicationInfo::Classifier::HTTP;

	HttpContentTypeRegex criteria;
	criteria.expression = "application/pdf"; 
	criteria.refresh();

	EXPECT_FALSE(criteria.match(packet, DIRECTION_SOURCE));
}

TEST(Criteria, HttpUserAgentRegex_matches){
	SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("192.168.1.1", "10.1.1.1", 12332, 80, "");
	SFwallCore::TcpConnection *connection = new SFwallCore::TcpConnection(packet);
	packet->setConnection(connection); 
		
	connection->getApplicationInfo()->http_set(REQUEST, "User-Agent", "Mozilla/4.0 (compatible; MSIE 7.0; America Online Browser 1.1; Windows NT 5.1; (R1 1.5); .NET CLR 2.0.50727; InfoPath.1)");
	connection->getApplicationInfo()->httpHeadersParsed= true;
	connection->getApplicationInfo()->type = SFwallCore::ConnectionApplicationInfo::Classifier::HTTP;

	HttpUserAgentRegex criteria;
	criteria.expression = "America"; 
	criteria.refresh();

	EXPECT_TRUE(criteria.match(packet, DIRECTION_SOURCE));
}

TEST(Criteria, HttpUserAgentRegex_matches_case){
	SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("192.168.1.1", "10.1.1.1", 12332, 80, "");
	SFwallCore::TcpConnection *connection = new SFwallCore::TcpConnection(packet);
	packet->setConnection(connection); 
		
	connection->getApplicationInfo()->http_set(REQUEST, "User-Agent", "Mozilla/4.0 (compatible; MSIE 7.0; America Online Browser 1.1; Windows NT 5.1; (R1 1.5); .NET CLR 2.0.50727; InfoPath.1)");
	connection->getApplicationInfo()->httpHeadersParsed= true;
	connection->getApplicationInfo()->type = SFwallCore::ConnectionApplicationInfo::Classifier::HTTP;

	HttpUserAgentRegex criteria;
	criteria.expression = "america"; 
	criteria.refresh();

	EXPECT_TRUE(criteria.match(packet, DIRECTION_SOURCE));
}

TEST(Criteria, HttpRequestRegexCriteria_matches_case){
        SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("192.168.1.1", "10.1.1.1", 12332, 80, "");
        SFwallCore::TcpConnection *connection = new SFwallCore::TcpConnection(packet);
        packet->setConnection(connection);

        connection->getApplicationInfo()->http_set(REQUEST, "REQUEST", "GET /home.html blah blah");
        connection->getApplicationInfo()->httpHeadersParsed= true;
        connection->getApplicationInfo()->type = SFwallCore::ConnectionApplicationInfo::Classifier::HTTP;

        HttpRequestRegexCriteria criteria;
        criteria.expression = "home.html";
        criteria.refresh();

        EXPECT_TRUE(criteria.match(packet, DIRECTION_SOURCE));
}

TEST(Criteria, HttpUserAgentRegex_mismatches){
	SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("192.168.1.1", "10.1.1.1", 12332, 80, "");
	SFwallCore::TcpConnection *connection = new SFwallCore::TcpConnection(packet);
	packet->setConnection(connection); 
		
	connection->getApplicationInfo()->http_set(REQUEST, "User-Agent", "Mozilla/4.0 (compatible; MSIE 7.0; America Online Browser 1.1; Windows NT 5.1; (R1 1.5); .NET CLR 2.0.50727; InfoPath.1)");
	connection->getApplicationInfo()->httpHeadersParsed= true;
	connection->getApplicationInfo()->type = SFwallCore::ConnectionApplicationInfo::Classifier::HTTP;

	HttpUserAgentRegex criteria;
	criteria.expression = "wget"; 
	criteria.refresh();

	EXPECT_FALSE(criteria.match(packet, DIRECTION_SOURCE));
}

TEST(Criteria, Regex_matches){
	SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("192.168.1.1", "10.1.1.1", 12332, 80, "data123");
	SFwallCore::TcpConnection *connection = new SFwallCore::TcpConnection(packet);
	packet->setConnection(connection); 
		
	RegexCriteria criteria;
	criteria.expression = "data"; 
	criteria.refresh();

	EXPECT_TRUE(criteria.match(packet, DIRECTION_SOURCE));
}

TEST(Criteria, Regex_matches_case){
	SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("192.168.1.1", "10.1.1.1", 12332, 80, "data123");
	SFwallCore::TcpConnection *connection = new SFwallCore::TcpConnection(packet);
	packet->setConnection(connection); 
		
	RegexCriteria criteria;
	criteria.expression = "Data"; 
	criteria.refresh();

	EXPECT_TRUE(criteria.match(packet, DIRECTION_SOURCE));
}


TEST(Criteria, Regex_mismatches){
	SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("192.168.1.1", "10.1.1.1", 12332, 80, "data123");
	SFwallCore::TcpConnection *connection = new SFwallCore::TcpConnection(packet);
	packet->setConnection(connection); 
		
	RegexCriteria criteria;
	criteria.expression = "buffer"; 
	criteria.refresh();

	EXPECT_FALSE(criteria.match(packet, DIRECTION_SOURCE));
}

TEST(Criteria, http_host_regex_matches) {
	SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("192.168.1.1", "10.1.1.1", 12332, 80, "");
	SFwallCore::TcpConnection *connection = new SFwallCore::TcpConnection(packet);
	packet->setConnection(connection);
	connection->getApplicationInfo()->http_hostNameSet = true;
	connection->getApplicationInfo()->http_hostName = "blah.google.com";
	connection->getApplicationInfo()->type = SFwallCore::ConnectionApplicationInfo::Classifier::HTTP;

	HttpHostnameRegex criteria;
	criteria.expression = "google";
	criteria.refresh();

	EXPECT_TRUE(criteria.match(packet, DIRECTION_SOURCE));
}

TEST(Criteria, http_host_regex_mismatches) {
	SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("192.168.1.1", "10.1.1.1", 12332, 80, "");
	SFwallCore::TcpConnection *connection = new SFwallCore::TcpConnection(packet);
	packet->setConnection(connection);
	connection->getApplicationInfo()->http_hostNameSet = true;
	connection->getApplicationInfo()->http_hostName = "fuck.com";
	connection->getApplicationInfo()->type = SFwallCore::ConnectionApplicationInfo::Classifier::HTTP;

	HttpHostnameRegex criteria;
	criteria.expression = "google";
	criteria.refresh();

	EXPECT_FALSE(criteria.match(packet, DIRECTION_SOURCE));
}

TEST(Criteria, destination_port_matches) {
	SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("192.168.1.100", "2.1.33.1", 12332, 80, "");

	DestinationPortCriteria criteria;
	criteria.ports.insert(80);	
	criteria.ports.insert(443);	

	EXPECT_TRUE(criteria.match(packet, DIRECTION_NA));
}

TEST(Criteria, destination_port_mismatches) {
	SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("192.168.1.100", "2.1.33.1", 12332, 80, "");

	DestinationPortCriteria criteria;
	criteria.ports.insert(81);	
	criteria.ports.insert(443);	

	EXPECT_FALSE(criteria.match(packet, DIRECTION_NA));
}

TEST(Criteria, source_port_matches) {
	SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("192.168.1.100", "2.1.33.1", 12332, 80, "");

	SourcePortCriteria criteria;
	criteria.ports.insert(12332);	
	criteria.ports.insert(443);	

	EXPECT_TRUE(criteria.match(packet , DIRECTION_NA));
}

TEST(Criteria, source_port_mismatches) {
	SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("192.168.1.100", "2.1.33.1", 12332, 80, "");

	SourcePortCriteria criteria;
	criteria.ports.insert(81);	
	criteria.ports.insert(443);	

	EXPECT_FALSE(criteria.match(packet, DIRECTION_NA));
}

TEST(Criteria, destination_port_range_matches) {
	SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("192.168.1.100", "2.1.33.1", 12332, 80, "");

	DestinationPortRangeCriteria criteria;
	criteria.startPort = 21;
	criteria.endPort = 81;

	EXPECT_TRUE(criteria.match(packet, DIRECTION_NA));
}

TEST(Criteria, destination_port_range_mismatches) {
	SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("192.168.1.100", "2.1.33.1", 12332, 80, "");

	DestinationPortRangeCriteria criteria;
	criteria.startPort = 21;
	criteria.endPort = 31;

	EXPECT_FALSE(criteria.match(packet, DIRECTION_NA));
}

TEST(Criteria, source_port_range_matches) {
	SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("192.168.1.100", "2.1.33.1", 80, 11111, "");

	SourcePortRangeCriteria criteria;
	criteria.startPort = 21;
	criteria.endPort = 81;

	EXPECT_TRUE(criteria.match(packet, DIRECTION_NA));
}

TEST(Criteria, source_port_range_mismatches) {
	SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("192.168.1.100", "2.1.33.1", 12332, 80, "");

	SourcePortRangeCriteria criteria;
	criteria.startPort = 21;
	criteria.endPort = 31;

	EXPECT_FALSE(criteria.match(packet, DIRECTION_NA));
}

TEST(Criteria, source_device_matches) {
	SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("192.168.1.100", "2.1.33.1", 80, 11111, "");
	SFwallCore::PacketBuilder::setSourceDev(packet, 10);

	SourceNetworkDeviceCriteria criteria;
	criteria.devicePtr = InterfacePtr(new Interface("eth10"));
	criteria.devicePtr->ifid = 10;

	EXPECT_TRUE(criteria.match(packet, DIRECTION_NA));
}

TEST(Criteria, source_device_mismatch) {
	SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("192.168.1.100", "2.1.33.1", 80, 11111, "");
	SFwallCore::PacketBuilder::setSourceDev(packet, 10);

	SourceNetworkDeviceCriteria criteria;
	criteria.devicePtr = InterfacePtr(new Interface("eth10"));
	criteria.devicePtr->ifid = 11;

	EXPECT_FALSE(criteria.match(packet, DIRECTION_NA));
}

TEST(Criteria, destination_device_matches) {
	SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("192.168.1.100", "2.1.33.1", 80, 11111, "");
	SFwallCore::PacketBuilder::setDestDev(packet, 10);

	DestinationNetworkDeviceCriteria criteria;
	criteria.devicePtr = InterfacePtr(new Interface("eth10"));
	criteria.devicePtr->ifid = 10;

	EXPECT_TRUE(criteria.match(packet, DIRECTION_NA));
}

TEST(Criteria, destination_device_mismatch) {
	SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("192.168.1.100", "2.1.33.1", 80, 11111, "");
	SFwallCore::PacketBuilder::setDestDev(packet, 10);

	DestinationNetworkDeviceCriteria criteria;
	criteria.devicePtr = InterfacePtr(new Interface("eth10"));
	criteria.devicePtr->ifid = 11;

	EXPECT_FALSE(criteria.match(packet, DIRECTION_NA));
}

TEST(Criteria, protocol_matches) {
	SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("192.168.1.100", "2.1.33.1", 80, 11111, "");

	ProtocolCriteria criteria;
	criteria.type = TCP;	
	EXPECT_TRUE(criteria.match(packet, DIRECTION_NA));
}

TEST(Criteria, protocol_mismatch) {
	SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("192.168.1.100", "2.1.33.1", 80, 11111, "");

	ProtocolCriteria criteria;
	criteria.type = UDP;
	EXPECT_FALSE(criteria.match(packet, DIRECTION_NA));
}

TEST(Criteria, geoip_source_match) {
	SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("14.128.4.3", "2.1.33.1", 80, 11111, "");

	GeoIPSourceCriteria criteria;
	criteria.country = "New Zealand";
	criteria.refresh();

	EXPECT_TRUE(criteria.match(packet, DIRECTION_NA));
}

TEST(Criteria, geoip_source_mismatch) {
	SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("10.1.1.1", "2.1.33.1", 80, 11111, "");

	GeoIPSourceCriteria criteria;
	criteria.country = "New Zealand";
	criteria.refresh();

	EXPECT_FALSE(criteria.match(packet, DIRECTION_NA));
}

TEST(Criteria, geoip_destination_match) {
	SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("10.1.1.1", "14.128.4.3", 80, 11111, "");

	GeoIPDestinationCriteria criteria;
	criteria.country = "New Zealand";
	criteria.refresh();

	EXPECT_TRUE(criteria.match(packet, DIRECTION_NA));
}

TEST(Criteria, geoip_destination_mismatch) {
	SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("10.1.1.1", "2.1.33.1", 80, 11111, "");

	GeoIPDestinationCriteria criteria;
	criteria.country = "New Zealand";
	criteria.refresh();

	EXPECT_FALSE(criteria.match(packet, DIRECTION_NA));
}

TEST(Criteria, geoip_invalid_country) {
	SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("10.1.1.1", "2.1.33.1", 80, 11111, "");

	GeoIPDestinationCriteria criteria;
	criteria.country = "Michael Land";
	criteria.refresh();

	EXPECT_FALSE(criteria.match(packet, DIRECTION_NA));
}

TEST(CriteriaBuilder, parse_basic_criteria){
	string basic = "{\"type\": \"application.http.useragent.regex\",\"negate\": false,\"conditions\": {\"expression\": \"Windows Phone\"}}";
	ObjectWrapper* wrapper = JsonSerialiser::parseJsonString(basic);
	ObjectContainer* object = wrapper->container();	
	Criteria* criteria = CriteriaBuilder::parse(object);

	EXPECT_TRUE(criteria != NULL);
	EXPECT_TRUE(criteria->key().compare("application.http.useragent.regex") == 0);
	
	RegexBasedCriteria* regex_criteria = (RegexBasedCriteria*) criteria;
	EXPECT_TRUE(regex_criteria->expression.compare("Windows Phone") == 0);
}

TEST(CriteriaBuilder, parse_missing_type){
	string basic = "{\"negate\": false,\"conditions\": {\"expression\": \"Windows Phone\"}}";
	ObjectWrapper* wrapper = JsonSerialiser::parseJsonString(basic);
	ObjectContainer* object = wrapper->container();	
	Criteria* criteria = CriteriaBuilder::parse(object);

	EXPECT_TRUE(criteria == NULL);
}

TEST(CriteriaBuilder, parse_invalid_type){
	string basic = "{\"type\":\"dudes\", \"negate\": false,\"conditions\": {\"expression\": \"Windows Phone\"}}";
	ObjectWrapper* wrapper = JsonSerialiser::parseJsonString(basic);
	ObjectContainer* object = wrapper->container();	
	Criteria* criteria = CriteriaBuilder::parse(object);

	EXPECT_TRUE(criteria == NULL);
}

TEST(CriteriaBuilder, parse_missing_conditions){
	string basic = "{\"type\":\"application.http.useragent.regex\", \"negate\": false}";
	ObjectWrapper* wrapper = JsonSerialiser::parseJsonString(basic);
	ObjectContainer* object = wrapper->container();	
	Criteria* criteria = CriteriaBuilder::parse(object);

	EXPECT_TRUE(criteria == NULL);
}

TEST(CriteriaBuilder, parse_http_hostname_url){
	string basic = "{\"type\": \"application.http.hostname\",\"negate\": false,\"conditions\": {\"source_url\": \"http://mirror.sphirewall.net/resouces/list.txt\"}}";
	ObjectWrapper* wrapper = JsonSerialiser::parseJsonString(basic);
	ObjectContainer* object = wrapper->container();	
	Criteria* criteria = CriteriaBuilder::parse(object);

	EXPECT_TRUE(criteria != NULL);
	EXPECT_TRUE(criteria->key().compare("application.http.hostname") == 0);
	
	HttpHostname* http_criteria = (HttpHostname*) criteria;
	EXPECT_TRUE(http_criteria->alias.get());
	EXPECT_TRUE(http_criteria->alias->source);
}

TEST(CriteriaBuilder, parse_http_hostname_entries){
	string basic = "{\"type\": \"application.http.hostname\",\"negate\": false,\"conditions\": {\"source_entries\": [\"google.com\", \"yahoo.com\"]}}";
	ObjectWrapper* wrapper = JsonSerialiser::parseJsonString(basic);
	ObjectContainer* object = wrapper->container();	
	Criteria* criteria = CriteriaBuilder::parse(object);

	EXPECT_TRUE(criteria != NULL);
	EXPECT_TRUE(criteria->key().compare("application.http.hostname") == 0);
	
	HttpHostname* http_criteria = (HttpHostname*) criteria;
	EXPECT_TRUE(http_criteria->alias.get());
	EXPECT_FALSE(http_criteria->alias->source);
	
	EXPECT_TRUE(http_criteria->alias->search("google.com"));
	EXPECT_TRUE(http_criteria->alias->search("yahoo.com"));
}

TEST(Criteria, fingerprint_matches) {
	MockFactory *mocks = new MockFactory();
	Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("192.168.1.2", "2.1.33.1", 12332, 80, "");
	SFwallCore::TcpConnection *connection = new SFwallCore::TcpConnection(packet);
	packet->setConnection(connection);

	FingerprintPtr fingerprint(new Fingerprint());
	fingerprint->uniqueId = "somecoolid";

	HostPtr host = HostPtr(new Host());
	host->device_fingerprint = fingerprint;
	connection->setHostDiscoveryServiceEntry(host);

	FingerprintCriteria criteria;
	criteria.fingerprints.push_back(fingerprint);
	EXPECT_TRUE(criteria.match(packet, DIRECTION_SOURCE));
}

TEST(Criteria, fingerprint_mismatches) {
	MockFactory *mocks = new MockFactory();
	Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("192.168.1.2", "2.1.33.1", 12332, 80, "");
	SFwallCore::TcpConnection *connection = new SFwallCore::TcpConnection(packet);
	packet->setConnection(connection);

	FingerprintPtr fingerprint(new Fingerprint());
	fingerprint->uniqueId = "somecoolid";

	HostPtr host = HostPtr(new Host());
	host->device_fingerprint = fingerprint;
	connection->setHostDiscoveryServiceEntry(host);

	FingerprintCriteria criteria;
	FingerprintPtr fingerprint1(new Fingerprint());
	fingerprint1->uniqueId = "otherid";
	criteria.fingerprints.push_back(fingerprint1);
	EXPECT_FALSE(criteria.match(packet, DIRECTION_SOURCE));
}
