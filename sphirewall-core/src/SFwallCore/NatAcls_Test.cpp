#include <gtest/gtest.h>
#include <iostream>

using namespace std;

#include "SFwallCore/TestFactory.h"
#include "Utils/IP4Addr.h"
#include "SFwallCore/Packet.h"
#include "SFwallCore/NatAcls.h"
#include "SFwallCore/Criteria.h"
#include "test.h"

using namespace SFwallCore;

TEST(PortForwardingRule, enabled) {
	SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("10.1.1.1", "2.1.33.1", 12332, 80, "");
	SFwallCore::PortForwardingRule rule;
	rule.enabled = true;	

	EXPECT_TRUE(rule.match(packet));
}

TEST(PortForwardingRule, disabled) {
	SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("10.1.1.1", "2.1.33.1", 12332, 80, "");
	SFwallCore::PortForwardingRule rule;
	rule.enabled = false;	
	EXPECT_FALSE(rule.match(packet));
}

TEST(PortForwardingRule, criteria_matches) {
	SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("10.1.1.1", "2.1.33.1", 12332, 80, "");
	SFwallCore::PortForwardingRule rule;
	
        IpCriteria* sc = new IpCriteria(); sc->ip = IP4Addr::stringToIP4Addr("10.1.1.1"); sc->mask = IP4Addr::stringToIP4Addr("255.255.255.0");
	ProtocolCriteria* pc = new ProtocolCriteria(); pc->type = TCP;

	rule.source_criteria.push_back(CriteriaPtr(sc));
	rule.source_criteria.push_back(CriteriaPtr(pc));
		
	rule.enabled = true;	
	EXPECT_TRUE(rule.match(packet));
}

TEST(PortForwardingRule, criteria_mismatches) {
	SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("10.1.1.1", "2.1.33.1", 12332, 80, "");
	SFwallCore::PortForwardingRule rule;
	rule.enabled = false;	

        IpCriteria* sc = new IpCriteria(); sc->ip = IP4Addr::stringToIP4Addr("10.1.1.1"); sc->mask = IP4Addr::stringToIP4Addr("255.255.255.0");
        ProtocolCriteria* pc = new ProtocolCriteria(); pc->type = UDP;

        rule.source_criteria.push_back(CriteriaPtr(sc));
        rule.source_criteria.push_back(CriteriaPtr(pc));

	EXPECT_FALSE(rule.match(packet));
}

TEST(MasqueradeRule, enabled) {
	SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("10.1.1.1", "2.1.33.1", 12332, 80, "");
	SFwallCore::MasqueradeRule rule;
	rule.enabled = true;	

	EXPECT_TRUE(rule.match(packet));
}

TEST(MasqueradeRule, disabled) {
	SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("10.1.1.1", "2.1.33.1", 12332, 80, "");
	SFwallCore::MasqueradeRule rule;
	rule.enabled = false;	
	EXPECT_FALSE(rule.match(packet));
}

TEST(MasqueradeRule, criteria_matches) {
	SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("10.1.1.1", "2.1.33.1", 12332, 80, "");
	SFwallCore::MasqueradeRule rule;
	
        IpCriteria* sc = new IpCriteria(); sc->ip = IP4Addr::stringToIP4Addr("10.1.1.1"); sc->mask = IP4Addr::stringToIP4Addr("255.255.255.0");
	ProtocolCriteria* pc = new ProtocolCriteria(); pc->type = TCP;

	rule.source_criteria.push_back(CriteriaPtr(sc));
	rule.source_criteria.push_back(CriteriaPtr(pc));
		
	rule.enabled = true;	
	EXPECT_TRUE(rule.match(packet));
}

TEST(MasqueradeRule, criteria_mismatches) {
	SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("10.1.1.1", "2.1.33.1", 12332, 80, "");
	SFwallCore::MasqueradeRule rule;
	rule.enabled = false;	

        SourceIpCriteria* sc = new SourceIpCriteria(); sc->ip = IP4Addr::stringToIP4Addr("10.1.1.1"); sc->mask = IP4Addr::stringToIP4Addr("255.255.255.0");
        ProtocolCriteria* pc = new ProtocolCriteria(); pc->type = UDP;

        rule.source_criteria.push_back(CriteriaPtr(sc));
        rule.source_criteria.push_back(CriteriaPtr(pc));

	EXPECT_FALSE(rule.match(packet));
}

TEST(Autowan, single_interface) {
        SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("10.1.1.1", "2.1.33.1", 12332, 80, "");
        SFwallCore::PacketBuilder::setSourceDev(packet, 10);
	//Configure interfaces
	IntMgr* interfaces = new IntMgr();
	InterfacePtr eth10(new Interface("eth10"));
	eth10->ifid = 10;
	interfaces->put_interface_by_id(10, eth10);

        SFwallCore::NatAclStore store(interfaces);
        store.set_autowan_mode(SINGLE);

	AutowanInterface* interface = new AutowanInterface();
	interface->interface = "eth10";	

	store.get_autowan_rules().push_back(AutowanInterfacePtr(interface));
	store.initRules();		
	EXPECT_TRUE(store.determine_routing_fwmark(packet) == -1);
}

TEST(Autowan, load_balancing_multipath_single_interface) {
        SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("10.1.1.1", "2.1.33.1", 12332, 80, "");

	//Configure interfaces
	IntMgr* interfaces = new IntMgr();
	InterfacePtr eth10(new Interface("eth10"));
	eth10->ifid = 10;
	eth10->state = true;
	interfaces->put_interface_by_id(10, eth10);

        SFwallCore::NatAclStore store(interfaces);
        store.set_autowan_mode(AUTOWAN_MODE_LOADBALANCING);

	AutowanInterface* interface = new AutowanInterface();
	interface->interface = "eth10";	
	interface->failover_index = 1;

	store.get_autowan_rules().push_back(AutowanInterfacePtr(interface));
	store.initRules();		
	EXPECT_TRUE(store.determine_routing_fwmark(packet) == 10);
}

TEST(Autowan, load_balancing_multipath_multi_interface) {
        SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("10.1.1.1", "2.1.33.1", 12332, 80, "");

        //Configure interfaces
        IntMgr* interfaces = new IntMgr();
        InterfacePtr eth10(new Interface("eth10"));
        eth10->ifid = 10;
        eth10->state = true;
        interfaces->put_interface_by_id(10, eth10);

        InterfacePtr eth11(new Interface("eth11"));
        eth11->ifid = 11;
        eth11->state = true;
        interfaces->put_interface_by_id(11, eth11);

        SFwallCore::NatAclStore store(interfaces);
        store.set_autowan_mode(AUTOWAN_MODE_LOADBALANCING);

        AutowanInterface* interface = new AutowanInterface();
        interface->interface = "eth10";
        interface->failover_index = 1;
        store.get_autowan_rules().push_back(AutowanInterfacePtr(interface));

        AutowanInterface* interface2 = new AutowanInterface();
        interface2->interface = "eth11";
        interface2->failover_index = 2;
        store.get_autowan_rules().push_back(AutowanInterfacePtr(interface2));

        store.initRules();
        EXPECT_TRUE(store.determine_routing_fwmark(packet) == 10);
        EXPECT_TRUE(store.determine_routing_fwmark(packet) == 11);
        EXPECT_TRUE(store.determine_routing_fwmark(packet) == 10);
}

TEST(Autowan, load_balancing_multipath_multi_interface_with_criteria) {
        SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("10.1.1.1", "2.1.33.1", 12332, 80, "");

        //Configure interfaces
        IntMgr* interfaces = new IntMgr();
        InterfacePtr eth10(new Interface("eth10"));
        eth10->ifid = 10;
        eth10->state = true;
        interfaces->put_interface_by_id(10, eth10);

        InterfacePtr eth11(new Interface("eth11"));
        eth11->ifid = 11;
        eth11->state = true;
        interfaces->put_interface_by_id(11, eth11);

        SFwallCore::NatAclStore store(interfaces);
        store.set_autowan_mode(AUTOWAN_MODE_LOADBALANCING);

        AutowanInterface* interface = new AutowanInterface();
        interface->interface = "eth10";
        interface->failover_index = 1;
	DestinationPortCriteria* criteria = new DestinationPortCriteria();
	criteria->ports.insert(443);
	interface->criteria.push_back(criteria);
	store.get_autowan_rules().push_back(AutowanInterfacePtr(interface));

	AutowanInterface* interface2 = new AutowanInterface();
	interface2->interface = "eth11";
	interface2->failover_index = 2;
	store.get_autowan_rules().push_back(AutowanInterfacePtr(interface2));

	store.initRules();
	EXPECT_TRUE(store.determine_routing_fwmark(packet) == 11);
	EXPECT_TRUE(store.determine_routing_fwmark(packet) == 11);
}

TEST(Autowan, load_balancing_multipath_multi_interface_with_criteria_multiple_matches) {
        SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("10.1.1.1", "2.1.33.1", 12332, 443, "");

        //Configure interfaces
        IntMgr* interfaces = new IntMgr();
        InterfacePtr eth10(new Interface("eth10"));
        eth10->ifid = 10;
        eth10->state = true;
        interfaces->put_interface_by_id(10, eth10);

        InterfacePtr eth11(new Interface("eth11"));
        eth11->ifid = 11;
        eth11->state = true;
        interfaces->put_interface_by_id(11, eth11);

        SFwallCore::NatAclStore store(interfaces);
        store.set_autowan_mode(AUTOWAN_MODE_LOADBALANCING);

        AutowanInterface* interface = new AutowanInterface();
        interface->interface = "eth10";
        interface->failover_index = 1;
        DestinationPortCriteria* criteria = new DestinationPortCriteria();
        criteria->ports.insert(443);
        interface->criteria.push_back(criteria);
        store.get_autowan_rules().push_back(AutowanInterfacePtr(interface));

        AutowanInterface* interface2 = new AutowanInterface();
        interface2->interface = "eth11";
        interface2->failover_index = 2;
        store.get_autowan_rules().push_back(AutowanInterfacePtr(interface2));

        store.initRules();
        EXPECT_TRUE(store.determine_routing_fwmark(packet) == 10);
        EXPECT_TRUE(store.determine_routing_fwmark(packet) == 11);
}

TEST(Autowan, load_balancing_multipath_multi_interface_interface_ignored_if_offline) {
	SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("10.1.1.1", "2.1.33.1", 12332, 80, "");

	//Configure interfaces
	IntMgr* interfaces = new IntMgr();
	InterfacePtr eth10(new Interface("eth10"));
	eth10->ifid = 10;
	eth10->state = true;
	interfaces->put_interface_by_id(10, eth10);

	InterfacePtr eth11(new Interface("eth11"));
	eth11->ifid = 11;
	eth11->state = false;
	interfaces->put_interface_by_id(11, eth11);

	SFwallCore::NatAclStore store(interfaces);
	store.set_autowan_mode(AUTOWAN_MODE_LOADBALANCING);

	AutowanInterface* interface = new AutowanInterface();
	interface->interface = "eth10";
	interface->failover_index = 1;
	store.get_autowan_rules().push_back(AutowanInterfacePtr(interface));

	AutowanInterface* interface2 = new AutowanInterface();
	interface2->interface = "eth11";
	interface2->failover_index = 2;
	store.get_autowan_rules().push_back(AutowanInterfacePtr(interface2));

	store.initRules();
	EXPECT_TRUE(store.determine_routing_fwmark(packet) == 10);
	EXPECT_TRUE(store.determine_routing_fwmark(packet) == 10);
	EXPECT_TRUE(store.determine_routing_fwmark(packet) == 10);
	EXPECT_TRUE(store.determine_routing_fwmark(packet) == 10);
}

TEST(Autowan, failover_primary_link_online) {
	SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("10.1.1.1", "2.1.33.1", 12332, 80, "");

	//Configure interfaces
	IntMgr* interfaces = new IntMgr();
	InterfacePtr eth10(new Interface("eth10"));
	eth10->ifid = 10;
	eth10->state = true;
	interfaces->put_interface_by_id(10, eth10);

	InterfacePtr eth11(new Interface("eth11"));
	eth11->ifid = 11;
	eth11->state = true;
	interfaces->put_interface_by_id(11, eth11);

	SFwallCore::NatAclStore store(interfaces);
	store.set_autowan_mode(FAILOVER);

	AutowanInterface* interface = new AutowanInterface();
	interface->interface = "eth10";
	interface->failover_index = 1;
	store.get_autowan_rules().push_back(AutowanInterfacePtr(interface));

	AutowanInterface* interface2 = new AutowanInterface();
	interface2->interface = "eth11";
	interface2->failover_index = 2;
	store.get_autowan_rules().push_back(AutowanInterfacePtr(interface2));

	store.initRules();
	EXPECT_TRUE(store.determine_routing_fwmark(packet) == 10);
	EXPECT_TRUE(store.determine_routing_fwmark(packet) == 10);
	EXPECT_TRUE(store.determine_routing_fwmark(packet) == 10);
	EXPECT_TRUE(store.determine_routing_fwmark(packet) == 10);
}

TEST(Autowan, failover_primary_link_offline_secondary_online) {
	SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("10.1.1.1", "2.1.33.1", 12332, 80, "");

	//Configure interfaces
	IntMgr* interfaces = new IntMgr();
	InterfacePtr eth10(new Interface("eth10"));
	eth10->ifid = 10;
	eth10->state = false;
	interfaces->put_interface_by_id(10, eth10);

	InterfacePtr eth11(new Interface("eth11"));
	eth11->ifid = 11;
	eth11->state = true;
	interfaces->put_interface_by_id(11, eth11);

	SFwallCore::NatAclStore store(interfaces);
	store.set_autowan_mode(FAILOVER);

	AutowanInterface* interface = new AutowanInterface();
	interface->interface = "eth10";
	interface->failover_index = 1;
	store.get_autowan_rules().push_back(AutowanInterfacePtr(interface));

	AutowanInterface* interface2 = new AutowanInterface();
	interface2->interface = "eth11";
	interface2->failover_index = 2;
	store.get_autowan_rules().push_back(AutowanInterfacePtr(interface2));

	store.initRules();
	EXPECT_TRUE(store.determine_routing_fwmark(packet) == 11);
	EXPECT_TRUE(store.determine_routing_fwmark(packet) == 11);
}

TEST(Autowan, failover_primary_link_criteria_determines_link) {
	SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("10.1.1.1", "2.1.33.1", 12332, 80, "");

	//Configure interfaces
	IntMgr* interfaces = new IntMgr();
	InterfacePtr eth10(new Interface("eth10"));
	eth10->ifid = 10;
	eth10->state = true;
	interfaces->put_interface_by_id(10, eth10);

	InterfacePtr eth11(new Interface("eth11"));
	eth11->ifid = 11;
	eth11->state = true;
	interfaces->put_interface_by_id(11, eth11);

	SFwallCore::NatAclStore store(interfaces);
	store.set_autowan_mode(FAILOVER);

	AutowanInterface* interface = new AutowanInterface();
	interface->interface = "eth10";
	interface->failover_index = 1;

        DestinationPortCriteria* criteria = new DestinationPortCriteria();
        criteria->ports.insert(443);
        interface->criteria.push_back(criteria);

	store.get_autowan_rules().push_back(AutowanInterfacePtr(interface));

	AutowanInterface* interface2 = new AutowanInterface();
	interface2->interface = "eth11";
	interface2->failover_index = 2;
	store.get_autowan_rules().push_back(AutowanInterfacePtr(interface2));

	store.initRules();
	EXPECT_TRUE(store.determine_routing_fwmark(packet) == 11);
	EXPECT_TRUE(store.determine_routing_fwmark(packet) == 11);
}

