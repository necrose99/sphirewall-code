/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef CRITERIA_H
#define CRITERIA_H

#include "SFwallCore/Packet.h"
#include "Auth/Group.h"
#include "SFwallCore/Alias.h"
#include "Core/ConfigurationManager.h"
#include "SFwallCore/ApplicationLevel/Signatures.h"
#include "SFwallCore/ApplicationLevel/Fingerprinting.h"
#include "SFwallCore/Connection.h"
#include "SFwallCore/TimePeriods.h"
#include "SFwallCore/SecurityGroup.h"

#include <boost/unordered_set.hpp>

class TimePeriod;

namespace SFwallCore {
	class CriteriaBuilder;
	class ConnectionIp;
	
	enum CriteriaType {IP, SESSION, APPLICATION, APPLICATION_EXPENSIVE};

	#define DIRECTION_NA 0
	#define DIRECTION_SOURCE 1
	#define DIRECTION_DESTINATION 2

	class Criteria {
		public:
			Criteria(){
				this->negate = false;
			}		
			virtual ~Criteria(){};

			virtual std::string key() const = 0;
			bool match(Packet* packet, int direction){
				if(!this->negate){
					return inner_match(packet, direction);
				}else{
					return !inner_match(packet, direction);
				}
			}

			virtual void refresh(){}
			virtual CriteriaType type() const {
				return IP;
			}		

			virtual ConnectionApplicationInfo::Classifier lowPassApplicationFilterType() const {
				return ConnectionApplicationInfo::Classifier::NDEF;
			}

			bool negate;

			virtual bool inner_match(Packet* packet, int direction) = 0;

	};

        class CriteriaSet {
                public:
                        ~CriteriaSet();
                        list<Criteria*> criteria;
        };
        typedef boost::shared_ptr<CriteriaSet> CriteriaSetPtr;

        class StatefulDnsDestinationCriteria : public Criteria{
                public:
                        StatefulDnsDestinationCriteria();
                        ~StatefulDnsDestinationCriteria(){}
                        std::string key() const;
                        bool inner_match(Packet* packet);
                        bool inner_match(Packet* packet, int direction){
                                return inner_match(packet);
                        }

			std::string target_hostname;
			AliasPtr stateful_cnames;
			AliasPtr stateful_ipaddresses;

                        virtual CriteriaType type() const {
                                return APPLICATION_EXPENSIVE;
                        }

			void refresh(){
				stateful_cnames->addEntry(target_hostname);
			}
        };

	class SourceIpCriteria : public Criteria{
		public:
			~SourceIpCriteria(){}
			std::string key() const;
			bool inner_match(Packet* packet, int direction){
				return inner_match(packet);
			}

			bool inner_match(Packet* packet);

			unsigned int ip;
			unsigned int mask;
	};

	class DestinationIpCriteria : public Criteria{
		public:
			~DestinationIpCriteria(){}
			std::string key() const;
			bool inner_match(Packet* packet);
                        bool inner_match(Packet* packet, int direction){
                                return inner_match(packet);
                        }

			unsigned int ip;
			unsigned int mask;
	};

        class IpCriteria : public Criteria{
                public:
                        ~IpCriteria(){}
                        std::string key() const;
                        bool inner_match(Packet* packet, int direction);

                        unsigned int ip;
                        unsigned int mask;
        };

	class SourceIp6Criteria : public Criteria{
		public:
			SourceIp6Criteria(){
				addr = (struct in6_addr*) malloc(sizeof(struct in6_addr));
				cidr = 0;
			}		
			~SourceIp6Criteria(){}
			std::string key() const;
			bool inner_match(Packet* packet);
                        bool inner_match(Packet* packet, int direction){
                                return inner_match(packet);
                        }

			struct in6_addr *addr;
			int cidr;
	};

	class DestinationIp6Criteria : public Criteria{
		public:
			DestinationIp6Criteria(){
				addr = (struct in6_addr*) malloc(sizeof(struct in6_addr));
				cidr = 0;
			}
			~DestinationIp6Criteria(){}
	
			std::string key() const;
			bool inner_match(Packet* packet);
                        bool inner_match(Packet* packet, int direction){
                                return inner_match(packet);
                        }

			struct in6_addr *addr;
			int cidr;
	};

        class Ip6Criteria : public Criteria{
                public:
                        Ip6Criteria(){
                                addr = (struct in6_addr*) malloc(sizeof(struct in6_addr));
                                cidr = 0;
                        }
                        ~Ip6Criteria(){}
                        std::string key() const;
                        bool inner_match(Packet* packet, int direction);

                        struct in6_addr *addr;
                        int cidr;
        };

	class GroupCriteria : public Criteria{
		public:
			~GroupCriteria(){}
			std::string key() const;
			bool inner_match(Packet* packet);
                        bool inner_match(Packet* packet, int direction){
                                return inner_match(packet);
                        }

			GroupPtr group;
			std::list<GroupPtr> groups;

			CriteriaType type() const {
				return SESSION;
			}
	};

        class IpAliasCriteria : public Criteria{
                public:
                        ~IpAliasCriteria(){}
                        std::string key() const;
                        bool inner_match(Packet* packet, int direction);

                        std::list<AliasPtr> aliases;
                        AliasPtr alias;

                        void refresh();
        };

	class SourceIpAliasCriteria : public Criteria{
		public:
			~SourceIpAliasCriteria(){}
			std::string key() const;
			bool inner_match(Packet* packet);
                        bool inner_match(Packet* packet, int direction){
                                return inner_match(packet);
                        }

			std::list<AliasPtr> aliases;		
			AliasPtr alias;

			void refresh();
	};

	class DestinationIpAliasCriteria : public Criteria{
		public:
			~DestinationIpAliasCriteria(){}
			std::string key() const;
			bool inner_match(Packet* packet);
                        bool inner_match(Packet* packet, int direction){
                                return inner_match(packet);
                        }

			std::list<AliasPtr> aliases;		
			AliasPtr alias;

			void refresh();
	};

	class TimePeriodCriteria : public Criteria{
		public:
			~TimePeriodCriteria(){}
			std::string key() const;
			bool inner_match(Packet* packet);
                        bool inner_match(Packet* packet, int direction){
                                return inner_match(packet);
                        }

			std::list<TimePeriodPtr> periods;
	};

	class IsHttp : public Criteria {
		public:
			~IsHttp(){}
			std::string key() const;
			bool inner_match(Packet* packet);
                        bool inner_match(Packet* packet, int direction){
                                return inner_match(packet);
                        }

			std::list<AliasPtr> aliases;
			CriteriaType type() const {
				return APPLICATION;
			}

                        ConnectionApplicationInfo::Classifier lowPassApplicationFilterType() const {
                                return ConnectionApplicationInfo::Classifier::HTTP;
                        }
	};

	class HttpContentType : public Criteria {
		public:
			~HttpContentType(){}
			std::string key() const;
			std::list<AliasPtr> aliases;
			bool inner_match(Packet* packet);
                        bool inner_match(Packet* packet, int direction){
                                return inner_match(packet);
                        }
			
			CriteriaType type() const {
				return APPLICATION;
			}

			ConnectionApplicationInfo::Classifier lowPassApplicationFilterType() const {
				return ConnectionApplicationInfo::Classifier::HTTP;
			}

	};	

	class HttpUserAgent : public Criteria {
		public:
			~HttpUserAgent(){}
			std::string key() const;
			std::list<AliasPtr> aliases;
			bool inner_match(Packet* packet);
                        bool inner_match(Packet* packet, int direction){
                                return inner_match(packet);
                        }

			CriteriaType type() const {
				return APPLICATION;
			}

			ConnectionApplicationInfo::Classifier lowPassApplicationFilterType() const {
				return ConnectionApplicationInfo::Classifier::HTTP;
			}
	};

	class HttpHostname : public Criteria{
		public:
			~HttpHostname(){}
			std::string key() const;
			bool inner_match(Packet* packet);
                        bool inner_match(Packet* packet, int direction){
                                return inner_match(packet);
                        }

			virtual bool httphostname_matches(const std::string host);

			std::list<AliasPtr> aliases;
			AliasPtr alias;
			CriteriaType type() const {
				return APPLICATION;
			}

			void refresh();
			ConnectionApplicationInfo::Classifier lowPassApplicationFilterType() const {
				return ConnectionApplicationInfo::Classifier::HTTP;
			}
		private:

			class BanHitListItem {
				public:
					void insert(ConnectionIp *ip);
					bool check(ConnectionIp *ip);
					int size();
					void clear();
				private:
					std::unordered_set<unsigned int> banned;
			} nonTlsBannedItems;

			boost::unordered_map<std::string, int> cache;
	};

	class RegexBasedCriteria : public Criteria {
		public:
			virtual ~RegexBasedCriteria(){}
			std::string expression;
			boost::regex regx;

			void refresh() {
				regx = boost::regex(expression, boost::regex::icase);
			}

			bool regex_matches(std::string input){
				boost::smatch what;
				return boost::regex_search(input, what, regx);
			}
	};

        class HttpRequestRegexCriteria : public RegexBasedCriteria {
                public:
                        ~HttpRequestRegexCriteria(){}
                        std::string key() const;
                        bool inner_match(Packet* packet);
                        bool inner_match(Packet* packet, int direction){
                                return inner_match(packet);
                        }

                        CriteriaType type() const {
                                return APPLICATION;
                        }

                        ConnectionApplicationInfo::Classifier lowPassApplicationFilterType() const {
                                return ConnectionApplicationInfo::Classifier::HTTP;
                        }
        };

	class HttpContentTypeRegex : public RegexBasedCriteria {
		public:
			~HttpContentTypeRegex(){}
			std::string key() const;
			bool inner_match(Packet* packet);
                        bool inner_match(Packet* packet, int direction){
                                return inner_match(packet);
                        }

			CriteriaType type() const {
				return APPLICATION;
			}

			ConnectionApplicationInfo::Classifier lowPassApplicationFilterType() const {
				return ConnectionApplicationInfo::Classifier::HTTP;
			}
	};	

	class HttpUserAgentRegex : public RegexBasedCriteria {
		public:
			~HttpUserAgentRegex(){}
			std::string key() const;
			bool inner_match(Packet* packet);
                        bool inner_match(Packet* packet, int direction){
                                return inner_match(packet);
                        }

			CriteriaType type() const {
				return APPLICATION;
			}

			ConnectionApplicationInfo::Classifier lowPassApplicationFilterType() const {
				return ConnectionApplicationInfo::Classifier::HTTP;
			}
	};

	class HttpHostnameRegex : public HttpHostname {
		public:
			~HttpHostnameRegex(){}
			std::string key() const;
			bool httphostname_matches(const std::string host);
			CriteriaType type() const {
				return APPLICATION;
			}

			ConnectionApplicationInfo::Classifier lowPassApplicationFilterType() const {
				return ConnectionApplicationInfo::Classifier::HTTP;
			}

                        std::string expression;
                        boost::regex regx;

                        void refresh() {
                                regx = boost::regex(expression, boost::regex::icase);
                        }

                        bool regex_matches(std::string input){
                                boost::smatch what;
                                return boost::regex_search(input, what, regx);
                        }
	};

	class DestinationPortCriteria : public Criteria {
		public:
			~DestinationPortCriteria(){}
			std::string key() const;
			bool inner_match(Packet* packet);
                        bool inner_match(Packet* packet, int direction){
                                return inner_match(packet);
                        }

			boost::unordered_set<unsigned int> ports;
	};

	class SourcePortCriteria : public Criteria {
		public:
			~SourcePortCriteria(){}
			std::string key() const;
			bool inner_match(Packet* packet);
                        bool inner_match(Packet* packet, int direction){
                                return inner_match(packet);
                        }

			boost::unordered_set<unsigned int> ports;
	};

        class PortCriteria : public Criteria {
                public:
                        ~PortCriteria(){}
                        std::string key() const;
                        bool inner_match(Packet* packet, int direction);

                        boost::unordered_set<unsigned int> ports;
        };

	class DestinationPortRangeCriteria : public Criteria {
		public:
			~DestinationPortRangeCriteria(){}
			std::string key() const;
			bool inner_match(Packet* packet);
                        bool inner_match(Packet* packet, int direction){
                                return inner_match(packet);
                        }

			int startPort, endPort;
	};

	class SourcePortRangeCriteria : public Criteria {
		public:
			~SourcePortRangeCriteria(){}
			std::string key() const;
			bool inner_match(Packet* packet);
                        bool inner_match(Packet* packet, int direction){
                                return inner_match(packet);
                        }

			int startPort, endPort;
	};

        class PortRangeCriteria : public Criteria {
                public:
                        ~PortRangeCriteria(){}
                        std::string key() const;
                        bool inner_match(Packet* packet, int direction);

                        int startPort, endPort;
        };

	class SourceNetworkDeviceCriteria: public Criteria {
		public:
			~SourceNetworkDeviceCriteria(){}
			std::string key() const;
			bool inner_match(Packet* packet);
                        bool inner_match(Packet* packet, int direction){
                                return inner_match(packet);
                        }

			std::string device;
			InterfacePtr devicePtr;

			void refresh();
	};

	class DestinationNetworkDeviceCriteria: public Criteria {
		public:
			~DestinationNetworkDeviceCriteria(){}
			std::string key() const;
			bool inner_match(Packet* packet);
                        bool inner_match(Packet* packet, int direction){
                                return inner_match(packet);
                        }

			std::string device;
			InterfacePtr devicePtr;

			void refresh();
	};

        class NetworkDeviceCriteria: public Criteria {
                public:
                        ~NetworkDeviceCriteria(){}
                        std::string key() const;
                        bool inner_match(Packet* packet, int direction);

                        std::string device;
                        InterfacePtr devicePtr;

                        void refresh();
        };

	class ProtocolCriteria: public Criteria {
		public:
			~ProtocolCriteria(){}
			std::string key() const;
			bool inner_match(Packet* packet);
                        bool inner_match(Packet* packet, int direction){
                                return inner_match(packet);
                        }

			proto type;
	};

	class UsersCriteria: public Criteria {
		public:
			~UsersCriteria(){}
			std::string key() const;
			bool inner_match(Packet* packet);
                        bool inner_match(Packet* packet, int direction){
                                return inner_match(packet);
                        }

			boost::unordered_set<UserPtr> users;
			CriteriaType type() const {
				return SESSION;
			}
	};

	class MacAddressCriteria: public Criteria {
		public:
			~MacAddressCriteria(){}
			std::string key() const;
			bool inner_match(Packet* packet);
                        bool inner_match(Packet* packet, int direction){
                                return inner_match(packet);
                        }

			boost::unordered_set<std::string> macs;
	};

	class SignatureCriteria: public Criteria {
		public:
			~SignatureCriteria(){}
			std::string key() const;
			bool inner_match(Packet* packet);
                        bool inner_match(Packet* packet, int direction){
                                return inner_match(packet);
                        }

			std::list<SignaturePtr> resolved_signatures;
			std::list<std::string> unresolved_signatures;

			void refresh();
			CriteriaType type() {
				return APPLICATION_EXPENSIVE;
			}

			ConnectionApplicationInfo::Classifier lowPassApplicationFilterType() const {
				return ConnectionApplicationInfo::Classifier::UNKNOWN;
			}
	};

	class FingerprintCriteria: public Criteria {
		public:
			~FingerprintCriteria(){}
			std::string key() const;
			bool inner_match(Packet* packet);
			bool inner_match(Packet* packet, int direction){
				return inner_match(packet);
			}
			void refresh();
			std::list<string> fingerprint_ids;
			std::list<FingerprintPtr> fingerprints;
			CriteriaType type() {
				return APPLICATION_EXPENSIVE;
			}

			ConnectionApplicationInfo::Classifier lowPassApplicationFilterType() const {
				return ConnectionApplicationInfo::Classifier::UNKNOWN;
			}
	};

	class UserQuotaExceededCriteria : public Criteria {
		public:
			~UserQuotaExceededCriteria(){}
			std::string key() const;
			bool inner_match(Packet* packet);
                        bool inner_match(Packet* packet, int direction){
                                return inner_match(packet);
                        }
	};

	class GlobalQuotaExceededCriteria : public Criteria {
		public:
			~GlobalQuotaExceededCriteria(){}
			std::string key() const;
			bool inner_match(Packet* packet);
                        bool inner_match(Packet* packet, int direction){
                                return inner_match(packet);
                        }
	};

	class HostQuarantinedCriteria: public Criteria {
		public:
			~HostQuarantinedCriteria(){}
			std::string key() const;
			bool inner_match(Packet* packet);
                        bool inner_match(Packet* packet, int direction){
                                return inner_match(packet);
                        }
	};

	class AuthenticatedCriteria: public Criteria {
		public:
			~AuthenticatedCriteria(){}
			std::string key() const;
			bool inner_match(Packet* packet);
                        bool inner_match(Packet* packet, int direction){
                                return inner_match(packet);
                        }
	};


	class NotAuthenticatedCriteria: public Criteria {
		public:
			~NotAuthenticatedCriteria(){}
			std::string key() const;
			bool inner_match(Packet* packet);
                        bool inner_match(Packet* packet, int direction){
                                return inner_match(packet);
                        }
	};

	class GeoIPCriteriaBase: public Criteria {
		public:
			GeoIPCriteriaBase(){
				this->alias = NULL;
			}
			~GeoIPCriteriaBase(){}

			void refresh();
			std::string country;

		protected:
			IpRangeAlias* alias;
	};

	class GeoIPSourceCriteria : public GeoIPCriteriaBase {
		public:
			~GeoIPSourceCriteria(){}
			std::string key() const;
			bool inner_match(Packet* packet);
                        bool inner_match(Packet* packet, int direction){
                                return inner_match(packet);
                        }
	};

	class GeoIPDestinationCriteria : public GeoIPCriteriaBase {
		public:
			~GeoIPDestinationCriteria(){}
			std::string key() const;
			bool inner_match(Packet* packet);
                        bool inner_match(Packet* packet, int direction){
                                return inner_match(packet);
                        }
	};

        class GeoIPCriteria : public GeoIPCriteriaBase {
                public:
                        ~GeoIPCriteria(){}
                        std::string key() const;
                        bool inner_match(Packet* packet, int direction);
        };

	class SourceMacAddressPoolCriteria : public Criteria {
		public:
			~SourceMacAddressPoolCriteria(){}
			std::string key() const;
			bool inner_match(Packet* packet);
                        bool inner_match(Packet* packet, int direction){
                                return inner_match(packet);
                        }

			std::list<AliasPtr> aliases;
	};

	class RegexCriteria : public Criteria {
		public:
			virtual ~RegexCriteria(){}
			std::string key() const;
			bool inner_match(Packet* packet);
                        bool inner_match(Packet* packet, int direction){
                                return inner_match(packet);
                        }

			std::string expression;

			void refresh(){
				reg = boost::regex(expression, boost::regex::icase);
			}

		private:
			boost::regex reg;
	};

	class SecurityGroupCriteria : public Criteria {
		public:
			~SecurityGroupCriteria(){}
			std::string key() const;
                        bool inner_match(Packet* packet, int direction);
	
			std::list<SecurityGroupPtr> groups;
			std::list<string> security_group_ids;

			void refresh();
	};

	class CriteriaBuilder {
		public:
			static ObjectContainer* serialize(Criteria* criteria);
			static Criteria* parse(ObjectContainer* options);
	};

	typedef boost::shared_ptr<Criteria> CriteriaPtr;
};

#endif
