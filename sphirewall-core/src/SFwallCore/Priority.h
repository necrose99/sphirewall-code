/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef PRIORITY_H
#define PRIORITY_H

#include <vector>
#include "Packetfwd.h"
#include "SFwallCore/Alias.h"
#include "Core/Logger.h"
#include "Auth/UserDb.h"
#include "Auth/GroupDb.h"
#include "SFwallCore/TimePeriods.h"
#include "Utils/Interfaces.h"
#include "Utils/Utils.h"

class ConfigurationManager;
class ObjectContainer;
class User;
namespace SFwallCore {
	class ACLStore;
	class FilteringRuleProvider;

	class Criteria;
	typedef boost::shared_ptr<Criteria> CriteriaPtr;
	class AliasDb;

	class PriorityRule : public Lockable {
		public:
			PriorityRule();

			bool enabled;
			std::list<CriteriaPtr> source_criteria;
			std::list<CriteriaPtr> destination_criteria;

			/* This method aquires and holds the lock */
			bool supermatch(SFwallCore::Packet *packet, GroupDb *groupDb);
			int nice;
			bool valid;
			std::string id;
			std::string comment;
	};

	typedef boost::shared_ptr<PriorityRule> PriorityRulePtr;
	class PriorityAclStore : public Lockable, public Configurable, public AliasRemovedListener {
		public:
			class InterfaceChangeListener : public virtual IntMgrChangeListener {
				public:
					InterfaceChangeListener(PriorityAclStore *store);
					void interface_change();
				private:
					PriorityAclStore* store;
			};

			PriorityAclStore(UserDb *userdb, GroupDb *groupdb, AliasDb *aliases, TimePeriodStore *tps);
			PriorityAclStore();
			int loadAclEntry(PriorityRulePtr rule, bool add=false);
			int unloadAclEntry(PriorityRulePtr rule);

			PriorityRulePtr getRuleById(std::string id);
			void movedown(PriorityRulePtr rule);
			void moveup(PriorityRulePtr rule);

			PriorityRulePtr match_rule(Packet *);
			void initLazyObjects(PriorityRulePtr rule);

			void save();
			bool load();
			void setInterfaceManager(IntMgr* im);
			void aliasRemoved(AliasPtr alias);

			vector<PriorityRulePtr> listPriorityRules();
			const char* getConfigurationSystemName(){
				return "Filtering rule store";
			}

		private:
			int findPos(PriorityRulePtr rule);
			UserDb *userDb;
			GroupDb *groupDb;
			TimePeriodStore *tps;
			AliasDb *aliases;
			IntMgr* interfaceManager;

			vector<PriorityRulePtr> priorityQosRules;

			void serializeRule(ObjectContainer *rule, SFwallCore::PriorityRulePtr target);
			void deserializeRule(ObjectContainer *source, SFwallCore::PriorityRulePtr entry);
	};
}
#endif
