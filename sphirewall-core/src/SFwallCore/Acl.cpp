/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <vector>
#include <time.h>
#include <sys/types.h>
#include <sstream>
#include <map>
#include <fstream>
#include <string>

using namespace std;

#include "Utils/IP4Addr.h"
#include "Utils/IP6Addr.h"
#include "SFwallCore/Firewall.h"
#include "SFwallCore/Acl.h"
#include "SFwallCore/Alias.h"
#include "Utils/Utils.h"
#include "Utils/StringUtils.h"
#include "Json//JSON.h"
#include "Json/JSONValue.h"
#include "Core/ConfigurationManager.h"
#include "Utils/TimeWrapper.h"
#include "Core/System.h"
#include "SFwallCore/TimePeriods.h"
#include "Packet.h"
#include "Auth/User.h"
#include "SFwallCore/Criteria.h"

SFwallCore::Nat::Nat() : type(NO_NAT) {}

SFwallCore::ACLStore::ACLStore(UserDb *userdb, GroupDb *groupdb, SFwallCore::AliasDb *aliases, TimePeriodStore *tps)
	: userDb(userdb),
	  groupDb(groupdb),
	  aliases(aliases),
	  tps(tps) {
	for (int i = 1; i <= 5; i++) {
		this->group_labels[i] = "Group " + std::to_string(i);
	}
}

SFwallCore::ACLStore::ACLStore() {}

void SFwallCore::ACLStore::setInterfaceManager(IntMgr* interfaceManager) {
	this->interfaceManager = interfaceManager;
}

std::vector<SFwallCore::FilterRulePtr> SFwallCore::ACLStore::listFilterRules() {
	holdLock();
	vector<SFwallCore::FilterRulePtr> ret;
	ret = entries;
	releaseLock();
	return ret;
}

SFwallCore::FilterRule::FilterRule()
{
	this->log = false;
	this->enabled = false;
	this->valid = false;
	this->count = 0;
	this->last_hit = 0;
	this->ignoreconntrack = false;
	this->ignore_application_layer_filters = false;
	this->action = 0;
	this->group = 0;
	this->hits_per_minute = new FlexibleSecondIntervalSampler();
}

SFwallCore::FilterRulePtr SFwallCore::ACLStore::match_rule(Packet *packet) {
	if (packet == NULL) {
		return SFwallCore::FilterRulePtr();
	}

	holdLock();
	for (auto fr : entries) {
		if (fr->supermatch(packet, groupDb)) {
			fr->count++;
			fr->last_hit = time(NULL);
			fr->hits_per_minute->input(1);

			releaseLock();
			return fr;
		}
	}
	releaseLock();
	return SFwallCore::FilterRulePtr();
}


bool SFwallCore::FilterRule::isActive() {
	return enabled;
}

bool SFwallCore::FilterRule::valueInRange(int lower, int value, int higher) {
	return (value >= lower && value <= higher);
}

bool SFwallCore::FilterRule::supermatch(Packet *packet, GroupDb *groupDb) {
	holdLock();
	if(!isActive()){
		releaseLock();
		return false;
	}

	for(auto c : source_criteria){
		if(!c->match(packet, DIRECTION_SOURCE)){
			releaseLock();
			return false;
		}
	}

        for(auto c : destination_criteria){
                if(!c->match(packet, DIRECTION_DESTINATION)){
			releaseLock();
                        return false;
                }
        }

	releaseLock();
	return true;
}

void translate(SFwallCore::CriteriaPtr criteria, list<SFwallCore::CriteriaPtr>& source,  list<SFwallCore::CriteriaPtr>& destination){
	if(criteria->key().compare("source.ipv4") == 0){
		SFwallCore::SourceIpCriteria* rc = dynamic_cast<SFwallCore::SourceIpCriteria*>(criteria.get());
		SFwallCore::IpCriteria* nc = new SFwallCore::IpCriteria();
		nc->ip = rc->ip;
		nc->mask = rc->mask;
		
		source.push_back(SFwallCore::CriteriaPtr(nc));

	}else if(criteria->key().compare("destination.ipv4") == 0){
                SFwallCore::DestinationIpCriteria* rc = dynamic_cast<SFwallCore::DestinationIpCriteria*>(criteria.get());
                SFwallCore::IpCriteria* nc = new SFwallCore::IpCriteria();
                nc->ip = rc->ip;
                nc->mask = rc->mask;

                destination.push_back(SFwallCore::CriteriaPtr(nc));

        }else if(criteria->key().compare("source.ipv4.alias") == 0){
		SFwallCore::SourceIpAliasCriteria* rc = dynamic_cast<SFwallCore::SourceIpAliasCriteria*>(criteria.get());
		SFwallCore::IpAliasCriteria* nc = new SFwallCore::IpAliasCriteria();
		nc->aliases = rc->aliases;
		nc->alias = rc->alias;
		source.push_back(SFwallCore::CriteriaPtr(nc));

	}else if(criteria->key().compare("destination.ipv4.alias") == 0){
                SFwallCore::DestinationIpAliasCriteria* rc = dynamic_cast<SFwallCore::DestinationIpAliasCriteria*>(criteria.get());
                SFwallCore::IpAliasCriteria* nc = new SFwallCore::IpAliasCriteria();
                nc->aliases = rc->aliases;
                nc->alias = rc->alias;
                destination.push_back(SFwallCore::CriteriaPtr(nc));

        }else if(criteria->key().compare("source.device") == 0){
                SFwallCore::SourceNetworkDeviceCriteria* rc = dynamic_cast<SFwallCore::SourceNetworkDeviceCriteria*>(criteria.get());
                SFwallCore::NetworkDeviceCriteria* nc = new SFwallCore::NetworkDeviceCriteria();
		nc->device = rc->device;
                source.push_back(SFwallCore::CriteriaPtr(nc));

        }else if(criteria->key().compare("destination.device") == 0){
                SFwallCore::DestinationNetworkDeviceCriteria* rc = dynamic_cast<SFwallCore::DestinationNetworkDeviceCriteria*>(criteria.get());
                SFwallCore::NetworkDeviceCriteria* nc = new SFwallCore::NetworkDeviceCriteria();
                nc->device = rc->device;
                destination.push_back(SFwallCore::CriteriaPtr(nc));

        }else if(criteria->key().compare("source.transport.port") == 0){
		SFwallCore::SourcePortCriteria* rc = dynamic_cast<SFwallCore::SourcePortCriteria*>(criteria.get());
		SFwallCore::PortCriteria* nc = new SFwallCore::PortCriteria();
		nc->ports = rc->ports;
		source.push_back(SFwallCore::CriteriaPtr(nc));

	}else if(criteria->key().compare("destination.transport.port") == 0){
                SFwallCore::DestinationPortCriteria* rc = dynamic_cast<SFwallCore::DestinationPortCriteria*>(criteria.get());
                SFwallCore::PortCriteria* nc = new SFwallCore::PortCriteria();
                nc->ports = rc->ports;
                destination.push_back(SFwallCore::CriteriaPtr(nc));

        }else if(criteria->key().compare("source.transport.portrange") == 0){
		SFwallCore::SourcePortRangeCriteria* rc = dynamic_cast<SFwallCore::SourcePortRangeCriteria*>(criteria.get());
		SFwallCore::PortRangeCriteria* nc = new SFwallCore::PortRangeCriteria();
		nc->startPort = rc->startPort;
		nc->endPort = rc->endPort;
		source.push_back(SFwallCore::CriteriaPtr(nc));

	}else if(criteria->key().compare("destination.transport.portrange") == 0){
                SFwallCore::DestinationPortRangeCriteria* rc = dynamic_cast<SFwallCore::DestinationPortRangeCriteria*>(criteria.get());
                SFwallCore::PortRangeCriteria* nc = new SFwallCore::PortRangeCriteria();
                nc->startPort = rc->startPort;
                nc->endPort = rc->endPort;
                destination.push_back(SFwallCore::CriteriaPtr(nc));
        }else{
		source.push_back(criteria);
	}
} 


void SFwallCore::ACLStore::deserializeRule(ObjectContainer *source, SFwallCore::FilterRulePtr entry) {
	if (source->has("id")) {
		entry->id = source->get("id")->string();
	}

	if (source->has("enabled")) {
		entry->enabled = source->get("enabled")->boolean();
	}

	if (source->has("group")) {
		entry->group = source->get("group")->number();
	}

	if(source->has("criteria")){
		ObjectContainer* criteria = source->get("criteria")->container();
		for(int y = 0; y < criteria->size(); y++){
			CriteriaPtr c = CriteriaPtr(CriteriaBuilder::parse(criteria->get(y)->container()));
			if(c){
				translate(c, entry->source_criteria, entry->destination_criteria);
			}
		}
	}

	if(source->has("source_criteria")){
		ObjectContainer* criteria = source->get("source_criteria")->container();
		for(int y = 0; y < criteria->size(); y++){
			CriteriaPtr c = CriteriaPtr(CriteriaBuilder::parse(criteria->get(y)->container()));
			if(c){
				entry->source_criteria.push_back(c);
			}
		}
	}

	if(source->has("destination_criteria")){
		ObjectContainer* criteria = source->get("destination_criteria")->container();
		for(int y = 0; y < criteria->size(); y++){
			CriteriaPtr c = CriteriaPtr(CriteriaBuilder::parse(criteria->get(y)->container()));
			if(c){
				entry->destination_criteria.push_back(c);
			}
		}
	}
}

bool SFwallCore::ACLStore::load() {
	int loaded = 0;

	ObjectContainer *root;
	entries.clear();

	if (configurationManager->has("filterAcls")) {
		root = configurationManager->getElement("filterAcls");
		ObjectContainer *rules = root->get("rules")->container();

		if (root->has("group_labels")) {
			ObjectContainer *groups = root->get("group_labels")->container();
			for (int y = 0; y < groups->size(); y++) {
				group_labels[groups->get(y)->container()->get("id")->number()] = groups->get(y)->container()->get("label")->string();
			}
		}

		for (int x = 0; x < rules->size(); x++) {
			FilterRulePtr entry = FilterRulePtr(new FilterRule());
			ObjectContainer *source = rules->get(x)->container();

			deserializeRule(source, entry);

			if (source->has("log")) {
				entry->log = source->get("log")->boolean();
			}

			if (source->has("action")) {
				entry->action = source->get("action")->number();
			}

			if (source->has("group")) {
				entry->group = source->get("group")->number();
			}

			if (source->has("comment")) {
				entry->comment = source->get("comment")->string();
			}

			if (source->has("ignoreconntrack")) {
				entry->ignoreconntrack = source->get("ignoreconntrack")->boolean();
			}

			loadAclEntry(entry, true);
			loaded++;
		}
	}

	if (loaded == 0) {
		Logger::instance()->log("sphirewalld.firewall.acls", INFO, "No filter nat rules found, loading default accept all ruleset");
		FilterRulePtr def= FilterRulePtr(new FilterRule());
		def->action = SQ_ACCEPT;
		def->enabled = true;
		loadAclEntry(def);
	}

	return true;
}

void SFwallCore::ACLStore::serializeRule(ObjectContainer *rule, SFwallCore::FilterRulePtr target) {
	if (target->comment.size() > 0) {
		rule->put("comment", new ObjectWrapper((string) target->comment));
	}

	rule->put("id", new ObjectWrapper((string) target->id));

	ObjectContainer* target_source_criteria = new ObjectContainer(CARRAY);
	for(auto c: target->source_criteria){
		target_source_criteria->put(new ObjectWrapper(CriteriaBuilder::serialize(c.get())));
	}
	rule->put("source_criteria", new ObjectWrapper(target_source_criteria));

	ObjectContainer* target_destination_criteria = new ObjectContainer(CARRAY);
	for(auto c: target->destination_criteria){
		target_destination_criteria->put(new ObjectWrapper(CriteriaBuilder::serialize(c.get())));
	}
	rule->put("destination_criteria", new ObjectWrapper(target_destination_criteria));
}

void SFwallCore::ACLStore::save() {
	int loaded = 0;

	ObjectContainer *root = new ObjectContainer(CREL);
	ObjectContainer *rules = new ObjectContainer(CARRAY);
	ObjectContainer *groups = new ObjectContainer(CARRAY);

	for (auto iter : group_labels) {
		ObjectContainer *grp = new ObjectContainer(CREL);
		grp->put("id", new ObjectWrapper((double) iter.first));
		grp->put("label", new ObjectWrapper((string) iter.second));
		groups->put(new ObjectWrapper(grp));
	}
	root->put("group_labels", new ObjectWrapper(groups));

	for (int x = 0; x < (int) entries.size(); x++) {
		ObjectContainer *rule = new ObjectContainer(CREL);
		serializeRule(rule, entries[x]);
		rule->put("id", new ObjectWrapper((string) entries[x]->id));
		rule->put("enabled", new ObjectWrapper((bool) entries[x]->enabled));
		rule->put("action", new ObjectWrapper((double) entries[x]->action));
		rule->put("group", new ObjectWrapper((double) entries[x]->group));
		rule->put("ignoreconntrack", new ObjectWrapper((bool) entries[x]->ignoreconntrack));
		rule->put("log", new ObjectWrapper((bool) entries[x]->log));
		loaded++;
		rules->put(new ObjectWrapper(rule));
	}

	root->put("rules", new ObjectWrapper(rules));

	configurationManager->holdLock();
	configurationManager->setElement("filterAcls", root);
	configurationManager->save();
	configurationManager->releaseLock();
}

void SFwallCore::FilterRule::refresh(){
	holdLock();
	valid = true;

	for(auto c : source_criteria){
		c->refresh();
	}

	for(auto c : destination_criteria){
		c->refresh();
	}
	releaseLock();
}

void SFwallCore::ACLStore::initLazyObjects(FilterRulePtr rule) {
	rule->refresh();
}

void SFwallCore::ACLStore::movedown(FilterRulePtr rule) {
	holdLock();
	int x = findPos(rule);

	if ((x+ 1) < (int) entries.size()) {
		FilterRulePtr a = entries[x];
		FilterRulePtr b = entries[x+ 1];

		entries[x+ 1] = a;
		entries[x]     = b;
	}
	releaseLock();
	save();
}

void SFwallCore::ACLStore::moveup(FilterRulePtr rule) {
	holdLock();
	int x = findPos(rule);

	if ((x - 1) >= 0) {
		FilterRulePtr a = entries[x];
		FilterRulePtr b = entries[x - 1];

		entries[x - 1] = a;
		entries[x]     = b;
	}
	releaseLock();
	save();
}

int SFwallCore::ACLStore::findPos(FilterRulePtr rule) {
	for (uint x = 0; x < entries.size(); x++) {
		FilterRulePtr target = entries[x];

		if (target == rule) {
			return x;
		}
	}
	return -1;
}

SFwallCore::FilterRulePtr SFwallCore::ACLStore::getRuleById(std::string id) {
	holdLock();
	for (auto rule : entries) {
		if (rule->id.compare(id) == 0) {
			releaseLock();
			return rule;
		}
	}
	releaseLock();
	return SFwallCore::FilterRulePtr();
}

void SFwallCore::ACLStore::InterfaceChangeListener::interface_change(InterfacePtr interface) {
	for (auto entry : store->listFilterRules()) {
		entry->refresh();
	}
}

SFwallCore::ACLStore::InterfaceChangeListener::InterfaceChangeListener(SFwallCore::ACLStore *store) : store(store)
{}

int SFwallCore::ACLStore::loadAclEntry(FilterRulePtr rule, bool add){
	initLazyObjects(rule);

	holdLock();
	if(rule->id.size() == 0 || add){
		rule->id = StringUtils::genRandom();
		entries.push_back(rule);
	}
	releaseLock();
}

int SFwallCore::ACLStore::unloadAclEntry(FilterRulePtr rule){
	holdLock();
	int pos = findPos(rule);
	if(pos != -1){
		entries.erase(entries.begin() + pos);
	}
	releaseLock();
	save();
}

void SFwallCore::ACLStore::sample(map<string, double> &input){
	holdLock();
	for(auto rule : entries){
		stringstream ss;
		ss << "firewall.acl." << rule->id << ".hits_per_minute";
		input[ss.str()] = rule->hits_per_minute->value();
	}
	releaseLock();
}
