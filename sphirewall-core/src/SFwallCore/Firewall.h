/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef FIREWALL_H
#define FIREWALL_H

#include <map>
#include <list>
#include <vector>
#include <queue>

#include "Core/Logger.h"
#include "Core/Config.h"
#include "Acl.h"
#include "Priority.h"
#include "Alias.h"
#include "Kernel/client.h"
#include "TrafficShaper/TrafficShaper.h"
#include "Core/Cron.h"
#include "Core/SysMonitor.h"

class ConfigurationManager;
class Sampler;

void translate(SFwallCore::CriteriaPtr criteria, list<SFwallCore::CriteriaPtr>& source,  list<SFwallCore::CriteriaPtr>& destination);

namespace SFwallCore {
	class PlainConnTracker;
	class SNatConnTracker;
	class DNatConnTracker;
	class ApplicationLayerTracker;
	class ApplicationLayerFilterHandler;
	class CapturePortalEngine;
	class ManagementLocator;
	class ApplicationFilter;
	class GoogleSafeSearchEngine;
	class MultipathLoadBalancer;
	class NatAclStore;
	class SignatureStore;
	class MyLinewizeRedirectHelper;
	class SecurityGroupStore;
	class FingerprintStore;

	class GarbageCollectorCron : public CronJob {
		public:

			GarbageCollectorCron(PlainConnTracker *connectionTracker) :
				CronJob(60 * 5, "CONNTRACKER_GARBAGECOLLECTOR_CRON", true),
				connectionTracker(connectionTracker) {
				waiting = false;
			}

			void run();
			bool waiting;

		private:
			PlainConnTracker *connectionTracker;
	};

	enum {
		EXTERNAL_HANDLER__IGNORE = 0,
		EXTERNAL_HANDLER__ALLOW = 1,
		EXTERNAL_HANDLER__DENY = 2
	};

	class ExternalFirewallHandler {
		public:
			virtual int efh__prerouting_handle(Packet* packet) {
				return EXTERNAL_HANDLER__IGNORE;
			}

			virtual int efh__filter_handle(Packet* packet){
				return EXTERNAL_HANDLER__IGNORE;
			}

			virtual int efh__postrouting_handle(Packet* packet) {
				return EXTERNAL_HANDLER__IGNORE;
			}

			virtual bool efh__enabled(){
				return false;
			}			
	};

	class Firewall : public MetricSampler, public Configurable {
		public:
			static int TCP_CONNECTION_TIMEOUT_THESHOLD;
			static int UDP_CONNECTION_TIMEOUT_THESHOLD;
			static int ICMP_CONNECTION_TIMEOUT_THESHOLD;

			static int SYN_SYNACK_WAIT_TIMEOUT;
			static int SYN_ACKACK_TIMEOUT;
			static int FIN_FINACK_WAIT_TIMEOUT;
			static int FIN_WAIT_TIMEOUT;
			static int TIME_WAIT_TIMEOUT;
			static int CLOSE_WAIT_TIMEOUT;
			static int LAST_ACK_TIMEOUT;
			static int CACHE_TTL_MAX;
			static int SQUEUE_DEQUEUE_YIELD_TIMEOUT;
			static int SQUEUE_DEQUEUE_YIELD_SLEEP_INTERVAL;

			static int POST_PROCESSOR_ENABLED;
			static int BLOCK_LIST_ENABLED;
			static int PREROUTING_ENABLED;
			static int POSTROUTING_ENABLED;
			static int QOS_ENABLED;
			static int FIREWALL_FORCE_AUTHENTICATION;
			static int BANDWIDTHDB_ENABLED;
			static int REWRITE_ENABLED;
			static int REWRITE_PROXY_REQUESTS_ENABLED;
			static int HTTP_FILTERING_ENABLED;

			static int FIREWALL_IPV4_ENABLED;
			static int FIREWALL_IPV6_ENABLED;

			static const int PLAIN_CONN_HASH_MAX = 200000;
			static int BLOCK_HTTP_FILTERING_TLS_SSLV3_RETRY;
			static int IGNORE_MAC_ADDRESS;
			static int AGGRESIVE_APPLICATION_FILTERING;
			static int BLOCK_HTTP_FILTERING_TLS_SSLV3_RETRY_THRESHOLD;
			static int CONN_TRACKER_DEADLOCK_PROTECTION_ENABLED;
			static int TCP_CONNECTION_TRACKER_COLLECT_DETAILED_STATS;
			static int MULTIPATH_WAN_LOADBALANCING;
			static int ANONYMIZE_WEBSITE_DATA;
			static int ANONYMIZE_USER_DATA;
			static int RESIZE_TCP_ADJUST_SEQ_ENABLED;
			static int APPLICATION_FILTERING_MIN_PAYLOAD_SIZE;
			static int APPLICATION_LAYER_FILTERS_ENABLED;
			static int APPLICATION_LAYER_PROTOCOL_HANDLERS_ENABLED;
			static int APPLICATION_LAYER_PROTOCOL_HANDLERS_DNS_ENABLED;
			static int WEBSITE_ALIAS_CACHE_ENABLED;
			static int ALIAS_CACHE_SIZE_MAX;
			static int ENABLE_FIREWALL_EXCEPTION_HANDLERS;
			static int ENABLE_APPLICATION_LAYER_TAGGING;
			static int ENABLE_FIREWALL_ALLOW_LOCAL_OUTBOUND;
			static int DEFAULT_VERDICT_DENY;
			static int FINGERPRINT_RETRY_COUNT;
			static int FINGERPRINT_RETRY_INTERVAL;

			TrafficShaper *trafficShaper;
			SqClient sqClient;
			ACLStore *acls;
			PriorityAclStore* priority_acls;
			PlainConnTracker *connectionTracker;
			GarbageCollectorCron *garbageCron;
			SecurityGroupStore* security_groups_store;

			Firewall(Config *config);
			Firewall();
			virtual ~Firewall();

			void start();
			void stop();

			string filterFile;
			string natFile;
			string aliasFilename;
			int defaultAction;
			int defaultCount;

			AliasDb *aliases;

			int totalTcpUp;
			int totalTcpDown;
			int totalUdpUp;
			int totalUdpDown;

			bool debug;

			long totalTcpPackets;
			long totalUdpPackets;
			long totalIcmpPackets;
			long totalDefaultAction;
			long totalAllowed;
			long totalDenied;
			long totalReset;
			long totalRewrites;
			long totalQueued;
			long totalTcpWindowResized;

			Sampler *avgPacketProcess;

			PlainConnTracker *getPlainConnTracker() const {
				return connectionTracker;
			}

			void setInterfaceManager(IntMgr* i) {
				this->interfaceManager = i;
			}

			Sampler *transferSampler;
			void sample(map<string, double> &input);
			ApplicationLayerTracker *applicationLayerTracker;
			std::list<ApplicationLayerFilterHandler *> applicationLayerFilteringHandlers;

			CapturePortalEngine *capturePortalEngine;
			ManagementLocator *managementLocator;
			ApplicationFilter *httpFilter;
			GoogleSafeSearchEngine *googleSafeSearchEngine;
			MyLinewizeRedirectHelper* my_linewize_helper;
			MultipathLoadBalancer *multiPathWanLoadBalancer;
			NatAclStore *natAcls;
			SignatureStore* signatureStore;
			FingerprintStore* fingerprint_store;

			bool load();
			void registerConfigurables(ConfigurationManager* configurationManager);
			const char* getConfigurationSystemName(){
				return "Firewall core";
			}

			void register_external_firewall_handler(ExternalFirewallHandler* handler){
				external_firewall_handlers.push_back(handler);
			}
			std::list<ExternalFirewallHandler*> external_firewall_handlers;
		private:
			IntMgr* interfaceManager;
			Config* config;
	};

        class LocalOutboundExternalFirewallHandler : public ExternalFirewallHandler {
                public:
                        int efh__filter_handle(Packet* packet);
                        virtual bool efh__enabled(){
                                return Firewall::ENABLE_FIREWALL_ALLOW_LOCAL_OUTBOUND == 1;
                        }
        };
}

#endif
