/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <linux/icmp.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <netinet/tcp.h>
#include <netinet/udp.h>
#include <arpa/inet.h>
#include <sstream>
#include "SFwallCore/Firewall.h"
#include "SFwallCore/ConnTracker.h"
#include "SFwallCore/Packet.h"
#include "SFwallCore/Connection.h"
#include "SFwallCore/Rewrite.h"
#include "SFwallCore/State.h"
#include "Utils/Utils.h"
#include "Core/System.h"
#include "Utils/TimeWrapper.h"
#include "Utils/IP6Addr.h"
#include "Core/HostDiscoveryService.h"

using namespace std;

/* Plain, snat, dnat connections for each protocol */
SFwallCore::Connection::~Connection() {
	if(hostptr){
		hostptr->total_active_connections--;
	}

	delete rewriteRule;
	delete ip;
	delete applicationInfo;
}


void SFwallCore::TcpConnection::resizePayload(PacketDirection direction, int diff) {
	if (direction == DIR_SAME) {
		seqack_recv_offset += diff;
	}
	else {
		seqack_send_offset += diff;
	}
}

bool SFwallCore::TcpConnection::adjustSeqAckWindow(SFwallCore::Packet *packet) {
	if (seqack_recv_offset > 0) {
		struct tcphdr *tcpFrame = (struct tcphdr *) packet->getTransport()->getInternal();

		if (checkPacket(packet, DIR_SAME)) {
			tcpFrame->seq = htonl((ntohl(tcpFrame->seq)) + seqack_recv_offset + 1);
		}
		else {
			tcpFrame->ack_seq = htonl((ntohl(tcpFrame->ack_seq)) - seqack_recv_offset - 1);
		}

		return true;
	}

	return false;
}

HostPtr SFwallCore::Connection::getHostDiscoveryServiceEntry() {
	return hostptr;
}

void SFwallCore::Connection::setHostDiscoveryServiceEntry(HostPtr host) {
	hostptr = host;
}


std::string SFwallCore::ConnectionIpV6::getSrcIpString() {
	return IP6Addr::toString(getSrcIp());
}

std::string SFwallCore::ConnectionIpV4::getSrcIpString() {
	return IP4Addr::ip4AddrToString(getSrcIp());
}

bool SFwallCore::ConnectionIpV6::checkPacket(Packet *packet, PacketDirection filter) {
	if (packet->type() != IPV6) {
		return false;
	}

	PacketV6 *v4 = (PacketV6 *) packet;

	if (filter & DIR_SAME) {
		if (IP6Addr::matches(v4->getSrcIp(), getSrcIp()) && IP6Addr::matches(v4->getDstIp(), getDstIp())) {
			return true;
		}
	}

	if (filter & DIR_OPPOSITE) {
		if (IP6Addr::matches(v4->getSrcIp(), getDstIp()) && IP6Addr::matches(v4->getDstIp(), getSrcIp())) {
			return true;
		}
	}

	return false;
}

InterfacePtr SFwallCore::Connection::getSourceNetDevice() {
	IntMgr* manager = System::getInstance()->get_interface_manager();
	return manager->get_interface_by_id(getSourceDev());
}

InterfacePtr SFwallCore::Connection::getDestNetDevice() {
	IntMgr* manager = System::getInstance()->get_interface_manager();
	return manager->get_interface_by_id(getDestDev());
}

int SFwallCore::Connection::getDestDev() const {
	return this->destDev;
}
int SFwallCore::Connection::getSourceDev() const {
	return this->sourceDev;
}


void SFwallCore::Connection::updateTransferRate(SFwallCore::Packet *packet) {
	if (checkPacket(packet, DIR_SAME)) {
		upload += packet->getLen();
	}
	else {
		download += packet->getLen();
	}
}

int SFwallCore::Connection::hash() const {
	return m_hash;
}

int SFwallCore::Connection::getTime() const {
	return m_time_stamp;
}

TokenBucketPtr SFwallCore::Connection::getQosBucket() const {
	return qos_bucket;
}

void SFwallCore::Connection::increment(SFwallCore::Packet *packet) {
	m_last_packet = time(NULL);
	noPackets++;
	updateTransferRate(packet);
}

void SFwallCore::Connection::setQosBucket(TokenBucketPtr bucket) {
	qos_bucket = bucket;
}

int SFwallCore::Connection::idle() const {
	return (time(NULL) - m_last_packet);
}

SFwallCore::TcpState *SFwallCore::TcpConnection::getState() {
	return &tcpTrackable->state;
}

bool SFwallCore::Connection::hasExpired() {
	if (getProtocol() == TCP) {
		int i = idle();
		return ((TcpConnection *)this)->tcpTrackable->hasExpired(i);
	}
	else if (getProtocol() == UDP) {
		int diff = time(NULL) - m_last_packet;

		if (diff > Firewall::UDP_CONNECTION_TIMEOUT_THESHOLD) {
			return true;
		}

		if (isTerminating() && ((time(NULL) - pleaseTerminate) > 120)) {
			return true;
		}

		return false;
	}
	else if (getProtocol() == ICMP) {
		int diff = time(NULL) - m_last_packet;

		if (diff > Firewall::ICMP_CONNECTION_TIMEOUT_THESHOLD) {
			return true;
		}

		if (isTerminating() && ((time(NULL) - pleaseTerminate) > 120)) {
			return true;
		}

		return false;
	}

	return false;
}

int SFwallCore::Connection::getNoPackets() const {
	return noPackets;
}

double SFwallCore::Connection::getDownload() const {
	return download;
}

double SFwallCore::Connection::getUpload() const {
	return upload;
}

string SFwallCore::Connection::getHwAddress() const {
	return hwAddress;
}

SFwallCore::Connection *SFwallCore::Connection::parseConnection(Packet *packet) {
	Connection *conn = NULL;

	switch (packet->getProtocol()) {
		case TCP:
			conn = new TcpConnection(packet);
			break;

		case UDP:
			conn = new UdpConnection(packet);
			break;

		case ICMP:
			conn = new IcmpConnection(packet);
			break;

		default:
			break;
	};

	if (conn) {
		conn->pleaseTerminate = -1;
	}

	return conn;
}

SFwallCore::Connection::Connection(Packet *packet)
	: rewriteVerdictApplied(false), protocol(-1), nice(1), rewriteRule(NULL), 
	pleaseTerminate(-1), m_hash(packet->hash()), m_time_stamp(time(NULL)), m_icmpId(0),
	noPackets(1),  m_last_packet(time(NULL)), upload(0), download(0), 
	log(false), hostptr(HostPtr()), hwAddress(packet->getHw()), applicationInfo(NULL),
	sourceDev(0), destDev(0), m_sport(0), m_dport(0), ip(NULL) {
		/*Check to see if we must reverse this connection*/
		if (packet->type() == IPV4) {
			PacketV4 *ipv4 = (PacketV4 *) packet;

			if (ipv4->getDstPort() <= IP4Addr::CLIENT_PORT_MIN) {
				ip = new ConnectionIpV4(ipv4->getSrcIp(), ipv4->getDstIp());
				m_dport = ipv4->getDstPort();
				m_sport = ipv4->getSrcPort();
				sourceDev = packet->getSourceDev();
				destDev = packet->getDestDev();
			}
			else {
				ip = new ConnectionIpV4(ipv4->getDstIp(), ipv4->getSrcIp());
				m_dport = ipv4->getSrcPort();
				m_sport = ipv4->getDstPort();
				sourceDev = packet->getDestDev();
				destDev = packet->getSourceDev();
			}
		}
		else if (packet->type() == IPV6) {
			PacketV6 *ipv4 = (PacketV6 *) packet;

			if (ipv4->getDstPort() <= IP4Addr::CLIENT_PORT_MIN) {
				ip = new ConnectionIpV6(ipv4->getSrcIp(), ipv4->getDstIp());
				m_dport = ipv4->getDstPort();
				m_sport = ipv4->getSrcPort();
				sourceDev = packet->getSourceDev();
				destDev = packet->getDestDev();
			}
			else {
				ip = new ConnectionIpV6(ipv4->getDstIp(), ipv4->getSrcIp());
				m_dport = ipv4->getSrcPort();
				m_sport = ipv4->getDstPort();
				sourceDev = packet->getDestDev();
				destDev = packet->getSourceDev();
			}
		}

		applicationInfo = new ConnectionApplicationInfo();
		id = StringUtils::genRandom();
	}

SFwallCore::TcpConnection::TcpConnection(Packet *tcpPacket) : Connection(tcpPacket) {
	m_hash = TcpConnection::hash(tcpPacket);
	tcpTrackable = new TcpTrackable((TcpPacket *) tcpPacket->getTransport());
	protocol = TCP;
	seqack_recv_offset = 0;
	seqack_send_offset = 0;
}

SFwallCore::UdpConnection::UdpConnection(Packet *udpPacket) : Connection(udpPacket) {
	m_hash = UdpConnection::hash(udpPacket);
	protocol = UDP;
}

SFwallCore::IcmpConnection::IcmpConnection(Packet *icmpPacket) : Connection(icmpPacket) {
	m_icmpId = ((IcmpPacket *) icmpPacket->getTransport())->getIcmpId();
	m_hash = IcmpConnection::hash(icmpPacket);
	protocol = ICMP;
}

int SFwallCore::Connection::hash(Packet *packet) {
	return packet->hash() % Firewall::PLAIN_CONN_HASH_MAX;
}

bool SFwallCore::ConnectionIpV4::checkPacket(Packet *packet, PacketDirection filter) {
	if (packet->type() != IPV4) {
		return false;
	}

	PacketV4 *v4 = (PacketV4 *) packet;

	if (filter & DIR_SAME) {
		return v4->getSrcIp() == getSrcIp() && v4->getDstIp() == getDstIp();
	}
	else if (filter & DIR_OPPOSITE) {
		return v4->getSrcIp() == getDstIp() && v4->getDstIp() == getSrcIp();
	}

	return false;
}

bool SFwallCore::Connection::checkPacket(Packet *p, PacketDirection filter) {
	//Check the IP paramaters first:
	if ((getProtocol() == TCP && p->getProtocol() == TCP) || (getProtocol() == UDP && p->getProtocol() == UDP)) {
		TcpUdpPacket *packet = (TcpUdpPacket *) p->getTransport();

		if (!packet)
			return false;

		if (filter & DIR_SAME) {
			if (m_sport == packet->getSrcPort() && m_dport == packet->getDstPort() && ip->checkPacket(p, DIR_SAME)) {
				return true;
			}
		}

		if (filter & DIR_OPPOSITE) {
			if (m_sport == packet->getDstPort() && m_dport == packet->getSrcPort() && ip->checkPacket(p, DIR_OPPOSITE)) {
				return true;
			}
		}

		return false;
	}
	else if (getProtocol() == ICMP && p->getProtocol() == ICMP) {
		IcmpPacket *packet = (IcmpPacket *) p->getTransport();

		if (!packet)
			return false;

		if (filter & DIR_SAME) {
			if (ip->checkPacket(p, DIR_SAME) && m_icmpId == packet->getIcmpId()) {
				return true;
			}
		}

		if (filter & DIR_OPPOSITE) {
			if (ip->checkPacket(p, DIR_OPPOSITE) && m_icmpId == packet->getIcmpId()) {
				return true;
			}
		}

		return false;
	}

	return false;
}

std::string SFwallCore::TcpConnection::toString() {
	/*
	   std::stringstream ret;
	   ret << "Tcp ";
	   ret << IP4Addr::ip4AddrToString(getSrcIp()) << ":" << getSourcePort();
	   ret << " -> ";
	   ret << IP4Addr::ip4AddrToString(getDstIp()) << ":" << getDestinationPort();
	   ret << " Hash:" << m_hash;

	   return ret.str();
	 */
	return "";
}

std::string SFwallCore::UdpConnection::toString() {
	/*
	   std::stringstream ret;
	   ret << "Tcp ";
	   ret << IP4Addr::ip4AddrToString(getSrcIp()) << ":" << getSourcePort();
	   ret << " -> ";
	   ret << IP4Addr::ip4AddrToString(getDstIp()) << ":" << getDestinationPort();
	   ret << " Hash:" << m_hash;

	   return ret.str();
	 */
	return "";
}

std::string SFwallCore::IcmpConnection::toString() {
	/*
	   std::stringstream ret;
	   ret << "Icmp ";
	   ret << IP4Addr::ip4AddrToString(getSrcIp());
	   ret << " -> ";
	   ret << IP4Addr::ip4AddrToString(getDstIp());
	   ret << " Hash:" << m_hash;
	   return ret.str();
	 */
	return "";
}

void SFwallCore::Connection::terminate() {
	TcpConnection *connection = NULL;

	if (getProtocol() == TCP) {
		connection = (TcpConnection *) this;
		connection->getState()->state = RESET;
	}

	pleaseTerminate = time(NULL);
}

bool SFwallCore::Connection::isTerminating() {
	if (pleaseTerminate != -1 && pleaseTerminate != 0) {
		return true;
	}

	return false;
}

void SFwallCore::ConnectionApplicationInfo::setHttpHost(std::string hostname) {
	if (hostname.size() > VALID_HOSTNAME_THREHOLD) {
		http_hostNameSet = true;
		http_hostName = hostname;
		type = HTTP;
	}
}

bool SFwallCore::ConnectionApplicationInfo::http_contains(int direction, int type) {
	switch(type){
		case HTTP_HOSTNAME:
			return http_hostNameSet;
		case HTTP_USERAGENT:
			return http_useragentSet;
		case HTTP_CONTENTTYPE:
			return http_contenttypeSet;
	}

	return false;
}

std::string SFwallCore::ConnectionApplicationInfo::http_get(int direction, int type) {
	switch(type){
		case HTTP_HOSTNAME:
			return http_hostName;
		case HTTP_USERAGENT:
			return http_useragent;
		case HTTP_CONTENTTYPE:
			return http_contenttype;
	}	

	return "";
}

void SFwallCore::ConnectionApplicationInfo::http_set(int direction, std::string key, std::string value) {
	if(direction == REQUEST){	
		if(key.compare("Host") == 0){
			setHttpHost(value);
		}else if(key.compare("User-Agent") == 0){
			http_useragent = value;
			http_useragentSet = true;
		}else if(key.compare("REQUEST") == 0){
			http_request = value;
			http_requestSet = true;
		}
	}
	if(direction == RESPONSE){
		if(key.compare("Content-Type") == 0){
			http_contenttype = value;
			http_contenttypeSet = true;
			//finally do something even cooler
		}
	}
}

SFwallCore::ConnectionApplicationInfo *SFwallCore::Connection::getApplicationInfo() {
	return applicationInfo;
}

SFwallCore::ConnectionIp *SFwallCore::Connection::getIp() {
	return ip;
}

bool SFwallCore::Connection::isRewriteVerdictApplied() {
	return rewriteVerdictApplied;
}

void SFwallCore::Connection::setRewriteVerdictApplied() {
	rewriteVerdictApplied = true;
}

SFwallCore::RewriteRule *SFwallCore::Connection::getRewriteRule() const {
	return rewriteRule;
}

void SFwallCore::Connection::setRewriteRule(SFwallCore::RewriteRule *rewriteRule) {
	if (this->rewriteRule) {
		delete this->rewriteRule;
	}

	this->rewriteRule = rewriteRule;
}

bool SFwallCore::Connection::mustLog() {
	return log;
}

void SFwallCore::Connection::setMustLog(bool i) {
	this->log = i;
}

int SFwallCore::Connection::getSourcePort() const {
	return this->m_sport;
}

int SFwallCore::Connection::getDestinationPort() const {
	return this->m_dport;
}

int SFwallCore::Connection::getProtocol() const {
	return this->protocol;
}


SFwallCore::TcpTrackable *SFwallCore::TcpConnection::getTcpTrackable() const {
	return this->tcpTrackable;
}

SFwallCore::TcpConnection::~TcpConnection() {
	delete tcpTrackable;
}


SFwallCore::IcmpConnection::~IcmpConnection()
{}

SFwallCore::UdpConnection::~UdpConnection()
{}

	SFwallCore::Connection::Connection()
: pleaseTerminate(-1), rewriteVerdictApplied(false), nice(0), noPackets(0), sourceDev(0), destDev(0),
	m_sport(0), m_dport(0), m_hash(0), m_time_stamp(0), m_icmpId(0), m_last_packet(0), upload(0), download(0), log(false),
	protocol(-1), hostptr(), hwAddress(), rewriteRule(NULL), ip(NULL), applicationInfo(NULL)
{}


	SFwallCore::ConnectionApplicationInfo::ConnectionApplicationInfo()
: afVerdictApplied(false), afProcessedRequest(false), afProcessedResponse(false), http_hostNameSet(false), tls(false), type(UNKNOWN),
	httpHeadersParsed(false), httpHeadersResponseParsed(false), dnsQueryName(), dnsQueryNameSet(false), ignore_cp(false), http_requestSet(false), http_useragentSet(false), http_contenttypeSet(false), sslv3(false)
{
	ignore_all_filtering = false;
	tagging_non_http_evaluated = false;
	tagging_http_evaluated = false;
	tagging_ip_evaluated = false;
	http_ssl3IpMatch = false;
	app_filtering_denied = false;
}

SFwallCore::ConnectionIpV6::~ConnectionIpV6()
{}

SFwallCore::ConnectionIpV6::ConnectionIpV6(in6_addr *source, in6_addr *dest) {
	memcpy(&this->source, source, sizeof(struct in6_addr));
	memcpy(&this->destination, dest, sizeof(struct in6_addr));
}

int SFwallCore::ConnectionIpV6::type() const {
	return IPV6;
}

in6_addr *SFwallCore::ConnectionIpV6::getDstIp() {
	return &destination;
}

in6_addr *SFwallCore::ConnectionIpV6::getSrcIp() {
	return &source;
}

int SFwallCore::ConnectionIpV6::hash() {
	int cursor = 1;
	cursor += IP6Addr::hash(getSrcIp());
	cursor += IP6Addr::hash(getDstIp());
	return cursor;
}

in_addr_t SFwallCore::ConnectionIpV4::getDstIp() const {
	return m_destIp;
}

in_addr_t SFwallCore::ConnectionIpV4::getSrcIp() const {
	return m_sourceIp;
}

int SFwallCore::ConnectionIpV4::type() const {
	return IPV4;
}

int SFwallCore::ConnectionIpV4::hash() {
	return m_sourceIp + m_destIp;
}

SFwallCore::ConnectionIpV4::~ConnectionIpV4()
{}

	SFwallCore::ConnectionIpV4::ConnectionIpV4(unsigned int sourceIp, unsigned int destIp)
: m_sourceIp(sourceIp), m_destIp(destIp)
{}

void SFwallCore::ConnectionApplicationInfo::http_clear(int direction) {
	http_hostNameSet = false;
	http_hostName = "";
	http_useragent = "";
	http_useragentSet = false;
	http_contenttype = "";
	http_contenttypeSet = false;
}

UserPtr SFwallCore::Connection::get_authenticated_user(){
	if(getHostDiscoveryServiceEntry() && getHostDiscoveryServiceEntry()->authenticated_user){
		return getHostDiscoveryServiceEntry()->authenticated_user;
	}

	return UserPtr();
}
