/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <sstream>
#include <vector>
#include <boost/regex.hpp>
#include <map>

using namespace std;

#include "Core/System.h"
#include "Utils/IP4Addr.h"
#include "Utils/IP6Addr.h"
#include "SFwallCore/Alias.h"
#include "SFwallCore/ConnTracker.h"
#include "Utils/Utils.h"
#include "Utils/NetworkUtils.h"
#include "Json/JSON.h"
#include "Json/JSONValue.h"
#include "Core/ConfigurationManager.h"
#include <curl/curl.h>
#include "Api/JsonManagementService.h"
#include "SFwallCore/ApplicationLevel/ApplicationFilter.h"
#include "Utils/HttpRequestWrapper.h"

void SFwallCore::AliasDb::setAclStore(SFwallCore::ACLStore *store) {
	this->store = store;
}

void SFwallCore::AliasDb::setResolve(bool r) {
	this->resolve = r;
}

void SFwallCore::AliasDb::registerRemovedListeners(SFwallCore::AliasRemovedListener *target) {
	this->removedListeners.push_back(target);
}

SFwallCore::AliasDb::AliasDb() : resolve(true)
{}


void SFwallCore::AliasDb::cleanUpCaches(){
	for(auto alias : aliases){
		if(alias.second->cacheSize() > System::getInstance()->getFirewall()->ALIAS_CACHE_SIZE_MAX){
			alias.second->flushCache();
		}
	}	
}

int SFwallCore::AliasDb::cacheSize(){
	int ret = 0;
	for(auto alias : aliases){
		ret += alias.second->cacheSize();
	}	
	return ret;
}

SFwallCore::AliasPtr SFwallCore::AliasDb::get(string id) {
	for (auto alias : aliases) {
		if (alias.first.compare(id) == 0) {
			return alias.second;
		}
	}

	return AliasPtr();
}

SFwallCore::AliasPtr SFwallCore::AliasDb::getByName(string id) {
	for (auto alias : aliases){
		if (alias.second->name.compare(id) == 0) {
			return alias.second;
		}
	}

	return AliasPtr();
}

bool SFwallCore::AliasDb::load() {
	ObjectContainer *root;
	aliases.clear();		
	if (configurationManager->has("aliasDb")) {
		root = configurationManager->getElement("aliasDb");
		ObjectContainer *arr = root->get("aliases")->container();

		for (int x = 0; x < arr->size(); x++) {
			Alias *alias = NULL;
			ObjectContainer *o = arr->get(x)->container();
			int type = o->get("type")->number();
			std::string name = o->get("name")->string();
			std::string id = o->get("id")->string();

			switch ((AliasType)type) {
				case IP_RANGE: {
						       alias = new IpRangeAlias();
						       break;
					       }

				case IP_SUBNET: {
							alias = new IpSubnetAlias();
							break;
						}

				case WEBSITE_LIST : {
							    alias = new WebsiteListAlias();
							    break;
						    }

				case STRING_WILDCARD_LIST: {
								   alias = new StringWildcardListAlias();
								   break;
							   }
				case MAC_ADDRESS_LIST : {
					alias = new MacAddressListAlias();
					break;
				}
			}

			if (alias) {
				alias->name = name;
				alias->id = id;

				if (o->has("source")) {
					AliasListSourceType source = (AliasListSourceType) o->get("source")->number();

					switch (source) {
						case DNS: {
								  alias->source = new DnsListSource();
								  alias->source->detail = o->get("detail")->string();
								  break;
							  }

						case HTTP_FILE: {
									alias->source = new HttpFileSource();
									alias->source->detail = o->get("detail")->string();
									break;
								}
					};

				}
				else {
					ObjectContainer *values = o->get("values")->container();

					for (int z = 0; z < values->size(); z++) {
						alias->addEntry(values->get(z)->string());
					}
				}
			}

			alias->load();
			aliases[alias->id] = AliasPtr(alias);
		}
	}
	return true;
}

void SFwallCore::AliasDb::save() {
	ObjectContainer *root = new ObjectContainer(CREL);

	map<string, AliasPtr>::iterator iter = aliases.begin();
	ObjectContainer *arr = new ObjectContainer(CARRAY);

	while (iter != aliases.end()) {
		ObjectContainer *o = new ObjectContainer(CREL);
		AliasPtr temp = iter->second;
		o->put("id", new ObjectWrapper((string) temp->id));
		o->put("name", new ObjectWrapper((string) temp->name));
		o->put("type", new ObjectWrapper((double) temp->type()));

		if (temp->source != NULL) {
			o->put("source", new ObjectWrapper((double) temp->source->type()));
			o->put("detail", new ObjectWrapper((string) temp->source->detail));
		}
		else {
			ObjectContainer *values = new ObjectContainer(CARRAY);

			for (std::string aliasEntry : temp->listEntries()) {
				values->put(new ObjectWrapper((string) aliasEntry));
			}

			o->put("values", new ObjectWrapper(values));
		}

		arr->put(new ObjectWrapper(o));
		iter++;
	}

	root->put("aliases", new ObjectWrapper(arr));

	if (configurationManager) {
		configurationManager->holdLock();
		configurationManager->setElement("aliasDb", root);
		configurationManager->save();
		configurationManager->releaseLock();
	}
}


int SFwallCore::AliasDb::create(AliasPtr alias) {
	alias->id = StringUtils::genRandom();
	aliases[alias->id] = alias;

	int ret = alias->load();
	save();
	return ret;
}

int SFwallCore::AliasDb::del(AliasPtr target) {
	std::string key = target->id;

	for (AliasRemovedListener* listener : removedListeners) {
		listener->aliasRemoved(target);
	}

	map<string, AliasPtr>::iterator iter = aliases.find(key);
	if (iter != aliases.end()) {
		aliases.erase(iter);
	}

	save();
	return 0;
}

bool SFwallCore::IpRangeAlias::searchForNetworkMatch(unsigned int ip) {
	if (tryLock()) {
		Range target;
		target.start = ip;
		bool res = items.find(target) != items.end();
		releaseLock();
		return res;
	}

	return false;
}

bool SFwallCore::IpRangeAlias::searchForNetworkMatch(struct in6_addr *ip) {
	return false;
}

SFwallCore::Range::Range(std::string value) {
	static boost::regex exp1("([0-9]+)\\.([0-9]+)\\.([0-9]+)\\.([0-9]+)(\\/|\\-)([0-9]+)\\.([0-9]+)\\.([0-9]+)\\.([0-9]+)");
	static boost::regex exp2("([0-9,\\.]+)");
	static boost::regex exp3("([0-9,\\.]+)/([0-9]+)");
	boost::smatch what;

	if (boost::regex_match(value, what, exp1)) {
		stringstream ip; ip << what[1] << "." << what[2] << "." << what[3] << "." << what[4];
		stringstream netmask; netmask << what[6] << "." << what[7] << "." << what[8] << "." << what[9];
		
		start = IP4Addr::stringToIP4Addr(ip.str());
		end = IP4Addr::stringToIP4Addr(netmask.str());
	}
	else if(boost::regex_match(value, what, exp2)){
		start = IP4Addr::stringToIP4Addr(what[1]);
		end = -1;
        }else if(boost::regex_match(value, what, exp3)){
		std::string mask = what[2];
                start = IP4Addr::stringToIP4Addr(what[1]);
                end = IP4Addr::cidr_to_netmask(atoi(mask.c_str()));
	}else{
		start = -1;
		end = -1;
	}
}

bool SFwallCore::Range::valid(std::string value) {
	static boost::regex exp("([0-9]+)\\.([0-9]+)\\.([0-9]+)\\.([0-9]+)(\\/|\\-)([0-9]+)\\.([0-9]+)\\.([0-9]+)\\.([0-9]+)");
	static boost::regex exp2("([0-9]+)\\.([0-9]+)\\.([0-9]+)\\.([0-9]+)");
	static boost::regex exp3("([0-9,\\.]+)/([0-9]+)");

	boost::smatch what;

	if (boost::regex_match(value, what, exp)) {
		return true;
	}

	if (boost::regex_match(value, what, exp2)) {
		return true;
	}

        if (boost::regex_match(value, what, exp3)) {
                return true;
        }

	return false;
}

SFwallCore::RangeV6::RangeV6(std::string value) {
	static boost::regex exp("([0-9,a-f,A-F,\\:]+)\\/([0-9]+)");
	boost::smatch what;

	if (boost::regex_match(value, what, exp)) {
		start = (struct in6_addr *) malloc(sizeof(struct in6_addr));
		IP6Addr::fromString(start, what[1]);
		string temp = what[2];
		cidr = stoi(temp);
	}
}

bool SFwallCore::RangeV6::valid(std::string value) {
	static boost::regex exp("([0-9,a-f,A-F,\\:]+)\\/([0-9]+)");
	boost::smatch what;

	if (boost::regex_match(value, what, exp)) {
		return true;
	}

	return false;
}

void SFwallCore::IpRangeAlias::addEntry(const std::string& entry) {
	holdLock();
	if (Range::valid(entry)) {
		Range new_range(entry);
		if(new_range.end == -1){
			new_range.end = new_range.start;
		}
		items.insert(new_range);
	}
	releaseLock();
}

std::list<std::string> SFwallCore::IpRangeAlias::listEntries() {
	std::list<std::string> ret;
	std::multiset<Range>::iterator iter;

	holdLock();
	for (iter = items.begin(); iter != items.end(); iter++) {
		stringstream ss;
		ss << IP4Addr::ip4AddrToString((*iter).start);
		ss << "-";
		ss << IP4Addr::ip4AddrToString((*iter).end);
		ret.push_back(ss.str());
	}
	releaseLock();
	return ret;
}

void SFwallCore::IpRangeAlias::removeEntry(const std::string& entry) {
	holdLock();
	Range target(entry);

	std::multiset<Range>::iterator iter;
	for (iter = items.begin(); iter != items.end(); iter++) {
		if (target.start == (*iter).start && target.end == (*iter).end) {
			items.erase(iter);
			break;
		}
	}
	releaseLock();
}

bool SFwallCore::IpSubnetAlias::searchForNetworkMatch(unsigned int ip) {
	if (tryLock()) {
		Range target;
		target.start = ip;
		bool res = items.find(target) != items.end();

		releaseLock();
		return res;
	}

	return false;
}

bool SFwallCore::IpSubnetAlias::searchForNetworkMatch(struct in6_addr *ip) {
	if (tryLock()) {
		for (list<RangeV6>::iterator iter = itemsV6.begin(); iter != itemsV6.end(); iter++) {
			if (IP6Addr::matchNetwork(ip, (*iter).start, (*iter).cidr)) {
				releaseLock();
				return true;
			}
		}

		releaseLock();
	}

	return false;
}

void SFwallCore::IpSubnetAlias::addEntry(const std::string& entry) {
	holdLock();
	if (Range::valid(entry)) {
                Range new_range(entry);
                if(new_range.end == -1){
                        new_range.end = IP4Addr::stringToIP4Addr("255.255.255.255");
                }
                items.insert(new_range);
	}

	if (RangeV6::valid(entry)) {
		itemsV6.push_back(RangeV6(entry));
	}
	releaseLock();
}

std::list<std::string> SFwallCore::IpSubnetAlias::listEntries() {
	std::list<std::string> ret;
	std::multiset<Range>::iterator iter;
	holdLock();
	for (iter = items.begin(); iter != items.end(); iter++) {
		stringstream ss;
		ss << IP4Addr::ip4AddrToString((*iter).start);
		ss << "/";
		ss << IP4Addr::ip4AddrToString((*iter).end);
		ret.push_back(ss.str());
	}

	for (list<RangeV6>::iterator iter = itemsV6.begin(); iter != itemsV6.end(); iter++) {
		stringstream ss;
		ss << IP6Addr::toString((*iter).start) << "/" << (*iter).cidr;
		ret.push_back(ss.str());
	}
	releaseLock();
	return ret;
}

void SFwallCore::IpSubnetAlias::removeEntry(const std::string& entry) {
	holdLock();
	if (Range::valid(entry)) {
		Range target(entry);
		std::multiset<Range>::iterator iter;

		for (iter = items.begin(); iter != items.end(); iter++) {
			if (target.start == (*iter).start && target.end == (*iter).end) {
				items.erase(iter);
				break;
			}
		}
	}
	else if (RangeV6::valid(entry)) {
		RangeV6 target(entry);

		std::list<RangeV6>::iterator iter;

		for (iter = itemsV6.begin(); iter != itemsV6.end(); iter++) {
			if (memcmp(target.start, (*iter).start, sizeof(struct in6_addr)) == 0 && target.cidr == (*iter).cidr) {
				iter = itemsV6.erase(iter);
			}
		}
	}
	releaseLock();
}

bool SFwallCore::WebsiteListAlias::search(const std::string& url) {
	if(tryLock()){
		int cacheEnabled = System::getInstance()->getFirewall()->WEBSITE_ALIAS_CACHE_ENABLED;
		if(cacheEnabled == 1){
			boost::unordered_map<std::string, bool>::iterator cacheSearch = cache.find(url);
			if(cacheSearch != cache.end()){
				bool cache_result = cacheSearch->second;
				releaseLock();
				return cache_result;
			}
		}

		list<std::string> chunks;
		chunks.push_back(url);

		for (uint x = 0; x < url.size(); x++) {
			if (url[x] == '.') {
				string target = url.substr(x + 1, url.size() - x + 1);
				chunks.push_back(target);
			}
		}

		for (list<string>::reverse_iterator rit = chunks.rbegin(); rit != chunks.rend(); ++rit) {
			std::string target = (*rit);
			if (items.find(target) != items.end()) {
				if (cacheEnabled == 1){
					cache.insert(pair<std::string, bool>(target, true));
				}

				releaseLock();
				return true;
			}
		}

		if(cacheEnabled == 1){
			cache.insert(pair<std::string, bool>(url, false));
		}
		releaseLock();
	}
	return false;
}

void SFwallCore::WebsiteListAlias::addEntry(const std::string& entry) {
	if (entry.size() == 0) {
		return;
	}

	holdLock();
	cache.clear();
	
	items.insert(entry);
	releaseLock();
}

std::list<std::string> SFwallCore::WebsiteListAlias::listEntries() {
	holdLock();
	std::list<std::string> ret;
	unordered_set<std::string>::iterator iter;

	for (iter = items.begin(); iter != items.end(); iter++) {
		ret.push_back((*iter));
	}

	releaseLock();
	return ret;
}

void SFwallCore::WebsiteListAlias::removeEntry(const std::string& entry) {
	holdLock();
	items.erase(entry);
	cache.clear();
	releaseLock();
}

void SFwallCore::StringWildcardListAlias::addEntry(const string& entry) {
	holdLock();
	items.insert(entry);
	releaseLock();
}

list<string> SFwallCore::StringWildcardListAlias::listEntries() {
	holdLock();
	std::list<std::string> l;
	for (std::string s : items) {
		l.emplace_back(s);
	}

	releaseLock();
	return l;
}

void SFwallCore::StringWildcardListAlias::removeEntry(const string& entry) {
	holdLock();
	items.erase(entry);
	releaseLock();
}

bool SFwallCore::StringWildcardListAlias::search(const string& s) {
	if(tryLock()){
		for (std::string m : items) {
			if (s.find(m) != std::string::npos) {
				releaseLock();
				return true;
			}
		}
		releaseLock();
	}

	return false;
}

void SFwallCore::MacAddressListAlias::addEntry(const string& entry) {
	holdLock();
	items.insert(StringUtils::normalize_mac_address(entry));
	releaseLock();
}

list<string> SFwallCore::MacAddressListAlias::listEntries() {
	std::list<std::string> l;
	holdLock();
	for (std::string s : items) {
		l.emplace_back(s);
	}
	releaseLock();
	return l;
}

void SFwallCore::MacAddressListAlias::removeEntry(const string& entry) {
	holdLock();
	items.erase(entry);
	releaseLock();
}

bool SFwallCore::MacAddressListAlias::search(const string& s) {
	if(tryLock()){
		for (std::string m : items) {
			if (s.find(m) != std::string::npos) {
				releaseLock();
				return true;
			}
		}

		releaseLock();
	}
	return false;
}


int SFwallCore::DnsListSource::load(Alias *alias) {
	if (alias->type() != IP_SUBNET) {
		Logger::instance()->log("sphirewalld.firewall.aliases", INFO, " Tried to add a alias with source as dns with invalid format");
		return -2;
	}

	Logger::instance()->log("sphirewalld.firewall.aliases", INFO, "Loading Hostname alias for " + detail);
	vector<unsigned int> hosts = hostToIPAddresses(detail);

	for (int x = 0; x < (int) hosts.size(); x++) {
		unsigned int ip = hosts[x];
		stringstream ss;
		ss << IP4Addr::ip4AddrToString(ip) << "/255.255.255.255";
		alias->addEntry(ss.str());
	}

	return 0;
}

int SFwallCore::HttpFileSource::load(Alias *alias) {
	Logger::instance()->log("sphirewalld.firewall.aliases", INFO, "Loading HttpList alias from " + detail);
	HttpRequestWrapper *request = new HttpRequestWrapper(detail.c_str(), GET);

	try {
		vector<string> items;
		std::string raw_entries = request->execute();
		split(raw_entries, '\n', items);

		for (string item : items) {
			alias->addEntry(item);
		}

		Logger::instance()->log("sphirewalld.firewall.aliases", INFO, "Loaded %d entries", alias->listEntries().size());
	}
	catch (const HttpRequestWrapperException &ex) {
		Logger::instance()->log("sphirewalld.firewall.aliases", ERROR, "Failed to load HttpList alias from " + detail);
		delete request;
		return -1;
	}

	delete request;
	return 0;
}

void SFwallCore::Alias::clear() {
	for (string entry : listEntries()) {
		removeEntry(entry);
	}
}

bool SFwallCore::IpRangeOperator::operator()(SFwallCore::Range const &n1, SFwallCore::Range const &n2) const {
	if (n1.end == (unsigned int) - 1) {
		bool res = (n1.start < n2.start);
		return res;
	}

	if (n2.end == (unsigned int) - 1) {
		bool res = n1.end < n2.start;
		return res;
	}

	if (n1.start < n2.start) {
		return true;
	}

	return false;
}

bool SFwallCore::IpSubnetOperator::operator()(SFwallCore::Range const &n1, SFwallCore::Range const &n2) const {
	if (n1.end == (unsigned int) - 1) {
		return (n1.start < n2.start);
	}

	if (n2.end == (unsigned int) - 1) {
		return (n1.start | (~ n1.end)) < n2.start;
	}

	if (n1.start < n2.start) {
		return true;
	}

	return false;
}

