/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <algorithm>

using namespace std;

#include "SFwallCore/NatAcls.h"
#include "Utils/StringUtils.h"
#include "Core/ConfigurationManager.h"
#include "Core/System.h"
#include "SFwallCore/Packet.h"
#include "SFwallCore/Firewall.h"
#include "Core/TorProvider.h"

void SFwallCore::NatAclStore::addOneToOneRule(OneToOneNatRulePtr rule) {
        holdLock();
        rule->id = StringUtils::genRandom();
        oneToOneNatRules.push_back(rule);
        releaseLock();
}

void SFwallCore::NatAclStore::delOneToOneRule(OneToOneNatRulePtr rule) {
        holdLock();
        oneToOneNatRules.remove(rule);
        releaseLock();
}

std::list<SFwallCore::OneToOneNatRulePtr> SFwallCore::NatAclStore::listOneToOneRule() {
	list<SFwallCore::OneToOneNatRulePtr> ret;
	holdLock();
	ret = oneToOneNatRules;
	releaseLock();
        return ret;
}

SFwallCore::OneToOneNatRulePtr SFwallCore::NatAclStore::getOneToOneRule(std::string id) {
        holdLock();
        for (auto rule : oneToOneNatRules){
		if(rule->id.compare(id) == 0){
			releaseLock();
			return rule;
		}
	}

	releaseLock();
	return OneToOneNatRulePtr();
}


void SFwallCore::NatAclStore::addMasqueradeRule(MasqueradeRulePtr rule) {
	holdLock();
	rule->id = StringUtils::genRandom();
	masqueradeRules.push_back(rule);
	releaseLock();
}

void SFwallCore::NatAclStore::delMasqueradeRule(MasqueradeRulePtr rule) {
	holdLock();
	masqueradeRules.remove(rule);
	releaseLock();
}

std::list<SFwallCore::MasqueradeRulePtr>  SFwallCore::NatAclStore::listMasqueradeRules() {
	list<SFwallCore::MasqueradeRulePtr> ret;
	holdLock();
	ret = masqueradeRules;
	releaseLock();
	return ret;	
}

void  SFwallCore::NatAclStore::addForwardingRule(PortForwardingRulePtr rule) {
	holdLock();
	rule->id = StringUtils::genRandom();
	portFowardingRules.push_back(rule);
	releaseLock();
}

void  SFwallCore::NatAclStore::delPortForwardingRule(PortForwardingRulePtr rule) {
	holdLock();
	portFowardingRules.remove(rule);
	releaseLock();
}

std::list<SFwallCore::PortForwardingRulePtr>  SFwallCore::NatAclStore::listPortForwardingRules() {
	list<SFwallCore::PortForwardingRulePtr> ret;
	holdLock();
	ret = portFowardingRules;
	releaseLock();
	return ret;
}

bool SFwallCore::NatAclStore::load() {
	portFowardingRules.clear();
	masqueradeRules.clear();
	autowan_mode = -1;
	autowan_active_interfaces.clear();
	autowan_rules.clear();

	ObjectContainer *root;
	if (configurationManager->has("forwarding")) {
		root = configurationManager->getElement("forwarding");
		ObjectContainer *rules = root->get("rules")->container();

		for (int x = 0; x < rules->size(); x++) {
			ObjectContainer *source = rules->get(x)->container();

			PortForwardingRulePtr rule(new PortForwardingRule());
			rule->id = source->get("id")->string();
			rule->forwardingDestination = source->get("forwardingDestination")->number();

			if(source->has("criteria")){
				ObjectContainer* criteria = source->get("criteria")->container();
				for(int y = 0; y < criteria->size(); y++){
					ObjectContainer* individualCriteria = criteria->get(y)->container();
					translate(CriteriaPtr(CriteriaBuilder::parse(individualCriteria)), rule->source_criteria, rule->destination_criteria);
				}
			}

			if(source->has("source_criteria")){
				ObjectContainer* criteria = source->get("source_criteria")->container();
				for(int y = 0; y < criteria->size(); y++){
					ObjectContainer* individualCriteria = criteria->get(y)->container();
					rule->source_criteria.push_back(CriteriaPtr(CriteriaBuilder::parse(individualCriteria)));
				}
			}

			if(source->has("destination_criteria")){
				ObjectContainer* criteria = source->get("destination_criteria")->container();
				for(int y = 0; y < criteria->size(); y++){
					ObjectContainer* individualCriteria = criteria->get(y)->container();
					rule->destination_criteria.push_back(CriteriaPtr(CriteriaBuilder::parse(individualCriteria)));
				}
			}

			if (source->has("forwardingDestinationPort")) {
				rule->forwardingDestinationPort = source->get("forwardingDestinationPort")->number();
			}
			if (source->has("comment")) {
				rule->comment = source->get("comment")->string();
			}

			rule->enabled = source->get("enabled")->boolean();
			portFowardingRules.push_back(rule);
		}
	}

	ObjectContainer *masq;
	if (configurationManager->has("masquerading")) {
		masq = configurationManager->getElement("masquerading");
		ObjectContainer *rules = masq->get("rules")->container();

		for (int x = 0; x < rules->size(); x++) {
			ObjectContainer *source = rules->get(x)->container();

			MasqueradeRulePtr rule(new MasqueradeRule());
			rule->id = source->get("id")->string();
			rule->enabled = source->get("enabled")->boolean();

			if(source->has("criteria")){
				ObjectContainer* criteria = source->get("criteria")->container();
				for(int y = 0; y < criteria->size(); y++){
					ObjectContainer* individualCriteria = criteria->get(y)->container();
					translate(CriteriaPtr(CriteriaBuilder::parse(individualCriteria)), rule->source_criteria, rule->destination_criteria);
					//rule->criteria.push_back(CriteriaBuilder::parse(individualCriteria));
				}
			}

			if(source->has("source_criteria")){
				ObjectContainer* criteria = source->get("source_criteria")->container();
				for(int y = 0; y < criteria->size(); y++){
					ObjectContainer* individualCriteria = criteria->get(y)->container();
					rule->source_criteria.push_back(CriteriaPtr(CriteriaBuilder::parse(individualCriteria)));
				}
			}

			if(source->has("destination_criteria")){
				ObjectContainer* criteria = source->get("destination_criteria")->container();
				for(int y = 0; y < criteria->size(); y++){
					ObjectContainer* individualCriteria = criteria->get(y)->container();
					rule->destination_criteria.push_back(CriteriaPtr(CriteriaBuilder::parse(individualCriteria)));
				}
			}

			rule->natTargetDevice = source->get("natTargetDevice")->string();
			if(source->has("natTargetIp")){
				rule->natTargetIp = source->get("natTargetIp")->number();
			}
			if(source->has("comment")){
				rule->comment = source->get("comment")->string();
			}
			masqueradeRules.push_back(rule);
		}
	}

	ObjectContainer *onetoone;
	if (configurationManager->has("oto")) {
		onetoone = configurationManager->getElement("oto");
		ObjectContainer *rules = onetoone->get("rules")->container();

		for (int x = 0; x < rules->size(); x++) {
			ObjectContainer *source = rules->get(x)->container();

			OneToOneNatRulePtr rule(new OneToOneNatRule());
			rule->id = source->get("id")->string();
			rule->enabled = source->get("enabled")->boolean();
			rule->interface = source->get("interface")->string();
			rule->name = source->get("name")->string();
			rule->internalIp = IP4Addr::stringToIP4Addr(source->get("internalIp")->string());
			rule->externalIp = IP4Addr::stringToIP4Addr(source->get("externalIp")->string());

			oneToOneNatRules.push_back(rule);
		}
	}

	if(configurationManager->has("autowan2")){
		ObjectContainer* obj = configurationManager->getElement("autowan2");
		this->autowan_mode = obj->get("mode")->number();
		if(this->autowan_mode == SINGLE){
			if(obj->has("interface")){
				this->set_autowan_single_interface(obj->get("interface")->string());
			}else{
				ObjectContainer* devices= obj->get("devices")->container();
				for(int y = 0; y < devices->size(); y++){
					ObjectContainer* criteria = devices->get(y)->container();
					this->set_autowan_single_interface(criteria->get("interface")->string());
					break;
				}
			}

		}else if(this->autowan_mode == AUTOWAN_MODE_LOADBALANCING || this->autowan_mode == FAILOVER){
			ObjectContainer* devices= obj->get("devices")->container();
			for(int y = 0; y < devices->size(); y++){
				ObjectContainer* criteria = devices->get(y)->container();
				AutowanInterface* aw = new AutowanInterface();
				aw->failover_index = criteria->get("failover_index")->number();	
				aw->interface = criteria->get("interface")->string();	

				autowan_rules.push_back(AutowanInterfacePtr(aw));	
			}
		}
	}

	initRules();
	return true;
}
void  SFwallCore::NatAclStore::save() {
	holdLock();

	ObjectContainer *root = new ObjectContainer(CREL);
	ObjectContainer *rules = new ObjectContainer(CARRAY);

	for (PortForwardingRulePtr target : portFowardingRules) {
		ObjectContainer *rule = new ObjectContainer(CREL);
		rule->put("id", new ObjectWrapper((string) target->id));
		rule->put("comment", new ObjectWrapper((string) target->comment));
		rule->put("enabled", new ObjectWrapper((bool) target->enabled));
		rule->put("forwardingDestination", new ObjectWrapper((double) target->forwardingDestination));

		if (target->forwardingDestinationPort != UNSET) {
			rule->put("forwardingDestinationPort", new ObjectWrapper((double) target->forwardingDestinationPort));
		}

		ObjectContainer* target_source_criteria = new ObjectContainer(CARRAY);
		for(CriteriaPtr crit : target->source_criteria){
			target_source_criteria->put(new ObjectWrapper(CriteriaBuilder::serialize(crit.get())));
		}
		rule->put("source_criteria", new ObjectWrapper(target_source_criteria));

		ObjectContainer* target_destination_criteria = new ObjectContainer(CARRAY);
		for(CriteriaPtr crit : target->destination_criteria){
			target_destination_criteria->put(new ObjectWrapper(CriteriaBuilder::serialize(crit.get())));
		}
		rule->put("destination_criteria", new ObjectWrapper(target_destination_criteria));

		rules->put(new ObjectWrapper(rule));
	}

	root->put("rules", new ObjectWrapper(rules));

	ObjectContainer *masq = new ObjectContainer(CREL);
	ObjectContainer *masqRules = new ObjectContainer(CARRAY);

	for (MasqueradeRulePtr target : masqueradeRules) {
		ObjectContainer *rule = new ObjectContainer(CREL);
		rule->put("id", new ObjectWrapper((string) target->id));
		rule->put("enabled", new ObjectWrapper((bool) target->enabled));
		rule->put("natTargetDevice", new ObjectWrapper((string) target->natTargetDevice));
		rule->put("comment", new ObjectWrapper((string) target->comment));
		rule->put("natTargetIp", new ObjectWrapper((double) target->natTargetIp));

		ObjectContainer* target_source_criteria = new ObjectContainer(CARRAY);
		for(CriteriaPtr crit : target->source_criteria){
			target_source_criteria->put(new ObjectWrapper(CriteriaBuilder::serialize(crit.get())));
		}
		rule->put("source_criteria", new ObjectWrapper(target_source_criteria));

		ObjectContainer* target_destination_criteria = new ObjectContainer(CARRAY);
		for(CriteriaPtr crit : target->destination_criteria){
			target_destination_criteria->put(new ObjectWrapper(CriteriaBuilder::serialize(crit.get())));
		}
		rule->put("destination_criteria", new ObjectWrapper(target_destination_criteria));

		masqRules->put(new ObjectWrapper(rule));
	}

	masq->put("rules", new ObjectWrapper(masqRules));

	ObjectContainer *oto = new ObjectContainer(CREL);
	ObjectContainer *otoRules = new ObjectContainer(CARRAY);

	for (OneToOneNatRulePtr target : oneToOneNatRules) {
		ObjectContainer *rule = new ObjectContainer(CREL);
		rule->put("id", new ObjectWrapper((string) target->id));
		rule->put("enabled", new ObjectWrapper((bool) target->enabled));
		rule->put("name", new ObjectWrapper((string) target->name));
		rule->put("interface", new ObjectWrapper((string) target->interface));
		rule->put("internalIp", new ObjectWrapper((string) IP4Addr::ip4AddrToString(target->internalIp)));
		rule->put("externalIp", new ObjectWrapper((string) IP4Addr::ip4AddrToString(target->externalIp)));

		otoRules->put(new ObjectWrapper(rule));
	}

	oto->put("rules", new ObjectWrapper(otoRules));


	ObjectContainer *autowanObj = new ObjectContainer(CREL);
	autowanObj->put("mode", new ObjectWrapper((double) autowan_mode));
	if(autowan_mode == SINGLE){
		autowanObj->put("interface", new ObjectWrapper((string) autowan_single_interface_str));

	}else if(autowan_mode == AUTOWAN_MODE_LOADBALANCING || autowan_mode == FAILOVER){
		ObjectContainer* devices = new ObjectContainer(CARRAY);
		for(AutowanInterfacePtr device : autowan_rules){
			ObjectContainer* device_container = new ObjectContainer(CREL);
			device_container->put("interface", new ObjectWrapper((string) device->interface));
			device_container->put("failover_index", new ObjectWrapper((double) device->failover_index));
			devices->put(new ObjectWrapper(device_container));
		}
		autowanObj->put("devices", new ObjectWrapper(devices));
	}

	configurationManager->holdLock();	
	configurationManager->setElement("autowan2", autowanObj);
	configurationManager->setElement("masquerading", masq);
	configurationManager->setElement("forwarding", root);
	configurationManager->setElement("oto", oto);
	configurationManager->save();
	configurationManager->releaseLock();

	releaseLock();
}

SFwallCore::MasqueradeRulePtr  SFwallCore::NatAclStore::matchMasqueradeRule(SFwallCore::Packet *packet) {
	holdLock();
	for (MasqueradeRulePtr target : masqueradeRules) {
		if (target->match(packet)) {
			releaseLock();
			return target;
		}
	}

	releaseLock();
	return MasqueradeRulePtr();
}

SFwallCore::OneToOneNatRulePtr SFwallCore::NatAclStore::matchOneToOneNatRule_prerouting(SFwallCore::Packet *packet) {
	holdLock();	
	if(packet->type() == IPV4){
		for(OneToOneNatRulePtr rule : oneToOneNatRules){
			if(rule->enabled){
				PacketV4* v4 = (PacketV4*) packet;
				if(v4->getDstIp() == rule->externalIp && v4->getSourceNetDevice() == rule->interface_ptr){
					releaseLock();
					return rule;
				}
			}
		}
	}

	releaseLock();
	return OneToOneNatRulePtr();
}

SFwallCore::OneToOneNatRulePtr SFwallCore::NatAclStore::matchOneToOneNatRule_postrouting(SFwallCore::Packet *packet) {
        holdLock();
	if(packet->type() == IPV4){
		for(OneToOneNatRulePtr rule : oneToOneNatRules){
			if(rule->enabled){
				PacketV4* v4 = (PacketV4*) packet;
				if(v4->getSrcIp() == rule->internalIp && v4->getDestNetDevice() == rule->interface_ptr){
                                        releaseLock();
					return rule;
				}
			}
		}
	}

	releaseLock();
	return OneToOneNatRulePtr();
}

SFwallCore::PortForwardingRulePtr  SFwallCore::NatAclStore::matchForwardingRule(SFwallCore::Packet *packet) {
        holdLock();
	for (PortForwardingRulePtr target : portFowardingRules) {
		if (target->match(packet)) {
			releaseLock();
			return target;
		}
	}

	releaseLock();
	return PortForwardingRulePtr();
}

SFwallCore::MasqueradeRulePtr SFwallCore::NatAclStore::getMasqueradeRule(std::string id) {
        holdLock();
	for (MasqueradeRulePtr target : masqueradeRules) {
		if (target->id.compare(id) == 0) {
			releaseLock();
			return target;
		}
	}

	releaseLock();
	return MasqueradeRulePtr();
}

SFwallCore::PortForwardingRulePtr SFwallCore::NatAclStore::getPortForwardingRule(std::string id) {
        holdLock();
	for (PortForwardingRulePtr target : portFowardingRules) {
		if (target->id.compare(id) == 0) {
                        releaseLock();
			return target;
		}
	}

	releaseLock();
	return PortForwardingRulePtr();
}

bool sort_by_priority(const SFwallCore::AutowanInterfacePtr& x, const SFwallCore::AutowanInterfacePtr& y){
	return x->failover_index < y->failover_index;
}

void SFwallCore::NatAclStore::initRules() {
	for (MasqueradeRulePtr target : masqueradeRules) {
		target->natTargetDevicePtr = interfaceManager->get_interface(target->natTargetDevice);
		for(CriteriaPtr criteria : target->source_criteria){
			criteria->refresh();
		}

		for(CriteriaPtr criteria : target->destination_criteria){
			criteria->refresh();
		}
	}

	for (OneToOneNatRulePtr target : oneToOneNatRules) {
		target->interface_ptr = interfaceManager->get_interface(target->interface);
	}

	for (PortForwardingRulePtr target : portFowardingRules) {
		for(CriteriaPtr criteria : target->source_criteria){
			criteria->refresh();
		}

		for(CriteriaPtr criteria : target->destination_criteria){
			criteria->refresh();
		}
	}

	if(autowan_mode == SINGLE){
		auto device = interfaceManager->get_interface(autowan_single_interface_str);
		if(device){
			set_autowan_single_interface(device);
		}
	}else{
		autowan_active_interfaces.clear();
		autowan_load_balancing_device_pool_last = 0;

		sort(autowan_rules.begin(), autowan_rules.end(), sort_by_priority);
		for (AutowanInterfacePtr target : autowan_rules) {
			auto device = interfaceManager->get_interface(target->interface);
			if(device){
				target->resolved_interface = device;
				autowan_active_interfaces.insert(device);	
			}
		}
	}
}

bool SFwallCore::PortForwardingRule::match(SFwallCore::Packet *packet) {
	holdLock();
	if (!enabled) {
		releaseLock();
		return false;
	}

	for(CriteriaPtr target: source_criteria){
		if(!target->match(packet, DIRECTION_SOURCE)){
			releaseLock();
			return false;
		}
	}

	for(CriteriaPtr target: destination_criteria){
		if(!target->match(packet, DIRECTION_DESTINATION)){
			releaseLock();
			return false;
		}
	}

	releaseLock();
	return true;
}

bool SFwallCore::MasqueradeRule::match(SFwallCore::Packet *packet) {
	holdLock();

	if (!enabled) {
		releaseLock();
		return false;
	}

	for(CriteriaPtr target: source_criteria){
		if(!target->match(packet, DIRECTION_SOURCE)){
			releaseLock();
			return false;
		}
	}

	for(CriteriaPtr target: destination_criteria){
		if(!target->match(packet, DIRECTION_DESTINATION)){
			releaseLock();
			return false;
		}
	}

	releaseLock();
	return true;
}

int SFwallCore::NatAclStore::determine_routing_fwmark(SFwallCore::Packet* incomming_packet){
	holdLock();
	if(autowan_mode == DISABLED || autowan_mode == SINGLE){
		releaseLock();
		return -1;
	}

	if(autowan_mode == AUTOWAN_MODE_LOADBALANCING){
		int size = autowan_rules.size();
		if (size == 0) {
                        releaseLock();
			return -1;
		}

		if (autowan_load_balancing_device_pool_last == size) {
			autowan_load_balancing_device_pool_last = 0;
		}

		bool looped=false;		
		while(true){
			if (autowan_load_balancing_device_pool_last == size) {
				autowan_load_balancing_device_pool_last = 0;
				if(looped){
					break;
				}

				looped = true;
			}

			AutowanInterfacePtr ptr = autowan_rules[autowan_load_balancing_device_pool_last++];
			if(ptr && ptr->resolved_interface && ptr->resolved_interface->state){
				bool criteria_matched = true;
				for(Criteria* criteria : ptr->criteria){
					if(!criteria->match(incomming_packet, DIRECTION_NA)){
						criteria_matched = false;
						break;
					}
				}

				if(criteria_matched){
					autowan_load_balancing_device_pool_last;
					int ret_interface_id = ptr->resolved_interface->ifid;
					releaseLock();
					return ret_interface_id;
				}
			}
		}
	}


	if(autowan_mode == FAILOVER){
		int size = autowan_rules.size();
		if (size == 0) {
                        releaseLock();
			return -1;
		}

		//Find a suitable host
		for(int x =0; x < size; x++){
			AutowanInterfacePtr ptr = autowan_rules[x];
			if(ptr && ptr->resolved_interface->state){

				bool criteria_matched = true;
				for(Criteria* criteria : ptr->criteria){
					if(!criteria->match(incomming_packet, DIRECTION_NA)){
						criteria_matched = false;
						break;
					}
				}

				if(criteria_matched){
                                        int ret_interface_id = ptr->resolved_interface->ifid;
					releaseLock();
					return ret_interface_id;
				}
			}
		}
	}

	releaseLock();  
	return -1;
}

bool SFwallCore::NatAclStore::determine_automatic_tor_gateway(SFwallCore::Packet* incomming_packet){
	holdLock();
	if(autowan_mode == TOR_GATEWAY){
		PacketV4* ipv4_packet = (PacketV4*) incomming_packet;
		if(incomming_packet->getSourceNetDevice() == System::getInstance()->get_tor_provider()->interface_ptr){
			releaseLock();  
			return true;
		}
	}

	releaseLock();  
	return false;
}

bool SFwallCore::NatAclStore::determine_autowan_matches(SFwallCore::Packet* incomming_packet){
	if(autowan_mode == SINGLE){
		holdLock();
		if(get_autowan_single_interface() && get_autowan_single_interface() == incomming_packet->getDestNetDevice()){
			PacketV4* ipv4_packet = (PacketV4*) incomming_packet;
			if(ipv4_packet->getDstIp() != incomming_packet->getDestNetDevice()->get_primary_ipv4_address()
					&& ipv4_packet->getSrcIp() != incomming_packet->getDestNetDevice()->get_primary_ipv4_address()){
				releaseLock();                          
				return true;
			}
		}
		releaseLock();

	}else if(autowan_mode == AUTOWAN_MODE_LOADBALANCING || autowan_mode == FAILOVER){
		AutowanInterfacePtr rule = get_autowan_rule_interface(incomming_packet->getDestNetDevice());
		if(rule){	
			rule->holdLock();
			PacketV4* ipv4_packet = (PacketV4*) incomming_packet;
			if(ipv4_packet->getDstIp() != incomming_packet->getDestNetDevice()->get_primary_ipv4_address() 
					&& ipv4_packet->getSrcIp() != incomming_packet->getDestNetDevice()->get_primary_ipv4_address()){

				bool criteria_matched = true;
				for(Criteria* criteria : rule->criteria){
					if(!criteria->match(incomming_packet, DIRECTION_NA)){
						rule->releaseLock();                          
						return false;
					}
				}

				if(criteria_matched){
					rule->releaseLock();
					return true;
				}
			}
			rule->releaseLock();
		}
	}

	return false;
}

int SFwallCore::NatAclStore::get_autowan_mode(){
	return autowan_mode; 
}

void SFwallCore::NatAclStore::set_autowan_mode(int mode){
	autowan_mode = mode;	
}

std::vector<SFwallCore::AutowanInterfacePtr> SFwallCore::NatAclStore::get_autowan_rules(){
	vector<SFwallCore::AutowanInterfacePtr> ret;
	holdLock();	
	ret = autowan_rules;
	releaseLock();
	return ret;
}

SFwallCore::AutowanInterfacePtr SFwallCore::NatAclStore::get_autowan_rule_interface(std::string interface){
	for(AutowanInterfacePtr i : get_autowan_rules()){
		i->holdLock();
		if(i->interface.compare(interface) == 0){
			i->releaseLock();
			return i;
		}
		i->releaseLock();
	}

	return AutowanInterfacePtr();
}

SFwallCore::AutowanInterfacePtr SFwallCore::NatAclStore::get_autowan_rule_interface(InterfacePtr target){
	for(AutowanInterfacePtr i : get_autowan_rules()){
		i->holdLock();
		if(i->resolved_interface == target){
			i->releaseLock();
			return i;
		}
		i->releaseLock();
	}
	return AutowanInterfacePtr();
}

void SFwallCore::NatAclStore::del_autowan_rule_interface(std::string interface){
	holdLock();
	std::vector<AutowanInterfacePtr>::iterator iter;
	for(iter = autowan_rules.begin(); iter != autowan_rules.end(); iter++){
		AutowanInterfacePtr i = (*iter);
		if(i->interface.compare(interface) == 0){
			autowan_rules.erase(iter);
			releaseLock();
			return;
		}
	}
	releaseLock();
}

void SFwallCore::NatAclStore::add_autowan_rule(AutowanInterfacePtr rule){
	holdLock();
	autowan_rules.push_back(rule);
	releaseLock();
}

void SFwallCore::NatAclStore::remove_autowan_rules(){
	holdLock();
	autowan_rules.clear();
	releaseLock();
}

