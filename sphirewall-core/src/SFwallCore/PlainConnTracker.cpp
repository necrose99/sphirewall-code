/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <vector>
#include <map>
#include <algorithm>
#include <sstream>
#include "SFwallCore/ConnTracker.h"
#include "SFwallCore/Connection.h"
#include "SFwallCore/State.h"
#include "Core/System.h"
#include "SFwallCore/Packet.h"

void SFwallCore::ProtocolHandler::registerDeleteHook(SFwallCore::DeleteConnectionHook *handler) {
	deleteHooks.push_back(handler);
}

SFwallCore::TrackerMetrics *SFwallCore::TcpProtocolHandler::metrics() {
	TrackerMetrics *metrics = new TrackerMetrics();

	for (int x = 0; x < Firewall::PLAIN_CONN_HASH_MAX; x++) {
		std::list<Connection *> &l = conn_map[x];

		for (std::list<Connection *>::const_iterator liter = l.begin(); liter != l.end(); ++liter) {
			Connection *conn = *liter;

			if (conn) {
				metrics->firewall_conntracker_tcp_size++;

				if (!conn->hasExpired()) {
					metrics->firewall_conntracker_tcp_valid++;
				}

				TcpConnection *tcpConn = (TcpConnection *) conn;

				switch (tcpConn->getState()->state) {
				case SYNSYNACK:
					metrics->firewall_conntracker_tcp_synsynack++;
					break;

				case SYNACKACK:
					metrics->firewall_conntracker_tcp_synackack++;
					break;

				case TCPUP:
					metrics->firewall_conntracker_tcp_tcpup++;
					break;

				case FIN_WAIT:
					metrics->firewall_conntracker_tcp_finwait++;
					break;

				case CLOSE_WAIT:
					metrics->firewall_conntracker_tcp_closewait++;
					break;

				case LAST_ACK:
					metrics->firewall_conntracker_tcp_lastack++;
					break;

				case TIME_WAIT:
					metrics->firewall_conntracker_tcp_timewait++;
					break;

				case FINFINACK:
					metrics->firewall_conntracker_tcp_finfinack++;
					break;

				case CLOSED:
					metrics->firewall_conntracker_tcp_closed++;
					break;

				case RESET:
					metrics->firewall_conntracker_tcp_reset++;
					break;
				}

			}
		}
	}

	return metrics;
}

void SFwallCore::TcpProtocolHandler::sample(map<string, double> &items) {
	items["firewall.conntracker.tcp.size"] = size();

	if (firewall->TCP_CONNECTION_TRACKER_COLLECT_DETAILED_STATS == 1) {
		//Must aquire root connection tracker lock:
		firewall->connectionTracker->holdLock();

		TrackerMetrics *m = metrics();
		items["firewall.conntracker.tcp.synsynack"] = m->firewall_conntracker_tcp_synsynack;
		items["firewall.conntracker.tcp.synackack"] = m->firewall_conntracker_tcp_synackack;
		items["firewall.conntracker.tcp.tcpup"] = m->firewall_conntracker_tcp_tcpup;
		items["firewall.conntracker.tcp.finwait"] = m->firewall_conntracker_tcp_finwait;
		items["firewall.conntracker.tcp.closewait"] = m->firewall_conntracker_tcp_closewait;
		items["firewall.conntracker.tcp.lastack"] = m->firewall_conntracker_tcp_lastack;
		items["firewall.conntracker.tcp.timewait"] = m->firewall_conntracker_tcp_timewait;
		items["firewall.conntracker.tcp.finfinack"] = m->firewall_conntracker_tcp_finfinack;
		items["firewall.conntracker.tcp.closed"] = m->firewall_conntracker_tcp_closed;
		items["firewall.conntracker.tcp.reset"] = m->firewall_conntracker_tcp_reset;

		firewall->connectionTracker->releaseLock();
		delete m;
	}

	items["firewall.conntracker.tcp.hashcollisions"] = hashCollisions;
	items["firewall.conntracker.tcp.newconnections"] = newConnections;
	items["firewall.conntracker.tcp.inlineremovedconnections"] = inlineRemovedConnections;
	items["firewall.conntracker.tcp.gbremovedconnections"] = gbRemovedConnections;

	items["firewall.conntracker.tcp.search.count"] = searchRequests;
	items["firewall.conntracker.tcp.search.success"] = successSearchRequests;
	items["firewall.conntracker.tcp.search.failed"] = failedSearchRequests;
}

void SFwallCore::UdpProtocolHandler::sample(map<string, double> &items) {
	items["firewall.conntracker.udp.size"] = size();
	items["firewall.conntracker.udp.hashcollisions"] = hashCollisions;
	items["firewall.conntracker.udp.newconnections"] = newConnections;
	items["firewall.conntracker.udp.inlineremovedconnections"] = inlineRemovedConnections;
	items["firewall.conntracker.udp.gbremovedconnections"] = gbRemovedConnections;
	items["firewall.conntracker.udp.search.count"] = searchRequests;
	items["firewall.conntracker.udp.search.success"] = successSearchRequests;
	items["firewall.conntracker.udp.search.failed"] = failedSearchRequests;
}

void SFwallCore::IcmpProtocolHandler::sample(map<string, double> &items) {
	items["firewall.conntracker.icmp.size"] = size();
	items["firewall.conntracker.icmp.hashcollisions"] = hashCollisions;
	items["firewall.conntracker.icmp.newconnections"] = newConnections;
	items["firewall.conntracker.icmp.inlineremovedconnections"] = inlineRemovedConnections;
	items["firewall.conntracker.icmp.gbremovedconnections"] = gbRemovedConnections;

        items["firewall.conntracker.icmp.search.count"] = searchRequests;
        items["firewall.conntracker.icmp.search.success"] = successSearchRequests;
        items["firewall.conntracker.icmp.search.failed"] = failedSearchRequests;
}

void SFwallCore::PlainConnTracker::sample(map<string, double> &items) {
	for (int x = 0; x < MAX_HANDLERS; x++) {
		if (handlers[x]) {
			handlers[x]->sample(items);
		}
	}
}


void SFwallCore::PlainConnTracker::terminate(Connection *connection) {
	Logger::instance()->log("sphirewalld.firewall.conntracker", INFO,
			"ConnectionTracker terminating connection %s", connection->toString().c_str());
	connection->terminate();
}

int SFwallCore::PlainConnTracker::size() {
	int ret = 0;

	for (int x = 0; x < MAX_HANDLERS; x++) {
		if (handlers[x]) {
			ret += handlers[x]->size();
		}
	}

	return ret;
}

SFwallCore::PlainConnTracker::~PlainConnTracker() {
}

SFwallCore::Connection *SFwallCore::ProtocolHandler::create(SFwallCore::Packet *packet) {
	Connection *conn = Connection::parseConnection(packet);

	if (conn) {
		add(conn);
		if(applicationLayerTracker) applicationLayerTracker->update(conn, packet);

		newConnections++;
		if (Logger::instance()->criticalPathEnabled) {
			Logger::instance()->log("sphirewalld.firewall.conntracker", INFO,
					"Creating new Filter connection with hash %d packet %s", conn->hash(), packet->toString().c_str());
		}
	}

	return conn;
}

SFwallCore::Connection *SFwallCore::PlainConnTracker::create(SFwallCore::Packet *packet) {
	return handlers[packet->getProtocol()]->create(packet);
}

SFwallCore::Connection *SFwallCore::ProtocolHandlerV4::offer(SFwallCore::Packet *packet) {
	Connection *connection = findConn(packet, UdpConnection::hash(packet));

	if (connection) {
		connection->increment(packet);
		if(applicationLayerTracker) applicationLayerTracker->update(connection, packet);
	}

	return connection;
}

SFwallCore::Connection *SFwallCore::TcpProtocolHandler::offer(SFwallCore::Packet *packet) {
	Connection *connection = NULL;
	connection = findConn(packet, TcpConnection::hash(packet));

	if (connection) {
		((TcpConnection *)connection)->getTcpTrackable()->calcState((const TcpPacket *) packet->getTransport());
	}

	if (connection) {
		connection->increment(packet);
		applicationLayerTracker->update(connection, packet);
	}

	return connection;
}

SFwallCore::Connection *SFwallCore::PlainConnTracker::offer(SFwallCore::Packet *packet) {
	SFwallCore::Connection *c = NULL;

	if ((c = handlers[packet->getProtocol()]->offer(packet))) {
		packet->setConnection(c);
	}

	return c;
}

vector<SFwallCore::Connection *> SFwallCore::ProtocolHandler::listConnections() {
	vector<Connection *> ret;

	for (int x = 0; x < Firewall::PLAIN_CONN_HASH_MAX; x++) {
		std::list<Connection *> &l = conn_map[x];

		for (std::list<Connection *>::const_iterator liter = l.begin(); liter != l.end(); ++liter) {
			ret.push_back(*liter);
		}
	}

	return ret;
}

vector<SFwallCore::Connection *> SFwallCore::PlainConnTracker::listConnections() {
	vector<Connection *> ret;

	for (int x = 0; x < MAX_HANDLERS; x++) {
		ProtocolHandler *target = handlers[x];

		if (target) {
			vector<Connection *> toInsert = target->listConnections();
			ret.insert(ret.end(), toInsert.begin(), toInsert.end());
		}
	}

	return ret;
}

SFwallCore::Connection* SFwallCore::PlainConnTracker::get_connection_by_id(std::string id){
	for(Connection* connection : listConnections()){
		if(connection->get_id().compare(id) == 0){
			return connection;
		}
	}

	return NULL;
}

SFwallCore::Connection *SFwallCore::ProtocolHandlerV4::findConn(Packet *packet, int hash) {
	list<Connection *> &vec = conn_map[abs(hash)];
	list<Connection *>::iterator iter;

	int c = 0;

	searchRequests++;
	for (iter = vec.begin(); iter != vec.end(); ++iter) {
		c++;
		Connection *conn = *iter;

		if (conn->hasExpired()) {
			if (Logger::instance()->criticalPathEnabled) {
				Logger::instance()->log("sphirewalld.firewall.conntracker", DEBUG, "Erasing expired connection %s", conn->toString().c_str());
			}

			deleteHook(conn);
			delete conn;
			iter = vec.erase(iter);
			inlineRemovedConnections++;

		}
		else if (conn->checkPacket(packet, DIR_ANY)) {
			if (Logger::instance()->criticalPathEnabled) {
				Logger::instance()->log("sphirewalld.firewall.conntracker", DEBUG,
						"Found matching filter connection for packet %s", packet->toString().c_str());
			}

			if(c == 1){
				hashMatchWithoutCollision++;
			}

			successSearchRequests++;
			return *iter;
		}

		hashCollisions++;
	}

	failedSearchRequests++;
	return NULL;
}

SFwallCore::Connection *SFwallCore::PlainConnTracker::findConn(Packet *packet, int hash) {
	return handlers[packet->getProtocol()]->findConn(packet, hash);
}

void SFwallCore::ProtocolHandler::add(Connection *conn) {
	numberConnections++;
	conn_map[abs(conn->hash())].push_back(conn);
}

void SFwallCore::PlainConnTracker::add(Connection *conn) {
	handlers[conn->getProtocol()]->add(conn);
}

void SFwallCore::ProtocolHandler::deleteHook(Connection *connection) {
	numberConnections--;
	//Ignore other traffic
	if (connection->getProtocol() == TCP || connection->getProtocol() == UDP) {
		if (firewall->BANDWIDTHDB_ENABLED == 1) {
			for (DeleteConnectionHook * hook : deleteHooks) {
				hook->hook(connection);
			}
		}
	}
}

void SFwallCore::PlainConnTracker::erase(Connection *conn) {
	handlers[conn->getProtocol()]->erase(conn);
}

void SFwallCore::ProtocolHandler::erase(Connection *conn) {
	list<Connection *>::iterator item;
	list<Connection *> &vec = conn_map[conn->hash()];
	item = find(vec.begin(), vec.end(), conn);

	if (item != vec.end()) {
		vec.erase(item);

		if (Logger::instance()->criticalPathEnabled) {
			Logger::instance()->log("sphirewalld.firewall.conntracker", INFO, "Erasing connection " + conn->toString());
		}
	}
}

int SFwallCore::ProtocolHandlerV4::cleanup() {
	int count = 0;

	for (int x = 0; x < Firewall::PLAIN_CONN_HASH_MAX; x++) {
		list<Connection *> &vec = conn_map[x];

		for (list<Connection *>::iterator liter = vec.begin(); liter != vec.end(); liter++) {
			Connection *conn = *liter;

			if (conn->hasExpired()) {

				deleteHook(conn);
				liter = vec.erase(liter);

				if (Logger::instance()->criticalPathEnabled) {
					Logger::instance()->log("sphirewalld.firewall.conntracker", INFO, "Erasing connection " + conn->toString());
				}

				count++;
				delete conn;
			}
		}
	}

	gbRemovedConnections++;
	return count;
}

int SFwallCore::TcpProtocolHandler::cleanup() {
	int count = 0;

	for (int x = 0; x < Firewall::PLAIN_CONN_HASH_MAX; x++) {
		list<Connection *> &vec = conn_map[x];

		for (list<Connection *>::iterator liter = vec.begin(); liter != vec.end(); liter++) {
			Connection *conn = *liter;

			if (conn->hasExpired()) {
				deleteHook(conn);
				liter = vec.erase(liter);

				if (Logger::instance()->criticalPathEnabled) {
					Logger::instance()->log("sphirewalld.firewall.conntracker", INFO, "Erasing connection " + conn->toString());
				}

				count++;
				delete conn;
			}
		}
	}

	return count;
}

int SFwallCore::PlainConnTracker::cleanup() {
	int count = 0;
	holdLock();
	Timer *timer = new Timer();
	timer->start();

	for (int x = 0; x < MAX_HANDLERS; x++) {
		ProtocolHandler *handler = handlers[x];

		if (handler) {
			count += handler->cleanup();
		}
	}

	timer->stop();
	delete timer;
	releaseLock();
	return count;
}

std::list<SFwallCore::Connection *> SFwallCore::PlainConnTracker::listConnections(ConnectionCriteria criteria) {
	list<Connection *> ret;

	for (Connection* target : listConnections()) {
		if (criteria.match(target)) {
			ret.push_back(target);
		}
	}

	return ret;
}

bool SFwallCore::ConnectionCriteria::match(SFwallCore::Connection *connection) {
	if (connection->getProtocol() == TCP || connection->getProtocol() == UDP) {
		if (destPort != -1) {
			if (connection->getDestinationPort() != destPort) {
				return false;
			}
		}

		if (sourcePort != -1) {
			if (connection->getSourcePort() != sourcePort) {
				return false;
			}
		}
	}

	if (type != -1 && type != connection->getProtocol()) {
		return false;
	}

	if (dest != (unsigned int) - 1) {
		if (connection->getIp()->type() == IPV4) {
			ConnectionIpV4 *ipv4 = (ConnectionIpV4 *) connection->getIp();

			if (ipv4->getDstIp() != dest) {
				return false;
			}
		}
	}

	if (source != (unsigned int) - 1) {
		if (connection->getIp()->type() == IPV4) {
			ConnectionIpV4 *ipv4 = (ConnectionIpV4 *) connection->getIp();

			if (ipv4->getSrcIp() != source) {
				return false;
			}
		}
	}

	return true;
}

void SFwallCore::PlainConnTracker::user_logged_in(HostPtr host_entry){
	if(tryLock()){
		for(Connection* connection : listConnections()){
			if(connection->getHostDiscoveryServiceEntry() == host_entry){
				if(connection->getRewriteRule()){
					connection->setRewriteRule(NULL);
				}
			}
		}

		releaseLock();
	}
}
