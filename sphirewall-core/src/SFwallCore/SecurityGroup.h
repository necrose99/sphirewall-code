#ifndef SECURITY_GROUP_H
#define SECURITY_GROUP_H

#include <boost/shared_ptr.hpp>

/* Internal Imports */
#include "Core/ConfigurationManager.h"
#include "SFwallCore/Packet.h"

/*
Locking and Using this Structure:
The SecurityGroupStore is threadsafe, but entities inside it are not. When using or modifying
entries in the store, you must aquire a lock on that object first. 
*/

namespace SFwallCore {
	class Criteria;
	typedef boost::shared_ptr<Criteria> CriteriaPtr;

	class SecurityGroupEntry {
		public:
			std::string id;
			std::string name;
			list<CriteriaPtr> criteria;
	
			bool matches(SFwallCore::Packet* packet, int direction);
	};

	typedef boost::shared_ptr<SecurityGroupEntry> SecurityGroupEntryPtr;

	class SecurityGroup : public Lockable {
		public:
			std::string id;
			std::string name;
			list<SecurityGroupEntryPtr> entries;

			bool matches(SFwallCore::Packet* packet, int direction);
			void refresh();
	};

	typedef boost::shared_ptr<SecurityGroup> SecurityGroupPtr;

	class SecurityGroupStore : public Lockable, public Configurable {
		public:
			list<SecurityGroupPtr> list_groups();	
			SecurityGroupPtr get_group(std::string id);	
			void add_group(SecurityGroupPtr group);
			void del_group(SecurityGroupPtr group);

			/* Required for Configurable */	
			const char* getConfigurationSystemName(){
				return "Security Group Store";
			}
			bool load();
			void save();

			void refresh();
		private:
			list<SecurityGroupPtr> security_groups;
	};
};
#endif
