/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <gtest/gtest.h>

#include "SFwallCore/Alias.h"
#include "Utils/IP6Addr.h"

using namespace std;

TEST(IpRangeAlias, complexMatch) {
	SFwallCore::Alias *alias = new SFwallCore::IpRangeAlias();
	alias->addEntry("10.1.1.1-10.1.1.5");
	alias->addEntry("10.1.2.1-10.1.2.10");
	alias->addEntry("192.168.1.1-192.168.10.1");
	alias->addEntry("192.168.8.1-192.168.8.1");

	EXPECT_TRUE(alias->searchForNetworkMatch(IP4Addr::stringToIP4Addr("10.1.1.1")));
	EXPECT_TRUE(alias->searchForNetworkMatch(IP4Addr::stringToIP4Addr("10.1.1.2")));
	EXPECT_TRUE(alias->searchForNetworkMatch(IP4Addr::stringToIP4Addr("10.1.1.3")));
	EXPECT_FALSE(alias->searchForNetworkMatch(IP4Addr::stringToIP4Addr("10.1.1.6")));

	EXPECT_TRUE(alias->searchForNetworkMatch(IP4Addr::stringToIP4Addr("10.1.2.5")));
	EXPECT_FALSE(alias->searchForNetworkMatch(IP4Addr::stringToIP4Addr("10.1.2.11")));

	EXPECT_TRUE(alias->searchForNetworkMatch(IP4Addr::stringToIP4Addr("192.168.2.10")));
	EXPECT_TRUE(alias->searchForNetworkMatch(IP4Addr::stringToIP4Addr("192.168.8.1")));
	EXPECT_FALSE(alias->searchForNetworkMatch(IP4Addr::stringToIP4Addr("192.168.10.2")));
}

TEST(IpSubnetAlias, complexMatch) {
	SFwallCore::Alias *alias = new SFwallCore::IpSubnetAlias();
	alias->addEntry("10.1.1.0/255.255.255.0");
	alias->addEntry("10.1.2.0/255.255.255.0");
	alias->addEntry("192.168.1.1/255.255.255.255");
	alias->addEntry("192.168.1.10/255.255.255.240");
	alias->addEntry("7.7.7.7/32");
	alias->addEntry("8.8.8.8/24");

	EXPECT_TRUE(alias->searchForNetworkMatch(IP4Addr::stringToIP4Addr("10.1.1.1")));
	EXPECT_TRUE(alias->searchForNetworkMatch(IP4Addr::stringToIP4Addr("10.1.1.2")));
	EXPECT_TRUE(alias->searchForNetworkMatch(IP4Addr::stringToIP4Addr("10.1.1.3")));
	EXPECT_TRUE(alias->searchForNetworkMatch(IP4Addr::stringToIP4Addr("192.168.1.10")));
	EXPECT_TRUE(alias->searchForNetworkMatch(IP4Addr::stringToIP4Addr("192.168.1.11")));
	EXPECT_TRUE(alias->searchForNetworkMatch(IP4Addr::stringToIP4Addr("192.168.1.12")));
	EXPECT_TRUE(alias->searchForNetworkMatch(IP4Addr::stringToIP4Addr("7.7.7.7")));
	EXPECT_TRUE(alias->searchForNetworkMatch(IP4Addr::stringToIP4Addr("8.8.8.9")));
	EXPECT_FALSE(alias->searchForNetworkMatch(IP4Addr::stringToIP4Addr("10.1.3.1")));

	EXPECT_TRUE(alias->searchForNetworkMatch(IP4Addr::stringToIP4Addr("10.1.2.5")));

	EXPECT_TRUE(alias->searchForNetworkMatch(IP4Addr::stringToIP4Addr("192.168.1.1")));
	EXPECT_FALSE(alias->searchForNetworkMatch(IP4Addr::stringToIP4Addr("192.168.1.2")));
}

TEST(IpSubnetAliasV6, complexMatch) {
	SFwallCore::Alias *alias = new SFwallCore::IpSubnetAlias();
	alias->addEntry("2607:f0d0:2001:a::0/64");
	alias->addEntry("2607:f0d0:2001:b::1/128");

	struct in6_addr addr = IP6Addr::fromString("2607:f0d0:2001:a::1");
	struct in6_addr invalid = IP6Addr::fromString("2604:f0d0:2001:a::1");
	EXPECT_TRUE(alias->searchForNetworkMatch(&addr));
	EXPECT_FALSE(alias->searchForNetworkMatch(&invalid));
}

TEST(IpSubnetAlias, add_remove_list) {
	SFwallCore::Alias *alias = new SFwallCore::IpSubnetAlias();
	alias->addEntry("2607:f0d0:2001:a::0/64");
	alias->addEntry("10.1.1.1/255.255.255.0");

	EXPECT_TRUE(alias->listEntries().size() == 2);
	alias->removeEntry("2607:f0d0:2001:a::0/64");
	alias->removeEntry("10.1.1.1/255.255.255.0");
	EXPECT_TRUE(alias->listEntries().size() == 0);
}


TEST(WebsiteListAlias, falseForSearch) {
	SFwallCore::Alias *alias = new SFwallCore::WebsiteListAlias();
	EXPECT_FALSE(alias->searchForNetworkMatch(IP4Addr::stringToIP4Addr("10.1.2.5")));
}

TEST(WebsiteListAlias, complexMatch) {
	SFwallCore::WebsiteListAlias *alias = new SFwallCore::WebsiteListAlias();
	alias->addEntry("google.com");
	alias->addEntry("yahoo.com");
	alias->addEntry("msn.com");

	EXPECT_FALSE(alias->search("google.comisdfs"));
	EXPECT_TRUE(alias->search("google.com"));
	EXPECT_TRUE(alias->search("yahoo.com"));
	EXPECT_TRUE(alias->search("msn.com"));
	EXPECT_FALSE(alias->search("wesmsn.com"));
}

TEST(IpRangeAlias, invalidAdd) {
	SFwallCore::Alias *alias = new SFwallCore::IpRangeAlias();
	alias->addEntry("fuck me");
	alias->addEntry("10.1.1.1");
	alias->addEntry("10.1.2.1-10.1.2.10");
	EXPECT_TRUE(alias->listEntries().size() == 2);
}

TEST(IpSubnetAlias, invalidAdd) {
	SFwallCore::Alias *alias = new SFwallCore::IpSubnetAlias();
	alias->addEntry("10.1.1.ss");
	alias->addEntry("10.1.1.1");
	alias->addEntry("10.1.2.1/255.255.255.0");

	EXPECT_TRUE(alias->listEntries().size() == 2);
}

TEST(MacAddressListAlias, add_remove){
	SFwallCore::Alias *alias = new SFwallCore::MacAddressListAlias();
	alias->addEntry("aa:bb:cc:dd:ee:ff");
	alias->addEntry("aa:bb:cc:dd:ee:f3");
	EXPECT_TRUE(alias->listEntries().size() == 2);

	alias->removeEntry("aa:bb:cc:dd:ee:ff");
	EXPECT_TRUE(alias->listEntries().size() == 1);
}

TEST(MacAddressListAlias, search){
	SFwallCore::Alias *alias = new SFwallCore::MacAddressListAlias();
	alias->addEntry("aa:bb:cc:dd:ee:ff");
	alias->addEntry("00:bb:cc:dd:ee:ff");
	alias->addEntry("01:bb:cc:dd:ee:ff");
	EXPECT_TRUE(alias->listEntries().size() == 3);

	EXPECT_TRUE(alias->search("aa:bb:cc:dd:ee:ff"));
	EXPECT_TRUE(alias->search("00:bb:cc:dd:ee:ff"));
	EXPECT_TRUE(alias->search("00:bb:cc:dd:ee:ff"));
	EXPECT_FALSE(alias->search("88:bb:cc:dd:ee:ff"));
}

TEST(StringWildcardListAlias, add_remove){
	SFwallCore::Alias *alias = new SFwallCore::StringWildcardListAlias();
	alias->addEntry("google");
	alias->addEntry("porn");
	EXPECT_TRUE(alias->listEntries().size() == 2);

	alias->removeEntry("google");
	EXPECT_TRUE(alias->listEntries().size() == 1);

	alias->removeEntry("porn");
	EXPECT_TRUE(alias->listEntries().size() == 0);
}

TEST(StringWildcardListAlias, search){
	SFwallCore::Alias *alias = new SFwallCore::StringWildcardListAlias();
	alias->addEntry("google");
	alias->addEntry("porn");
	EXPECT_TRUE(alias->listEntries().size() == 2);

	EXPECT_FALSE(alias->search("yahoo.com"));
	EXPECT_TRUE(alias->search("domain.google.co.nz"));
	EXPECT_TRUE(alias->search("google.com"));
	EXPECT_TRUE(alias->search("xxxxpornxxxxx"));
}

TEST(isLocal, isLocal){
	EXPECT_TRUE(IP4Addr::isLocal(IP4Addr::stringToIP4Addr("192.168.2.199")));
}

