/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "DnsHandler.h"
#include "SFwallCore/Connection.h"
#include "SFwallCore/Packet.h"
#include "Utils/DnsType.h"
#include <Core/System.h>
#include <Core/Logger.h>
#include <SFwallCore/Firewall.h>
#include <memory>

using namespace SFwallCore;


DnsHandler::DnsHandler() {

}

DnsHandler::~DnsHandler()
{}

bool DnsHandler::supportsProto(int p) {
	return p == UDP;
}

void DnsHandler::handle(SFwallCore::PacketDirection direction, SFwallCore::Connection *connection, SFwallCore::ApplicationFrame *frame) {
	if (frame->getSize() <= (int) sizeof(dns_packet_header)) {
		return;
	}

	ConnectionApplicationInfo *cai = connection->getApplicationInfo();
	DnsMessage* message = new DnsMessage((char*) frame->getInternal());
	if(direction == DIR_SAME){	
		for (DnsQuestion* question : message->getQuestions()) {
			cai->dnsQueryName = question->getQName();
			cai->dnsQueryNameSet = true;
			cai->type = SFwallCore::ConnectionApplicationInfo::Classifier::DNS;
		}
	}

	if(direction == DIR_OPPOSITE){
		for(DnsAnswer* ans : message->getAnswers()){
			DnsResourceRecord* rr = ans->getResourceRecord();
			
			if(rr){	
				DnsARecord* a_record = dynamic_cast<DnsARecord*>(rr);
				if(a_record){
					cai->dnsQueryAnswerSet = true;
					cai->dnsQueryAAnswers.insert(ntohl(a_record->getIP()));
					continue;
				}

				DnsCNAMERecord* cname = dynamic_cast<DnsCNAMERecord*>(rr);	
				if(cname){
					cai->dnsQueryCNameAnswers.insert(cname->getCanonicalName());
					continue;
				}
			}
		}
	}

	delete message;
}

bool DnsHandler::tls(SFwallCore::Connection *connection, SFwallCore::ApplicationFrame *frame) {
	return false;
}

bool DnsHandler::enabled(){
	return false;
}
