/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef HTTPHANDLER_H
#define HTTPHANDLER_H

#include <SFwallCore/ApplicationLevel/Handlers.h>

using namespace SFwallCore;

class HttpHandler : public virtual SFwallCore::ApplicationLayerTrackerHandler {
	public:
		HttpHandler();
		HttpHandler(const HttpHandler &other);
		~HttpHandler();

		virtual bool supportsProto(int protocol);
		virtual void handle(SFwallCore::PacketDirection direction, SFwallCore::Connection *connection, SFwallCore::ApplicationFrame *frame);
		virtual bool tls(SFwallCore::Connection *connection, SFwallCore::ApplicationFrame *frame);
};

class HttpsHandler : public virtual SFwallCore::ApplicationLayerTrackerHandler {
	public:
		HttpsHandler();
		HttpsHandler(const HttpsHandler &other);
		~HttpsHandler();
		virtual bool supportsProto(int protocol);
		virtual void handle(SFwallCore::PacketDirection direction, SFwallCore::Connection *connection, SFwallCore::ApplicationFrame *frame);
		virtual bool tls(SFwallCore::Connection *connection, SFwallCore::ApplicationFrame *frame);
};

#endif // HTTPHANDLER_H
