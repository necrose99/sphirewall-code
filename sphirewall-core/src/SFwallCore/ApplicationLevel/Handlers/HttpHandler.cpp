/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "HttpHandler.h"
#include "SFwallCore/Connection.h"
#include "SFwallCore/Packet.h"
#include <Utils/TlsUtils.h>
#include <boost/regex.hpp>


using namespace SFwallCore;


HttpsHandler::HttpsHandler() {

}

HttpsHandler::HttpsHandler(const HttpsHandler &other) {

}

HttpsHandler::~HttpsHandler() {

}

bool HttpsHandler::supportsProto(int p) {
	return (p == SFwallCore::TCP) ? true : false;
}

void HttpsHandler::handle(SFwallCore::PacketDirection direction, SFwallCore::Connection *connection, SFwallCore::ApplicationFrame *frame) {
	if (frame->getSize() <= 0 || connection->getApplicationInfo()->http_hostNameSet) {
		return;
	}

	if (tls(connection, frame)) {
		const char *title = parse_tls_header((const char *)frame->getInternal(), frame->getSize()) ;

		if (title) {
			connection->getApplicationInfo()->setHttpHost(title);
			connection->getApplicationInfo()->tls = true;
		}
	}
	else {
		connection->getApplicationInfo()->type = SFwallCore::ConnectionApplicationInfo::Classifier::HTTP;
		connection->getApplicationInfo()->sslv3= true;
		connection->getApplicationInfo()->http_hostNameSet = true;
	}
}

bool HttpsHandler::tls(SFwallCore::Connection *connection, SFwallCore::ApplicationFrame *frame) {
	const char *p = (const char *) frame->getInternal();

	if (p[0] != TLS_HANDSHAKE_CONTENT_TYPE) {
		return false;
	}

	return true;
}


HttpHandler::HttpHandler() {

}

HttpHandler::HttpHandler(const HttpHandler &other) {

}

HttpHandler::~HttpHandler() {

}

bool HttpHandler::supportsProto(int protocol) {
	return (protocol == TCP) ? true : false;
}

int isWhitespace(const char c) {
	if (c == ' ' || c == '\r' || c == '\t' || c == '\n') {
		return true;
	}

	return false;
}

void HttpHandler::handle(SFwallCore::PacketDirection direction, SFwallCore::Connection *connection, SFwallCore::ApplicationFrame *frame) {
	ConnectionApplicationInfo *appinfo = connection->getApplicationInfo();

	if (frame->getSize() <= 0) {
		return;
	}

	if (direction == DIR_SAME) {
		//If we get this hit, null out the response headers
		appinfo->httpHeadersParsed = true;
		appinfo->httpHeadersResponseParsed = false;

		appinfo->http_clear(RESPONSE);
		appinfo->http_clear(REQUEST);
	}
	else {
		//Check if we have already evaluated response headers, if not, evaluate
		if (appinfo->httpHeadersResponseParsed) {
			return;
		}

		appinfo->httpHeadersResponseParsed = true;
	}

	const char *packet = (const char *) frame->getInternal();
	const char newline[2] = {0x0D, 0x0A};
	string hkey = "", hvalue = "";
	string request = "";
	enum PATH { PATH_REQUEST, PATH_KEY, PATH_VALUE, PATH_DATA_BLOB };

	for (uint i = 0, path = PATH_REQUEST; i < frame->getSize(); i++) {
		switch (path) {
			case PATH_REQUEST: {
						   if ((uint16_t) packet[i] == (uint16_t) *newline) {
							   path = PATH_KEY;
							   appinfo->http_set(direction == DIR_SAME ? REQUEST : RESPONSE, "REQUEST", request);
						   }
						   else {
							   request += packet[i];
						   }
					   };

					   break;

			case PATH_KEY: {
					       if((uint16_t) packet[i] == (uint16_t) *newline){
						       path = PATH_DATA_BLOB;
						       break;
					       }

					       if (packet[i] == ':') {
						       path = PATH_VALUE;

						       do {
							       i++;
						       }
						       while (isWhitespace(packet[i]));

						       i--;
					       }
					       else if (!isWhitespace(packet[i])) {
						       hkey.append(&packet[i], 1);
					       }
				       };

				       break;

			case PATH_VALUE: {
						 if ((uint16_t) packet[i] == (uint16_t) *newline && packet[i + 2] != '\t') {
							 appinfo->http_set(direction == DIR_SAME ? REQUEST : RESPONSE, hkey, hvalue);

							 hkey = "";
							 hvalue = "";
							 path = PATH_KEY;
						 }
						 else {
							 hvalue.append(&packet[i], 1);
						 }
					 };

					 break;

			case PATH_DATA_BLOB: {
						     return;
					     };

			default:
					     break;
		}
	}


	if (direction == DIR_SAME && appinfo->http_contains(REQUEST, HTTP_HOSTNAME)) {
		appinfo->setHttpHost(appinfo->http_get(REQUEST, HTTP_HOSTNAME));
	}
}

bool HttpHandler::tls(SFwallCore::Connection *connection, SFwallCore::ApplicationFrame *frame) {
	return false;
}
