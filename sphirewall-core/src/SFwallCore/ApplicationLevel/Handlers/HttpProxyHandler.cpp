/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <boost/regex.hpp>

#include "HttpProxyHandler.h"
#include <Utils/TlsUtils.h>
#include "SFwallCore/Connection.h"
#include "SFwallCore/Packet.h"

using namespace boost;

HttpProxyHandler::HttpProxyHandler() {

}

HttpProxyHandler::HttpProxyHandler(const HttpProxyHandler &other) {

}

HttpProxyHandler::~HttpProxyHandler() {

}

bool HttpProxyHandler::supportsProto(int protocol) {
	return (protocol == SFwallCore::TCP) ? true : false;
}

void HttpProxyHandler::handle(SFwallCore::PacketDirection direction, SFwallCore::Connection *connection, SFwallCore::ApplicationFrame *frame) {
	if (frame->getSize() <= 0 || (connection->getApplicationInfo()->http_hostNameSet && direction == DIR_SAME) || direction == DIR_OPPOSITE) {
		return;
	}

	static boost::regex getRequest("^GET http://(.*?)/.*? HTTP/1\\.[01]$");
	std::string packetData((const char *) frame->getInternal(), frame->getSize());
	std::string packetUrl = "";

	boost::smatch what;

	if (regex_search(packetData, what, getRequest)) {
		connection->getApplicationInfo()->setHttpHost(what[1]);
		return;
	}

	static boost::regex connectRequest("^CONNECT ([a-z,A-Z,\\-,0-9,\\.]+)");
	boost::smatch what2;

	while (boost::regex_search(packetData, what2, connectRequest)) {
		connection->getApplicationInfo()->setHttpHost(what2[1]);
		connection->getApplicationInfo()->tls = true; //CONNECT is reserverd for tunnels: http://tools.ietf.org/html/rfc2616#page-57 - We mark it as https so it can be treated as so
		return;
	}

	static boost::regex normalRequest("^Host: ([a-z,A-Z,\\-,0-9,\\.]+)");
	boost::smatch what3;

	if (regex_search(packetData, what3, normalRequest)) {
		connection->getApplicationInfo()->setHttpHost(what3[1]);
		return;
	}

	const char *title = parse_tls_header((const char *) frame->getInternal(), frame->getSize());

	if (title) {
		connection->getApplicationInfo()->setHttpHost(title);
		connection->getApplicationInfo()->tls = true;
		return;
	}
}

bool HttpProxyHandler::tls(SFwallCore::Connection *connection, SFwallCore::ApplicationFrame *frame) {
	return false;
}
