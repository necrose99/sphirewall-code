/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef HTTPPROXYHANDLER_H
#define HTTPPROXYHANDLER_H

#include <SFwallCore/ApplicationLevel/Handlers.h>
#include <Core/ConfigurationManager.h>

using namespace SFwallCore;

class HttpProxyHandler : public SFwallCore::ApplicationLayerTrackerHandler {
	public:
		HttpProxyHandler();
		HttpProxyHandler(const HttpProxyHandler &other);
		~HttpProxyHandler();
		virtual bool supportsProto(int protocol);
		virtual void handle(SFwallCore::PacketDirection direction, SFwallCore::Connection *connection, SFwallCore::ApplicationFrame *frame);
		virtual bool tls(SFwallCore::Connection *connection, SFwallCore::ApplicationFrame *frame);
};

#endif // HTTPPROXYHANDLER_H
