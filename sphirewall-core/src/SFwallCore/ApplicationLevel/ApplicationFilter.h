/*
   Copyright Michael Lawson
   This file is part of Sphirewall.

   Sphirewall is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Sphirewall is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef APP_LEVEL_FILTER
#define APP_LEVEL_FILTER

#include <pthread.h>
#include <unordered_set>
#include "SFwallCore/Alias.h"
#include "Utils/Lock.h"
#include "Utils/Utils.h"
#include "Auth/GroupDb.h"
#include "SFwallCore/Connectionfwd.h"
#include "SFwallCore/TimePeriods.h"
#include "SFwallCore/ApplicationLevel/FilterHandler.h"
#include "SFwallCore/Criteria.h"
#include "SFwallCore/Connection.h"

class TimePeriod;
class ConfigurationManager;
class Group;
class User;
class EventDb;
class UserDb;

namespace SFwallCore {
	class WebsiteListAlias;
	class Alias;
	class AliasDb;
	class Packet;
	typedef std::shared_ptr<Alias> AliasPtr;

	enum ApplicationFilterAction {
		ALLOW = 1, /* Whitelist*/
		DENY = 0 /* Blacklist */
	};

	class FilterCriteria : public Lockable {
		public:
			FilterCriteria();
			~FilterCriteria(){
				delete hits_per_minute;
			}

			string id;

			ApplicationFilterAction action;
			bool log;
			bool fireEvent;
			bool redirect;
			std::string redirectUrl;
			bool enabled;

			bool temp_rule;
			int expiry_timestamp;			

			std::string metadata;
			std::list<CriteriaPtr> criteria;
			std::list<CriteriaPtr> source_criteria;

			std::string name;

                        double hits;
                        int last_hit;
			FlexibleSecondIntervalSampler* hits_per_minute;

                        bool match(SFwallCore::Packet *packet);
			void refresh();
	};

	typedef boost::shared_ptr<FilterCriteria> ApplicationLayerFilterCriteriaPtr;

	class ApplicationFilter : 	public SignatureStoreChangeHandler, 
					public IntMgrChangeListener, 
					public Configurable, 
					public ApplicationLayerFilterHandler, 
					public TimePeriodDeleteListener, 
					public AliasRemovedListener, 
					public GroupRemovedListener, 
					public Lockable {

		public:
			class CleanupCronJob : public CronJob {
				public:
					CleanupCronJob(ApplicationFilter* filter) : 
						CronJob(60 * 5, "APPLICATION_FILTER_EXPIRED_RULE_CLEANUP", true), filter(filter){
					}
					void run();
	
				ApplicationFilter* filter;
			};

			ApplicationFilter(AliasDb *aliases, GroupDb *groupDb, EventDb *eventDb, UserDb *userDb);
			void process(SFwallCore::Connection *conn, SFwallCore::Packet *packet);

			bool matchesIpCriteria(Connection *conn, Packet *packet);
			bool enabled();

			bool load();
			void save();

			void addRule(ApplicationLayerFilterCriteriaPtr rule, bool top);
			void removeRule(ApplicationLayerFilterCriteriaPtr rule);
			std::list<ApplicationLayerFilterCriteriaPtr> getRules();
			ApplicationLayerFilterCriteriaPtr get(std::string id);
			AliasDb *getAliasDb();

			void setEventDb(EventDb *eventDb);

			void moveup(const ApplicationLayerFilterCriteriaPtr target);
			void movedown(const ApplicationLayerFilterCriteriaPtr target);

			void timePeriodDeleted(TimePeriodPtr removed);
			const char* getConfigurationSystemName(){
				return "Application layer filter";
			}

			void interface_change(InterfacePtr) {
				refresh();
			}	

			void signature_store_changed(){
				refresh();
			}

			void refresh();
			void sample(map<string, double> &input);
		private:
			int findpos(const ApplicationLayerFilterCriteriaPtr target);
			void insert(int pos, ApplicationLayerFilterCriteriaPtr target);
			std::list<ApplicationLayerFilterCriteriaPtr> filters;

			AliasDb* aliases;
			GroupDb* groupDb;
			UserDb* userDb;
			EventDb* eventDb;

			void aliasRemoved(AliasPtr alias);
			void groupRemoved(GroupPtr group);
			void handleMatchFound(const ApplicationLayerFilterCriteriaPtr matching, Connection *conn, Packet *packet);

			int requires;
	};
};
#endif
