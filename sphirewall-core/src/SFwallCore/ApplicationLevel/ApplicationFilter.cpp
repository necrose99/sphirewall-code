/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <list>
#include <boost/regex.hpp>

#include "SFwallCore/Packet.h"
#include "SFwallCore/Firewall.h"
#include "SFwallCore/Connection.h"
#include "Core/Config.h"
#include "SFwallCore/ApplicationLevel/ApplicationFilter.h"
#include "Json/JSON.h"
#include "Json/JSONValue.h"
#include "Core/ConfigurationManager.h"
#include "Core/System.h"
#include "SFwallCore/Rewrite.h"
#include "SFwallCore/Alias.h"
#include "Utils/TimeWrapper.h"
#include "Core/Event.h"
#include "SFwallCore/TimePeriods.h"
#include "Utils/StringUtils.h"
#include "Utils/Utils.h"

bool SFwallCore::ApplicationFilter::load() {
	filters.clear();
	ObjectContainer *root;

	if (configurationManager->has("webfilter")) {
		root = configurationManager->getElement("webfilter");
		if(root->has("rules")){
			ObjectContainer *rules = root->get("rules")->container();

			for (int x = 0; x < rules->size(); x++) {
				ObjectContainer *source = rules->get(x)->container();
				ApplicationLayerFilterCriteriaPtr target = ApplicationLayerFilterCriteriaPtr(new FilterCriteria());
				target->id = source->get("id")->string();

				if(source->has("criteria")){
					ObjectContainer* criteria = source->get("criteria")->container();	
					for(int y = 0; y < criteria->size(); y++){
						ObjectContainer* individualCriteria = criteria->get(y)->container();
						target->criteria.push_back(CriteriaPtr(CriteriaBuilder::parse(individualCriteria)));
					}
				}

                                if(source->has("source_criteria")){
                                        ObjectContainer* criteria = source->get("source_criteria")->container();
                                        for(int y = 0; y < criteria->size(); y++){
                                                ObjectContainer* individualCriteria = criteria->get(y)->container();
                                                target->source_criteria.push_back(CriteriaPtr(CriteriaBuilder::parse(individualCriteria)));
                                        }
                                }

				if (source->has("fireEvent")) {
					target->fireEvent = source->get("fireEvent")->boolean();
				}
				if (source->has("name")) {
					target->name = source->get("name")->string();
				}

				if (source->has("action")) {
					target->action = (ApplicationFilterAction) source->get("action")->number();
				}

				if (source->has("redirect")) {
					target->redirect = source->get("redirect")->boolean();
					target->redirectUrl = source->get("redirectUrl")->string();
				}

				if (source->has("enabled")) {
					target->enabled  = source->get("enabled")->boolean();
				}

				if (source->has("metadata")) {
					target->metadata = source->get("metadata")->string();
				}

				if(source->has("temp_rule")){
					target->temp_rule = source->get("temp_rule")->boolean();
					target->expiry_timestamp = source->get("expiry_timestamp")->number();
				}

				filters.push_back(target);
			}
		}
	}

	refresh();
	return true;
}

void SFwallCore::ApplicationFilter::save() {
	ObjectContainer *root = new ObjectContainer(CREL);
	ObjectContainer *rulesArray = new ObjectContainer(CARRAY);

	for (ApplicationLayerFilterCriteriaPtr source : filters) {
		ObjectContainer *target = new ObjectContainer(CREL);
		target->put("id", new ObjectWrapper((string) source->id));

		ObjectContainer* criteria = new ObjectContainer(CARRAY);
		for(CriteriaPtr target : source->criteria){
			criteria->put(new ObjectWrapper(CriteriaBuilder::serialize(target.get())));
		}
		target->put("criteria", new ObjectWrapper(criteria));

                ObjectContainer* source_criteria = new ObjectContainer(CARRAY);
                for(CriteriaPtr target : source->source_criteria){
                        source_criteria->put(new ObjectWrapper(CriteriaBuilder::serialize(target.get())));
                }

                target->put("source_criteria", new ObjectWrapper(source_criteria));

		target->put("fireEvent", new ObjectWrapper((bool) source->fireEvent));
		target->put("action", new ObjectWrapper((double) source->action));
		target->put("redirect", new ObjectWrapper((bool) source->redirect));
		target->put("redirectUrl", new ObjectWrapper((string) source->redirectUrl));
		target->put("metadata", new ObjectWrapper((string) source->metadata));
		target->put("name", new ObjectWrapper((string) source->name));
		target->put("enabled", new ObjectWrapper((bool) source->enabled));

		target->put("temp_rule", new ObjectWrapper((bool) source->temp_rule));
		target->put("expiry_timestamp", new ObjectWrapper((double) source->expiry_timestamp));
		rulesArray->put(new ObjectWrapper(target));
	}

	root->put("rules", new ObjectWrapper(rulesArray));

	configurationManager->holdLock();
	configurationManager->setElement("webfilter", root);
	configurationManager->save();
	configurationManager->releaseLock();
}

void SFwallCore::FilterCriteria::refresh(){
	holdLock();
	for(CriteriaPtr target : criteria){
		target->refresh();
	}

	for(CriteriaPtr target : source_criteria){
		target->refresh();
	}

	releaseLock();
}

void SFwallCore::ApplicationFilter::refresh(){
	requires = 0;	
	for (ApplicationLayerFilterCriteriaPtr source : getRules()) {
		source->holdLock();
		if(source->enabled){
			for(CriteriaPtr target : source->criteria){
				if(target->lowPassApplicationFilterType() != ConnectionApplicationInfo::Classifier::NDEF){
					requires = ( requires | target->lowPassApplicationFilterType());	

					Logger::instance()->log("sphirewalld.firewall.applicationfilter.refresh", INFO, 
							"Adding low pass filter for connection info type %d", target->lowPassApplicationFilterType());
				}
			}

			for(CriteriaPtr target : source->source_criteria){
				if(target->lowPassApplicationFilterType() != ConnectionApplicationInfo::Classifier::NDEF){
					requires = ( requires | target->lowPassApplicationFilterType());

					Logger::instance()->log("sphirewalld.firewall.applicationfilter.refresh", INFO,
							"Adding low pass filter for connection info type %d", target->lowPassApplicationFilterType());
				}
			}
		}
		source->releaseLock();
		source->refresh();
	}
}


SFwallCore::AliasDb *SFwallCore::ApplicationFilter::getAliasDb() {
	return aliases;
}

void SFwallCore::ApplicationFilter::setEventDb(EventDb *eventDb) {
	this->eventDb = eventDb;
}

	SFwallCore::ApplicationFilter::ApplicationFilter(SFwallCore::AliasDb *aliases, GroupDb *groupDb, EventDb *eventDb, UserDb * userDb)
: aliases(aliases), groupDb(groupDb), eventDb(eventDb), userDb(userDb)
{}


void SFwallCore::ApplicationFilter::timePeriodDeleted(TimePeriodPtr removed) {
	refresh();
}

void SFwallCore::ApplicationFilter::aliasRemoved(AliasPtr alias) {
	refresh();
}

bool SFwallCore::ApplicationFilter::matchesIpCriteria(Connection *conn, Packet *packet) {
	if(conn->getApplicationInfo()->type == ConnectionApplicationInfo::Classifier::HTTP || conn->getApplicationInfo()->exclusive_signature){
		return true;
	}

	return false;
}

bool SFwallCore::ApplicationFilter::enabled() {
	return System::getInstance()->getFirewall()->HTTP_FILTERING_ENABLED == 1;
}

void SFwallCore::ApplicationFilter::sample(map<string, double> &input){
	for (ApplicationLayerFilterCriteriaPtr filter : filters) {
		stringstream ss; ss << "firewall.applicationfilter." << filter->id << ".average_hits_per_minute";
		input[ss.str()] = filter->hits_per_minute->value();
	}
}

void SFwallCore::ApplicationFilter::process(Connection *conn, SFwallCore::Packet *packet) {
	if(conn->checkPacket(packet, DIR_SAME)){
		conn->getApplicationInfo()->afProcessedRequest= true;	
	}else{
		conn->getApplicationInfo()->afProcessedResponse= true;	
	}

	bool should_save = false;
	for (ApplicationLayerFilterCriteriaPtr filter : getRules()) {
		if(filter->temp_rule && time(NULL) > filter->expiry_timestamp){
			removeRule(filter);
			should_save = true;
		}

		if (filter->match(packet)) {
			conn->verdict_application_rule = filter;
			conn->verdict_application_rule->hits_per_minute->input(1);
			filter->hits++;
			filter->last_hit = time(NULL);

			packet->getConnection()->getApplicationInfo()->afVerdictApplied = true;
			if (filter->action != ALLOW) {
				if (conn->getApplicationInfo()->type == SFwallCore::ConnectionApplicationInfo::Classifier::HTTP 
						&& !conn->getApplicationInfo()->tls && !conn->getApplicationInfo()->sslv3 && filter->redirect) {

					conn->setRewriteRule(new HttpRedirectRewrite(filter->name, filter->redirectUrl));
				}
				else {
					conn->terminate();
				}

				packet->incrementRuleMetrics(LAYER_TYPE_7, SQ_DENY);
				conn->getApplicationInfo()->app_filtering_denied = true;
			} else {
				packet->incrementRuleMetrics(LAYER_TYPE_7, SQ_ACCEPT);
			}

			if (filter->fireEvent && eventDb) {
				EventParams params;
				if(conn->getApplicationInfo()->exclusive_signature){
					params["tags"] = conn->getApplicationInfo()->exclusive_signature->id();
				}

				params["host"] = packet->getConnection()->getApplicationInfo()->http_hostName;
				params["sourceIp"] = packet->getConnection()->getIp()->getSrcIpString();
				params["macAddress"] = packet->getHw();
				params["metadata"] = filter->metadata;
				params["policy.name"] = filter->name;
				params["policy.id"] = filter->id;

				if(packet->getConnection()->getApplicationInfo()->exclusive_signature){
					params["tag.id"] = packet->getConnection()->getApplicationInfo()->exclusive_signature->id();
					params["tag.name"] = packet->getConnection()->getApplicationInfo()->exclusive_signature->name();
				}

				if (packet->getUser()) {
					params["user"] = packet->getUser()->getUserName();
				}

				eventDb->add(new Event(FIREWALL_WEBFILTER_HIT, params));
			}

			return;
		}
	}

	if(should_save){
		save();
	}

	return;
}

SFwallCore::FilterCriteria::FilterCriteria() {
	this->log = false;
	this->fireEvent = false;
	this->action = DENY;
	this->redirect = false;
	this->enabled = false;
	this->temp_rule = false;	
	this->expiry_timestamp = -1;	
	this->hits = 0;
	this->last_hit = 0;
	this->hits_per_minute = new FlexibleSecondIntervalSampler();
}

bool SFwallCore::FilterCriteria::match(SFwallCore::Packet *packet) {
	holdLock();
	if(!enabled || criteria.size() == 0){
		releaseLock();
		return false;
	}

	for(auto c : criteria){
		if(!c->match(packet, DIRECTION_NA)){
			releaseLock();
			return false;
		}
	}

	for(auto c : source_criteria){
		if(!c->match(packet, DIRECTION_SOURCE)){
			releaseLock();
			return false;
		}
	}

	releaseLock();
	return true;
}

void SFwallCore::ApplicationFilter::addRule(SFwallCore::ApplicationLayerFilterCriteriaPtr rule, bool top) {
	holdLock();
	if(top){
		filters.insert(filters.begin(), rule);
	}else{
		filters.push_back(rule);
	}
	releaseLock();
}

void SFwallCore::ApplicationFilter::removeRule(SFwallCore::ApplicationLayerFilterCriteriaPtr rule) {
	holdLock();
	filters.remove(rule);
	releaseLock();
}

std::list<SFwallCore::ApplicationLayerFilterCriteriaPtr> SFwallCore::ApplicationFilter::getRules() {
	std::list<SFwallCore::ApplicationLayerFilterCriteriaPtr> ret;
	holdLock();
	ret = filters;
	releaseLock();
	return ret;
}

int SFwallCore::ApplicationFilter::findpos(ApplicationLayerFilterCriteriaPtr target) {
	int n = 0;
	for (auto x : filters) {
		if (x == target) {
			return n;
		}
		n++;
	}

	return -1;
}

void SFwallCore::ApplicationFilter::moveup(ApplicationLayerFilterCriteriaPtr target) {
	holdLock();
	int currentpos = findpos(target);
	if (currentpos <= 0) {
		return;
	}

	filters.remove(target);
	insert(currentpos - 1, target);
	releaseLock();
}

void SFwallCore::ApplicationFilter::insert(int pos, ApplicationLayerFilterCriteriaPtr target) {
	int n = 0;

	for (list<ApplicationLayerFilterCriteriaPtr>::iterator iter = filters.begin();
			iter != filters.end();
			iter++) {

		if (n == pos) {
			filters.insert(iter, target);
			return;
		}

		n++;
	}

	filters.push_back(target);
}

void SFwallCore::ApplicationFilter::movedown(ApplicationLayerFilterCriteriaPtr target) {
	holdLock();
	int currentpos = findpos(target);

	if ((size_t)currentpos == filters.size() - 1) {
		return;
	}

	filters.remove(target);
	insert(currentpos + 1, target);
	releaseLock();
}

SFwallCore::ApplicationLayerFilterCriteriaPtr SFwallCore::ApplicationFilter::get(std::string id) {
	holdLock();
	for (ApplicationLayerFilterCriteriaPtr filter : filters) {
		if (filter->id.compare(id) == 0) {
			releaseLock();
			return filter;
		}
	}

	releaseLock();
	return ApplicationLayerFilterCriteriaPtr();
}

void SFwallCore::ApplicationFilter::groupRemoved(GroupPtr group) {
	refresh();
}

void SFwallCore::ApplicationFilter::CleanupCronJob::run(){
	for(auto rule : filter->getRules()){
		if(rule->temp_rule && time(NULL) > rule->expiry_timestamp){
			Logger::instance()->log("sphirewalld.firewall.applicationfilter.cleanup", INFO, 
					"Removing layer7 rule with id '%s', it was temporary and has expired", rule->id.c_str());

			filter->removeRule(rule);
		}
	}

	filter->save();
}

