#include <iostream>
#include <boost/algorithm/string.hpp>
#include <boost/format.hpp>
#include <openssl/sha.h>

#include "Core/System.h"
#include "Core/Cloud.h"
#include "Core/ConfigurationManager.h"

#include "SFwallCore/State.h"
#include "Utils/StringUtils.h"
#include "SFwallCore/Connection.h"
#include "SFwallCore/Packet.h"
#include "SFwallCore/Rewrite.h"
#include "SFwallCore/ApplicationLevel/FilterHandler.h"
#include "SFwallCore/ApplicationLevel/MyLinewizeLoginRedirector.h"

int SFwallCore::MyLinewizeRedirectHelper::MYLINEWIZE_REDIRECT_HELPER_ENABLED=0;

SFwallCore::MyLinewizeRedirectHelper::MyLinewizeRedirectHelper(){
	System::getInstance()->config.getRuntime()->loadOrPut("MYLINEWIZE_REDIRECT_HELPER_ENABLED", &MYLINEWIZE_REDIRECT_HELPER_ENABLED);
}

std::string SFwallCore::MyLinewizeRedirectHelper::get_private_key(){
	return "key123";
}

void SFwallCore::MyLinewizeRedirectHelper::process(Connection *conn, Packet *packet){
	if (conn->getApplicationInfo()->http_hostNameSet) {
		std::string host = conn->getApplicationInfo()->http_hostName;
		if (host.compare("my.linewize.net") == 0) {
			conn->getApplicationInfo()->afVerdictApplied = true;
			conn->setRewriteRule(new MyLinewizeSignedRedirect(get_private_key()));
			return;
		}
	}
}

bool SFwallCore::MyLinewizeRedirectHelper::matchesIpCriteria(Connection *conn, Packet *packet){
	if (conn->getProtocol() != TCP) {
		return false;
	}

	if (((TcpConnection *) conn)->getState()->state != TCPUP) {
		return false;
	}

	if (conn->getApplicationInfo()->type != SFwallCore::ConnectionApplicationInfo::Classifier::HTTP) {
		return false;
	}

	if(!conn->get_authenticated_user()){
		return false;
	}

	return true;
}

void SFwallCore::MyLinewizeRedirectHelper::save(){
}

bool SFwallCore::MyLinewizeRedirectHelper::load(){
	return true;
}

const string redirectTemplate = "HTTP/1.1 307 Temporary Redirect\r\n"
"Location: https://mylinewize.linewize.net/login?%1%&h=%2%&next=https://mylinewize.linewize.net/mylinewize\r\n"
"Content-Length:0\r\n\r\n";

SFwallCore::MyLinewizeSignedRedirect::MyLinewizeSignedRedirect(std::string key){
	this->private_key = key;
}

void SFwallCore::MyLinewizeSignedRedirect::rewrite(SFwallCore::Connection *connection, SFwallCore::Packet *packet) {
	stringstream hashed_params;
	hashed_params << "d=" << System::getInstance()->getCloudConnection()->getDeviceId();
	hashed_params << "&u=" << connection->get_authenticated_user()->getUserName();
	hashed_params << "&t=" << time(NULL); 

        ConfigurationManager* config = &System::getInstance()->configurationManager;
	if(!config->has("general:mylinewize_key")){
		config->put("general:mylinewize_key", StringUtils::genRandom());
	}

	std::string key = config->get("general:mylinewize_key")->string();
	stringstream hashed_params_final;
	hashed_params_final << key << "_" << hashed_params.str();

	unsigned char hash[SHA_DIGEST_LENGTH] = {0};
	SHA1((const unsigned char*) hashed_params_final.str().c_str(), hashed_params_final.str().size(), hash);

	const string redirect = boost::str(boost::format(redirectTemplate)
			% hashed_params.str()
			% StringUtils::hexdigest(hash, SHA_DIGEST_LENGTH) 
			);

	if (packet->getTransport()->getApplication()->write(redirect)) {
		if(redirect.size() > packet->getTransport()->getApplication()->getSize()){
			packet->resizePayload(redirect.size());
			connection->resizePayload(DIR_OPPOSITE, redirect.size() - packet->getTransport()->getApplication()->getSize());
		}
	}else{
		connection->terminate();
	}
}

