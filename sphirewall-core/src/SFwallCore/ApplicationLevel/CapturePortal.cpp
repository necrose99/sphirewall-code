/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <cstring>
#include <boost/regex.hpp>
#include <boost/algorithm/string.hpp>
#include "SFwallCore/Rewrite.h"
#include "SFwallCore/Packet.h"
#include "Core/ConfigurationManager.h"
#include "Core/System.h"
#include "Core/Cloud.h"
#include "SFwallCore/Connection.h"
#include "SFwallCore/State.h"
#include "Utils/DnsType.h"
#include <Utils/Dump.h>
#include "SFwallCore/Alias.h"
#include "SFwallCore/ApplicationLevel/FilterHandler.h"
#include "SFwallCore/ApplicationLevel/CapturePortal.h"

using namespace std;

int SFwallCore::CapturePortalEngine::CAPTURE_PORTAL_FORCE = 0;
int SFwallCore::CapturePortalEngine::REWRITE_PROXY_REQUESTS_ENABLED = 1;
int SFwallCore::CapturePortalEngine::CAPTURE_PORTAL_REQUIRES_CLOUD_CONNECTION = 0;
std::string SFwallCore::CapturePortalEngine::CAPTURE_PORTAL_LINEWIZE_URL = "http://login.linewize.net/login";
int SFwallCore::CapturePortalEngine::CAPTURE_PORTAL_DMZ_MODE_ENABLED = 0;
int SFwallCore::CapturePortalEngine::CAPTURE_PORTAL_MAGIC_LINK_ENABLED = 1;
int SFwallCore::CapturePortalEngine::CAPTURE_PORTAL_MAGIC_LINK_TIMEOUT = 600;
int SFwallCore::CapturePortalEngine::CAPTURE_PORTAL_MAGIC_LINK_CLICKED_TIME = 0;
std::string SFwallCore::CapturePortalEngine::CAPTURE_PORTAL_MAGIC_LINK_URL = "disablecp.linewize.net";

SFwallCore::CapturePortalEngine::CapturePortalEngine(SFwallCore::AliasDb *aliases) :
        aliases(aliases) {

        System::getInstance()->config.getRuntime()->loadOrPut("CAPTURE_PORTAL_FORCE", &CAPTURE_PORTAL_FORCE);
        System::getInstance()->config.getRuntime()->loadOrPut("REWRITE_PROXY_REQUESTS_ENABLED", &REWRITE_PROXY_REQUESTS_ENABLED);
	System::getInstance()->config.getRuntime()->loadOrPut("CAPTURE_PORTAL_REQUIRES_CLOUD_CONNECTION", &CAPTURE_PORTAL_REQUIRES_CLOUD_CONNECTION);
	System::getInstance()->config.getRuntime()->loadOrPut("CAPTURE_PORTAL_DMZ_MODE_ENABLED", &CAPTURE_PORTAL_DMZ_MODE_ENABLED);
	System::getInstance()->config.getRuntime()->loadOrPut("CAPTURE_PORTAL_MAGIC_LINK_ENABLED", &CAPTURE_PORTAL_MAGIC_LINK_ENABLED);
	System::getInstance()->config.getRuntime()->loadOrPut("CAPTURE_PORTAL_MAGIC_LINK_TIMEOUT", &CAPTURE_PORTAL_MAGIC_LINK_TIMEOUT);

	queries = 0;
	hits = 0;
	allow_local = true;
	capture_all_traffic = false;
	disable_apple_captive_prompt = false;

	iprange_inclusions_alias = AliasPtr(new IpRangeAlias());
	macaddress_inclusions_alias = AliasPtr(new MacAddressListAlias());
	website_exclusions_alias = AliasPtr(new WebsiteListAlias());
	iprange_exclusions_alias = AliasPtr(new IpRangeAlias());
	macaddress_exclusions_alias = AliasPtr(new MacAddressListAlias());

	}

void SFwallCore::CapturePortalEngine::save() {
	ObjectContainer *root = new ObjectContainer(CREL);
	ObjectContainer *oc_provided_inclusions = new ObjectContainer(CARRAY);
	ObjectContainer *oc_provided_exclusions = new ObjectContainer(CARRAY);

	ObjectContainer *oc_provided_object_inclusions = new ObjectContainer(CARRAY);
	ObjectContainer *oc_provided_object_exclusions = new ObjectContainer(CARRAY);

	for (string item : get_all_provided_inclusions()) {
		oc_provided_inclusions->put(new ObjectWrapper((string) item));
	}

	for (string item : get_all_provided_exclusions()) {
		oc_provided_exclusions->put(new ObjectWrapper((string) item));
	}

	for (AliasPtr ap : provided_inclusions_aliases) {
		oc_provided_object_inclusions->put(new ObjectWrapper((string) ap->id));
	}

	for (AliasPtr ap : provided_exclusions_aliases) {
		oc_provided_object_exclusions->put(new ObjectWrapper((string) ap->id));
	}

	root->put("object_inclusions", new ObjectWrapper(oc_provided_object_inclusions));
	root->put("object_exclusions", new ObjectWrapper(oc_provided_object_exclusions));
	root->put("inclusions", new ObjectWrapper(oc_provided_inclusions));
	root->put("exclusions", new ObjectWrapper(oc_provided_exclusions));
	root->put("endpoint", new ObjectWrapper((double) endpoint));
	root->put("mode", new ObjectWrapper((double) mode));
	root->put("allow_local", new ObjectWrapper((bool) allow_local));
	root->put("capture_all_traffic", new ObjectWrapper((bool) capture_all_traffic));
	root->put("disable_apple_captive_prompt", new ObjectWrapper((bool) disable_apple_captive_prompt));
	root->put("url", new ObjectWrapper((string) cp_url));

	configurationManager->holdLock();
	configurationManager->setElement("authentication:captiveportal", root);
	configurationManager->save();
	configurationManager->releaseLock();
}

bool SFwallCore::CapturePortalEngine::load() {
	if(configurationManager->has("authentication:captiveportal")){
		ObjectContainer* root = configurationManager->getElement("authentication:captiveportal");

		ObjectContainer *oc_object_inclusions = root->get("object_inclusions")->container();
		for (int x = 0; x < oc_object_inclusions->size(); x++) {
			string id = oc_object_inclusions->get(x)->string();

			if (aliases->get(id)) {
				provided_inclusions_aliases.push_back(aliases->get(id));
			}
		}

		ObjectContainer *oc_object_exclusions = root->get("object_exclusions")->container();
		for (int x = 0; x < oc_object_exclusions->size(); x++) {
			string id = oc_object_exclusions->get(x)->string();

			if (aliases->get(id)) {
				provided_exclusions_aliases.push_back(aliases->get(id));
			}
		}

		clear_provided_inclusions();
		clear_provided_exclusions();

		ObjectContainer *oc_exclusions = root->get("exclusions")->container();
		for (int x = 0; x < oc_exclusions->size(); x++) {
			add_provided_exclusions(StringUtils::trim(oc_exclusions->get(x)->string()));
		}

		ObjectContainer *oc_inclusions = root->get("inclusions")->container();
		for (int x = 0; x < oc_inclusions->size(); x++) {
			add_provided_inclusions(StringUtils::trim(oc_inclusions->get(x)->string()));
		}

		cp_url = root->get("url")->string();
		endpoint = root->get("endpoint")->number();
		mode = root->get("mode")->number();

		if(root->has("allow_local")){	
			allow_local = root->get("allow_local")->boolean();
		}

		if(root->has("capture_all_traffic")){
			capture_all_traffic = root->get("capture_all_traffic")->boolean();
		}

		if(root->has("disable_apple_captive_prompt")){
			disable_apple_captive_prompt = root->get("disable_apple_captive_prompt")->boolean();
		}


	}else if (configurationManager->has("authentication:ranges")) {
		ObjectContainer* root;
		root = configurationManager->getElement("authentication:ranges");

		ObjectContainer *sources = root->get("included_networks")->container();
		for (int x = 0; x < sources->size(); x++) {
			string id = sources->get(x)->string();

			AliasPtr alias = aliases->get(id);
			if (alias) {
				if(alias->name.compare("network_inclusions") == 0){
					for(std::string item : alias->listEntries()){
						add_provided_inclusions(item);
					}
				}else{	
					provided_inclusions_aliases.push_back(aliases->get(id));
				}
			}
		}

		if (root->has("websites_exceptions")) {
			ObjectContainer *sources = root->get("websites_exceptions")->container();

			for (int x = 0; x < sources->size(); x++) {
				string id = sources->get(x)->string();

				AliasPtr alias = aliases->get(id);
				if (alias) {
					if(alias->name.compare("website_exceptions") == 0){
						for(std::string item : alias->listEntries()){
							add_provided_exclusions(item);
						}
					}else{
						provided_exclusions_aliases.push_back(alias);
					}
				}
			}
		}        

		if (root->has("network_exceptions")) {
			ObjectContainer *sources = root->get("network_exceptions")->container();

			for (int x = 0; x < sources->size(); x++) {
				string id = sources->get(x)->string();

				AliasPtr alias = aliases->get(id);
				if (alias) {
					if(alias->name.compare("network_exceptions") == 0){
						for(std::string item : alias->listEntries()){
							add_provided_exclusions(item);
						}
					}else{
						provided_exclusions_aliases.push_back(alias);
					}
				}
			}
		}	

		if (root->has("mac_exceptions")) {
			ObjectContainer *sources = root->get("mac_exceptions")->container();

			for (int x = 0; x < sources->size(); x++) {
				string id = sources->get(x)->string();

				AliasPtr alias = aliases->get(id);
				if (alias) { 
					if(alias->name.compare("mac_exceptions") == 0){
						for(std::string item : alias->listEntries()){
							add_provided_exclusions(item);
						}
					}else{
						provided_exclusions_aliases.push_back(alias);
					}
				}
			}
		}

		if(configurationManager->has("authentication:cp_url")){
			std::string old_cp_url = configurationManager->get("authentication:cp_url")->string();
			if(old_cp_url.compare(CAPTURE_PORTAL_LINEWIZE_URL) == 0 || old_cp_url.compare("https://login.linewize.net/login") == 0){
				endpoint = 1;
			}else{
				endpoint = 0;
				cp_url = old_cp_url;
			}
		}

		mode = CAPTURE_PORTAL_FORCE;
		save();
	}
	initopts();
	return true;
}

bool SFwallCore::CapturePortalEngine::isWebsiteInAliases(const AliasPtr& alias, const std::string &website) {
	vector<std::string> chunks;
	chunks.push_back(website);

	for (uint x = 0; x < website.size(); x++) {
		if (website[x] == '.') {
			string target = website.substr(x + 1, website.size() - x + 1);
			chunks.push_back(target);
		}
	}

	for (int x = (chunks.size() - 1); x >= 0; x--) {
		if (alias->search(chunks[x])) {
			return true;
		}
	}

	return false;
}

void SFwallCore::CapturePortalEngine::process(SFwallCore::Connection *conn, SFwallCore::Packet *packet) {
	queries++;
	if (mode == 1) {
		if(conn->getIp()->type() == IPV4){
			ConnectionIpV4* conn_v4 = (ConnectionIpV4*) conn->getIp();
			if(should_be_included(conn, packet) && !should_be_excluded(conn, packet)){
				send_connection_to_portal(conn, packet);
			}
		}

	}else if(mode == 2){
		/* Public or private source address? */
		if(conn->getIp()->type() == IPV4){
			ConnectionIpV4* conn_v4 = (ConnectionIpV4*) conn->getIp();

			if(IP4Addr::isLocal(conn_v4->getSrcIp())){
				if(!should_be_excluded(conn, packet)){
					send_connection_to_portal(conn, packet);
				}
			}
		}
	}			

	conn->getApplicationInfo()->ignore_cp = true;
	return;
}

bool SFwallCore::CapturePortalEngine::should_be_excluded(Connection *conn, Packet *packet){
	for(AliasPtr alias : optimised_exclusions){
		/* Determine type and run with it */
		if(conn->getIp()->type() == IPV4){
			ConnectionIpV4* conn_v4 = (ConnectionIpV4*) conn->getIp();
			if(alias->type() == IP_RANGE || alias->type() == IP_SUBNET){
				if(alias->searchForNetworkMatch(conn_v4->getDstIp()) || alias->searchForNetworkMatch(conn_v4->getSrcIp())){
					return true;
				}
			}

			if(alias->type() == WEBSITE_LIST){
				if(isWebsiteInAliases(alias, conn->getApplicationInfo()->http_hostName)){
					return true;
				}
			}

			if(alias->type() == MAC_ADDRESS_LIST){
				if(alias->search(conn->getHwAddress())){
					return true;
				}
			}
			/* exclude the Apple Captive Portal detection connection */
			if (disable_apple_captive_prompt) {
				static boost::regex exp("CaptiveNetworkSupport");
				boost::smatch what;
				if (conn->getApplicationInfo()->http_useragentSet && boost::regex_search(conn->getApplicationInfo()->http_useragent, what, exp)) {
					return true;
				}
			}
		}
	}

	return false;
}

bool SFwallCore::CapturePortalEngine::should_be_included(Connection *conn, Packet *packet){
	for(AliasPtr alias : optimised_inclusions){
		/* Determine type and run with it */
		if(conn->getIp()->type() == IPV4){
			ConnectionIpV4* conn_v4 = (ConnectionIpV4*) conn->getIp();
			if(alias->type() == IP_RANGE || alias->type() == IP_SUBNET){
				if(alias->searchForNetworkMatch(conn_v4->getSrcIp())){
					return true;
				}
			}

			if(alias->type() == MAC_ADDRESS_LIST){
				if(alias->search(conn->getHwAddress())){
					return true;
				}
			}
		}
	}

	return false;
}


void SFwallCore::CapturePortalEngine::send_connection_to_portal(Connection *conn, Packet *packet){
	if (!(CAPTURE_PORTAL_MAGIC_LINK_ENABLED && (CAPTURE_PORTAL_MAGIC_LINK_CLICKED_TIME + CAPTURE_PORTAL_MAGIC_LINK_TIMEOUT) > System::getInstance()->getTimeManager().time())) {
		if (conn->getApplicationInfo()->tls || conn->getApplicationInfo()->sslv3 ||
											   conn->getApplicationInfo()->type != SFwallCore::ConnectionApplicationInfo::Classifier::HTTP) {
			conn->terminate();
		}
		else {
			if (CAPTURE_PORTAL_MAGIC_LINK_ENABLED && (CAPTURE_PORTAL_MAGIC_LINK_CLICKED_TIME +
													   CAPTURE_PORTAL_MAGIC_LINK_TIMEOUT) < System::getInstance()->getTimeManager().time()) {
				if (conn->getApplicationInfo()->http_hostNameSet) {
					if (conn->getApplicationInfo()->http_hostName.compare(CAPTURE_PORTAL_MAGIC_LINK_URL) == 0) {
						CAPTURE_PORTAL_MAGIC_LINK_CLICKED_TIME = System::getInstance()->getTimeManager().time();
					}
					else {
						do_send_connection_to_portal(conn, packet);
					}
				}
				else {
					do_send_connection_to_portal(conn, packet);
				}
			}
			else {
				do_send_connection_to_portal(conn, packet);
			}
		}
	}
}

void SFwallCore::CapturePortalEngine::do_send_connection_to_portal(Connection *conn, Packet *packet){
	if(endpoint == 1){
		conn->setRewriteRule(new HttpRedirectRewrite(CAPTURE_PORTAL_LINEWIZE_URL));
	}else if(endpoint == 2){
		//Resolve relavant ip address
		stringstream ss;
		ss << "http://";
		InterfacePtr interface = conn->getSourceNetDevice();
		if(interface){
			ss << IP4Addr::ip4AddrToString(interface->get_primary_ipv4_address());
		}

		ss << ":5002/login";
		conn->setRewriteRule(new HttpRedirectRewrite(ss.str()));

	}else{
		conn->setRewriteRule(new HttpRedirectRewrite(cp_url));
	}
}

void SFwallCore::CapturePortalEngine::add_provided_inclusions(std::string input){
	if(SFwallCore::Range::valid(input)){
		iprange_inclusions_alias->addEntry(input);
	}else if(IP4Addr::isHw(input)){
		macaddress_inclusions_alias->addEntry(input);
	}
}

void SFwallCore::CapturePortalEngine::add_provided_exclusions(std::string item){
	if(SFwallCore::Range::valid(item)){
		iprange_exclusions_alias->addEntry(item);
	}else if(IP4Addr::isHw(item)){
		macaddress_exclusions_alias->addEntry(item);
	}else{
		website_exclusions_alias->addEntry(item);
	}
}

std::list<string> SFwallCore::CapturePortalEngine::get_all_provided_inclusions(){
	list<string> ret;
	ret.merge(iprange_inclusions_alias->listEntries());
	ret.merge(macaddress_inclusions_alias->listEntries());
	return ret;
}

std::list<string> SFwallCore::CapturePortalEngine::get_all_provided_exclusions(){
	list<string> ret;
	ret.merge(iprange_exclusions_alias->listEntries());
	ret.merge(macaddress_exclusions_alias->listEntries());
	ret.merge(website_exclusions_alias->listEntries());
	return ret;
}

void SFwallCore::CapturePortalEngine::clear_provided_inclusions(){
	iprange_inclusions_alias->clear();
	macaddress_inclusions_alias->clear();
}

void SFwallCore::CapturePortalEngine::clear_provided_exclusions(){
	iprange_exclusions_alias->clear();
	macaddress_exclusions_alias->clear();
	website_exclusions_alias->clear();
}

void SFwallCore::CapturePortalEngine::initopts(){
	optimised_inclusions.clear();
	optimised_exclusions.clear();

	for(AliasPtr target : provided_inclusions_aliases){
		optimised_inclusions.push_back(target);
	}

	for(AliasPtr target : provided_exclusions_aliases){
		optimised_exclusions.push_back(target);
	}

	optimised_inclusions.push_back(iprange_inclusions_alias);
	optimised_inclusions.push_back(macaddress_inclusions_alias);

	optimised_exclusions.push_back(website_exclusions_alias);	
	optimised_exclusions.push_back(iprange_exclusions_alias);	
	optimised_exclusions.push_back(macaddress_exclusions_alias);	
}

bool SFwallCore::CapturePortalEngine::matchesIpCriteria(Connection *conn, Packet *packet) {
	if (conn->getApplicationInfo()->ignore_cp) {
		return false;
	}

	/* Check that its not local traffic from the box*/
	if(conn->getSourceDev() == 0 || conn->getDestDev() == 0){
		return false;
	}

	/* Check that its local traffic */
	if(conn->getIp()->type() == IPV4){
		ConnectionIpV4* conn_v4 = (ConnectionIpV4*) conn->getIp();
		if(IP4Addr::isLocal(conn_v4->getDstIp()) && IP4Addr::isLocal(conn_v4->getSrcIp()) && allow_local){
			return false;
		}
	}

	if(!capture_all_traffic){
		if (conn->getProtocol() != TCP) {
			return false;
		}

		if (((TcpConnection *) conn)->getState()->state != TCPUP) {
			return false;
		}

		if (conn->getApplicationInfo()->type != SFwallCore::ConnectionApplicationInfo::Classifier::HTTP) {
			return false;
		}
	}else{
		if (conn->getProtocol() == TCP) {
			if (((TcpConnection *) conn)->getState()->state != TCPUP) {
				return false;
			}

			/* TODO/Note:
			   If the user is going to be redirected to an external captive portal, in the public world, we need to allow that 
			   request out. Unfortunately, we cannot properly evaluate the target until the TCP handshake has been done, and 
			   the http headers have been properly evaluated :(.

			   One option here, could be to have an expected protocol --> until the protocol handler has completed evaluation.
			*/
			if((conn->getDestinationPort() == 80 || conn->getDestinationPort() == 443) 
				&& conn->getApplicationInfo()->type != SFwallCore::ConnectionApplicationInfo::Classifier::HTTP){

				return false;
			}
		}
	}	

	if (conn->getHostDiscoveryServiceEntry() && conn->getHostDiscoveryServiceEntry()->authenticated_user) {
		return false;
	}

	return true;
}

bool SFwallCore::CapturePortalEngine::enabled() {
	return mode > 0 && (CAPTURE_PORTAL_REQUIRES_CLOUD_CONNECTION == 0 || System::getInstance()->getCloudConnection()->connected());
}

