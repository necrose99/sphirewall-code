/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

using namespace std;

#include <SFwallCore/ApplicationLevel/Handlers/HttpHandler.h>
#include <SFwallCore/ApplicationLevel/Handlers/HttpProxyHandler.h>
#include <SFwallCore/ApplicationLevel/Handlers/DnsHandler.h>
#include "SFwallCore/Connection.h"
#include "SFwallCore/Firewall.h"
#include "SFwallCore/Packet.h"
#include "Core/System.h"
#include "SFwallCore/ApplicationLevel/Handlers.h"
#include "Core/Logger.h"
#include <mutex>
#include <assert.h>

ApplicationLayerTrackerHandler::~ApplicationLayerTrackerHandler() {}

std::mutex mtx;
void SFwallCore::ApplicationLayerTracker::update(SFwallCore::Connection *connection, SFwallCore::Packet *incomming) {
	incomming->setConnection(connection);
	if (!connection->getApplicationInfo()->afVerdictApplied && connection->getDestinationPort() != -1) {
		std::lock_guard<std::mutex> lck(mtx);
		if(connection->getDestinationPort() < MAX_PORTS){
			ApplicationLayerTrackerHandler *handler = handlers[connection->getDestinationPort()];

			if (handler && handler->enabled() && handler->supportsProto(connection->getProtocol())) {
				handler->handle(connection->checkPacket(incomming, DIR_SAME) ? DIR_SAME : DIR_OPPOSITE, connection, incomming->getTransport()->getApplication());
			}
		}
	}

	if(System::getInstance()->getFirewall()->ENABLE_APPLICATION_LAYER_TAGGING == 1){
		/* Now that we have evaluated the handlers, lets see if we can tag this connection*/
		if(connection->getApplicationInfo()->exclusive_signature && !connection->getApplicationInfo()->exclusive_signature->is_fallback_signature()){
			return;
		}

		if(!connection->getApplicationInfo()->tagging_http_evaluated && connection->getApplicationInfo()->type == ConnectionApplicationInfo::Classifier::HTTP){
			std::lock_guard<std::mutex> lck(mtx);
			SignaturePtr cache_hit = http_signatures_cache_get(connection->getApplicationInfo()->http_hostName);
			if(cache_hit){
				http_signature_cache_hits++;
				http_signature_hits++;

				connection->getApplicationInfo()->exclusive_signature = cache_hit;
				connection->getApplicationInfo()->tagging_http_evaluated = true;

				if(Logger::instance()->criticalPathEnabled){
					Logger::instance()->log("sphirewalld.firewall.applicationlayer", INFO, "Cache hit categorized packet '%s' as '%s'", incomming->toString().c_str(), cache_hit->id().c_str());                                      
				}

				return;
			}	

			for(SignaturePtr signature : http_signatures){
				http_signature_cache_misses++;
				if(signature->matches(connection, incomming)){
					http_signature_hits++;
					connection->getApplicationInfo()->exclusive_signature = signature;
					connection->getApplicationInfo()->tagging_http_evaluated = true;

					if(Logger::instance()->criticalPathEnabled){
						Logger::instance()->log("sphirewalld.firewall.applicationlayer", INFO, "Categorized packet '%s' as '%s'", incomming->toString().c_str(), signature->id().c_str());					
					}

					http_signatures_cache_put(connection->getApplicationInfo()->http_hostName, signature);
					if(!signature->is_fallback_signature()){
						return;		
					}
				}
			}

			http_signature_misses++;
			connection->getApplicationInfo()->tagging_http_evaluated = true;
		}

		if(!connection->getApplicationInfo()->tagging_ip_evaluated && connection->checkPacket(incomming, SFwallCore::DIR_SAME)){
			std::lock_guard<std::mutex> lck(mtx);
			for(SignaturePtr signature : ip_signatures){
				if(signature->matches(connection, incomming)){
					ip_signature_hits++;
					connection->getApplicationInfo()->exclusive_signature = signature;
					connection->getApplicationInfo()->tagging_ip_evaluated= true;
					connection->getApplicationInfo()->afProcessedRequest= false;
					connection->getApplicationInfo()->afProcessedResponse= false;

					if(Logger::instance()->criticalPathEnabled){
						Logger::instance()->log("sphirewalld.firewall.applicationlayer", INFO, "Categorized packet '%s' as '%s'", incomming->toString().c_str(), signature->id().c_str());
					}

					if(!signature->is_fallback_signature()){
						return;		
					}
				}
			}
			connection->getApplicationInfo()->tagging_ip_evaluated= true;
			non_ip_signature_hits++;
		}

		/* Work out what we can evaluate */
		if(!connection->getApplicationInfo()->tagging_non_http_evaluated && incomming->getPayloadLen() > 0){
			std::lock_guard<std::mutex> lck(mtx);
			for(SignaturePtr signature : non_http_signatures){
				if(signature->matches(connection, incomming)){
					non_http_signature_hits++;
					connection->getApplicationInfo()->exclusive_signature = signature;
					connection->getApplicationInfo()->tagging_non_http_evaluated = true;
					connection->getApplicationInfo()->afProcessedRequest= false;   
					connection->getApplicationInfo()->afProcessedResponse= false;  


					if(Logger::instance()->criticalPathEnabled){
						Logger::instance()->log("sphirewalld.firewall.applicationlayer", INFO, "Categorized packet '%s' as '%s'", incomming->toString().c_str(), signature->id().c_str());
					}

                                        if(!signature->is_fallback_signature()){
                                                return;
                                        }
				}
			}
			non_http_signature_misses++;
		}
	}
}

void SFwallCore::ApplicationLayerTracker::reset_counters(){
	http_signature_cache_hits = 0;
	http_signature_cache_misses = 0;
	http_signature_hits = 0;
	http_signature_misses = 0;
	non_http_signature_hits = 0;
	non_http_signature_misses = 0;
	ip_signature_hits = 0;
	non_ip_signature_hits = 0;
}

void SFwallCore::ApplicationLayerTracker::signature_store_changed(){
	std::lock_guard<std::mutex> lck(mtx);

	http_signatures.clear();
	non_http_signatures.clear();
	ip_signatures.clear();
	http_signatures_cache.clear();

	for(SignaturePtr signature : signature_store->available()){
		if(signature->requires_http()){
			http_signatures.push_back(signature);
		}else if(signature->requires_data()){
			non_http_signatures.push_back(signature);
		}else{
			ip_signatures.push_back(signature);
		}
	}

	reset_counters();
}

SFwallCore::ApplicationLayerTracker::ApplicationLayerTracker(ConfigurationManager *cm, SignatureStore* signature_store) : cm(cm), signature_store(signature_store) {
	if(System::getInstance()->getFirewall()->APPLICATION_LAYER_PROTOCOL_HANDLERS_ENABLED == 1){
		for (int i = 0; i < MAX_PORTS; i++) {
			handlers[i] = NULL;
		}

		addHandler(53, new DnsHandler());
		addHandler(80, new HttpHandler());
		addHandler(443, new HttpsHandler());
		addHandler(8080, new HttpProxyHandler());
		addHandler(3128, new HttpProxyHandler());
		addHandler(1080, new HttpProxyHandler());
	}

	signature_store->register_change_listener(this);
	reset_counters();
}

void ApplicationLayerTracker::addHandler(int port, ApplicationLayerTrackerHandler *handler) {
	std::lock_guard<std::mutex> lck(mtx);
	if (handlers[port] != NULL) {
		Logger::instance()->log("sphirewalld.firewall.applicationlayer", INFO, "Replacing an old application layer tracker handler. [port %d]", port);
		delete handlers[port];
	}
	else {
		Logger::instance()->log("sphirewalld.firewall.applicationlayer", INFO, "Adding an application layer tracker handler. [port %d] enabled: %d", port, handler->enabled());
	}
	handlers[port] = handler;
}

void ApplicationLayerTracker::removeHandler(int port) {
	if(port < MAX_PORTS){
		std::lock_guard<std::mutex> lck(mtx);
		if (handlers[port]) {
			Logger::instance()->log("sphirewalld.firewall.applicationlayer", INFO, "Removing an application layer tracker handler. [port %d]", port);
			delete handlers[port];
		}
		handlers[port] = NULL;
	}else{
		Logger::instance()->log("sphirewalld.firewall.applicationlayer", ERROR, " a handler with a insane port number was operated on");
	}
}
