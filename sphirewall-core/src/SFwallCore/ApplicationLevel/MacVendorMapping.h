#ifndef MAC_VENDOR_H
#define MAC_VENDOR_H

#include <string>
#include <boost/unordered_map.hpp>
#include "Core/Cron.h"

class MacVendorMapping {
	public:
		bool is_enabled();
		std::string get_database_url();
		bool import_mapping();
		void load_mapping();
		MacVendorMapping();
	private:
		boost::unordered_map<std::string, std::string> mac_vendor_table;
};

class MacVendorMappingCron : public CronJob {
	public:
		MacVendorMappingCron(MacVendorMapping* mac_vendor_mapping) :
				mac_vendor_mapping(mac_vendor_mapping),
				CronJob(60*60*24, "UPDATE_MAC_VENDOR_MAPPING_CRONJOB", true) {
			this->mac_vendor_mapping = mac_vendor_mapping;
		}
		void run() {
			mac_vendor_mapping->load_mapping();
		};
	private:
		MacVendorMapping* mac_vendor_mapping;
};

#endif