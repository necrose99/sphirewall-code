#include <iostream>
#include <boost/regex.hpp>

#include "SFwallCore/ApplicationLevel/Fingerprinting.h"
#include "SFwallCore/Connection.h"
#include "Core/System.h"
#include "SFwallCore/Packet.h"
#include "SFwallCore/Criteria.h"
#include "Utils/HttpRequestWrapper.h"

using namespace std;
using namespace SFwallCore;

FingerprintStore::FingerprintStore(){
}

std::string FingerprintStore::get_database_url(){
	if (System::getInstance()->getConfigurationManager()->hasByPath("general:fingerprint_url")) {
		return System::getInstance()->getConfigurationManager()->get("general:fingerprint_url")->string();
	}

	return "http://appindex.sphirewall.net/get/fingerprints/v1";
}

void FingerprintStore::__load_fingerprints(ObjectContainer* object){
	holdLock();
	available_fingerprints.clear();
	releaseLock();

	for (int x = 0; x < object->size(); x++) {
		ObjectContainer *source = object->get(x)->container();
		Fingerprint* entry = NULL;

		std::string id = source->get("id")->string();
		FingerprintPtr existing = get(id);
		if(!existing){
			entry = new Fingerprint();
			add(FingerprintPtr(entry));
		}

		entry->uniqueId= source->get("id")->string();
		entry->userDefinedName = source->get("name")->string();
		entry->userDefinedDescription= source->get("description")->string();

		ObjectContainer* criteria_sets = source->get("criteria")->container();
		entry->criteria.clear();
		for(int y = 0; y < criteria_sets->size(); y++){
			CriteriaSet* cset = new CriteriaSet();
			ObjectContainer* criteria = criteria_sets->get(y)->container();
			for(int z = 0; z < criteria->size(); z++){
				Criteria* criteria_parsed = CriteriaBuilder::parse(criteria->get(z)->container());
				if(criteria_parsed){
					cset->criteria.push_back(criteria_parsed);
				}
			}

			entry->criteria.push_back(CriteriaSetPtr(cset));
		}

		entry->refresh();
	}
}

bool FingerprintStore::import(){
	Logger::instance()->log("sphirewalld.firewall.application", INFO, "Loading application layer fingerprints from '%s'", get_database_url().c_str());

	HttpRequestWrapper http(get_database_url(), GET);
	std::string content;
	try{
		content = http.execute();
		ObjectWrapper* wrapper = JsonSerialiser::parseJsonString(content);
		if(wrapper){
			ObjectContainer* outer_root = wrapper->container();
			if(outer_root->has("fingerprints")){
				__load_fingerprints(outer_root->get("fingerprints")->container());
			}
			delete wrapper;
		}

		Logger::instance()->log("sphirewalld.firewall.application", INFO, 
				"Finished loading application layer fingerprints from '%s', loaded '%d' fingerprints", get_database_url().c_str(), available_fingerprints.size());

	}catch(HttpRequestWrapperException& exception){
		Logger::instance()->log("sphirewalld.firewall.application", ERROR, 
				"Could not load application layer fingerprint from '%s', curl reported '%s'", get_database_url().c_str(), exception.what());

		return false;
	}catch(ConfigurationException& exception){
		Logger::instance()->log("sphirewalld.firewall.application", ERROR, 
				"Could not load application layer fingerprint from '%s', there was a parsing error '%s'", get_database_url().c_str(), exception.what());
		return false;
	}

	return true;
}

FingerprintPtr FingerprintStore::get(string id){
	/* Threadsafe as we copy the list first */
	for(FingerprintPtr signature : available()){
		if(signature->id().compare(id) == 0){
			return signature;
		}
	}

	return FingerprintPtr();
}

std::list<FingerprintPtr> FingerprintStore::available(){
	std::list<FingerprintPtr> ret;
	holdLock();
	ret = available_fingerprints;
	releaseLock();
	return ret;
}

bool Fingerprint::matches(Connection* connection, Packet* packet){
	for(CriteriaSetPtr cset : criteria){
		bool matches = true;
		for(Criteria* criteria : cset->criteria){
			if(!criteria->match(packet, DIRECTION_NA)){
				matches = false;
				break;	
			}
		}

		if(matches){
			return true;
		}
	}

	return false;
}

void FingerprintStore::add(FingerprintPtr signature){
	holdLock();
	available_fingerprints.push_back(signature);		
	releaseLock();
}

void FingerprintStore::remove(FingerprintPtr signature){
	holdLock();
	available_fingerprints.remove(signature);
	releaseLock();
}

FingerprintStoreUpdateCronjob::FingerprintStoreUpdateCronjob(FingerprintStore* store) :
	CronJob(60 * 5, "APPLICATION_FINGERPRINT_CHECK_CRONJOB", true),
	store(store) {
		last_import = 0;
		System::getInstance()->config.getRuntime()->loadOrPut("APPLICATION_FINGERPRINT_CHECK_CRONJOB", &APPLICATION_FINGERPRINT_CHECK_CRONJOB);
	}

int FingerprintStoreUpdateCronjob::APPLICATION_FINGERPRINT_CHECK_CRONJOB= 60 * 60 * 24;

void FingerprintStoreUpdateCronjob::run(){
	/* Only works if there are less than 10 local signatures */
	if(store->available().size() < 10 || time(NULL) > (last_import + APPLICATION_FINGERPRINT_CHECK_CRONJOB)){
		store->import();
		last_import = time(NULL);
	}
}

void Fingerprint::refresh(){
	for(CriteriaSetPtr cset : criteria){
		for(Criteria* criteria : cset->criteria){
			criteria->refresh();
		}
	}
}
