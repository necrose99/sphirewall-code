/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef APP_LEVEL_HANDLERS
#define APP_LEVEL_HANDLERS

#include <vector>
#include <iosfwd>
#include "SFwallCore/Packetfwd.h"
#include "Core/ConfigurationManager.h"
#include "Core/SysMonitor.h"
#include "SFwallCore/ApplicationLevel/Signatures.h"
#include <mutex>

#define HTTP_HOST_LEN 5
#define HTTP_CR '\r'
#define HTTP_SPACE ' '
#define HTTP_HEADER_SEP ':'
#define MAX_PORTS 65535

namespace SFwallCore {
	class ApplicationLayerTrackerHandler {
		public:
			virtual ~ApplicationLayerTrackerHandler();
			virtual void handle(SFwallCore::PacketDirection direction, Connection *connection, ApplicationFrame *frame) = 0;
			virtual bool supportsProto(int protocol_number) = 0;
			virtual bool tls(Connection *connection, ApplicationFrame *frame) = 0;

			virtual bool enabled(){
				return true;
			}
	};

	class ApplicationLayerTracker : public virtual SignatureStoreChangeHandler, public virtual MetricSampler {
		public:
			ApplicationLayerTracker(ConfigurationManager *cm, SignatureStore* signature_store); 
			void initializeHandlers();
			void addHandler(int port, ApplicationLayerTrackerHandler*);
			void removeHandler(int port);
			void update(Connection *connection, Packet *incomming);

			void sample(std::map<std::string, double> &input){
				input["firewall.applicationtracker.http_signature_cache_hits"] = http_signature_cache_hits;
				input["firewall.applicationtracker.http_signature_cache_misses"] = http_signature_cache_misses;
				input["firewall.applicationtracker.http_signature_hits"] = http_signature_hits;
				input["firewall.applicationtracker.http_signature_misses"] = http_signature_misses;
				input["firewall.applicationtracker.non_http_signature_hits"] = non_http_signature_hits;
				input["firewall.applicationtracker.non_http_signature_misses"] = non_http_signature_misses;
				input["firewall.applicationtracker.ip_signature_hits"] = ip_signature_hits;
				input["firewall.applicationtracker.non_ip_signature_hits"] = non_ip_signature_hits;
			}

			void signature_store_changed();
		private:
			ApplicationLayerTrackerHandler * handlers[MAX_PORTS];
			ConfigurationManager *cm;

                        SignatureStore* signature_store;
                        std::list<SignaturePtr> http_signatures;
                        std::list<SignaturePtr> non_http_signatures;
                        std::list<SignaturePtr> ip_signatures;


			SignaturePtr http_signatures_cache_get(const std::string& website){
				if(website.size() == 0){
					return SignaturePtr();
				}

				return http_signatures_cache[website];
			}

			void http_signatures_cache_put(const std::string website, SignaturePtr signature){
				if(website.size() == 0){
					return;
				}
				http_signatures_cache[website] = signature;
			}
		
			std::unordered_map<std::string, SignaturePtr> http_signatures_cache;

			void reset_counters();
			double http_signature_cache_hits;
			double http_signature_cache_misses;
			double http_signature_hits;
			double http_signature_misses;
			double non_http_signature_hits;
			double non_http_signature_misses;
			double ip_signature_hits;
			double non_ip_signature_hits;
	};
}

#endif
