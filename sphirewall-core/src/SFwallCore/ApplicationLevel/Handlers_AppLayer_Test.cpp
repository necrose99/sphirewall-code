#include <iostream>
#include <gtest/gtest.h>

#include "SFwallCore/TestFactory.h"
#include "test.h"

#include "Utils/IP4Addr.h"
#include "SFwallCore/Packet.h"
#include "SFwallCore/Alias.h"
#include "SFwallCore/Connection.h"
#include "SFwallCore/ApplicationLevel/ApplicationFilter.h"
#include "SFwallCore/ApplicationLevel/Handlers.h"
#include "Core/Event.h"
#include "Core/System.h"
#include "SFwallCore/ApplicationLevel/Signatures.h"

using namespace SFwallCore;
using namespace std;


TEST(ApplicationLayerTracker, tagging_no_match){
	SFwallCore::SignatureStore* signature_store = new SFwallCore::SignatureStore();
	SFwallCore::ApplicationLayerTracker* tracker = new SFwallCore::ApplicationLayerTracker(NULL, signature_store);	

        SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("10.1.1.1", "2.1.33.1", 12332, 80, "--");
        SFwallCore::TcpConnection *connection = new SFwallCore::TcpConnection(packet);
        connection->getApplicationInfo()->setHttpHost("testing.com");
        packet->setConnection(connection);
        connection->increment(packet);

	tracker->signature_store_changed();
	tracker->update(connection, packet);	
			
	EXPECT_TRUE(!connection->getApplicationInfo()->exclusive_signature);
}

TEST(ApplicationLayerTracker, tagging_http_match){
	SFwallCore::SignatureStore* signature_store = new SFwallCore::SignatureStore();
	SFwallCore::ApplicationLayerTracker* tracker = new SFwallCore::ApplicationLayerTracker(NULL, signature_store);	

        SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("10.1.1.1", "2.1.33.1", 12332, 80, "--");
        SFwallCore::TcpConnection *connection = new SFwallCore::TcpConnection(packet);
        connection->getApplicationInfo()->setHttpHost("testing.com");
        packet->setConnection(connection);
        connection->increment(packet);

	SFwallCore::DynamicSignature* signature = new DynamicSignature();
	signature->uniqueId = "sig1";
	signature->userDefinedName = "sig1";
	signature->userDefinedCategory = "category";

        HttpHostname* hc = new HttpHostname(); 
	hc->aliases.push_back(AliasPtr(new SFwallCore::WebsiteListAlias( {"testing.com"})));

	CriteriaSet* criteria_set = new CriteriaSet();
	criteria_set->criteria.push_back(hc);
	signature->criteria.push_back(CriteriaSetPtr(criteria_set));

	signature_store->add(SignaturePtr(signature));
	tracker->signature_store_changed();
	tracker->update(connection, packet);	
		
	EXPECT_TRUE(connection->getApplicationInfo()->exclusive_signature.get() != NULL);

	connection->getApplicationInfo()->exclusive_signature = SignaturePtr();
	connection->getApplicationInfo()->setHttpHost("testing.com");
	tracker->update(connection, packet);
	
	EXPECT_TRUE(connection->getApplicationInfo()->exclusive_signature.get() == NULL);
}

TEST(ApplicationLayerTracker, tagging_ip_match){
        SFwallCore::SignatureStore* signature_store = new SFwallCore::SignatureStore();
        SFwallCore::ApplicationLayerTracker* tracker = new SFwallCore::ApplicationLayerTracker(NULL, signature_store);

        SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("10.1.1.1", "2.1.33.1", 12332, 80, "--");
        SFwallCore::TcpConnection *connection = new SFwallCore::TcpConnection(packet);
        packet->setConnection(connection);
        connection->increment(packet);

        SFwallCore::DynamicSignature* signature = new DynamicSignature();
        signature->uniqueId = "sig1";
        signature->userDefinedName = "sig1";
        signature->userDefinedCategory = "category";

	DestinationIpCriteria* dc = new DestinationIpCriteria();
	dc->ip = IP4Addr::stringToIP4Addr("2.1.33.1");
	dc->mask = IP4Addr::stringToIP4Addr("255.255.255.255");

        CriteriaSet* criteria_set = new CriteriaSet();
        criteria_set->criteria.push_back(dc);
        signature->criteria.push_back(CriteriaSetPtr(criteria_set));

        signature_store->add(SignaturePtr(signature));
        tracker->signature_store_changed();
        tracker->update(connection, packet);

        EXPECT_TRUE(connection->getApplicationInfo()->exclusive_signature.get() != NULL);
}

TEST(ApplicationLayerTracker, tagging_ip_mismatch){
        SFwallCore::SignatureStore* signature_store = new SFwallCore::SignatureStore();
        SFwallCore::ApplicationLayerTracker* tracker = new SFwallCore::ApplicationLayerTracker(NULL, signature_store);

        SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("10.1.1.1", "2.1.33.1", 12332, 80, "--");
        SFwallCore::TcpConnection *connection = new SFwallCore::TcpConnection(packet);
        packet->setConnection(connection);
        connection->increment(packet);

        SFwallCore::DynamicSignature* signature = new DynamicSignature();
        signature->uniqueId = "sig1";
        signature->userDefinedName = "sig1";
        signature->userDefinedCategory = "category";

	DestinationIpCriteria* dc = new DestinationIpCriteria();
	dc->ip = IP4Addr::stringToIP4Addr("2.1.33.10");
	dc->mask = IP4Addr::stringToIP4Addr("255.255.255.255");

        CriteriaSet* criteria_set = new CriteriaSet();
        criteria_set->criteria.push_back(dc);
        signature->criteria.push_back(CriteriaSetPtr(criteria_set));

        signature_store->add(SignaturePtr(signature));
        tracker->signature_store_changed();
        tracker->update(connection, packet);

        EXPECT_TRUE(connection->getApplicationInfo()->exclusive_signature.get() == NULL);
}

TEST(ApplicationLayerTracker, tagging_application_match){
        SFwallCore::SignatureStore* signature_store = new SFwallCore::SignatureStore();
        SFwallCore::ApplicationLayerTracker* tracker = new SFwallCore::ApplicationLayerTracker(NULL, signature_store);

        SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("10.1.1.1", "2.1.33.1", 12332, 80, "...DUDE...");
        SFwallCore::TcpConnection *connection = new SFwallCore::TcpConnection(packet);
        packet->setConnection(connection);
        connection->increment(packet);

        DynamicSignature* signature = new DynamicSignature();
        signature->uniqueId = "sig1";
        signature->userDefinedName = "sig1";
        signature->userDefinedCategory = "category";

	RegexCriteria* rc = new RegexCriteria();
	rc->expression = "DUDE";
	rc->refresh();

        CriteriaSet* criteria_set = new CriteriaSet();
        criteria_set->criteria.push_back(rc);
        signature->criteria.push_back(CriteriaSetPtr(criteria_set));

        signature_store->add(SignaturePtr(signature));
        tracker->signature_store_changed();
        tracker->update(connection, packet);

        EXPECT_TRUE(connection->getApplicationInfo()->exclusive_signature.get() != NULL);
}

TEST(ApplicationLayerTracker, tagging_application_mismatch){
        SFwallCore::SignatureStore* signature_store = new SFwallCore::SignatureStore();
        SFwallCore::ApplicationLayerTracker* tracker = new SFwallCore::ApplicationLayerTracker(NULL, signature_store);

        SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("10.1.1.1", "2.1.33.1", 12332, 80, "...DUDE...");
        SFwallCore::TcpConnection *connection = new SFwallCore::TcpConnection(packet);
        packet->setConnection(connection);
        connection->increment(packet);

        DynamicSignature* signature = new DynamicSignature();
        signature->uniqueId = "sig1";
        signature->userDefinedName = "sig1";
        signature->userDefinedCategory = "category";

	RegexCriteria* rc = new RegexCriteria();
	rc->expression = "FUCK";
	rc->refresh();

        CriteriaSet* criteria_set = new CriteriaSet();
        criteria_set->criteria.push_back(rc);
        signature->criteria.push_back(CriteriaSetPtr(criteria_set));

        signature_store->add(SignaturePtr(signature));
        tracker->signature_store_changed();
        tracker->update(connection, packet);

        EXPECT_TRUE(connection->getApplicationInfo()->exclusive_signature.get() == NULL);
}

