#ifndef MYLINEWIZE_REDIRECT_HELER_H 
#define MYLINEWIZE_REDIRECT_HELER_H 

#include "SFwallCore/ApplicationLevel/FilterHandler.h"

namespace SFwallCore {
        class Packet;
        class Connection;
        class ConnectionIp;

        class MyLinewizeSignedRedirect: public RewriteRule {
                public:
                        MyLinewizeSignedRedirect(std::string private_key);
                        virtual ~MyLinewizeSignedRedirect(){
			}

                        virtual void rewrite(Connection *conn, Packet *packet);
                        PacketDirection direction(){
				return DIR_OPPOSITE;
			}

		private:
			std::string private_key;
        };

        class MyLinewizeRedirectHelper : public ApplicationLayerFilterHandler, public Configurable {
                public:
                        MyLinewizeRedirectHelper();

                        virtual void process(Connection *conn, Packet *packet);
                        bool matchesIpCriteria(Connection *conn, Packet *packet);
                        bool enabled() {
				return MYLINEWIZE_REDIRECT_HELPER_ENABLED == 1;
			}

                        static int MYLINEWIZE_REDIRECT_HELPER_ENABLED;

                        void save();
                        bool load();
                        const char* getConfigurationSystemName() {
                                return "MyLinewizeRedirectHelper";
                        }

			std::string get_private_key();
        };
};

#endif
