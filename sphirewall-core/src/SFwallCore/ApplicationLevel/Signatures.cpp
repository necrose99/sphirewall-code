#include <iostream>
#include <boost/regex.hpp>

#include "SFwallCore/ApplicationLevel/Signatures.h"
#include "SFwallCore/Connection.h"
#include "Core/System.h"
#include "SFwallCore/Packet.h"
#include "SFwallCore/Criteria.h"
#include "Utils/HttpRequestWrapper.h"

using namespace std;
using namespace SFwallCore;

SignatureStore::SignatureStore(){
}

bool SignatureStore::is_enabled(){
        if (System::getInstance()->getConfigurationManager()->hasByPath("general:signature_enabled")) {
                return System::getInstance()->getConfigurationManager()->get("general:signature_enabled")->boolean();
        }

	return true;
}

std::string SignatureStore::get_database_url(){
	if (System::getInstance()->getConfigurationManager()->hasByPath("general:signature_url")) {
		return System::getInstance()->getConfigurationManager()->get("general:signature_url")->string();
	}

	return "http://appindex.sphirewall.net/get/v3";
}

void SignatureStore::__load_signatures(ObjectContainer* object){
	holdLock();
	availableSignatures.clear();
	load_categorys_and_metadata();
	releaseLock();

	for (int x = 0; x < object->size(); x++) {
		ObjectContainer *source = object->get(x)->container();
		DynamicSignature* entry = NULL;

		std::string id = source->get("id")->string();
		SignaturePtr existing_signature = get(id);
		if(existing_signature){
			entry = dynamic_cast<DynamicSignature*>(existing_signature.get());
		}else{
			entry = new DynamicSignature();
			add(SignaturePtr(entry));
		}

		entry->uniqueId= source->get("id")->string();
		entry->userDefinedName = source->get("name")->string();
		entry->userDefinedDescription= source->get("description")->string();

		if(source->has("is_category")){
			entry->is_category_flag= source->get("is_category")->boolean();
		}

                if(source->has("is_fallback_signature")){
                        entry->is_fallback_signature_flag= source->get("is_fallback_signature")->boolean();
                }

		if(!entry->is_category()){
			entry->userDefinedCategory = source->get("category")->string();
			ObjectContainer* criteria_sets = source->get("criteria")->container();
			entry->criteria.clear();
			for(int y = 0; y < criteria_sets->size(); y++){
				CriteriaSet* cset = new CriteriaSet();
				ObjectContainer* criteria = criteria_sets->get(y)->container();
				for(int z = 0; z < criteria->size(); z++){
					Criteria* criteria_parsed = CriteriaBuilder::parse(criteria->get(z)->container());
					if(criteria_parsed){
						cset->criteria.push_back(criteria_parsed);
					}
				}

				entry->criteria.push_back(CriteriaSetPtr(cset));
			}
		}

		entry->refresh();
	}

	availableSignatures.push_back(SignaturePtr(new BitTorrentSignature()));
	load_categorys_and_metadata();
}

void SignatureStore::load_categorys_and_metadata(){
	for(SignaturePtr signature : availableSignatures){
		if(!signature->is_category()){
			signature->resolved_category = get(signature->category());
		}
	}

	for(SignatureStoreChangeHandler* listener : change_listeners){
		listener->signature_store_changed();
	}
}

bool SignatureStore::import_signatures(){
	if(is_enabled()){
		Logger::instance()->log("sphirewalld.firewall.application", INFO, "Loading application layer signatures from '%s'", get_database_url().c_str());

		HttpRequestWrapper http(get_database_url(), GET);
		std::string content;
		try{
			content = http.execute();
			ObjectWrapper* wrapper = JsonSerialiser::parseJsonString(content);
			if(wrapper){
				ObjectContainer* outer_root = wrapper->container();
				if(outer_root->has("signatures")){
					__load_signatures(outer_root->get("signatures")->container());
				}
				delete wrapper;
			}

			Logger::instance()->log("sphirewalld.firewall.application", INFO, 
				"Finished loading application layer signatures from '%s', loaded '%d' signatures", get_database_url().c_str(), availableSignatures.size());

		}catch(HttpRequestWrapperException& exception){
			Logger::instance()->log("sphirewalld.firewall.application", ERROR, 
				"Could not load application layer signatures from '%s', curl reported '%s'", get_database_url().c_str(), exception.what());

			return false;
		}catch(ConfigurationException& exception){
			Logger::instance()->log("sphirewalld.firewall.application", ERROR, 
				"Could not load application layer signatures from '%s', there was a parsing error '%s'", get_database_url().c_str(), exception.what());
			return false;
		}

		return true;
	}

	return false;
}

SignaturePtr SignatureStore::get(string id){
	/* Threadsafe as we copy the list first */
	for(SignaturePtr signature : availableSignatures){
		if(signature->id().compare(id) == 0){
			return signature;
		}
	}

	return SignaturePtr();
}

std::list<SignaturePtr> SignatureStore::available(){
	return availableSignatures;
}

bool BitTorrentSignature::matches(Connection* connection, Packet* packet){
	if(packet->getTransport()->getApplication()->getSize() > 3){
		unsigned char* data = packet->getTransport()->getApplication()->getInternal();
		if(data[0] == '\x13' && data[1] == 'B' && data[2] == 'i'){
			return true;
		}		
	}		

	return false;
}

bool DynamicSignature::matches(Connection* connection, Packet* packet){
	/* If its a category, we ignore */
	if(is_category()){
		return false;
	}

	for(CriteriaSetPtr cset : criteria){
		bool matches = true;
		for(Criteria* criteria : cset->criteria){
			if(!criteria->match(packet, DIRECTION_NA)){
				matches = false;
				break;	
			}
		}

		if(matches){
			return true;
		}
	}

	return false;
}

bool DynamicSignature::requires_http() const {
	for(CriteriaSetPtr cset : criteria){
		for(Criteria* criteria : cset->criteria){
			if(criteria->lowPassApplicationFilterType() == ConnectionApplicationInfo::Classifier::HTTP){
				return true;
			}
		}
	}
	return false;	
}

bool DynamicSignature::requires_data() const {
	for(CriteriaSetPtr cset : criteria){
		for(Criteria* criteria : cset->criteria){
			if(criteria->lowPassApplicationFilterType() != ConnectionApplicationInfo::Classifier::HTTP &&
					(criteria->type() == APPLICATION || criteria->type() == APPLICATION_EXPENSIVE)){
				return true;
			}
		}
	}
	return false;
}

void SignatureStore::add(SignaturePtr signature){
	holdLock();
	availableSignatures.push_back(signature);		
	releaseLock();
}

void SignatureStore::remove(SignaturePtr signature){
	holdLock();
	availableSignatures.remove(signature);
	releaseLock();
}

SignatureStoreUpdateCronjob::SignatureStoreUpdateCronjob(SignatureStore* store) :
	CronJob(60 * 5, "APPLICATION_SIGNATURE_CHECK_CRONJOB", true),
	store(store) {
	last_import = 0;
        System::getInstance()->config.getRuntime()->loadOrPut("APPLICATION_SIGNATURE_UPDATE_PERIOD", &APPLICATION_SIGNATURE_UPDATE_PERIOD);
}

int SignatureStoreUpdateCronjob::APPLICATION_SIGNATURE_UPDATE_PERIOD = 60 * 60 * 24;

void SignatureStoreUpdateCronjob::run(){
	/* Only works if there are less than 10 local signatures */
	if(store->available().size() < 10 || time(NULL) > (last_import + APPLICATION_SIGNATURE_UPDATE_PERIOD)){
		store->import_signatures();
		last_import = time(NULL);
	}
}

