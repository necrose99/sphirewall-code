#ifndef FINGERPRINT_H
#define FINGERPRINT_H 

#include <boost/shared_ptr.hpp>
#include <boost/regex.hpp>
#include <list>
#include "Core/ConfigurationManager.h"
#include "SFwallCore/Connection.h"
#include "SFwallCore/Alias.h"

namespace SFwallCore {
	class Packet;
	class Criteria;
	class CriteriaSet;
	typedef boost::shared_ptr<CriteriaSet> CriteriaSetPtr;

	class Fingerprint {
		public:
			Fingerprint(){
			}
			~Fingerprint(){}

			std::string id(){
				return uniqueId;
			}			

			std::string name(){
				return userDefinedName;
			}

			std::string description(){
				return userDefinedDescription;
			};

			bool matches(Connection* connection, Packet* packet);

			std::string uniqueId;
			std::string userDefinedName;
			std::string userDefinedDescription;

			std::list<CriteriaSetPtr> criteria;
			void refresh();
	};

	typedef boost::shared_ptr<Fingerprint> FingerprintPtr;
	class FingerprintStore : public virtual Lockable {
		public:
			FingerprintStore();
			FingerprintPtr get(std::string key);
			std::list<FingerprintPtr> available();

			void add(FingerprintPtr signature);
			void remove(FingerprintPtr signature);

			bool import();
			std::string get_database_url();

		private:
			std::list<FingerprintPtr> available_fingerprints;
			void __load_fingerprints(ObjectContainer* object);
	};	

	class FingerprintStoreUpdateCronjob : public CronJob {
		public:

			FingerprintStoreUpdateCronjob(FingerprintStore* store);
			static int APPLICATION_FINGERPRINT_UPDATE_PERIOD;
			static int APPLICATION_FINGERPRINT_CHECK_CRONJOB;

			void run();

		private:
			FingerprintStore* store;
			int last_import;
	};


};

#endif
