/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef CAPTURE_PORTAL_H 
#define CAPTURE_PORTAL_H 

namespace SFwallCore {
        class TcpConnection;
        class Packet;
        class Connection;
        class ConnectionIp;
        class Alias;
        class AliasDb;

        class CapturePortalEngine: public ApplicationLayerFilterHandler, public Configurable {
                public:
                        CapturePortalEngine(AliasDb *aliases);

                        virtual void process(Connection *conn, Packet *packet);
                        bool matchesIpCriteria(Connection *conn, Packet *packet);
                        bool enabled();

                        static int CAPTURE_PORTAL_FORCE; /* DEPRECATED, mode replaces this runtime var: 0 = disabled, 1 = selected networks, 2 = all networks/devices */
                        static int REWRITE_PROXY_REQUESTS_ENABLED;
                        static int CAPTURE_PORTAL_REQUIRES_CLOUD_CONNECTION;
                        static std::string CAPTURE_PORTAL_LINEWIZE_URL;
                        static int CAPTURE_PORTAL_DMZ_MODE_ENABLED;
                        static int CAPTURE_PORTAL_MAGIC_LINK_ENABLED;
                        static int CAPTURE_PORTAL_MAGIC_LINK_TIMEOUT;
                        static int CAPTURE_PORTAL_MAGIC_LINK_CLICKED_TIME;
                        static std::string CAPTURE_PORTAL_MAGIC_LINK_URL;

                        void save();
                        bool load();
                        const char* getConfigurationSystemName() {
                                return "Capture portal engine";
                        }

                        long queries;
                        long hits;

                        std::list<AliasPtr> provided_inclusions_aliases;
                        std::list<AliasPtr> provided_exclusions_aliases;

			/* These are helper methods for working with the unknown inclusions/exclusions */
			void clear_provided_inclusions();
			void clear_provided_exclusions();
			std::list<string> get_all_provided_inclusions();
			std::list<string> get_all_provided_exclusions();
			void add_provided_inclusions(std::string input);
			void add_provided_exclusions(std::string input);
	
			int endpoint;
			int mode;
			std::string cp_url;
			bool allow_local;
			bool capture_all_traffic;
                bool disable_apple_captive_prompt;
			void initopts();
                private:
                        AliasDb *aliases;

                        AliasPtr iprange_inclusions_alias;
                        AliasPtr macaddress_inclusions_alias;
                        AliasPtr website_exclusions_alias;
                        AliasPtr iprange_exclusions_alias;
                        AliasPtr macaddress_exclusions_alias;

			/* Items in the above provided inclusions/exclusions will be mapped into objects here on load/reload */	
                        std::list<AliasPtr> optimised_inclusions;
                        std::list<AliasPtr> optimised_exclusions;

                        bool isWebsiteInAliases(const AliasPtr& alias, const std::string &website);
			bool isMacAddressInAlias(const std::string &mac);
			bool should_be_excluded(Connection *conn, Packet *packet);
			bool should_be_included(Connection *conn, Packet *packet);
			void send_connection_to_portal(Connection *conn, Packet *packet);
			void do_send_connection_to_portal(Connection *conn, Packet *packet);
        };
};

#endif
