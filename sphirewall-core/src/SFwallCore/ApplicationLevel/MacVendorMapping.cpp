#include "SFwallCore/ApplicationLevel/MacVendorMapping.h"
#include "Core/System.h"
#include "Core/HostDiscoveryService.h"
#include "Utils/HttpRequestWrapper.h"
#include <fstream>
#include <sstream>
#include <algorithm>
#include <boost/algorithm/string.hpp>

using namespace std;
using namespace SFwallCore;


MacVendorMapping::MacVendorMapping() {
}

bool MacVendorMapping::is_enabled(){
	if (System::getInstance()->getConfigurationManager()->hasByPath("general:mac_vendor_mapping_enabled")) {
		return System::getInstance()->getConfigurationManager()->get("general:mac_vendor_mapping_enabled")->boolean();
	}
	return true;
}

std::string MacVendorMapping::get_database_url(){
	if (System::getInstance()->getConfigurationManager()->hasByPath("general:mac_vendor_mapping_url")) {
		return System::getInstance()->getConfigurationManager()->get("general:mac_vendor_mapping_url")->string();
	}

	return "http://appindex.sphirewall.net/get/vendors/v1";
}


void MacVendorMapping::load_mapping(){
	if(is_enabled()) {
		import_mapping();
		System::getInstance()->getArp()->set_mac_vendor_table(mac_vendor_table);
	}
}


bool MacVendorMapping::import_mapping() {
	if(is_enabled()){
		Logger::instance()->log("sphirewalld.firewall.application", INFO, "Loading mac address vendor mapping from '%s'", get_database_url().c_str());
		HttpRequestWrapper http(get_database_url(), GET);
		std::string content;
		try{
			content = http.execute();
			ObjectWrapper* wrapper = JsonSerialiser::parseJsonString(content);
			if(wrapper){
				ObjectContainer* outer_root = wrapper->container()->get("vendors")->container();
				for (int y = 0; y < outer_root->size(); y++) {
					std::string mac_prefix = outer_root->get(y)->container()->get("mac_prefix")->string();
					std::string company = outer_root->get(y)->container()->get("company")->string();
					boost::algorithm::to_lower(mac_prefix);
					std::replace(mac_prefix.begin(), mac_prefix.end(), '-', ':');
					mac_vendor_table.insert({ mac_prefix, company });
				}
				delete wrapper;
			}

			Logger::instance()->log("sphirewalld.firewall.application", INFO,
									"Finished loading mac address vendor mapping from '%s', loaded '%d' mac to vendor mappings", get_database_url().c_str(), mac_vendor_table.size());

		}catch(HttpRequestWrapperException& exception){
			Logger::instance()->log("sphirewalld.firewall.application", ERROR,
									"Could not load mac address vendor mapping from '%s', curl reported '%s'", get_database_url().c_str(), exception.what());

			return false;
		}catch(ConfigurationException& exception){
			Logger::instance()->log("sphirewalld.firewall.application", ERROR,
									"Could not load mac address vendor mapping from '%s', there was a parsing error '%s'", get_database_url().c_str(), exception.what());
			return false;
		}

		return true;
	}

	return false;
}
