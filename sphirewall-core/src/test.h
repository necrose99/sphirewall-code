/* This file is part of Sphirewall, it comes with a copy of the license in LICENSE.txt, which is also available at http://sphirewall.net/LICENSE.txt */

#ifndef test_h
#define test_h

#include <iostream>

#include <gtest/gtest.h>
#include "Json/JSON.h"
#include "Api/JsonManagementService.h"
#include "Auth/User.h"
#include "Auth/Group.h"
#include "Auth/GroupDb.h"
#include "Core/ConfigurationManager.h"
#include "Core/HostDiscoveryService.h"
#include "SFwallCore/Firewall.h"
#include "SFwallCore/TrafficShaper/TrafficShaper.h"
#include "Core/Event.h"
#include "Core/System.h"

using namespace std;

class MockFactory {
	private:
		GroupDb *groupDb ;
		UserDb *userDb ;
		SFwallCore::ACLStore *store;
		SFwallCore::Firewall *firewall;
		Config *config;
		CronManager *cronManager;
		EventDb *eventDb;
	public:
		MockFactory() {
			groupDb = NULL;
			userDb = NULL;
			store = NULL;
			firewall = NULL;
			config = NULL;
			cronManager = NULL;
			eventDb = NULL;
		}

		EventDb *givenEventDb() {
			if (eventDb == NULL) {
				eventDb = new EventDb(givenConfig());
			}

			return eventDb;
		}

		GroupDb *givenGroupDb() {
			if (groupDb == NULL) {
				groupDb = new GroupDb();
			}

			return groupDb;
		}

		UserDb *givenUserDb() {
			if (userDb == NULL) {
				userDb = new UserDb(NULL, givenGroupDb());
			}

			return userDb;
		}

		Config *givenConfig() {
			if (config == NULL) {
				config = new Config();
			}

			return config;
		}

		SFwallCore::Firewall *givenFw() {
			if (firewall == NULL) {
				firewall = new SFwallCore::Firewall();
				firewall->trafficShaper = new TrafficShaper();
				firewall->trafficShaper->setConfigurationManager(new ConfigurationManager());
			}

			return firewall;
		}

		SFwallCore::ACLStore *givenRuleStore() {
			if (store == NULL) {
				store = new SFwallCore::ACLStore(NULL, NULL, NULL, NULL);
				store->setConfigurationManager(new ConfigurationManager());
				givenFw()->acls = store;
			}

			return store;
		}

		CronManager *givenCronManager() {
			if (cronManager == NULL) {
				cronManager = new CronManager(givenConfig());
			}

			return cronManager;
		}

		HostDiscoveryService *givenArp() {
			HostDiscoveryService *ret = new HostDiscoveryService(givenCronManager(), givenConfig());
			ret->setEventDb(givenEventDb());
			return ret;
		}

		void reset() {
			delete groupDb;
			delete userDb;

			groupDb = NULL;
			userDb = NULL;
		}

};

void expectException(Delegate *delegate, std::string path, JSONObject args);
#endif
