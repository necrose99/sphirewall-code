/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <boost/algorithm/string.hpp>

using namespace std;

#include "Api/Delegates/NetworkSettingsDelegate.h"

#include "Json/JSON.h"
#include "Api/Exceptions.h"
#include "Utils/StringUtils.h"
#include "Core/HostDiscoveryService.h"
#include "SFwallCore/ApplicationLevel/Fingerprinting.h"
#include "Core/ConnectionManager.h"
#include "Core/ConnectionManager.h"
#include "Core/System.h"
#include "Core/Wireless.h"
#include "Utils/DynDns.h"
#include "Core/Event.h"
#include "Utils/IP4Addr.h"
#include "Utils/Interfaces.h"
#include "Utils/NRoute.h"
#include "Utils/NetworkUtils.h"
#include "Core/TorProvider.h"
#include "Utils/DhcpServer.h"
#include "Utils/MdnsForwarder.h"
#include "Auth/GroupDb.h"
#include "Utils/PacketCapture.h"

#define installHandler(x,y) delegateMap.insert(DelegatePath(x, &NetworkSettingsDelegate::y))

NetworkSettingsDelegate::NetworkSettingsDelegate(IntMgr* interfaceManager, DNSConfig *dnsConfig, HostDiscoveryService *arpTable)
	: interfaceManager(interfaceManager), dnsConfig(dnsConfig), arp(arpTable), eventDb(NULL){

		installHandler("network/devices/list", devices_list);
		installHandler("network/devices/set", devices_set);
		installHandler("network/devices/delete", devices_delete);
		installHandler("network/devices/up", devices_up);
		installHandler("network/devices/down", devices_down);
		installHandler("network/devices/save", devices_save);

		installHandler("network/dns/get", dns_get);
		installHandler("network/dns/set", dns_set);
		installHandler("network/routes/list", routes_list);
		installHandler("network/routes/add", routes_add);
		installHandler("network/routes/del", routes_del);
		installHandler("network/arp/list", arp_list);
		installHandler("network/arp/size", arp_size);

		installHandler("network/devices/leases/add", devices_leases_add);
		installHandler("network/devices/leases/del", devices_leases_del);

		installHandler("network/connections/list", connections_list);
		installHandler("network/connections/add", connectons_add);
		installHandler("network/connections/del", connections_del);
		installHandler("network/connections/save", connections_save);
		installHandler("network/connections/connect", connections_connect);
		installHandler("network/connections/disconnect", connections_disconnect);

		installHandler("network/vpn", vpn_instances_list);
		installHandler("network/vpn/start", vpn_start);
		installHandler("network/vpn/stop", vpn_stop);
		installHandler("network/vpn/status", vpn_status);
		installHandler("network/vpn/create", vpn_instance_create);
		installHandler("network/vpn/remove", vpn_instance_remove);
		installHandler("network/vpn/options", vpn_options);

		installHandler("network/clientvpn", vpn_clientvpn);
		installHandler("network/clientvpn/options", vpn_clientvpn_options);

		installHandler("network/wireless/start", wireless_start);
		installHandler("network/wireless/stop", wireless_stop);
		installHandler("network/wireless/online", wireless_online);
		installHandler("network/dyndns", dyndns);
		installHandler("network/dyndns/set", dyndns_set);

		installHandler("network/ping", ping);
		installHandler("network/dig", dig);
		installHandler("network/traceroute", traceroute);
		installHandler("network/arp/quarantined", set_quarantined);

		installHandler("network/tor", tor);
		installHandler("network/tor/set", tor_set);
		installHandler("network/tor/status", tor_status);
		installHandler("network/tor/start", tor_start);
		installHandler("network/tor/stop", tor_stop);

		installHandler("network/mdns", mdns);
		installHandler("network/mdns/set", mdns_set);
		installHandler("network/mdns/del", mdns_del);

		installHandler("network/capture/start", capture_start);
		installHandler("network/capture/stop", capture_stop);
		installHandler("network/capture", capture);
		installHandler("network/capture/raw", capture_raw);
	}

std::string NetworkSettingsDelegate::rexpression() {
	return "network/(.*)";
}

void NetworkSettingsDelegate::setConnectionManager(ConnectionManager *connectionManager) {
	this->connectionManager = connectionManager;
}

void NetworkSettingsDelegate::setEventDb(EventDb *eventDb) {
	this->eventDb = eventDb;
}

JSONObject NetworkSettingsDelegate::process(std::string uri, JSONObject obj) {
	try {
		return invokeHandler(uri, obj);
	}
	catch (std::out_of_range e) {
		throw DelegateNotFoundException(uri);
	}
}

JSONObject NetworkSettingsDelegate::capture(JSONObject obj){
	PacketCaptureEngine* capture = System::getInstance()->get_capture_engine();
	JSONObject ret;
	ret.put(L"no_packets_captured", new JSONValue((double) capture->no_packets_captured()));	
	ret.put(L"running", new JSONValue((bool) capture->is_running()));	
	ret.put(L"filter", new JSONValue((string) capture->capture_filter));	
	ret.put(L"interface", new JSONValue((string) capture->capture_interface));	

	if(capture->capture_type == MAC){
		ret.put(L"type", new JSONValue((string) "mac"));	
		ret.put(L"target", new JSONValue((string) capture->hw));	
	}else if(capture->capture_type == USER){
		ret.put(L"type", new JSONValue((string) "user"));	

		if(capture->user_target){
			ret.put(L"target", new JSONValue((string) capture->user_target->getUserName()));	
		}
	}else if(capture->capture_type == PCAP){
		ret.put(L"type", new JSONValue((string) "pcap"));	
		ret.put(L"target", new JSONValue((string) capture->capture_filter));	
	}

	ret.put(L"raw_pcap", new JSONValue((string) capture->get_base64_encoded_pcap_string()));	

	JSONArray arr;
	for(CapturedPacketPtr packet : capture->get_captured_packets()){
		arr.push_back(new JSONValue((string) packet->to_string()));
	}

	ret.put(L"packets", new JSONValue(arr));	
	return ret;	
}

JSONObject NetworkSettingsDelegate::capture_raw(JSONObject obj){
	PacketCaptureEngine* capture = System::getInstance()->get_capture_engine();
	JSONObject ret;
	ret.put(L"raw_pcap", new JSONValue((string) capture->get_base64_encoded_pcap_string()));	
	return ret;	
}

JSONObject NetworkSettingsDelegate::capture_start(JSONObject obj){
	PacketCaptureEngine* capture = System::getInstance()->get_capture_engine();
	capture->capture_interface = obj[L"interface"]->String();

	string cType = obj[L"type"]->String();
	string captureTarget = obj[L"target"]->String();

	if (cType.compare("mac") == 0) {
		boost::regex macRegex("^([0-9A-Fa-f]{2}[:]){5}([0-9A-Fa-f]{2})$");
		if(!boost::regex_match(captureTarget, macRegex)) {
			throw DelegateGeneralException("Could not set PCAP capture: mac address invalid.");
		}
		capture->capture_type = MAC;
		capture->hw = captureTarget;
	} else if (cType.compare("user") == 0) {
		if(!System::getInstance()->getUserDb()->getUser(captureTarget)) {
			throw DelegateGeneralException("Could not set PCAP capture: user does not exist.");
		} else {
			capture->capture_type = USER;
			capture->user_target = System::getInstance()->getUserDb()->getUser(captureTarget);
		}
	} else if (cType.compare("pcap") == 0){
		capture->capture_type = PCAP;
		capture->capture_filter = captureTarget;
	}else{
		capture->capture_type = NONE;
	}

	capture->start_capture();
	return JSONObject();
}

JSONObject NetworkSettingsDelegate::capture_stop(JSONObject obj){
	PacketCaptureEngine* capture = System::getInstance()->get_capture_engine();
	capture->stop_capture();
	return JSONObject();
}

JSONObject NetworkSettingsDelegate::mdns(JSONObject obj) {
	MdnsForwarder* forwarder = System::getInstance()->get_mdns_forwarder();

	JSONObject ret;
	JSONArray bridges;
	for(MdnsForwarderBridgePtr rule : forwarder->get_rules()){
		JSONObject bridge;
		bridge.put(L"id", new JSONValue((std::string) rule->id));
		bridge.put(L"name", new JSONValue((std::string) rule->name));

		JSONArray bridge_slaves;
		for(std::string ifname : rule->members){
			bridge_slaves.push_back(new JSONValue((string) ifname));
		}	
		bridge.put(L"slaves", new JSONValue(bridge_slaves));
		bridges.push_back(new JSONValue(bridge));
	}

	ret.put(L"bridges", new JSONValue(bridges));
	return ret;
}

JSONObject NetworkSettingsDelegate::mdns_set(JSONObject obj) {
	MdnsForwarder* forwarder = System::getInstance()->get_mdns_forwarder();
	MdnsForwarderBridgePtr rule;
	if(obj.has(L"id")){
		rule = forwarder->get_rule_by_id(obj[L"id"]->String());
	}

	if(!rule){
		rule = MdnsForwarderBridgePtr(new MdnsForwarderBridge());
		rule->id = StringUtils::genRandom();
		forwarder->add_rule(rule);
	}	

	if(obj.has(L"name")){
		rule->name = obj[L"name"]->String();
	}

	if(obj.has(L"slaves")){
		rule->resolved_members.clear();
		rule->members.clear();

		JSONArray slaves = obj[L"slaves"]->AsArray();
		for(int x = 0; x < slaves.size(); x++){
			rule->members.push_back(slaves[x]->String());		
		}	
	}

	forwarder->refresh_rule(rule);
	forwarder->save();
	return JSONObject();
}

JSONObject NetworkSettingsDelegate::mdns_del(JSONObject obj) {
	MdnsForwarder* forwarder = System::getInstance()->get_mdns_forwarder();
	MdnsForwarderBridgePtr rule = forwarder->get_rule_by_id(obj[L"id"]->String());
	if(rule){
		forwarder->del_rule(rule);
	}

	forwarder->save();
	return JSONObject();
}

JSONObject NetworkSettingsDelegate::tor(JSONObject obj) {
	JSONObject ret;
	TorProvider* tor_provider = System::getInstance()->get_tor_provider();
	ret.put(L"enabled", new JSONValue(tor_provider->enabled));	
	ret.put(L"interface", new JSONValue(tor_provider->interface));	
	ret.put(L"dns", new JSONValue(tor_provider->dns_enabled));	
	return ret;
}

JSONObject NetworkSettingsDelegate::tor_set(JSONObject obj) {
	TorProvider* tor_provider = System::getInstance()->get_tor_provider();
	tor_provider->enabled = obj[L"enabled"]->AsBool();
	tor_provider->interface = obj[L"interface"]->String();
	tor_provider->dns_enabled = obj[L"dns"]->AsBool();

	tor_provider->save();
	return JSONObject();
}

JSONObject NetworkSettingsDelegate::tor_status(JSONObject obj) {
	JSONObject ret;
	TorProvider* tor_provider = System::getInstance()->get_tor_provider();
	ret.put(L"status", new JSONValue(tor_provider->status()));
	ret.put(L"logs", new JSONValue(tor_provider->logs()));
	return ret;
}

JSONObject NetworkSettingsDelegate::tor_start(JSONObject obj) {
	TorProvider* tor_provider = System::getInstance()->get_tor_provider();
	tor_provider->start();
	return JSONObject();
}

JSONObject NetworkSettingsDelegate::tor_stop(JSONObject obj) {
	TorProvider* tor_provider = System::getInstance()->get_tor_provider();
	tor_provider->stop();
	return JSONObject();
}

JSONObject NetworkSettingsDelegate::wireless_online(JSONObject obj) {
	JSONObject ret;
	ret.put(L"status", new JSONValue((bool)System::getInstance()->getWirelessConfiguration()->online()));
	return ret;
}

JSONObject NetworkSettingsDelegate::ping(JSONObject obj) {
	JSONObject ret;
	float i = NetworkUtils::ping(obj[L"host"]->String());

	ret.put(L"result", new JSONValue((double) i));
	return ret;
}

JSONObject NetworkSettingsDelegate::wireless_start(JSONObject obj) {
	System::getInstance()->getWirelessConfiguration()->start();
	return JSONObject();
}

JSONObject NetworkSettingsDelegate::wireless_stop(JSONObject obj) {
	System::getInstance()->getWirelessConfiguration()->stop();
	return JSONObject();
}

JSONObject NetworkSettingsDelegate::devices_leases_add(JSONObject obj) {
	InterfacePtr device = System::getInstance()->get_interface_manager()->get_interface(obj[L"device"]->String());
	if (device) {
		DDInterfaceConfigurationPtr dhcp_server = System::getInstance()->get_dhcp_server_manager()->get_server_for_interface(device);

		DhcpServerLease* lease = new DhcpServerLease();
		lease->ip = IP4Addr::stringToIP4Addr(obj[L"ip"]->String());
		lease->mac_address = obj[L"mac"]->String();
		lease->state = DHCP_SERVER_LEASE_PERMANENT;

		dhcp_server->add_leases(DhcpServerLeasePtr(lease));
		System::getInstance()->get_dhcp_server_manager()->save();
	}

	return JSONObject();
}

JSONObject NetworkSettingsDelegate::devices_leases_del(JSONObject obj) {
	InterfacePtr device = System::getInstance()->get_interface_manager()->get_interface(obj[L"device"]->String());
	if (device) {
		DDInterfaceConfigurationPtr dhcp_server = System::getInstance()->get_dhcp_server_manager()->get_server_for_interface(device);
		DhcpServerLeasePtr lease = dhcp_server->find_lease_by_mac_ip(IP4Addr::stringToIP4Addr(obj[L"ip"]->String()), obj[L"mac"]->String());
		if(lease){
			dhcp_server->del_leases(lease);
		}
		System::getInstance()->get_dhcp_server_manager()->save();
	}

	return JSONObject();
}


JSONObject NetworkSettingsDelegate::devices_list(JSONObject obj) {
	JSONObject ret;
	JSONArray devices;
	System::getInstance()->get_interface_manager()->holdLock();
	for (InterfacePtr d : System::getInstance()->get_interface_manager()->get_all_interfaces()) {
		JSONObject interface;

		if (d) {
			interface.put(L"interface", new JSONValue((string) d->name));
			JSONArray ipv4_array;
			for(IPv4AddressPtr ip : d->ipv4_addresses){
				JSONObject address;
				address.put(L"ip", new JSONValue((string) IP4Addr::ip4AddrToString(ip->ip)));
				address.put(L"mask", new JSONValue((string) IP4Addr::ip4AddrToString(ip->mask)));
				address.put(L"broadcast", new JSONValue((string) IP4Addr::ip4AddrToString(ip->bcast)));
				ipv4_array.push_back(new JSONValue(address));
			}

			interface.put(L"ipv4", new JSONValue(ipv4_array));
			char hw[6];
			d->get_hardware_addr((char*) &hw);
			interface.put(L"mac", new JSONValue((string) IP4Addr::convertHw((unsigned char*) &hw)));
			interface.put(L"state", new JSONValue((bool) d->state));
			interface.put(L"name", new JSONValue((string) d->label));

			if(d->addr_mode == INTERFACE_ADDR_MODE_DHCP){
				interface.put(L"dhcp",  new JSONValue((bool) true));
			}else{
				interface.put(L"dhcp",  new JSONValue((bool) false));
			}

			interface.put(L"id", new JSONValue((double) d->ifid));
			interface.put(L"gateway", new JSONValue((string) IP4Addr::ip4AddrToString(d->gateway)));
			interface.put(L"persisted", new JSONValue((bool) d->configuration_entity));

			//Type information
			interface.put(L"bridge", new JSONValue((bool) d->is_bridge()));
			interface.put(L"vlan", new JSONValue((bool) d->is_vlan()));
			interface.put(L"lacp", new JSONValue((bool) d->is_bonding()));
			interface.put(L"physical", new JSONValue((bool) !d->is_bridge() && !d->is_vlan() && !d->is_bonding()));

			DDInterfaceConfigurationPtr dhcp_server = System::getInstance()->get_dhcp_server_manager()->get_server_for_interface(d);
			interface.put(L"dhcpServerMode", new JSONValue((double) dhcp_server->dhcp_mode));
			if(dhcp_server->dhcp_mode == DHCP_SERVER_SERVICE){
				interface.put(L"dhcpServerStart", new JSONValue((string) IP4Addr::ip4AddrToString(dhcp_server->start_ip)));
				interface.put(L"dhcpServerEnd", new JSONValue((string) IP4Addr::ip4AddrToString(dhcp_server->end_ip)));

				interface.put(L"dhcpServerDnsMode", new JSONValue((double) dhcp_server->dns_mode));
				interface.put(L"dhcpServerLeaseTime", new JSONValue((double) dhcp_server->max_lease_time));
				interface.put(L"dhcpServerDnsServer", new JSONValue((string) IP4Addr::ip4AddrToString(dhcp_server->dns_server)));

				interface.put(L"dhcpServerTftpEnabled", new JSONValue((bool) dhcp_server->tftp_boot_enabled));
				interface.put(L"dhcpServerTftpServer", new JSONValue((string) dhcp_server->tftp_boot_server));
				interface.put(L"dhcpServerTftpFilename", new JSONValue((string) dhcp_server->tftp_boot_filename));

				JSONArray leases;
				for (DhcpServerLeasePtr lease : dhcp_server->list_leases()) {
					JSONObject object;
					object.put(L"ip", new JSONValue((string) IP4Addr::ip4AddrToString(lease->ip)));
					object.put(L"mac", new JSONValue((string) lease->mac_address));
					object.put(L"state", new JSONValue((double) lease->state));
					object.put(L"lease_time", new JSONValue((double) lease->lease_time));

					leases.push_back(new JSONValue(object));
				}
				interface.put(L"dhcpServerLeases", new JSONValue(leases));

			}else if(dhcp_server->dhcp_mode == DHCP_SERVER_RELAY){
				interface.put(L"dhcpServerRelay", new JSONValue((string) IP4Addr::ip4AddrToString(dhcp_server->relay_server)));
			}

			//We must override the settings if another ip is set:
			if (d->is_bridge()) {
				BridgeInterface* bridge = (BridgeInterface*) d.get();

				JSONArray barr;
				for (string name : bridge->get_bridged_devices()) {
					barr.push_back(new JSONValue(name));
				}
				interface.put(L"bridgeDevices", new JSONValue(barr));
			}

			if (d->is_bonding()) {
				BondingInterface* lacp = (BondingInterface*) d.get();
				if(lacp){
					JSONArray barr;
					for (string bond : lacp->get_slaves()) {
						barr.push_back(new JSONValue(bond));
					}
					interface.put(L"lacpDevices", new JSONValue(barr));

					interface.put(L"lacpMode", new JSONValue((double) lacp->get_bonding_mode()));
					interface.put(L"lacpLinkStatePollFrequency", new JSONValue((double) lacp->get_link_state_poll_frequency()));
					interface.put(L"lacpLinkStateDownDelay", new JSONValue((double) lacp->get_link_down_delay()));
					interface.put(L"lacpLinkStateUpDelay", new JSONValue((double) lacp->get_link_up_delay()));
				}
			}

			if (d->is_vlan()) {
				VlanInterface* vlan = (VlanInterface*) d.get();

				interface.put(L"vlanId", new JSONValue((double) vlan->get_vlan_id()));
				interface.put(L"vlanInterface", new JSONValue((string) vlan->get_vlan_interface()));
			}

			interface.put(L"stats__rx_packets", new JSONValue((double) d->stats__get_rx_packets()));
			interface.put(L"stats__rx_errors", new JSONValue((double) d->stats__get_rx_errors()));
			interface.put(L"stats__rx_dropped", new JSONValue((double) d->stats__get_rx_dropped()));
			interface.put(L"stats__rx_bytes", new JSONValue((double) d->stats__get_rx_bytes()));

			interface.put(L"stats__tx_packets", new JSONValue((double) d->stats__get_tx_packets()));
			interface.put(L"stats__tx_errors", new JSONValue((double) d->stats__get_tx_errors()));
			interface.put(L"stats__tx_dropped", new JSONValue((double) d->stats__get_tx_dropped()));
			interface.put(L"stats__tx_bytes", new JSONValue((double) d->stats__get_tx_bytes()));

			if(!d->is_bridge() && !d->is_vlan() && !d->is_bonding()){
				interface.put(L"full_duplex", new JSONValue((bool) d->is_full_duplex()));
				interface.put(L"autoneg", new JSONValue((bool) d->get_autoneg()));
				interface.put(L"speed", new JSONValue((double) d->get_speed()));
				interface.put(L"has_link", new JSONValue((bool) d->has_link()));
				interface.put(L"supports_ethtool", new JSONValue((bool) d->supports_ethtool()));

			}
		}
		else {
			continue;
		}

		devices.push_back(new JSONValue(interface));
	}

	System::getInstance()->get_interface_manager()->releaseLock();
	ret.put(L"devices", new JSONValue(devices));
	return ret;
}

JSONObject NetworkSettingsDelegate::devices_set(JSONObject obj) {
	JSONObject interface;
	InterfacePtr dcp;

	//If we are editing an interface, then we have an interface already, otherwise we wont
	if(obj.has(L"interface")){	
		dcp = System::getInstance()->get_interface_manager()->get_interface(obj[L"interface"]->String());
		if(!dcp){
			return JSONObject();	
		}
	}else{
		//Determine debian interface name
		if(obj.has(L"vlan") && obj[L"vlan"]->AsBool()){
			System::getInstance()->get_interface_manager()->create_vlan_interface(obj[L"vlanInterface"]->String(), obj[L"vlanId"]->AsNumber());	
			return JSONObject();
		}else if(obj.has(L"lacp") && obj[L"lacp"]->AsBool()){
			//We need to find a suitable interface name
			for(int x = 0; x < 255; x++){
				stringstream interface;
				interface << "bond" << x;

				if(!System::getInstance()->get_interface_manager()->get_interface(interface.str())){	
					System::getInstance()->get_interface_manager()->create_bonding_interface(interface.str());
					return JSONObject();
				}
			}
		}else if(obj.has(L"bridge") && obj[L"bridge"]->AsBool()){
			//We need to find a suitable interface name
			for(int x = 0; x < 255; x++){
				stringstream interface;
				interface << "br" << x;

				if(!System::getInstance()->get_interface_manager()->get_interface(interface.str())){
					System::getInstance()->get_interface_manager()->create_bridge_interface(interface.str());
					return JSONObject();
				}
			}
		}else{
			return JSONObject();
		}
	}

	//Deal with difference modes:
	if (dcp->is_bridge() && obj.has(L"bridgeDevices")) {
		BridgeInterface* bridge = dynamic_cast<BridgeInterface*>(dcp.get());
		for(std::string bridge_member : bridge->get_bridged_devices()){
			bridge->remove_bridged_device(bridge_member);
		}

		JSONArray &barr = obj[L"bridgeDevices"]->AsArray();
		for (JSONValue * value : barr) {
			bridge->add_bridged_device(value->String());
		}
	}else if(dcp->is_bonding()){
		BondingInterface* bonding = dynamic_cast<BondingInterface*>(dcp.get());
		for(std::string slave : bonding->get_slaves()){
			bonding->remove_slave(slave);
		}

		bool was_up = bonding->state;
		bonding->bring_down();	
		bonding->set_bonding_mode(obj[L"lacpMode"]->AsNumber());
		bonding->set_link_state_poll_frequency(obj[L"lacpLinkStatePollFrequency"]->AsNumber());
		bonding->set_link_down_delay(obj[L"lacpLinkStateDownDelay"]->AsNumber());
		bonding->set_link_up_delay(obj[L"lacpLinkStateUpDelay"]->AsNumber());

		if(was_up){
			bonding->bring_up(System::getInstance()->get_interface_manager());
		}

		JSONArray &barr = obj[L"lacpDevices"]->AsArray();
		for (JSONValue * value : barr) {
			//We must make sure the interface target is down:
			InterfacePtr target = System::getInstance()->get_interface_manager()->get_interface(value->String());
			if(target){
				target->bring_down();	
				bonding->add_slave(value->String());
			}
		}
	}

	//Deal with addresses
	if (obj.has(L"dhcp")) {
		if(obj[L"dhcp"]->AsBool()){
			dcp->addr_mode = INTERFACE_ADDR_MODE_DHCP;
		}else{
			dcp->addr_mode = INTERFACE_ADDR_MODE_STATIC;
		}
	}

	if (obj.has(L"name")) {
		dcp->label = obj[L"name"]->String();
	}

	if (obj.has(L"gateway")) {
		dcp->gateway = IP4Addr::stringToIP4Addr(obj[L"gateway"]->String());
	}

	if (obj.has(L"persisted")) {
		System::getInstance()->get_interface_manager()->set_persisted(dcp, obj[L"persisted"]->AsBool());
	}

	if(dcp->is_physical()){
		if(obj.has(L"autoneg")){
			if(obj[L"autoneg"]->AsBool()){
				dcp->set_speed(true, -1, false);
			}else{
				dcp->set_speed(false, obj[L"speed"]->AsNumber(), obj[L"full_duplex"]->AsBool());
			}
		}
	}

	//Remove the current addresses:
	//Add the new addresses
	if(obj.has(L"ipv4")){
		if(dcp->addr_mode != INTERFACE_ADDR_MODE_DHCP){
			list<IPv4AddressPtr> copy_of_ipv4_addresses = dcp->ipv4_addresses;
			for(IPv4AddressPtr ip : copy_of_ipv4_addresses){
				dcp->remove_ipv4_address(ip->ip, ip->mask, ip->bcast);
			}		

			JSONArray ipv4 = obj[L"ipv4"]->AsArray();
			for(int x= 0; x < ipv4.size(); x++){
				JSONObject value = ipv4[x]->AsObject();
				dcp->add_ipv4_address(IP4Addr::stringToIP4Addr(value[L"ip"]->String()), IP4Addr::stringToIP4Addr(value[L"mask"]->String()), IP4Addr::stringToIP4Addr(value[L"broadcast"]->String()));
			}
		}
	}	

	if (obj.has(L"dhcpServerMode")) {
		DDServerManager* dhcp_server_manager = System::getInstance()->get_dhcp_server_manager();
		DDInterfaceConfigurationPtr instance = dhcp_server_manager->get_server_for_interface(dcp);
		instance->dhcp_mode = obj[L"dhcpServerMode"]->AsNumber();
		if(instance->dhcp_mode == DHCP_SERVER_SERVICE){
			instance->start_ip = IP4Addr::stringToIP4Addr(obj[L"dhcpServerStart"]->String());
			instance->end_ip = IP4Addr::stringToIP4Addr(obj[L"dhcpServerEnd"]->String());

			instance->dns_mode = obj[L"dhcpServerDnsMode"]->AsNumber();
			instance->max_lease_time = obj[L"dhcpServerLeaseTime"]->AsNumber();
			instance->dns_server = IP4Addr::stringToIP4Addr(obj[L"dhcpServerDnsServer"]->String());

			if(obj.has(L"dhcpServerTftpEnabled")){
				instance->tftp_boot_enabled = obj[L"dhcpServerTftpEnabled"]->AsBool();
				instance->tftp_boot_server = obj[L"dhcpServerTftpServer"]->String();
				instance->tftp_boot_filename = obj[L"dhcpServerTftpFilename"]->String();
			}
		}

		if(instance->dhcp_mode == DHCP_SERVER_RELAY){
			instance->relay_server = IP4Addr::stringToIP4Addr(obj[L"dhcpServerRelay"]->String());
		}

		instance->refresh_interfaces(System::getInstance()->get_interface_manager());
		dhcp_server_manager->save();
	}

	System::getInstance()->get_interface_manager()->save();
	return JSONObject();
}

JSONObject NetworkSettingsDelegate::devices_up(JSONObject obj) {
	InterfacePtr dcp;
	if(obj.has(L"device")){
		dcp = System::getInstance()->get_interface_manager()->get_interface(obj[L"device"]->String());
		if(dcp){
			dcp->bring_up(System::getInstance()->get_interface_manager());
		}

	}		
	return JSONObject();
}

JSONObject NetworkSettingsDelegate::devices_down(JSONObject obj) {
	InterfacePtr dcp;
	if(obj.has(L"device")){
		dcp = System::getInstance()->get_interface_manager()->get_interface(obj[L"device"]->String());
		if(dcp){
			dcp->bring_down();
		}

	}
	return JSONObject();
}

JSONObject NetworkSettingsDelegate::devices_save(JSONObject obj) {
	System::getInstance()->get_interface_manager()->save();
	return JSONObject();
}


JSONObject NetworkSettingsDelegate::dns_get(JSONObject) {
	JSONObject ret;
	ret.put(L"ns1", new JSONValue((string) dnsConfig->getNS1()));
	ret.put(L"ns2", new JSONValue((string) dnsConfig->getNS2()));
	ret.put(L"domain", new JSONValue((string) dnsConfig->getDomain()));
	ret.put(L"search", new JSONValue((string) dnsConfig->getSearch()));

	return ret;
}

JSONObject NetworkSettingsDelegate::dns_set(JSONObject obj) {
	dnsConfig->setNS1(obj[L"ns1"]->String());
	dnsConfig->setNS2(obj[L"ns2"]->String());
	dnsConfig->setSearch(obj[L"search"]->String());
	dnsConfig->setDomain(obj[L"domain"]->String());

	dnsConfig->save();

	if (eventDb) {
		EventParams params;
		eventDb->add(new Event(AUDIT_CONFIGURATION_NETWORK_DNS_SET, params));
	}

	return JSONObject();
}

JSONObject NetworkSettingsDelegate::routes_list(JSONObject) {
	JSONObject ret;
	JSONArray routes;

	for (PersistedNRoutePtr route : System::getInstance()->get_route_manager()->list_persisted_routes()) {
		JSONObject r;
		r.put(L"id", new JSONValue((string) route->id));

		if(route->route->destination != 0){
			r.put(L"destination", new JSONValue((string) IP4Addr::ip4AddrToString(route->route->destination)));
			r.put(L"destination_cidr", new JSONValue((double) route->route->destination_cidr));
		}

		if(route->route->route_device != 0){
			InterfacePtr interface = System::getInstance()->get_interface_manager()->get_interface_by_id(route->route->route_device);
			if(interface){
				r.put(L"route_device", new JSONValue((string) interface->name));
			}
		}

		if(route->route->route_nexthop != 0){
			r.put(L"route_nexthop", new JSONValue((string) IP4Addr::ip4AddrToString(route->route->route_nexthop)));
		}

		routes.push_back(new JSONValue(r));
	}

	ret.put(L"routes", new JSONValue(routes));
	return ret;
}

JSONObject NetworkSettingsDelegate::routes_add(JSONObject args) {
	PersistedNRoutePtr route(new PersistedNRoute());
	route->route = NRoutePtr(new NRoute());
	route->id = StringUtils::genRandom();

	if(args.has(L"destination")){
		route->route->destination = IP4Addr::stringToIP4Addr(args[L"destination"]->String());
		route->route->destination_cidr = args[L"destination_cidr"]->AsNumber();
	}

	if(args.has(L"route_device")){
		InterfacePtr interface = System::getInstance()->get_interface_manager()->get_interface(args[L"route_device"]->String());
		if(interface){
			route->route->route_device = interface->ifid;
		}
	}

	if(args.has(L"route_nexthop")){
		route->route->route_nexthop = IP4Addr::stringToIP4Addr(args[L"route_nexthop"]->String());
	}

	System::getInstance()->get_route_manager()->add_persisted_route_entry(route);
	System::getInstance()->get_route_manager()->save();
	return JSONObject();
}

JSONObject NetworkSettingsDelegate::routes_del(JSONObject args) {
	PersistedNRoutePtr target = System::getInstance()->get_route_manager()->get_persisted_route_entry(args[L"id"]->String());	
	if (target) {
		System::getInstance()->get_route_manager()->del_persisted_route_entry(target);
	}

	System::getInstance()->get_route_manager()->save();
	return JSONObject();
}

JSONObject NetworkSettingsDelegate::arp_size(JSONObject args) {
	JSONObject ret;
	ret.put(L"size", new JSONValue((double) arp->size()));
	return ret;
}

JSONObject NetworkSettingsDelegate::arp_list(JSONObject args) {
	JSONObject ret;
	JSONArray hosts;

	for (HostPtr entry : arp->get_all_entries()) {
		JSONObject obj;
		obj.put(L"host", new JSONValue((string) entry->getIp()));
		obj.put(L"hw", new JSONValue((string) entry->mac));
		obj.put(L"vendor", new JSONValue((string) entry->vendor));
		obj.put(L"loginTime", new JSONValue((double) entry->firstSeen));
		obj.put(L"lastSeenTime", new JSONValue((double) entry->lastSeen));
		obj.put(L"hostname", new JSONValue((string) entry->hostname()));
		obj.put(L"quarantined", new JSONValue((bool) entry->quarantined));

		if(entry->interface){
			obj.put(L"interface", new JSONValue((string) entry->interface->name));
		}

		if(entry->device_fingerprint){
			obj.put(L"fingerprint", new JSONValue((string) entry->device_fingerprint->id()));
			obj.put(L"fingerprint_name", new JSONValue((string) entry->device_fingerprint->name()));
		}

		obj.put(L"total_packet_count", new JSONValue((double) entry->total_packet_count));
		obj.put(L"total_transfer_count", new JSONValue((double) entry->total_transfer_count));
		obj.put(L"total_active_connections", new JSONValue((double) entry->total_active_connections));

		/* Find username */
		if(entry->authenticated_user){
			obj.put(L"user", new JSONValue((std::string) entry->authenticated_user->getUserName()));
			obj.put(L"authentication_provider", new JSONValue((std::string) entry->authentication_provider));
		}

		hosts.push_back(new JSONValue(obj));
	}

	ret.put(L"hosts", new JSONValue(hosts));
	return ret;
}

JSONObject NetworkSettingsDelegate::set_quarantined(JSONObject args){
	HostPtr host = arp->get_by_ip_mac(IP4Addr::stringToIP4Addr(args[L"ip"]->String()), args[L"mac"]->String());
	if(host){
		host->quarantined = args[L"quarantined"]->AsBool();	
	}

	return JSONObject();
}

JSONObject NetworkSettingsDelegate::connections_list(JSONObject obj) {
	JSONObject ret;
	JSONArray arr;

	list<Connection *> connections = connectionManager->getConnections();
	list<Connection *>::iterator iter;

	for (iter = connections.begin();
			iter != connections.end();
			iter++) {

		Connection *target = (*iter);
		JSONObject o;
		o.put(L"name", new JSONValue((string) target->getName()));
		o.put(L"device", new JSONValue((string) target->getDevice()));
		o.put(L"username", new JSONValue((string) target->getAuthenticationCredentials().username));
		o.put(L"password", new JSONValue((string) target->getAuthenticationCredentials().password));

		o.put(L"authenticationType", new JSONValue((double) target->getAuthenticationCredentials().authenticationType));
		o.put(L"type", new JSONValue((double) target->type()));
		o.put(L"state", new JSONValue((bool) target->isConnected()));
		o.put(L"keyfile", new JSONValue((string) target->getAuthenticationCredentials().keyfile));
		o.put(L"log", new JSONValue((string) target->log()));
		o.put(L"connected", new JSONValue((bool) target->isConnected()));
		arr.push_back(new JSONValue(o));
	}

	ret.put(L"items", new JSONValue(arr));
	return ret;
}

JSONObject NetworkSettingsDelegate::connectons_add(JSONObject obj) {
	connectionManager->createConnection(obj[L"name"]->String(), (ConnectionType) obj[L"type"]->AsNumber());
	return JSONObject();
}

JSONObject NetworkSettingsDelegate::connections_del(JSONObject obj) {
	Connection *target = connectionManager->getConnection(obj[L"name"]->String());

	if (target) {
		connectionManager->deleteConnection(target);
	}
	else {
		throw DelegateGeneralException("could not find connection");
	}

	return JSONObject();
}

JSONObject NetworkSettingsDelegate::connections_save(JSONObject obj) {
	Connection *connection = connectionManager->getConnection(obj[L"name"]->String());

	if (connection) {
		//Set the details:
		AuthenticationCredentials &auth = connection->getAuthenticationCredentials();

		if (connection->type() == PPPoE) {
			connection->setDevice(obj[L"device"]->String());
			auth.username = obj[L"username"]->String();
			auth.password = obj[L"password"]->String();
			auth.authenticationType = (ConnectionAuthenticationType) obj[L"authenticationType"]->AsNumber();
		}
		else {
			auth.keyfile = obj[L"keyfile"]->String();
		}

		connectionManager->save();

		connectionManager->publishAuthenticationCredentials();
		connection->publishSettings();
	}
	else {
		throw DelegateGeneralException("could not find connection");
	}

	return JSONObject();
}

JSONObject NetworkSettingsDelegate::connections_connect(JSONObject o) {
	Connection *connection = connectionManager->getConnection(o[L"name"]->String());

	if (connection) {
		connection->connect();

		if (eventDb) {
			EventParams params;
			params["name"] = o[L"name"]->String();
			eventDb->add(new Event(AUDIT_CONFIGURATION_NETWORK_CONNECTION_START, params));
		}
	}
	else {
		throw DelegateGeneralException("could not find connection");
	}

	return JSONObject();
}

JSONObject NetworkSettingsDelegate::connections_disconnect(JSONObject o) {
	Connection *connection = connectionManager->getConnection(o[L"name"]->String());

	if (connection) {
		connection->disconnect();

		if (eventDb) {
			EventParams params;
			params["name"] = o[L"name"]->String();
			eventDb->add(new Event(AUDIT_CONFIGURATION_NETWORK_CONNECTION_STOP, params));
		}
	}
	else {
		throw DelegateGeneralException("could not find connection");
	}

	return JSONObject();
}

JSONObject NetworkSettingsDelegate::vpn_start(JSONObject obj) {
	IPSecGatewayToGatewayInstancePtr instance = vpnManager->get_sitetosite()->get(obj[L"name"]->String());

	if (instance) {
		vpnManager->get_sitetosite()->do_connect(instance);
	}

	vpnManager->save();
	return JSONObject();
}

JSONObject NetworkSettingsDelegate::vpn_stop(JSONObject obj) {
	IPSecGatewayToGatewayInstancePtr instance = vpnManager->get_sitetosite()->get(obj[L"name"]->String());

	if (instance) {
		vpnManager->get_sitetosite()->do_disconnect(instance);
	}

	vpnManager->save();
	return JSONObject();
}

JSONObject NetworkSettingsDelegate::vpn_status(JSONObject obj) {
	IPSecGatewayToGatewayInstancePtr instance = vpnManager->get_sitetosite()->get(obj[L"name"]->String());
	if (instance) {
		JSONObject ret;
		ret.put(L"status", new JSONValue((double) vpnManager->get_sitetosite()->status(instance)));
		return ret;
	}

	return JSONObject();
}

JSONObject NetworkSettingsDelegate::vpn_instances_list(JSONObject obj) {
	JSONObject ret;
	JSONArray arr;

	for (IPSecGatewayToGatewayInstancePtr instance : vpnManager->get_sitetosite()->list_instances()) {
		JSONObject i;

		i.put(L"name", new JSONValue((string) instance->name));
		i.put(L"local_device", new JSONValue((std::string) instance->local_device));
		i.put(L"local_shared_device", new JSONValue((std::string) instance->local_shared_device));
		i.put(L"local_id", new JSONValue((string) instance->local_id));

		i.put(L"remote_host", new JSONValue((string) IP4Addr::ip4AddrToString(instance->remote_host)));
		i.put(L"remote_network", new JSONValue((string) IP4Addr::ip4AddrToString(instance->remote_shared_network)));
		i.put(L"remote_subnet", new JSONValue((string) IP4Addr::ip4AddrToString(instance->remote_shared_subnet)));
		i.put(L"remote_id", new JSONValue((string) instance->remote_id));
		i.put(L"auth_mode", new JSONValue((double) instance->auth_mode));
		i.put(L"secret_key", new JSONValue((string) instance->secret_key));
		i.put(L"auto_start", new JSONValue((bool) instance->auto_start));
		i.put(L"meta", new JSONValue((string) instance->meta));
		i.put(L"auto_configure_firewall", new JSONValue((bool) instance->auto_configure_firewall));

		i.put(L"status", new JSONValue((double) vpnManager->get_sitetosite()->status(instance)));

		map<string, string> detailed_status = instance->detailed_status();
		JSONObject state;
		for(pair<string, string> pairs : detailed_status){
			state.put(StringToWString(pairs.first), new JSONValue((string) pairs.second));
		}
		i.put(L"state", new JSONValue(state));
		i.put(L"log", new JSONValue((string) vpnManager->get_sitetosite()->log(instance)));

		arr.push_back(new JSONValue(i));
	}

	ret.put(L"items", new JSONValue(arr));
	return ret;
}

JSONObject NetworkSettingsDelegate::vpn_instance_create(JSONObject obj) {
	string name = obj[L"name"]->String();

	IPSecGatewayToGatewayInstancePtr instance = IPSecGatewayToGatewayInstancePtr(new IPSecGatewayToGatewayInstance());
	instance->name = name;
	vpnManager->get_sitetosite()->add(instance);
	vpnManager->save();
	return JSONObject();
}

JSONObject NetworkSettingsDelegate::vpn_instance_remove(JSONObject obj) {
	IPSecGatewayToGatewayInstancePtr instance = vpnManager->get_sitetosite()->get(obj[L"name"]->String());
	if (instance) {
		vpnManager->get_sitetosite()->remove(instance);
	}

	vpnManager->save();
	return JSONObject();
}

JSONObject NetworkSettingsDelegate::vpn_options(JSONObject obj) {
	IPSecGatewayToGatewayInstancePtr instance = vpnManager->get_sitetosite()->get(obj[L"name"]->String());
	if (instance) {
		instance->local_device = obj[L"local_device"]->String();
		instance->local_shared_device = obj[L"local_shared_device"]->String();
		instance->local_id = obj[L"local_id"]->String();

		instance->remote_host = IP4Addr::stringToIP4Addr(obj[L"remote_host"]->String());
		instance->remote_shared_network = IP4Addr::stringToIP4Addr(obj[L"remote_network"]->String());
		instance->remote_shared_subnet = IP4Addr::stringToIP4Addr(obj[L"remote_subnet"]->String());
		instance->remote_id = obj[L"remote_id"]->String();
		instance->auth_mode = obj[L"auth_mode"]->AsNumber();
		instance->secret_key = obj[L"secret_key"]->String();
		instance->meta = obj[L"meta"]->String();
		instance->auto_start = obj[L"auto_start"]->AsBool();
		instance->auto_configure_firewall = obj[L"auto_configure_firewall"]->AsBool();

	}

	vpnManager->save();
	return JSONObject();
}

JSONObject NetworkSettingsDelegate::vpn_clientvpn(JSONObject obj) {
	JSONObject i;	
	IPSecL2TPGateway* target = vpnManager->get_l2tp(); 
	i.put(L"local_device", new JSONValue((std::string) target->local_device));
	i.put(L"auth_mode", new JSONValue((double) target->auth_mode));
	i.put(L"auto_start", new JSONValue((bool) target->auto_start));
	i.put(L"secret_key", new JSONValue((string) target->secret_key));

	i.put(L"vpn_assign_ip", new JSONValue((bool) target->vpn_assign_ip));
	i.put(L"vpn_remote_ip_range_start", new JSONValue((string) IP4Addr::ip4AddrToString(target->vpn_remote_ip_range_start)));
	i.put(L"vpn_remote_ip_range_end", new JSONValue((string) IP4Addr::ip4AddrToString(target->vpn_remote_ip_range_end)));
	i.put(L"vpn_local_ip", new JSONValue((string) IP4Addr::ip4AddrToString(target->vpn_local_ip)));
	i.put(L"status", new JSONValue((bool) target->status()));
	i.put(L"auto_configure_firewall", new JSONValue((bool) target->auto_configure_firewall));

	i.put(L"provide_dns", new JSONValue((bool) target->provide_dns));
	i.put(L"dns_ns1", new JSONValue((string) target->dns_ns1));
	i.put(L"dns_ns2", new JSONValue((string) target->dns_ns2));

	i.put(L"user_authentication_mode", new JSONValue((double) target->user_authentication_mode));
	JSONArray allowed_groups;
	for(GroupPtr group : target->allowed_users_groups){
		allowed_groups.push_back(new JSONValue((double) group->getId()));
	}

	i.put(L"allowed_groups", new JSONValue(allowed_groups));

	JSONArray active_clients;
	for(IPSecL2TPGatewayInstanceClientPtr c: target->active_clients){
		JSONObject client;
		client.put(L"local_ip", new JSONValue((string)c->local_ip));
		client.put(L"remote_ip", new JSONValue((string)c->remote_ip));
		client.put(L"ifname", new JSONValue((string)c->ifname));
		client.put(L"username", new JSONValue((string)c->username));

		active_clients.push_back(new JSONValue(client));
	}
	i.put(L"active_clients", new JSONValue(active_clients));
	return i;
}

JSONObject NetworkSettingsDelegate::vpn_clientvpn_options(JSONObject obj) {
	IPSecL2TPGateway* igtg = vpnManager->get_l2tp();

	igtg->local_device = obj[L"local_device"]->String();
	igtg->auth_mode = obj[L"auth_mode"]->AsNumber();
	igtg->secret_key = obj[L"secret_key"]->String();
	igtg->auto_start = obj[L"auto_start"]->AsBool();
	igtg->auto_configure_firewall = obj[L"auto_configure_firewall"]->AsBool();

	igtg->vpn_assign_ip = obj[L"vpn_assign_ip"]->AsBool();
	igtg->vpn_remote_ip_range_start = IP4Addr::stringToIP4Addr(obj[L"vpn_remote_ip_range_start"]->String());
	igtg->vpn_remote_ip_range_end = IP4Addr::stringToIP4Addr(obj[L"vpn_remote_ip_range_end"]->String());
	igtg->vpn_local_ip = IP4Addr::stringToIP4Addr(obj[L"vpn_local_ip"]->String());
	igtg->user_authentication_mode = obj[L"user_authentication_mode"]->AsNumber();

	igtg->provide_dns = obj[L"provide_dns"]->AsBool();
	igtg->dns_ns1= obj[L"dns_ns1"]->String();
	igtg->dns_ns2= obj[L"dns_ns2"]->String();

	if(obj.has(L"groups")){
		JSONArray allowed_groups = obj[L"groups"]->AsArray();
		igtg->allowed_users_groups.clear();
		for(int x= 0; x < allowed_groups.size(); x++){
			int gid = allowed_groups[x]->AsNumber();
			GroupPtr group_ptr = System::getInstance()->getGroupDb()->getGroup(gid);
			if(group_ptr){
				igtg->allowed_users_groups.push_back(group_ptr);
			}
		}
	}

	igtg->refresh();
	vpnManager->save();	
	return JSONObject();
}

JSONObject NetworkSettingsDelegate::dyndns(JSONObject obj) {
	GlobalDynDns *dns = System::getInstance()->getGlobalDynDns();
	JSONObject ret;
	ret.put(L"username", new JSONValue((string) dns->getUsername()));
	ret.put(L"password", new JSONValue((string) dns->getPassword()));
	ret.put(L"domain", new JSONValue((string) dns->getDomain()));
	ret.put(L"enabled", new JSONValue((bool) dns->isEnabled()));
	ret.put(L"type", new JSONValue((double)(DynamicDnsClientProvider) dns->getType()));
	ret.put(L"active", new JSONValue((bool) dns->active()));
	ret.put(L"lastip", new JSONValue((string) dns->getLastIp()));
	return ret;
}

JSONObject NetworkSettingsDelegate::dyndns_set(JSONObject obj) {
	GlobalDynDns *dns = System::getInstance()->getGlobalDynDns();
	dns->setUsername(obj[L"username"]->String());
	dns->setPassword(obj[L"password"]->String());
	dns->setDomain(obj[L"domain"]->String());
	dns->setEnabled(obj[L"enabled"]->AsBool());
	dns->setType((DynamicDnsClientProvider) obj[L"type"]->AsNumber());
	dns->run(true);

	dns->save();
	return JSONObject();
}

JSONObject NetworkSettingsDelegate::devices_delete(JSONObject obj) {
	IntMgr* mgr = System::getInstance()->get_interface_manager();
	InterfacePtr interface = mgr->get_interface(obj[L"device"]->String());
	if(interface){
		interface->bring_down();
		mgr->delete_interface(interface);
	}
	return JSONObject();
}

JSONObject NetworkSettingsDelegate::dig(JSONObject obj) {
	JSONObject ret;
	ret.put(L"result", new JSONValue(NetworkUtils::dig(obj[L"hostname"]->String())));
	return ret;
}

JSONObject NetworkSettingsDelegate::traceroute(JSONObject obj) {
	JSONObject ret;
	ret.put(L"result", new JSONValue(NetworkUtils::trace(obj[L"hostname"]->String())));
	return ret;
}

