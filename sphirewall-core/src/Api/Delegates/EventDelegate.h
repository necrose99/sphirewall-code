/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef EventDelegate_h
#define EventDelegate_h

#include "Api/Delegate.h"

class EventDelegate : public virtual Delegate {
	public:
		EventDelegate(EventDb *eventDb);
		JSONObject process(std::string uri, JSONObject object);
		std::string rexpression();

	private:
		EventDb *eventDb;

		_BEGIN_SPHIREWALL_DELEGATES(EventDelegate)
		DelegateHandlerImpl list;
		DelegateHandlerImpl size;
		DelegateHandlerImpl config_handlers_available;
		DelegateHandlerImpl config_type_available;
		DelegateHandlerImpl config_add;
		DelegateHandlerImpl config_del;
		DelegateHandlerImpl config_list;
		DelegateHandlerImpl config_handlers_config;
		DelegateHandlerImpl config_handlers_config_list;
		DelegateHandlerImpl purge;
		_END_SPHIREWALL_DELEGATES
};

#endif
