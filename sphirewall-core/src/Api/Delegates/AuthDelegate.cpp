/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <string>
using namespace std;

#include "Core/HostDiscoveryService.h"
#include "Api/Exceptions.h"
#include "Json/JSON.h"
#include "Utils/StringUtils.h"
#include "Core/System.h"
#include "Core/Event.h"
#include "Auth/UserDb.h"
#include "Auth/User.h"
#include "Utils/Hash.h"
#include "Utils/IP4Addr.h"
#include "Utils/IP6Addr.h"
#include "Auth/WindowsWmiAuthenticator.h"
#include "Api/Delegates/AuthDelegate.h"
#include "SFwallCore/Firewall.h"
#include "Core/Vpn.h"
#include "Core/IPSec.h"

#define installHandler(x,y) delegateMap.insert(DelegatePath(x, &AuthDelegate::y))

AuthDelegate::AuthDelegate(GroupDb *groupDb, UserDb *userDb, SFwallCore::Firewall *firewall, HostDiscoveryService *arp)
	: groupDb(groupDb), userDb(userDb), firewall(firewall), arp(arp), authManager(authManager), eventDb(NULL) {

	installHandler("auth/groups/list", groups_list);
	installHandler("auth/groups/list/w/members", groups_list_with_members);
	installHandler("auth/groups/create", groups_create);
	installHandler("auth/groups/create/bulk", groups_create_bulk);
	installHandler("auth/groups/get", groups_get);
	installHandler("auth/groups/del", groups_del);
	installHandler("auth/groups/mergeoveruser", groups_mergeoveruser);
	installHandler("auth/groups/save", groups_save);
	installHandler("auth/users/list", users_list);
	installHandler("auth/users/add", users_add);
	installHandler("auth/users/del", users_del);
	installHandler("auth/users/get", users_get);
	installHandler("auth/users/save", users_save);
	installHandler("auth/users/setpassword", user_setpassword);
	installHandler("auth/users/groups/add", users_groups_add);
	installHandler("auth/users/groups/del", users_groups_del);
	installHandler("auth/users/disable", users_disable);
	installHandler("auth/users/enable", users_enable);
	installHandler("auth/sessions/list", sessions_list);
	installHandler("auth/sessions/persist", sessions_persist);
	installHandler("auth/sessions/persist/remove", sessions_persist_remove);
	installHandler("auth/sessions/persist/remove/group", sessions_persist_remove_by_group);
	installHandler("auth/sessions/persist/remove/list", sessions_persist_remove_list);
	installHandler("auth/login", login);
	installHandler("auth/logout", logout);
	installHandler("auth/users/defaultpasswordset" , defaultpasswordset);
	installHandler("auth/createsession", sessions_create);
	installHandler("auth/ldap", ldap);
	installHandler("auth/ldap/sync", ldap_sync);
	installHandler("auth/wmic", wmic);
        installHandler("auth/wmic/add", wmic_add);
        installHandler("auth/wmic/del", wmic_del);
	installHandler("auth/wmic/resetcursor", wmic_reset_cursor);

	installHandler("auth/user/merge", user_merge);
	installHandler("auth/sessions/networktimeouts", sessions_networktimeouts);
	installHandler("auth/sessions/networktimeouts/remove", sessions_networktimeouts_remove);
	installHandler("auth/sessions/networktimeouts/manage", sessions_networktimeouts_manage);

	installHandler("auth/realm/login", realm_login);
	installHandler("auth/realm/join", realm_join);
	installHandler("auth/realm/leave", realm_leave);
	installHandler("auth/sessions/group/list", sessions_group_list);
}

string AuthDelegate::rexpression() {
	return "auth/(.*)";
}

void AuthDelegate::setEventDb(EventDb *eventDb) {
	this->eventDb = eventDb;
}

JSONObject AuthDelegate::process(string uri, JSONObject args) {
	try {
		return invokeHandler(uri, args);
	}
	catch (std::out_of_range e) {
		throw DelegateNotFoundException(uri);
	}

	return JSONObject();
}

JSONObject AuthDelegate::wmic(JSONObject args) {
	JSONObject ret;
	WinWmiAuth* provider = System::getInstance()->wmic;
	JSONArray dc_array;
	for(WinDomainControllerPtr dc : provider->get_domain_controllers()){
		JSONObject o;
		o.put(L"cursor", new JSONValue((double) dc->cursor));
		o.put(L"hostname", new JSONValue((std::string) dc->hostname));
		o.put(L"id", new JSONValue((std::string) dc->id));
		o.put(L"username", new JSONValue((std::string) dc->username));
		o.put(L"password", new JSONValue((std::string) dc->password));
		o.put(L"domain", new JSONValue((std::string) dc->domain));
		o.put(L"mode", new JSONValue((double) dc->mode));
		o.put(L"enabled", new JSONValue((bool) dc->enabled));

		JSONArray exceptions;
		for(std::string exception : dc->exceptions){
			exceptions.push_back(new JSONValue((std::string) exception));
		}
		o.put(L"exceptions", new JSONValue(exceptions));
		
		if(dc->enabled){
			try {
				dc->check();
				o.put(L"status", new JSONValue((std::string) "Online"));
			}catch(const WmiException& ex){
				o.put(L"status", new JSONValue((std::string) ex.message()));
			}
		}

		dc_array.push_back(new JSONValue(o));
	}	

	ret.put(L"controllers", new JSONValue(dc_array));
	return ret;
}

JSONObject AuthDelegate::wmic_add(JSONObject args) {
	JSONObject ret;
	WinWmiAuth* provider = System::getInstance()->wmic;

	WinDomainControllerPtr controller;
	if(args.has(L"id")){
		controller = provider->get_controller(args[L"id"]->String());	
	}else{
		controller = WinDomainControllerPtr(new WinDomainController());
		controller->id = StringUtils::genRandom();
		provider->add_domain_controller(controller);
	}

	controller->hostname = args[L"hostname"]->String();
	controller->username = args[L"username"]->String();
	controller->password= args[L"password"]->String();
	controller->domain= args[L"domain"]->String();
	controller->mode = args[L"mode"]->AsNumber();
	controller->enabled = args[L"enabled"]->AsBool();
	controller->cursor =0;

	JSONArray exceptions = args[L"exceptions"]->AsArray();
	controller->exceptions.clear();
	for(int x= 0; x < exceptions.size(); x++){
		if(exceptions[x]->String().size() > 0){
			controller->exceptions.insert(exceptions[x]->String());
		}
	}

	provider->save();
	return ret;
}

JSONObject AuthDelegate::wmic_del(JSONObject args) {
	WinWmiAuth* provider = System::getInstance()->wmic;
	WinDomainControllerPtr controller = provider->get_controller(args[L"id"]->String());	
	provider->del_domain_controller(controller);
	provider->save();
	return JSONObject();
}               

JSONObject AuthDelegate::wmic_reset_cursor(JSONObject args) {
	//System::getInstance()->wmic->refresh();
	return JSONObject();
}

JSONObject AuthDelegate::ldap_sync(JSONObject args) {
	DirectoryServiceProvider* method = userDb->get_directory_service(LDAP_DB);
	JSONObject ret;

	try {
		/* First check that we can connect, and how many groups are available to be imported */
		list<string> new_groups;
		method->find_all_groups(new_groups);	
		ret.put(L"count", new JSONValue((double) new_groups.size()));

		/* Now trigger a full update on the userdb */
		userDb->sync_directory_providers();
	}catch(ProviderException& exception){
		ret.put(L"err", new JSONValue((string) exception.what()));
	}

	return ret;
}

JSONObject AuthDelegate::ldap(JSONObject args) {
	DirectoryServiceProvider* method = userDb->get_directory_service(LDAP_DB);

	JSONObject ret;
	try {
		ret.put(L"online", new JSONValue((bool) method->check()));
	}catch(ProviderException& exception){
		ret.put(L"online", new JSONValue((bool) false));
		ret.put(L"err", new JSONValue((string) exception.what()));
	}

	return ret;
}

JSONObject AuthDelegate::groups_create(JSONObject args) {
	string name = args[L"name"]->String();
	if(name.size() == 0){
		throw DelegateGeneralException("Could not create a group without a name");
	}

	if (!groupDb->getGroup(name)) {
		groupDb->createGroup(name);

		if (eventDb) {
			eventDb->add(
					new Event(
						AUDIT_CONFIGURATION_GROUPDB_ADD,
						EventParams(
							list<pair<string, Param>> {
							pair<string, Param>{"name", Param(name)}
							}
							)
						)
				    );
		}

		groupDb->save();
	}
	return JSONObject();
}

JSONObject AuthDelegate::groups_create_bulk(JSONObject args) {
	for (JSONValue * jvp : args[L"values"]->AsArray()) {
		JSONObject &o = jvp->AsObject();
		string name = o[L"name"]->String();
		if(name.size() == 0){
			throw DelegateGeneralException("Could not create a group without a name");
		}

		if (!groupDb->getGroup(name)) {
			groupDb->createGroup(name);

			if (eventDb) {
				eventDb->add(
						new Event(
							AUDIT_CONFIGURATION_GROUPDB_ADD,
							EventParams(
								list<pair<string, Param>> {
								pair<string, Param>{"name", Param(name)}
								}
								)
							)
					    );
			}
		}
	}

	groupDb->save();
	return JSONObject();
}

JSONObject AuthDelegate::groups_mergeoveruser(JSONObject args) {
	//First remove the user from all groups:
	UserPtr target = userDb->getUser(args[L"username"]->String());

	if (!target) {
		target = userDb->createUser(args[L"username"]->String());
	}

	vector<GroupPtr> groups = target->getGroups();

	for (GroupPtr toDelete : groups) {
		target->removeGroup(toDelete);
	}

	JSONArray &groupNames = args[L"groups"]->AsArray();

	for (JSONValue * value : groupNames) {
		GroupPtr targetGroup = groupDb->getGroup(value->String());

		if (!targetGroup) {
			targetGroup = groupDb->createGroup(value->String());
		}

		target->addGroup(targetGroup);
	}

	groupDb->save();
	userDb->save();
	return JSONObject();
}

JSONObject AuthDelegate::defaultpasswordset(JSONObject args) {
	JSONObject ret;

	UserPtr targetUser = userDb->getUser("admin");
	if (targetUser) {
		Hash hash;

		if (targetUser->getPassword().compare(hash.create_hash("admin", 5)) != -1) {
			ret.put(L"value", new JSONValue((bool) true));
		}
		else {
			ret.put(L"value", new JSONValue((bool) false));
		}
	}
	else {
		ret.put(L"value", new JSONValue((bool) false));
	}

	return ret;
}

JSONObject AuthDelegate::serialize_group(GroupPtr target, bool include_members){
	JSONObject group;
	group.put(L"id", new JSONValue((double) target->getId()));
	group.put(L"name", new JSONValue((string) target->getName()));
	group.put(L"manager", new JSONValue((string) target->getManager()));
	group.put(L"allowMui", new JSONValue((bool) target->isAllowMui()));
	group.put(L"metadata", new JSONValue((string) target->getMetadata()));

	if(include_members){
		JSONArray group_members;
		for(std::string username : userDb->list()){
			UserPtr potential_user = userDb->getUser(username);
			if(potential_user){
				if(potential_user->checkForGroup(target)){
					group_members.push_back(new JSONValue((string) username));
				}
			}
		}

		group.put(L"members", new JSONValue(group_members));
	}
	return group;
}

JSONObject AuthDelegate::groups_list(JSONObject args) {
	JSONObject ret;
	JSONArray groups;
	for (GroupPtr target : groupDb->list()) {
		JSONObject group = serialize_group(target, false);
		groups.push_back(new JSONValue(group));
	}

	ret.put(L"groups", new JSONValue(groups));
	return ret;
}

JSONObject AuthDelegate::groups_list_with_members(JSONObject args) {
	JSONObject ret;
	JSONArray groups;
	for (GroupPtr target : groupDb->list()) {
		JSONObject group = serialize_group(target, true);
		groups.push_back(new JSONValue(group));
	}

	ret.put(L"groups", new JSONValue(groups));
	return ret;
}

JSONObject AuthDelegate::groups_get(JSONObject args) {
	GroupPtr target = groupDb->getGroup(args[L"id"]->AsNumber());

	if (target) {
		return serialize_group(target, true);
	}

	return JSONObject();
}

JSONObject AuthDelegate::groups_del(JSONObject args) {
	GroupPtr target;

	if (args.has(L"id")) {
		target = groupDb->getGroup(args[L"id"]->AsNumber());
	}
	else if (args.has(L"name")) {
		target = groupDb->getGroup(args[L"name"]->String());
	}

	if (target) {
		if (eventDb) {
			EventParams params;
			params["group"] = target->getName();
			eventDb->add(new Event(AUDIT_CONFIGURATION_GROUPDB_DEL, params));
		}

		groupDb->delGroup(target);
	}

	return JSONObject();
}

JSONObject AuthDelegate::groups_save(JSONObject args) {
	GroupPtr target = groupDb->getGroup(args[L"id"]->AsNumber());

	if (target) {
		if (args.has(L"desc")) {
			target->setDesc(args[L"desc"]->String());
		}

		if (args.has(L"manager")) {
			target->setManager(args[L"manager"]->String());
		}

		if (args.has(L"mui")) {
			target->setAllowMui(args[L"mui"]->AsBool());
		}

		if (args.has(L"metadata")) {
			target->setMetadata(args[L"metadata"]->String());
		}

		QuotaInfo &quota = target->getQuota();
		if (args.has(L"dailyQuota")) {
			quota.dailyQuota = args[L"dailyQuota"]->AsBool();
			quota.dailyQuotaLimit = args[L"dailyQuotaLimit"]->AsNumber();
		}

		if (args.has(L"weeklyQuota")) {
			quota.weeklyQuota = args[L"weeklyQuota"]->AsBool();
			quota.weeklyQuotaLimit = args[L"weeklyQuotaLimit"]->AsNumber();
		}

		if (args.has(L"monthQuota")) {
			quota.monthQuota = args[L"monthQuota"]->AsBool();
			quota.monthQuotaLimit = args[L"monthQuotaLimit"]->AsNumber();
		}

		if (args.has(L"totalQuota")) {
			quota.totalQuota = args[L"totalQuota"]->AsBool();
			quota.totalQuotaLimit = args[L"totalQuotaLimit"]->AsNumber();
		}

		if (args.has(L"timeQuota")) {
			quota.timeQuota = args[L"timeQuota"]->AsBool();
			quota.timeQuotaLimit = args[L"timeQuotaLimit"]->AsNumber();
		}

		if (eventDb) {
			EventParams params;
			params["id"] = args[L"id"]->AsNumber();
			eventDb->add(new Event(AUDIT_CONFIGURATION_GROUPDB_MODIFIED, params));
		}
	}

	groupDb->save();
	return JSONObject();
}

JSONObject AuthDelegate::users_list(JSONObject args) {
	JSONObject ret;
	JSONArray users;

	vector<string> userList = userDb->list();

	for (string userListItem : userList) {
		UserPtr source = userDb->getUser(userListItem);
		if (source) {
			JSONObject user;
			user.put(L"username", new JSONValue((string) source->getUserName()));
			user.put(L"fname", new JSONValue((string) source->getFname()));
			user.put(L"lname", new JSONValue((string) source->getLname()));
			user.put(L"email", new JSONValue((string) source->getEmail()));
			user.put(L"lastLogin", new JSONValue((double) source->getLastLogin()));
			user.put(L"enabled", new JSONValue((bool) source->getIsEnabled()));
			user.put(L"temp_user", new JSONValue((bool) source->temp_user));
			user.put(L"expiry_timestamp", new JSONValue((double) source->expiry_timestamp));

			users.push_back(new JSONValue(user));
		}
	}

	ret.put(L"users", new JSONValue(users));
	return ret;
}

JSONObject AuthDelegate::user_merge(JSONObject args) {
	std::string username = args[L"username"]->String();
	UserPtr user = userDb->getUser(username);
	if (!user) {
		if(username.size() == 0){
			throw DelegateGeneralException("Could not create user without a name");
		}

		user = userDb->createUser(username);
	}

	user->setFname(args[L"fname"]->String());
	user->setLname(args[L"lname"]->String());
	user->setPassword(args[L"password"]->String());

	JSONArray groups = args[L"groups"]->AsArray();

	for (uint x = 0; x < groups.size(); x++) {
		GroupPtr target = groupDb->getGroup(groups[x]->String());

		if (target) {
			user->addGroup(target);
		}
	}

	userDb->save();
	return JSONObject();
}

JSONObject AuthDelegate::users_add(JSONObject args) {
	if (StringUtils::trim(args[L"username"]->String()) != "") {
		userDb->createUser(args[L"username"]->String());

		if (eventDb) {
			EventParams params;
			params["username"] = args[L"username"]->String();
			eventDb->add(new Event(AUDIT_CONFIGURATION_USERDB_ADD, params));
		}
	}else{
		throw DelegateGeneralException("Could not create user without a name");
	}

	return JSONObject();
}

JSONObject AuthDelegate::users_del(JSONObject args) {
	UserPtr user = userDb->getUser(args[L"username"]->String());
	if (user) {
		userDb->delUser(user);

		if (eventDb) {
			EventParams params;
			params["username"] = args[L"username"]->String();
			eventDb->add(new Event(AUDIT_CONFIGURATION_USERDB_DEL, params));
		}
	}

	userDb->save();

	return JSONObject();
}

JSONObject AuthDelegate::users_get(JSONObject args) {
	UserPtr target = userDb->getUser(args[L"username"]->String());
	if (target) {
		JSONObject user;
		user.put(L"username", new JSONValue((string) target->getUserName()));
		user.put(L"fname", new JSONValue((string) target->getFname()));
		user.put(L"lname", new JSONValue((string) target->getLname()));
		user.put(L"email", new JSONValue((string) target->getEmail()));
		user.put(L"lastLogin", new JSONValue((double) target->getLastLogin()));
		user.put(L"enabled", new JSONValue((bool) target->getIsEnabled()));
		user.put(L"temp_user", new JSONValue((bool) target->temp_user));
		user.put(L"expiry_timestamp", new JSONValue((double) target->expiry_timestamp));

		JSONArray groupsArray;
		for (GroupPtr group : target->getGroups()) {
			JSONObject jsonGroup;
			jsonGroup.put(L"id", new JSONValue((double) group->getId()));
			jsonGroup.put(L"name", new JSONValue((string) group->getName()));
			jsonGroup.put(L"desc", new JSONValue((string) group->getDesc()));
			jsonGroup.put(L"metadata", new JSONValue((string) group->getMetadata()));

			groupsArray.push_back(new JSONValue(jsonGroup));
		}

		user.put(L"groups", new JSONValue(groupsArray));

		return user;
	}
	else {
		throw DelegateGeneralException("Could not find user");
	}
}

JSONObject AuthDelegate::users_save(JSONObject args) {
	UserPtr target = userDb->getUser(args[L"username"]->String());
	if (target) {
		if (args.has(L"fname")) {
			target->setFname(args[L"fname"]->String());
		}

		if (args.has(L"lname")) {
			target->setLname(args[L"lname"]->String());
		}

		if (args.has(L"email")) {
			target->setEmail(args[L"email"]->String());
		}

		if(args.has(L"temp_user")){
			target->temp_user = args[L"temp_user"]->AsBool();
			target->expiry_timestamp = args[L"expiry_timestamp"]->AsNumber();
		}

		if (eventDb) {
			EventParams params;
			params["username"] = args[L"username"]->String();
			eventDb->add(new Event(AUDIT_CONFIGURATION_USERDB_MODIFIED, params));
		}
	}

	userDb->save();
	return JSONObject();
}

JSONObject AuthDelegate::user_setpassword(JSONObject args) {
	UserPtr target = userDb->getUser(args[L"username"]->String());
	if (target) {
		target->setPassword(args[L"password"]->String());

		if (eventDb) {
			EventParams params;
			params["username"] = args[L"username"]->String();
			eventDb->add(new Event(AUDIT_CONFIGURATION_USERDB_SETPASSWORD, params));
		}
	}

	userDb->save();
	return JSONObject();
}

JSONObject AuthDelegate::users_groups_add(JSONObject args) {
	UserPtr target = userDb->getUser(args[L"username"]->String());
	GroupPtr group = groupDb->getGroup(args[L"group"]->AsNumber());

	if (target && group) {
		target->addGroup(group);

		if (eventDb) {
			EventParams params;
			params["username"] = args[L"username"]->String();
			params["group"] = args[L"group"]->AsNumber();
			eventDb->add(new Event(AUDIT_CONFIGURATION_USERDB_GROUPS_ADD, params));
		}
	}

	userDb->save();
	return JSONObject();
}

JSONObject AuthDelegate::users_groups_del(JSONObject args) {
	UserPtr target = userDb->getUser(args[L"username"]->String());
	GroupPtr group = groupDb->getGroup(args[L"group"]->AsNumber());

	if (target && group) {
		target->removeGroup(group);

		if (eventDb) {
			EventParams params;
			params["username"] = args[L"username"]->String();
			params["group"] = args[L"group"]->AsNumber();
			eventDb->add(new Event(AUDIT_CONFIGURATION_USERDB_GROUPS_DEL, params));
		}
	}

	userDb->save();
	return JSONObject();
}

JSONObject AuthDelegate::users_disable(JSONObject args) {
	UserPtr user = userDb->getUser(args[L"username"]->String());
	if (user) {
		userDb->disableUser(user);

		if (eventDb) {
			EventParams params;
			params["username"] = args[L"username"]->String();
			eventDb->add(new Event(AUDIT_CONFIGURATION_USERDB_DISABLE, params));
		}
	}

	userDb->save();
	return JSONObject();
}

JSONObject AuthDelegate::users_enable(JSONObject args) {
	UserPtr user = userDb->getUser(args[L"username"]->String());
	if (user) {
		userDb->enableUser(user);

		if (eventDb) {
			EventParams params;
			params["username"] = args[L"username"]->String();
			eventDb->add(new Event(AUDIT_CONFIGURATION_USERDB_ENABLE, params));
		}
	}

	userDb->save();
	return JSONObject();
}

JSONObject AuthDelegate::sessions_networktimeouts(JSONObject args) {
	JSONObject ret;
	JSONArray timeouts;

	for(TimeoutConfigurationPtr timeout : arp->get_authentication_timeout_configurations()){
		JSONObject target;
		target.put(L"id", new JSONValue((string) timeout->id));
		target.put(L"timeout", new JSONValue((double) timeout->value));
		target.put(L"type", new JSONValue((double) timeout->type));
		target.put(L"user_device_limit", new JSONValue((double) timeout->sessions_limit));

		if(timeout->type == TIME){
			target.put(L"mon", new JSONValue((bool) timeout->mon));
			target.put(L"tues", new JSONValue((bool) timeout->tues));
			target.put(L"wed", new JSONValue((bool) timeout->wed));
			target.put(L"thur", new JSONValue((bool) timeout->thur));
			target.put(L"fri", new JSONValue((bool) timeout->fri));
			target.put(L"sat", new JSONValue((bool) timeout->sat));
			target.put(L"sun", new JSONValue((bool) timeout->sun));
		}
		JSONArray networks;	
		for(SFwallCore::AliasPtr network : timeout->networks){
			JSONObject obj;
			obj.put(L"id", new JSONValue((string) network->id));
			obj.put(L"name", new JSONValue((string) network->name));
			networks.push_back(new JSONValue(obj));
		}
		target.put(L"networks", new JSONValue(networks));

		JSONArray groups;
		for(GroupPtr group: timeout->groups){
			JSONObject obj;
			obj.put(L"id", new JSONValue((double) group->getId()));
			obj.put(L"name", new JSONValue((string) group->getName()));
			groups.push_back(new JSONValue(obj));
		}
		target.put(L"groups", new JSONValue(groups));

		timeouts.push_back(new JSONValue(target));
	}	
	ret.put(L"timeouts", new JSONValue(timeouts));
	return ret;
}

JSONObject AuthDelegate::sessions_networktimeouts_remove(JSONObject args) { 
	/*Find out entity by id*/
	TimeoutConfigurationPtr target;
	for(TimeoutConfigurationPtr timeout : arp->get_authentication_timeout_configurations()){
		if(timeout->id.compare(args[L"id"]->String()) == 0){
			target = timeout;	
			break;
		}
	}

	if(target){
		arp->remove_authentication_timeout_configuration(target);	
	}
	arp->save();
	return JSONObject();
}

JSONObject AuthDelegate::sessions_networktimeouts_manage(JSONObject args) {
	TimeoutConfigurationPtr target;
	/*Find out entity by id*/
	if(args.has(L"id")){
		for(TimeoutConfigurationPtr timeout : arp->get_authentication_timeout_configurations()){
			if(timeout->id.compare(args[L"id"]->String()) == 0){
				target = timeout;
				break;
			}
		}
	}else{
		target = TimeoutConfigurationPtr(new TimeoutConfiguration());
		target->id = StringUtils::genRandom();
		arp->add_authentication_timeout_configuration(target);
	}

	/*Now modify it*/
	if(target){
		target->networks.clear();
		target->groups.clear();
		target->value = args[L"timeout"]->AsNumber();		
		target->sessions_limit = args[L"user_device_limit"]->AsNumber();
		if(args.has(L"type")){
			target->type = args[L"type"]->AsNumber();
			if(target->type == TIME){
				target->mon = args[L"mon"]->AsBool();
				target->tues = args[L"tues"]->AsBool();
				target->wed= args[L"wed"]->AsBool();
				target->thur= args[L"thur"]->AsBool();
				target->fri= args[L"fri"]->AsBool();
				target->sat= args[L"sat"]->AsBool();
				target->sun= args[L"sun"]->AsBool();
			}

		}else{
			target->type = IDLE;
		}

		JSONArray networkIds = args[L"networks"]->AsArray();
		for(int x= 0; x < networkIds.size(); x++){
			SFwallCore::AliasPtr alias = firewall->aliases->get(networkIds[x]->String());
			if(alias){
				target->networks.push_back(alias);
			}
		}

		if(args.has(L"groups")){
			JSONArray groupIds = args[L"groups"]->AsArray();
			for(int x= 0; x < groupIds.size(); x++){
				GroupPtr group = System::getInstance()->getGroupDb()->getGroup((string) groupIds[x]->String());
				if(group){
					target->groups.push_back(group);
				}
			}

		}
		arp->save();
	}

	return JSONObject();
}

JSONObject AuthDelegate::sessions_group_list(JSONObject args) {
	JSONObject ret;
	JSONArray sessions;
	JSONArray persistedSessions;

	GroupPtr group = System::getInstance()->getGroupDb()->getGroup(args[L"groupname"]->String());
	if(group){
		for (HostPtr session : arp->get_all_entries(true)) {
			if(!session->authenticated_user){
				continue;
			}

			if(session->authenticated_user->checkForGroup(group)){
				JSONObject s;
				s.put(L"user", new JSONValue((string) session->authenticated_user->getUserName()));
				s.put(L"hw", new JSONValue((string) session->mac));
				s.put(L"loginTime", new JSONValue((double) session->authenticated_user_login_time));
				s.put(L"idle_timeout", new JSONValue((double) session->timeout__idle_timeout_value));
				s.put(L"absolute_timeout", new JSONValue((double) session->timeout__absolute_timeout_value));
				s.put(L"host", new JSONValue((string) session->getIp()));

				sessions.push_back(new JSONValue(s));
			}
		}

		ret.put(L"sessions", new JSONValue(sessions));
	}
	return ret;
}
JSONObject AuthDelegate::sessions_list(JSONObject args) {
	JSONObject ret;
	JSONArray sessions;
	JSONArray persistedSessions;

	for (HostPtr session : arp->get_all_entries(true)) {
		if(!session->authenticated_user){
			continue;
		}	

		JSONObject s;
		s.put(L"user", new JSONValue((string) session->authenticated_user->getUserName()));
		s.put(L"hw", new JSONValue((string) session->mac));
		s.put(L"authentication_provider", new JSONValue((string) session->authentication_provider));
		s.put(L"loginTime", new JSONValue((double) session->authenticated_user_login_time));
		s.put(L"idle_timeout", new JSONValue((double) session->timeout__idle_timeout_value));
		s.put(L"absolute_timeout", new JSONValue((double) session->timeout__absolute_timeout_value));
		s.put(L"host", new JSONValue((string) session->getIp()));

		sessions.push_back(new JSONValue(s));
	}

	for (PersistedSessionPtr session : arp->get_persisted_authentication_sessions()) {
		JSONObject s;
		s.put(L"user", new JSONValue((string) session->username));
		s.put(L"hw", new JSONValue((string) session->mac));
		persistedSessions.push_back(new JSONValue(s));
	}

	ret.put(L"sessions", new JSONValue(sessions));
	ret.put(L"persisted", new JSONValue(persistedSessions));

	return ret;
}

JSONObject AuthDelegate::sessions_persist(JSONObject args) {
	string username = args[L"username"]->String();
	string hw = args[L"hw"]->String();

	arp->add_persisted_authentication_session(hw, username);
	arp->save();
	return JSONObject();
}

JSONObject AuthDelegate::sessions_persist_remove(JSONObject args) {
	string username = args[L"username"]->String();
	string hw = args[L"hw"]->String();

	arp->remove_persisted_authentication_session(hw, username);
	arp->save();
	return JSONObject();
}

JSONObject AuthDelegate::sessions_persist_remove_by_group(JSONObject args) {
	string group_name = args[L"group"]->String();
	GroupPtr group_ptr = groupDb->getGroup(group_name);
	if (group_ptr) {
		std::list<PersistedSessionPtr> persisted_sessions = arp->get_persisted_authentication_sessions();
		vector <UserPtr> users = userDb->get_users_of_group(group_ptr);
		for (UserPtr user: users) {
			string name = user->getUserName();
			for (PersistedSessionPtr session: persisted_sessions) {
				if (!session->username.empty() && session->username.compare(name) == 0)	{
					arp->remove_persisted_authentication_session(session->mac, name);
				}
			}
		}
		arp->save();
	}
	return JSONObject();
}

JSONObject AuthDelegate::sessions_persist_remove_list(JSONObject args) {
	JSONArray sessions = args[L"sessions"]->AsArray();
	for(int x= 0; x < sessions.size(); x++){
		JSONObject session = sessions[x]->AsObject();
		string username = session[L"username"]->String();
		string hw = session[L"hw"]->String();
		arp->remove_persisted_authentication_session(hw, username);
	}
	arp->save();
	return JSONObject();
}

JSONObject AuthDelegate::sessions_create(JSONObject input) {
	string ipAddress = input[L"ipaddress"]->String();
	string username = input[L"username"]->String();
	int authenticate_result;

	/* Try to find a Host entry for this ip address */
	list<HostPtr> host_entries = arp->get_by_ip(IP4Addr::stringToIP4Addr(ipAddress));
	if(host_entries.size() == 0){
		if (eventDb != NULL) {
			EventParams params;
			params["user"] = username;
			params["ip"] = ipAddress;
			eventDb->add(new Event(USERDB_COULD_NOT_FIND_HOST, params));
		}

		throw DelegateGeneralException("Could not find mac address for given ip address");
	}

	UserPtr user = userDb->getUser(username, true);
	if (!user) {
		if(input.has(L"create_user") && input[L"create_user"]->AsBool()){
			user = userDb->createUser(username);
		}else{
			if (eventDb != NULL) {
				EventParams params;
				params["user"] = username;
				params["ip"] = ipAddress;
				eventDb->add(new Event(USERDB_LOGIN_FAILED, params));
			}

			throw DelegateGeneralException("Could not find user for the given username");
		}
	}

	for(HostPtr host_entry : host_entries){
		if (host_entry->authenticated_user) {
			if (host_entry->authenticated_user == user) {
				return JSONObject();
			}
			else {
				arp->deauthenticate_user_to_host(host_entry, user);	
			}
		}

		if(input.has(L"absoluteTimeout") && input.has(L"timeout")){
			authenticate_result = arp->authenticate_user_to_host(host_entry, user, "provider.api.createsession", input[L"timeout"]->AsNumber(), input[L"absoluteTimeout"]->AsBool());
		}else{
			authenticate_result = arp->authenticate_user_to_host(host_entry, user, "provider.api.createsession", -1, false);
		}
		if (authenticate_result == DEVICE_LIMIT_EXCEEDED) {
			throw DelegateGeneralException("You have reached the limit for concurrently logged in devices");
		}
	}

	return JSONObject();
}

JSONObject AuthDelegate::realm_login(JSONObject input) {
	JSONObject ret;

	string username = input[L"username"]->String();
	string password = input[L"password"]->String();
	string realm = input[L"realm"]->String();
	
	if(realm.compare("xl2tpd") == 0){
		string realm_key = input[L"realm-key"]->String();
		if(userDb->authenticateUser(username, password)){
			UserPtr user = userDb->getUser(username, true);
			//Find our vpn instance
			VpnManager* vpn_manager = System::getInstance()->getVpnManager();
			IPSecL2TPGateway* real_instance = vpn_manager->get_l2tp(); 
			if(real_instance){
				for(GroupPtr allowed : real_instance->allowed_users_groups){
					if(user->checkForGroup(allowed)){
						EventParams params;
						params["username"] = username;
						params["realm-key"] = realm_key;
						eventDb->add(new Event(VPN_L2TP_IPSEC_AUTHENTICATE_SUCCESS, params));

						ret.put(L"response", new JSONValue((double) 0));
						return ret;
					}
				}

				/*If we pass through here, then permissions were not granted*/
				EventParams params;
				params["username"] = username;
				params["realm-key"] = realm_key;
				eventDb->add(new Event(VPN_L2TP_IPSEC_AUTHENTICATE_BAD_PERMISSIONS, params));
			}
		}else{
			EventParams params;
			params["username"] = username;
			params["realm-key"] = realm_key;
			eventDb->add(new Event(VPN_L2TP_IPSEC_AUTHENTICATE_FAILURE, params));
		}
	}else if(realm.compare("sshd") == 0){
		if(userDb->authenticateUser(username, password)){
                        UserPtr user = userDb->getUser(username, true);
			for(GroupPtr group : user->getGroups()){
				if(group->isAllowMui()){
					ret.put(L"response", new JSONValue((double) 0));
					return ret;
				}
			}
		}
	}

	ret.put(L"response", new JSONValue((double) - 1));
	ret.put(L"message", new JSONValue((string) "Invalid username, password or realm permissions"));

	return ret;
}

JSONObject AuthDelegate::realm_join(JSONObject input) {
	string username = input[L"username"]->String();
	string realm = input[L"realm"]->String();
	string realm_key = input[L"realm-key"]->String();

	if(realm.compare("xl2tpd") == 0){
		IPSecL2TPGatewayInstanceClientPtr connection(new IPSecL2TPGatewayInstanceClient());
		connection->username = username;
		connection->local_ip = IP4Addr::ip4AddrToString(input[L"local_ip"]->AsNumber());
		connection->remote_ip = IP4Addr::ip4AddrToString(input[L"remote_ip"]->AsNumber());
		connection->ifname = input[L"device"]->String();

		VpnManager* vpn_manager = System::getInstance()->getVpnManager();
		IPSecL2TPGateway* real_instance = vpn_manager->get_l2tp();   
		if(real_instance){
			EventParams params;
			params["username"] = username;
			params["realm-key"] = realm_key;
			params["local_ip"] = connection->local_ip;
			params["remote_ip"] = connection->remote_ip;
			params["device"] = connection->ifname;
			eventDb->add(new Event(VPN_L2TP_IPSEC_JOIN, params));

			real_instance->active_clients.push_back(connection);
		}	
	}

	return JSONObject();
}

JSONObject AuthDelegate::realm_leave(JSONObject input) {
	string username = input[L"username"]->String();
	string realm = input[L"realm"]->String();
	string realm_key = input[L"realm-key"]->String();

	if(realm.compare("xl2tpd") == 0){
		VpnManager* vpn_manager = System::getInstance()->getVpnManager();
		IPSecL2TPGateway* real_instance = vpn_manager->get_l2tp();
		if(real_instance){
			list<IPSecL2TPGatewayInstanceClientPtr>::iterator iter;
			for(iter = real_instance->active_clients.begin(); iter != real_instance->active_clients.end(); iter++){
				IPSecL2TPGatewayInstanceClientPtr conn = (*iter);
				if(conn->username.compare(username) == 0 && conn->remote_ip.compare(IP4Addr::ip4AddrToString(input[L"remote_ip"]->AsNumber())) == 0){
					real_instance->active_clients.erase(iter);

					EventParams params; 
					params["username"] = username;
					params["realm-key"] = realm_key;
					params["local_ip"] = conn->local_ip;
					params["remote_ip"] = conn->remote_ip;
					params["device"] = conn->ifname;
					eventDb->add(new Event(VPN_L2TP_IPSEC_LEAVE, params));

					break;
				}
			}       
		}       
	}

	return JSONObject();
}

JSONObject AuthDelegate::login(JSONObject input) {
	string ipAddress = input[L"ipaddress"]->String();
	string username = input[L"username"]->String();
	string password;
	int authenticate_result;

	if (input.has(L"password")) {
		password = input[L"password"]->String();
	}

	JSONObject ret;
	list<HostPtr> host_entries = arp->get_by_ip(IP4Addr::stringToIP4Addr(ipAddress));
	if(host_entries.size() == 0){
		ret.put(L"response", new JSONValue((double) - 1));
		ret.put(L"message", new JSONValue((string) "Could not find mac address for ip"));

		if (eventDb != NULL) {
			EventParams params;
			params["user"] = username;
			params["ip"] = ipAddress;
			eventDb->add(new Event(USERDB_COULD_NOT_FIND_HOST, params));
		}
		return ret;
	}

	if(!userDb->authenticateUser(username, password)){
		ret.put(L"response", new JSONValue((double) - 1));
		ret.put(L"message", new JSONValue((string) "Invalid username or password"));

		if (eventDb != NULL) {
			EventParams params;
			params["user"] = username;
			params["ip"] = ipAddress;
			eventDb->add(new Event(USERDB_LOGIN_FAILED, params));
		}
		return ret;
	}else{
		UserPtr user = userDb->getUser(username, true);
		for(HostPtr host_entry : host_entries){
			if(input.has(L"absoluteTimeout") && input.has(L"timeout")){
				authenticate_result = arp->authenticate_user_to_host(host_entry, user, "provider.api.login",input[L"timeout"]->AsNumber(), input[L"absoluteTimeout"]->AsBool());
			}else{
				authenticate_result = arp->authenticate_user_to_host(host_entry, user,"provider.api.login", -1, false);
			}

			if (authenticate_result == DEVICE_LIMIT_EXCEEDED) {
				ret.put(L"response", new JSONValue((double) - 1));
				ret.put(L"message", new JSONValue((string) "You have reached the limit for concurrently logged in devices"));
				return ret;
			}

			if (input.has(L"persist") && input[L"persist"]->AsBool()) {
				arp->persist_authentication_session(host_entry, user);
			}
		}
		ret.put(L"response", new JSONValue((double) 0));
	}

	return ret;
}

JSONObject AuthDelegate::logout(JSONObject input) {
	JSONObject ret;
	string ipAddress = input[L"ipaddress"]->String();

	list<HostPtr> sessions = arp->get_by_ip(IP4Addr::stringToIP4Addr(ipAddress));
	if (sessions.size() > 0) {
		ret.put(L"response", new JSONValue((double) 0));

		for(HostPtr session : sessions){
			if(session->authenticated_user){
				if (eventDb) {
					EventParams params;
					params["user"] = session->authenticated_user->getUserName();
					params["ipaddress"] = ipAddress;
					params["hw"] = session->mac;
					eventDb->add(new Event(USERDB_LOGOUT, params));
				}

				arp->deauthenticate_user_to_host(session, session->authenticated_user);
			}
		}
	}
	else {
		ret.put(L"response", new JSONValue((double) - 1));
		ret.put(L"message", new JSONValue((string) "Could not find session"));
	}

	return ret;
}
