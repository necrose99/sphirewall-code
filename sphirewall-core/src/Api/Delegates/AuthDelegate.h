/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef AuthDelegate_h
#define AuthDelegate_h

#include "Api/Delegate.h"
#include "Auth/Group.h"

class AuthDelegate : public virtual Delegate {
	public:
		AuthDelegate(GroupDb *groupDb, UserDb *userDb, SFwallCore::Firewall *firewall, HostDiscoveryService *arp);
		JSONObject process(std::string uri, JSONObject object);
		std::string rexpression();
		void setEventDb(EventDb *eventDb);

	private:
		GroupDb *groupDb;
		UserDb *userDb;
		SFwallCore::Firewall *firewall;
		HostDiscoveryService *arp;
		AuthenticationManager *authManager;
		EventDb *eventDb;

		JSONObject serialize_group(GroupPtr target, bool include_members);

		_BEGIN_SPHIREWALL_DELEGATES(AuthDelegate)
		DelegateHandlerImpl groups_list;
		DelegateHandlerImpl groups_list_with_members;
		DelegateHandlerImpl groups_create;
		DelegateHandlerImpl groups_create_bulk;
		DelegateHandlerImpl groups_get;
		DelegateHandlerImpl groups_del;
		DelegateHandlerImpl groups_save;
		DelegateHandlerImpl groups_mergeoveruser;
		DelegateHandlerImpl users_list;
		DelegateHandlerImpl users_get;
		DelegateHandlerImpl users_add;
		DelegateHandlerImpl users_del;
		DelegateHandlerImpl users_save;
		DelegateHandlerImpl users_groups_add;
		DelegateHandlerImpl users_groups_del;
		DelegateHandlerImpl users_enable;
		DelegateHandlerImpl users_disable;
		DelegateHandlerImpl user_merge;
		DelegateHandlerImpl user_setpassword;
		DelegateHandlerImpl sessions_create;
		DelegateHandlerImpl sessions_list;
		DelegateHandlerImpl sessions_persist;
		DelegateHandlerImpl login;
		DelegateHandlerImpl logout;
		DelegateHandlerImpl defaultpasswordset;
		DelegateHandlerImpl ldap;
		DelegateHandlerImpl ldap_sync;
		DelegateHandlerImpl wmic;
		DelegateHandlerImpl wmic_add;
		DelegateHandlerImpl wmic_del;
		DelegateHandlerImpl wmic_reset_cursor;
		DelegateHandlerImpl sessions_persist_remove;
		DelegateHandlerImpl sessions_persist_remove_by_group;
		DelegateHandlerImpl sessions_persist_remove_list;

		DelegateHandlerImpl sessions_networktimeouts;
		DelegateHandlerImpl sessions_networktimeouts_remove;
		DelegateHandlerImpl sessions_networktimeouts_manage;

		DelegateHandlerImpl realm_join;
		DelegateHandlerImpl realm_leave;
		DelegateHandlerImpl realm_login;
		DelegateHandlerImpl sessions_group_list;
		_END_SPHIREWALL_DELEGATES
};

#endif
