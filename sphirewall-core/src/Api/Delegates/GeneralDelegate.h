/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef GeneralDelegate_H
#define GeneralDelegate_H

#include "Api/Delegate.h"

class GeneralDelegate : public virtual Delegate {
	public:

		GeneralDelegate(Config *config, SysMonitor *sysMonitor, LoggingConfiguration *loggingConfiguration);
		GeneralDelegate(Config *config);

		JSONObject process(std::string uri, JSONObject object);

		std::string rexpression();

		void setEventDb(EventDb *eventDb);
	private:
		Config *config;
		SysMonitor *sysMonitor;
		LoggingConfiguration *loggingConfiguration;
		EventDb *eventDb;

		void installHandlers();

		_BEGIN_SPHIREWALL_DELEGATES(GeneralDelegate)
		DelegateHandlerImpl config_dump;
		DelegateHandlerImpl config_get_version;
		DelegateHandlerImpl config_load;
		DelegateHandlerImpl config_get;
		DelegateHandlerImpl config_set;
		DelegateHandlerImpl config_set_bulk;
		DelegateHandlerImpl runtime_list;
		DelegateHandlerImpl runtime_get;
		DelegateHandlerImpl runtime_set;
		DelegateHandlerImpl metrics;
		DelegateHandlerImpl metrics_spec;
		DelegateHandlerImpl version;
		DelegateHandlerImpl logging_list;
		DelegateHandlerImpl logging_set;
		DelegateHandlerImpl logging_remove;
		DelegateHandlerImpl logging_critical;

		DelegateHandlerImpl ldap_testconnection;
		DelegateHandlerImpl smtp_testconnection;
		DelegateHandlerImpl smtp_publish;
		DelegateHandlerImpl cloud_connected;
		DelegateHandlerImpl rpc;
		DelegateHandlerImpl quotas;
		DelegateHandlerImpl quotas_set;
		DelegateHandlerImpl logs;
		DelegateHandlerImpl info;
		DelegateHandlerImpl install_watchdog;
		DelegateHandlerImpl config_create_snapshot;

		DelegateHandlerImpl sphireos_version;
		DelegateHandlerImpl sphireos_update_start;
		DelegateHandlerImpl sphireos_update_status;
		DelegateHandlerImpl sphireos_restart;
		
		_END_SPHIREWALL_DELEGATES
};

#endif
