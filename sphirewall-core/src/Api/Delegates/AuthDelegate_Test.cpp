/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>

#include <gtest/gtest.h>
#include "Json/JSON.h"
#include "Json/JSONValue.h"
#include "Api/Delegates/AuthDelegate.h"
#include "Auth/User.h"
#include "Auth/Group.h"
#include "Auth/GroupDb.h"
#include "Core/ConfigurationManager.h"
#include "Core/HostDiscoveryService.h"
#include "SFwallCore/TestFactory.h"
#include "test.h"

using namespace std;

TEST(AuthDelegate, base) {
	EXPECT_TRUE(true);
}

/*
TEST(AuthDelegate, auth_login) {
	MockFactory *tester = new MockFactory();
	GroupDb *groupDb = new GroupDb();
	UserDb *userDb = new UserDb(NULL, groupDb);
	HostDiscoveryService *arp = new HostDiscoveryService();
	arp->setConfigurationManager(new ConfigurationManager());

        SFwallCore::Packet *packet = SFwallCore::PacketBuilder::createTcp("10.1.1.1", "10.1.1.8", 80, 12333);
        packet->setHw("aa:bb:cc:dd:ee:ff");
        arp->update(packet);

	AuthDelegate *del = new AuthDelegate(groupDb, userDb, NULL, arp);

	UserPtr user = userDb->createUser("michael");
	user->setPassword("1234");

	JSONObject args;
	args.put(L"username", new JSONValue((string) "michael"));
	args.put(L"password", new JSONValue((string) "1234"));
	args.put(L"ipaddress", new JSONValue((string) "10.1.1.1"));

	JSONObject ret = del->process("auth/login", args);
	EXPECT_TRUE(ret[L"response"]->AsNumber() == 0);

	if (ret[L"response"]->AsNumber() != 0) cerr << "Reason: " << ret[L"message"]->String() << endl;
}

TEST(AuthDelegate, auth_login_withoutpassword) {
	MockFactory *tester = new MockFactory();

	GroupDb *groupDb = new GroupDb();
	UserDb *userDb = new UserDb(NULL, groupDb);
	HostDiscoveryService *arp = new HostDiscoveryService();
	arp->setConfigurationManager(new ConfigurationManager());

        SFwallCore::Packet *packet = SFwallCore::PacketBuilder::createTcp("10.1.1.1", "10.1.1.8", 80, 12333);
        packet->setHw("aa:bb:cc:dd:ee:ff");
        arp->update(packet);

	AuthDelegate *del = new AuthDelegate(groupDb, userDb, NULL, arp);
	userDb->createUser("michael");

	JSONObject args;
	args.put(L"username", new JSONValue((string) "michael"));
	args.put(L"ipaddress", new JSONValue((string) "10.1.1.1"));

	JSONObject ret = del->process("auth/login", args);
	EXPECT_TRUE(ret[L"response"]->AsNumber() < 0);
}

TEST(AuthDelegate, auth_login_invalid_user) {
	MockFactory *tester = new MockFactory();
	GroupDb *groupDb = new GroupDb();
	UserDb *userDb = new UserDb(NULL, groupDb);
	HostDiscoveryService *arp = new HostDiscoveryService();
	arp->setConfigurationManager(new ConfigurationManager());

        SFwallCore::Packet *packet = SFwallCore::PacketBuilder::createTcp("10.1.1.1", "10.1.1.8", 80, 12333);
        packet->setHw("aa:bb:cc:dd:ee:ff");
        arp->update(packet);

	AuthDelegate *del = new AuthDelegate(groupDb, userDb, NULL, arp);

	JSONObject args;
	args.put(L"username", new JSONValue((string) "michael"));
	args.put(L"password", new JSONValue((string) "1234"));
	args.put(L"ipaddress", new JSONValue((string) "10.1.1.1"));

	JSONObject ret = del->process("auth/login", args);
	EXPECT_TRUE(ret[L"response"]->AsNumber() == -1);
}

TEST(AuthDelegate, auth_login_invalid_password) {
	MockFactory *tester = new MockFactory();

	GroupDb *groupDb = new GroupDb();
	UserDb *userDb = new UserDb(NULL, groupDb);
	HostDiscoveryService *arp = new HostDiscoveryService();
	arp->setConfigurationManager(new ConfigurationManager());

        SFwallCore::Packet *packet = SFwallCore::PacketBuilder::createTcp("10.1.1.1", "10.1.1.8", 80, 12333);
        packet->setHw("aa:bb:cc:dd:ee:ff");
        arp->update(packet);

	AuthDelegate *del = new AuthDelegate(groupDb, userDb, NULL, arp);

	UserPtr user = userDb->createUser("michael");
	user->setPassword("proper_password");

	JSONObject args;
	args.put(L"username", new JSONValue((string) "michael"));
	args.put(L"password", new JSONValue((string) "1234"));
	args.put(L"ipaddress", new JSONValue((string) "10.1.1.1"));

	JSONObject ret = del->process("auth/login", args);
	EXPECT_TRUE(ret[L"response"]->AsNumber() == -1);
}

TEST(AuthDelegate, auth_login_invalid_macaddress) {
	MockFactory *tester = new MockFactory();
	GroupDb *groupDb = new GroupDb();
	UserDb *userDb = new UserDb(NULL, groupDb);
	HostDiscoveryService *arp = new HostDiscoveryService();
	arp->setConfigurationManager(new ConfigurationManager());

	UserPtr user = userDb->createUser("michael");
	user->setPassword("1234");
        AuthDelegate *del = new AuthDelegate(groupDb, userDb, NULL, arp);

	JSONObject args;
	args.put(L"username", new JSONValue((string) "michael"));
	args.put(L"password", new JSONValue((string) "1234"));
	args.put(L"ipaddress", new JSONValue((string) "1.1.1.2"));

	JSONObject ret = del->process("auth/login", args);
	EXPECT_TRUE(ret[L"response"]->AsNumber() == -1);
}

*/
