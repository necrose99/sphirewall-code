/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <boost/regex.hpp>
#include <sstream>

#include "Core/System.h"
#include "Utils/LinuxUtils.h"
#include "Utils/FileUtils.h"
#include "Api/Exceptions.h"
#include "Api/Delegates/GeneralDelegate.h"
#include "Json/JSONValue.h"
#include "Json/JSON.h"
#include "Core/Config.h"
#include "Core/SysMonitor.h"
#include "Core/Cloud.h"
#include "Core/Event.h"
#include "Core/QuotaManager.h"
#include "Core/HostDiscoveryService.h"
#include "SFwallCore/ConnTracker.h"
#include "Utils/SphireOsUpdateHelper.h"

#define installHandler(x,y) delegateMap.insert(DelegatePath(x, &GeneralDelegate::y))

using namespace std;

void GeneralDelegate::installHandlers() {
	installHandler("general/config/get", config_get);
	installHandler("general/config/set", config_set);
	installHandler("general/config/set/bulk", config_set_bulk);
	installHandler("general/runtime/list", runtime_list);
	installHandler("general/runtime/get", runtime_get);
	installHandler("general/runtime/set", runtime_set);
	installHandler("general/version", version);
	installHandler("general/logging/list", logging_list);
	installHandler("general/logging/set", logging_set);
	installHandler("general/logging/remove", logging_remove);
	installHandler("general/logging/critical", logging_critical);
	installHandler("general/ldap/testconnection", ldap_testconnection);
	installHandler("general/config", config_dump);
	installHandler("general/config/snapshot/local", config_create_snapshot);
	installHandler("general/config/load", config_load);
	installHandler("general/cloud/connected", cloud_connected);
	installHandler("general/quotas", quotas);
	installHandler("general/quotas/set", quotas_set);
	installHandler("general/logs", logs);
	installHandler("general/info", info);

	//This returns a list of all available metrics
	installHandler("general/metrics", metrics);
	//This queries for a type 
	installHandler("general/metrics/spec", metrics_spec);
	installHandler("general/installwatchdog", install_watchdog);

	installHandler("general/sphireos/version", sphireos_version);
	installHandler("general/sphireos/update/start", sphireos_update_start);
	installHandler("general/sphireos/update/status", sphireos_update_status);
	installHandler("general/sphireos/restart", sphireos_restart);
}

GeneralDelegate::GeneralDelegate(Config *config)
	: config(config), eventDb(NULL) {
	installHandlers();
}

GeneralDelegate::GeneralDelegate(Config *config, SysMonitor *sysMonitor, LoggingConfiguration *loggingConfiguration)
	: config(config), sysMonitor(sysMonitor), loggingConfiguration(loggingConfiguration), eventDb(NULL) {
	installHandlers();
}

string GeneralDelegate::rexpression() {
	return "general/(.*)";
}

void GeneralDelegate::setEventDb(EventDb *eventDb) {
	this->eventDb = eventDb;
}

JSONObject GeneralDelegate::process(string uri, JSONObject object) {
	try {
		return invokeHandler(uri, object);
	}
	catch (std::out_of_range e) {
		throw DelegateNotFoundException(uri);
	}
}

JSONObject GeneralDelegate::sphireos_version(JSONObject obj) {
	JSONObject ret;
	string line;
	ifstream version_file("/version");
	if (version_file.is_open())
	{
		getline (version_file,line);
		version_file.close();
	}

	ret.put(L"version", new JSONValue((string) line));
	return ret;	
}

JSONObject GeneralDelegate::sphireos_update_start(JSONObject obj) {
	System::getInstance()->get_sphireos_update_helper()->start_update();
	return JSONObject();
}

JSONObject GeneralDelegate::sphireos_update_status(JSONObject obj) {
	JSONObject ret;
	ret.put(L"status", new JSONValue((double) System::getInstance()->get_sphireos_update_helper()->status()));
	ret.put(L"log", new JSONValue((string) System::getInstance()->get_sphireos_update_helper()->get_log()));
        return ret; 
}

JSONObject GeneralDelegate::sphireos_restart(JSONObject obj){
	system("init 6");
	return JSONObject();
}	

JSONObject GeneralDelegate::metrics_spec(JSONObject obj) {
	JSONObject ret;
	JSONArray ret_individual_points;
	InMemoryMetricsStore* store = System::getInstance()->get_metric_store();

	std::string key = obj[L"metric"]->String();
	list<InMemoryMetricsStorePointPtr> points = store->query(key);
	for(InMemoryMetricsStorePointPtr point : points){
		JSONObject target;
		target.put(L"time", new JSONValue((double) point->timestamp));
		target.put(L"value", new JSONValue((double) point->value));
		ret_individual_points.push_back(new JSONValue(target));	
	}	

	ret.put(L"points", new JSONValue(ret_individual_points));
	return ret;
}

JSONObject GeneralDelegate::metrics(JSONObject obj) {
	JSONObject ret;
	JSONArray ret_individual_points;
	InMemoryMetricsStore* store = System::getInstance()->get_metric_store();

	list<string> points = store->available_types();
	for(string point : points){
		ret_individual_points.push_back(new JSONValue((std::string) point));	
	}	

	ret.put(L"points", new JSONValue(ret_individual_points));
	return ret;
}

JSONObject GeneralDelegate::info(JSONObject obj) {
	JSONObject ret;
	ret.put(L"version", new JSONValue((string) System::getInstance()->version));
	ret.put(L"versionName", new JSONValue((string) System::getInstance()->versionName));
	ret.put(L"configurationVersion", new JSONValue((double) System::getInstance()->configurationManager.configuration_version));

	ret.put(L"no_active_devices", new JSONValue((double) System::getInstance()->getArp()->size()));
	ret.put(L"no_authenticated_users", new JSONValue((double) System::getInstance()->getArp()->get_all_entries(true).size()));
	ret.put(L"no_connections", new JSONValue((double) System::getInstance()->getFirewall()->connectionTracker->size()));

	return ret;
}

JSONObject GeneralDelegate::install_watchdog(JSONObject obj) {
	JSONObject ret;
	FileUtils::write("/etc/linewize_watchdog_id", obj[L"id"]->String(), true);	
	system("export DEBIAN_FRONTEND=noninteractive && export PATH=/usr/bin:/bin:/usr/sbin:/sbin && apt-get -yf --force-yes update");
	system("export DEBIAN_FRONTEND=noninteractive && export PATH=/usr/bin:/bin:/usr/sbin:/sbin && apt-get -yf --force-yes install linewize-watchdog");
	return ret;
}


JSONObject GeneralDelegate::logs(JSONObject obj) {
	int no_lines = 100;
	stringstream cmd;
	cmd << "cat /var/log/syslog";

	if (obj.has(L"filter")) {
		string filter = obj[L"filter"]->String();

		if (filter.size() > 0) {
			cmd << " | grep " << filter;
		}
	}

	if(obj.has(L"no_lines")){
		no_lines = obj[L"no_lines"]->AsNumber();
	}

	cmd << " | tail -n " << no_lines;
	string response;
	LinuxUtils::exec(cmd.str(), response);

	JSONObject ret;
	ret.put(L"log", new JSONValue((string) response));
	return ret;
}

JSONObject GeneralDelegate::quotas(JSONObject obj) {
	JSONObject ret;
	QuotaManager *watch = System::getInstance()->getQuotaManager();

	ret.put(L"dailyQuota", new JSONValue((bool) watch->getQuota()->dailyQuota));
	ret.put(L"dailyQuotaLimit", new JSONValue((double) watch->getQuota()->dailyQuotaLimit));

	ret.put(L"weeklyQuota", new JSONValue((bool) watch->getQuota()->weeklyQuota));
	ret.put(L"weeklyQuotaLimit", new JSONValue((double) watch->getQuota()->weeklyQuotaLimit));

	ret.put(L"monthQuota", new JSONValue((bool) watch->getQuota()->monthQuota));
	ret.put(L"monthQuotaLimit", new JSONValue((double) watch->getQuota()->monthQuotaLimit));

	return ret;
}

JSONObject GeneralDelegate::quotas_set(JSONObject args) {
	QuotaManager *watch = System::getInstance()->getQuotaManager();
	QuotaInfo *quota = watch->getQuota();
	quota->dailyQuota = args[L"dailyQuota"]->AsBool();
	quota->dailyQuotaLimit = args[L"dailyQuotaLimit"]->AsNumber();
	quota->weeklyQuota = args[L"weeklyQuota"]->AsBool();
	quota->weeklyQuotaLimit = args[L"weeklyQuotaLimit"]->AsNumber();
	quota->monthQuota = args[L"monthQuota"]->AsBool();
	quota->monthQuotaLimit = args[L"monthQuotaLimit"]->AsNumber();

	watch->save();
	return JSONObject();
}

JSONObject GeneralDelegate::cloud_connected(JSONObject obj) {
	JSONObject ret;
	ret.put(L"status", new JSONValue((bool) System::getInstance()->getCloudConnection()->connected()));
	return ret;
}

JSONObject GeneralDelegate::config_dump(JSONObject obj) {
	JSONObject ret;
	config->getConfigurationManager()->holdLock();
	ret.put(L"dump", new JSONValue((string) config->getConfigurationManager()->dump()));
	config->getConfigurationManager()->releaseLock();
	return ret;
}

JSONObject GeneralDelegate::config_load(JSONObject obj) {
	ConfigurationManager* cm = config->getConfigurationManager();
	string config = obj[L"snapshot"]->String();
	cm->loadFromString(obj[L"snapshot"]->String());	
	return JSONObject();
}

JSONObject GeneralDelegate::config_create_snapshot(JSONObject obj) {
	config->getConfigurationManager()->holdLock();
	std::string snapshot = config->getConfigurationManager()->dump();
	if(snapshot.size() > 1){
		ofstream fp;
		fp.open("/etc/sphirewall.conf.snapshot.latest");
		fp << "# sphirewall.conf.snapshot.latest -- Generated by Sphirewall for the Core Sphirewall Daemon\n";
		fp << "# For support, see http://sphirewall.net\n";

		fp << snapshot;
		fp.close();
	}

	config->getConfigurationManager()->releaseLock();
	return JSONObject();
}

JSONObject GeneralDelegate::config_get(JSONObject obj) {
	JSONObject ret;

	if (config->getConfigurationManager()->hasByPath(obj[L"key"]->String())) {
		ObjectWrapper *o = config->getConfigurationManager()->get(obj[L"key"]->String());

		if (o->isBoolean()) {
			ret.put(L"value", new JSONValue((bool) o->boolean()));
		}
		else if (o->isNumber()) {
			ret.put(L"value", new JSONValue((double) o->number()));
		}
		else if (o->isString()) {
			ret.put(L"value", new JSONValue((string) o->string()));
		}
		else {
			if (obj.has(L"hideExceptions") && obj[L"hideExceptions"]->AsBool() == true) {
				ret.put(L"value", new JSONValue((string) ""));
			}
			else {
				throw DelegateGeneralException("Variable type is not defined, this is very strange");
			}
		}
	}
	else {
		if (obj.has(L"hideExceptions") && obj[L"hideExceptions"]->AsBool() == true) {
			ret.put(L"value", new JSONValue((string) ""));
		}
		else {
			throw DelegateGeneralException("Variable not found in configuration");
		}
	}

	return ret;
}

JSONObject GeneralDelegate::config_set_bulk(JSONObject obj) {
	JSONArray &values = obj[L"values"]->AsArray();

	for (JSONValue * jsonValue : values) {
		config_set(jsonValue->AsObject());
	}

	return JSONObject();
}

JSONObject GeneralDelegate::config_set(JSONObject obj) {
	string key = WStringToString(obj[L"key"]->AsString());

	if (obj[L"value"]->IsString()) {
		if (eventDb) {
			if (config->getConfigurationManager()->hasByPath(key)) {
				string oldvalue = config->getConfigurationManager()->get(key)->string();

				if (oldvalue.compare(obj[L"value"]->String()) != 0) {
					EventParams params;
					params["key"] = obj[L"key"]->String();
					params["value"] = obj[L"value"]->String();
					eventDb->add(new Event(AUDIT_CONFIGURATION_CHANGED, params));
				}
			}
		}

		config->getConfigurationManager()->put(key, obj[L"value"]->String());
	}
	else if (obj[L"value"]->IsBool()) {
		if (eventDb) {
			if (config->getConfigurationManager()->hasByPath(key)) {
				if (config->getConfigurationManager()->get(key)->boolean() != obj[L"value"]->AsBool()) {
					EventParams params;
					params["key"] = obj[L"key"]->String();
					params["value"] = obj[L"value"]->AsBool();
					eventDb->add(new Event(AUDIT_CONFIGURATION_CHANGED, params));
				}
			}
		}

		config->getConfigurationManager()->put(key, obj[L"value"]->AsBool());
	}
	else if (obj[L"value"]->IsNumber()) {
		if (eventDb) {
			if (config->getConfigurationManager()->hasByPath(key)) {
				if (config->getConfigurationManager()->get(key)->number() != obj[L"value"]->AsNumber()) {

					EventParams params;
					params["key"] = obj[L"key"]->String();
					params["value"] = obj[L"value"]->AsNumber();
					eventDb->add(new Event(AUDIT_CONFIGURATION_CHANGED, params));
				}
			}
		}

		config->getConfigurationManager()->put(key, obj[L"value"]->AsNumber());
	}

	config->getConfigurationManager()->save();
	return JSONObject();
}

JSONObject GeneralDelegate::runtime_list(JSONObject object) {
	list<string> keys;
	config->getRuntime()->listAll(keys);

	JSONArray array;
	list<string>::iterator iter;

	for (iter = keys.begin(); iter != keys.end(); iter++) {
		JSONObject v;
		v.put(L"key", new JSONValue((string) *iter));
		v.put(L"value", new JSONValue((double) config->getRuntime()->get(*iter)));
		array.push_back(new JSONValue((v)));
	}

	JSONObject ret;
	ret.put(L"keys", new JSONValue(array));

	return ret;
}

JSONObject GeneralDelegate::runtime_get(JSONObject object) {
	string key = object[L"key"]->String();

	if (config->getRuntime()->contains(key)) {
		int value = config->getRuntime()->get(key);

		JSONObject ret;
		ret.put(L"value", new JSONValue((double) value));
		return ret;
	}
	else {
		throw DelegateGeneralException("invalid runtime variable supplied");
	}
}

JSONObject GeneralDelegate::runtime_set(JSONObject object) {
	string key = object[L"key"]->String();
	int value = object[L"value"]->AsNumber();

	if (eventDb) {
		if (config->getRuntime()->get(key) != value) {

			EventParams params;
			params["key"] = object[L"key"]->String();
			params["value"] = object[L"value"]->AsNumber();
			eventDb->add(new Event(AUDIT_CONFIGURATION_CHANGED, params));
		}
	}

	config->getRuntime()->set(key, value);
	return JSONObject();
}

JSONObject GeneralDelegate::version(JSONObject obj) {
	JSONObject ret;
	ret.put(L"version", new JSONValue((string) System::getInstance()->version));
	ret.put(L"name", new JSONValue((string) System::getInstance()->versionName));

	return ret;
}

JSONObject GeneralDelegate::logging_list(JSONObject obj) {
	JSONObject ret;
	JSONArray arr;

	std::list<string> contexts = loggingConfiguration->getContexts();

	for (list<string>::iterator iter = contexts.begin();
			iter != contexts.end();
			iter++) {

		JSONObject o;
		o.put(L"context", new JSONValue((string) * iter));
		o.put(L"level", new JSONValue((double) loggingConfiguration->getLevel(*iter)));
		arr.push_back(new JSONValue(o));
	}

	ret.put(L"levels", new JSONValue(arr));
	return ret;
}

JSONObject GeneralDelegate::logging_set(JSONObject obj) {
	loggingConfiguration->setLevel(obj[L"context"]->String(), obj[L"level"]->AsNumber());
	return JSONObject();
}

JSONObject GeneralDelegate::logging_remove(JSONObject obj) {
	loggingConfiguration->rmLevel(obj[L"context"]->String());
	return JSONObject();
}

JSONObject GeneralDelegate::logging_critical(JSONObject obj) {
	if (obj.has(L"critical")) {
		Logger::instance()->criticalPathEnabled = obj[L"critical"]->AsBool();
		loggingConfiguration->save();
	}

	JSONObject ret;
	ret.put(L"critical", new JSONValue((bool) Logger::instance()->criticalPathEnabled));
	return ret;
}

JSONObject GeneralDelegate::ldap_testconnection(JSONObject o) {
	JSONObject ret;
	/*AuthenticationMethod *method = authenticationManager->getMethod(LDAP_DB);

	  if (method) {
	  bool canConnect = method->canConnect();
	  ret.put(L"status", new JSONValue((bool) canConnect));
	  }
	  else {
	  ret.put(L"status", new JSONValue((bool) false));
	  }*/

	return ret;
}


