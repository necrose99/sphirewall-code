/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef IdsDelegate_h
#define IdsDelegate_h

#include "Api/Delegate.h"

class IdsDelegate : public virtual Delegate {
	public:
		IdsDelegate(IDS *ids);
		JSONObject process(std::string uri, JSONObject object);
		std::string rexpression();

		void setEventDb(EventDb *eventDb);
	private:
		IDS *ids;
		EventDb *eventDb;

		_BEGIN_SPHIREWALL_DELEGATES(IdsDelegate)
		DelegateHandlerImpl exceptions_list;
		DelegateHandlerImpl exceptions_add;
		DelegateHandlerImpl exceptions_del;

		DelegateHandlerImpl config;
		DelegateHandlerImpl config_set;
		DelegateHandlerImpl start;
		DelegateHandlerImpl stop;
		DelegateHandlerImpl status;

		_END_SPHIREWALL_DELEGATES
};

#endif
