/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <arpa/inet.h>

using namespace std;

#include "Core/Event.h"
#include "Core/System.h"
#include "Json/JSON.h"
#include "Utils/IP6Addr.h"
#include "SFwallCore/State.h"
#include "SFwallCore/Connection.h"
#include "SFwallCore/ConnTracker.h"
#include "Api/Delegates/FirewallDelegate.h"
#include "Api/Exceptions.h"
#include "SFwallCore/NatAcls.h"
#include "SFwallCore/Criteria.h"
#include "SFwallCore/ApplicationLevel/GoogleSafeSearchEnforcer.h"
#include "SFwallCore/ApplicationLevel/CapturePortal.h"

#define installHandler(x,y) delegateMap.insert(DelegatePath(x, &FirewallDelegate::y))

using namespace SFwallCore;


FirewallDelegate::FirewallDelegate(Firewall *firewall, IntMgr* interfaceManager, GroupDb *groupDb)
	: firewall(firewall), interfaceManager(interfaceManager), groupDb(groupDb), httpApplicationFilter(NULL), eventDb(NULL) {
	installHandler("firewall/aliases/list", aliases_list);
	installHandler("firewall/aliases/add", aliases_add);
	installHandler("firewall/aliases/add/bulk", aliases_add_bulk);
	installHandler("firewall/aliases/del", aliases_del);
	installHandler("firewall/aliases/del/all", aliases_del_all);
	installHandler("firewall/aliases/del/bulk", aliases_del_bulk);
	installHandler("firewall/aliases/truncate", aliases_truncate);
	installHandler("firewall/aliases/alias/search", aliases_alias_search);
	installHandler("firewall/aliases/alias/list", aliases_alias_list);
	installHandler("firewall/aliases/alias/add", aliases_alias_add);
	installHandler("firewall/aliases/alias/del", aliases_alias_del);
	installHandler("firewall/aliases/get", aliases_get);
	installHandler("firewall/aliases/load", aliases_load);
	installHandler("firewall/tracker/list", tracker_list);
	installHandler("firewall/tracker/details", tracker_stats);
	installHandler("firewall/tracker/terminate", tracker_terminate);
        installHandler("firewall/tracker/size", tracker_size);
        installHandler("firewall/tracker/connection/details", tracker_connection_details);

	installHandler("firewall/acls/filter/up", acls_filter_up);
	installHandler("firewall/acls/filter/down", acls_filter_down);
	installHandler("firewall/acls/filter/delete", acls_filter_delete);
	installHandler("firewall/acls/nat/delete", acls_nat_delete);
	installHandler("firewall/acls/list", acls_list);
	installHandler("firewall/acls/add", acls_add);
	installHandler("firewall/acls/add/bulk", acls_bulk_add_set);
	installHandler("firewall/acls/list/trafficshaper", acls_list_trafficshaper);
	installHandler("firewall/acls/add/trafficshaper", acls_add_trafficshaper);
	installHandler("firewall/acls/del/trafficshaper", acls_list_del_trafficshaper);
	installHandler("firewall/acls/enable", acls_enable);
	installHandler("firewall/acls/disable", acls_disable);
	installHandler("firewall/webfilter/rules/list", acls_webfilter_rules_list);
	installHandler("firewall/webfilter/rules/deleteall", acls_webfilter_rules_deleteall);
	installHandler("firewall/webfilter/rules/add", acls_webfilter_rules_add);
	installHandler("firewall/webfilter/rules/add/bulk", acls_webfilter_rules_add_bulk);
	installHandler("firewall/webfilter/rules/remove", acls_webfilter_rules_remove);
	installHandler("firewall/webfilter/rules/moveup", acls_webfilter_rules_moveup);
	installHandler("firewall/webfilter/rules/movedown", acls_webfilter_rules_movedown);
	installHandler("firewall/webfilter/rules/enable", acls_webfilter_rules_enable);
	installHandler("firewall/webfilter/rules/disable", acls_webfilter_rules_disable);
	installHandler("firewall/acls/groups/list", acls_groups_list);
	installHandler("firewall/acls/groups/add", acls_groups_add);
	installHandler("firewall/periods/list", periods_list);
	installHandler("firewall/periods/add", periods_add);
	installHandler("firewall/periods/delete", periods_delete);

	installHandler("firewall/acls/forwarding", acls_forwarding);
	installHandler("firewall/acls/forwarding/enable", acls_forwarding_enable);
	installHandler("firewall/acls/forwarding/disable", acls_forwarding_disable);
	installHandler("firewall/acls/forwarding/add", acls_forwarding_add);
	installHandler("firewall/acls/forwarding/delete", acls_forwarding_delete);

	installHandler("firewall/acls/masquerading", acls_masquerading);
	installHandler("firewall/acls/masquerading/add", acls_masquerading_add);
	installHandler("firewall/acls/masquerading/delete", acls_masquerading_delete);
	installHandler("firewall/acls/masquerading/enable", acls_masquerading_enable);
	installHandler("firewall/acls/masquerading/disable", acls_masquerading_disable);

        installHandler("firewall/acls/otonat", acls_otonat);
        installHandler("firewall/acls/otonat/add", acls_otonat_add);
        installHandler("firewall/acls/otonat/delete", acls_otonat_delete);
	
	installHandler("firewall/autowan", autowan);
	installHandler("firewall/autowan/set", autowan_set);
	installHandler("firewall/autowan/interface/set", autowan_interface_set);
	installHandler("firewall/autowan/interface/del", autowan_interface_del);

	installHandler("firewall/signatures", signatures);
	installHandler("firewall/signatures/import", signatures_import);

	installHandler("firewall/captiveportal", captiveportal);
	installHandler("firewall/captiveportal/set", captiveportal_set);

        installHandler("firewall/fingerprints/list", fingerprints_list);

        installHandler("firewall/securitygroups/list", securitygroups_list);
        installHandler("firewall/securitygroups/add", securitygroups_add);
        installHandler("firewall/securitygroups/del", securitygroups_del);

        installHandler("firewall/securitygroups/entries/update", securitygroups_entries_update);
        installHandler("firewall/securitygroups/entries/add", securitygroups_entries_add);
        installHandler("firewall/securitygroups/entries/set", securitygroups_entries_set);
        installHandler("firewall/securitygroups/entries/del", securitygroups_entries_del);
}

string FirewallDelegate::rexpression() {
	return "firewall/(.*)";
}

void FirewallDelegate::setApplicationFilter(ApplicationFilter *filter) {
	this->httpApplicationFilter = filter;
}

ApplicationFilter *FirewallDelegate::getApplicationFilter() {
	if (this->httpApplicationFilter == NULL) {
		this->httpApplicationFilter = firewall->httpFilter;
	}

	return this->httpApplicationFilter;
}

void FirewallDelegate::setEventDb(EventDb *eventDb) {
	this->eventDb = eventDb;
}

void FirewallDelegate::serializeRule(JSONObject &rule, SFwallCore::FilterRulePtr target) {
	rule.put(L"enabled", new JSONValue((bool) target->enabled));

	if (target->comment.size() > 0) {
		rule.put(L"comment", new JSONValue((string) target->comment));
	}

        ObjectContainer* source_criterias = new ObjectContainer(CARRAY);
        for(SFwallCore::CriteriaPtr c: target->source_criteria){
                source_criterias->put(new ObjectWrapper(CriteriaBuilder::serialize(c.get())));
        }
        ObjectWrapper* source_criteriaWrapper = new ObjectWrapper(source_criterias);
        rule.put(L"source_criteria", ObjectWrapper::convertRecurse(source_criteriaWrapper));
        delete source_criteriaWrapper;

        ObjectContainer* destination_criterias = new ObjectContainer(CARRAY);
        for(SFwallCore::CriteriaPtr c: target->destination_criteria){
                destination_criterias->put(new ObjectWrapper(CriteriaBuilder::serialize(c.get())));
        }
        ObjectWrapper* destination_criteriaWrapper = new ObjectWrapper(destination_criterias);
        rule.put(L"destination_criteria", ObjectWrapper::convertRecurse(destination_criteriaWrapper));
        delete destination_criteriaWrapper;
}

void FirewallDelegate::serializeRule(JSONObject &rule, SFwallCore::PriorityRulePtr target) {
	rule.put(L"enabled", new JSONValue((bool) target->enabled));
        if (target->comment.size() > 0) {
                rule.put(L"comment", new JSONValue((string) target->comment));
        }

        ObjectContainer* source_criterias = new ObjectContainer(CARRAY);
        for(SFwallCore::CriteriaPtr c: target->source_criteria){
                source_criterias->put(new ObjectWrapper(CriteriaBuilder::serialize(c.get())));
        }
        ObjectWrapper* source_criteriaWrapper = new ObjectWrapper(source_criterias);
        rule.put(L"source_criteria", ObjectWrapper::convertRecurse(source_criteriaWrapper));
        delete source_criteriaWrapper;

        ObjectContainer* destination_criterias = new ObjectContainer(CARRAY);
        for(SFwallCore::CriteriaPtr c: target->destination_criteria){
                destination_criterias->put(new ObjectWrapper(CriteriaBuilder::serialize(c.get())));
        }
        ObjectWrapper* destination_criteriaWrapper = new ObjectWrapper(destination_criterias);
        rule.put(L"destination_criteria", ObjectWrapper::convertRecurse(destination_criteriaWrapper));
        delete destination_criteriaWrapper;
}

JSONObject FirewallDelegate::signatures(JSONObject args) {
	JSONObject ret;
	JSONArray arr;

	for (SFwallCore::SignaturePtr signature: firewall->signatureStore->available()) {
		JSONObject target;

		target.put(L"type", new JSONValue((string) signature->type()));
		target.put(L"name", new JSONValue((string) signature->name()));
		target.put(L"description", new JSONValue((string) signature->description()));
		target.put(L"is_category", new JSONValue((bool) signature->is_category()));

		if(!signature->is_category()){
			target.put(L"category", new JSONValue((string) signature->category()));

			if(signature->type().compare("dynamic") == 0){
				DynamicSignature* rsig = (DynamicSignature*) signature.get();
				JSONArray criteria_set;
				for(CriteriaSetPtr cset : rsig->criteria){
					JSONArray criteria_list; 
					for(Criteria* c : cset->criteria){
						criteria_list.push_back(ObjectWrapper::convertRecurse(new ObjectWrapper(CriteriaBuilder::serialize(c))));
					}

					criteria_set.push_back(new JSONValue(criteria_list));
				}

				target.put(L"criteria", new JSONValue(criteria_set)); 
			}
		}
		target.put(L"id", new JSONValue((string) signature->id()));
		arr.push_back(new JSONValue(target));
	}

	ret.put(L"signatures", new JSONValue(arr));
	return ret;
}

JSONObject FirewallDelegate::signatures_import(JSONObject args){
	JSONObject ret;
	ret.put(L"imported", new JSONValue((bool) firewall->signatureStore->import_signatures()));
	return ret; 
}

JSONObject FirewallDelegate::acls_forwarding(JSONObject args) {
	JSONObject ret;
	JSONArray arr;

	for (SFwallCore::PortForwardingRulePtr rule : firewall->natAcls->listPortForwardingRules()) {
		rule->holdLock();
		JSONObject target;

		ObjectContainer* source_criterias = new ObjectContainer(CARRAY);
		for(SFwallCore::CriteriaPtr c: rule->source_criteria){
			source_criterias->put(new ObjectWrapper(CriteriaBuilder::serialize(c.get())));
		}
		ObjectWrapper* source_criteriaWrapper = new ObjectWrapper(source_criterias);
		target.put(L"source_criteria", ObjectWrapper::convertRecurse(source_criteriaWrapper));
		delete source_criteriaWrapper;

		ObjectContainer* destination_criterias = new ObjectContainer(CARRAY);
		for(SFwallCore::CriteriaPtr c: rule->destination_criteria){
			destination_criterias->put(new ObjectWrapper(CriteriaBuilder::serialize(c.get())));
		}
		ObjectWrapper* destination_criteriaWrapper = new ObjectWrapper(destination_criterias);
		target.put(L"destination_criteria", ObjectWrapper::convertRecurse(destination_criteriaWrapper));
		delete destination_criteriaWrapper;

		target.put(L"forwardingDestination", new JSONValue((string) IP4Addr::ip4AddrToString(rule->forwardingDestination)));
		target.put(L"id", new JSONValue((string) rule->id));
		target.put(L"comment", new JSONValue((string) rule->comment));
		target.put(L"enabled", new JSONValue((bool) rule->enabled));

		if (rule->forwardingDestinationPort != UNSET) {
			target.put(L"forwardingDestinationPort", new JSONValue((double) rule->forwardingDestinationPort));
		}

		arr.push_back(new JSONValue(target));
		rule->releaseLock();
	}

	ret.put(L"rules", new JSONValue(arr));
	return ret;
}

JSONObject FirewallDelegate::acls_forwarding_add(JSONObject args) {
	SFwallCore::PortForwardingRulePtr rule;

	if (args.has(L"id")) {
		rule = firewall->natAcls->getPortForwardingRule(args[L"id"]->String());
	}

	if (!rule) {
		rule = SFwallCore::PortForwardingRulePtr(new SFwallCore::PortForwardingRule());
	}

	rule->holdLock();
	rule->source_criteria.clear();
	JSONArray source_criteria = args[L"source_criteria"]->AsArray();
	for(int x= 0; x < source_criteria.size(); x++){
		rule->source_criteria.push_back(CriteriaPtr(CriteriaBuilder::parse(ObjectWrapper::parseRecurse(source_criteria[x])->container())));
	}

	rule->destination_criteria.clear();
	JSONArray destination_criteria = args[L"destination_criteria"]->AsArray();
	for(int x= 0; x < destination_criteria.size(); x++){
		rule->destination_criteria.push_back(CriteriaPtr(CriteriaBuilder::parse(ObjectWrapper::parseRecurse(destination_criteria[x])->container())));
	}

	rule->forwardingDestination= IP4Addr::stringToIP4Addr(args[L"forwardingDestination"]->String());

	if (args.has(L"forwardingDestinationPort")) {
		rule->forwardingDestinationPort = args[L"forwardingDestinationPort"]->AsNumber();
	}
	if (args.has(L"comment")) {
		rule->comment = args[L"comment"]->String();
	} else {
		rule->comment = "";
	}

        if (args.has(L"enabled")) {
                rule->enabled = args[L"enabled"]->AsBool();
        }

	rule->releaseLock();	
	if (!args.has(L"id")) {
		firewall->natAcls->addForwardingRule(rule);
	}
	firewall->natAcls->initRules();
	firewall->natAcls->save();
	return JSONObject();
}

JSONObject FirewallDelegate::acls_forwarding_delete(JSONObject args) {
	std::string targetid = args[L"id"]->String();
	SFwallCore::PortForwardingRulePtr rule = firewall->natAcls->getPortForwardingRule(targetid);

	if (rule) {
		firewall->natAcls->delPortForwardingRule(rule);
	}

	firewall->natAcls->save();
	return JSONObject();
}

JSONObject FirewallDelegate::acls_forwarding_enable(JSONObject args) {
	std::string targetid = args[L"id"]->String();
	SFwallCore::PortForwardingRulePtr rule = firewall->natAcls->getPortForwardingRule(targetid);

	if (rule) {
		rule->holdLock();
		rule->enabled = true;
		rule->releaseLock();
	}

	firewall->natAcls->save();
	return JSONObject();
}

JSONObject FirewallDelegate::acls_forwarding_disable(JSONObject args) {
	std::string targetid = args[L"id"]->String();
	SFwallCore::PortForwardingRulePtr rule = firewall->natAcls->getPortForwardingRule(targetid);

	if (rule) {
		rule->holdLock();
		rule->enabled = false;
		rule->releaseLock();
	}

	firewall->natAcls->save();
	return JSONObject();
}
//MASQUERADING Routes
JSONObject FirewallDelegate::acls_masquerading(JSONObject args) {
	JSONObject ret;
	JSONArray arr;

	for (SFwallCore::MasqueradeRulePtr rule : firewall->natAcls->listMasqueradeRules()) {
		rule->holdLock();
		JSONObject target;

		ObjectContainer* source_criterias = new ObjectContainer(CARRAY);
		for(SFwallCore::CriteriaPtr c: rule->source_criteria){
			source_criterias->put(new ObjectWrapper(CriteriaBuilder::serialize(c.get())));
		}
		ObjectWrapper* source_criteriaWrapper = new ObjectWrapper(source_criterias);
		target.put(L"source_criteria", ObjectWrapper::convertRecurse(source_criteriaWrapper));
		delete source_criteriaWrapper;

		ObjectContainer* destination_criterias = new ObjectContainer(CARRAY);
		for(SFwallCore::CriteriaPtr c: rule->destination_criteria){
			destination_criterias->put(new ObjectWrapper(CriteriaBuilder::serialize(c.get())));
		}
		ObjectWrapper* destination_criteriaWrapper = new ObjectWrapper(destination_criterias);
		target.put(L"destination_criteria", ObjectWrapper::convertRecurse(destination_criteriaWrapper));
		delete destination_criteriaWrapper;

		target.put(L"natTargetDevice", new JSONValue((string) rule->natTargetDevice));
		target.put(L"comment", new JSONValue((string) rule->comment));
		if (rule->natTargetIp != UNSET) {
			target.put(L"natTargetIp", new JSONValue((string) IP4Addr::ip4AddrToString(rule->natTargetIp)));
		}

		target.put(L"id", new JSONValue((string) rule->id));
		target.put(L"enabled", new JSONValue((bool) rule->enabled));

		arr.push_back(new JSONValue(target));
		rule->releaseLock();
	}

	ret.put(L"rules", new JSONValue(arr));
	return ret;
}

JSONObject FirewallDelegate::acls_masquerading_add(JSONObject args) {
	SFwallCore::MasqueradeRulePtr rule;
	if (args.has(L"id")) {
		rule = firewall->natAcls->getMasqueradeRule(args[L"id"]->String());
	}

	if (!rule) {
		rule = SFwallCore::MasqueradeRulePtr(new SFwallCore::MasqueradeRule());
	}
	rule->holdLock();
	rule->source_criteria.clear();
	JSONArray source_criteria = args[L"source_criteria"]->AsArray();
	for(int x= 0; x < source_criteria.size(); x++){
		rule->source_criteria.push_back(CriteriaPtr(CriteriaBuilder::parse(ObjectWrapper::parseRecurse(source_criteria[x])->container())));
	}

	rule->destination_criteria.clear();
	JSONArray destination_criteria = args[L"destination_criteria"]->AsArray();
	for(int x= 0; x < destination_criteria.size(); x++){
		rule->destination_criteria.push_back(CriteriaPtr(CriteriaBuilder::parse(ObjectWrapper::parseRecurse(destination_criteria[x])->container())));
	}

	if (args.has(L"natTargetDevice")) {
		rule->natTargetDevice = args[L"natTargetDevice"]->String();
	}
	else {
		rule->natTargetDevice = "";
	}
	if (args.has(L"comment")) {
		rule->comment = args[L"comment"]->String();
	}
	else {
		rule->comment = "";
	}
	if (args.has(L"natTargetIp")) {
		rule->natTargetIp = IP4Addr::stringToIP4Addr(args[L"natTargetIp"]->String());
	}
	else {
		rule->natTargetIp = -1;
	}

	if (args.has(L"enabled")) {
		rule->enabled = args[L"enabled"]->AsBool(); 
	}

	rule->releaseLock();
	if (!args.has(L"id")) {
		firewall->natAcls->addMasqueradeRule(rule);
	}

	firewall->natAcls->initRules();
	firewall->natAcls->save();
	return JSONObject();
}

JSONObject FirewallDelegate::acls_masquerading_delete(JSONObject args) {
	std::string targetid = args[L"id"]->String();
	SFwallCore::MasqueradeRulePtr rule = firewall->natAcls->getMasqueradeRule(targetid);

	if (rule) {
		firewall->natAcls->delMasqueradeRule(rule);
	}

	firewall->natAcls->save();
	return JSONObject();
}

JSONObject FirewallDelegate::acls_masquerading_enable(JSONObject args) {
	std::string targetid = args[L"id"]->String();

	SFwallCore::MasqueradeRulePtr rule = firewall->natAcls->getMasqueradeRule(targetid);
	if (rule) {
		rule->holdLock();
		rule->enabled = true;
		rule->releaseLock();
	}

	firewall->natAcls->save();
	return JSONObject();
}

JSONObject FirewallDelegate::acls_masquerading_disable(JSONObject args) {
	std::string targetid = args[L"id"]->String();
	SFwallCore::MasqueradeRulePtr rule = firewall->natAcls->getMasqueradeRule(targetid);

	if (rule) {
                rule->holdLock();
		rule->enabled = false;
		rule->releaseLock();
	}

	firewall->natAcls->save();
	return JSONObject();
}

JSONObject FirewallDelegate::acls_otonat(JSONObject args){
	JSONObject ret;
	JSONArray arr;

	for (SFwallCore::OneToOneNatRulePtr rule : firewall->natAcls->listOneToOneRule()) {
		rule->holdLock();
		JSONObject target;

		target.put(L"id", new JSONValue((string) rule->id));
		target.put(L"name", new JSONValue((string) rule->name));
		target.put(L"interface", new JSONValue((string) rule->interface));
		target.put(L"internalIp", new JSONValue((string) IP4Addr::ip4AddrToString(rule->internalIp)));
		target.put(L"externalIp", new JSONValue((string) IP4Addr::ip4AddrToString(rule->externalIp)));
		target.put(L"enabled", new JSONValue((bool) rule->enabled));

		arr.push_back(new JSONValue(target));
		rule->releaseLock();
	}

	ret.put(L"rules", new JSONValue(arr));
	return ret;
}

JSONObject FirewallDelegate::acls_otonat_add(JSONObject args){
	SFwallCore::OneToOneNatRulePtr rule;

	if (args.has(L"id")) {
		rule = firewall->natAcls->getOneToOneRule(args[L"id"]->String());
	}

	if (!rule) {
		rule = SFwallCore::OneToOneNatRulePtr(new SFwallCore::OneToOneNatRule());
	}

	rule->holdLock();
	if (args.has(L"interface")){
		rule->interface = args[L"interface"]->String();
	}
	else {
		rule->interface = "";
	}

	if (args.has(L"internalIp")) {
		rule->internalIp= IP4Addr::stringToIP4Addr(args[L"internalIp"]->String());
	}
	else {
		rule->internalIp= -1;
	}

	if (args.has(L"externalIp")) {
		rule->externalIp= IP4Addr::stringToIP4Addr(args[L"externalIp"]->String());
	}
	else {
		rule->externalIp= -1;
	}

	rule->name = args[L"name"]->String();
	rule->enabled = args[L"enabled"]->AsBool();
	rule->releaseLock();

	if (!args.has(L"id")) {
		firewall->natAcls->addOneToOneRule(rule);
	}

	firewall->natAcls->initRules();
	firewall->natAcls->save();
	return JSONObject();
}

JSONObject FirewallDelegate::acls_otonat_delete(JSONObject args){
	std::string targetid = args[L"id"]->String();
	SFwallCore::OneToOneNatRulePtr rule = firewall->natAcls->getOneToOneRule(targetid);

	if (rule) {
		firewall->natAcls->delOneToOneRule(rule);
	}

	firewall->natAcls->save();
	return JSONObject();
}

JSONObject FirewallDelegate::process(std::string uri, JSONObject obj) {
	try {
		return invokeHandler(uri, obj);
	}
	catch (std::out_of_range e) {
		throw DelegateNotFoundException(uri);
	}
}

JSONObject FirewallDelegate::periods_list(JSONObject args) {
	TimePeriodStore *store = System::getInstance()->periods;
	JSONObject ret;
	JSONArray rarr;

	for (TimePeriodPtr target : store->list()) {
		JSONObject obj;
		obj.put(L"startTime", new JSONValue((double) target->startTime));
		obj.put(L"endTime", new JSONValue((double) target->endTime));
		obj.put(L"startDate", new JSONValue((double) target->startDate));
		obj.put(L"endDate", new JSONValue((double) target->endDate));
		//days:
		obj.put(L"any", new JSONValue((bool) target->any));
		obj.put(L"mon", new JSONValue((bool) target->mon));
		obj.put(L"tue", new JSONValue((bool) target->tue));
		obj.put(L"wed", new JSONValue((bool) target->wed));
		obj.put(L"thu", new JSONValue((bool) target->thu));
		obj.put(L"fri", new JSONValue((bool) target->fri));
		obj.put(L"sat", new JSONValue((bool) target->sat));
		obj.put(L"sun", new JSONValue((bool) target->sun));
		obj.put(L"id", new JSONValue((string) target->uuid));
		obj.put(L"name", new JSONValue((string) target->name));

		rarr.push_back(new JSONValue(obj));
	}

	ret.put(L"periods", new JSONValue(rarr));
	return ret;
}

JSONObject FirewallDelegate::periods_add(JSONObject obj) {
	TimePeriodStore *store = System::getInstance()->periods;
	TimePeriodPtr period;

	if(obj.has(L"id")){
		period = store->getById(obj[L"id"]->String());
		period->startTime = obj[L"startTime"]->AsNumber();
		period->endTime = obj[L"endTime"]->AsNumber();
		period->startDate = obj[L"startDate"]->AsNumber();
		period->endDate = obj[L"endDate"]->AsNumber();
		period->any = obj[L"any"]->AsBool();
		period->mon = obj[L"mon"]->AsBool();
		period->tue = obj[L"tue"]->AsBool();
		period->wed = obj[L"wed"]->AsBool();
		period->thu = obj[L"thu"]->AsBool();
		period->fri = obj[L"fri"]->AsBool();
		period->sat = obj[L"sat"]->AsBool();
		period->sun = obj[L"sun"]->AsBool();
	}else{
		string name = obj[L"name"]->String();
		if(name.size() > 0){
			period = TimePeriodPtr(new TimePeriod());
			period->name = obj[L"name"]->String();
			store->add(period);
		}else{
			throw DelegateGeneralException("Could not create a timeperiod without a name");
		}
	}

	store->save();

	JSONObject ret;
	ret.put(L"id", new JSONValue((string) period->uuid));
	return ret;
}

JSONObject FirewallDelegate::periods_delete(JSONObject obj) {
	TimePeriodStore *store = System::getInstance()->periods;
	JSONObject ret;

	try {
		TimePeriodPtr period = store->getById(obj[L"id"]->String());
		if (period) {
			store->remove(period);
			store->save();
			ret.put(L"status", new JSONValue((double) 0));
		}
		else {
			throw DelegateGeneralException("That period does not exist.");
		}
	}
	catch (const exception &e) {
		ret.put(L"status", new JSONValue((double) - 1));
	}

	return ret;
}

JSONObject FirewallDelegate::autowan(JSONObject obj) {
	JSONObject ret;
	NatAclStore *store = firewall->natAcls;
	ret.put(L"mode", new JSONValue((double) store->get_autowan_mode()));
	if(store->get_autowan_mode() == SINGLE){
		ret.put(L"interface", new JSONValue(store->get_autowan_single_interface()->name));

	}else if(store->get_autowan_mode() == AUTOWAN_MODE_LOADBALANCING || store->get_autowan_mode() == FAILOVER){
		JSONArray devices;
		for (AutowanInterfacePtr interface: store->get_autowan_rules()) {
			interface->holdLock();

			JSONObject o;
			o.put(L"failover_index", new JSONValue((double) interface->failover_index));
			o.put(L"interface", new JSONValue((string) interface->interface));

			ObjectContainer* criterias = new ObjectContainer(CARRAY);
			for(SFwallCore::Criteria* c: interface->criteria){
				criterias->put(new ObjectWrapper(CriteriaBuilder::serialize(c)));
			}
			ObjectWrapper* criteriaWrapper = new ObjectWrapper(criterias);
			o.put(L"criteria", ObjectWrapper::convertRecurse(criteriaWrapper));

			devices.push_back(new JSONValue(o));
			interface->releaseLock();
		}

		ret.put(L"interfaces", new JSONValue(devices));
	}

	return ret;
}

JSONObject FirewallDelegate::autowan_set(JSONObject obj) {
	NatAclStore *store = firewall->natAcls;
	store->set_autowan_mode(obj[L"mode"]->AsNumber());

	if(store->get_autowan_mode() == SINGLE){
		store->set_autowan_single_interface(obj[L"interface"]->String());			

	}else if(store->get_autowan_mode() == AUTOWAN_MODE_LOADBALANCING || store->get_autowan_mode() == FAILOVER){
		if(obj.has(L"interfaces")){
			store->remove_autowan_rules();	
			JSONArray devices = obj[L"interfaces"]->AsArray();
			for (uint x = 0; x < devices.size(); x++) {
				JSONObject o = devices[x]->AsObject();	

				AutowanInterface* interface = new AutowanInterface();
				interface->interface = o[L"interface"]->String();
				interface->failover_index = o[L"failover_index"]->AsNumber();

				if(obj.has(L"criteria")){
					interface->criteria.clear();
					JSONArray criteria = obj[L"criteria"]->AsArray();
					for(int x= 0; x < criteria.size(); x++){
						interface->criteria.push_back(CriteriaBuilder::parse(ObjectWrapper::parseRecurse(criteria[x])->container()));
					}
				}

				store->add_autowan_rule(AutowanInterfacePtr(interface));
			}
		}
	}

	store->initRules();
	store->save();
	return JSONObject();
}

JSONObject FirewallDelegate::autowan_interface_set(JSONObject obj) {
	NatAclStore *store = firewall->natAcls;
	AutowanInterfacePtr interface = store->get_autowan_rule_interface(obj[L"interface"]->String());
	if(!interface){
		interface = AutowanInterfacePtr(new AutowanInterface());	
		interface->interface = obj[L"interface"]->String();
		store->add_autowan_rule(interface);
	}

	if(interface){
		interface->holdLock();
		if(obj.has(L"failover_index")){
			interface->failover_index = obj[L"failover_index"]->AsNumber();
		}

		if(obj.has(L"criteria")){
			interface->criteria.clear();
			JSONArray criteria = obj[L"criteria"]->AsArray();
			for(int x= 0; x < criteria.size(); x++){
				interface->criteria.push_back(CriteriaBuilder::parse(ObjectWrapper::parseRecurse(criteria[x])->container()));
			}
		}

		interface->releaseLock();
	}
	store->initRules();
	store->save();
	return JSONObject();
}

JSONObject FirewallDelegate::autowan_interface_del(JSONObject obj) {
	NatAclStore *store = firewall->natAcls;
	store->del_autowan_rule_interface(obj[L"interface"]->String());

	store->initRules();
	store->save();
	return JSONObject();
}

JSONObject FirewallDelegate::captiveportal(JSONObject args){
	JSONObject ret;
	CapturePortalEngine *engine = firewall->capturePortalEngine;

	JSONArray object_inclusions;
	JSONArray object_exclusions;
	JSONArray exclusions;
	JSONArray inclusions;

	for(AliasPtr alias : engine->provided_inclusions_aliases){
		JSONObject o;
		o.put(L"aliasId", new JSONValue((string) alias->id));
		o.put(L"aliasName", new JSONValue((string) alias->name));
		object_inclusions.push_back(new JSONValue(o));
	}

	for(AliasPtr alias : engine->provided_exclusions_aliases){
		JSONObject o;
		o.put(L"aliasId", new JSONValue((string) alias->id));
		o.put(L"aliasName", new JSONValue((string) alias->name));
		object_exclusions.push_back(new JSONValue(o));
	}

	for(std::string item: engine->get_all_provided_exclusions()){
		exclusions.push_back(new JSONValue(item));
	}

	for(std::string item : engine->get_all_provided_inclusions()){
		inclusions.push_back(new JSONValue(item));
	}

	ret.put(L"object_inclusions", new JSONValue(object_inclusions));
	ret.put(L"object_exclusions", new JSONValue(object_exclusions));
	ret.put(L"exclusions", new JSONValue(exclusions));
	ret.put(L"inclusions", new JSONValue(inclusions));
	ret.put(L"mode", new JSONValue((double) engine->mode));
	ret.put(L"endpoint", new JSONValue((double) engine->endpoint));
	ret.put(L"url", new JSONValue(engine->cp_url));
	ret.put(L"allow_local", new JSONValue(engine->allow_local));
	ret.put(L"capture_all_traffic", new JSONValue(engine->capture_all_traffic));
	ret.put(L"disable_apple_captive_prompt", new JSONValue(engine->disable_apple_captive_prompt));

	return ret;
}

JSONObject FirewallDelegate::captiveportal_set(JSONObject args){
	CapturePortalEngine *engine = firewall->capturePortalEngine;
	AliasDb *aliases = firewall->aliases;

	engine->mode = args[L"mode"]->AsNumber();
	engine->endpoint = args[L"endpoint"]->AsNumber();
	engine->cp_url = args[L"url"]->String();

	/* We are overriding the provided values */
	engine->clear_provided_inclusions();
	engine->clear_provided_exclusions();
	engine->provided_inclusions_aliases.clear();
	engine->provided_exclusions_aliases.clear();

	JSONArray inclusions = args[L"inclusions"]->AsArray();
	for (unsigned x = 0; x < inclusions.size(); x++) {
		engine->add_provided_inclusions(StringUtils::trim(inclusions[x]->String()));
	}

	JSONArray exclusions = args[L"exclusions"]->AsArray();
	for (unsigned x = 0; x < exclusions.size(); x++) {
		engine->add_provided_exclusions(StringUtils::trim(exclusions[x]->String()));
	}

	JSONArray object_inclusions = args[L"object_inclusions"]->AsArray();
	for (unsigned x = 0; x < object_inclusions.size(); x++) {
		string range = object_inclusions[x]->String();
		AliasPtr alias = aliases->get(range);

		if (alias) {
			engine->provided_inclusions_aliases.push_back(alias);
		}
	}

	JSONArray object_exclusions = args[L"object_exclusions"]->AsArray();
	for (unsigned x = 0; x < object_exclusions.size(); x++) {
		string range = object_exclusions[x]->String();
		AliasPtr alias = aliases->get(range);

		if (alias) {
			engine->provided_exclusions_aliases.push_back(alias);
		}
	}

	if(args.has(L"allow_local")){
		engine->allow_local = args[L"allow_local"]->AsBool();
	}

	if(args.has(L"capture_all_traffic")){
		engine->capture_all_traffic = args[L"capture_all_traffic"]->AsBool();
	}

	if(args.has(L"disable_apple_captive_prompt")){
		engine->disable_apple_captive_prompt = args[L"disable_apple_captive_prompt"]->AsBool();
	}

	engine->initopts();
	engine->save();
	return JSONObject();
}

JSONObject FirewallDelegate::aliases_truncate(JSONObject args){
	AliasPtr alias = firewall->aliases->get(args[L"id"]->String());
	if (alias) {
		for(std::string entry : alias->listEntries()){
			alias->removeEntry(entry);
		}
	}

	return JSONObject();
}

JSONObject FirewallDelegate::aliases_list(JSONObject) {
	JSONObject ret;
	JSONArray array;

	for (std::pair<string, AliasPtr> aliasMapping : firewall->aliases->aliases) {
		JSONObject o;

		string a = aliasMapping.second->description() + ((!aliasMapping.second->source) ? "." : (" from " + aliasMapping.second->source->description()));

		o.put(L"id", new JSONValue(aliasMapping.first));
		o.put(L"name", new JSONValue(aliasMapping.second->name));
		o.put(L"type", new JSONValue((double) aliasMapping.second->type()));
		o.put(L"description", new JSONValue(a));
		array.push_back(new JSONValue(o));
	}

	ret.put(L"aliases", new JSONValue(array));
	return ret;
}

JSONObject FirewallDelegate::aliases_get(JSONObject object) {
	AliasPtr alias = firewall->aliases->get(object[L"id"]->String());

	if (alias) {
		JSONObject o;
		o.put(L"id", new JSONValue(alias->id));
		o.put(L"name", new JSONValue(alias->name));
		o.put(L"description", new JSONValue(alias->description()));

		return o;
	}

	throw DelegateGeneralException("Could not find alias");
}

JSONObject FirewallDelegate::aliases_load(JSONObject object) {
	JSONObject ret;
	AliasPtr alias = firewall->aliases->get(object[L"id"]->String());
	ret.put(L"loadstate", new JSONValue((double) ((alias) ? alias->load() : -1)));
	return ret;
}

JSONObject FirewallDelegate::aliases_alias_search(JSONObject object) {
	JSONObject ret;
	AliasPtr alias = firewall->aliases->get(object[L"id"]->String());

	/*Depending on the alias type we must search for different things*/
	std::string searchExpression = object[L"search"]->String();	
	if(alias->type() == IP_RANGE || alias->type() == IP_SUBNET){
		ret.put(L"matched", new JSONValue((bool) alias->searchForNetworkMatch(IP4Addr::stringToIP4Addr(searchExpression))));
	}

	if(alias->type() == WEBSITE_LIST || alias->type() == STRING_WILDCARD_LIST){
		ret.put(L"matched", new JSONValue((bool) alias->search(searchExpression)));
	}

	return ret;
}

JSONObject FirewallDelegate::aliases_add_bulk(JSONObject object) {
	JSONArray items = object[L"items"]->AsArray();

	for (unsigned x = 0; x < items.size(); x++) {
		aliases_add(items[x]->AsObject());
	}

	return JSONObject();
}

JSONObject FirewallDelegate::aliases_add(JSONObject object) {
	if (firewall->aliases->getByName(object[L"name"]->String())) {
		return JSONObject();
	}

	SFwallCore::AliasType type = (SFwallCore::AliasType) object[L"type"]->AsNumber();
	int ret = 0;
	AliasPtr alias;

	switch (type) {
		case IP_RANGE: {
				       alias = AliasPtr(new IpRangeAlias());
				       alias->name = object[L"name"]->String();
				       break;
			       }

		case IP_SUBNET: {
					alias = AliasPtr(new IpSubnetAlias());
					alias->name = object[L"name"]->String();
					break;
				}

		case WEBSITE_LIST : {
					    alias = AliasPtr(new WebsiteListAlias());
					    alias->name = object[L"name"]->String();
					    break;
				    }

		case STRING_WILDCARD_LIST: {
						   alias = AliasPtr(new StringWildcardListAlias());
						   alias->name = object[L"name"]->String();
						   break;
					   }
		case MAC_ADDRESS_LIST:{
					      alias = AliasPtr(new MacAddressListAlias());
					      alias->name = object[L"name"]->String();
					      break;

				      }
		default:
				      throw DelegateGeneralException("Alias type could not be resolved");

	}

	if (object.has(L"source") && object[L"source"]->AsNumber() != -1) {
		AliasListSourceType source = (AliasListSourceType) object[L"source"]->AsNumber();

		switch (source) {
			case DNS: {
					  alias->source = new DnsListSource();
					  alias->source->detail = object[L"detail"]->String();
					  break;
				  }

			case HTTP_FILE: {
						alias->source = new HttpFileSource();
						alias->source->detail = object[L"detail"]->String();
						break;
					}

			default:
					throw DelegateGeneralException("Alias source type could not be resolved");

		};

	}
	else {
		alias->source = NULL;

		//Can I already add entries:
		if (object.has(L"items")) {
			JSONArray items = object[L"items"]->AsArray();

			for (unsigned x = 0; x < items.size(); x++) {
				alias->addEntry(items[x]->String());
			}
		}
	}

	ret = firewall->aliases->create(alias);

	JSONObject jret;
	jret.put(L"loadstate", new JSONValue((double) ret));
	return jret;
}

JSONObject FirewallDelegate::aliases_del(JSONObject object) {
	AliasPtr target = firewall->aliases->get(object[L"id"]->String());

	if (!target) {
		throw DelegateGeneralException("Could not find alias");
	}

	firewall->aliases->del(target);
	return JSONObject();
}

JSONObject FirewallDelegate::aliases_del_all(JSONObject object) {
	map<string, AliasPtr>::iterator iter;
	list<AliasPtr> toDelete;

	for (iter = firewall->aliases->aliases.begin(); iter != firewall->aliases->aliases.end(); iter++) {
		AliasPtr alias = iter->second;
		toDelete.push_back(alias);
	}

	for (list<AliasPtr>::iterator iter = toDelete.begin(); iter != toDelete.end(); iter++) {
		firewall->aliases->del((*iter));
	}

	return JSONObject();
}

JSONObject FirewallDelegate::aliases_del_bulk(JSONObject object) {
	JSONArray ids = object[L"ids"]->AsArray();

	for (unsigned x = 0; x < ids.size(); x++) {
		AliasPtr target = firewall->aliases->get(ids[x]->String());

		if (target) {
			firewall->aliases->del(target);
		}
	}

	return JSONObject();
}

JSONObject FirewallDelegate::aliases_alias_list(JSONObject object) {
	JSONObject ret;
	JSONArray items;

	AliasPtr alias = firewall->aliases->get(object[L"id"]->String());

	if (alias) {
		std::list<std::string> entries = alias->listEntries();

		for (std::list<std::string>::iterator iter = entries.begin();
				iter != entries.end();
				iter++) {

			items.push_back(new JSONValue((string)(*iter)));
		}

		ret.put(L"items", new JSONValue(items));
		ret.put(L"type", new JSONValue((double) alias->type()));
		return ret;
	}

	return JSONObject();
}

JSONObject FirewallDelegate::aliases_alias_add(JSONObject object) {
	JSONObject ret;

	AliasPtr alias = firewall->aliases->get(object[L"id"]->String());

	if (alias) {
		alias->addEntry(object[L"value"]->String());
		firewall->aliases->save();
	}

	return JSONObject();
}

JSONObject FirewallDelegate::aliases_alias_del(JSONObject object) {
	JSONObject ret;

	AliasPtr alias = firewall->aliases->get(object[L"id"]->String());

	if (alias) {
		alias->removeEntry(object[L"value"]->String());
		firewall->aliases->save();
	}

	return JSONObject();
}

JSONObject FirewallDelegate::tracker_size(JSONObject object) {
	JSONObject ret;
	ret.put(L"size", new JSONValue((double) firewall->connectionTracker->size()));
	return ret;
}

JSONObject FirewallDelegate::tracker_stats(JSONObject object) {
	JSONObject ret;

	firewall->connectionTracker->holdLock();
	vector<SFwallCore::Connection*> connections = firewall->connectionTracker->listConnections();
	ret.put(L"total_connections", new JSONValue((double) connections.size()));

	map<std::string, double> top_signatures;
	map<std::string, double> top_users;
	map<Host*, double> top_devices;
	map<int, double> top_ports;

	map<std::string, double> top_signatures_transfer;
	map<std::string, double> top_users_transfer;
	map<Host*, double> top_devices_transfer;
	map<int, double> top_ports_transfer;


	for (SFwallCore::Connection* connection : firewall->connectionTracker->listConnections()) {
		if (!connection->hasExpired()) {
			if(connection->getApplicationInfo()->exclusive_signature){
				top_signatures[connection->getApplicationInfo()->exclusive_signature->id()] = 
					top_signatures[connection->getApplicationInfo()->exclusive_signature->id()] + 1;

				top_signatures_transfer[connection->getApplicationInfo()->exclusive_signature->id()] =
					top_signatures_transfer[connection->getApplicationInfo()->exclusive_signature->id()] + connection->getUpload() + connection->getDownload();
			}	

			if(connection->getHostDiscoveryServiceEntry() && connection->getHostDiscoveryServiceEntry()->authenticated_user){
				top_users[connection->getHostDiscoveryServiceEntry()->authenticated_user->getUserName()] =  
					top_users[connection->getHostDiscoveryServiceEntry()->authenticated_user->getUserName()] + 1;

				top_users_transfer[connection->getHostDiscoveryServiceEntry()->authenticated_user->getUserName()] =
					top_users_transfer[connection->getHostDiscoveryServiceEntry()->authenticated_user->getUserName()] + connection->getUpload() + connection->getDownload();
			}

			if(connection->getDestinationPort()){
				top_ports[connection->getDestinationPort()] =  top_ports[connection->getDestinationPort()] + 1;
				top_ports_transfer[connection->getDestinationPort()] =  top_ports_transfer[connection->getDestinationPort()] + connection->getUpload() + connection->getDownload();
			}

			if(connection->getHostDiscoveryServiceEntry()){
				top_devices[connection->getHostDiscoveryServiceEntry().get()] = 
					top_devices[connection->getHostDiscoveryServiceEntry().get()] + 1;

				top_devices_transfer[connection->getHostDiscoveryServiceEntry().get()] = 
					top_devices_transfer[connection->getHostDiscoveryServiceEntry().get()] + connection->getUpload() + connection->getDownload();
			}
		}
	}

	JSONArray top_signatures_array;
	for(pair<std::string, double> item : top_signatures){
		JSONObject stat;
		SignaturePtr signature = System::getInstance()->getFirewall()->signatureStore->get(item.first);
		stat.put(L"signature_name", new JSONValue(signature->name()));
		stat.put(L"signature", new JSONValue(item.first));
		stat.put(L"connections", new JSONValue(item.second));
		top_signatures_array.push_back(new JSONValue(stat));
	}
	ret.put(L"top_signatures", new JSONValue(top_signatures_array));

	JSONArray top_users_array;
	for(pair<std::string, double> item : top_users){
		JSONObject stat;
		stat.put(L"username", new JSONValue(item.first));
		stat.put(L"connections", new JSONValue(item.second));
		top_users_array.push_back(new JSONValue(stat));
	}
	ret.put(L"top_users", new JSONValue(top_users_array));

	JSONArray top_devices_array;
	for(pair<Host*, double> item : top_devices){
		JSONObject stat;
		stat.put(L"ip", new JSONValue(item.first->getIp()));
		stat.put(L"mac", new JSONValue(item.first->mac));
		stat.put(L"connections", new JSONValue(item.second));
		top_devices_array.push_back(new JSONValue(stat));
	}
	ret.put(L"top_devices", new JSONValue(top_devices_array));

	JSONArray top_ports_array;
	for(pair<int, double> item : top_ports){
		JSONObject stat;
		stat.put(L"device", new JSONValue((double) item.first));
		stat.put(L"connections", new JSONValue(item.second));
		top_ports_array.push_back(new JSONValue(stat));
	}
	ret.put(L"top_ports", new JSONValue(top_ports_array));

	/* Transfer Stats*/
	JSONArray top_signatures_transfer_array;
	for(pair<std::string, double> item : top_signatures_transfer){
		JSONObject stat;
		SignaturePtr signature = System::getInstance()->getFirewall()->signatureStore->get(item.first);
		stat.put(L"signature_name", new JSONValue(signature->name()));
		stat.put(L"signature", new JSONValue(item.first));
		stat.put(L"transfer", new JSONValue(item.second));
		top_signatures_transfer_array.push_back(new JSONValue(stat));
	}
	ret.put(L"top_signatures_transfer", new JSONValue(top_signatures_transfer_array));

	JSONArray top_users_transfer_array;
	for(pair<std::string, double> item : top_users_transfer){
		JSONObject stat;
		stat.put(L"username", new JSONValue(item.first));
		stat.put(L"transfer", new JSONValue(item.second));
		top_users_transfer_array.push_back(new JSONValue(stat));
	}
	ret.put(L"top_users_transfer", new JSONValue(top_users_transfer_array));

	JSONArray top_devices_transfer_array;
	for(pair<Host*, double> item : top_devices_transfer){
		JSONObject stat;
		stat.put(L"ip", new JSONValue(item.first->getIp()));
		stat.put(L"mac", new JSONValue(item.first->mac));
		stat.put(L"transfer", new JSONValue(item.second));
		top_devices_transfer_array.push_back(new JSONValue(stat));
	}
	ret.put(L"top_devices_transfer", new JSONValue(top_devices_transfer_array));

	JSONArray top_ports_transfer_array;
	for(pair<int, double> item : top_ports_transfer){
		JSONObject stat;
		stat.put(L"device", new JSONValue((double) item.first));
		stat.put(L"tranfer", new JSONValue(item.second));
		top_ports_transfer_array.push_back(new JSONValue(stat));
	}
	ret.put(L"top_ports_transfer", new JSONValue(top_ports_transfer_array));

	firewall->connectionTracker->releaseLock();
	return ret;
}	

JSONObject FirewallDelegate::tracker_list(JSONObject object) {
	JSONObject ret;
	JSONArray conns;

	firewall->connectionTracker->holdLock();
	for (SFwallCore::Connection* connection : firewall->connectionTracker->listConnections()) {
		if (!connection->hasExpired()) {
			if (object.has(L"filterTime")) {
				int filterTime = object[L"filterTime"]->AsNumber();

				if (connection->getTime() < filterTime) {
					continue;
				}
			}

			if (object.has(L"user")) {
				string user = object[L"user"]->String();
				if (connection->getHostDiscoveryServiceEntry() && connection->getHostDiscoveryServiceEntry()->authenticated_user) {
					if(connection->getHostDiscoveryServiceEntry()->authenticated_user->getUserName().compare(user) != 0){
						continue;
					}
				}else{
					continue;
				}
			}

			if (object.has(L"mac")) {
				string mac = object[L"mac"]->String();
				string ip = object[L"ip"]->String();
				if (connection->getHostDiscoveryServiceEntry()) {
					if(connection->getHostDiscoveryServiceEntry()->mac.compare(mac) != 0 || connection->getHostDiscoveryServiceEntry()->getIp().compare(ip) != 0){
						continue;
					}
				}else{
					continue;
				}
			}

			JSONObject o;

			if (connection->getIp()->type() == IPV4) {
				ConnectionIpV4 *ipv4 = (ConnectionIpV4 *) connection->getIp();
				o.put(L"sourceIp", new JSONValue((string) IP4Addr::ip4AddrToString(ipv4->getSrcIp())));
				o.put(L"destIp", new JSONValue((string) IP4Addr::ip4AddrToString(ipv4->getDstIp())));
			}
			else if (connection->getIp()->type() == IPV6) {
				ConnectionIpV6 *ipv6 = (ConnectionIpV6 *) connection->getIp();

				char dst[INET6_ADDRSTRLEN];
				char src[INET6_ADDRSTRLEN];

				inet_ntop(AF_INET6, ipv6->getDstIp(), (char *) &dst, INET6_ADDRSTRLEN);
				inet_ntop(AF_INET6, ipv6->getSrcIp(), (char *) &src, INET6_ADDRSTRLEN);

				o.put(L"sourceIp", new JSONValue((string) src));
				o.put(L"destIp", new JSONValue((string) dst));
			}

			o.put(L"bytes", new JSONValue((double) connection->getUpload() + connection->getDownload()));
			o.put(L"protocol", new JSONValue((double) connection->getProtocol()));
			o.put(L"id", new JSONValue((string) connection->get_id()));

			if (connection->getHostDiscoveryServiceEntry() && connection->getHostDiscoveryServiceEntry()->authenticated_user) {
				o.put(L"user", new JSONValue((string) connection->getHostDiscoveryServiceEntry()->authenticated_user->getUserName()));
			}

			if (connection->getProtocol() == SFwallCore::TCP) {
				SFwallCore::TcpConnection *tcp = dynamic_cast<SFwallCore::TcpConnection *>(connection);
				o.put(L"sourcePort", new JSONValue((double) tcp->getSourcePort()));
				o.put(L"destPort", new JSONValue((double) tcp->getDestinationPort()));

				o.put(L"state", new JSONValue((string) tcp->getState()->echo()));
			}

			if (connection->getProtocol() == SFwallCore::UDP) {
				SFwallCore::UdpConnection *udp = dynamic_cast<SFwallCore::UdpConnection *>(connection);
				o.put(L"sourcePort", new JSONValue((double) udp->getSourcePort()));
				o.put(L"destPort", new JSONValue((double) udp->getDestinationPort()));
			}

			if (connection->idle() > (60 * 2)) {
				o.put(L"isIdle", new JSONValue(true));
			}

			if(connection->getApplicationInfo()->exclusive_signature){
				o.put(L"application_tag", new JSONValue((string) connection->getApplicationInfo()->exclusive_signature->id()));
				o.put(L"application_tag_name", new JSONValue((string) connection->getApplicationInfo()->exclusive_signature->name()));
			}

			o.put(L"hw", new JSONValue((string) connection->getHwAddress()));
			conns.push_back(new JSONValue(o));
		}
	}
	firewall->connectionTracker->releaseLock();
	ret.put(L"connections", new JSONValue(conns));

	return ret;
}

JSONObject FirewallDelegate::tracker_connection_details(JSONObject obj) {
	SFwallCore::PlainConnTracker *connTracker = firewall->connectionTracker;
	firewall->connectionTracker->holdLock();

	SFwallCore::Connection* connection = connTracker->get_connection_by_id(obj[L"id"]->String());

	JSONObject o;
	if(connection){
		o.put(L"upload", new JSONValue((double) connection->getUpload()));
		o.put(L"download", new JSONValue((double) connection->getDownload()));
		o.put(L"packets", new JSONValue((double) connection->getNoPackets()));
		o.put(L"hw", new JSONValue((string) connection->getHwAddress()));
		o.put(L"nice", new JSONValue((double) connection->nice));

		if(connection->getApplicationInfo()->exclusive_signature){
			o.put(L"application_tag", new JSONValue((string) connection->getApplicationInfo()->exclusive_signature->id()));
			o.put(L"application_tag_name", new JSONValue((string) connection->getApplicationInfo()->exclusive_signature->name()));
			o.put(L"application_tag_ssl3_ip_match", new JSONValue((bool) connection->getApplicationInfo()->http_ssl3IpMatch));
		}

		if(connection->verdict_filter_rule){
			o.put(L"verdict_filter_rule", new JSONValue((string) connection->verdict_filter_rule->id));
			o.put(L"verdict_filter_rule_name", new JSONValue((string) connection->verdict_filter_rule->comment));
		}

		if(connection->verdict_application_rule){
			o.put(L"verdict_application_rule", new JSONValue((string) connection->verdict_application_rule->id));
			o.put(L"verdict_application_rule_name", new JSONValue((string) connection->verdict_application_rule->name));
		}

		o.put(L"application_http_hostname", new JSONValue((string) connection->getApplicationInfo()->http_hostName));
		o.put(L"application_http_useragent", new JSONValue((string) connection->getApplicationInfo()->http_useragent));
		o.put(L"application_http_contenttype", new JSONValue((string) connection->getApplicationInfo()->http_contenttype));
	}

	firewall->connectionTracker->releaseLock();
	return o; 
}

JSONObject FirewallDelegate::tracker_terminate(JSONObject obj) {
	if (eventDb) {
		EventParams params;
		eventDb->add(new Event(AUDIT_FIREWALL_CONNECTIONS_TERMINATE, params));
	}

	SFwallCore::PlainConnTracker *connTracker = firewall->connectionTracker;
	connTracker->holdLock();
	SFwallCore::Connection* connection = connTracker->get_connection_by_id(obj[L"id"]->String());

	if(connection){
		connTracker->terminate(connection);
	}
	firewall->connectionTracker->releaseLock();

	return JSONObject();
}

JSONObject FirewallDelegate::acls_filter_up(JSONObject args) {
	std::string id = args[L"id"]->String();
	FilterRulePtr filtering_rule = firewall->acls->getRuleById(id);
	if (filtering_rule) {
		firewall->acls->moveup(filtering_rule);
	}

	PriorityRulePtr priority_rule = firewall->priority_acls->getRuleById(id);
	if(priority_rule){
		firewall->priority_acls->moveup(priority_rule);
	}

	return JSONObject();
}

JSONObject FirewallDelegate::acls_filter_down(JSONObject args) {
	std::string id = args[L"id"]->String();
	FilterRulePtr filtering_rule = firewall->acls->getRuleById(id);
	if (filtering_rule) {
		firewall->acls->movedown(filtering_rule);
	}

	PriorityRulePtr priority_rule = firewall->priority_acls->getRuleById(id);
	if(priority_rule){
		firewall->priority_acls->movedown(priority_rule);
	}
	return JSONObject();
}

JSONObject FirewallDelegate::acls_filter_delete(JSONObject args) {
	std::string id = args[L"id"]->String();
	FilterRulePtr filtering_rule = firewall->acls->getRuleById(id);
	if (filtering_rule) {
		firewall->acls->unloadAclEntry(filtering_rule);
	}

	PriorityRulePtr priority_rule = firewall->priority_acls->getRuleById(id);
	if(priority_rule){
		firewall->priority_acls->unloadAclEntry(priority_rule);
	}
	return JSONObject();
}

JSONObject FirewallDelegate::acls_nat_delete(JSONObject args) {
	return acls_filter_delete(args);
}

JSONObject FirewallDelegate::acls_list(JSONObject) {
	JSONObject ret;
	JSONArray filter;
	JSONArray nat;

	for (auto target : firewall->acls->listFilterRules()) {
		target->holdLock();
		JSONObject rule;

		if (target && target->valid) {
			serializeRule(rule, target);

			rule.put(L"action", new JSONValue((double) target->action));
			rule.put(L"ignoreconntrack", new JSONValue((bool) target->ignoreconntrack));
			rule.put(L"hits", new JSONValue((double) target->count));
			rule.put(L"last_hit", new JSONValue((double) target->last_hit));
			rule.put(L"id", new JSONValue((string) target->id));
			rule.put(L"log", new JSONValue((bool) target->log));
			rule.put(L"group", new JSONValue((double) target->group));

			filter.push_back(new JSONValue(rule));
		}
		target->releaseLock();
	}

	JSONArray priority;
	for (auto target :  firewall->priority_acls->listPriorityRules()) {
		target->holdLock();
		JSONObject rule;

		if (target->valid) {
			serializeRule(rule, target);
			rule.put(L"nice", new JSONValue((double) target->nice));
			rule.put(L"id", new JSONValue((string) target->id));

			priority.push_back(new JSONValue(rule));
		}
		target->releaseLock();
	}

	ret.put(L"priority", new JSONValue(priority));
	ret.put(L"nat", new JSONValue(nat));
	ret.put(L"normal", new JSONValue(filter));
	return ret;
}

JSONObject FirewallDelegate::acls_groups_list(JSONObject args) {
	JSONArray grp_array;
	JSONObject ret;
	map<int, std::string> group_labels = firewall->acls->group_labels;
	for (auto iter : group_labels) {
		JSONObject grp;
		grp.put(L"id", new JSONValue((double) iter.first));
		grp.put(L"label", new JSONValue((string) iter.second));
		grp_array.push_back(new JSONValue(grp));
	}
	ret.put(L"firewall_groups", new JSONValue(grp_array));
	return ret;
}

JSONObject FirewallDelegate::acls_groups_add(JSONObject args) {
	JSONArray groups = args[L"groups"]->AsArray();
	firewall->acls->holdLock();
	if (groups.size() == 0) {
		JSONObject grp_obj = args[L"groups"]->AsObject();
		firewall->acls->group_labels[grp_obj[L"id"]->AsNumber()] = grp_obj[L"label"]->String();
	}
	else {
		for(int x = 0; x < groups.size(); x++){
			JSONObject grp = groups[x]->AsObject();
			firewall->acls->group_labels[grp[L"id"]->AsNumber()] = grp[L"label"]->String();
		}
	}
	firewall->acls->save();
	firewall->acls->releaseLock();
	return JSONObject();
}

JSONObject FirewallDelegate::acls_bulk_add_set(JSONObject args) {
	JSONArray rules = args[L"rules"]->AsArray();
	for(int x = 0; x < rules.size(); x++){
		JSONObject rule = rules[x]->AsObject();
		acls_add(rule);
	}
	return JSONObject();
}

JSONObject FirewallDelegate::acls_add(JSONObject args) {
	//Work out what kind of rule we are dealing with first:
	int action = args[L"action"]->AsNumber();
	if (action == 0 || action == 1) {
		SFwallCore::FilterRulePtr rule;
		if (args.has(L"id")) {
			rule = firewall->acls->getRuleById(args[L"id"]->String());
		}else{
			rule = SFwallCore::FilterRulePtr(new FilterRule());
			rule->enabled = false;
		}
		rule->holdLock();

		/* This seems a bit crazy, but without locking the entire structure while modifying the rule, we could run in to issues */
		rule->enabled = false;
		if (args.has(L"comment")) {
			rule->comment = args[L"comment"]->String();
		}
		if (args.has(L"group")) {
			rule->group = args[L"group"]->AsNumber();
		}

		if (args.has(L"enabled")) {
			rule->enabled = args[L"enabled"]->AsBool();
		}

		rule->source_criteria.clear();
		JSONArray source_criteria = args[L"source_criteria"]->AsArray();
		for(int x= 0; x < source_criteria.size(); x++){
			rule->source_criteria.push_back(CriteriaPtr(CriteriaBuilder::parse(ObjectWrapper::parseRecurse(source_criteria[x])->container())));
		}

		rule->destination_criteria.clear();
		JSONArray destination_criteria = args[L"destination_criteria"]->AsArray();
		for(int x= 0; x < destination_criteria.size(); x++){
			rule->destination_criteria.push_back(CriteriaPtr(CriteriaBuilder::parse(ObjectWrapper::parseRecurse(destination_criteria[x])->container())));
		}

		if (args.has(L"ignoreconntrack")) {
			rule->ignoreconntrack = args[L"ignoreconntrack"]->AsBool();
		}

		rule->action = action;
		rule->releaseLock();
		firewall->acls->loadAclEntry(rule);
		firewall->acls->save();

		JSONObject ret;
		ret.put(L"id", new JSONValue((string) rule->id));
		return ret;

	}
	else if (action == 4) {
		SFwallCore::PriorityRulePtr rule;
		if (args.has(L"id")) {
			rule = firewall->priority_acls->getRuleById(args[L"id"]->String());
		}else{
			rule = SFwallCore::PriorityRulePtr(new SFwallCore::PriorityRule());
			rule->enabled = false;
		}
		rule->holdLock();

		if (args.has(L"comment")) {
			rule->comment = args[L"comment"]->String();
		}
		rule->source_criteria.clear();
		JSONArray source_criteria = args[L"source_criteria"]->AsArray();
		for(int x= 0; x < source_criteria.size(); x++){
			rule->source_criteria.push_back(CriteriaPtr(CriteriaBuilder::parse(ObjectWrapper::parseRecurse(source_criteria[x])->container())));
		}

		rule->destination_criteria.clear();
		JSONArray destination_criteria = args[L"destination_criteria"]->AsArray();
		for(int x= 0; x < destination_criteria.size(); x++){
			rule->destination_criteria.push_back(CriteriaPtr(CriteriaBuilder::parse(ObjectWrapper::parseRecurse(destination_criteria[x])->container())));
		}

		rule->nice = args[L"nice"]->AsNumber();
		rule->releaseLock();
		firewall->priority_acls->loadAclEntry(rule);

		JSONObject ret;
		ret.put(L"id", new JSONValue((string) rule->id));
		return ret;
	}

	return JSONObject();
}

JSONObject FirewallDelegate::acls_add_trafficshaper(JSONObject args) {
	TsRulePtr rule; 
	bool modify = false;
	if(args.has(L"id")){
		modify = true;
		rule = firewall->trafficShaper->get(args[L"id"]->String());
	}

	if(!rule){
		rule = TsRulePtr(new TsRule(StringUtils::genRandom()));
	}

	rule->filter->source_criteria.clear();
	JSONArray source_criteria = args[L"source_criteria"]->AsArray();
	for(int x= 0; x < source_criteria.size(); x++){
		rule->filter->source_criteria.push_back(CriteriaPtr(CriteriaBuilder::parse(ObjectWrapper::parseRecurse(source_criteria[x])->container())));
	}

	rule->filter->destination_criteria.clear();
	JSONArray destination_criteria = args[L"destination_criteria"]->AsArray();
	for(int x= 0; x < destination_criteria.size(); x++){
		rule->filter->destination_criteria.push_back(CriteriaPtr(CriteriaBuilder::parse(ObjectWrapper::parseRecurse(destination_criteria[x])->container())));
	}

	int proposedUploadRate = args[L"upload"]->AsNumber();
	int proposedDownloadRate = args[L"download"]->AsNumber();

	//Check thats this rule isnt a complete wildcard with tiny values:
	rule->uploadRate = proposedUploadRate; 
	rule->downloadRate = proposedDownloadRate; 

	rule->cumulative = args[L"cumulative"]->AsBool();
	rule->name = args[L"name"]->String();
	if(!modify){
		firewall->trafficShaper->add(rule);
	}

	firewall->trafficShaper->invalidate();
	firewall->trafficShaper->save();
	return JSONObject();
}

JSONObject FirewallDelegate::acls_list_trafficshaper(JSONObject args) {
	JSONObject ret;
	JSONArray arr;

	for (TsRulePtr rule : firewall->trafficShaper->list()) {
		TokenFilterPtr target = rule->filter;
		JSONObject o;

		ObjectContainer* source_criterias = new ObjectContainer(CARRAY);
		for(SFwallCore::CriteriaPtr c: target->source_criteria){
			source_criterias->put(new ObjectWrapper(CriteriaBuilder::serialize(c.get())));
		}
		ObjectWrapper* source_criteriaWrapper = new ObjectWrapper(source_criterias);
		o.put(L"source_criteria", ObjectWrapper::convertRecurse(source_criteriaWrapper));
		delete source_criteriaWrapper;

		ObjectContainer* destination_criterias = new ObjectContainer(CARRAY);
		for(SFwallCore::CriteriaPtr c: target->destination_criteria){
			destination_criterias->put(new ObjectWrapper(CriteriaBuilder::serialize(c.get())));
		}
		ObjectWrapper* destination_criteriaWrapper = new ObjectWrapper(destination_criterias);
		o.put(L"destination_criteria", ObjectWrapper::convertRecurse(destination_criteriaWrapper));
		delete destination_criteriaWrapper;

		o.put(L"upload", new JSONValue((double) rule->uploadRate));
		o.put(L"download", new JSONValue((double) rule->downloadRate));
		o.put(L"id", new JSONValue((string) rule->id));
		o.put(L"cumulative", new JSONValue((bool) rule->cumulative));
		o.put(L"name", new JSONValue((string) rule->name));

		o.put(L"hits", new JSONValue((double) rule->getHits()));

		arr.push_back(new JSONValue(o));
	}

	ret.put(L"items", new JSONValue(arr));
	return ret;
}

JSONObject FirewallDelegate::acls_list_del_trafficshaper(JSONObject args) {
	TsRulePtr rule = firewall->trafficShaper->get(args[L"id"]->String());

	if (rule) {
		firewall->trafficShaper->del(rule);
	}
	else {
		throw DelegateGeneralException("could not find rule");
	}

	firewall->trafficShaper->invalidate();
	firewall->trafficShaper->save();
	return JSONObject();
}

JSONObject FirewallDelegate::acls_enable(JSONObject obj) {
	std::string id = obj[L"id"]->String();
	FilterRulePtr filtering_rule = firewall->acls->getRuleById(id);
	if(filtering_rule){
		filtering_rule->holdLock();
		filtering_rule->enabled = true;
		filtering_rule->releaseLock();
	}

	PriorityRulePtr priority_rule = firewall->priority_acls->getRuleById(id);
	if(priority_rule){
		priority_rule->holdLock();
		priority_rule->enabled = true;
		priority_rule->releaseLock();
	}

	firewall->priority_acls->save();
	firewall->acls->save();
	return JSONObject();
}

JSONObject FirewallDelegate::acls_disable(JSONObject obj) {
	std::string id = obj[L"id"]->String();
	FilterRulePtr filtering_rule = firewall->acls->getRuleById(id);
	if(filtering_rule){
		filtering_rule->holdLock();
		filtering_rule->enabled = false;
		filtering_rule->releaseLock();
	}

	PriorityRulePtr priority_rule = firewall->priority_acls->getRuleById(id);
	if(priority_rule){
		priority_rule->holdLock();
		priority_rule->enabled = false;
		priority_rule->releaseLock();
	}

	firewall->priority_acls->save();
	firewall->acls->save();
	return JSONObject();
}

JSONObject FirewallDelegate::acls_webfilter_rules_list(JSONObject obj) {
	JSONObject ret;
	JSONArray array;

	for (SFwallCore::ApplicationLayerFilterCriteriaPtr target : getApplicationFilter()->getRules()) {
		target->holdLock();
		JSONObject rule;
		rule.put(L"id", new JSONValue((string) target->id));

		ObjectContainer* criterias = new ObjectContainer(CARRAY);
		for(CriteriaPtr criteria: target->criteria){
			criterias->put(new ObjectWrapper(CriteriaBuilder::serialize(criteria.get())));
		}
		ObjectWrapper* criteriaWrapper = new ObjectWrapper(criterias);
		rule.put(L"criteria", ObjectWrapper::convertRecurse(criteriaWrapper));	
		delete criteriaWrapper;

		ObjectContainer* source_criterias = new ObjectContainer(CARRAY);
		for(CriteriaPtr criteria: target->source_criteria){
			criterias->put(new ObjectWrapper(CriteriaBuilder::serialize(criteria.get())));
		}
		ObjectWrapper* source_criteriaWrapper = new ObjectWrapper(source_criterias);
		rule.put(L"source_criteria", ObjectWrapper::convertRecurse(source_criteriaWrapper));
		delete source_criteriaWrapper;

		rule.put(L"action", new JSONValue((double) target->action));
		rule.put(L"fireEvent", new JSONValue((bool) target->fireEvent));
		rule.put(L"redirect", new JSONValue((bool) target->redirect));
		rule.put(L"redirectUrl", new JSONValue((string) target->redirectUrl));
		rule.put(L"name", new JSONValue((string) target->name));
		rule.put(L"enabled", new JSONValue((bool) target->enabled));
		rule.put(L"metadata", new JSONValue((string) target->metadata));

		rule.put(L"temp_rule", new JSONValue((bool) target->temp_rule));
		rule.put(L"expiry_timestamp", new JSONValue((double) target->expiry_timestamp));
		rule.put(L"hits", new JSONValue((double) target->hits));
		rule.put(L"last_hit", new JSONValue((double) target->last_hit));

		target->releaseLock();
		array.push_back(new JSONValue(rule));
	}

	ret.put(L"rules", new JSONValue(array));
	return ret;
}

JSONObject FirewallDelegate::acls_webfilter_rules_add_bulk(JSONObject obj) {
	for (auto item : obj[L"items"]->AsArray()) {
		acls_webfilter_rules_add(item->AsObject());
	}
	return JSONObject();
}

JSONObject FirewallDelegate::acls_webfilter_rules_add(JSONObject obj) {
	SFwallCore::ApplicationLayerFilterCriteriaPtr rule;
	bool newRule = true;
	std::string id;

	if (obj.has(L"id")) {
		id = obj[L"id"]->String();
		rule = getApplicationFilter()->get(id);
	}
	else {
		id =  StringUtils::genRandom();
	}

	if (!rule) {
		rule = SFwallCore::ApplicationLayerFilterCriteriaPtr(new FilterCriteria());
		rule->id = id;
	}
	else {
		newRule = false;
	}
	rule->holdLock();

	if(obj.has(L"criteria")){
		rule->criteria.clear();
		JSONArray criteria = obj[L"criteria"]->AsArray();
		for(int x= 0; x < criteria.size(); x++){
			CriteriaPtr c = CriteriaPtr(CriteriaBuilder::parse(ObjectWrapper::parseRecurse(criteria[x])->container()));
			if(c){
				rule->criteria.push_back(c);
			}
		}
	}

	if(obj.has(L"source_criteria")){
		rule->source_criteria.clear();
		JSONArray criteria = obj[L"source_criteria"]->AsArray();
		for(int x= 0; x < criteria.size(); x++){
			CriteriaPtr c = CriteriaPtr(CriteriaBuilder::parse(ObjectWrapper::parseRecurse(criteria[x])->container()));
			if(c){
				rule->source_criteria.push_back(c);
			}
		}
	}

	rule->action = (ApplicationFilterAction) obj[L"action"]->AsNumber();
	rule->fireEvent = obj[L"fireEvent"]->AsBool();
	rule->redirect = obj[L"redirect"]->AsBool();
	rule->redirectUrl = obj[L"redirectUrl"]->String();

	if (obj.has(L"enabled")) {
		rule->enabled = obj[L"enabled"]->AsBool();
	}

	if (obj.has(L"metadata")) {
		rule->metadata = obj[L"metadata"]->String();
	}
	if (obj.has(L"name")) {
		rule->name= obj[L"name"]->String();
	}
	if(obj.has(L"temp_rule")){
		rule->temp_rule = obj[L"temp_rule"]->AsBool();
		rule->expiry_timestamp = obj[L"expiry_timestamp"]->AsNumber();
	}	

	bool top = false;
	if(obj.has(L"top")){
		top = obj[L"top"]->AsBool();
	}

	if (newRule) {
		getApplicationFilter()->addRule(rule, top);
	}
	rule->releaseLock();
	getApplicationFilter()->refresh();
	getApplicationFilter()->save();
	JSONObject ret;
	ret.put(L"id", new JSONValue((rule->id)));
	return ret;
}

JSONObject FirewallDelegate::acls_webfilter_rules_deleteall(JSONObject obj) {
	for(auto rule : getApplicationFilter()->getRules()){
		getApplicationFilter()->removeRule(rule);
	}

	getApplicationFilter()->refresh();
	getApplicationFilter()->save();
	return JSONObject();
}

JSONObject FirewallDelegate::acls_webfilter_rules_remove(JSONObject obj) {
	for (SFwallCore::ApplicationLayerFilterCriteriaPtr f : getApplicationFilter()->getRules()) {
		if (f->id.compare(obj[L"id"]->String()) == 0) {
			getApplicationFilter()->removeRule(f);
			break;
		}
	}

	getApplicationFilter()->save();
	getApplicationFilter()->refresh();
	return JSONObject();
}

JSONObject FirewallDelegate::acls_webfilter_rules_moveup(JSONObject obj) {
	SFwallCore::ApplicationLayerFilterCriteriaPtr criteria = getApplicationFilter()->get(obj[L"id"]->String());
	if(criteria){
		getApplicationFilter()->moveup(criteria);
	}

	getApplicationFilter()->refresh();
	getApplicationFilter()->save();
	return JSONObject();
}

JSONObject FirewallDelegate::acls_webfilter_rules_movedown(JSONObject obj) {
	SFwallCore::ApplicationLayerFilterCriteriaPtr criteria = getApplicationFilter()->get(obj[L"id"]->String());
	if(criteria){
		getApplicationFilter()->movedown(criteria);
	}

	getApplicationFilter()->refresh();
	getApplicationFilter()->save();
	return JSONObject();
}

JSONObject FirewallDelegate::acls_webfilter_rules_enable(JSONObject obj) {
	SFwallCore::ApplicationLayerFilterCriteriaPtr criteria = getApplicationFilter()->get(obj[L"id"]->String());
	if (criteria) {
		criteria->enabled = true;
		getApplicationFilter()->save();
	}

	getApplicationFilter()->refresh();
	return JSONObject();
}

JSONObject FirewallDelegate::acls_webfilter_rules_disable(JSONObject obj) {
	SFwallCore::ApplicationLayerFilterCriteriaPtr criteria = getApplicationFilter()->get(obj[L"id"]->String());
	if (criteria) {
		criteria->enabled = false;
		getApplicationFilter()->save();
	}

	getApplicationFilter()->refresh();
	return JSONObject();
}

JSONObject FirewallDelegate::fingerprints_list(JSONObject obj){
	FingerprintStore* fingerprint_store = firewall->fingerprint_store;
	JSONObject ret;
	JSONArray fingerprints;
	for (FingerprintPtr fingerprint : fingerprint_store->available()) {
		JSONObject target;
		target.put(L"id", new JSONValue((string) fingerprint->id()));
		target.put(L"name", new JSONValue((string) fingerprint->name()));

		fingerprints.push_back(new JSONValue(target));
	}
	ret.put(L"fingerprints", new JSONValue(fingerprints));
	return ret;
}


JSONObject FirewallDelegate::securitygroups_list(JSONObject obj){
	SecurityGroupStore* store = firewall->security_groups_store;
	JSONObject ret;
	JSONArray groups;

	for(SecurityGroupPtr group : store->list_groups()){
		group->holdLock();
		JSONObject target;
		target.put(L"id", new JSONValue((string) group->id));
		target.put(L"name", new JSONValue((string) group->name));

		JSONArray target_entries;
		for(SecurityGroupEntryPtr entry : group->entries){
			JSONObject entry_target;
			entry_target.put(L"id", new JSONValue((string) entry->id));
			entry_target.put(L"name", new JSONValue((string) entry->name));

			ObjectContainer* criterias = new ObjectContainer(CARRAY);
			for(CriteriaPtr criteria: entry->criteria){
				criterias->put(new ObjectWrapper(CriteriaBuilder::serialize(criteria.get())));
			}
			ObjectWrapper* criteriaWrapper = new ObjectWrapper(criterias);
			entry_target.put(L"criteria", ObjectWrapper::convertRecurse(criteriaWrapper));
			delete criteriaWrapper;

			target_entries.push_back(new JSONValue(entry_target));
		}

		target.put(L"entries", new JSONValue(target_entries));
		groups.push_back(new JSONValue(target));
		group->releaseLock();
	}

	ret.put(L"groups", new JSONValue(groups));
	return ret;
}

JSONObject FirewallDelegate::securitygroups_add(JSONObject obj){
	SecurityGroupStore* store = firewall->security_groups_store;
	std::string id = StringUtils::genRandom(); 
	std::string name = obj[L"name"]->String();

	SecurityGroup* group = new SecurityGroup();
	group->id = id;
	group->name = name;
	store->add_group(SecurityGroupPtr(group));

	store->refresh();
	store->save();	
	return JSONObject();
}

JSONObject FirewallDelegate::securitygroups_del(JSONObject obj){
	SecurityGroupStore* store = firewall->security_groups_store;
	std::string id = obj[L"id"]->String();
	SecurityGroupPtr root_group = store->get_group(id);
	store->del_group(root_group);

	store->refresh();
	store->save();
	return JSONObject();
}

JSONObject FirewallDelegate::securitygroups_entries_add(JSONObject obj){
	SecurityGroupStore* store = firewall->security_groups_store;
	std::string id = obj[L"groupid"]->String();
	SecurityGroupPtr root_group = store->get_group(id);
	if(root_group){
		root_group->holdLock();

		SecurityGroupEntry* new_group_entry = new SecurityGroupEntry();
		new_group_entry->id = StringUtils::genRandom();
		new_group_entry->name = obj[L"name"]->String();

		root_group->entries.push_back(SecurityGroupEntryPtr(new_group_entry));
		root_group->releaseLock();
	}

	store->refresh();
	store->save();
	return JSONObject();
}

JSONObject FirewallDelegate::securitygroups_entries_update(JSONObject obj){
	JSONArray entries = obj[L"entries"]->AsArray();
	std::string group_id = obj[L"groupid"]->String();
	for(int x = 0; x < entries.size(); x++){
		JSONObject entry = entries[x]->AsObject();
		entry.put(L"groupid", new JSONValue((string) group_id));
		securitygroups_entries_set(entry);
	} 

	return JSONObject();
}
JSONObject FirewallDelegate::securitygroups_entries_set(JSONObject obj){
	SecurityGroupStore* store = firewall->security_groups_store;
	std::string groupid = obj[L"groupid"]->String();
	std::string entryid = obj[L"id"]->String();

	SecurityGroupPtr root_group = store->get_group(groupid);
	if(root_group){
		root_group->holdLock();
		for(SecurityGroupEntryPtr entry : root_group->entries){
			if(entry->id.compare(entryid) == 0){
				entry->criteria.clear();

				entry->name = obj[L"name"]->String();
				JSONArray criteria = obj[L"criteria"]->AsArray();
				for(int x= 0; x < criteria.size(); x++){
					CriteriaPtr c = CriteriaPtr(CriteriaBuilder::parse(ObjectWrapper::parseRecurse(criteria[x])->container()));
					if(c){
						entry->criteria.push_back(c);
					}
				}
			}
		}	
		root_group->releaseLock();
	}

	store->refresh();
	store->save();
	return JSONObject();
}

JSONObject FirewallDelegate::securitygroups_entries_del(JSONObject obj){
	SecurityGroupStore* store = firewall->security_groups_store;
	std::string groupid = obj[L"groupid"]->String();
	std::string entryid = obj[L"id"]->String();

	SecurityGroupPtr root_group = store->get_group(groupid);
	if(root_group){
		root_group->holdLock();
		for(SecurityGroupEntryPtr entry : root_group->entries){
			if(entry->id.compare(entryid) == 0){
				root_group->entries.remove(entry);
				break;
			}		
		}
		root_group->releaseLock();
	}

	store->refresh();
	store->save();
	return JSONObject();
}

