/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef Exceptions_H
#define Exceptions_H
#include <exception>

namespace std {
	typedef basic_string<char> string;
};

enum {
	DELEGATE_NOT_FOUND_ERRCODE = -1,
	PARSING_ERROR_ERRCODE = -4,
	SERVICE_NOT_AVAILABLE_ERRCODE = -3,
	DELEGATE_GENERAL_ERROR_ERRCODE = -5,
	AUTHENTICATION_ERROR_ERRCODE = -2
};

class JsonManagementServiceException : public std::exception {
	public:
		JsonManagementServiceException(std::string message,  int statuscode = 500, int internalCode = -1) noexcept;
		virtual ~JsonManagementServiceException() noexcept;
		virtual const char *what() const noexcept;
		std::string message() const;
		int statusCode() const;
		int internalCode() const;
	protected:
		std::string w;
		int statuscode;
		int internalcode;
};

class ParsingException : public JsonManagementServiceException {
	public:
		ParsingException(std::string message = "A field is missing or of the wrong type", int statusCode = 500, int internalCode = PARSING_ERROR_ERRCODE) noexcept;
};

class ServiceNotAvailableException : public JsonManagementServiceException {
	public:
		ServiceNotAvailableException(std::string message = "This service is currently unavailable.",  int statuscode = 503, int internalCode = SERVICE_NOT_AVAILABLE_ERRCODE) noexcept;
};

class DelegateGeneralException : public JsonManagementServiceException {
	public:
		DelegateGeneralException(std::string message, int statuscode = 500, int internalCode = DELEGATE_GENERAL_ERROR_ERRCODE) noexcept;
};

class DelegateNotFoundException : public JsonManagementServiceException {
	public:
		DelegateNotFoundException(std::string message = "Could not find the requested delegate.", int statuscode = 404,  int internalCode = DELEGATE_NOT_FOUND_ERRCODE) noexcept;
};

class AuthenticationException: public JsonManagementServiceException {
	public:
		AuthenticationException(std::string message = "Authentication Failed at this point in time", int statuscode = 403, int internalCode = AUTHENTICATION_ERROR_ERRCODE) noexcept;
};

#endif
