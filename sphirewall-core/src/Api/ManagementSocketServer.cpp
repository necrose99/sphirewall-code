/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <boost/bind.hpp>
#include <boost/smart_ptr.hpp>
#include <boost/asio.hpp>
#include <boost/thread.hpp>

#include "Api/JsonManagementService.h"
#include "Api/ManagementSocketServer.h"
#include "Core/System.h"
#include "Core/ConfigurationManager.h"

using boost::asio::ip::tcp;
using namespace std;

void ManagementSocketServer::run() {
	for(;;){
		try{
			boost::asio::io_service io_service;
			tcp::acceptor a(io_service, tcp::endpoint(tcp::v4(), port));

			Logger::instance()->log("sphirewalld.api.normal", INFO, "API::ManagementSocketServer::starting non-SSL  on port: " + intToString(port));

			for (;;) {
				socket_ptr sock(new tcp::socket(io_service));
				a.accept(*sock);

				Logger::instance()->log("sphirewalld.api.normal", DEBUG, "accepted new connection from the management service");
				boost::thread(boost::bind(&ManagementSocketServer::handleConnection, this, sock));
				Logger::instance()->log("sphirewalld.api.normal", DEBUG, "connection closed from the management service");
			}
		}catch(exception& e){
			Logger::instance()->log("sphirewalld.api.normal", ERROR, "Could not bind to management port, will try again shortly");
		}
		
		sleep(2);
	}
}

void ManagementSocketServer::handleConnection(socket_ptr sock) {
	try {
		boost::asio::streambuf response;
		boost::asio::read_until(*sock, response, "\n");
		string buffer = string((istreambuf_iterator<char>(&response)),
				istreambuf_iterator<char>());

		string ipaddress = (*sock).remote_endpoint().address().to_string();

		if (config) {
			if (config->hasByPath("general:disable_remote_management") && config->get("general:disable_remote_management")->boolean()) {
				if (ipaddress.compare("127.0.0.1") != 0) {
					Logger::instance()->log("sphirewalld.api.normal.handle", INFO, "denied management api request from " + ipaddress);
					return;
				}
			}
		}

		Logger::instance()->log("sphirewalld.api.normal.handle", DEBUG, "management api request from client: " + buffer);
		std::pair<string, int> res = jsonManagementService->process(buffer, ipaddress, false);
		Logger::instance()->log("sphirewalld.api.normal.handle", DEBUG, "management api response to client:" + res.first);

		boost::asio::streambuf request(MAX_SOCKET_DATA_LENGTH);
		std::ostream request_stream(&request);

		request_stream << res.first << "\n";
		boost::asio::write(*sock, request);

		(*sock).close();

	}
	catch (boost::exception_detail::clone_impl<boost::exception_detail::error_info_injector<boost::system::system_error> > e) {
		char message[256];
		sprintf(message,
				"management connection was terminated unexpectantly, this can be caused by an invalid terminating charater. Error: %s",
				e.what());
		Logger::instance()->log("sphirewalld.api.normal", ERROR, message);

	}
}

