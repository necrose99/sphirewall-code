/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <map>
#include <list>
#include <vector>
#include <queue>

#ifndef SPHIREWALL_CLOUD_ANA_H_INCLUDED
#define SPHIREWALL_CLOUD_ANA_H_INCLUDED
#include <boost/asio.hpp>
#include <boost/asio/ssl.hpp>

#include "BandwidthDb/BandwidthListener.h"
#include "Core/Event.h"
#include "Utils/Lock.h"
#include "Core/Cron.h"
#include "Core/SysMonitor.h"

class ConnectionStatistics;
class AnalyticsClient;
class Lock;

class CloudAnalyticsListenerException : public std::exception {
	public:
		CloudAnalyticsListenerException() noexcept {}
		virtual ~CloudAnalyticsListenerException() noexcept {}
		virtual const char *what() const noexcept {
			return "The remote service could not persist the data";
		}
};

class BaseHttpAnalyticsListener : public Lockable, public virtual MetricHandler, public virtual EventHandler, public virtual BandwidthListener {
	public:
		BaseHttpAnalyticsListener();
		int push(std::vector<ConnectionStatistics *> input);
		void operate(std::map<std::string, double> input);

		virtual bool enabled() = 0;
		virtual std::string getAnalyticsUrl() = 0;
		virtual std::string getDeviceId() = 0;
		virtual std::string getToken() = 0;
		virtual std::string name() = 0;

		//EventHandler Methods:
		void handle(EventPtr event);
		virtual int getId() = 0;
		virtual std::string key() const = 0;

		void flushEvents();
		bool dontremove(){
			return true;
		}
	private:
		std::vector<ConnectionStatistics *> errors;
		std::string sendAndRecv(std::string);
		std::list<EventPtr> bufferedEvents;

		static int CLOUD_EVENTS_PUSH_THRESHOLD;
		static int CLOUD_EVENTS_ENABLED;
};

class CloudAnalyticsListener : public virtual BaseHttpAnalyticsListener {
	public:
		bool enabled();
		std::string getAnalyticsUrl();
		std::string getDeviceId();
		std::string getToken();
		int getPort();
		std::string name();

		int getId() {
			return 99;
		}

		std::string key() const {
			return "handler.cloud";
		}
};

class ExternalAnalyticsListener : public virtual BaseHttpAnalyticsListener {
	public:
		bool enabled();
		std::string getAnalyticsUrl();
		std::string getDeviceId();
		std::string getToken();
		int getPort();
		std::string name();

		int getId() {
			return 100;
		}

		std::string key() const {
			return "handler.external";
		}
};

class CloudAnalyticsListenerEventFlushCron : public CronJob {
	public:
		CloudAnalyticsListenerEventFlushCron(std::string name, BaseHttpAnalyticsListener* root)
			: CronJob(60 * 5, name,  true) {

				this->root = root;
			}
		void run() {
			root->flushEvents();
		}

	private:
		BaseHttpAnalyticsListener* root;
};

#endif
