/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <time.h>
#include <string>
#include <map>
#include <sstream>
#include <vector>
#include <queue>
#include <boost/regex.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/asio.hpp>
#include <boost/asio/ssl.hpp>

#include "Core/ConfigurationManager.h"
#include "Core/System.h"
#include "BandwidthDb/BandwidthDbPrimer.h"
#include "BandwidthDb/CloudAnalyticsListener.h"
#include "BandwidthDb/ConnectionStatistics.h"
#include "SFwallCore/Connection.h"
#include "Json/JSON.h"
#include "Json/JSONValue.h"
#include "Utils/Utils.h"
#include "Utils/IP4Addr.h"
#include "Utils/Lock.h"

using boost::asio::ip::tcp;
using namespace boost::algorithm;
using namespace std;

int BandwidthDbPrimer::EXCLUDE_NON_WEB = 0;
int BandwidthDbPrimer::MIN_BANDWIDTH_TRANSFER_THRESHOLD = 100;
int BandwidthDbPrimer::BANDWIDTHDB_PRIMER_MAX_BUFSIZE= 50000;
int BandwidthDbPrimer::EXCLUDE_LAYER7_FILTERED= 1;

BandwidthDbPrimer::BandwidthDbPrimer(UserDb *userDb, Config *config)
	: transferLock(new Lock()), transferToAnaLock(new Lock()), CronJob(60 * 15, "BANDWIDTHDB_FLUSH_CRON", true), userDb(userDb), conf(config), primerAlreadyRunning(false) {
	config->getRuntime()->loadOrPut("BANDWIDTHDB_PRIMER_MAX_BUFSIZE", &BANDWIDTHDB_PRIMER_MAX_BUFSIZE);
	config->getRuntime()->loadOrPut("EXCLUDE_NON_WEB", &EXCLUDE_NON_WEB);
	config->getRuntime()->loadOrPut("MIN_BANDWIDTH_TRANSFER_THRESHOLD", &MIN_BANDWIDTH_TRANSFER_THRESHOLD);
	config->getRuntime()->loadOrPut("EXCLUDE_LAYER7_FILTERED", &EXCLUDE_LAYER7_FILTERED);
}


BandwidthDbPrimer::~BandwidthDbPrimer() {
}


void BandwidthDbPrimer::push(ConnectionStatistics *statistic) {
	transferLock->lock();
	bufferedConnections.push_back(statistic);
	int size = bufferedConnections.size();
	transferLock->unlock();

	if (size > BANDWIDTHDB_PRIMER_MAX_BUFSIZE && !primerAlreadyRunning) {
		try{
			boost::thread(boost::bind(&BandwidthDbPrimer::run, this));
		}catch(const exception& e){
			Logger::instance()->log("sphirewalld.bandwidthdb.run", ERROR, "Could not create thread for bandwidth processing, reason %s", e.what());
		}
	}
}

/*Cron job*/
void BandwidthDbPrimer::run() {
	primerAlreadyRunning = true;
	Logger::instance()->log("sphirewalld.bandwidthdb.run", INFO, "Beginning transmit of connection statistics");

	transferLock->lock();
	std::vector<ConnectionStatistics *> temp(bufferedConnections);
	bufferedConnections.clear();
	transferLock->unlock();

	if (temp.size() < (size_t) MIN_BANDWIDTH_TRANSFER_THRESHOLD) {
		Logger::instance()->log("sphirewalld.bandwidthdb.run", INFO, "Not enought statistics to warrant transmission");

		for (ConnectionStatistics * stats : temp) {
			this->push(stats);
		}
	}
	else {
		Logger::instance()->log("sphirewalld.bandwidthdb.run", INFO,
				"Pushing connection information to bandwidth listeners, count = %d", temp.size());

		transferToAnaLock->lock();

		for (BandwidthListener * handler : bandwidthListeners) {
			if (handler->enabled()) {
				try {
					Logger::instance()->log("sphirewalld.bandwidthdb.run", DEBUG, "running handler %s", handler->name().c_str());
					handler->push(temp);
					Logger::instance()->log("sphirewalld.bandwidthdb.run", DEBUG, "finished running handler %s", handler->name().c_str());
				}
				catch (exception &e) {
					Logger::instance()->log("sphirewalld.bandwidthdb.run", ERROR,
							"Could not push analytics data to handler %s for reason %s", handler->name().c_str(), e.what());
				}
			}
		}

		transferToAnaLock->unlock();

		for (ConnectionStatistics * stats : temp) {
			delete stats;
		}

		temp.clear();

	}
	primerAlreadyRunning = false;
}

void BandwidthDbPrimer::hook(SFwallCore::Connection *connection) {
	if (connection->getIp()->type() != IPV4) {
		return;
	}

	if (EXCLUDE_NON_WEB == 1 && connection->getApplicationInfo()->type != SFwallCore::ConnectionApplicationInfo::Classifier::HTTP) {
		return;
	}

	if (connection->getApplicationInfo()->app_filtering_denied && EXCLUDE_LAYER7_FILTERED == 1){
		return;
	}

	push(new ConnectionStatistics(connection));
}
