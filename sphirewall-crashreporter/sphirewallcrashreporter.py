import os
import socket
import fcntl, struct, socket
import datetime
import ConfigParser

LOG_PATH="/var/log"
CORE_DUMP_PATH="/core"
COMPRESS_DIR="/tmp"
config_parser = ConfigParser.ConfigParser()
config_parser.read("/etc/sphirewallcrashreporter.conf")

def get_hw_addr(ifname):
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    info = fcntl.ioctl(s.fileno(), 0x8927,  struct.pack('256s', ifname[:15]))
    return ''.join(['%02x_' % ord(char) for char in info[18:24]])[:-1]

def is_sphirewall_running():
    import commands
    output = commands.getoutput('ps -A')
    return 'sphirewalld' in output


def prepare_upload(file_path):
    print("compressing {0} and {1} to file {2}".format(LOG_PATH, CORE_DUMP_PATH, file_path))
    os.system("tar -zcf {0} {1} {2}".format(file_path, LOG_PATH, CORE_DUMP_PATH))

def get_file_path():
    return "{0}/crashreport_{1}_{2}.tar.gz".format(COMPRESS_DIR, datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S'), get_hw_addr("eth0"))


def upload(file_path):
    ftp = "ftp://{0}:{1}@{2}{3}".format(config_parser.get('general', 'ftp_user'), config_parser.get('general', 'ftp_password'), config_parser.get('general', 'ftp_url'), config_parser.get('general', 'ftp_folder'))
    print "uploading {0} to {1}".format(file_path, ftp)
    os.system("curl -v -T %s %s" % (file_path, ftp))

def restart_sphirewall():
    print("restarting sphirewall-core")
    os.system("service sphirewall-core start")

def run():
    file_path = get_file_path()
    if is_sphirewall_running():
        print "sphirewall-core is running"
    if not is_sphirewall_running():
        print("sphirewall-core is not running")
        prepare_upload(file_path)
        upload(file_path)
        restart_sphirewall()

def is_enabled():
    try:
        return config_parser.getboolean('general', 'enabled')
    except:
        return False

if  __name__ == '__main__':
    if is_enabled():
        run()
